"use strict";
/**
 * Created by Motion Picture Laboratories, Inc. (2014)
 *
 * This work is licensed under a Creative Commons Attribution (CC BY) 3.0
 * Unported License.
 */

/*
 * API Version: 0.90 Last Modified: 2014-06-28
 */

// Constants used to identify API subgroups:
// See Sec 4.2.1 of API Document
window.GRP_PM_LC = 101;
window.GRP_PM_CONN = 102;
window.GRP_PM_ENV = 103;
window.GRP_CA_AV = 104;
window.GRP_CA_EVT = 105;
window.GRP_CA_DLD = 106;
window.GRP_AA_B = 107;
window.GRP_AA_EVT = 108;
window.GRP_PI_LC = 109;
window.GRP_PI_B = 110;
window.GRP_PI_TP = 111;
window.GRP_PI_CTRL = 112;
window.GRP_PI_SND = 113;
window.GRP_PI_EVT = 114;
window.GRP_PI_GEOM = 115;
window.GRP_SN_SHARE = 116;
window.GRP_SN_EVT = 117;
window.GRP_EN_WSH = 118;
window.GRP_EN_BKM = 119;
window.GRP_EN_PH = 120;

// Constants used in StatusDescriptor:
// See Sec 4.4.2 of API Document
window.CC_OP_CANCELLED = 201;
window.CC_OP_COMPLETED = 202;
window.CC_OP_FAILED = 203;
window.CC_OP_PENDING = 204;
window.CC_OP_IN_PROGRESS = 205;
window.CC_STATE_CHANGE = 206;
window.CC_INVALID_PARAM = 207;
window.CC_UNSUPPORTED = 208;

window.LEVEL_INFO = 211;
window.LEVEL_WARNING = 212;
window.LEVEL_ERROR = 212;
window.LEVEL_FATAL = 213;

// Data structure
window.StatusDescriptor = function(level, code) {
    this.level = level;
    this.completionCode = code;
    this.message = null;
    this.context = null;
};

// Constants used in ConnectivityState:
// See Sec 4.4.1 of API Document
window.NET_CAPABILITY_LOW = 301;
window.NET_CAPABILITY_MEDIUM = 302;
window.NET_CAPABILITY_HIGH = 303;
window.NET_CAPABILITY_UNKNOWN = 304;

window.NET_STATE_CONNECTED = 311;
window.NET_STATE_CONNECTING = 312;
window.NET_STATE_DISCONNECTED = 313;
window.NET_STATE_UNKNOWN = 314;

window.NET_TYPE_BLUETOOTH = 321;
window.NET_TYPE_WIRE = 322;
window.NET_TYPE_MOBILE = 323;
window.NET_TYPE_WIFI = 324;
window.NET_TYPE_WIMAX = 325;
window.NET_TYPE_NONE = 326;
window.NET_TYPE_UNKNOWN = 327;

window.P_STATE_LOADED = 401; // synonym for STARTED added in API v0.90
window.P_STATE_STARTED = 401;
window.P_STATE_INIT = 402;
window.P_STATE_AVAILABLE = 403;
window.P_STATE_RUNNING = 404;
window.P_STATE_FAILED = 405;
window.P_STATE_EXITING = 406; // New state added in API v0.90
window.P_STATE_TERMINATED = 407;

// Section 5.1 Content Access Codes
/*
 * These codes are used when inquiries are made as to the accessibility of a
 * content item or when an event is associated with some change in
 * accessibility.
 */
/*
 * IMPORTANT: The API specifies the names but not the values.
 */

window.ACC_AVAIL_2BUY = 5001;
window.ACC_AVAIL_2RENT = 5002;
window.ACC_AVAIL_FREE = 5003;
window.ACC_AVAIL_BOUGHT = 5004;
window.ACC_AVAIL_RENTED = 5005;
window.ACC_AVAIL_PENDING = 5006;
window.ACC_AVAIL_UNAVAIL = 5007;
window.ACC_AVAIL_UNK = 5008;
window.ACC_CONTENT_UNK = 5009;

// Section 6.2.1.1 Account Access Codes

window.ACCNT_SIGNED_IN = 6001; // New code added in API v0.90
window.ACCNT_SIGNED_OUT = 6002;
window.ACCNT_PREF_CHANGE = 6003; // New code added in API v0.93

// Generic Response Codes:
// See Sec 7.2.1.1 of API Document
window.RESP_OK = 7101;
window.RESP_INVALID_STATE = 7102;
window.RESP_UNSUPPORTED_CMD = 7103;
window.RESP_UNSUPPORTED_OPT = 7104;
window.RESP_FAILED = 7105;

// Player Event Codes:
// See Sec 7.2.3.1 or 7.3.1.1 of API Document
window.PE_READY = 7200; // added in API v0.90
window.PE_PLAYING = 7201;
window.PE_PAUSED = 7202;
window.PE_STOPPED = 7203;
window.PE_SUSPENDED = 7204;
window.PE_RESUMED = 7205;
window.PE_ERROR = 7206;
window.PE_MODE_CHANGE = 7207;
// window.PE_CTRLBAR_CHANGE = 7209; // DELETED in API v0.93
window.PE_MUTE_CHANGE = 7210;
window.PE_REPOSITION_START = 7211;
window.PE_REPOSITION_END = 7212;
window.PE_TIME_EVT_P = 7213;
window.PE_TIME_EVT_R = 7214;
window.PE_VISIBILITY = 7215; // added in API v0.90
window.PE_CTRLBAR_HIDDEN = 7216; // added in API v0.93
window.PE_CTRLBAR_VISIBLE = 7217; // added in API v0.93

// See Sec 7.2.9.1 of API Document
window.PE_GEOMETRY_CHANGE = 7301;

// see Sec 7.2.4.2.1
window.RATE_QUARTER = 7401;
window.RATE_HALF = 7402;
window.RATE_NORMAL = 7403;
window.RATE_DOUBLE = 7404;
window.RATE_TRIPLE = 7405;
window.RATE_SLOWEST = 7406;
window.RATE_FASTEST = 7407;

window.LE_ADDED = 9201; // added in API v0.93
window.LE_DELETED = 9202; // added in API v0.93

/*
 * To support logging.... Text labels and descriptions for each event code are
 * NOT considered part of the API specification. text used here is intended for
 * display to developers. text intended for display to end-users should be
 * defined in the locale-specific files (e.g.
 * './scripts/locale/english/EventText.js')
 */
window.CC_Label = {};
window.CC_Label[CC_OP_CANCELLED] = "CANCELLED";
window.CC_Label[CC_OP_COMPLETED] = "COMPLETED";
window.CC_Label[CC_OP_FAILED] = "FAILED";
window.CC_Label[CC_OP_PENDING] = "PENDING";
window.CC_Label[CC_OP_IN_PROGRESS] = "IN PROGRESS";
window.CC_Label[CC_STATE_CHANGE] = "STATE CHANGE";
window.CC_Label[CC_INVALID_PARAM] = "INVALID PARAMETER";
window.CC_Label[CC_UNSUPPORTED] = "UNSUPPORTED";

window.P_STATE_Label = {};
window.P_STATE_Label[P_STATE_LOADED] = "LOADED";
window.P_STATE_Label[P_STATE_INIT] = "INITIALIZED";
window.P_STATE_Label[P_STATE_AVAILABLE] = "AVAILABLE";
window.P_STATE_Label[P_STATE_RUNNING] = "RUNNING";
window.P_STATE_Label[P_STATE_FAILED] = "FAILED";
window.P_STATE_Label[P_STATE_EXITING] = "EXITING";
window.P_STATE_Label[P_STATE_TERMINATED] = "TERMINATED";

window.PE_EventLabel = {};

window.PE_EventLabel[7200] = "READY"; /* added to API */
window.PE_EventLabel[7201] = "PLAYING";
window.PE_EventLabel[7202] = "PAUSED";
window.PE_EventLabel[7203] = "STOPPED";
window.PE_EventLabel[7204] = "SUSPENDED";
window.PE_EventLabel[7205] = "RESUMED";
window.PE_EventLabel[7206] = "ERROR";
window.PE_EventLabel[7207] = "MODE_CHANGE";
window.PE_EventLabel[7208] = "GEOMETRY_CHANGE";
window.PE_EventLabel[7209] = "CTRLBAR_CHANGE";
window.PE_EventLabel[7210] = "MUTE_CHANGE";
window.PE_EventLabel[7211] = "REPOSITION_START";
window.PE_EventLabel[7212] = "REPOSITION_END";
window.PE_EventLabel[7213] = "TIME_EVT_P";
window.PE_EventLabel[7214] = "TIME_EVT_R";
window.PE_EventLabel[7215] = "VISIBILITY_CHANGE"; /* added to API */

window.PE_EventLabel[7216] = "CTRLBAR_HIDDEN"; /* added to API v0.93 */
window.PE_EventLabel[7217] = "CTRLBAR_VISIBLE"; /* added to API  v0.93 */

window.ACC_EventLabel = {};
window.ACC_EventLabel[5001] = "Available to buy";
window.ACC_EventLabel[5002] = "Available to rent";
window.ACC_EventLabel[5003] = "Free (no purchase required)";
window.ACC_EventLabel[5004] = "Has been purchased";
window.ACC_EventLabel[5005] = "Rented and available for viewing";
window.ACC_EventLabel[5006] = "Availability pending..";
window.ACC_EventLabel[5007] = "No available";
window.ACC_EventLabel[5008] = "Availability unknown";
window.ACC_EventLabel[5009] = "The specified content ID is not recognized";