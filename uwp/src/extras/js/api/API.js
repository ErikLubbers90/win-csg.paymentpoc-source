'use strict';
var Config = require("../config.js");
var Constants = require("./utils/Constants.js");
var PackageManagement = require('./management/PackageManagement.js');
var AccountManagement = require('./management/AccountManagement.js');
var ContentManagement = require('./management/ContentManagement.js');
var PlayerManagement = require('./management/PlayerManagement.js');

var API = (function() {
    function API() {
        this.AccountMgmt = new AccountManagement();
        this.ContentMgmt = new ContentManagement();
        this.PackageMgmt = new PackageManagement();
        this.PlayerMgmt = new PlayerManagement();

        //listener required - bug in cpe
        this.WishlistMgmt = {
            addListener : function() {},
            removeListener : function() {}
        };
    }
    return API;

})();

//make singleton
module.exports = new API();