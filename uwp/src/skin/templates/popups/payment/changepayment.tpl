<div class="content payment change-payment">
    <div id="paymentContent">
        <div id="logo"></div>
        <div id="popupCloseButton" class="menu-sound closeButton" data-down="defaultFocus"><span class='buttonText'><%- manifest.close %></span></div>
        <div class="paymentContainer">
            <div class="header">
                <div class="title"><%- manifest.payment_select_payment %></div>
                <div class="sps-logo"></div>
            </div>
            <div class="divider"></div>
            <div class="payment-options">
                <% for(var i=0;i<paymentInstruments.length;i++){ %>
                    <div id="paymentOption-<%-i %>" class="button payment-option" data-paymentid="<%- paymentInstruments[i].Id %>" data-up="focusUp" data-down="focusDown" data-left="focusLeft" data-right="focusRight">
                        <% if(paymentInstruments[i].TypeName == "Credit Card"){ %>
                            <%-paymentInstruments[i].CardType%> ****-****-****-<%- paymentInstruments[i].LastPartOfID %>
                        <% }else if(paymentInstruments[i].TypeName == "PayPal Account"){ %>
                            <span class="paypal-icon"></span> <span class="lowercase">
                                <% if(typeof paymentInstruments[i].PayPalAccount !== "undefined" && typeof paymentInstruments[i].PayPalAccount.UserName !== "undefined"){ %>
                                    <%- paymentInstruments[i].PayPalAccount.UserName %>
                                <% }else{ %>
                                    <%- paymentInstruments[i].TypeName %>
                                <% } %>
                            </span>
                        <% }else{ %>
                            <%- paymentInstruments[i].Name %>
                        <% } %>


                    </div>
                    <% if((i+1)%2 == 0){ %><br/><% } %>
                <% } %>
            </div>


        <div class="divider divider-bottom"></div>
            <div class="buttons">
                <div id="continueButton" data-up="#paymentOption-1" data-right="defaultFocus" class="button"><%- manifest.continue %></div>
            </div>

        </div>
    </div>
</div>