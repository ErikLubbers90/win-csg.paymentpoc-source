<div class="content payment change-payment">
    <div id="paymentContent">
        <div id="logo"></div>
        <div id="popupCloseButton" class="menu-sound closeButton" data-down="defaultFocus"><span class='buttonText'><%- manifest.close %></span></div>
        <div class="paymentContainer">
            <div class="header">
                <div class="title"><%- manifest.payment_method_error %></div>
                <div class="sps-logo"></div>
            </div>
            <div class="divider"></div>
            <div class="payment-options">
                <% for(var i=0;i<paymentInstruments.length;i++){ %>
                    <div id="paymentOption-1" class="button payment-option" data-down="#continueButton" data-left="defaultFocus" data-right="defaultFocus">
                        <%-paymentInstruments[0].CardType%> ****-****-****-<%- paymentInstruments[0].LastPartOfID %>
                    </div>
                <% } %>
            </div>
            <div class="divider"></div>
            <div class="buttons">
                <div id="continueButton" data-up="#paymentOption-1" data-right="defaultFocus" data-down="#helpButton" class="button"><%- manifest.continue %></div>
            </div>

        </div>
    </div>
    <div class="helpContainer">
        <div class="divider"></div>
        <div class="message"><%- manifest.help_message %></div>
        <div id="helpButton" class="button" data-up="#continueButton"><%- manifest.help %></div>
    </div>
</div>