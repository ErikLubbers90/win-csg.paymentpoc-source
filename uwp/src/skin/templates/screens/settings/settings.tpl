<div id="container" class="background settings">
    <div id="menu">
        <div id="mainMenu"></div>
        <div id="subBar">

            <div id="subMenuItems">
                <div id="subMenuHeader">
                    <%- manifest.settings %>
                </div>
                <div class="divider"></div>
                <% for(var i=0;i<SubMenuItems.length;i++){ %>
                <% var subMenuItem = SubMenuItems[i] %>
                <div id="submenu_<%= i %>" class="menuButton submenu menu-sound"
                     data-left="moveToMainMenu"
                     data-right="moveToContent"
                    <% if(i > 0){ %>
                        data-up="defaultFocus"
                    <% } %>

                    <% if(i < SubMenuItems.length-1){ %>
                        data-down="defaultFocus"
                    <% } %>
                     data-id="<%= i %>" data-loginrequired="<%= (subMenuItem.loginStatus ? 1 : 0) %>"><span class="icon"></span><%= subMenuItem.Name %></div>
                <% } %>
            </div>
        </div>
    </div>
    <div id="content">

        <!-- separate account page -->
        <div id="settings_0" class="contact settingsContainer">
            <div class="content">
                <h1><%- manifest.settings_help_about %></h1>
                <div class="versionNumber"><%- version %>.<%- build %></div>
                <div class="divider"></div>
                <p><%- manifest.settings_contact_title %></p>
                <div class="contact-options">
                    <div class="option-title"><div class="icon"></div><%- manifest.settings_contact_phone %></div>
                    <div class="option-value"><%- manifest.settings_contact_phone_value %></div>

                    <div class="option-title"><div class="icon"></div><%- manifest.settings_contact_email %></div>
                    <div class="option-value"><a href="mailto:<%- manifest.settings_contact_email_value %>?subject=Sony ULTRA Windows app"><%- manifest.settings_contact_email_value %></a></div>

                    <div class="option-title"><div class="icon"></div><%- manifest.settings_contact_chat %></div>
                    <div class="option-value"><%= manifest.settings_contact_chat_value %></div>
                </div>
                <p class="copyright"><%= manifest.settings_copyright %></p>
            </div>
        </div>
        <div id="settings_1" class="faq settingsContainer">
            <div class="content">
                <h1><%- manifest.settings_faqs %></h1>
                <div class="divider"></div>
                <div class="settingsItems">
                  <div class="faq-container"><div class="faq-scroll">
                    <% for(var i=0;i<settingsManifest.faq_content.length;i++){ %>
                        <div class="faq-item" data-down="faqFocusDown" data-up="faqFocusUp" data-left="moveToMenu" >
                            <div class="question"><span class="sign">+</span><%-settingsManifest.faq_content[i].question %></div>
                            <div class="answer"><%-settingsManifest.faq_content[i].answer %></div>
                        </div>
                    <% } %>
                    <% if(settingsManifest.faq_content.length > 0){ %>
                            </div><!-- faq-scroll -->
                        </div><!-- faq-container -->
                    <% } %>
                </div>
            </div>
            <div class="navigationContainer noTimeout">
                <div id="navigationTop" class="navigationButton vertical menu-sound" data-down="defaultFocus">
                    <div class="icon" />
                </div>
                <div id="navigationBottom" class="navigationButton vertical menu-sound" data-up="defaultFocus">
                    <div class="icon" />
                </div>
            </div>
        </div>

        <div id="settings_2" class="redemption settingsContainer">
            <div class="content">
                <div id="logo"></div>
                <div class="header"><div class="title">Enter your code to recieve your offer!</div></div>
                <div class="divider"></div>                
                <input id="inputField" data-down="focusDownFromKeyboard"/>
                <div class="actionResult"></div>
                <div id="ime"></div>
                <div class="buttons">
                    <div id="submitButton" class="button" data-up="focusKeyboard">Enter</div>
                </div>
            </div>
        </div>
        <div id="settings_2b" class="redemption-success settingsContainer">
            <div class="content">
                <div class="redemptionTitle"></div>
                <div class="divider"></div>
                <div class="redemptionPlaylistContent">
                </div>
                <div class="buttons">
                    <div id="redemptionBackButton" class="button" data-left="moveToMenu" data-right="defaultFocus" data-up="itemFocusUp"><%- manifest.settings_enter_another_code %></div>
                    <div id="goToMovieButton" class="button" data-left="defaultFocus" data-up="itemFocusUp"><%- manifest.settings_go_to_ultraviolet_library %></div>
                </div>
            </div>
        </div>


        <div id="settings_3" class="account settingsContainer">
            <div class="content">
                <h1><%- manifest.settings_my_account %></h1>
                <div class="divider"></div>
                <p class="margin-bottom"><%- manifest.settings_welcome %><span class="first_name"></span></p>
                <p class="margin-bottom"><%= manifest.settings_full_account_management %></p>

                <p><%- manifest.settings_logged_in_as %><br/><span class="username white"></span></p>
                <div id="button_account" class="button light" data-left="moveToMenu" data-down="focusDown"><%- manifest.logout %></div>

                <div class="payment-content">
                    <p class="payment-title"></p>
                    <div class="payment-methods"></div>
                </div>
                <div class="pin-settings">
                    <p class="pin-title"></p>
                    <div class="pin-buttons">
                        <div id="set_pin" class="button light pin-button disable" data-left="moveToMenu" data-up="focusUp" ><%-  manifest.pin_set_purchase_pin %></div>
                        <div id="reset_pin" class="button light pin-button disable" data-left="moveToMenu" data-up="focusUp"><%- manifest.pin_change_pin %></div>
                        <div id="disable_pin" class="button light pin-button disable" data-up="focusUp" data-left="defaultFocus"><%- manifest.pin_disable_pin %></div>

                    </div>
                </div>
                <p class="uv-account-description"></p>
            </div>
        </div>

        <div id="settings_4" class="privacy serviceText settingsContainer">
            <div class="content">
                <h1><%- manifest.settings_terms_of_service %></h1>
                <div class="paginator">
                    <span class="pageNumber"></span>
                </div>
                <div class="divider"></div>
                <div class="contentText"></div>
            </div>
            <div id="termsOfServicePageLeft" data-left="moveToMenu" data-right="paginatorRight" class="pageLeft"></div>
            <div id="termsOfServicePageRight" data-left="paginatorLeft" class="pageRight"></div>
        </div>
        <div id="settings_5" class="privacy serviceText settingsContainer">
            <div class="content">
                <h1><%- manifest.settings_privacy_policy %></h1>
                <div class="paginator">
                    <span class="pageNumber"></span>
                </div>
                <div class="divider"></div>
                <div class="contentText"></div>
            </div>
            <div id="privacyPageLeft" data-left="moveToMenu" data-right="paginatorRight" class="pageLeft"></div>
            <div id="privacyPageRight" data-left="paginatorLeft" class="pageRight"></div>
        </div>
    </div><!-- end content -->
</div>
