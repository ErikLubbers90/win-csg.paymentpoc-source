<div id="container" class="imageviewer">
    <div id="screenCloseButton" class="menu-sound closeButton" data-down="defaultFocus"><span class='buttonText'><%- manifest.close %></span></div>
    <div id="slider-items">
        <ul>
            <li id="image-<%- CurrentIdx %>" class="active slider-image"><img src="<%- Images[CurrentIdx].MediaUrl %>"/></li>
        </ul>
    </div>
    <div id="galleryContent">
        <div id="controls">
            <div id="buttonLeft" class="button pageLeft light disable" data-right="focusRight"></div>
            <div id="buttonRight" class="button pageRight light disable" data-left="focusLeft" data-right="focusRight"></div>
        </div>
        <div id="thumbnails">
            <% for(var i=0;i<Images.length;i++){ %>
                <div id="leanbackImage-<%- i %>" class="leanback-image" data-left="showPrevImage" data-right="showNextImage">
                    <div class="focusBorder"></div>
                    <img src="<%- Images[i].ThumbnailUrl %>"/>
                </div>
            <% } %>
        </div>
    </div>
</div>
