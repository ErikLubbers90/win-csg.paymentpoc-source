<div id="container" class="productDetail detail">
    <div id="menu">
        <div id="mainMenu"></div>
        <div id="subBar">

            <div id="subMenuItems">
                <div id="subMenuHeader">
                    <%- SubMenuTitle %>
                </div>
                <div class="divider"></div>
                <% for(var i=0;i<SubMenuItems.length;i++){ %>
                    <% var subMenuItem = SubMenuItems[i] %>
                    <% if(!SubMenuOptionalVisible && subMenuItem.Optional){ %>
                    <% }else{ %>
                        <div id="submenu_<%= i %>" class="menuButton submenu menu-sound"
                             data-left="moveToMainMenu"
                             data-right="moveToContent"
                            <% if(i > 0){ %>
                                data-up="defaultFocus"
                            <% } %>

                            <% if(i == 0){ %>
                                data-up="focusBack"
                            <% } %>

                            <% if(i < SubMenuItems.length-1){ %>
                                data-down="defaultFocus"
                            <% } %>
                             data-id="<%= subMenuItem.Id %>"><span class="icon"></span><%= subMenuItem.Name %></div>
                    <% } %>
                <% } %>
            </div>
        </div>
    </div>
    <div class="productWindow">
    <div id="product">
        <div class="title-row">
            <% if(SeriesId == ""){ %>
                <div id="productTitle" class="title"><%= Name %></div>
            <% }else{ %>
                <div class="seasonTitle"><%- manifest.season %> <%= SeasonNumber %> <%- manifest.episode %> <%= EpisodeNumber %></div>
                <div id="productTitle" class="title"><%= SeriesName %></div>
            <% } %>
        </div>
        <div class="productInfoContainer <%= (SeriesId != '' ? ' episode': '') %>">
            <div class="thumb"><img src="<%= ThumbnailUrl %>" alt="<%= Name %>"/>
                <div class="episodeProgressDetail">
                    <% if (ViewingComplete) { %>
                    <div class="episodeProgressBar" style="width:100%"></div>
                    <% } else { %>
                    <div class="episodeProgressBar" style="width:<%- ContentProgressPercentage %>%"></div>
                    <% } %>
                </div>
                <p class="remainingWatchTimeLeft"></p>
            </div>
            <div class="productInfo">
                <% if(SeriesId != ""){ %>
                    <div id="episodeTitle" class="title"><%= Name %></div>
                <% } %>
                <div id="buttonContainer" class="buttonContainer">

                    <div id="productPlayButton" class="button light disable" data-down="focusDown" data-left="focusLeft" data-up="focusUp" data-right="focusRight"><span class="icon"></span><span class="highlight"><%= manifest.play_button %></span></div>
                    <div id="productPurchaseButton" class="button light disable" data-down="focusDown" data-left="focusLeft" data-up="focusUp" data-right="focusRight"><%= manifest.buy_button %></div>
                    <div id="productRentButton" class="button light disable" data-down="focusDown" data-left="focusLeft" data-up="focusUp" data-right="focusRight"><%= manifest.rent_button %></div>
                    <div id="productUpgradeButton" class="button light disable" data-down="focusDown" data-left="focusLeft" data-up="focusUp" data-right="focusRight"><%= manifest.upgrade_button %></div>
                    <div id="availableButton" class="button light disable" data-down="focusDown" data-left="focusLeft" data-up="focusUp" data-right="focusRight"><span class="highlight"></span></div>
                    <div id="watch10MinPreviewButton" class="button light disable" data-down="focusDown" data-up="focusUp" data-left="focusLeft" data-right="focusRight"><span class="icon"></span><span class="highlight"><%= manifest.watch_10_min_preview %></span></div>
                    <div id="watchPreviewButton" class="button light disable" data-down="focusDown" data-up="focusUp" data-left="focusLeft" data-right="focusRight"><span class="icon"></span><span class="highlight"><%= manifest.watch_preview %></span></div>
                    <div id="productPreviewExtrasButton" class="button light disable" data-down="focusDown" data-up="focusUp" data-left="focusLeft" data-right="focusRight" ><span class="icon"></span><span class="highlight"><%= manifest.preview_extras_button %></span></div>
                    <div id="productExtrasButton" class="button light disable" data-down="focusDown" data-up="focusUp" data-left="focusLeft" data-right="focusRight" ><span class="icon"></span><span class="highlight"><%= manifest.extras_button %></span></div>
                    <div id="productFavoritesButton" class="button light disable" data-down="focusDown" data-up="focusUp" data-left="focusLeft" data-right="focusRight" ><span class="icon"></span><span class="highlight"><%= manifest.favorites_button %></span></div>
                    <div id="watchSeasonButton" class="button light disable" data-down="focusDown" data-up="focusUp" data-left="focusLeft" data-right="focusRight" ><span class="icon"></span><span class="highlight"><%= manifest.view_season_button %></span></div>

                </div><!-- buttonContainer -->
                <p class="metadata">
                    <% if(GuidanceRatings.length >  0){ %>
                        <span class="guidanceRating"><%- GuidanceRatings[0].Name %></span>
                    <% } %>

                    <% if(Runtime !==  0){ %>
                        <%= Runtime %> <%= manifest.min %>
                        <span class="separator">|</span>
                    <% } %>

                    <%= ReleaseDate.substr(0,4) %>
                    <span class="separator">|</span>

                    <%- AvailableQuality %>
                    <span class="separator">|</span>

                    <% if(AvailableHDR != ""){ %>
                        <%- AvailableHDR %>
                        <span class="separator">|</span>
                    <% } %>
                    <span class="uv-icon"></span>
                </p><!-- metadata -->
                <div id="productDescriptionContainer">
                    <div id="productDescriptionMask">
                        <p class="productDescription"><%= Description.stub(470) %></p>
                        <p class="copyright">
                            <%- Copyright.stub(220) %>
                        </p>
                    </div>
                </div><!-- productDescriptionContainer -->
                <div id="productMeta">
                    <div class="metadataBlock">
                        <% if(RatingReason.length >  0){ %>
                            <span class="bold"><%- manifest.rating_reason %>:</span> <%- RatingReason %><br/>
                        <% } %>

                        <% if(typeof Genre != "undefined" && Genre.length > 0) { %>
                            <span class="bold"><%- manifest.genre %>:</span> <%- Genre %><br/>
                        <% } %>
                        <% if(Studio != ""){ %>
                            <span class="bold"><%- manifest.studio %>:</span> <%- Studio %><br/>
                        <% } %>
                    </div>
                </div>
            </div><!-- productInfo -->
            <% if(SeriesId != ""){ %>
                <div class="episodeList">
                    <div class="episodes related-episodes">
                        <div id="prevEpisode" data-left="moveToMenu" data-right="focusRight" data-up="focusUp" class="episode-thumbnail disable">
                            <div class="related-title">
                                <span class="blue-rect"></span><%- manifest.previous_episode %>
                            </div>
                            <div class="thumbnail-border"></div>
                            <img src=""/>
                            <div class="episode-meta">
                                <div class="white">Episode <span class="episode-number"></span></div>
                                <div class="episode-title"><span class="episode-name"></span></div>
                            </div>
                        </div>
                        <div id="nextEpisode" data-left="focusLeft" data-up="focusUp" class="episode-thumbnail disable">
                            <div class="related-title">
                                <span class="blue-rect"></span><%- manifest.next_episode %>
                            </div>
                            <div class="thumbnail-border"></div>
                            <img src=""/>
                            <div class="episode-meta">
                                <div class="white">Episode <span class="episode-number"></span></div>
                                <div class="episode-title"><span class="episode-name"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            <% } %>
            <div class="productExtras">

                <% if(RelatedVideos.length > 0){ %>
                    <div class="relatedVideos">
                        <div class="extraTitle"><div class="icon"></div><%- manifest.explore_clips %></div>
                        <div class="thumbnail-container">
                            <div class="scroll-container">
                                <% for(var i=0;i<RelatedVideos.length;i++){ %>
                                    <div id="relatedVideo-<%- i %>" data-left="focusLeft" data-right="focusRight" data-down="focusDown" data-up="focusUp" class="extra-thumbnail video">
                                        <div class="thumbnail-border"></div>
                                        <div class="play-icon"></div>
                                        <div class="lock-icon"></div>
                                        <img src="<%- RelatedVideos[i].ThumbnailUrl %>"/>
                                    </div>
                                <% } %>
                            </div>
                            <% if(RelatedVideos.length > 4){ %>
                                <!--#navigation arrows-->
                                <div class="navigationContainer">
                                    <div id="navigationLeftVideos" class="navigationButton menu-sound" data-right="defaultFocus">
                                        <div class="icon" />
                                    </div>
                                    <div id="navigationRightVideos" class="navigationButton menu-sound" data-left="defaultFocus">
                                        <div class="icon" />
                                    </div>
                                </div>
                            <% } %>
                        </div>
                    </div>
                <% } %>

                <% if(RelatedImages.length > 0){ %>
                    <div class="relatedImages">
                        <div class="extraTitle"><div class="icon"></div><%- manifest.explore_images %></div>
                        <div class="thumbnail-container">
                            <div class="scroll-container">
                                <% for(var i=0;i<RelatedImages.length;i++){ %>
                                    <div id="relatedImage-<%- i %>" class="extra-thumbnail image" data-left="focusLeft" data-right="focusRight" data-down="focusDown" data-up="focusUp">
                                        <div class="thumbnail-border"></div>
                                        <img src="<%- RelatedImages[i].ThumbnailUrl %>"/>
                                    </div>
                                <% } %>
                            </div>
                            <% if(RelatedImages.length > 4){ %>
                                <!--#navigation arrows-->
                                <div class="navigationContainer">
                                    <div id="navigationLeftImages" class="navigationButton menu-sound" data-right="defaultFocus">
                                        <div class="icon" />
                                    </div>
                                    <div id="navigationRightImages" class="navigationButton menu-sound" data-left="defaultFocus">
                                        <div class="icon" />
                                    </div>
                                </div>
                            <% } %>
                        </div>
                    </div>
                <% } %>
            </div><!-- productExtras -->

        </div>
    </div><!-- end of product -->
    <div class="navigationContainer">
        <div id="navigationTop" class="navigationButton vertical menu-sound" data-down="defaultFocus">
            <div class="icon" />
        </div>
        <div id="navigationBottom" class="navigationButton vertical menu-sound" data-up="defaultFocus">
            <div class="icon" />
        </div>
    </div>

    </div>
</div>
