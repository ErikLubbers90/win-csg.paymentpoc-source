<div id="keyboard">
    <div id="keyboardScreen">
        <div id="keyboardFrame">
            <div class="characters">
                <ul>
                </ul>
            </div>
            <div class="numpad">
                <ul>
                    <li id="num_1" class="numbers keyboard-sound" data-left="moveNumLeft" data-right="defaultFocus" data-up="moveNumUp" data-down="moveNumDown"><div class="glow"></div>1</li>
                    <li id="num_2" class="numbers keyboard-sound" data-left="moveNumLeft" data-right="defaultFocus" data-up="moveNumUp" data-down="moveNumDown"><div class="glow"></div>2</li>
                    <li id="num_3" class="numbers keyboard-sound" data-left="moveNumLeft" data-right="moveNumRight" data-up="moveNumUp" data-down="moveNumDown"><div class="glow"></div>3</li>
                    <li id="num_4" class="numbers keyboard-sound" data-left="moveNumLeft" data-right="defaultFocus" data-up="moveNumUp" data-down="moveNumDown"><div class="glow"></div>4</li>
                    <li id="num_5" class="numbers keyboard-sound" data-left="moveNumLeft" data-right="defaultFocus" data-up="moveNumUp" data-down="moveNumDown"><div class="glow"></div>5</li>
                    <li id="num_6" class="numbers keyboard-sound" data-left="moveNumLeft" data-right="moveNumRight" data-up="moveNumUp" data-down="moveNumDown"><div class="glow"></div>6</li>
                    <li id="num_7" class="numbers keyboard-sound" data-left="moveNumLeft" data-right="defaultFocus" data-up="moveNumUp" data-down="moveNumDown"><div class="glow"></div>7</li>
                    <li id="num_8" class="numbers keyboard-sound" data-left="moveNumLeft" data-right="defaultFocus" data-up="moveNumUp" data-down="moveNumDown"><div class="glow"></div>8</li>
                    <li id="num_9" class="numbers keyboard-sound" data-left="moveNumLeft" data-right="moveNumRight" data-up="moveNumUp" data-down="moveNumDown"><div class="glow"></div>9</li>
                    <li id="num_10" class="characterMoveCarot keyboard-sound" data-left="moveNumLeft" data-right="moveNumRight" data-up="moveNumUp" data-down="moveNumDown"><div class="glow"></div> < </li>
                    <li id="num_11" class="numbers keyboard-sound" data-left="moveNumLeft" data-right="moveNumRight" data-up="moveNumUp" data-down="moveNumDown"><div class="glow"></div>0</li>
                    <li id="num_12" class="characterMoveCarot keyboard-sound" data-left="moveNumLeft" data-right="moveNumRight" data-up="moveNumUp" data-down="moveNumDown"><div class="glow"></div> > </li>
                </ul>
            </div>
        </div>
        <div class="" id="backButton" data-up="backtoKeyboard">
            <div class="close"></div><div class="closeTxt">Close</div>
        </div>
    </div>
</div>
<div class="fakeCursor">^</div>
