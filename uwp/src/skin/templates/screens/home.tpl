<div id="container" class="background">
  <div id="logout" class="button" style="float:right;" data-down="#azureToken"><div class="glow"></div>Logout</div>
  <h1>UWP Tokens</h1>
    <div id="leftButton" class="button" data-down="#azureToken"><div class="glow"></div>Retrieve UWP Token</div><br/>
    <div style="width:60%; height:45px">
      Access Token:
      <input style="float:right" id="azureToken" data-down="#collectionsToken"/><br/>
    </div>
    <!-- <div style="width:60%; height:45px">
      Collections Token:
      <input style="float:right" id="collectionsToken" data-down="#leftButton"/><br/>
    </div> -->
    <!-- <div style="width:60%; height:45px">
      Publisher User ID:
      <input style="float:right" id="publisherUserID" data-down="#leftButton"/><br/>
    </div> -->
    <div id="rightButton" class="button" data-up="#azureToken"><div class="glow"></div>Retrieve Store ID</div><br/>
    <div style="width:60%; height:55px">
      Windows store ID:
      <span style="float:right" id="storeid"/>
    </div>
    <div id="azureError" style="color:red;"/>
  <h1>Product</h1>
  <div id="products">
  </div>
  <div style="width:60%; height:45px">
    Product ID:
    <input style="float:right" id="sku" data-up="#leftButton" data-down="#pricingplanid"/>
  </div>
  <div style="width:60%; height:45px">
    Pricing Plan ID:
    <input style="float:right" id="pricingplanid" data-up="#sku" data-down="#productname"/>
  </div>
  <div style="width:60%; height:45px">
    Product name:
    <input style="float:right" id="productname" data-up="#pricingplanid"/>
  </div>
  <div style="width:60%; height:45px">
    Windows Product Store ID:
    <input style="float:right" id="productStoreID" data-up="#rightButton" data-down="#submitWindowsOrder"/>
  </div>
  <div style="font-size:14px;">*Note: if these fields are left blank the app defaults to ProductID: 329341, PricingPlanID: 4468, Name: "Captain America", Windows ID: "9n4x54z69n4s"</div>
  <div id="submitWindowsOrder" class="button" data-up="#rightButton"><div class="glow"></div>Purchase product</div><br/>
  <div id="windowsError" style="color:red;"/>
  <div id="windowsMessage"/>
  <div id="submitCSGOrder" class="button" data-up="#paymentinstrumentid"><div class="glow"></div>Submit order to CSG</div><br/>
  <div id="purchaseError" style="color:red;"/>
  <div id="purchaseMessage"/>
</div>
