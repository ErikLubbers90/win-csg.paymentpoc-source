<div class="content payment payment-success">
    <div id="paymentContent">
        <div id="logo"></div>
        <div class="paymentContainer">
            <% if(productThumbnail != undefined && productThumbnail != ""){ %><div class="thumbnail"><img src="<%= productThumbnail %>" /></div><% } %>
            <div class="header">
                <div class="title"><%- manifest.payment_success_title %></div>
                <div class="sps-logo"></div>
            </div>
            <div class="divider<% if(productThumbnail != undefined && productThumbnail != ""){ %> withThumbnail<% } %>"></div>
            <div class="success-message"><%- manifest.payment_success_description %></div>

            <div class="buttons">
                <!--<div id="watchNowButton" data-up="#paymentOption-1" data-right="defaultFocus" class="button"><%- manifest.payment_success_watch_now %></div>-->
                <div id="continueButton" data-up="#paymentOption-1" class="button"><%- manifest.payment_success_continue %></div>
            </div>

        </div>
    </div>
</div>