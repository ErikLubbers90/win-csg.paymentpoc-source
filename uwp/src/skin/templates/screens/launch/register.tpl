<div class="content login register">
    <div id="loginContent" class="fullscreen">
        <div id="logo"></div>
        <div class="loginContainer">
          <div class="header">
              <div class="title grey">Endpoint</div>
          </div><br/><br/><br/>
          <div class="divider"></div>
          <div id="loginResult"></div>
          <input id="inputFieldEndpoint" data-down="focusDownFromKeyboard" placeholder="Endpoint" /><br /><br/>
          <div id="currentEndpoint">
            TestTest
          </div><br/>
          <div class="buttons">
              <div id="setEndpoint" data-up="focusUp" data-down="focusDown" data-left="defaultFocus" data-right="defaultFocus" class="button"><div class="glow"></div>Set endpoint</div>
          </div>
          <div class="header">
              <div class="title grey">SystemID</div>
          </div><br/><br/><br/>
          <div class="divider"></div>
          <div id="loginResult"></div>
          <input id="inputFieldSystemID" data-down="focusDownFromKeyboard" placeholder="SystemID" /><br /><br/>
          <div id="currentSystemID">
            TestTestID
          </div><br/>
          <div class="buttons">
              <div id="setSystemID" data-up="focusUp" data-down="focusDown" data-left="defaultFocus" data-right="defaultFocus" class="button"><div class="glow"></div>Set SystemID</div>
          </div>
            <div class="header">
                <div class="title grey">Log into your account</div>
            </div>
            <div class="divider"></div><br /><br /><br />
            <div id="loginResult"></div>
            <input id="inputFieldEmail" data-down="focusDownFromKeyboard" placeholder="Email Address" /><br /><br />
            <input id="inputFieldPassword" data-down="focusDownFromKeyboard" type="password" placeholder="Password" /><br /><br />
            <div class="buttons">
                <div id="loginButton" data-up="focusUp" data-down="focusDown" data-left="defaultFocus" class="button"><div class="glow"></div>Login</div>
            </div>
        </div>
    </div>
</div>
