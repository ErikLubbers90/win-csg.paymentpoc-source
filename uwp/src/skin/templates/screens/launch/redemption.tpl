<div class="content redemption">
    <div id="redemptionContent" class="fullscreen">
        <div id="logo"></div>        
        <div class="header"><div class="title"><%= manifest.redemption_enter_your_code %></div></div>
        
        <div class="buttonsContainer">                        
            <div class="divider"></div>
            <input id="inputField" data-down="focusDownFromKeyboard"/>
            <div class="buttons">        
                <div id="submitPromoButton" data-up="#inputField" class="button"><div class="glow"></div><%- manifest.redemption_enter %></div>
            </div>
        </div>
    </div>

    <div id="promotionContent" class="fullscreen redemption-success settingsContainer">
        <div class="redemptionTitle"></div><div class="redemptionSubtitle"></div>
        <div class="divider"></div>
        <div class="content">            
            <div class="redemptionPlaylistContent"></div>
        </div>
        <div class="buttons">        
            <div id="submitRedemptionButton" data-up="#inputField" class="button disabled"><%- manifest.redemption_redeem %></div>
        </div>
    </div>    

    <div id="successContent" class="fullscreen redemption-success settingsContainer">
        <div id="logo"></div>        
        <div class="header"><div class="title"><%- manifest.account_success %></div></div>
        <div class="divider"></div>
        <div class="subtitle"><%= manifest.redemption_titles_added %></div>       
            
        <div class="buttons">        
            <div id="browseMoviesButton" class="button" data-right="#browseLibraryButton"><%- manifest.account_button_browse_movies %></div>
            <div id="browseLibraryButton" class="button" data-left="#browseMoviesButton"><%- manifest.redemption_go_to_library %></div>
        </div>
    </div>        

</div>

