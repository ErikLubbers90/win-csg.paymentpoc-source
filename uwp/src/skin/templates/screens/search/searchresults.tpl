<% if(Products.length > 0){ %>
    <div class="playlistContent">
        <% for(var j=0;j<Math.min(Products.length, 18);j++) { %>
            <% product = Products[j] %>
            <div id="playlist_product-<%= j %>" class="product content-sound" data-left="focusLeft" data-right="focusRight" data-down="focusDown" data-id="<%= product.Id %>" data-type="<%= product.StructureType %>">
                <div class="productBorder"></div>
                <img src="<%= product.ThumbnailUrl %>" alt="<%= product.Name %>" />
                <div class="productTitle"><% if(product.Name.length > 20){ print("<marquee>"+product.Name+"</marquee>")} else { print(product.Name) } %></div>
            </div>
        <% } %>
    </div>
<% }else if(keyword.length > 2){ %>
    <div class="noResultsContent">
        <div class="info"><span class="blueRect"></span>Info</div>
        <div class="description">There are no products available for display</div>
    </div>
<% }else{ %>
    <div id="logo"></div>
<% } %>
