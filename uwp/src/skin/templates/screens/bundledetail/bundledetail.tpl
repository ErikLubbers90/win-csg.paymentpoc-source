<div id="container" class="bundle-detail detail">
    <div id="menu">
        <div id="mainMenu"></div>
        <div id="subBar">

            <div id="subMenuItems">
                <div id="subMenuHeader">
                    <%- SubMenuTitle %>
                </div>
                <div class="divider"></div>
                <% for(var i=0;i<SubMenuItems.length;i++){ %>
                    <% var subMenuItem = SubMenuItems[i] %>
                    <% if(!SubMenuOptionalVisible && subMenuItem.Optional){ %>
                    <% }else{ %>
                        <div id="submenu_<%= i %>" class="menuButton submenu menu-sound"
                             data-left="moveToMainMenu"
                             data-right="moveToContent"
                             <% if(i > 0){ %>
                                 data-up="defaultFocus"
                             <% } %>

                             <% if(i == 0){ %>
                                 data-up="focusBack"
                             <% } %>

                             <% if(i < SubMenuItems.length-1){ %>
                                 data-down="defaultFocus"
                             <% } %>
                             data-id="<%= subMenuItem.Id %>"><span class="icon"></span><%= subMenuItem.Name %></div>
                    <% } %>
                <% } %>
            </div>
        </div>
    </div>
    <div class="productWindow">
    <div id="product">
        <div class="title-row">
            <div class="seasonTitle"><%- manifest.season %> <%= SeasonNumber %></div>
            <div id="productTitle" class="title"><%= Name %></div>
        </div>
        <div class="productInfoContainer">
            <div class="thumb"><img src="<%= ThumbnailUrl %>" alt="<%= Name %>"/></div>
            <div class="productInfo">
                <div id="buttonContainer" class="buttonContainer">

                    <div id="productPurchaseButton" class="button light disable" data-down="focusDown" data-left="focusLeft" data-up="focusUp" data-right="focusRight"><%= manifest.buy_button %></div>
                    <div id="productUpgradeButton" class="button light disable" data-down="focusDown" data-left="focusLeft" data-up="focusUp" data-right="focusRight"><%= manifest.upgrade_button %></div>
                    <div id="watchPreviewButton" class="button light disable" data-down="focusDown" data-up="focusUp" data-left="focusLeft" data-right="focusRight"><span class="icon"></span><span class="highlight"><%= manifest.watch_preview %></span></div>
                    <div id="productPreviewExtrasButton" class="button light disable" data-down="focusDown" data-up="focusUp" data-left="focusLeft" data-right="focusRight" ><span class="icon"></span><span class="highlight"><%= manifest.preview_extras_button %></span></div>
                    <div id="productExtrasButton" class="button light disable" data-down="focusDown" data-up="focusUp" data-left="focusLeft" data-right="focusRight" ><span class="icon"></span><span class="highlight"><%= manifest.extras_button %></span></div>
                    <div id="productFavoritesButton" class="button light disable" data-down="focusDown" data-up="focusUp" data-left="focusLeft" data-right="focusRight" ><span class="icon"></span><span class="highlight"><%= manifest.favorites_button %></span></div>
                    <div id="productSeasonsButton" class="button light disable" data-down="focusDown" data-up="focusUp" data-left="focusLeft" data-right="focusRight" ><span class="icon"></span><span class="highlight"><%= manifest.view_seasons_button %></span></div>

                </div><!-- buttonContainer -->
                <p class="metadata">
                    <% if(GuidanceRatings.length >  0){ %>
                        <span class="guidanceRating"><%- GuidanceRatings[0].Name %></span>
                    <% } %>

                    <% if(Runtime !==  0){ %>
                        <%= Runtime %> <%= manifest.min %>
                        <span class="separator">|</span>
                    <% } %>

                    <%= ReleaseDate.substr(0,4) %>
                    <span class="separator">|</span>

                    <span class="uv-icon"></span>
                </p><!-- metadata -->
                <div id="productDescriptionContainer">
                    <div id="productDescriptionMask">
                        <p class="productDescription"><%= Description.stub(650) %></p>
                        <p class="copyright">
                            <%- Copyright.stub(300) %>
                        </p>
                    </div>
                </div><!-- productDescriptionContainer -->
                <div id="productMeta">
                    <div class="metadataBlock">
                        <% if(RatingReason.length >  0){ %>
		                        <span class="bold"><%- manifest.rating_reason %>:</span> <%- RatingReason %><br/>
		                    <% } %>

                        <% if(typeof Genre != "undefined" && Genre.length > 0) { %>
                            <span class="bold"><%- manifest.genre %>:</span> <%- Genre %><br/>
                        <% } %>
                        <% if(Studio != ""){ %>
                            <span class="bold"><%- manifest.studio %>:</span> <%- Studio %><br/>
                        <% } %>

                    </div>
                </div>
            </div><!-- productInfo -->
            <div class="episodeList">
                <% if(BundleChildren.length > 0){ %>
                    <div class="episodes">
                        <% for(var i=0;i<BundleChildren.length;i++){ %>
                            <% episode = BundleChildren[i] %>
                            <div id="episode-<%- i %>" data-left="focusLeft" data-right="focusRight" data-down="focusDown" data-up="focusUp" class="episode-thumbnail video" data-id="<%= episode.Id %>" data-type="<%= episode.StructureType %>">
                                <div class="thumbnail-border"></div>
                                <img src="<%- episode.ThumbnailLandscape %>"/>
                                <div class="episodeProgress">
                                    <% if (episode.ViewingComplete) { %>
                                        <div class="episodeProgressBar" style="width:100%"></div>
                                    <% } else { %>
                                        <div class="episodeProgressBar" style="width:<%- episode.ContentProgressPercentage %>%"></div>
                                    <% } %>
                                </div>
                                <div class="episode-meta">
                                    <div class="white">Episode <%- episode.EpisodeNumber %></div>
                                    <div class="episode-title"><%- episode.Name %></div>
                                </div>
                            </div>
                        <% } %>
                    </div>
                <% } %>
        </div>
    </div><!-- end of product -->
    </div>
    <div class="navigationContainer">
        <div id="navigationTop" class="navigationButton vertical menu-sound" data-down="defaultFocus">
            <div class="icon" />
        </div>
        <div id="navigationBottom" class="navigationButton vertical menu-sound" data-up="defaultFocus">
            <div class="icon" />
        </div>
    </div>
</div>
