/* Win10 API features */
var started = false;
var init = (function() {
    console.log("Win10 before init", Windows);
    if(Windows) {
        console.log('Win10 init');

        var ViewManagement = Windows.UI.ViewManagement;
        var ApplicationViewWindowingMode = ViewManagement.ApplicationViewWindowingMode;
        var ApplicationView = ViewManagement.ApplicationView;

        // Set default fullscreen
        Windows.UI.WebUI.WebUIApplication.addEventListener("activated", function () {
            if (!started) {
                ApplicationView.getForCurrentView().setDesiredBoundsMode(ViewManagement.ApplicationViewBoundsMode.useCoreWindow);
                started = true;
            }
        }, false);

    }
})(this);
