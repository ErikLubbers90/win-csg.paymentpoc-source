<?php
//error_reporting(-1);
//ini_set('display_errors', 'On');

/**
 * URL constants as defined in the PHP Manual under "Constants usable with
 * http_build_url()".
 *
 * @see http://us2.php.net/manual/en/http.constants.php#http.constants.url
 */
if (!defined('HTTP_URL_REPLACE')) {
    define('HTTP_URL_REPLACE', 1);
}
if (!defined('HTTP_URL_JOIN_PATH')) {
    define('HTTP_URL_JOIN_PATH', 2);
}
if (!defined('HTTP_URL_JOIN_QUERY')) {
    define('HTTP_URL_JOIN_QUERY', 4);
}
if (!defined('HTTP_URL_STRIP_USER')) {
    define('HTTP_URL_STRIP_USER', 8);
}
if (!defined('HTTP_URL_STRIP_PASS')) {
    define('HTTP_URL_STRIP_PASS', 16);
}
if (!defined('HTTP_URL_STRIP_AUTH')) {
    define('HTTP_URL_STRIP_AUTH', 32);
}
if (!defined('HTTP_URL_STRIP_PORT')) {
    define('HTTP_URL_STRIP_PORT', 64);
}
if (!defined('HTTP_URL_STRIP_PATH')) {
    define('HTTP_URL_STRIP_PATH', 128);
}
if (!defined('HTTP_URL_STRIP_QUERY')) {
    define('HTTP_URL_STRIP_QUERY', 256);
}
if (!defined('HTTP_URL_STRIP_FRAGMENT')) {
    define('HTTP_URL_STRIP_FRAGMENT', 512);
}
if (!defined('HTTP_URL_STRIP_ALL')) {
    define('HTTP_URL_STRIP_ALL', 1024);
}
if (!function_exists('http_build_url')) {
    /**
     * Build a URL.
     *
     * The parts of the second URL will be merged into the first according to
     * the flags argument.
     *
     * @param mixed $url     (part(s) of) an URL in form of a string or
     *                       associative array like parse_url() returns
     * @param mixed $parts   same as the first argument
     * @param int   $flags   a bitmask of binary or'ed HTTP_URL constants;
     *                       HTTP_URL_REPLACE is the default
     * @param array $new_url if set, it will be filled with the parts of the
     *                       composed url like parse_url() would return
     * @return string
     */
    function http_build_url($url, $parts = array(), $flags = HTTP_URL_REPLACE, &$new_url = array())
    {
        is_array($url) || $url = parse_url($url);
        is_array($parts) || $parts = parse_url($parts);
        isset($url['query']) && is_string($url['query']) || $url['query'] = null;
        isset($parts['query']) && is_string($parts['query']) || $parts['query'] = null;
        $keys = array('user', 'pass', 'port', 'path', 'query', 'fragment');
        // HTTP_URL_STRIP_ALL and HTTP_URL_STRIP_AUTH cover several other flags.
        if ($flags & HTTP_URL_STRIP_ALL) {
            $flags |= HTTP_URL_STRIP_USER | HTTP_URL_STRIP_PASS
                | HTTP_URL_STRIP_PORT | HTTP_URL_STRIP_PATH
                | HTTP_URL_STRIP_QUERY | HTTP_URL_STRIP_FRAGMENT;
        } elseif ($flags & HTTP_URL_STRIP_AUTH) {
            $flags |= HTTP_URL_STRIP_USER | HTTP_URL_STRIP_PASS;
        }
        // Schema and host are alwasy replaced
        foreach (array('scheme', 'host') as $part) {
            if (isset($parts[$part])) {
                $url[$part] = $parts[$part];
            }
        }
        if ($flags & HTTP_URL_REPLACE) {
            foreach ($keys as $key) {
                if (isset($parts[$key])) {
                    $url[$key] = $parts[$key];
                }
            }
        } else {
            if (isset($parts['path']) && ($flags & HTTP_URL_JOIN_PATH)) {
                if (isset($url['path']) && substr($parts['path'], 0, 1) !== '/') {
                    $url['path'] = rtrim(
                            str_replace(basename($url['path']), '', $url['path']),
                            '/'
                        ) . '/' . ltrim($parts['path'], '/');
                } else {
                    $url['path'] = $parts['path'];
                }
            }
            if (isset($parts['query']) && ($flags & HTTP_URL_JOIN_QUERY)) {
                if (isset($url['query'])) {
                    parse_str($url['query'], $url_query);
                    parse_str($parts['query'], $parts_query);
                    $url['query'] = http_build_query(
                        array_replace_recursive(
                            $url_query,
                            $parts_query
                        )
                    );
                } else {
                    $url['query'] = $parts['query'];
                }
            }
        }
        if (isset($url['path']) && substr($url['path'], 0, 1) !== '/') {
            $url['path'] = '/' . $url['path'];
        }
        foreach ($keys as $key) {
            $strip = 'HTTP_URL_STRIP_' . strtoupper($key);
            if ($flags & constant($strip)) {
                unset($url[$key]);
            }
        }
        $parsed_string = '';
        if (isset($url['scheme'])) {
            $parsed_string .= $url['scheme'] . '://';
        }
        if (isset($url['user'])) {
            $parsed_string .= $url['user'];
            if (isset($url['pass'])) {
                $parsed_string .= ':' . $url['pass'];
            }
            $parsed_string .= '@';
        }
        if (isset($url['host'])) {
            $parsed_string .= $url['host'];
        }
        if (isset($url['port'])) {
            $parsed_string .= ':' . $url['port'];
        }
        if (!empty($url['path'])) {
            $parsed_string .= $url['path'];
        } else {
            $parsed_string .= '/';
        }
        if (isset($url['query'])) {
            $parsed_string .= '?' . $url['query'];
        }
        if (isset($url['fragment'])) {
            $parsed_string .= '#' . $url['fragment'];
        }
        $new_url = $url;
        return $parsed_string;
    }
}


/* SONY
 * This code is provided just as a reference and may contain untested parts of code
 * and bugs.
*/

header('Content-type: application/vnd.ms-playready.initiator+xml');
//header('Content-type: text/plain');
error_log("****************** Webinitiator LOG START ******************");

//content url, the link to the actual protected ismc (manifest) file
$contentUrl = null;
if (isset($_GET["contentUrl"])) {$contentUrl=$_GET["contentUrl"];} else { exit(401); }
error_log("Content URL : ".$contentUrl);


$laUrl = null;
$kid = null;
$customData = null;
$checksum = null;

//License acquisition url
if (isset($_GET["laUrl"])) {
    $laUrl=$_GET["laUrl"];
    error_log("LA Url (param): ".$laUrl);
}

//kid for content url
if (isset($_GET["kid"])) {
    $kid=$_GET["kid"];
    error_log("KID (param): ".$kid);
}

//checksum for content url
if (isset($_GET["checksum"])) {
    $checksum=$_GET["checksum"];
    error_log("Checksum (param): ".$checksum);
}

//If parameters are passed, do not use Manifest
if (!isset($_GET["laUrl"]) or !isset($_GET["kid"])) {
    $climString = curl_get_contents ($contentUrl);
    $climArr = simplexml_load_string ($climString);

    if ($climArr === FALSE) {
        error_log("failed to load contentUrl: ".$contentUrl);
    }
    else {
        //If Manifest includes protection header, get information from there
        if (isset ($climArr->Protection->ProtectionHeader)) {
            error_log("Manifest contains protection header");
            $protectionStr = base64_decode($climArr->Protection->ProtectionHeader);
            $protectionStr = preg_replace('/[\x00-\x1F\x80-\xFF]/','', $protectionStr);

            if (!isset($_GET["checksum"])) {
                preg_match ('@^.+<CHECKSUM>?(.+)</CHECKSUM>.+@i', $protectionStr, $tmp);
                $checksum = $tmp[1];
                error_log("Checksum (manifest): ".$checksum);
            }

            if (!isset($_GET["kid"])) {
                preg_match ('@^.+<KID>?(.+)</KID>.+@i', $protectionStr, $tmp);
                $kid = $tmp[1];
                error_log("KID (manifest): ".$kid);
            }

            if (!isset($_GET["laUrl"])) {
                preg_match ('@^.+<LA_URL>?(.+)</LA_URL>.+@i', $protectionStr, $tmp);
                $laUrl = $tmp[1];
                error_log("LA Url (manifest): ".$laUrl);
            }
        }
        if (isset ($climArr->Period->AdaptationSet) && isset ($climArr->Period->AdaptationSet[0]->ContentProtection)) {
            if (!isset($_GET["kid"])) {
                preg_match('/<mspr\:kid>(.*)<\/mspr\:kid>/', $climString, $tmp);
                $kid = $tmp[1];
                error_log("KID (manifest): " . $kid);
            }
        }
    }
}

//customData for content url
if (isset($_GET["customData"])) {
    $customData=$_GET["customData"];
    //plus sign automatically removed by php, make sur eit is correctly re-added
    $customData = urlencode($customData);
    $customData = str_replace("+", "%2B",$customData);
    $customData = urldecode($customData);
}
error_log("Custom Data (param): ".$customData);

if (filter_var($contentUrl, FILTER_VALIDATE_URL) === FALSE) {
    //canonicalize into absolute URL
    $contentUrl = http_build_url ((isset ($_SERVER['HTTPS']) ? "https" : "http")."://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'],
        $contentUrl,
        HTTP_URL_JOIN_PATH | HTTP_URL_STRIP_QUERY | HTTP_URL_STRIP_FRAGMENT);
}
error_log("Content URL : ".$contentUrl);



//-- GENERATE the Initiator XML
$xmlInitiator=
    "<?xml version=\"1.0\" encoding=\"utf-8\"?>".
    "<PlayReadyInitiator xmlns=\"http://schemas.microsoft.com/DRM/2007/03/protocols/\">".
    "<LicenseAcquisition>".
    "<Header>".
    "<WRMHEADER xmlns=\"http://schemas.microsoft.com/DRM/2007/03/PlayReadyHeader\" version=\"4.0.0.0\">".

    "<DATA>".
    "<PROTECTINFO>".
    "<KEYLEN>16</KEYLEN>".
    "<ALGID>AESCTR</ALGID>".
    "</PROTECTINFO>".
    "<LA_URL>".xmlencode($laUrl)."</LA_URL>";
if ($kid) {
    $xmlInitiator=$xmlInitiator."<KID>".$kid."</KID>";
}

if ($checksum) {
    $xmlInitiator=$xmlInitiator."<CHECKSUM>".$checksum."</CHECKSUM>";
}

$xmlInitiator=$xmlInitiator.
    "</DATA>".
    "</WRMHEADER>".
    "</Header>";
if ($customData) {
    $xmlInitiator=$xmlInitiator."<CustomData>".$customData."</CustomData>";
} else {
    //This optional tag is required in some Sony devices!
    $xmlInitiator=$xmlInitiator."<CustomData></CustomData>";
}

$xmlInitiator=$xmlInitiator."<Content>".xmlencode($contentUrl)."</Content>".
    "</LicenseAcquisition>".
    "</PlayReadyInitiator>";
error_log("XML : ".$xmlInitiator);
echo $xmlInitiator;
error_log("****************** Webinitiator LOG END ******************");
exit(200);

// -------------------- FUNCTIONS --------------------------------

function xmlencode($instr) {
    return str_replace(array("&","<",">","\"","'"),array("&amp;","&lt;","&gt;","&quot;","&apos;"),$instr);
}


function curl_get_contents ($url)
{
    $request = curl_init ($url);
    curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
    $ret = curl_exec ($request);
    curl_close ($request);
    return $ret;
}

?>
