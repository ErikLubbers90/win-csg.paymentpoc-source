var fs = require('fs-extra');
var path = require('path');
module.exports = function (grunt, platform, config) {
    grunt.log.write("platform: "+platform);
    var index, data;
	index = "./build/"+platform+"/index.html";
	data = grunt.file.read(index);

    //insert viewport metatag
    data = data.replace('charset=UTF-8" />', "charset=UTF-8\" /> \n        <meta name=\"viewport\" content=\"user-scalable=no, width=1280\">");
    //
    ////add cache buster to js and css files
    data = data.replace(/.js/g, function(match, contents, offset, s){
            return ".js?"+(Math.floor(Math.random() * 9999999) + 1000000);
    });
    //data = data.replace(/.css/g, function(match, contents, offset, s){
    //    return ".css?"+(Math.floor(Math.random() * 9999999) + 1000000);
    //});

	grunt.file.write(index, data);
};