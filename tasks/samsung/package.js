module.exports = function (grunt, platform, config) {
  grunt.log.write("platform: "+platform);
  var index, data
  var packageUrl = (config.packageUrl+"").replace("@platform", platform);
	index = "./package/"+platform+"/index.html";
  data = '<html>\n\
      <head>\n\
          <meta http-equiv="refresh" content="0;url='+packageUrl+'">\n\
      </head>\n\
      <script>location.href="'+packageUrl+'";</script>\n\
  </html>'

	grunt.file.write(index, data);

  //update config.xml
  var configxml, configdata;
  configxml = "./package/"+platform+"/config.xml";
  configdata = grunt.file.read(configxml);
  configdata = configdata.replace("@app.version", config.version.slice(0,3) + ""+ (config.build+"").slice(-2)); //slice last 2 digits from build number since orsay does not support more then 3
    configdata = configdata.replace("@app.organisation", config.organisation);
  configdata = configdata.replace("@app.appTitle", config.title);
  configdata = configdata.replace("@app.desc", config.description);
    configdata = configdata.replace("@app.email", config.email);
    configdata = configdata.replace("@app.link", config.link);
  configdata = configdata.replace("@app.category", config.category);
  grunt.file.write(configxml, configdata);
};
