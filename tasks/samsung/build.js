module.exports = function (grunt, platform, config) {
    grunt.log.write("platform: "+platform);
    var index, sdkdata, data
	index = "./build/"+platform+"/index.html";
	data = grunt.file.read(index);

	//insert platform sdk.inc
	sdkdata = grunt.file.read("./tasks/"+platform+"/sdk.inc")
    data = data.replace("<!-- js -->", sdkdata);

	grunt.file.write(index, data);

	//copy fonts to root build dir
	grunt.file.expand("./build/"+platform+"/skin/fonts/*").forEach(function(file) {
        grunt.file.copy(file, file.replace("/skin/fonts", ""));
    });

	//update config.xml
	var configxml, configdata;
	configxml = "./build/"+platform+"/config.xml";
	configdata = grunt.file.read(configxml);
	configdata = configdata.replace("@app.version", config.version.slice(0,3) + ""+ (config.build+"").slice(-2)); //slice last 2 digits from build number since orsay does not support more then 3
    configdata = configdata.replace("@app.organisation", config.organisation);
	configdata = configdata.replace("@app.appTitle", config.title);
	configdata = configdata.replace("@app.desc", config.description);
    configdata = configdata.replace("@app.email", config.email);
    configdata = configdata.replace("@app.link", config.link);
	configdata = configdata.replace("@app.category", config.category);
	grunt.file.write(configxml, configdata);

};
