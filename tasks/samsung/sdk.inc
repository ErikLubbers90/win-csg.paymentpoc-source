
            <!-- Definitions -->
        	<script language="javascript" type="text/javascript" src="$MANAGER_WIDGET/Common/Plugin/Define.js"></script>

        	<!-- API-->			
        	<script language="javascript" type="text/javascript" src="$MANAGER_WIDGET/Common/API/Widget.js"></script>
        	<script language="javascript" type="text/javascript" src="$MANAGER_WIDGET/Common/API/TVKeyValue.js"></script>
        	<script language="javascript" type="text/javascript" src="$MANAGER_WIDGET/Common/API/Plugin.js"></script>
            <script language="javascript" type="text/javascript" src="$MANAGER_WIDGET/Common/webapi/1.0/webapis.js"></script>

        	<!-- UTIL -->				
        	<script language="javascript" type="text/javascript" src="$MANAGER_WIDGET/Common/Util/Include.js"></script>
        	<script language="javascript" type="text/javascript" src="$MANAGER_WIDGET/Common/Util/Language.js"></script>

        	<!-- IME -->
            <script language="javascript" type="text/javascript" src="$MANAGER_WIDGET/Common/IME_XT9/ime.js"></script>
        	<script language="javascript" type="text/javascript" src="$MANAGER_WIDGET/Common/IME_XT9/inputCommon/ime_input.js"></script>

			<!-- OBJECTS -->
        	<object id="pluginPlayer" style="visibility: hidden; width: 0px; height: 0px; opacity: 0.0" classid="clsid:SAMSUNG-INFOLINK-PLAYER"></object>
        	<object id="pluginAudio" style="visibility: hidden; width: 0px; height: 0px; opacity: 0.0" classid="clsid:SAMSUNG-INFOLINK-AUDIO"></object>

        	<object id="pluginObjectTVMW" style="visibility: hidden; width: 0px; height: 0px; opacity: 0.0" classid="clsid:SAMSUNG-INFOLINK-TVMW"></object>
        	<object id="pluginObjectTV" style="visibility: hidden; width: 0px; height: 0px; opacity: 0.0" classid="clsid:SAMSUNG-INFOLINK-TV"></object>
        	<object id="pluginWindow" style="visibility: hidden; width: 0px; height: 0px; opacity: 0.0" classid="clsid:SAMSUNG-INFOLINK-WINDOW"></object>

        	<object id="pluginAppCommon" style="visibility: hidden; width: 0px; height: 0px; opacity: 0.0" classid="clsid:SAMSUNG-INFOLINK-APPCOMMON"></object>
        	<object id="pluginObjectNNavi" style="visibility: hidden; width: 0px; height: 0px; opacity: 0.0" classid="clsid:SAMSUNG-INFOLINK-NNAVI"></object>
        	<object id="wvPlugin" style="visibility: hidden; width: 0px; height: 0px; opacity: 0.0" classid="clsid:SAMSUNG-INFOLINK-EXTERNALWIDGETINTERFACE"></object>
        	<object id="networkPlugin" style="visibility: hidden; width: 0px; height: 0px; opacity: 0.0" classid="clsid:SAMSUNG-INFOLINK-NETWORK"></object>
        