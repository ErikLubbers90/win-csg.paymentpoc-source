var fs = require('fs-extra');
var path = require('path');
module.exports = function (grunt, platform, config) {
    grunt.log.write("platform: "+platform);
    var index, sdkdata, data;
	index = "./build/"+platform+"/index.html";
	data = grunt.file.read(index);

    //insert viewport metatag
    data = data.replace('charset=UTF-8" />', "charset=UTF-8\" /> \n        <meta name=\"viewport\" content=\"user-scalable=no, width="+config.resolutions["windows10"][0]+"\">");

	//insert platform sdk.inc
	sdkdata = grunt.file.read("./tasks/"+platform+"/sdk.inc");
    data = data.replace("<!-- js -->", sdkdata);

	grunt.file.write(index, data);

    //update cordova config with version number from package.json
    // cordovaConfigPath = "./cordova/config.xml";
    // data = grunt.file.read(cordovaConfigPath);
    // data = data.replace(/version="(.*?)"/, 'version="'+config.version+'"')
    //     .replace(/\<name\>(.*?)\<\/name\>/, '<name>'+config.title+'</name>');
    //
    // grunt.file.write(cordovaConfigPath, data);

    //copy build dir to cordova folder
    fs.emptyDirSync(path.resolve(__dirname, "../../uwp/src"));
    fs.copySync(path.resolve(__dirname, "../../build/windows10"), path.resolve(__dirname, "../../uwp/src/"));
};
