﻿//// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
//// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
//// PARTICULAR PURPOSE.
////
//// Copyright (c) Microsoft Corporation. All rights reserved

// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232509

var g_mseSupported;
var g_upgrade_button;
var g_dismiss_button;
var g_library;
var g_videoFrame;
var g_video;
var g_msMediaKeys;
var g_selectedItem;
var g_player;
var g_chainedLicense;
var g_persistentLicense;
var g_secureStopEnabled;
var g_enabelSecureStopButton = false;
var g_HDCPType1;
var g_uncompDigitalVideo;
var g_ignoreLDLTimer;
var g_adaptiveStreaming = Windows.Media.Streaming.Adaptive;
var g_mediaSource = null;
var g_url;

var g_OPLValues = ["None", "0", "100", "150", "200", "201", "250", "270", "300"];

var g_videoLibrary = [
        {
            title: "Big Buck Bunny",
            property: "Clear",
            attribution: "© Copyright 2008, Blender Foundation / www.bigbuckbunny.org",
						baseUrl: "http://amssamples.streaming.mediaservices.windows.net/683f7e47-bd83-4427-b0a3-26a6c4547782/BigBuckBunny.ism/manifest(format=mpd-time-csf)",
						src: "http://amssamples.streaming.mediaservices.windows.net/683f7e47-bd83-4427-b0a3-26a6c4547782/BigBuckBunny.ism/manifest(format=mpd-time-csf)",
            fileType: "manifest(format=mpd-time-csf)",
            LAURL: "null",
            proactiveLA: "null",
            LDL: "null",
            kid1: "null",
            kid2: "null",
            kidRoot: "null",
            secureStopURL: "null"
        },
        {
            title: "Tears of Steel",
            property: "Clear",
            attribution: "© Copyright Blender Foundation | mango.blender.org",
            baseUrl: "http://amssamples.streaming.mediaservices.windows.net/bc57e088-27ec-44e0-ac20-a85ccbcd50da/TearsOfSteel.ism/manifest(format=mpd-time-csf)",
            fileType: "manifest(format=mpd-time-csf)",
            LAURL: "null",
            proactiveLA: "null",
            LDL: "null",
            kid1: "null",
            kid2: "null",
            kidRoot: "null",
            secureStopURL: "null"
        },
        {
            title: "DASH DualKey Sample (AV sep)",
            property: "Protected",
            attribution: "Proactive License Acquisition",
            baseUrl: "http://playready.directtaps.net/media/dash/dualkey/dash.mpd",
            fileType: "manifest(format=mpd-time-csf)",
            LAURL: "http://playready.directtaps.net/svc/pr30priv/rightsmanager.asmx?PlayRight=1",
            proactiveLA: "true",
            LDL: "false",
            kid1: "SFNBRGhGRkdPS1VTdmlkZQ==",
            kid2: "SFNBRGhGRkdPS1VTc291bg==",
            kidRoot: "u9G4VugLLUSV/En1i2JnYw==",  // only when chained license is selected
            secureStopURL: "http://playready.directtaps.net/svc/pr30stage/rightsmanager.asmx"
        },
        {
            title: "DASH DualKey Sample (AV sep, LDL)",
            property: "Protected",
            attribution: "Proactive License Acquisition",
            baseUrl: "http://playready.directtaps.net/media/dash/dualkey/dash.mpd",
            fileType: "manifest(format=mpd-time-csf)",
            LAURL: "http://playready.directtaps.net/svc/pr30priv/rightsmanager.asmx?PlayRight=1",
            proactiveLA: "true",  // Available only in the proactive LA
            LDL: "true",
            kid1: "SFNBRGhGRkdPS1VTdmlkZQ==",
            kid2: "SFNBRGhGRkdPS1VTc291bg==",
            kidRoot: "u9G4VugLLUSV/En1i2JnYw==", // only when chained license is selected
            secureStopURL: "http://playready.directtaps.net/svc/pr30stage/rightsmanager.asmx"
        },
        {
            title: "sintel trailer (1080p)",
            property: "Protected",
            attribution: "Reactive License Acquisition",
            baseUrl: "http://wams.edgesuite.net/media/SintelTrailer_Smooth_from_WAME_CENC/NoSubSampleAdjustment/sintel_trailer-1080p.ism/manifest(format=mpd-time-csf)",
            fileType: "manifest(format=mpd-time-csf)",
            LAURL: "http://playready.directtaps.net/svc/pr30priv/rightsmanager.asmx?PlayRight=1",
            proactiveLA: "false",
            LDL: "false",
            kid1: "null",
            kid2: "null",
            kidRoot: "Voad717JSkCgQTx5POiRSw==",
            secureStopURL: "http://playready.directtaps.net/svc/pr30stage/rightsmanager.asmx"
        },
        {
            title: "Tears of Steel (1080p)",
            property: "Protected",
            attribution: "Reactive License Acquisition",
            baseUrl: "http://wams.edgesuite.net/media/Tears_of_Steel_Smooth_1080p_Protected2/tears_of_steel_1080p.ism/manifest(format=mpd-time-csf)",
            fileType: "manifest(format=mpd-time-csf)",
            LAURL: "http://playready.directtaps.net/svc/pr30priv/rightsmanager.asmx?PlayRight=1",
            proactiveLA: "false",
            LDL: "false",
            kid1: "null",
            kid2: "null",
            kidRoot: "Yh3qcXy0iUyktEeiS09V4g==",
            secureStopURL: "http://playready.directtaps.net/svc/pr30stage/rightsmanager.asmx"
        }
    ];

function getItemDetails(idx) {
    //
    // Format the title (tooltip) base on the properties in
    // the selected item in g_videoLibrary.
    // Skip the property if it is 'null'.
    //
    var details = "Details:\n\n";
    var content = g_videoLibrary[idx];
    for (var prop in content) {
        if (content.hasOwnProperty(prop)) {
            if (content[prop] != 'null') {
                details += prop + ": " + content[prop] + "\n";
            }
        }
    }

    return details;
}

function init(video)
{
    // UI elements/functions.
    g_library = document.getElementById("library");
    g_videoFrame = document.getElementById("videoFrame");

		videoContainer = document.getElementById("vpPlayer");
		videoContainer.innerHTML = "<video id='videoPlayer' autoplay controls></video>";
		g_video = videoContainer.getElementById("videoPlayer");

    g_msMediaKeys = null;

    g_video.addEventListener("ended", video_onended, false);

    g_mseSupported = detectMSESupport();

    g_video.addEventListener("msneedkey", function (e) {

        console.log("Received needkey message");

        if (!g_video.msKeys) {
            console.log("Call video element to set media keys");
            try {
                g_video.msSetMediaKeys(g_msMediaKeys);

                if (g_selectedItem.proactiveLA == "false") {
                    var reactiveLA = new ReactivePlayReadyManager(g_selectedItem, g_selectedItem.LDL == "true", false);
                    reactiveLA.ReactiveLicenseAcquire(g_msMediaKeys, e.initData, function () {
                    });
                }

            } catch (e) {
                throw "Fail to set media keys. Verify the components are installed and functional. Original error: " + e.message;
            }
        } else {
            console.log("NeedKey in progress");
        }

    }, false);

		 onVideoSelected(0);
}

function mapAdaptiveMediaSourceCreationStatus(status) {
    var statusStrings = ["Success",
                         "ManifestDownloadFailure",
                         "ManifestParseFailure",
                         "UnsupportedManifestContentType",
                         "UnsupportedManifestVersion",
                         "UnsupportedManifestProfile",
                         "UnknownFailure"];

    if (status < 0 || status > statusStrings.length) {
        return "Unknown AdaptiveMediaSourceCreationStatus";
    }

    return statusStrings[status];
}

function onMediaSourceCreated(result) {
    if (result.status === g_adaptiveStreaming.AdaptiveMediaSourceCreationStatus.success) {

        console.log("AdaptiveMediaSource.createFromUriAsync completed with status: " + result.status + " - " + mapAdaptiveMediaSourceCreationStatus(result.status));
        g_mediaSource = result.mediaSource;

        try {
            if (g_mediaSource != null) {

                g_video.src = URL.createObjectURL(g_mediaSource, { oneTimeOnly: true });
                console.log("Set media element src to the AdaptiveMediaSource for url: " + g_url);
            }
        } catch (e) {
            WinJS.log && WinJS.log("EXCEPTION: " + e.toString(), "sample", "error");
        }

    } else {
        var errorString = "";
        var httpResponseMessage = result.httpResponseMessage;

        if (httpResponseMessage != null) {
            errorString = " (HTTP response: " + httpResponseMessage.statusCode + " - " + httpResponseMessage.reasonPhrase;

            if (httpResponseMessage.isSuccessStatusCode &&
                result.status == g_adaptiveStreaming.AdaptiveMediaSourceCreationStatus.unsupportedManifestContentType &&
                httpResponseMessage.content != null) {
                errorString += "; Content-Type: " + httpResponseMessage.content.headers.contentType;
            }

            errorString += ")";

        }
        console.log("Failed to create adaptive media source: " + mapAdaptiveMediaSourceCreationStatus(result.status) + errorString);
    }
}

function startVideo(url)
{
    console.log("Start video");

    resetVideo();

    var rbDashSourceInBox = document.getElementById("rbDashSourceInBox");
    if (rbDashSourceInBox.checked) {
        console.log("Use inbox DASH media source");

        try {
            g_url = new Windows.Foundation.Uri(url);

            console.log("Creating AdaptiveMediaSource for url: " + url);

            g_adaptiveStreaming.AdaptiveMediaSource.createFromUriAsync(g_url).done(
                function completed(result) {
                    onMediaSourceCreated(result);
                });

        } catch (e) {
            console.log("function(startVideo): exception ", e);
        }
    } else {
        console.log("Use dash.all.js");

        if (g_player == undefined || g_player == null) {
            g_player = new MediaPlayer(new Dash.di.DashContext());
        }

        g_player.startup();
        g_player.attachView(g_video);
        g_player.setAutoPlay(true);
        g_player.attachSource(url);
    }
}

function resetVideo() {

    if (g_player != undefined && g_player != null) {

        g_player.attachView(null);
        g_player.attachSource(null);
    }

    g_video.removeAttribute("src");
    g_mediaSource = null;
}

function processSelectedVideo()
{
    if (g_selectedItem.proactiveLA == "true") {

        if (g_selectedItem.LDL == "true") {

            console.log("Acquiring limited duration licenses.");
            var proactiveLDL = new ProactivePlayReadyManager(g_selectedItem, true, false);
            proactiveLDL.ProactiveLicenseAcquire(g_msMediaKeys, function () {
                startVideo(g_selectedItem.baseUrl);
                g_video.controls = true;
                g_ignoreLDLTimer = false;

                // Wait 30 seconds before acquiring full licenses
                setTimeout(function () {
                    if (!g_ignoreLDLTimer) {
                        // The user havsn't switched to other video since this timer started
                        // continue the full license acquisition.
                        console.log("Acquiring full licenses.");
                        var proactiveFull1 = new ProactivePlayReadyManager(g_selectedItem, false, false);
                        proactiveFull1.ProactiveLicenseAcquire(g_msMediaKeys, null);
                    }
                }, 30000);
            });

        } else {

            console.log("Acquiring simple/leaf licenses.");
            var proactiveLDL = new ProactivePlayReadyManager(g_selectedItem, false, false);
            proactiveLDL.ProactiveLicenseAcquire(g_msMediaKeys, function () {
                startVideo(g_selectedItem.baseUrl);
                g_video.controls = true;
            });
        }


    } else {

        // Start video to trigger reactive license acquisition
        console.log("Reactive license acquisition.");
        startVideo(g_selectedItem.baseUrl);
        g_video.controls = true;
    }
}

function onVideoSelected(index)
{
    // Set below flag to true in case the previous video is LDL
    // to stop the full license acquisition.
    g_ignoreLDLTimer = true;

    // Disable cbOptOutHWDRM button
    var cbOptOutHWDRM = document.getElementById("cbOptOutHWDRM");
    cbOptOutHWDRM.disabled = true;
    var bOptOutHWDRM = document.getElementById("bOptOutHWDRM");
    bOptOutHWDRM.style.color = "Gray";

    resetVideo();

    // Create a global media keys to create all the session.
    // This is required for non-persistent licenses.
    g_msMediaKeys = new MSMediaKeys("com.microsoft.playready");

    g_chainedLicense = cbChainLicense.checked;
    g_persistentLicense = cbPersistLicense.checked;
    g_secureStopEnabled = cbSecureStop.checked;
    g_HDCPType1 = cbHDCPType1.checked;

    g_uncompDigitalVideo = parseInt(sUncompDigitalVideo.value, 10);

    if (g_persistentLicense && g_secureStopEnabled) {
        console.log("Error: secure stop is for non-persistent license only!");
        return;
    }

    g_selectedItem = g_videoLibrary[index];

    btnSecureStop.disabled = true;
    if (g_secureStopEnabled && g_selectedItem.property == 'Protected') {
        g_enabelSecureStopButton = true;
        btnSecureStop.title = "This button will be enabled when the playback ends.";
    }
    else {
        g_enabelSecureStopButton = false;
        btnSecureStop.title = "This button is available for secure stop content only.";
    }

    if (g_selectedItem.property == "Protected") {

        if (g_chainedLicense) {
            console.log("Acquiring root licenses.");
            var proactiveLDL = new ProactivePlayReadyManager(g_selectedItem, g_selectedItem.LDL == "true", true);
            proactiveLDL.ProactiveLicenseAcquire(g_msMediaKeys, function () {
                processSelectedVideo();
            });
        } else {
            processSelectedVideo();
        }

    } else {

        // Play clear video
        startVideo(g_selectedItem.baseUrl);
        g_video.controls = true;
    }
}

function video_onended()
{
    console.log("video ended");
    if( g_enabelSecureStopButton ) {
        btnSecureStop.disabled = false;
    }
}

function btnSecureStop_click()
{
    ProcessSecureStop(g_msMediaKeys, g_selectedItem.secureStopURL);
}

function btClearLog_click()
{
    clearMsg();
}

//this function is not working when playing any content
function btnDeleteHDS_click()
{
    deleteHDS();
}

function deleteHDS(complete) {
    // Delete the PlayReady folder from local folder then create an empty PlayReady folder
    Windows.Storage.ApplicationData.current.localFolder.getFolderAsync("playready").then(function (folder) {
        return folder.deleteAsync().then(function (folder) {
            Windows.Storage.ApplicationData.current.localFolder.createFolderAsync("playready");
        }).done(function () {
                console.log('License store has been deleted');
                if (complete != null) {
                    complete();
                }
            }, function (e) {
                if (e.number == -2147024894) // ignore file not found error (0x80070002)
                {
                    if (complete != null) {
                        complete();
                    }
                }
                else {
                    var msg = 'Delete license store failed. Please close the app and relaunch it, then try delete license store again!';
                    console.log(msg);
                    var md = new Windows.UI.Popups.MessageDialog(msg);
                    md.showAsync();
                }
            });
    }).done(function () {
        // complete, do nothing
    }, function (e) {
        // error, do nothing
    });
}

function cbOptOutHWDRM_click() {
    var localSettings = Windows.Storage.ApplicationData.current.localSettings;
    var ContainerPlayReady;

    if (cbOptOutHWDRM.checked)
    {
        if (localSettings.containers.hasKey("PlayReady"))
        {
            console.log('ContainsKey PlayReady exists ');
            ContainerPlayReady = localSettings.containers.lookup("PlayReady");
        }
        else
        {
            console.log('create a new Container Key PlayReady ');
            ContainerPlayReady = localSettings.createContainer("PlayReady", Windows.Storage.ApplicationDataCreateDisposition.always);
        }
        console.log('set SoftwareOverride = 1 ');
        ContainerPlayReady.values["SoftwareOverride"] = 1;
    }
    else
    {
        console.log('set SoftwareOverride = 0 ');
        if (localSettings.containers.hasKey("PlayReady"))
        {
            ContainerPlayReady = localSettings.containers.lookup("PlayReady");
            ContainerPlayReady.values["SoftwareOverride"] = 0;
        }
    }

    //re-initilize when switching between SWDRM and HWDRM
    clearMsg();

    var videoFrame = document.getElementById("videoFrame");
    var video = document.getElementById("video");

    video.pause();
    video.msSetMediaProtectionManager(null);
    video.removeAttribute("src");
    video.load();

    videoFrame.removeChild(video);
    videoFrame.appendChild(video);
}

// if localSettings.Containers["PlayReady"].Values["SoftwareOverride"] = 1
// set  cbOptOutHWDRM.checked = true
// otherwise leave cbOptOutHWDRM alone
// this function need to be called right after app launched
function setSoftwareOverride_And_cbOptOutHWDRM() {
    var localSettings = Windows.Storage.ApplicationData.current.localSettings;
    if (localSettings.containers.hasKey("PlayReady")) {
        // "PlayReady" key exists, because "cbOptOutHWDRM" has been checked before, continue to set remembered previous setting
        var SoftwareOverride = localSettings.containers.lookup("PlayReady").values["SoftwareOverride"];

        if (SoftwareOverride != 'undefined' && SoftwareOverride == 1) {
            console.log('SoftwareOverride = 1 is already set, so continue mark cbOptOutHWDRM as checked');
            cbOptOutHWDRM.checked = true;
        }
    }
}

function bChainLicense_click() {
    var chkBox = document.getElementById("cbChainLicense");
    chkBox.checked = !chkBox.checked;
}

function bPersistLicense_click() {
    var chkBox = document.getElementById("cbPersistLicense");
    chkBox.checked = !chkBox.checked;
}

function bSecureStop_click() {
    var chkBox = document.getElementById("cbSecureStop");
    chkBox.checked = !chkBox.checked;
}

function bOptOutHWDRM_click() {
    var chkBox = document.getElementById("cbOptOutHWDRM");
    chkBox.checked = !chkBox.checked;
}

function bHDCPType1_click() {
    var chkBox = document.getElementById("cbHDCPType1");
    chkBox.checked = !chkBox.checked;
}

function rbDashSourceInBox_click() {
    setMediaSource("rbDashSourceInBox");
}

function rbDashSourceDashJS_click() {
    setMediaSource("rbDashSourceDashJS");
}

function setMediaSource(radioBtnName) {
    var inBox = document.getElementById("rbDashSourceInBox");
    var dashJS = document.getElementById("rbDashSourceDashJS");
    if (radioBtnName == "rbDashSourceInBox") {
        dashJS.checked = !inBox.checked;
    } else {
        inBox.checked = !dashJS.checked;
    }
}

function bDashSourceInBox_click() {
    document.getElementById("rbDashSourceInBox").checked = true;
    document.getElementById("rbDashSourceDashJS").checked = false;
}

function bDashSourceDashJS_click() {
    document.getElementById("rbDashSourceInBox").checked = false;
    document.getElementById("rbDashSourceDashJS").checked = true;
}
