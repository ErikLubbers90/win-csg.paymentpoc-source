module.exports = function (grunt, platform, config) {
    grunt.log.write("platform: "+platform);
    var index, data
	index = "./build/"+platform+"/index.html";
	data = grunt.file.read(index);
	
	//replace doctypes and mime
//    data = data.replace("text/html;", "application/ce-html+xml;");
//    data = data.replace("<!DOCTYPE html>", '<?php \n header(\'Content-type: application/vnd.hbbtv.xhtml+xml; charset=utf-8\'); \n echo \'<?xml version="1.0" encoding="UTF-8"?>\'; \n?> \n<!DOCTYPE html PUBLIC "-//HbbTV//1.1.1//EN" "http://www.hbbtv.org/dtd/HbbTV-1.1.1.dtd">');

	//insert platform sdk.inc
	sdkdata = grunt.file.read("./tasks/"+platform+"/sdk.inc")
    data = data.replace("<!-- js -->", sdkdata);
    
	grunt.file.write(index, data);
	
	//convert html file to php
//	var fs = require('fs');
//	fs.rename(index, index.replace(".html", ".php"))
};