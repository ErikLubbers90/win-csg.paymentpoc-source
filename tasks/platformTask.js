'use strict';

module.exports = function(grunt) {
    //custom task to run platform specific grunt script found in /task dir
    grunt.registerTask("platformTask", "exec custom platform grunt script", function(){
        var config = grunt.config.get("config");

        var options = this.options({});

        var exists = grunt.file.exists("tasks/"+options.platform+"/build.js");
        if (exists) {
            require("./"+options.platform+"/build.js")(grunt, options.platform, config);
        }

    });
};
