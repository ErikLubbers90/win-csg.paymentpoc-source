module.exports = function (grunt, platform, config) {
  grunt.log.write("platform: "+platform);
  var index, data
  var packageUrl = (config.packageUrl+"").replace("@platform", platform);
	index = "./package/"+platform+"/index.html";
  data = '<html>\n\
      <head>\n\
          <meta http-equiv="refresh" content="0;url='+packageUrl+'">\n\
      </head>\n\
      <script>location.href="'+packageUrl+'";</script>\n\
  </html>'

	grunt.file.write(index, data);
};
