module.exports = function (grunt, platform, config) {
    grunt.log.write("platform: "+platform);
    var index, sdkdata, data
	index = "./build/"+platform+"/index.html";
	data = grunt.file.read(index);
	
	//insert platform sdk.inc
	sdkdata = grunt.file.read("./tasks/"+platform+"/sdk.inc")
    data = data.replace("<!-- js -->", sdkdata);
    
	grunt.file.write(index, data);

};