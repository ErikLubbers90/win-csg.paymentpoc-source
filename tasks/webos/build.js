module.exports = function (grunt, platform, config) {
  grunt.log.write("platform: "+platform);
  var index, sdkdata, data
	index = "./build/"+platform+"/index.html";
	data = grunt.file.read(index);

	//insert platform sdk.inc
	sdkdata = grunt.file.read("./tasks/"+platform+"/sdk.inc")
    data = data.replace("<!-- js -->", sdkdata);

	grunt.file.write(index, data);

  //update appinfo webos file
  configfile = "./build/"+platform+"/appinfo.json";
  configdata = grunt.file.read(configfile);
  configdata = configdata.replace(/\@app\.environment/g, config.environment);
  configdata = configdata.replace(/\@app\.version/g, config.version);
  configdata = configdata.replace(/\@app\.name/g, config.name);
  configdata = configdata.replace(/\@app\.appTitle/g, config.title);
  configdata = configdata.replace(/\@app\.organisation/g, config.organisation);
	configdata = configdata.replace(/\@app\.desc/g, config.description > 60 ? config.description.substring(0,57)+"..." : config.description);
  configdata = configdata.replace(/\@app\.resolution/g, config.resolutions["webos"][0]+"x"+config.resolutions["webos"][1]);

  grunt.file.write(configfile, configdata);
};
