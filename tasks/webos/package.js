module.exports = function (grunt, platform, config) {
  grunt.log.write("platform: "+platform);
  var index, data
  var packageUrl = (config.packageUrl+"").replace("@platform", platform);
	index = "./package/"+platform+"/index.html";
  data = '<html>\n\
      <head>\n\
          <meta http-equiv="refresh" content="0;url='+packageUrl+'">\n\
      </head>\n\
      <script>location.href="'+packageUrl+'";</script>\n\
  </html>'

	grunt.file.write(index, data);

  //update appinfo webos file
  configfile = "./package/"+platform+"/appinfo.json";
  configdata = grunt.file.read(configfile);
  configdata = configdata.replace(/\@app\.environment/g, config.environment);
  configdata = configdata.replace(/\@app\.version/g, config.version);
  configdata = configdata.replace(/\@app\.name/g, config.name);
  configdata = configdata.replace(/\@app\.appTitle/g, config.title);
  configdata = configdata.replace(/\@app\.organisation/g, config.organisation);
  configdata = configdata.replace(/\@app\.desc/g, config.description > 60 ? config.description.substring(0,57)+"..." : config.description);
  configdata = configdata.replace(/\@app\.resolution/g, config.resolutions["webos"][0]+"x"+config.resolutions["webos"][1]);

  grunt.file.write(configfile, configdata);
};
