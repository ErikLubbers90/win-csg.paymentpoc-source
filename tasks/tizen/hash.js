/**
 * Spec here: http://www.crockford.com/wrmg/base32.html
 * I took the liberty of collapsing the decode of 'u' into 'v's slot, since that matches the visual funnelling of L, I, and O.
 *
 * @package default
 * @author Bryan Elliott
 **/
 module.exports = (function (parseInt, length) {
      var i,
          encodeMap = '0123456789ABCDEFGHJKMNPQRSTVWXYZ'.split(''),
          decodeMap = { L:1,I:1,O:0,U:27 },
          splitBy8 = /.{1,8}/g,
          splitBy5 = /.{1,5}/g;
      for (i in encodeMap) decodeMap[encodeMap[i]]=parseInt(i);
      function pad(str, len, right) {
          while (str[length] < len) str = (right?'':'0')+str+(right?'0':'');
          return str;
      }
      return {
          encode: function (str) {
              var i, bStr = '', ret='';
              for (i=0; i<str[length]; i++)
                  bStr+=pad(str.charCodeAt(i).toString(2), 8);
              bStr = bStr.match(splitBy5);
              for(i=0; i<bStr[length]; i++)
                  ret+=encodeMap[parseInt(pad(bStr[i], 5, 1),2)];
              return ret;
          },
          decode: function (str) {
              var i, bStr = '', bits, ret = '';
              str = str.replace(/\W|_/g, '', str);
              for (i=0; i<str[length]; i++)
                  bStr+=pad(decodeMap[str.charAt(i).toUpperCase()].toString(2),5);
              bStr = bStr.substr(0,~~(bStr[length]/8)*8).match(splitBy8);
              for (i=0; i<bStr[length]; i++)
                  ret+=String.fromCharCode(parseInt(bStr[i], 2));
              return ret;
          }
      };
})(parseInt, 'length');
