module.exports = function (grunt, platform, config) {
    grunt.log.write("platform: "+platform);

    var index, sdkData, data;
    index = "./build/"+platform+"/index.html";
    data = grunt.file.read(index);

    //insert platform sdk.inc
    sdkData = grunt.file.read("./tasks/"+platform+"/sdk.inc")
    data = data.replace("<!-- js -->", sdkData);

    grunt.file.write(index, data);

    //update config.xml
    var configxml, configdata;
    configxml = "./build/"+platform+"/config.xml";
    configdata = grunt.file.read(configxml);
    configdata = configdata.replace(/\@app\.version/g, config.version.slice(0,3) + "."+ config.build);
    configdata = configdata.replace(/\@app\.name/g, config.name);
    configdata = configdata.replace(/\@app\.appTitle/g, config.title);

    //generate unique package hash based on package name, the package name (and hash) should NOT change after 1st submission!!
    var Crock32 = require("./hash.js");
    var package_hash = Crock32.encode(config.name).slice(0, 10);
    if(package_hash.length < 10) package_hash = Crock32.encode(package_hash).slice(0, 10);
    configdata = configdata.replace(/\@app\.package_hash/g, package_hash);

    grunt.file.write(configxml, configdata);

    //replacements index.html
    var index, data;
    index = "./build/"+platform+"/index.html";
    data = grunt.file.read(index);

    //add viewport metatag
    data = data.replace("<head>", "<head>\n        <meta name=\"viewport\" content=\"width="+config.resolutions["tizen"][0]+"\" />");
    grunt.file.write(index, data);
};
