module.exports = function (grunt, platform, config) {
  grunt.log.write("platform: "+platform);
  var index, data
  var packageUrl = (config.packageUrl+"").replace("@platform", platform)
	index = "./package/"+platform+"/index.html";
  data = '<html>\n\
      <head>\n\
          <meta http-equiv="refresh" content="0;url='+packageUrl+'">\n\
          <meta name="viewport" content="width="'+config.resolutions[platform][1]+'" />\n\
      </head>\n\
      <script>location.href="'+packageUrl+'";</script>\n\
  </html>'

	grunt.file.write(index, data);

  //update config.xml
  var configxml, configdata;
  configxml = "./package/"+platform+"/config.xml";
  configdata = grunt.file.read(configxml);
  configdata = configdata.replace(/\@app\.version/g, config.version.slice(0,3) + "."+ config.build);
  configdata = configdata.replace(/\@app\.name/g, config.name);
  configdata = configdata.replace(/\@app\.appTitle/g, config.title);

  //generate unique package hash based on package name, the package name (and hash) should NOT change after 1st submission!!
  var Crock32 = require("./hash.js");
  var package_hash = Crock32.encode(config.name).slice(0, 10);
  if(package_hash.length < 10) package_hash = Crock32.encode(package_hash).slice(0, 10);
  configdata = configdata.replace(/\@app\.package_hash/g, package_hash);

  grunt.file.write(configxml, configdata);
};
