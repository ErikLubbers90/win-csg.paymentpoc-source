'use strict';

module.exports = function(grunt) {
    //custom task to run platform specific grunt script found in /task dir
    grunt.registerTask("platformPackageTask", "exec custom platform package script", function(){
      var config = grunt.config.get("config");
      var options = this.options({});
      var exists = grunt.file.exists("tasks/"+options.platform+"/package.js");
      if (exists) {
          require("./"+options.platform+"/package.js")(grunt, options.platform, config);
      }
    });
};
