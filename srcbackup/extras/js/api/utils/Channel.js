'use strict';
var Channel = require('../../vendor/jschannel.js');
var Config = require('../../config.js');

var chan;
try{
    chan = Channel.build({
        window: window.parent,
        origin: "*",
        scope: Config.context
    });
} catch(e){
    window.console.log(e);
    chan = {
        bind:function(){console.debug("Channel:bind", arguments)},
        call:function(){console.debug("Channel:call", arguments)}
    }
}
module.exports = chan;