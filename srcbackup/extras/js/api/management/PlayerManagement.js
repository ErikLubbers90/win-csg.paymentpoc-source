'use strict';

var Config = require("../../config.js");
var Events = require("../utils/Events.js");
var Player = require("./Player.js");
var Channel = require("../utils/Channel.js");


/**
 * Created by Motion Picture Laboratories, Inc. (2014)
 *
 * This work is licensed under a Creative Commons Attribution (CC BY) 3.0
 * Unported License.
 */

var PlayerManagement = (function() {

    function PlayerManagement() {
        this.playerMap = {};
        this.basePath = null;

        Channel.bind("playermgmt.getMetadata", function(trans) {
            window.CPX_Package.getMetadata(function(metadata) {
                try{
                    metadata = JSON.parse(metadata)
                }catch(error){
                    metadata = [];
                }
                trans.complete(metadata);
            });
            trans.delayReturn(true);
        });

    }

    // ================ LIFECYCLE SubGroup =========================
    /**
     * Create a Player instance that is able to play the desired media item. The
     * Framework will return a Player instance if it is able to (a) identify the
     * specified content, (b) verify that the Consumer is entitled to view the
     * content, and (c) provide a player that is compatible with the content�s
     * format and characteristics. It will then be the responsibility of the
     * Package to configure and initialize the player using the Controls, Sound,
     * and Geometry subgroups. The return value will be a StatusDescriptor. If
     * the framework is able to provide a player it will be returned to the
     * caller as the context Object of the StatusDescriptor. It is the
     * responsibility of the caller to add the player to the DOM by invoking the
     * setPlayerContainer() method on the wrapper.
     *
     * <b>NOTE</b> playerId is an API change!!
     */
    PlayerManagement.prototype.createPlayer = function(_contentId, _playerId, _useAdv) {
        console.log("create player", _contentId, _playerId, _useAdv, this.basePath);
        this.playerMap[_playerId] = new Player(_contentId, _playerId, this.basePath);

        var statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);
        statusDesc.message = "Player created for " + _contentId;
        statusDesc.context = this.playerMap[_playerId];

        return statusDesc;
    };

    /**
     * Indicates to the framework that the package has no further use for the
     * player and that clean-up and garbage collection may proceed.
     *
     *
     * <b>NOTE</b> playerId is an API change!!
     */
    PlayerManagement.prototype.destroyPlayer = function(_playerId) {
        if(this.playerMap[_playerId] != null) this.playerMap[_playerId].terminate();
        this.playerMap[_playerId] = null;
    };

    /**
     * sets a default 'base path' that the framework uses to resolve relative
     * URLs for media. This will only be used when content does not have a
     * specific source absolute URL specified in the associated manifest or in
     * the retailer's inventory database.
     *
     * <b>NOTE</b> this function is proposed API change for v0.91
     *
     * @param _path
     */
    PlayerManagement.prototype.setMediaPath = function(_path) {
        this.basePath = _path;
        window.console.log("PlayerManagement: mediaPath set to " + _path);
    };

    /**
     *
     * <b>NOTE</b> this function is proposed API change for v0.91
     */
    PlayerManagement.prototype.getContentId = function(_playerId) {
        return (this.playerMap[_playerId]._contentId);
    };

    return PlayerManagement;
})();

module.exports = PlayerManagement;