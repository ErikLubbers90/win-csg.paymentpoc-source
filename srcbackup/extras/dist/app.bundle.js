(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';
var Config = require("../config.js");
var Constants = require("./utils/Constants.js");
var PackageManagement = require('./management/PackageManagement.js');
var AccountManagement = require('./management/AccountManagement.js');
var ContentManagement = require('./management/ContentManagement.js');
var PlayerManagement = require('./management/PlayerManagement.js');

var API = (function() {
    function API() {
        this.AccountMgmt = new AccountManagement();
        this.ContentMgmt = new ContentManagement();
        this.PackageMgmt = new PackageManagement();
        this.PlayerMgmt = new PlayerManagement();

        //listener required - bug in cpe
        this.WishlistMgmt = {
            addListener : function() {},
            removeListener : function() {}
        };
    }
    return API;

})();

//make singleton
module.exports = new API();
},{"../config.js":11,"./management/AccountManagement.js":2,"./management/ContentManagement.js":3,"./management/PackageManagement.js":4,"./management/PlayerManagement.js":6,"./utils/Constants.js":8}],2:[function(require,module,exports){
'use strict';

var Config = require("../../config.js");
var Events = require("../utils/Events.js");
var Channel = require("../utils/Channel.js");
var API = require("../API.js");

/*
 * Implements the Account Access API group.
 */
var AccountManager = (function() {
    function AccountManager() {

        this.listener = new Events.Listener("Account");
        this.username = null;
        this.isLoggedIn = false;

    }

    AccountManager.prototype.signIn = function(_reqestId) {
        var statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_PENDING);
        var self = this;

        Channel.call({
            method: "account.signIn",
            success: function(resp){
                var isLoggedIn = resp[0];
                var username = resp[1];
//                console.log("[signIn success]", isLoggedIn, username);
                self.isLoggedIn = isLoggedIn;
                if(username) self.username = username;
                self.listener.notify(isLoggedIn ? ACCNT_SIGNED_IN : ACCNT_SIGNED_OUT, _reqestId);
                statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);
            },
            error: function(error, message){
                console.log("[sigIn Error] go back to previous state");
                self.listener.notify(ACCNT_SIGNED_OUT, _reqestId);
                statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);
            }
        });

        return statusDesc;
    };

    AccountManager.prototype.signOut = function(_reqestId) {
        var statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_PENDING);
        var self = this;

        Channel.call({
            method: "account.signOut",
            success: function(resp){
                var isLoggedIn = resp[0];
                self.isLoggedIn = isLoggedIn;
                self.username = null;
                self.listener.notify(isLoggedIn ? ACCNT_SIGNED_IN : ACCNT_SIGNED_OUT, _reqestId);
                statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);
            },
            error: function(error, message){
                console.log("[signOut error]", error, message)
            }
        });

        return statusDesc;
    };

    AccountManager.prototype.isSignedIn = function(_callback) {
        var statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_PENDING);
        var self = this;

        Channel.call({
            method: "account.isSignedIn",
            success: function(resp){
                var isLoggedIn = resp[0];
                var username = resp[1];
//                console.log("[isSignedIn success]", isLoggedIn, username);
                self.isLoggedIn = isLoggedIn;
                if(username) self.username = username;
                if ((_callback !== undefined) && (_callback != null)) _callback(isLoggedIn);
                statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);
            },
            error: function(error, message){
                console.log("[isSignedIn error]", error, message)
            }
        });

        return statusDesc;
    };

    /**
     * Request for information regarding the properties of a signed-in consumer
     * (i.e., screen name, age, avatar, etc.). Properties are specified in terms
     * of key-value pairs (KVP). The available properties are specific to each
     * retailer. Account properties are, for a package, immutable and read-only.
     * That is to say, only the retailers framework may include or modify the
     * account properties. * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * <h3>API Changes:</h3>
     * <ol>
     * <li>use of JSON instead of KVP</li>
     * <li>return NULL is user if not logged in</li>
     * </ol>
     */
    AccountManager.prototype.getAccountProperties = function() {
        return this.isLoggedIn ? {
            username : this.username
        } : {};
    };

    /**
     * Request for information regarding the user interface preferences of a
     * signed-in consumer (i.e., layout format, language, font size). Properties
     * are specified in terms of key-value pairs (KVP). The available properties
     * may be specific to each retailers framework. Unlike the
     * AccountProperties, the preferences are mutable. *
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * <h3>API Changes:</h3>
     * <ol>
     * <li>use of JSON instead of KVP</li>
     * <li>return retailer-specific default if user is not logged in</li>
     * </ol>
     */
    AccountManager.prototype.getPreferences = function() {
        return {
            lang : API.PackageMgmt.locale
        }
    };

    AccountManager.prototype.addListener = function(_listener) {
        this.listener.addListener(_listener);

    };

    AccountManager.prototype.removeListener = function(_listener) {
        this.listener.removeListener(_listener);
    };

    return AccountManager;
})();

module.exports = AccountManager;
},{"../../config.js":11,"../API.js":1,"../utils/Channel.js":7,"../utils/Events.js":9}],3:[function(require,module,exports){
'use strict';

var Config = require("../../config.js");
var Events = require("../utils/Events.js");
var Channel = require("../utils/Channel.js");
var API = require("../API.js");

/*
 * Implements the Content Access API group.
 */
var ContentManager = (function() {

    function ContentManager() {
        this.listener = new Events.Listener("Content");
    }

    /**
     * Checks the consumer's access rights to the specified content from the
     * CpxFramework. This may include identification of any restrictions that
     * may apply. Possible restrictions might include if it must be purchased or
     * rented prior to playback, if there is a time limit on the availability,
     * or if a limited number of playbacks are allowed. The value returned will
     * be an array containing all applicable codes.
     *
     * The function can be invoked either synchronously or asynchronously. When
     * a <tt>_requestId</tt> is provided by the caller, the function results
     * will be returned asynchronously via an Access Event notification. If the
     * <tt>_requestId</tt> is undefined or null, the results will be returned
     * synchronously as the <tt>context</tt> argument of the returned
     * <tt>StatusDescriptor</tt>
     */
    ContentManager.prototype.getAvailability = function(_contentId, _callback) {
        var rights = [];
        var statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_IN_PROGRESS);

        Channel.call({
            method: "content.getAvailability",
            parameters: _contentId,
            success: function(resp){
                var Free = resp[0];
                var Rented = resp[1];
                var Purchased = resp[2];
                var availableForRent = resp[3];
                var availableForPurchase = resp[4];

                if (Free) rights.push(ACC_AVAIL_FREE);
                if (Rented) rights.push(ACC_AVAIL_RENTED);
                if (Purchased) rights.push(ACC_AVAIL_BOUGHT);
                if (availableForRent) rights.push(ACC_AVAIL_2RENT);
                if (availableForPurchase) rights.push(ACC_AVAIL_2BUY);

                if (_callback !== undefined && (_callback != null)) _callback(_contentId, rights);
                statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);
                statusDesc.context = rights;
            },
            error: function(error, message){
                console.log("[getAvailability error]", error, message)
            }
        });

        return statusDesc;
    };

    ContentManager.prototype.acquire = function(_contentId, _requestTag) {
        var self = this;
        var statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_IN_PROGRESS);

        Channel.call({
            method: "content.acquire",
            parameters: _contentId,
            success: function (resp) {
                var isRented =  resp[0];
                var isPurchased = resp[1];
                var eventCode = [];
                if (isRented) eventCode.push(ACC_AVAIL_RENTED);
                if (isPurchased) eventCode.push(ACC_AVAIL_BOUGHT);
                self.listener.notify(_contentId, eventCode, _requestTag, CC_OP_COMPLETED);
                statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);
            },
            error: function(error, message){
                console.log("[acquire error]", error, message)
                self.listener.notify(_contentId, [], _requestTag, CC_OP_COMPLETED);
                statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);

            }

        });

        return statusDesc;
    };

    ContentManager.prototype.addListener = function(_listener) {
        this.listener.addListener(_listener);
    };

    ContentManager.prototype.removeListener = function(_listener) {
        this.listener.removeListener(_listener);
    };

    return ContentManager;
})();

module.exports = ContentManager;
},{"../../config.js":11,"../API.js":1,"../utils/Channel.js":7,"../utils/Events.js":9}],4:[function(require,module,exports){
'use strict';

var Config = require("../../config.js");
var Events = require("../utils/Events.js");
var Channel = require("../utils/Channel.js");

/**
 * Created by Motion Picture Laboratories, Inc. (2014)
 *
 * This work is licensed under a Creative Commons Attribution (CC BY) 3.0
 * Unported License.
 */
var PackageManager = (function() {
    function PackageManager() {
        this.locale = Config.defaultLang;
        this.environment = "production";
        this.isBooting = false;
        this.isTerminating = false;
        var self = this;

        Channel.bind("package.initialize", function(trans, params) {
            console.log("package.initialize", params.contentId);
            if (params.locale != null) self.locale = params.locale;
            if (params.contentId != null) self.contentId = params.contentId;
            if (params.environment != null) self.environment = params.environment;

            self.initialize();
            return;
        });
        Channel.bind("package.terminate", function(trans) {
            console.log("package.terminate");
            self.disable();
            self.terminate();
            return
        });
        Channel.bind("package.enable", function(trans) {
            console.log("package.enable");
            self.enable();
            return
        });
        Channel.bind("package.disable", function(trans) {
            console.log("package.disable");
            self.disable();
            return
        });
        Channel.bind("package.getStatus", function(trans) {
            console.log("package.getStatus");
            self.getState();
            return
        });

        Channel.bind("package.backButton", function(trans) {
            var event=document.createEvent("Events");
            event.initEvent("keydown",true,true);
            event.keyCode='8';
            event.which='8';
            document.body.dispatchEvent(event);           //manually trigger backspace event
        });

    }

    /**
     * @returns
     */
    PackageManager.prototype.getSupportedAPIs = function() {
        // supported cpe packae features (see chapter 4.2.1.1)
        return [
            GRP_PM_LC,      //Package Management - Lifecycle
            GRP_CA_AV,      //Content Access - Availability
            GRP_CA_EVT,     //Content Access - Access Event
            GRP_AA_B,       //Account Access - Basic
            GRP_AA_EVT,     //Account Access - Account Event
            GRP_PI_LC,      //Player Interaction - Lifecycle
            GRP_PI_B,       //Player Interaction - Basic
            GRP_PI_EVT,     //Player Interaction - Player Event
            GRP_PI_GEOM     //Player Interaction - Geometry
        ];
    };

    PackageManager.prototype.status = function(_statusDesc) {
        if (_statusDesc.level == LEVEL_ERROR) {
            window.console.error(_statusDesc.message);
        } else if (_statusDesc.level == LEVEL_INFO) {
            if (_statusDesc.completionCode == CC_STATE_CHANGE) {
                var curPState = this.getState();
                var context = {
                    metadata:{
                        featureContentId: "movie",
                        trailerContentId: "trailer",
                        titleContentId: this.contentId
                    }
                };
                window.console.log("STATE_CHANGE: Package " + context
                    + " is now " + P_STATE_Label[curPState]);
                switch (curPState) {
                    case P_STATE_LOADED:
                        window.CPX_Package.initialize(context,
                            window.CpxFramework, window.document
                                .getElementById(Config.packageDiv));
                        break;
                    case P_STATE_INIT:
                        this.isBooting = true;
                        break;
                    case P_STATE_AVAILABLE:
                        // when init is called (only boot) auto enable the package
                        if(this.isBooting) {
                            this.isBooting = false;
                            this.enable();
                        // when package disable is called
                        } else {
                            Channel.call({method: "package.disabled", success: function () {}, error: function () {}});
                        }
                        break;
                    case P_STATE_RUNNING:
                        Channel.call({ method: "package.enabled", success:function(){}, error:function(){}});
                        break;
                    case P_STATE_FAILED:
                        break;
                    case P_STATE_EXITING:
                        break;
                    case P_STATE_TERMINATED:
                        Channel.call({ method: "package.terminated", success:function(){}, error:function(){}});
                        break;
                }
            }
        }
    };

    PackageManager.prototype.getEnvironmentDesc = function() {
        /*
         * Contents may vary from Framework to Framework and should be specified and
         * agreed to as part of the integration process.
         */
        var envDesc = {};
        envDesc['lang'] = this.locale;
        envDesc["baseDir"] = Config.baseDir[this.environment];
        envDesc['frameworkId'] = Config.retailerID;
        envDesc['frameworkVer'] = Config.frameworkVer;
        envDesc['apiVer'] = Config.apiVer;
        return envDesc;
    };

    //
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ........ Bridge functions ......
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    PackageManager.prototype.initialize = function() {
        var env = this.getEnvironmentDesc();
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = (window.packageOverride ? window.packageOverride : env["baseDir"]+"/"+this.contentId+"/cpep-package-0_1.js");
        window.document.body.appendChild(script);
    };
    PackageManager.prototype.enable = function() {
        return window.CPX_Package.enable();
    };
    PackageManager.prototype.disable = function() {
        return window.CPX_Package.disable();
    };
    PackageManager.prototype.terminate = function() {
        this.isTerminating = true;
        return window.CPX_Package.terminate();
    };
    PackageManager.prototype.getState = function() {
        return window.CPX_Package.getState();
    };

    return PackageManager;
})();

module.exports = PackageManager;
},{"../../config.js":11,"../utils/Channel.js":7,"../utils/Events.js":9}],5:[function(require,module,exports){
'use strict';

var Config = require("../../config.js");
var Events = require("../utils/Events.js");
var Channel = require("../utils/Channel.js");

var Player = (function() {

    function Player(_contentId, _playerId, _basePath) {
        this._playerId = _playerId;
        this._basePath = _basePath;
        this._contentId = _contentId;
        this.listener = new Events.Listener("Player-"+_playerId);
        this.currentTime = 0;
        this.x = 0;
        this.y = 0;
        this.width = 1920;
        this.heigth = 1080;
        this.fullscreen = true;
        this.isPlaying = false;
        var self = this;
        Channel.call({
            method:"player.initialise",
            params: [self._playerId, self._contentId],
            success: function(resp) {
                console.log("Player.initialize successful");
            },
            error: function(error, message){
                console.log("Player.initialize error", error);
            }
        });
        Channel.bind("player."+self._playerId+".onProgress", function(trans, currentTime) {
            self.currentTime = currentTime;
        });
        Channel.bind("player." + self._playerId + ".getMetadata", function(trans) {
            window.CPX_Package.getMetadata(function(metadata) {
                try{
                    metadata = JSON.parse(metadata)
                }catch(error){
                    metadata = [];
                }
                trans.complete(metadata);
            });
            trans.delayReturn(true);
        });
        Channel.bind("player."+self._playerId+".onDisplayChange", function(trans, x, y, width, heigth) {
            self.x = x;
            self.y = y;
            self.width = width;
            self.heigth = heigth;
        });
        Channel.bind("player."+self._playerId+".onPlayStateChange", function(trans, isPlaying) {
            self.isPlaying = isPlaying;
        });
        Channel.bind("player."+_playerId+".onFullScreen", function(trans, fullscreen) {
            self.fullscreen = fullscreen;
        });
    }

    // ================ BASIC SubGroup =========================
    /**
     * Start playback using currently specified rate and direction.
     */
    Player.prototype.play = function() {
        Channel.call({
            method:"player.play",
            success: function(resp) {
                console.log("Player.play successful");
            },
            error: function(error, message){
                console.log("Player.play error", error);
            }
        });
        return RESP_OK;
    };

    /**
     * Restart or pause playback depending on current state. If restarting,
     * playback will resume at last location using currently specified rate and
     * direction.
     */
    Player.prototype.togglePause = function() {
        Channel.call({
            method:"player.play_pause",
            success: function(resp) {
                console.log("Player.play_pause successful");
            },
            error: function(error, message){
                console.log("Player.play_pause error", error);
            }
        });
        return RESP_OK;
    };

    /**
     */
    Player.prototype.setPaused = function(pause) {
        Channel.call({
            method:"player.pause",
            success: function(resp) {
                console.log("Player.pause successful");
            },
            error: function(error, message){
                console.log("Player.pause error", error);
            }
        });

        return RESP_OK;
    };

    /**
     */
    Player.prototype.isPaused = function() {
        return self.isPlaying;
    };

    /**
     * Stops playback and resets to the media stream to its initial state.
     */
    Player.prototype.stop = function() {
        Channel.call({
            method:"player.stop",
            success: function(resp) {
                console.log("Player.stop successful");
            },
            error: function(error, message){
                console.log("Player.stop error", error);
            }
        });
        return RESP_OK;
    };

    // ================ Geometry SubGroup ============================

    Player.prototype.setPlayerContainer = function(container, successCallback, errorCallback) {
        console.log("setPlayerContainer", container, successCallback, errorCallback);
    };

    Player.prototype.setFullScreen = function(enable) {
        //Player is always fullscreen
        if(enable){
            return RESP_OK;
        }else{
            return RESP_FAILED;
        }
    };

    Player.prototype.isFullScreen = function() {
        return self.fullscreen;
    };

    /**
     */
    Player.prototype.getPlayerGeometry = function() {
        var geometry            = {};
        geometry.playerHeight   = this.heigth;
        geometry.playerWidth    = this.width;
        geometry.videoHeight    = this.heigth;
        geometry.videoWidth     = this.width;
        geometry.videoOffsetX   = this.x;
        geometry.videoOffsetY   = this.y;
        geometry.safeAreaHeight = Math.round(this.heigth*-0.9);
        geometry.safeAreaWidth  = Math.round(this.width*-0.9);
        geometry.safeAreaOffsetX = Math.round(this.width*-0.1/2);
        geometry.safeAreaOffsetY = Math.round(this.heigth*-0.1/2);
        return geometry;
    };

    Player.prototype.setVisibleControls = function(show) {
        Channel.call({
            method:"player.setVisibleControls",
            params: [show],
            success: function(resp) {
                console.log("Player.setVisibleControls successful");
            },
            error: function(error, message){
                console.log("Player.setVisibleControls error", error);
            }
        });
    };

    /*
     * returns the current playback time, in seconds.
     */
    Player.prototype.getCurrentTime = function() {
        return this.currentTime;
    };

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Player.prototype.jumpTo = function(position){
        /*
         * ISSUE: is the <video> element (i.e., 'player') in a readyState that
         * allows the setting of the currentTime property?
         */
        Channel.call({
            method:"player.jumpTo",
            params: [position],
            success: function(resp) {
                console.log("Player.jumpTo successful");
            },
            error: function(error, message){
                console.log("Player.jumpTo error", error);
            }
        });
        return RESP_OK;
    };

    Player.prototype.addListener = function(listener) {
        this.listener.addListener(_listener);
    };

    Player.prototype.removeListener = function(listener) {
        this.listener.removeListener(_listener);
    };

    Player.prototype.terminate = function(listener) {
        Channel.call({ method:"player."+self._playerId+".terminate", success:function(){}, error:function(){} })
    };
    return Player;
})();

module.exports = Player;
},{"../../config.js":11,"../utils/Channel.js":7,"../utils/Events.js":9}],6:[function(require,module,exports){
'use strict';

var Config = require("../../config.js");
var Events = require("../utils/Events.js");
var Player = require("./Player.js");
var Channel = require("../utils/Channel.js");


/**
 * Created by Motion Picture Laboratories, Inc. (2014)
 *
 * This work is licensed under a Creative Commons Attribution (CC BY) 3.0
 * Unported License.
 */

var PlayerManagement = (function() {

    function PlayerManagement() {
        this.playerMap = {};
        this.basePath = null;

        Channel.bind("playermgmt.getMetadata", function(trans) {
            window.CPX_Package.getMetadata(function(metadata) {
                try{
                    metadata = JSON.parse(metadata)
                }catch(error){
                    metadata = [];
                }
                trans.complete(metadata);
            });
            trans.delayReturn(true);
        });

    }

    // ================ LIFECYCLE SubGroup =========================
    /**
     * Create a Player instance that is able to play the desired media item. The
     * Framework will return a Player instance if it is able to (a) identify the
     * specified content, (b) verify that the Consumer is entitled to view the
     * content, and (c) provide a player that is compatible with the content�s
     * format and characteristics. It will then be the responsibility of the
     * Package to configure and initialize the player using the Controls, Sound,
     * and Geometry subgroups. The return value will be a StatusDescriptor. If
     * the framework is able to provide a player it will be returned to the
     * caller as the context Object of the StatusDescriptor. It is the
     * responsibility of the caller to add the player to the DOM by invoking the
     * setPlayerContainer() method on the wrapper.
     *
     * <b>NOTE</b> playerId is an API change!!
     */
    PlayerManagement.prototype.createPlayer = function(_contentId, _playerId, _useAdv) {
        console.log("create player", _contentId, _playerId, _useAdv, this.basePath);
        this.playerMap[_playerId] = new Player(_contentId, _playerId, this.basePath);

        var statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);
        statusDesc.message = "Player created for " + _contentId;
        statusDesc.context = this.playerMap[_playerId];

        return statusDesc;
    };

    /**
     * Indicates to the framework that the package has no further use for the
     * player and that clean-up and garbage collection may proceed.
     *
     *
     * <b>NOTE</b> playerId is an API change!!
     */
    PlayerManagement.prototype.destroyPlayer = function(_playerId) {
        if(this.playerMap[_playerId] != null) this.playerMap[_playerId].terminate();
        this.playerMap[_playerId] = null;
    };

    /**
     * sets a default 'base path' that the framework uses to resolve relative
     * URLs for media. This will only be used when content does not have a
     * specific source absolute URL specified in the associated manifest or in
     * the retailer's inventory database.
     *
     * <b>NOTE</b> this function is proposed API change for v0.91
     *
     * @param _path
     */
    PlayerManagement.prototype.setMediaPath = function(_path) {
        this.basePath = _path;
        window.console.log("PlayerManagement: mediaPath set to " + _path);
    };

    /**
     *
     * <b>NOTE</b> this function is proposed API change for v0.91
     */
    PlayerManagement.prototype.getContentId = function(_playerId) {
        return (this.playerMap[_playerId]._contentId);
    };

    return PlayerManagement;
})();

module.exports = PlayerManagement;
},{"../../config.js":11,"../utils/Channel.js":7,"../utils/Events.js":9,"./Player.js":5}],7:[function(require,module,exports){
'use strict';
var Channel = require('../../vendor/jschannel.js');
var Config = require('../../config.js');

var chan;
try{
    chan = Channel.build({
        window: window.parent,
        origin: "*",
        scope: Config.context
    });
} catch(e){
    window.console.log(e);
    chan = {
        bind:function(){console.debug("Channel:bind", arguments)},
        call:function(){console.debug("Channel:call", arguments)}
    }
}
module.exports = chan;
},{"../../config.js":11,"../../vendor/jschannel.js":12}],8:[function(require,module,exports){
"use strict";
/**
 * Created by Motion Picture Laboratories, Inc. (2014)
 *
 * This work is licensed under a Creative Commons Attribution (CC BY) 3.0
 * Unported License.
 */

/*
 * API Version: 0.90 Last Modified: 2014-06-28
 */

// Constants used to identify API subgroups:
// See Sec 4.2.1 of API Document
window.GRP_PM_LC = 101;
window.GRP_PM_CONN = 102;
window.GRP_PM_ENV = 103;
window.GRP_CA_AV = 104;
window.GRP_CA_EVT = 105;
window.GRP_CA_DLD = 106;
window.GRP_AA_B = 107;
window.GRP_AA_EVT = 108;
window.GRP_PI_LC = 109;
window.GRP_PI_B = 110;
window.GRP_PI_TP = 111;
window.GRP_PI_CTRL = 112;
window.GRP_PI_SND = 113;
window.GRP_PI_EVT = 114;
window.GRP_PI_GEOM = 115;
window.GRP_SN_SHARE = 116;
window.GRP_SN_EVT = 117;
window.GRP_EN_WSH = 118;
window.GRP_EN_BKM = 119;
window.GRP_EN_PH = 120;

// Constants used in StatusDescriptor:
// See Sec 4.4.2 of API Document
window.CC_OP_CANCELLED = 201;
window.CC_OP_COMPLETED = 202;
window.CC_OP_FAILED = 203;
window.CC_OP_PENDING = 204;
window.CC_OP_IN_PROGRESS = 205;
window.CC_STATE_CHANGE = 206;
window.CC_INVALID_PARAM = 207;
window.CC_UNSUPPORTED = 208;

window.LEVEL_INFO = 211;
window.LEVEL_WARNING = 212;
window.LEVEL_ERROR = 212;
window.LEVEL_FATAL = 213;

// Data structure
window.StatusDescriptor = function(level, code) {
    this.level = level;
    this.completionCode = code;
    this.message = null;
    this.context = null;
};

// Constants used in ConnectivityState:
// See Sec 4.4.1 of API Document
window.NET_CAPABILITY_LOW = 301;
window.NET_CAPABILITY_MEDIUM = 302;
window.NET_CAPABILITY_HIGH = 303;
window.NET_CAPABILITY_UNKNOWN = 304;

window.NET_STATE_CONNECTED = 311;
window.NET_STATE_CONNECTING = 312;
window.NET_STATE_DISCONNECTED = 313;
window.NET_STATE_UNKNOWN = 314;

window.NET_TYPE_BLUETOOTH = 321;
window.NET_TYPE_WIRE = 322;
window.NET_TYPE_MOBILE = 323;
window.NET_TYPE_WIFI = 324;
window.NET_TYPE_WIMAX = 325;
window.NET_TYPE_NONE = 326;
window.NET_TYPE_UNKNOWN = 327;

window.P_STATE_LOADED = 401; // synonym for STARTED added in API v0.90
window.P_STATE_STARTED = 401;
window.P_STATE_INIT = 402;
window.P_STATE_AVAILABLE = 403;
window.P_STATE_RUNNING = 404;
window.P_STATE_FAILED = 405;
window.P_STATE_EXITING = 406; // New state added in API v0.90
window.P_STATE_TERMINATED = 407;

// Section 5.1 Content Access Codes
/*
 * These codes are used when inquiries are made as to the accessibility of a
 * content item or when an event is associated with some change in
 * accessibility.
 */
/*
 * IMPORTANT: The API specifies the names but not the values.
 */

window.ACC_AVAIL_2BUY = 5001;
window.ACC_AVAIL_2RENT = 5002;
window.ACC_AVAIL_FREE = 5003;
window.ACC_AVAIL_BOUGHT = 5004;
window.ACC_AVAIL_RENTED = 5005;
window.ACC_AVAIL_PENDING = 5006;
window.ACC_AVAIL_UNAVAIL = 5007;
window.ACC_AVAIL_UNK = 5008;
window.ACC_CONTENT_UNK = 5009;

// Section 6.2.1.1 Account Access Codes

window.ACCNT_SIGNED_IN = 6001; // New code added in API v0.90
window.ACCNT_SIGNED_OUT = 6002;
window.ACCNT_PREF_CHANGE = 6003; // New code added in API v0.93

// Generic Response Codes:
// See Sec 7.2.1.1 of API Document
window.RESP_OK = 7101;
window.RESP_INVALID_STATE = 7102;
window.RESP_UNSUPPORTED_CMD = 7103;
window.RESP_UNSUPPORTED_OPT = 7104;
window.RESP_FAILED = 7105;

// Player Event Codes:
// See Sec 7.2.3.1 or 7.3.1.1 of API Document
window.PE_READY = 7200; // added in API v0.90
window.PE_PLAYING = 7201;
window.PE_PAUSED = 7202;
window.PE_STOPPED = 7203;
window.PE_SUSPENDED = 7204;
window.PE_RESUMED = 7205;
window.PE_ERROR = 7206;
window.PE_MODE_CHANGE = 7207;
// window.PE_CTRLBAR_CHANGE = 7209; // DELETED in API v0.93
window.PE_MUTE_CHANGE = 7210;
window.PE_REPOSITION_START = 7211;
window.PE_REPOSITION_END = 7212;
window.PE_TIME_EVT_P = 7213;
window.PE_TIME_EVT_R = 7214;
window.PE_VISIBILITY = 7215; // added in API v0.90
window.PE_CTRLBAR_HIDDEN = 7216; // added in API v0.93
window.PE_CTRLBAR_VISIBLE = 7217; // added in API v0.93

// See Sec 7.2.9.1 of API Document
window.PE_GEOMETRY_CHANGE = 7301;

// see Sec 7.2.4.2.1
window.RATE_QUARTER = 7401;
window.RATE_HALF = 7402;
window.RATE_NORMAL = 7403;
window.RATE_DOUBLE = 7404;
window.RATE_TRIPLE = 7405;
window.RATE_SLOWEST = 7406;
window.RATE_FASTEST = 7407;

window.LE_ADDED = 9201; // added in API v0.93
window.LE_DELETED = 9202; // added in API v0.93

/*
 * To support logging.... Text labels and descriptions for each event code are
 * NOT considered part of the API specification. text used here is intended for
 * display to developers. text intended for display to end-users should be
 * defined in the locale-specific files (e.g.
 * './scripts/locale/english/EventText.js')
 */
window.CC_Label = {};
window.CC_Label[CC_OP_CANCELLED] = "CANCELLED";
window.CC_Label[CC_OP_COMPLETED] = "COMPLETED";
window.CC_Label[CC_OP_FAILED] = "FAILED";
window.CC_Label[CC_OP_PENDING] = "PENDING";
window.CC_Label[CC_OP_IN_PROGRESS] = "IN PROGRESS";
window.CC_Label[CC_STATE_CHANGE] = "STATE CHANGE";
window.CC_Label[CC_INVALID_PARAM] = "INVALID PARAMETER";
window.CC_Label[CC_UNSUPPORTED] = "UNSUPPORTED";

window.P_STATE_Label = {};
window.P_STATE_Label[P_STATE_LOADED] = "LOADED";
window.P_STATE_Label[P_STATE_INIT] = "INITIALIZED";
window.P_STATE_Label[P_STATE_AVAILABLE] = "AVAILABLE";
window.P_STATE_Label[P_STATE_RUNNING] = "RUNNING";
window.P_STATE_Label[P_STATE_FAILED] = "FAILED";
window.P_STATE_Label[P_STATE_EXITING] = "EXITING";
window.P_STATE_Label[P_STATE_TERMINATED] = "TERMINATED";

window.PE_EventLabel = {};

window.PE_EventLabel[7200] = "READY"; /* added to API */
window.PE_EventLabel[7201] = "PLAYING";
window.PE_EventLabel[7202] = "PAUSED";
window.PE_EventLabel[7203] = "STOPPED";
window.PE_EventLabel[7204] = "SUSPENDED";
window.PE_EventLabel[7205] = "RESUMED";
window.PE_EventLabel[7206] = "ERROR";
window.PE_EventLabel[7207] = "MODE_CHANGE";
window.PE_EventLabel[7208] = "GEOMETRY_CHANGE";
window.PE_EventLabel[7209] = "CTRLBAR_CHANGE";
window.PE_EventLabel[7210] = "MUTE_CHANGE";
window.PE_EventLabel[7211] = "REPOSITION_START";
window.PE_EventLabel[7212] = "REPOSITION_END";
window.PE_EventLabel[7213] = "TIME_EVT_P";
window.PE_EventLabel[7214] = "TIME_EVT_R";
window.PE_EventLabel[7215] = "VISIBILITY_CHANGE"; /* added to API */

window.PE_EventLabel[7216] = "CTRLBAR_HIDDEN"; /* added to API v0.93 */
window.PE_EventLabel[7217] = "CTRLBAR_VISIBLE"; /* added to API  v0.93 */

window.ACC_EventLabel = {};
window.ACC_EventLabel[5001] = "Available to buy";
window.ACC_EventLabel[5002] = "Available to rent";
window.ACC_EventLabel[5003] = "Free (no purchase required)";
window.ACC_EventLabel[5004] = "Has been purchased";
window.ACC_EventLabel[5005] = "Rented and available for viewing";
window.ACC_EventLabel[5006] = "Availability pending..";
window.ACC_EventLabel[5007] = "No available";
window.ACC_EventLabel[5008] = "Availability unknown";
window.ACC_EventLabel[5009] = "The specified content ID is not recognized";
},{}],9:[function(require,module,exports){
'use strict';

/*
 * Implements the Content Access API group.
 */
var Listener = (function() {
	/**
	 * Construct a new Listener and identify it with the specified API group.
	 *
	 * @param group
	 * @returns ListenerMgr
	 */
	function Listener(group) {
		this.group = group;
		this.listeners = [];
	}

	// prototype values are static defaults. The constructor MUST create it's own
	// instance-specific property
	Listener.prototype.group = '';
	Listener.prototype.listeners = [];

	// Methods required by API...

	/**
	 * Adds the listener to the listener list. The listener is registered for all
	 * event notifications associated with this interface.
	 *
	 * @param listener
	 * @returns true if the listener was added successfully
	 */
	Listener.prototype.addListener = function(listener) {
		// check for duplicates
		if (this.listeners.indexOf(listener) < 1) {
			this.listeners.push(listener);
			return true;
		} else {
			return false;
		}
	};

	/**
	 * Removes the listener from the listener list.
	 *
	 * @param listener
	 */
	Listener.prototype.removeListener = function(listener) {
		var loc = this.listeners.indexOf(listener);
		if (loc >= 0) {
			this.listeners.splice(loc, 1);
		}
	};

	// Methods NOT required by API....

	/**
	 *
	 * @param eventCode
	 */
	Listener.prototype.notify = function() {
		var cnt;
		for (cnt = 0; cnt < this.listeners.length; cnt++) {
			this.listeners[cnt].eventNotification.apply(this.listeners[cnt].eventNotification, arguments);
		}
	};

	Listener.prototype.countListener = function() {
		window.console.log(this.group + " has " + this.listeners.length);
	};

	return Listener;

})();

module.exports = {
	Listener:Listener
};
},{}],10:[function(require,module,exports){
'use strict';

//add prefix to console.log to identify extra logs
var orginal_log = console.log;
console.log = function(){
    var mainArguments = Array.prototype.slice.call(arguments);
    orginal_log.apply(console, ["EXTRA:"].concat(mainArguments));
};

//load API
var API = window.CpxFramework = require('./api/API.js');

},{"./api/API.js":1}],11:[function(require,module,exports){
module.exports = {
    baseDir: {
        "production": "http://movietouch.sony.com.edgesuite.net/UltraSPS/Packages",
        "development": "http://r60-cpe.s3-website-us-east-1.amazonaws.com/hosting/radius60/cpx/dev/bravia/packages"
    },
    packageDiv: "packDiv1",
    defaultLang: "en",
    retailerID: "SONY:SPS:ULTRA",
    frameworkVer: "0.10.a",
    apiVer: "0.93",
    context: "sps-ultra"
};
},{}],12:[function(require,module,exports){
/*
 * js_channel is a very lightweight abstraction on top of
 * postMessage which defines message formats and semantics
 * to support interactions more rich than just message passing
 * js_channel supports:
 *  + query/response - traditional rpc
 *  + query/update/response - incremental async return of results
 *    to a query
 *  + notifications - fire and forget
 *  + error handling
 *
 * js_channel is based heavily on json-rpc, but is focused at the
 * problem of inter-iframe RPC.
 *
 * Message types:
 *  There are 5 types of messages that can flow over this channel,
 *  and you may determine what type of message an object is by
 *  examining its parameters:
 *  1. Requests
 *    + integer id
 *    + string method
 *    + (optional) any params
 *  2. Callback Invocations (or just "Callbacks")
 *    + integer id
 *    + string callback
 *    + (optional) params
 *  3. Error Responses (or just "Errors)
 *    + integer id
 *    + string error
 *    + (optional) string message
 *  4. Responses
 *    + integer id
 *    + (optional) any result
 *  5. Notifications
 *    + string method
 *    + (optional) any params
 */

;var Channel = (function() {
    "use strict";

    // current transaction id, start out at a random *odd* number between 1 and a million
    // There is one current transaction counter id per page, and it's shared between
    // channel instances.  That means of all messages posted from a single javascript
    // evaluation context, we'll never have two with the same id.
    var s_curTranId = Math.floor(Math.random()*1000001);

    // no two bound channels in the same javascript evaluation context may have the same origin, scope, and window.
    // futher if two bound channels have the same window and scope, they may not have *overlapping* origins
    // (either one or both support '*').  This restriction allows a single onMessage handler to efficiently
    // route messages based on origin and scope.  The s_boundChans maps origins to scopes, to message
    // handlers.  Request and Notification messages are routed using this table.
    // Finally, channels are inserted into this table when built, and removed when destroyed.
    var s_boundChans = { };

    // add a channel to s_boundChans, throwing if a dup exists
    function s_addBoundChan(win, origin, scope, handler) {
        function hasWin(arr) {
            for (var i = 0; i < arr.length; i++) if (arr[i].win === win) return true;
            return false;
        }

        // does she exist?
        var exists = false;


        if (origin === '*') {
            // we must check all other origins, sadly.
            for (var k in s_boundChans) {
                if (!s_boundChans.hasOwnProperty(k)) continue;
                if (k === '*') continue;
                if (typeof s_boundChans[k][scope] === 'object') {
                    exists = hasWin(s_boundChans[k][scope]);
                    if (exists) break;
                }
            }
        } else {
            // we must check only '*'
            if ((s_boundChans['*'] && s_boundChans['*'][scope])) {
                exists = hasWin(s_boundChans['*'][scope]);
            }
            if (!exists && s_boundChans[origin] && s_boundChans[origin][scope])
            {
                exists = hasWin(s_boundChans[origin][scope]);
            }
        }
        if (exists) throw "A channel is already bound to the same window which overlaps with origin '"+ origin +"' and has scope '"+scope+"'";

        if (typeof s_boundChans[origin] != 'object') s_boundChans[origin] = { };
        if (typeof s_boundChans[origin][scope] != 'object') s_boundChans[origin][scope] = [ ];
        s_boundChans[origin][scope].push({win: win, handler: handler});
    }

    function s_removeBoundChan(win, origin, scope) {
        var arr = s_boundChans[origin][scope];
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].win === win) {
                arr.splice(i,1);
            }
        }
        if (s_boundChans[origin][scope].length === 0) {
            delete s_boundChans[origin][scope];
        }
    }

    function s_isArray(obj) {
        if (Array.isArray) return Array.isArray(obj);
        else {
            return (obj.constructor.toString().indexOf("Array") != -1);
        }
    }

    // No two outstanding outbound messages may have the same id, period.  Given that, a single table
    // mapping "transaction ids" to message handlers, allows efficient routing of Callback, Error, and
    // Response messages.  Entries are added to this table when requests are sent, and removed when
    // responses are received.
    var s_transIds = { };

    // class singleton onMessage handler
    // this function is registered once and all incoming messages route through here.  This
    // arrangement allows certain efficiencies, message data is only parsed once and dispatch
    // is more efficient, especially for large numbers of simultaneous channels.
    var s_onMessage = function(e) {
        try {
            var m = JSON.parse(e.data);
            if (typeof m !== 'object' || m === null) throw "malformed";
        } catch(e) {
            // just ignore any posted messages that do not consist of valid JSON
            return;
        }

        var w = e.source;
        var o = e.origin;
        var s, i, meth;

        if (typeof m.method === 'string') {
            var ar = m.method.split('::');
            if (ar.length == 2) {
                s = ar[0];
                meth = ar[1];
            } else {
                meth = m.method;
            }
        }

        if (typeof m.id !== 'undefined') i = m.id;

        // w is message source window
        // o is message origin
        // m is parsed message
        // s is message scope
        // i is message id (or undefined)
        // meth is unscoped method name
        // ^^ based on these factors we can route the message

        // if it has a method it's either a notification or a request,
        // route using s_boundChans
        if (typeof meth === 'string') {
            var delivered = false;
            if (s_boundChans[o] && s_boundChans[o][s]) {
                for (var j = 0; j < s_boundChans[o][s].length; j++) {
                    if (s_boundChans[o][s][j].win === w) {
                        s_boundChans[o][s][j].handler(o, meth, m);
                        delivered = true;
                        break;
                    }
                }
            }

            if (!delivered && s_boundChans['*'] && s_boundChans['*'][s]) {
                for (var j = 0; j < s_boundChans['*'][s].length; j++) {
                    if (s_boundChans['*'][s][j].win === w) {
                        s_boundChans['*'][s][j].handler(o, meth, m);
                        break;
                    }
                }
            }
        }
        // otherwise it must have an id (or be poorly formed
        else if (typeof i != 'undefined') {
            if (s_transIds[i]) s_transIds[i](o, meth, m);
        }
    };

    // Setup postMessage event listeners
    if (window.addEventListener) window.addEventListener('message', s_onMessage, false);
    else if(window.attachEvent) window.attachEvent('onmessage', s_onMessage);

    /* a messaging channel is constructed from a window and an origin.
     * the channel will assert that all messages received over the
     * channel match the origin
     *
     * Arguments to Channel.build(cfg):
     *
     *   cfg.window - the remote window with which we'll communicate
     *   cfg.origin - the expected origin of the remote window, may be '*'
     *                which matches any origin
     *   cfg.scope  - the 'scope' of messages.  a scope string that is
     *                prepended to message names.  local and remote endpoints
     *                of a single channel must agree upon scope. Scope may
     *                not contain double colons ('::').
     *   cfg.debugOutput - A boolean value.  If true and window.console.log is
     *                a function, then debug strings will be emitted to that
     *                function.
     *   cfg.debugOutput - A boolean value.  If true and window.console.log is
     *                a function, then debug strings will be emitted to that
     *                function.
     *   cfg.postMessageObserver - A function that will be passed two arguments,
     *                an origin and a message.  It will be passed these immediately
     *                before messages are posted.
     *   cfg.gotMessageObserver - A function that will be passed two arguments,
     *                an origin and a message.  It will be passed these arguments
     *                immediately after they pass scope and origin checks, but before
     *                they are processed.
     *   cfg.onReady - A function that will be invoked when a channel becomes "ready",
     *                this occurs once both sides of the channel have been
     *                instantiated and an application level handshake is exchanged.
     *                the onReady function will be passed a single argument which is
     *                the channel object that was returned from build().
     */
    return {
        build: function(cfg) {
            var debug = function(m) {
                if (cfg.debugOutput && window.console && window.console.log) {
                    // try to stringify, if it doesn't work we'll let javascript's built in toString do its magic
                    try { if (typeof m !== 'string') m = JSON.stringify(m); } catch(e) { }
                    console.log("["+chanId+"] " + m);
                }
            };

            /* browser capabilities check */
            if (!window.postMessage) throw("jschannel cannot run this browser, no postMessage");
            if (!window.JSON || !window.JSON.stringify || ! window.JSON.parse) {
                throw("jschannel cannot run this browser, no JSON parsing/serialization");
            }

            /* basic argument validation */
            if (typeof cfg != 'object') throw("Channel build invoked without a proper object argument");

            if (!cfg.window || !cfg.window.postMessage) throw("Channel.build() called without a valid window argument");

            /* we'd have to do a little more work to be able to run multiple channels that intercommunicate the same
             * window...  Not sure if we care to support that */
            if (window === cfg.window) throw("target window is same as present window -- not allowed");

            // let's require that the client specify an origin.  if we just assume '*' we'll be
            // propagating unsafe practices.  that would be lame.
            var validOrigin = false;
            if (typeof cfg.origin === 'string') {
                var oMatch;
                if (cfg.origin === "*") validOrigin = true;
                // allow valid domains under http and https.  Also, trim paths off otherwise valid origins.
                else if (null !== (oMatch = cfg.origin.match(/^https?:\/\/(?:[-a-zA-Z0-9_\.])+(?::\d+)?/))) {
                    cfg.origin = oMatch[0].toLowerCase();
                    validOrigin = true;
                }
            }

            if (!validOrigin) throw ("Channel.build() called with an invalid origin");

            if (typeof cfg.scope !== 'undefined') {
                if (typeof cfg.scope !== 'string') throw 'scope, when specified, must be a string';
                if (cfg.scope.split('::').length > 1) throw "scope may not contain double colons: '::'";
            }

            /* private variables */
            // generate a random and psuedo unique id for this channel
            var chanId = (function () {
                var text = "";
                var alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                for(var i=0; i < 5; i++) text += alpha.charAt(Math.floor(Math.random() * alpha.length));
                return text;
            })();

            // registrations: mapping method names to call objects
            var regTbl = { };
            // current oustanding sent requests
            var outTbl = { };
            // current oustanding received requests
            var inTbl = { };
            // are we ready yet?  when false we will block outbound messages.
            var ready = false;
            var pendingQueue = [ ];

            var createTransaction = function(id,origin,callbacks) {
                var shouldDelayReturn = false;
                var completed = false;

                return {
                    origin: origin,
                    invoke: function(cbName, v) {
                        // verify in table
                        if (!inTbl[id]) throw "attempting to invoke a callback of a nonexistent transaction: " + id;
                        // verify that the callback name is valid
                        var valid = false;
                        for (var i = 0; i < callbacks.length; i++) if (cbName === callbacks[i]) { valid = true; break; }
                        if (!valid) throw "request supports no such callback '" + cbName + "'";

                        // send callback invocation
                        postMessage({ id: id, callback: cbName, params: v});
                    },
                    error: function(error, message) {
                        completed = true;
                        // verify in table
                        if (!inTbl[id]) throw "error called for nonexistent message: " + id;

                        // remove transaction from table
                        delete inTbl[id];

                        // send error
                        postMessage({ id: id, error: error, message: message });
                    },
                    complete: function(v) {
                        completed = true;
                        // verify in table
                        if (!inTbl[id]) throw "complete called for nonexistent message: " + id;
                        // remove transaction from table
                        delete inTbl[id];
                        // send complete
                        postMessage({ id: id, result: v });
                    },
                    delayReturn: function(delay) {
                        if (typeof delay === 'boolean') {
                            shouldDelayReturn = (delay === true);
                        }
                        return shouldDelayReturn;
                    },
                    completed: function() {
                        return completed;
                    }
                };
            };

            var setTransactionTimeout = function(transId, timeout, method) {
                return window.setTimeout(function() {
                    if (outTbl[transId]) {
                        // XXX: what if client code raises an exception here?
                        var msg = "timeout (" + timeout + "ms) exceeded on method '" + method + "'";
                        (1,outTbl[transId].error)("timeout_error", msg);
                        delete outTbl[transId];
                        delete s_transIds[transId];
                    }
                }, timeout);
            };

            var onMessage = function(origin, method, m) {
                // if an observer was specified at allocation time, invoke it
                if (typeof cfg.gotMessageObserver === 'function') {
                    // pass observer a clone of the object so that our
                    // manipulations are not visible (i.e. method unscoping).
                    // This is not particularly efficient, but then we expect
                    // that message observers are primarily for debugging anyway.
                    try {
                        cfg.gotMessageObserver(origin, m);
                    } catch (e) {
                        debug("gotMessageObserver() raised an exception: " + e.toString());
                    }
                }

                // now, what type of message is this?
                if (m.id && method) {
                    // a request!  do we have a registered handler for this request?
                    if (regTbl[method]) {
                        var trans = createTransaction(m.id, origin, m.callbacks ? m.callbacks : [ ]);
                        inTbl[m.id] = { };
                        try {
                            // callback handling.  we'll magically create functions inside the parameter list for each
                            // callback
                            if (m.callbacks && s_isArray(m.callbacks) && m.callbacks.length > 0) {
                                for (var i = 0; i < m.callbacks.length; i++) {
                                    var path = m.callbacks[i];
                                    var obj = m.params;
                                    var pathItems = path.split('/');
                                    for (var j = 0; j < pathItems.length - 1; j++) {
                                        var cp = pathItems[j];
                                        if (typeof obj[cp] !== 'object') obj[cp] = { };
                                        obj = obj[cp];
                                    }
                                    obj[pathItems[pathItems.length - 1]] = (function() {
                                        var cbName = path;
                                        return function(params) {
                                            return trans.invoke(cbName, params);
                                        };
                                    })();
                                }
                            }
                            var resp = regTbl[method](trans, m.params);
                            if (!trans.delayReturn() && !trans.completed()) trans.complete(resp);
                        } catch(e) {
                            console.error(e);
                            // automagic handling of exceptions:
                            var error = "runtime_error";
                            var message = null;
                            // * if it's a string then it gets an error code of 'runtime_error' and string is the message
                            if (typeof e === 'string') {
                                message = e;
                            } else if (typeof e === 'object') {
                                // either an array or an object
                                // * if it's an array of length two, then  array[0] is the code, array[1] is the error message
                                if (e && s_isArray(e) && e.length == 2) {
                                    error = e[0];
                                    message = e[1];
                                }
                                // * if it's an object then we'll look form error and message parameters
                                else if (typeof e.error === 'string') {
                                    error = e.error;
                                    if (!e.message) message = "";
                                    else if (typeof e.message === 'string') message = e.message;
                                    else e = e.message; // let the stringify/toString message give us a reasonable verbose error string
                                }
                            }

                            // message is *still* null, let's try harder
                            if (message === null) {
                                try {
                                    message = JSON.stringify(e);
                                    /* On MSIE8, this can result in 'out of memory', which
                                     * leaves message undefined. */
                                    if (typeof(message) == 'undefined')
                                        message = e.toString();
                                } catch (e2) {
                                    message = e.toString();
                                }
                            }

                            trans.error(error,message);
                        }
                    }
                } else if (m.id && m.callback) {
                    if (!outTbl[m.id] ||!outTbl[m.id].callbacks || !outTbl[m.id].callbacks[m.callback])
                    {
                        debug("ignoring invalid callback, id:"+m.id+ " (" + m.callback +")");
                    } else {
                        // XXX: what if client code raises an exception here?
                        outTbl[m.id].callbacks[m.callback](m.params);
                    }
                } else if (m.id) {
                    if (!outTbl[m.id]) {
                        debug("ignoring invalid response: " + m.id);
                    } else {
                        // XXX: what if client code raises an exception here?
                        if (m.error) {
                            (1,outTbl[m.id].error)(m.error, m.message);
                        } else {
                            if (m.result !== undefined) (1,outTbl[m.id].success)(m.result);
                            else (1,outTbl[m.id].success)();
                        }
                        delete outTbl[m.id];
                        delete s_transIds[m.id];
                    }
                } else if (method) {
                    // tis a notification.
                    if (regTbl[method]) {
                        // yep, there's a handler for that.
                        // transaction has only origin for notifications.
                        regTbl[method]({ origin: origin }, m.params);
                        // if the client throws, we'll just let it bubble out
                        // what can we do?  Also, here we'll ignore return values
                    }
                }
            };

            // now register our bound channel for msg routing
            s_addBoundChan(cfg.window, cfg.origin, ((typeof cfg.scope === 'string') ? cfg.scope : ''), onMessage);

            // scope method names based on cfg.scope specified when the Channel was instantiated
            var scopeMethod = function(m) {
                if (typeof cfg.scope === 'string' && cfg.scope.length) m = [cfg.scope, m].join("::");
                return m;
            };

            // a small wrapper around postmessage whose primary function is to handle the
            // case that clients start sending messages before the other end is "ready"
            var postMessage = function(msg, force) {
                if (!msg) throw "postMessage called with null message";

                // delay posting if we're not ready yet.
                var verb = (ready ? "post  " : "queue ");
                debug(verb + " message: " + JSON.stringify(msg));
                if (!force && !ready) {
                    pendingQueue.push(msg);
                } else {
                    if (typeof cfg.postMessageObserver === 'function') {
                        try {
                            cfg.postMessageObserver(cfg.origin, msg);
                        } catch (e) {
                            debug("postMessageObserver() raised an exception: " + e.toString());
                        }
                    }

                    cfg.window.postMessage(JSON.stringify(msg), cfg.origin);
                }
            };

            var onReady = function(trans, type) {
                debug('ready msg received');
                if (ready) throw "received ready message while in ready state.  help!";

                if (type === 'ping') {
                    chanId += '-R';
                } else {
                    chanId += '-L';
                }

                obj.unbind('__ready'); // now this handler isn't needed any more.
                ready = true;
                debug('ready msg accepted.');

                if (type === 'ping') {
                    obj.notify({ method: '__ready', params: 'pong' });
                }

                // flush queue
                while (pendingQueue.length) {
                    postMessage(pendingQueue.pop());
                }

                // invoke onReady observer if provided
                if (typeof cfg.onReady === 'function') cfg.onReady(obj);
            };

            var obj = {
                // tries to unbind a bound message handler.  returns false if not possible
                unbind: function (method) {
                    if (regTbl[method]) {
                        if (!(delete regTbl[method])) throw ("can't delete method: " + method);
                        return true;
                    }
                    return false;
                },
                bind: function (method, cb) {
                    if (!method || typeof method !== 'string') throw "'method' argument to bind must be string";
                    if (!cb || typeof cb !== 'function') throw "callback missing from bind params";

                    if (regTbl[method]) throw "method '"+method+"' is already bound!";
                    regTbl[method] = cb;
                    return this;
                },
                call: function(m) {
                    if (!m) throw 'missing arguments to call function';
                    if (!m.method || typeof m.method !== 'string') throw "'method' argument to call must be string";
                    if (!m.success || typeof m.success !== 'function') throw "'success' callback missing from call";

                    // now it's time to support the 'callback' feature of jschannel.  We'll traverse the argument
                    // object and pick out all of the functions that were passed as arguments.
                    var callbacks = { };
                    var callbackNames = [ ];
                    var seen = [ ];

                    var pruneFunctions = function (path, obj) {
                        if (seen.indexOf(obj) >= 0) {
                            throw "params cannot be a recursive data structure"
                        }
                        seen.push(obj);

                        if (typeof obj === 'object') {
                            for (var k in obj) {
                                if (!obj.hasOwnProperty(k)) continue;
                                var np = path + (path.length ? '/' : '') + k;
                                if (typeof obj[k] === 'function') {
                                    callbacks[np] = obj[k];
                                    callbackNames.push(np);
                                    delete obj[k];
                                } else if (typeof obj[k] === 'object') {
                                    pruneFunctions(np, obj[k]);
                                }
                            }
                        }
                    };
                    pruneFunctions("", m.params);

                    // build a 'request' message and send it
                    var msg = { id: s_curTranId, method: scopeMethod(m.method), params: m.params };
                    if (callbackNames.length) msg.callbacks = callbackNames;

                    if (m.timeout)
                    // XXX: This function returns a timeout ID, but we don't do anything with it.
                    // We might want to keep track of it so we can cancel it using clearTimeout()
                    // when the transaction completes.
                        setTransactionTimeout(s_curTranId, m.timeout, scopeMethod(m.method));

                    // insert into the transaction table
                    outTbl[s_curTranId] = { callbacks: callbacks, error: m.error, success: m.success };
                    s_transIds[s_curTranId] = onMessage;

                    // increment current id
                    s_curTranId++;

                    postMessage(msg);
                },
                notify: function(m) {
                    if (!m) throw 'missing arguments to notify function';
                    if (!m.method || typeof m.method !== 'string') throw "'method' argument to notify must be string";

                    // no need to go into any transaction table
                    postMessage({ method: scopeMethod(m.method), params: m.params });
                },
                destroy: function () {
                    s_removeBoundChan(cfg.window, cfg.origin, ((typeof cfg.scope === 'string') ? cfg.scope : ''));
                    if (window.removeEventListener) window.removeEventListener('message', onMessage, false);
                    else if(window.detachEvent) window.detachEvent('onmessage', onMessage);
                    ready = false;
                    regTbl = { };
                    inTbl = { };
                    outTbl = { };
                    cfg.origin = null;
                    pendingQueue = [ ];
                    debug("channel destroyed");
                    chanId = "";
                }
            };

            obj.bind('__ready', onReady);
            setTimeout(function() {
                postMessage({ method: scopeMethod('__ready'), params: "ping" }, true);
            }, 0);

            return obj;
        }
    };
})();

module.exports = Channel;
},{}]},{},[10]);
