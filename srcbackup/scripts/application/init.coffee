define [
  "jquery",
  "framework",
  "moment"
  "moment-timezone"
  "vendor/plugins/mvc_backbone",
  "vendor/backbone/backbone-nested",
  "config",
  "i18n!../lang/nls/manifest",
  "application/helpers/audiomanager",
  "application/helpers/audiomanagerHelper",

  "application/screens/splash",
  "application/screens/loader",

  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDPaymentManagement",
  "application/api/cd/CDSubscriberManagement",

  "application/screens/launch/launch",
  "application/screens/launch/register",
  "application/screens/launch/redemption",
  "application/screens/launch/account",

  "application/screens/home",
  "application/screens/login/login",
  "application/screens/login/pin",

  "application/popups/payment/changePayment",
  "application/popups/payment/paymentError",

  "application/api/authentication"

  "application/popups/exit/exit",
  "application/popups/error/error"

],($,
   TWC, Moment, MomentTimeZone, MVCPlugin, BackboneNested, Config, Manifest, AudioManager, AudioManagerHelper
   Splash, Loader,
   CDBaseRequest, CDPaymentManagement, CDSubscriberManagement,
   Launch, Register, Redemption, Account, Home, Login, PIN, ChangePayment, PaymentError,
   Authentication, Exit, Error) ->

  app =
    launch:  ->
      log "launch"
      #bind to return event
      @returnHandling = true

      # Block keys which aren't used
      input = TWC.SDK.Input.get()
      input.addToBlocked("exit")

      #set screen orientation to landscape
      try
        Windows.Graphics.Display.DisplayInformation.autoRotationPreferences = Windows.Graphics.Display.DisplayOrientations.landscape | Windows.Graphics.Display.DisplayOrientations.landscapeFlipped;
      catch e
        log e

      #attach mousemove event
      @mouseActiveTimer = null
      $('body').on "mousemove", (e)=>
        clearTimeout(@mouseActiveTimer)
        $(".navigationContainer:not(.noTimeout)").show()
        @mouseActiveTimer = setTimeout =>
          $(".navigationContainer:not(.noTimeout)").hide()
        , 2000


      $('body').on "return", (e) =>
        return if not @returnHandling
        #get active view
        active = TWC.MVC.Router.getActive()
        activePage = TWC.MVC.Router.getActivePage()

        # in case normal screen (not popup) pop history state
        if active.type is "popup" and activePage.uid isnt "settings-view"
          active.onReturn?()
        else if TWC.MVC.Router.getActive().uid is "filter-zone" and TWC.MVC.Router.getActive().filterIsActive
          TWC.MVC.Router.getActive().onReturn?()
        else if activePage.uid is "login-view" and activePage.returnCallback?
          activePage.returnCallback()
        else if activePage.uid is "pin-view" and activePage.failCallback?
          activePage.failCallback()
        else if activePage.uid is "extravideoplayer-view"
          activePage.onReturn?()
        else if activePage.uid is "videoplayer-view"
          activePage.onReturn?()
        # in case popup execute popup onReturn function
        else if activePage.uid is "productDetail-view"
          #custom return animation
          activePage.onReturn?()
        else if activePage.uid is "settings-view"
          return null
          #handled locally
        else if activePage.uid is "paymentconfirmation-view"
          #custom return animation
          activePage.cancel?()
        else
          TWC.MVC.History.popState()

          # Execute after popstate
          activeView = TWC.MVC.History.last()
          uid = if activeView? then activeView.uid

          # Set correct selected menuitem
          switch uid
            when "storefrontpage-view" then MenuHelper.setSelectedMenuItem("browse")
            when "settings-view" then MenuHelper.setSelectedMenuItem("settings")
            when "favorites-view" then MenuHelper.setSelectedMenuItem("favorites")
            when "bundleDetail-view" then return #do not change the selected menu item
            when "productDetail-view" then return #do not change the selected menu item
            when "seasonlist-view" then return #do not change the selected menu item
            when "video-search-view" then MenuHelper.setSelectedMenuItem("search")
            when "locker-view" then MenuHelper.setSelectedMenuItem("library")
            else
              # Default select movies
              MenuHelper.setSelectedMenuItem("browse")

      $(TWC.MVC.View.eventbus).on 'returnhandling', (e, status) =>
        @returnHandling = status

      #bind to exit event
      $('body').on 'exit', (e, type) ->
        # in case a "soft" exit and no other popup is open execute exit popup
        if type is "soft"
          exitAlert = new Exit()
          exitAlert.load
            data: [e, type]

      #bind to error event
      $(TWC.MVC.View.eventbus).on "error", (e, error)->
        try
          AudioManager.playErrorAudio()
        catch e
          log e
        TWC.SDK.Input.get().setBlockAll(false)                        #reset input block
        $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"]) #reset loader

        if error.type is "http"
          storage = TWC.SDK.Storage.get()
          storageValue = storage.getItem("network_errors")
          networkErrorCount = if storageValue? then parseInt(storageValue) else 0
          if networkErrorCount >= 3
            httpExitAlert = new HttpErrorExit()
            httpExitAlert.load()
            return

        errorAlert = new Error()
        errorAlert.load
          data: [error]

      AudioManager.initialize()

      # Start interval to reinitialize audiomanager due to bug in Chrome on Android
      if Config.platform is "androidtv"
        AudioManagerHelper.initialize()

      #Check credential state and determine first page load
      Authentication.silentLogin (error)->
        if Authentication.isLoggedIn()
          startPage = 'home-view'
        else
          startPage = 'launch-view'

        TWC.MVC.Router.load startPage,
            callback: =>
              $(TWC.MVC.View.eventbus).trigger("splashscreen", [false])
