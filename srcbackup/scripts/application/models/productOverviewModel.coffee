define [
  "backbone",
  "underscore",
  "application/models/baseModel",
  "application/api/cd/collections/CDProductCollection"
], (Backbone, _, BaseModel, CDProductCollection)->

  # Backbone model with product overview information
  productOverviewModel = BaseModel.extend
    relations:
      Products: CDProductCollection
    defaults:
      "Name":""
      "Products": []
      "TotalProductsPerPage": 16
      "TotalProducts": 0
      "subMenuItems": []