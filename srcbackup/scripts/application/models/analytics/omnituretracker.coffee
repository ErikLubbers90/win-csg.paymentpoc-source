define [
  "jquery",
  "framework",
  "config"
  "vendor/jschannel/jschannel"
], ($, TWC, Config, jschannel)->

  class OmnitureTracker extends TWC.SDK.Base

    instance: 14

    init: ()->
      return if not Config.omniture_tracking

      iframe = document.createElement('iframe')
      iframe.setAttribute "id", "otiframe"
      iframe.setAttribute "src", "ms-appx-web:///src/tracking/omnituretracker.html"
      document.body.appendChild(iframe)

      @channel = jschannel.build
        window: $("#otiframe").get(0).contentWindow,
        origin: "*",
        scope: "sps-ultra",
        onReady: =>
          @channel.call
            method:"init",
            params: { name: "name"},
            success: =>
              log("channel ready successs")

    trackPageView: (pageName)->
      return if not Config.omniture_tracking
      log("trackPageView - ", pageName)
      if not @channel
        setTimeout ()=>
          @trackPageView(pageName)
        , 1000
        return
      try
        @channel.call
          method:"trackPageView",
          params: { name: pageName},
          success: =>
            log("channel ready successs")
      catch err
        log "OmnitureTracker: trackPageView failed", err

    trackOutboundClick: (targetUrl, adId)->
      return if not Config.omniture_tracking
      log("trackOutboundClick - ", targetUrl, adId)
      if not @channel
        setTimeout ()=>
          @trackOutboundClick(targetUrl, adId)
        , 1000
        return
      try
        @channel.call
          method:"trackOutboundClick",
          params: { targetUrl: targetUrl, adId: adId },
          success: =>
            log("trackOutboundClick success")
      catch err
        log "OmnitureTracker: trackOutboundClick failed", err

    trackOutboundClickToBuy: (targetUrl, adId)->
      return if not Config.omniture_tracking
      log("trackOutboundClickToBuy - ", targetUrl, adId)
      if not @channel
        setTimeout ()=>
          @trackOutboundClickToBuy(targetUrl, adId)
        , 1000
        return
      try
        @channel.call
          method:"trackOutboundClickToBuy",
          params: { targetUrl: targetUrl, adId: adId },
          success: =>
            log("trackOutboundClickToBuy success")
      catch err
        log "OmnitureTracker: trackOutboundClickToBuy failed", err



    trackVideo: (name, event)->
      return if not Config.omniture_tracking
      log("trackVideo - ", name, event)
      if not @channel
        setTimeout ()=>
          @trackVideo(name, event)
        , 1000
        return
      try
        @channel.call
          method:"trackVideo",
          params: { name: name, event: event},
          success: =>
            log("trackVideo success")
      catch err
        log "OmnitureTracker: trackVideo failed", err
