define [
  "framework",
  "underscore",
  "config",
  "vendor/jschannel/jschannel"
], (TWC, _, Config, jschannel) ->
  # This module allows to use Akamai QOS
  #
  class AkamaiQOS extends TWC.SDK.Base

    akaPlugin: null

    akamaiIframeId: "akamaiIframe"

    init: (@options = {}) ->
      if not Config.akamai_enabled
        return

      log "Akamai: init - "

      iframe = document.createElement('iframe')
      iframe.setAttribute "id", @akamaiIframeId
      iframe.setAttribute "style", "width:0;height:0;visibility:hidden;"
      iframe.setAttribute "src", "ms-appx-web:///src/tracking/akamai.html"
      document.body.appendChild(iframe)

      @channel = jschannel.build
        window: $("##{@akamaiIframeId}").get(0).contentWindow,
        origin: "*",
        scope: "sps-ultra",
        onReady: =>
          @channel.call
            method:"init",
            params: { options: @options, akamaiXmlPath: Config.akamai_xml_path },
            success: =>
              log("channel ready successs")


    #Implemented calls
    handleSessionInit: ->
      return if not Config.akamai_enabled
      @channel.call
        method:"handleSessionInit",
        params: {},
        success: =>
          log("channel ready successs")

    handlePlaying: ->
      return if not Config.akamai_enabled
      @channel.call
        method:"handlePlaying",
        params: {},
        success: =>
          log("channel ready successs")

    handlePause: ->
      return if not Config.akamai_enabled
      @channel.call
        method:"handlePause",
        params: {},
        success: =>
          log("channel ready successs")

    handleResume: ->
      return if not Config.akamai_enabled
      @channel.call
        method:"handleResume",
        params: {},
        success: =>
          log("channel ready successs")

    #not used
    handleBitRateSwitch: (newBitRate)->
      return if not Config.akamai_enabled
      @channel.call
        method:"handleBitRateSwitch",
        params: {newBitRate: newBitRate},
        success: =>
          log("channel ready successs")

    #not used
    handleBufferStart: ->
      return if not Config.akamai_enabled
      @channel.call
        method:"handleBufferStart",
        params: {},
        success: =>
          log("channel ready successs")

    #not used
    handleBufferEnd: ->
      return if not Config.akamai_enabled
      @channel.call
        method:"handleBufferEnd",
        params: {},
        success: =>
          log("channel ready successs")

    handleApplicationExit: ->
      return if not Config.akamai_enabled
      @channel.call
        method:"handleApplicationExit",
        params: {},
        success: =>
          log("channel ready successs")

    handleError: (errorCode) ->
      return if not Config.akamai_enabled
      @channel.call
        method:"handleError",
        params: {errorCode:errorCode},
        success: =>
          log("channel ready successs")

    handlePlayEnd: (endReasonCode) ->
      return if not Config.akamai_enabled
      @channel.call
        method:"handlePlayEnd",
        params: {endReasonCode:endReasonCode},
        success: =>
          log("channel ready successs")
