define [
  "backbone",
  "underscore"
], (Backbone, _)->

  baseModel = Backbone.Model.extend({
    # Overrides fetch function for models which do not require to make requests
    fetch: (options) ->
      options.success()
  })