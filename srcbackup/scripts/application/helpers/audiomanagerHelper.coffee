define [
  "framework",
  "jquery",
  "config",
  "application/helpers/audiomanager"
], (TWC, $, Config, AudioManager)->

  class AudioManagerHelper

    # Interval to restart the audio manager
    @restartAudioManagerTime: 30000

    @initialize: =>
      $("body").on "keydown", (e) =>
        @restartAudioManagerInterval()

      @restartAudioManagerInterval()

    # Restart audio manager after set interval time has been passed
    @restartAudioManagerInterval: ->
      @stopTimerInitAudioManager()
      @restartAudioManagerTimer = setInterval =>
        AudioManager.destroy()
        AudioManager.initialize()
      , @restartAudioManagerTime

    @stopTimerInitAudioManager: ->
      clearInterval(@restartAudioManagerTimer) if @restartAudioManagerTimer?