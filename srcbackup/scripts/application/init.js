define(["jquery", "framework", "moment", "moment-timezone", "vendor/plugins/mvc_backbone", "vendor/backbone/backbone-nested", "config", "i18n!../lang/nls/manifest", "application/helpers/audiomanager", "application/helpers/audiomanagerHelper", "application/screens/splash", "application/screens/loader", "application/api/cd/CDBaseRequest", "application/api/cd/CDPaymentManagement", "application/api/cd/CDSubscriberManagement", "application/screens/launch/launch", "application/screens/launch/register", "application/screens/launch/redemption", "application/screens/launch/account", "application/screens/home", "application/screens/login/login", "application/screens/login/pin", "application/popups/payment/changePayment", "application/popups/payment/paymentError", "application/api/authentication", "application/popups/exit/exit", "application/popups/error/error"], function($, TWC, Moment, MomentTimeZone, MVCPlugin, BackboneNested, Config, Manifest, AudioManager, AudioManagerHelper, Splash, Loader, CDBaseRequest, CDPaymentManagement, CDSubscriberManagement, Launch, Register, Redemption, Account, Home, Login, PIN, ChangePayment, PaymentError, Authentication, Exit, Error) {
  var app;
  return app = {
    launch: function() {
      var e, input;
      log("launch");
      this.returnHandling = true;
      input = TWC.SDK.Input.get();
      input.addToBlocked("exit");
      try {
        Windows.Graphics.Display.DisplayInformation.autoRotationPreferences = Windows.Graphics.Display.DisplayOrientations.landscape | Windows.Graphics.Display.DisplayOrientations.landscapeFlipped;
      } catch (error1) {
        e = error1;
        log(e);
      }
      this.mouseActiveTimer = null;
      $('body').on("mousemove", (function(_this) {
        return function(e) {
          clearTimeout(_this.mouseActiveTimer);
          $(".navigationContainer:not(.noTimeout)").show();
          return _this.mouseActiveTimer = setTimeout(function() {
            return $(".navigationContainer:not(.noTimeout)").hide();
          }, 2000);
        };
      })(this));
      $('body').on("return", (function(_this) {
        return function(e) {
          var active, activePage, activeView, base, uid;
          if (!_this.returnHandling) {
            return;
          }
          active = TWC.MVC.Router.getActive();
          activePage = TWC.MVC.Router.getActivePage();
          if (active.type === "popup" && activePage.uid !== "settings-view") {
            return typeof active.onReturn === "function" ? active.onReturn() : void 0;
          } else if (TWC.MVC.Router.getActive().uid === "filter-zone" && TWC.MVC.Router.getActive().filterIsActive) {
            return typeof (base = TWC.MVC.Router.getActive()).onReturn === "function" ? base.onReturn() : void 0;
          } else if (activePage.uid === "login-view" && (activePage.returnCallback != null)) {
            return activePage.returnCallback();
          } else if (activePage.uid === "pin-view" && (activePage.failCallback != null)) {
            return activePage.failCallback();
          } else if (activePage.uid === "extravideoplayer-view") {
            return typeof activePage.onReturn === "function" ? activePage.onReturn() : void 0;
          } else if (activePage.uid === "videoplayer-view") {
            return typeof activePage.onReturn === "function" ? activePage.onReturn() : void 0;
          } else if (activePage.uid === "productDetail-view") {
            return typeof activePage.onReturn === "function" ? activePage.onReturn() : void 0;
          } else if (activePage.uid === "settings-view") {
            return null;
          } else if (activePage.uid === "paymentconfirmation-view") {
            return typeof activePage.cancel === "function" ? activePage.cancel() : void 0;
          } else {
            TWC.MVC.History.popState();
            activeView = TWC.MVC.History.last();
            uid = activeView != null ? activeView.uid : void 0;
            switch (uid) {
              case "storefrontpage-view":
                return MenuHelper.setSelectedMenuItem("browse");
              case "settings-view":
                return MenuHelper.setSelectedMenuItem("settings");
              case "favorites-view":
                return MenuHelper.setSelectedMenuItem("favorites");
              case "bundleDetail-view":
                break;
              case "productDetail-view":
                break;
              case "seasonlist-view":
                break;
              case "video-search-view":
                return MenuHelper.setSelectedMenuItem("search");
              case "locker-view":
                return MenuHelper.setSelectedMenuItem("library");
              default:
                return MenuHelper.setSelectedMenuItem("browse");
            }
          }
        };
      })(this));
      $(TWC.MVC.View.eventbus).on('returnhandling', (function(_this) {
        return function(e, status) {
          return _this.returnHandling = status;
        };
      })(this));
      $('body').on('exit', function(e, type) {
        var exitAlert;
        if (type === "soft") {
          exitAlert = new Exit();
          return exitAlert.load({
            data: [e, type]
          });
        }
      });
      $(TWC.MVC.View.eventbus).on("error", function(e, error) {
        var errorAlert, httpExitAlert, networkErrorCount, storage, storageValue;
        try {
          AudioManager.playErrorAudio();
        } catch (error1) {
          e = error1;
          log(e);
        }
        TWC.SDK.Input.get().setBlockAll(false);
        $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"]);
        if (error.type === "http") {
          storage = TWC.SDK.Storage.get();
          storageValue = storage.getItem("network_errors");
          networkErrorCount = storageValue != null ? parseInt(storageValue) : 0;
          if (networkErrorCount >= 3) {
            httpExitAlert = new HttpErrorExit();
            httpExitAlert.load();
            return;
          }
        }
        errorAlert = new Error();
        return errorAlert.load({
          data: [error]
        });
      });
      AudioManager.initialize();
      if (Config.platform === "androidtv") {
        AudioManagerHelper.initialize();
      }
      return Authentication.silentLogin(function(error) {
        var startPage;
        if (Authentication.isLoggedIn()) {
          startPage = 'home-view';
        } else {
          startPage = 'launch-view';
        }
        return TWC.MVC.Router.load(startPage, {
          callback: (function(_this) {
            return function() {
              return $(TWC.MVC.View.eventbus).trigger("splashscreen", [false]);
            };
          })(this)
        });
      });
    }
  };
});
