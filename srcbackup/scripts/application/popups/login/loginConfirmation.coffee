define [
  "framework"
  "../generic/alertWithOptions"
  "application/api/authentication"
  "i18n!../../../lang/nls/manifest"
  "application/models/baseModel"
], (TWC, alertWithOptions, Authentication, Manifest, baseModel) ->

  class loginConfirmationAlert extends alertWithOptions

    onload:(@isFocusInMainMenu, @confirmationCallback, @isFocusInSettingsSubmenu = false, @cancelCallback = null)->
      @model = new baseModel();
      @model.set("title", Manifest.login_confirmation_text)
      @model.set("description", "")
      @model.set("leftButtonText", Manifest.popup_confirm)
      @model.set("rightButtonText", Manifest.popup_no)

    leftButtonAction: ->
      @close =>
        @confirmationCallback?()

    rightButtonAction: ->
      @cancelCallback?()
      @close()
      TWC.MVC.Router.find("menu-zone").fixSelectedMenuItem(@isFocusInMainMenu)
      if @isFocusInSettingsSubmenu
        TWC.MVC.Router.find("settings-view").fixSelectedSubmenuItem()
