define [
  "framework",
  "application/models/baseModel",
  "jquery",
  "application/popups/generic/alertWithOptions",
  "application/api/cd/collections/CDPaymentInstrumentCollection",
  "i18n!../../../lang/nls/manifest",
  "application/helpers/gatrackinghelper"
], (TWC, BaseModel, $, AlertWithOptions, CDPaymentInstrumentCollection, Manifest, GATracker) ->

  class paymentOptionsView extends AlertWithOptions

    html:"""
      <div id="popupContainer" class="paymentOptionsContainer">
          <h1><%= manifest.payment_options_title %></h1>

          <div id="popupButtons">
              <% var totalPaymentOptions = (data.length > limitPaymentOptions ? limitPaymentOptions : data.length); %>
              <% if(totalPaymentOptions > 0) { %>
                <% for(var i=0;i<totalPaymentOptions;i++){ %>
                  <div id="payment_<%= data[i].Id %>" data-paymenttype="<%= data[i].Type %>" data-paymentid="<%= data[i].Id %>" class="button paymentOption" data-up="defaultFocus" data-down="<%= (i < totalPaymentOptions-1 ? "defaultFocus" : "#popupBack") %>"><%= data[i].Name %></div>
                <% } %>
              <% } else { %>
                <p><%= manifest.payment_method_unavailable %></p>
                <div id="leftButton" class="button" data-down="#popupBack" data-right="#rightButton"><%= manifest.close %></div>
              <% } %>
          </div>
          <div id="popupNavhelp">
            <div id="popupBack" class="button" data-up="<%= (totalPaymentOptions > 0 ? "#payment_"+data[totalPaymentOptions-1].Id : "#leftButton" ) %>"><%- manifest.popup_back %></div>
          </div>
      </div>
    """

    defaultFocus: "#popupButtons .button:first"

    events:
      "enter .button.paymentOption" : "selectPayment"
      "enter #leftButton"           : "closePopup"
      "enter #popupBack"            : "onReturn"

    # Max number of paymentoptions displayed excluding fixed promotioncode option
    # TODO create scrolllist for paymentoptions
    limitPaymentOptions: 7

    onload: (currency, @goBack, @goToOrder) ->
      @model = new CDPaymentInstrumentCollection({}, {currency: currency})

    template:(html, data) ->
      return _.template(html)({data:data, manifest:Manifest, limitPaymentOptions: @limitPaymentOptions})

    onReturn: ->
      @goBack()

    closePopup: ->
      $('#popup').removeClass('fadein')
      setTimeout ()=>
        @unload()
        callback?()
      , 500

    selectPayment: (el) ->
      $(TWC.MVC.View.eventbus).trigger("loading", true)

      $el = $(el.target)
      paymentInstrumentId = parseInt($el.data("paymentid"))
      paymentType = $el.data("paymenttype")
      defaultPaymentInstrument = @model.getDefaultPaymentInstrument()
      paymentInstrumentIds = {
        "selectedId": paymentInstrumentId
        "defaultId": defaultPaymentInstrument.Id
      }

      GATracker.trackEvent("Purchase", "Payment option select", paymentType)

      @goToOrder?(paymentInstrumentIds)