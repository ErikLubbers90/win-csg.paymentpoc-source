define [
  "framework"
  "../generic/alertWithOptions"
  "application/api/authentication"
  "i18n!../../../lang/nls/manifest"
  "application/models/baseModel",
  "application/popups/payment/setDevicePin",
  "application/api/cd/CDBaseRequest"
], (TWC, alertWithOptions, Authentication, Manifest, baseModel, SetDevicePinPopup, CDBaseRequest) ->

  class useDevicePinAlert extends alertWithOptions

    html:"""
      <div id="popupContainer" class="headerOnly">
          <h1><%= header %></h1>
          <div id="popupButtons">
              <div id="leftButton" class="" data-right="#rightButton"><%= leftButtonText %></div>
              <div id="rightButton" class="" data-left="#leftButton"><%= rightButtonText %></div>
          </div>
      </div>
    """

    onload: (@callback) ->
      header = Manifest.set_device_pin_popup_title

      @model = new baseModel();
      @model.set("header", header)
      @model.set("leftButtonText", Manifest.popup_no)
      @model.set("rightButtonText", Manifest.popup_confirm)

    leftButtonAction: ->
      submitAction = false
      $(TWC.MVC.View.eventbus).trigger("loading", true)
      @close(submitAction, @callback)

    rightButtonAction: ->
      submitAction = true
      $(TWC.MVC.View.eventbus).trigger("loading", true)
      @close(submitAction, @callback)

    close: (submitAction=false, callback) ->
      super
      callback?(submitAction)