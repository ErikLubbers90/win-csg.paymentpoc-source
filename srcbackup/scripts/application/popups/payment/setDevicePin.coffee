define [
  "config",
  "framework",
  "application/models/baseModel",
  "jquery",
  "application/api/authentication",
  "application/popups/keyboard/keyboard",
  "application/popups/generic/alertWithOptions",
  "application/api/cd/CDSubscriberManagement",
  "application/api/cd/CDPaymentManagement",
  "i18n!../../../lang/nls/manifest"
], (Config, TWC, BaseModel, $, Authentication, Keyboard, AlertWithOptions, CDSubscriberManagement, CDPaymentManagement, Manifest) ->

  class setDevicePinView extends AlertWithOptions

    html:"""
      <div id="popupContainer" class="devicePinContainer">
          <h1><%= header %></h1>
          <p class="description"><%= description %></p>

          <div id="pin" data-down="focusPassword">
              <div id="one" class="pinfield"><span></span></div>
              <div id="two" class="pinfield"><span></span></div>
              <div id="three" class="pinfield"><span></span></div>
              <div id="four" class="pinfield"><span></span></div>
              <input type="text" id="pinNumber" maxlength="4" />
          </div>
          <div id="passwordContainer">
              <p class="description"><%= description_password %></p>
              <input id="pinPassword" type="password" class="inputLogin" data-up="focusPin" data-down="focusRightButton" placeholder="<%= passwordFieldPlaceholder %>" value="" />
          </div>
          <p id="devicePinResult"></p>
          <div id="popupButtons">
              <div id="leftButton" class="" data-up="focusPassword" data-right="#rightButton"><%= leftButtonText %></div>
              <div id="rightButton" class="" data-up="focusPassword" data-left="#leftButton"><%= rightButtonText %></div>
          </div>
      </div>
    """

    # Used to keep track of which keyboard is opened
    keyboardId: ""

    maxPinlength: 4

    focusedInput = false

    events:
      "enter #pin"                : "showNumbersIME"
      "enter #pinPassword"        : "showIME"
      "enter #leftButton"         : "leftButtonAction"
      "enter #rightButton"        : "rightButtonAction"

    onload: (@callback, @password=null) ->
      @model = new BaseModel();

      @model.set("header", Manifest.set_device_pin_title)
      @model.set("description", Manifest.set_device_pin_description)
      @model.set("description_password", Manifest.set_device_pin_description_password)
      @model.set("passwordFieldPlaceholder", Manifest.login_input_password)
      @model.set("leftButtonText", Manifest.set_device_pin_cancel_button)
      @model.set("rightButtonText", Manifest.set_device_pin_submit_button)

    onunload: ->
      $("#devicePinResult").html("")
      $("#pinNumber").val("")
      $("#pinPassword").val("")
      $("#pin .pinfield").removeClass "set"

    onRenderComplete: ->
      super
      if @password
        $("#pinPassword").val(@password)

      setTimeout =>
        @showNumbersIME()
      , 1000

    setFieldActive: ->
      TWC.MVC.Router.unFocus()
      $("##{@keyboardId}").addClass("active")

    unsetFieldActive: ->
      $("##{@keyboardId}").removeClass("active")

    showIME: ->
      @focusedInput = true
      $("#devicePinResult").html("")
      $("#popupContainer.devicePinContainer").addClass("moveUp")

      @keyboardId = "pinPassword"
      @setFieldActive()
      @focusPassword()
      if Config.platform in ["windows10"]
        TWC.SDK.Input.get().addToBlocked("enter", "up", "down", "left", "right", "axes")
      else
        Keyboard.load
          data: [@keyboardId, false]

    showNumbersIME: ->
      @focusedInput = true
      $("#devicePinResult").html("")
      $("#pinNumber").val("")
      $("#pin .pinfield").removeClass "set"

      $("#popupContainer.devicePinContainer").addClass("moveUp")

      @keyboardId = "pin"
      @setFieldActive()

      #@param useOnlyNumbers [Bool] to activate IME with a numbers only mode
      numbersOnlyMode = true

      @focusPin()

      if Config.platform in ["windows10"]
        TWC.SDK.Input.get().addToBlocked("enter", "up", "down", "left", "right", "axes")
      else
        Keyboard.load
          data: ["pinNumber", numbersOnlyMode]

    isFocused: ->
      return @focusedInput

    hideIME: ->
      $("#popupContainer.devicePinContainer").removeClass("moveUp")
      $("#pinPassword").blur()
      $("#pinNumber").blur()
      @focusedInput = false

    focusPassword: ->
      TWC.MVC.Router.setFocus("#pinPassword")
      if Config.platform in ["windows10"]
        $("#pinPassword").focus()

    focusPin: ->
      TWC.MVC.Router.setFocus("#pin")
      if Config.platform in ["windows10"]
        $("#pinNumber").focus()

    focusRightButton: ->
      TWC.MVC.Router.setFocus("#rightButton")
      if Config.platform in ["windows10"]
        $("#pinPassword").blur()

    leftButtonAction: ->
      @close(@callback)

    rightButtonAction: ->
      pinNumber = $("#pinNumber").val()
      password = $("#pinPassword").val()

      if pinNumber.length < 4 or not /^[0-9]+$/.test(pinNumber) or password.length is 0
        $("#devicePinResult").html(Manifest.set_device_pin_error)
        return

      $(TWC.MVC.View.eventbus).trigger("loading", true)
      $("#devicePinResult").html("")

      CDSubscriberManagement.updateDevicePin pinNumber, password, (error, response) =>
        $(TWC.MVC.View.eventbus).trigger("loading", false)

        if error
          message = CDPaymentManagement.orderErrors(error)
          $("#devicePinResult").html(message)
          return

        @close(@callback)

    # Keyboard functions
    valueIsChanged: (keyword) ->
      $("#pin .pinfield").eq(keyword.length-1).addClass "set"

      if @keyboardId is "pin" and keyword.length is @maxPinlength
        Keyboard.closeView()

    closeKeyboard: ->
      $("#popupContainer.devicePinContainer").removeClass("moveUp")

      setTimeout =>
        if @keyboardId and $("##{@keyboardId}").length
          @unsetFieldActive()
          TWC.MVC.Router.setFocus("##{@keyboardId}")
          @keyboardId = ""
      ,1
