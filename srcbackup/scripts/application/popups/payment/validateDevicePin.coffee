define [
  "framework",
  "application/models/baseModel",
  "jquery",
  "application/api/authentication",
  "application/api/cd/CDSubscriberManagement",
  "application/api/cd/CDPaymentManagement",
  "application/popups/keyboard/keyboard",
  "application/popups/generic/alert",
  "application/popups/generic/alertWithOptions"
  "application/popups/payment/setDevicePin",
  "i18n!../../../lang/nls/manifest"
], (TWC, BaseModel, $, Authentication, CDSubscriberManagement, CDPaymentManagement, Keyboard, AlertMessage, AlertWithOptions, SetDevicePinPopup, Manifest) ->

  class validateDevicePinView extends AlertWithOptions

    html:"""
      <div id="popupContainer" class="validateDevicePinContainer">
          <h1><%= description %></h1>

          <div id="pin" data-down="#rightButton">
              <div id="one" class="pinfield"><span></span></div>
              <div id="two" class="pinfield"><span></span></div>
              <div id="three" class="pinfield"><span></span></div>
              <div id="four" class="pinfield"><span></span></div>
              <input type="password" id="pinNumber" maxlength="4" />
          </div>
          <p id="validateDevicePinResult"></p>
          <div id="popupButtons">
              <div id="leftButton" class="" data-up="#pin" data-right="#rightButton" data-down="#popupBack"><%= leftButtonText %></div>
              <div id="rightButton" class="" data-up="#pin" data-left="#leftButton" data-down="#popupBack"><%= rightButtonText %></div>
          </div>
          <div id="popupNavhelp">
            <div id="popupBack" class="button" data-up="#rightButton"><%- manifest.popup_back %></div>
          </div>
      </div>
    """

    keyboardId: ""

    maxPinlength: 4

    events:
      "enter #pin"                : "showNumbersIME"
      "enter #leftButton"         : "leftButtonAction"
      "enter #rightButton"        : "rightButtonAction"
      "enter #popupBack"          : "onReturn"

    onload: (@movieTitle, @quality, @pricingPlan, @onReturn, @goToOrder) ->
      description = Manifest.validate_device_pin_description.replace("{movie_title}", "'#{@movieTitle}'").replace("{movie_quality}", @quality)
      multipleDevicesText = Manifest.validate_device_pin_multiple_devices_text

      @model = new BaseModel();
      @model.set("header", Manifest.validate_device_pin_title)
      @model.set("description", description)
      @model.set("multipleDevicesText", multipleDevicesText)
      @model.set("leftButtonText", Manifest.validate_device_pin_forgot_password_button)
      @model.set("rightButtonText", Manifest.continue)
      @model.set("manifest", Manifest)

    onunload: ->
      $("#pinNumber").val("")
      $("#validateDevicePinResult").html("")
      $("#pin .pinfield").removeClass "set"

    onRenderComplete: ->
      super
      TWC.MVC.Router.setFocus("#pin")
      setTimeout =>
        @showNumbersIME()
      , 500

    showNumbersIME: ->
      $("#validateDevicePinResult").html("")
      $("#pinNumber").val("")
      $("#pin .pinfield").removeClass "set"


      $("#popupContainer.validateDevicePinContainer").addClass("moveUp")

      @keyboardId = "pin"
      @setFieldActive()

      #@param useOnlyNumbers [Bool] to activate IME with a numbers only mode
      numbersOnlyMode = true
      Keyboard.load
        data: ["pinNumber", numbersOnlyMode]

    setFieldActive: ->
      TWC.MVC.Router.unFocus()
      $("##{@keyboardId}").addClass("active")

    unsetFieldActive: ->
      $("##{@keyboardId}").removeClass("active")

    leftButtonAction: ->
      @close =>
        @forgotPin()

    # Validate pin
    rightButtonAction: ->
      $("#validateDevicePinResult").html("")
      pinNumber = $("#pinNumber").val()

      if pinNumber.length < 4 or (not /^[0-9]+$/.test(pinNumber))
        $("#validateDevicePinResult").html(Manifest.validate_device_pin_error)
        return

      $(TWC.MVC.View.eventbus).trigger("loading", true)

      CDPaymentManagement.promoteDeviceSession(pinNumber, (error, response) =>
        if not error
          # Current session has been converted to a fully authenticated session (Auth Type 5)
          @goToOrder(@pricingPlan, @quality)
        else
          # Something went wrong, pincode incorrect?
          $("#validateDevicePinResult").html(Manifest.validate_device_pin_error)
          $(TWC.MVC.View.eventbus).trigger("loading", false)
      )

    forgotPin: ->
      setDevicePinAlert = new SetDevicePinPopup()
      setDevicePinAlert.load()

    # Keyboard functions
    valueIsChanged: (keyword) ->
      $("#pin .pinfield").eq(keyword.length-1).addClass "set"

      if keyword.length is @maxPinlength
        Keyboard.closeView()

    closeKeyboard: ->
      $("#popupContainer.validateDevicePinContainer").removeClass("moveUp")

      setTimeout =>
        if @keyboardId and $("##{@keyboardId}").length
          @unsetFieldActive()
          TWC.MVC.Router.setFocus("##{@keyboardId}")
          @keyboardId = ""
      ,1

