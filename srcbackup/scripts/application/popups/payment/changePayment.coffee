define [
  "framework",
  "settingPages",
  "config",
  "jquery",
  "../../models/baseModel",
  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDSubscriberManagement",
  "application/api/authentication",
  "../../models/analytics/omnituretracker",
  "i18n!../../../lang/nls/manifest" 
], (TWC, ConfigSettings, Config, $, BaseModel, CDBaseRequest, CDSubscriberManagement, Authentication, OmnitureTracker, Manifest) ->

  changePaymentModel = BaseModel.extend
    defaults:
      "manifest": Manifest,

  class changePaymentView extends TWC.MVC.View
    uid: 'changepayment-popup'

    el: "#popup"

    type: 'popup'

    model: new changePaymentModel()

    defaultFocus: "#paymentOption-1"

    external:
      html:
        url:"skin/templates/popups/payment/changepayment.tpl"

    events:
      "enter .payment-option": "changePaymentOption"
      "enter #continueButton": "close"
      "enter #popupCloseButton": "close"


    onload: (@paymentInstruments, @selectedId) ->
      @model.set("paymentInstruments", @paymentInstruments)

    onRenderComplete: ->
      TWC.MVC.Router.setSelect( "##{$(".payment-option[data-paymentid=#{@selectedId}]").attr("id")}")
      defaultPaymentInstrument = _.findWhere(@model.get("paymentInstruments"), {Default:true})

      #return first payment instrument if no default is set within the user account
      if not defaultPaymentInstrument
        defaultPaymentInstrument = @model.get("paymentInstruments")[0]

      @paymentInstrumentIds = {
        "selectedId": @selectedId
        "defaultId": defaultPaymentInstrument.Id
      }

      setTimeout () =>
        $('#popup').addClass('fadein')
        $(TWC.MVC.View.eventbus).trigger("loading", [false])
      , 100
      @ot = OmnitureTracker.get()
      @ot.trackPageView('selectpayment.html')


    changePaymentOption: (el)->
      $el = $(el.target)
      paymentInstrumentId = parseInt($el.data("paymentid"))
      defaultPaymentInstrument = _.findWhere(@model.get("paymentInstruments"), {Default:true})
      #return first payment instrument if no default is set within the user account
      if not defaultPaymentInstrument
        defaultPaymentInstrument = @model.get("paymentInstruments")[0]

      @paymentInstrumentIds = {
        "selectedId": paymentInstrumentId
        "defaultId": defaultPaymentInstrument.Id
      }
      TWC.MVC.Router.setFocus "#continueButton"


    focusLeft: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("payment-option")
        pos = parseInt(TWC.MVC.Router.getFocus().split("-")[1])
        if pos % 2 is 1
          TWC.MVC.Router.setFocus "#paymentOption-#{pos-1}"
        else
          @moveToMenu()

    focusRight: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("payment-option")
        pos = parseInt(TWC.MVC.Router.getFocus().split("-")[1])
        if pos % 2 is 0
          TWC.MVC.Router.setFocus "#paymentOption-#{pos+1}"

    focusUp: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("payment-option")
        pos = parseInt(TWC.MVC.Router.getFocus().split("-")[1])
        if pos > 1
          TWC.MVC.Router.setFocus "#paymentOption-#{pos-2}"

    focusDown: ->
      $el = $(TWC.MVC.Router.getFocus())

      if $el.hasClass("payment-option")
        pos = parseInt(TWC.MVC.Router.getFocus().split("-")[1])
        if $("#paymentOption-#{pos+2}").length
          TWC.MVC.Router.setFocus "#paymentOption-#{pos+2}"
        else if pos % 2 is 1 and $("#paymentOption-#{pos+1}").length
          TWC.MVC.Router.setFocus "#paymentOption-#{pos+1}"
        else
          TWC.MVC.Router.setFocus "#continueButton"


    close: ->
      $('#popup').removeClass('fadein')
      TWC.MVC.Router.find("paymentconfirmation-view").setPaymentInstrumentIds(@paymentInstrumentIds)
      setTimeout ()=>
        @unload()
      , 500

    onReturn: ->
      @close()

  changePaymentView.get()
