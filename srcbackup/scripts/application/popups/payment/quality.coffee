define [
  "framework",
  "config",
  "../generic/alertWithOptions",
  "application/api/authentication",
  "application/api/cd/CDSubscriberManagement",
  "application/api/cd/CDPaymentManagement",
  "application/api/cd/CDBaseRequest",
  "i18n!../../../lang/nls/manifest",
  "application/models/baseModel",
  "application/helpers/gatrackinghelper"
], (TWC, Config, AlertWithOptions, Authentication, CDSubscriberManagement, CDPaymentManagement, CDBaseRequest, Manifest, BaseModel, GATracker) ->

  class qualityAlert extends AlertWithOptions

    html:"""
      <div id="popupContainer" class="headerOnly">
          <h1><%= header %></h1>
          <div id="popupButtons">
              <div id="sdContainer" class="buttonContainerContent">
                <div id="sdButton" class="" data-right="#hdButton" data-down="#popupBack"><%= sdButtonText %></div>
                <div class="buttonInfoContainer">
                    <p class="buttonInfo"><%= sdLoyaltyPoints %></p>
                </div>
              </div>
              <div id="hdContainer" class="buttonContainerContent">
                <div id="hdButton" class="" data-left="#sdButton" data-right="#uhdButton" data-down="#popupBack"><%= hdButtonText %></div>
                <div class="buttonInfoContainer">
                    <p class="buttonInfo"><%= hdLoyaltyPoints %></p>
                </div>
              </div>
              <div id="uhdContainer" class="buttonContainerContent">
                <div id="uhdButton" class="" data-left="#hdButton" data-down="#popupBack"><%= uhdButtonText %></div>
                <div class="buttonInfoContainer">
                    <p class="buttonInfo"><%= uhdLoyaltyPoints %></p>
                </div>
              </div>
          </div>
          <div id="popupNavhelp">
            <div id="popupBack" class="button" data-up="focusButtons"><%- manifest.popup_back %></div>
          </div>
      </div>
    """

    events:
      "enter #uhdButton": "uhdButtonAction"
      "enter #hdButton": "hdButtonAction"
      "enter #sdButton": "sdButtonAction"
      "enter #popupBack": "onReturn"

    onload: (@movieTitle, @pricingPlans, @goToValidatePin, @goToSetDevicePin, @goToOrder) ->
      header = Manifest.order_quality_title.replace("{movie_title}", "'#{@movieTitle}'")

      uhdButtonText = if @pricingPlans.uhd? then "UHD #{@pricingPlans.uhd.getPrice()}"
      uhdLoyaltyPoints = if @pricingPlans.uhd? then if Config.enableLoyaltyPoints then Manifest.loyaltyPoints.replace("{points}", @pricingPlans.uhd.getLoyaltyPoints()) else ""

      hdButtonText = if @pricingPlans.hd? then "HD #{@pricingPlans.hd.getPrice()}"
      hdLoyaltyPoints = if @pricingPlans.hd? then if Config.enableLoyaltyPoints then Manifest.loyaltyPoints.replace("{points}", @pricingPlans.hd.getLoyaltyPoints()) else "" if @pricingPlans.hd?

      sdButtonText = if @pricingPlans.sd? then "SD #{@pricingPlans.sd.getPrice()}"
      sdLoyaltyPoints = if @pricingPlans.sd? then if Config.enableLoyaltyPoints then Manifest.loyaltyPoints.replace("{points}", @pricingPlans.sd.getLoyaltyPoints()) else "" if @pricingPlans.sd?

      @model = new BaseModel();
      @model.set("header", header)
      @model.set("uhdButtonText", uhdButtonText)
      @model.set("hdButtonText", hdButtonText)
      @model.set("sdButtonText", sdButtonText)
      @model.set("uhdLoyaltyPoints", uhdLoyaltyPoints)
      @model.set("hdLoyaltyPoints", hdLoyaltyPoints)
      @model.set("sdLoyaltyPoints", sdLoyaltyPoints)
      @model.set("manifest", Manifest)



    uhdButtonAction: ->      
      @buttonAction(@pricingPlans.uhd, "UHD")

    hdButtonAction: ->
      @buttonAction(@pricingPlans.hd, "HD")

    sdButtonAction: ->
      @buttonAction(@pricingPlans.sd, "SD")

    buttonAction: (pricingPlan, quality) ->
      $(TWC.MVC.View.eventbus).trigger("loading", true)
      GATracker.trackEvent("Purchase", "Select quality", "Own " + quality)

      # allowPinlessPurchase = false then PIN is required
      if not CDBaseRequest.allowPinlessPurchase
        CDSubscriberManagement.retrieveDevicePinStatus (error, hasPin) =>
          $(TWC.MVC.View.eventbus).trigger("loading", false)
          if not hasPin
            # No pin set, show popup to user to set the pin
            @goToSetDevicePin?()
          else
            # Pin is set, show popup to let the user enter his/her pin
            @goToValidatePin?(pricingPlan, quality)

      # No PIN required for purchase
      else
        $(TWC.MVC.View.eventbus).trigger("loading", false)
        @goToOrder?(pricingPlan, quality)


    focusButtons: ->
      return TWC.MVC.Router.setFocus("#sdButton") if $("#sdButton").length
      return TWC.MVC.Router.setFocus("#hdButton") if $("#hdButton").length
      return TWC.MVC.Router.setFocus("#uhdButton") if $("#uhdButton").length


    onRenderComplete: ->
      $(TWC.MVC.View.eventbus).trigger("loading", [false])    #disable loader if it was still running e.g. during error

      #disable empty buttons
      $("#sdContainer").hide() if !@pricingPlans.sd
      $("#hdContainer").hide() if !@pricingPlans.hd
      $("#uhdContainer").hide() if !@pricingPlans.uhd


      if @pricingPlans.sd? and @pricingPlans.hd? and @pricingPlans.uhd?
        $("#popupContainer").addClass("widePopup")

      # Vertical center popup
      popupMarginTop = $("#popup").height() / 2 - $("#popupContainer").innerHeight() / 2
      $("#popupContainer").css("margin-top", "#{popupMarginTop}px")

      setTimeout () =>
        $('#popup').addClass('fadein')
        return TWC.MVC.Router.setFocus("#sdButton") if @pricingPlans.sd?        
        return TWC.MVC.Router.setFocus("#hdButton") if @pricingPlans.hd?
        return TWC.MVC.Router.setFocus("#uhdButton") if @pricingPlans.uhd?        
      , 100