define [
  "framework",
  "config",
  "../generic/alertWithOptions",
  "../generic/alert",
  "application/api/authentication",
  "i18n!../../../lang/nls/manifest",
  "application/models/baseModel",
  "application/api/cd/models/CDOrderQuote",
  "application/api/cd/CDPaymentManagement",
  "application/popups/payment/paymentOptions",
  "application/popups/payment/validatePromotionCode",
  "application/popups/error/error",
  "application/helpers/gatrackinghelper"
], (TWC, Config, alertWithOptions, Alert, Authentication, Manifest, BaseModel, CDOrderQuote, CDPaymentManagement, PaymentOptions, ValidatePromotionCode, Error, GATracker) ->

  class orderOverview extends alertWithOptions

    checkValidPromotionCode: false

    events:
      "enter #paymentRedemptionButton": "promotionCodeAction"
      "enter #leftButton": "closePopup"
      "enter #rightButton": "submitOrderAction"
      "enter #popupBack": "onReturn"

    html:"""
      <div id="popupContainer" class="purchase">
          <h1><%= manifest.order_popup_title %></h1>
          <p><%= movieTitle %></p>
          <p><%= orderDescription %></p>
          <table>            
            <% if(data.DiscountAmount && data.DiscountAmount > 0){ %>
            <tr class="rowWithTopSpacing">
              <td><% if(data.AppliedCoupons.length > 0){ %><%= data.AppliedCoupons %><% }else{ %><%= data.AppliedDiscounts %><% } %></td>
              <td>-<%= data.DiscountAmountAsPrice %></td>
            </tr>
            <% } %>
            <tr class="separator">
              <td colspan="2">
                <div style></div>
              </td>
            </tr>
            <tr class="orderTotal">
              <td><%= manifest.ordertotal %></td>
              <td><%= data.TotalAmountAsPrice %></td>
            </tr>
            <% for(var j=0;j<data.PaymentInstruments.length;j++){ %>
            <tr>
              <td><%= data.PaymentInstruments[j].Name %></td>
              <td><%= data.PaymentInstruments[j].AmountAsPrice %></td>
            </tr>
            <% } %>
            <% if(loyaltyPoints.length > 0){ %>
            <tr class="rowWithSpacing">
              <td><%= manifest.loyalty_points_total %></td>
              <td><%= loyaltyPoints %></td>
            </tr>
            <% } %>
            </table>
            <% if(checkValidPromotionCode){ %>
              <% if(data.AppliedCoupons.length == 0){ %>
                 <span class="promotioncode_warning"><%= manifest.promocode_error %></span>
              <% } %>
            <% } %>

          <div id="popupButtons">
              <% if(supportPromotionCode){ %><div id="paymentRedemptionButton" class="" data-down="#rightButton"><%= manifest.promocode %></div><% } %>
              <div id="leftButton" class="" data-right="#rightButton" data-up="#paymentRedemptionButton" data-down="#popupBack"><%= leftButtonText %></div>
              <div id="rightButton" class="" data-left="#leftButton" data-up="#paymentRedemptionButton" data-down="#popupBack"><%= rightButtonText %></div>
          </div>
          <div id="popupNavhelp">
            <div id="popupBack" class="button" data-up="#rightButton"><%- manifest.popup_back %></div>
          </div>
      </div>
    """

    onload: (@productData, @paymentInstrumentIds, promotionCode, @goBack) ->
      @movieTitle = "#{ @productData.title }"
      @movieTitle += " #{ @productData.quality }" if @productData.quality


      #validate if promotion code has been entered to check
      @checkValidPromotionCode = true if promotionCode?.length > 0

      @orderPaymentInstrumentIds = if @paymentInstrumentIds?.selectedId > 0 then [@paymentInstrumentIds.selectedId] else []

      @promotionCodes = if promotionCode? then [promotionCode] else []
      @pricingPlanId = @productData.selectedPricingPlan.get("Id")

      @model = new CDOrderQuote({}, {
        pricingPlanId: @pricingPlanId,
        productId: @productData.productId,
        paymentInstrumentIds: @orderPaymentInstrumentIds,
        defaultPaymentInstrumentId: @paymentInstrumentIds?.defaultId,
        promotionCodes: @promotionCodes
        errorCallback: @onOrderError
      })

    template:(html, data) ->
      _.template(html)(
        data: data
        manifest: Manifest
        movieTitle: @movieTitle
        checkValidPromotionCode: @checkValidPromotionCode
        loyaltyPoints: if data.LoyaltyRewards?[0]?.Amount? and Config.enableLoyaltyPoints then Manifest.loyaltyPoints.replace("{points}", data.LoyaltyRewards[0].Amount) else ""
        orderDescription: Manifest.order_description
        leftButtonText: Manifest.popup_exit_cancel,
        rightButtonText: if @productData.isRent then Manifest.rent_button else Manifest.buy_button
        supportPromotionCode: Config.supportPromotionCode
      )

    onReturn: ->
      GATracker.trackEvent("Close Button", "Order overview")
      @goBack()

    # Return to paymentoptions when an order occurs
    onOrderError: =>
      paymentOptions = new PaymentOptions()
      paymentOptions.load
        data: [
          @productData.selectedPricingPlan.get("Currency"),
          (paymentInstrumentIds, promotionCode) =>
            @.load(
              data: [@productData, paymentInstrumentIds, promotionCode]
            )
        ]
      $(TWC.MVC.View.eventbus).trigger("loading", true)

    submitOrderAction: ->
      $(TWC.MVC.View.eventbus).trigger("loading", true)

      if @productData.productType is 4
        GATracker.trackEvent("Purchase", "Confirm Boxset purchase", @productData.selectedPricingPlan.get("Name") + " " +  @productData.quality)
      if @productData.productType is 1
        GATracker.trackEvent("Purchase", "Confirm Film purchase", @productData.selectedPricingPlan.get("Name") + " " +  @productData.quality)


      # Check if selected payment option has sufficient balance for the order, otherwise add the default payment as second payment option
      selectedPaymentInstrument = @model.get("PaymentInstruments")[0]
      if selectedPaymentInstrument.Amount < @model.get("TotalAmount")
        # Selected payment method is not sufficient
        @orderPaymentInstrumentIds.push @paymentInstrumentIds.defaultId

      CDPaymentManagement.submitOrder(@orderPaymentInstrumentIds, @pricingPlanId, @productData.productId, @promotionCodes, (error, response) =>

        # Revert session level back to 4 after completing order
        CDPaymentManagement.degradeDeviceSession =>

          if not error
            @close =>
              activePage = TWC.MVC.Router.getActivePage()
              if activePage? then activePage.refresh()

          else
            message = CDPaymentManagement.orderErrors(error)
            errorAlert = new Error()
            errorAlert.load
              data: [message]

          $(TWC.MVC.View.eventbus).trigger("loading", false)
      )

    closePopup: ->
      CDPaymentManagement.degradeDeviceSession =>
        @close()

    promotionCodeAction: ->
      if Config.supportPromotionCode
        GATracker.trackEvent("Purchase", "Apply voucher")
        promotionCodePopup = new ValidatePromotionCode()
        promotionCodePopup.setPromotionCodeComplete( (promotionCode) =>
          orderOverviewPopup = @
          orderOverviewPopup.load
            data: [@productData, @paymentInstrumentIds, promotionCode, @goBack]
            callback: =>
              $(TWC.MVC.View.eventbus).trigger("loading", false)
        )
        promotionCodePopup.load
          data: [@productData, @paymentInstrumentIds]
          callback: =>
            $(TWC.MVC.View.eventbus).trigger("loading", false)
