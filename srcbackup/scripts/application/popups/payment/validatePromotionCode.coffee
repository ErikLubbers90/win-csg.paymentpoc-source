define [
  "framework",
  "application/models/baseModel",
  "jquery",
  "application/api/authentication",
  "application/api/cd/CDSubscriberManagement",
  "application/api/cd/CDPaymentManagement",
  "application/popups/keyboard/keyboard",
  "application/popups/generic/alert",
  "application/popups/payment/orderOverview",
  "i18n!../../../lang/nls/manifest"
], (TWC, BaseModel, $, Authentication, CDSubscriberManagement, CDPaymentManagement, Keyboard, AlertMessage, OrderOverviewPopup, Manifest) ->

  class validatePromotionCodeView extends AlertMessage

    html:"""
      <div id="popupContainer" class="validatePromotionCodeContainer">
          <h1><%= manifest.promocode_title %></h1>
          <p><%= manifest.promocode_description %></p>
          <input type="text" id="promoCode" maxlength="15" data-down="#leftButton" />
          <p id="validateCodeResult"></p>
          <div id="popupButtons">
              <div id="leftButton" class="" data-up="#promoCode" data-right="#rightButton"><%= manifest.popup_ok %></div>
              <div id="rightButton" class="" data-up="#promoCode" data-left="#leftButton"><%= manifest.popup_cancel %></div>
          </div>
      </div>
    """

    keyboardId: ""

    events:
      "enter #promoCode"          : "showNumbersIME"
      "enter #leftButton"         : "leftButtonAction"
      "enter #rightButton"         : "rightButtonAction"

    manifest: Manifest

    onload: (@productData, @paymentInstrumentIds) ->
      @model = new BaseModel();

    onunload: ->
      $("#promoCode").val("")
      $("#validateDevicePinResult").html("")
      $("#pin .pinfield").removeClass "set"

    onRenderComplete: ->
      super
      TWC.MVC.Router.setFocus("#pin")
      setTimeout =>
        @showNumbersIME()
      , 500

    showNumbersIME: ->
      $("#validateDevicePinResult").html("")
      $("#promoCode").val("")
      $("#pin .pinfield").removeClass "set"


      $("#popupContainer.validateDevicePinContainer").addClass("moveUp")

      @keyboardId = "promoCode"
      @setFieldActive()

      #@param useOnlyNumbers [Bool] to activate IME with a numbers only mode
      numbersOnlyMode = true
      Keyboard.load
        data: ["promoCode"]

    setFieldActive: ->
      TWC.MVC.Router.unFocus()
      $("##{@keyboardId}").addClass("active")

    unsetFieldActive: ->
      $("##{@keyboardId}").removeClass("active")

    # Validate promotion code
    leftButtonAction: ->
      $("#validateCodeResult").html("")
      promotionCode = $("#promoCode").val()
      if promotionCode.length is 0
        $("#validateCodeResult").html(Manifest.promocode_error)
        return

      @promotionCodeCompleteCallback?(promotionCode)
      $(TWC.MVC.View.eventbus).trigger("loading", true)

    rightButtonAction: ->
      #return promotion code as null to complete check
      @promotionCodeCompleteCallback?(null)
      $(TWC.MVC.View.eventbus).trigger("loading", true)

    # Keyboard functions
    closeKeyboard: ->
      setTimeout =>
        if @keyboardId and $("##{@keyboardId}").length
          @unsetFieldActive()
          TWC.MVC.Router.setFocus("##{@keyboardId}")
          @keyboardId = ""
      ,1

    setPromotionCodeComplete: (@promotionCodeCompleteCallback) ->