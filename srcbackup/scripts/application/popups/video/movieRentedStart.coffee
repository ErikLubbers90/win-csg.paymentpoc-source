define [
  "framework",
  "../generic/alertWithOptions"
  "i18n!../../../lang/nls/manifest"
  "application/models/baseModel"
], (TWC, alertWithOptions, Manifest, baseModel) ->

  class movieRentedStartAlert extends alertWithOptions

    onload:(rentExpireTimeHrs=48, @callback=null)->
      @model = new baseModel();

      @model.set("title", Manifest.popup_rented_start_title)
      @model.set("description", Manifest.popup_rented_start_popup_description.replace("{hours}", "#{rentExpireTimeHrs}"))
      @model.set("leftButtonText", Manifest.popup_exit_cancel)
      @model.set("rightButtonText", Manifest.popup_rented_start_movie)

    leftButtonAction: ->
      GATracker.trackEvent("Playback", "Unwrap rental", "Not now")
      @close()

    rightButtonAction: ->
      $(TWC.MVC.View.eventbus).trigger("loading", true)
      GATracker.trackEvent("Playback", "Unwrap rental", "Ok")
      @close =>
        @callback?()
