define [
    "framework",
    "jquery",
    "config",
    "backbone"
    "i18n!../../../lang/nls/manifest",
    "application/models/baseModel"
    "application/api/authentication"],
(TWC, $, Config, Backbone, Manifest, baseModel, Authentication) ->

  class alert extends TWC.MVC.View
    #dom element of reference
    el: "#popup"

    type: "popup"

    # default focus
    defaultFocus: "#leftButton"

    html:"""
      <div id="popupContainer">
          <h1><%= title %></h1>
          <% if(description) { %><p><%= description %></p><% } %>
          <div id="popupButtons">
              <div id="leftButton" class="" data-focusable="true"><div class="glow"></div><%= leftButtonText %></div>
          </div>
      </div>
    """

    events:
      "enter #leftButton": "leftButtonAction"

    onload:(title, description, leftButtonText, @closeCallback = null)->
      @popupTitle = title
      @model = new baseModel();
      @model.set("title", title)
      @model.set("description", description)
      @model.set("leftButtonText", leftButtonText)

    onRenderComplete: ->
      $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])  #disable loader if it was still running e.g. during error

      if @popupTitle is "Error"
        $("#popupContainer").addClass('error')

      setTimeout () =>
        $('#popup').addClass('fadein')
      , 100

    onReturn: ->
      @close()

    closeFromError: ->
      $('#popup').removeClass('fadein')
      @unload()
      @callback?()

    close: (callback)->
      $('#popup').removeClass('fadein')
      setTimeout ()=>
        @unload()
        callback?()
        @closeCallback?()
      , 500

    leftButtonAction: ->
      @close()
