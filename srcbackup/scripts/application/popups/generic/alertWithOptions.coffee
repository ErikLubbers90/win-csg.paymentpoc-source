define [
    "framework",
    "jquery",
    "config",
    "backbone"
    "i18n!../../../lang/nls/manifest",
    "application/models/baseModel",
    "application/api/authentication"],
(TWC, $, Config, Backbone, Manifest,baseModel,  Authentication) ->
  alertWithOptionsModel = Backbone.Model.extend({
    fetch: (options) ->
      options.success()
  })

  class alertWithOptions extends TWC.MVC.View
    #dom element of reference
    el: "#popup"

    type: "popup"

    defaultFocus: "#rightButton"

    html:"""
      <div id="popupContainer">
          <h1><%= title %></h1>
          <p><%= description %></p>
          <div id="popupButtons">
              <div id="leftButton" class="" data-right="#rightButton"><div class="glow"></div><%= leftButtonText %></div>
              <% if(rightButtonText != ''){ %>
                <div id="rightButton" class="" data-left="#leftButton"><div class="glow"></div><%= rightButtonText %></div>
              <% } %>
          </div>
      </div>
    """

    events:
      "enter #leftButton": "leftButtonAction"
      "enter #rightButton": "rightButtonAction"

    onload:(title, description, rightButtonText = "", leftButtonText = "", @defaultFocus = "#rightButton")->
      @model = new baseModel();
      @model.set("title", title)
      @model.set("description", description)
      @model.set("rightButtonText", rightButtonText)
      @model.set("leftButtonText", leftButtonText)

    onRenderComplete: ->
      $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])    #disable loader if it was still running e.g. during error

      setTimeout () =>
        $('#popup').addClass('fadein')
        #Fix focus in edge cases
        setTimeout () =>
          TWC.MVC.Router.setFocus(@defaultFocus)
        , 500
      , 100

    onReturn: ->
      @close()

    closeFromError: ->
      $('#popup').removeClass('fadein')
      @unload()
      @callback?()

    close: (callback)->
      $('#popup').removeClass('fadein')
      setTimeout ()=>
        @unload()
        callback?()
      , 500

    leftButtonAction: ->

    rightButtonAction: ->
