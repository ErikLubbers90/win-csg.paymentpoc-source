define [
  "framework"
  "../generic/alertWithOptions"
  "application/api/authentication"
  "i18n!../../../lang/nls/manifest"
  "../../models/analytics/omnituretracker"
  "application/models/baseModel"
], (TWC, AlertWithOptions, Authentication, Manifest, OmnitureTracker, baseModel) ->

  class errorAlert extends AlertWithOptions

    onload: (@error) ->
      @model = new baseModel()

      @model.set("title", Manifest.popup_error_title)

      # check the error type and define the correct text and buttons
      switch @error.type
        # in case of a network error
        when "http"
          if @error.visibleDescription?
            @model.set("description", @error.visibleDescription)
          else
            @model.set("description", Manifest.popup_error_connection)

          @model.set("rightButtonText", Manifest.popup_error_retry)
          @model.set("leftButtonText", Manifest.popup_exit_close)

          @leftButtonAction = =>
            @exit()

          @rightButtonAction = =>
            @networkRetry()


        # in case of a video player error
        when "player"
          @model.set("description", Manifest.popup_error_playback)
          @model.set("leftButtonText", Manifest.popup_error_retry)
          @model.set("rightButtonText", Manifest.popup_error_return)

          @leftButtonAction = =>
            @networkRetry()

          @rightButtonAction = =>
            @historyBack()

        # in case of a general unknown error
        else
          @model.set("description", Manifest.error_message_general_error)
          @model.set("leftButtonText", Manifest.popup_error_retry)
          @model.set("rightButtonText", Manifest.popup_error_return)

          @leftButtonAction = =>
            @networkRetry()

          @rightButtonAction = =>
            @forceBack()

    onRenderComplete: ->
      ot = OmnitureTracker.get()
      ot.trackPageView("#{@error.type}.error.html")
      $("#popupContainer").addClass('error')

      setTimeout ->
        $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])
      , 500

      super

    leftButtonAction: ->


    networkRetry:->
      if TWC.MVC.History.last() is null
        @closeFromError()
        return window.location.reload()
      else
        @closeFromError()
        TWC.MVC.History.reloadLast()

    forceBack: ->
      @closeFromError()
      TWC.MVC.History.popState()

    historyBack: ->
      @close =>
        TWC.MVC.History.popState()

    onReturn: ->
      return null

    exit: ->
      $("body").trigger "exit", ['hard']
