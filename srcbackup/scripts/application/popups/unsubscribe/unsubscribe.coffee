define [
  "framework",
  "../generic/alertWithOptions"
  "application/api/authentication"
  "i18n!../../../lang/nls/manifest"
  "application/models/baseModel"
], (TWC, alertWithOptions, Authentication, Manifest, baseModel) ->

  class unSubscribeAlert extends alertWithOptions

    onload:()->
      @model = new baseModel();

      @model.set("title", Manifest.popup_unsubscribe_title)
      @model.set("description", Manifest.popup_unsubscribe_subscribed_user + " " + Authentication.getUserEmail())
      @model.set("leftButtonText", Manifest.popup_unsubscribe_cancel)
      @model.set("rightButtonText", Manifest.popup_unsubscribe_confirm)

    leftButtonAction: ->
      @close()

    rightButtonAction: ->
      $(TWC.MVC.View.eventbus).trigger("loading", true)
      Authentication.logout =>
        @close =>
          TWC.MVC.Router.load "login-view"
