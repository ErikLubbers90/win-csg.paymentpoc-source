define [
  "framework"
  "../generic/alertWithOptions"
  "application/api/authentication"
  "i18n!../../../lang/nls/manifest"
  "application/models/baseModel"
], (TWC, alertWithOptions, Authentication, Manifest, baseModel) ->

  class exitAlert extends alertWithOptions

    onload:()->
      @model = new baseModel();
      @model.set("title", Manifest.popup_exit_title)
      @model.set("description", "")
      @model.set("leftButtonText", Manifest.popup_exit_cancel)
      @model.set("rightButtonText", Manifest.popup_exit_close)

    leftButtonAction: ->

      #storefrontpage exception, trigger setFocus
      @pageSetFocus()

      if TWC.MVC.History.last() is null
        @closeFromError()
        return window.location.reload()
      else
        @close()

    onReturn: ->
      @leftButtonAction()

      #storefrontpage exception, trigger setFocus
      @pageSetFocus()

    rightButtonAction: ->
      log("exit the application")
      TWC.SDK.Utils.get().exit()
      @close()


    pageSetFocus: ->
      setTimeout () ->
        active = TWC.MVC.Router.getActive()
        if active.uid is "storefrontpage-view"
          TWC.MVC.Router.find(active.uid).setFocusToProduct()
      , 500
