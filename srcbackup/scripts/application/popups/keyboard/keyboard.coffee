define [
  "framework",
  "../../models/baseModel",
  "jquery",
  "config",
  "i18n!../../../lang/nls/regionsettings"
], (TWC , BaseModel, $, Config, RegionSettings) ->

  keyboardModel = BaseModel.extend()

  class keyboardView extends TWC.MVC.View
    #reference id, should be unique
    uid: "keyboard-view"

    el: "#ime"  #div name where keyboard should be loaded into

    type: "popup"

    loader: "keyboardLoading" #id name of loader

    defaultFocus: ""

    extras: false   #not implemented at the moment

    searchField: true  #used for hiding/showing built in input field

    defaultLayout: RegionSettings.ime_default_layout

    activeLayouts: ["abc", "qwerty"]    #active keyboards used in the app

    #dynamic elements, keys will be used as object method
    external:
      html:
        url:"skin/templates/popups/keyboard/keyboard.tpl"

    #model connected to view
    model: new keyboardModel()

    events:
      "enter  #backButton"          : "closeView"
      "enter  .character"           : "getValue"
      "enter  .numbers"             : "getValue"
      "enter  .characterExtra"      : "getValue"
      "enter  .space"               : "space"
      "enter  .caps"                : "setCaps"
      "enter  .clear"               : "clearField"
      "enter  .return"              : "backspace"
      "enter  .submit"              : "closeView"
      "enter  .switchchars"         : "changeToEntities"
      "enter  .switchlan"           : "changeLanguage"
      "enter  #num_10"              : "moveCaretLeft"
      "enter  #num_11"              : "moveCaretRight"
      "1"                           : "set1"
      "2"                           : "set2"
      "3"                           : "set3"
      "4"                           : "set4"
      "5"                           : "set5"
      "6"                           : "set6"
      "7"                           : "set7"
      "8"                           : "set8"
      "9"                           : "set9"
      "0"                           : "set0"
      "click  #closeKeyboard"       : "closeView"

    onload: (inputField, pin) ->

      #focus workaround popup over popup
      @currentPopup = TWC.MVC.Router.popup

      @numbersOnly = pin  #true for numbers only version of the keyboard
      @submitButton = "#submitRedeem"
      if !inputField #check if external inputfield is defined or not
        @input = "inputField"
        @searchField = true
      else
        @input = inputField
        @searchField = false

      if Config.platform is "samsung" and navigator.userAgent.toLowerCase().indexOf("webkit") is -1
        @maple = true

      if Config.platform in ["cordova", "playstation", "windows10"]
        @disabled = true
        $("##{@input}").focus()
        $("#keyboardScreen").hide()

    onRenderComplete: ->
      TWC.SDK.Input.get().addToBlocked("red", "green", "blue", "yellow")

      #extra check to disable ime
      if Config.platform in ["tizen", "webos", "androidtv", "sony"]
        $("##{@input}").on 'focus', (e)->
          $(this).blur()
          e.preventDefault()
          false

      $(@el).addClass('ime').show()
      setTimeout () =>
        $(@el).addClass('fadein')
      , 100
      $('.extras').hide() if not @extras
      if !@searchField
        $('#input').hide()
#        $('#keyboardScreen').css("width", "1000px")
#        $('#keyboardScreen').css("left", "140px")

      @fillCharacters()
      # Set switchlan button to next layout
      nextLayout = @getNextLayout(@defaultLayout)
      $('.switchlan').text(nextLayout)

      @showLoader()
      TWC.MVC.Router.setFocus("#character_0")
      list = $(".characters")
      input = $("##{@input}")
      input.bind 'keydown', @keyHandler
      input.bind 'keypress', @keyHandler

      if @numbersOnly
        log "deactivate non numeric characters"
        $(".character").addClass("deactivated")
        $(".button").addClass("deactivated")
        $(".character").addClass("deactivated")
        TWC.MVC.Router.setFocus("#num_1")

    showLoader: ->
      #for the show, could be a check
      $("##{@loader}").show()
      setTimeout =>
        $("##{@loader}").hide()
      , 1000

    keyHandler: (e) ->
      input = $("##{@input}")
      if Config.platform in ["windows10"]
        return true
      if Config.platform in ["tizen", "androidtv", "sony"]
        input.blur()
      e.preventDefault()
      log e.keyCode
      return

    fillCharacters: (type) ->
      @showLoader()
      $(".character").remove()
      layout = []
      #define layouts
      qwerty = ['q','w','e','r','t','y','u','i','o','p','_','a','s','d','f','g','h','j','k','l','.','?','z','x','c','v','b','n','m','@']
      abc =    ['a','b','c','d','e','f','g','h','i','j','_','k','l','m','n','o','p','q','r','s','.','?','t','u','v','w','x','y','z','@']
      qwertz = ['q','w','e','r','t','z','u','i','o','p','&#252;','a','s','d','f','g','h','j','k','l','&#246;','&#228;','y','x','c','v','b','n','m','.']
      azerty = ['a','z','e','r','t','y','u','i','o','p','q','s','d','f','g','h','j','k','l','m','w','x','c','v','b','n','.','@','/','?']
      special = ['!',':','$','%','&#710;','&#38;','*','(',')', '&#8364;', '&#163;', '&#165;','&#169;','&#174;','&#8218;', ';','+','[',']','\\','_','-','&#732;','`','|','=','.','@','/','?']
      sign = ['&#228;','&#235;','&#239;','&#246;','&#252;', '&#255;', '&#231;', '&#248;','&#254;','&#230;','&#223;', '&#224;','&#232;','&#236;','&#242;','&#249;', '&#225;','&#233;','&#237;','&#243;','&#250;', '&#226;','&#234;','&#238;','&#244;', '&#251;','.','@','/','?']

      layouts = {
        "abc": abc
        "qwerty": qwerty
        "qwertz": qwertz
        "azerty": azerty
        "special": special
        "sign": sign
      }

      layout = if layouts[type]? then layouts[type] else layouts[@defaultLayout]
      list = $(".start") #div name of where it should be inserted before
      for i, index in layout
        list.before("<li class='character' id='character_"+index+"' data-left='moveCharacterLeft' data-right='moveCharacterRight' data-up='moveCharacterUp' data-down='moveCharacterDown'>"+i+"</li>")
      @correctCaretPos()

    submitInput: ->
      TWC.MVC.Router.setFocus("#submitRedeem")
      @unload()

    getValue: (e) ->
      id = e.target.id
      check = e.target.className

      return if $("##{id}").hasClass("deactivated")

      check = check.split(" ")[1]
      id = $("##{e.target.id}").text()
      #id = e.target.innerHTML
      @fillInputField(id)

    fillInputField: (key) ->
      pos = @getCaretPos()
      input = $("##{@input}")
      chain = $("##{@input}").val()
      if @numbersOnly

        end = $("##{@input}").val().length
        firstPart = chain.substr(0, pos)
        lastPart = chain.substr(pos, end)
        if pos < end
          lastPart = lastPart.slice(1)

        newChain = firstPart + key + lastPart

        @sendValueUpdateToSearch(firstPart + key + lastPart)

        if newChain.length > 4
          return

        $("##{@input}").val(newChain)

        @resetCaretPos(pos + key.length)
        return

      end = $("##{@input}").val().length
      first = chain.substr(0, pos)
      cursorWidth = first + key
      last = chain.substr(pos, end)
      if $('.character').hasClass("uppercase")  #check if caps is on
        $("##{@input}").val(first + key.toUpperCase() + last)
      else
        $("##{@input}").val(first + key + last)

      @sendValueUpdateToSearch(first + key + last)
      @resetCaretPos(pos + key.length)
      @getPosFakeCursor(cursorWidth)

    setActiveState: (pos) ->
      $(".pinfield").removeClass("activated")
      switch pos
        when 1 then $("#one").addClass("activated")
        when 2 then $("#two").addClass("activated")
        when 3 then $("#three").addClass("activated")
        when 4 then $("#four").addClass("activated")

    changeToEntities: ->
      return if @numbersOnly
      el = $('.switchchars')
      currentLayout = $('.switchlan').text()
      if el.text() is "ABC"
        el.text("#$%")
        @fillCharacters(currentLayout)
        return
      if el.text() is "#$%"
        if @maple  #skip special, special chars do not display in input fields in maple
          el.html("ABC")
          @fillCharacters("special")
          return
        el.html("aei")
        @fillCharacters("special")
        return
      if el.html() is "aei"
        el.text("ABC")
        @fillCharacters("sign")

    getCaretPos: ->
      o = document.getElementById @input
      pos = o.selectionStart
      o.blur()
      return pos

    correctCaretPos: ->
      input = document.getElementById @input
      input.blur() if input
      if Config.platform in ["humax", "tizen", "androidtv", "sony"] #prevent IME of philips/sony popping up
        return

      input.focus() if input
      @getPosFakeCursor()

    resetCaretPos: (position) ->
      o = document.getElementById @input
      pos = position
      o.setSelectionRange pos, pos

      if @numbersOnly
        @setActiveState(pos)
      @correctCaretPos()

    moveCaretLeft: ->
      @setCaretPos('left')

    moveCaretRight: ->
      @setCaretPos('right')

    setCaretPos: (direction) ->
      o = document.getElementById @input
      pos = @getCaretPos()
      if direction is 'left'
        newPos = parseInt(pos - 1)
      if direction is 'right'
        newPos = parseInt(pos + 1)
      o.focus()
      o.setSelectionRange newPos, newPos

      if @numbersOnly
        @setActiveState(pos)
      @correctCaretPos()

    getPosFakeCursor: (string) ->
      if Config.platform in ["philips","sony"] #check platform, fake cursor not needed on samsung, lg etc
        string = "" if !string
        input = $("##{@input}")

        tmp = document.createElement("span")
        tmp.className = "input-element tmp-element"
        tmp.innerHTML = string.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;')
        document.body.appendChild(tmp)  #create div, fill it with input val to get the width, then remove it again
        spacer = $(".input-element")
        spacer.css('font-size', input.css('font-size'))
        spacer.css('font-family', input.css('font-family')) #copy styles so spacing stays correct
        spacer.css('font-weight', input.css('font-weight'))
        theWidth = tmp.getBoundingClientRect().width
        document.body.removeChild(tmp)

        return if theWidth > $("##{@input}").width()  #stop fakecursor from moving past inputfield
        adjustValueCaret =  input.width() / 2
        @placeFakeCursor(theWidth + adjustValueCaret)

    placeFakeCursor: (pos) ->
      input = document.getElementById @input
      rect = input.getBoundingClientRect()    #this only delivers proper results with no scrollbar present (viewport), and this should always be the case on smarttv
      top = (rect.top + rect.height) + 'px'
      log "rect left: ", rect.left," - offsetleft: ",  input.offsetLeft, " - top: " , input.offsetTop , " - top rect: ", rect.top
      cursorPos = parseInt(rect.left + pos) + 'px'
      $(".fakeCursor").css('left', cursorPos)
      $(".fakeCursor").css('top', top)

    changeLanguage: ->
      return if @numbersOnly
      ele = $('.switchchars')
      return if ele.text() is "ABC"
      return if ele.text() is "aei"

      layout = $('.switchlan').text()
      nextLayout = @getNextLayout(layout)
      @fillCharacters(layout)
      $('.switchlan').text(nextLayout)

    getNextLayout: (layout) ->
      indexActiveLayout = @activeLayouts.indexOf layout
      if indexActiveLayout >= 0
        if indexActiveLayout is @activeLayouts.length-1
          layout = @activeLayouts[indexActiveLayout]
          nextLayout = @activeLayouts[0]
        else if indexActiveLayout < @activeLayouts.length
          layout = @activeLayouts[indexActiveLayout]
          indexActiveLayout++
          nextLayout = @activeLayouts[indexActiveLayout]

    clearField: ->
      $("##{@input}").val('')
      return if @numbersOnly

      @correctCaretPos()
      @getPosFakeCursor()
      @fillCharacters(@defaultLayout)

    setCaps: ->
      return if @numbersOnly
      el = $('.character')
      if el.hasClass('uppercase')
        el.removeClass('uppercase')
      else
        el.addClass('uppercase')

    space: ->
      return if @numbersOnly
      pos = @getCaretPos()
      chain = $("##{@input}").val()
      end = $("##{@input}").val().length
      first = chain.substr(0, pos)
      last = chain.substr(pos, end)
      $("##{@input}").val(first+' '+last)
      @resetCaretPos(pos + 1)
      @getPosFakeCursor(first)

    backspace: ->
      return if @numbersOnly
      pos = @getCaretPos()
      chain = $("##{@input}").val()
      end = $("##{@input}").val().length
      first = chain.substr(0, pos)
      last = chain.substr(pos, end)
      first = first.slice(0, -1)
      $("##{@input}").val(first + last)
      @resetCaretPos(pos - 1)
      @getPosFakeCursor(first)
      @sendValueUpdateToSearch(first + last)

    ##character navi
    moveCharacterRight: ->
      o = document.getElementById @input
      pos = o.selectionStart
      id = TWC.MVC.Router.getFocus()
      return TWC.MVC.Router.setFocus("#button_3") if id is "#character_36"
      id = parseInt(id.split("_")[1])
      return TWC.MVC.Router.setFocus("#num_1") if id is 10
      return TWC.MVC.Router.setFocus("#num_4") if id is 21
      return TWC.MVC.Router.setFocus("#num_7") if id is 31
      return TWC.MVC.Router.setFocus("#num_10") if id is 35
      TWC.MVC.Router.setFocus("#character_" + (id + 1))
      o.setSelectionRange pos, pos
      @correctCaretPos()

    moveCharacterLeft: ->
      o = document.getElementById @input
      pos = o.selectionStart
      id = TWC.MVC.Router.getFocus()
      id = parseInt(id.split("_")[1])
      return TWC.MVC.Router.setFocus("#num_3") if id is 0
      return TWC.MVC.Router.setFocus("#num_6") if id is 11
      return TWC.MVC.Router.setFocus("#num_9") if id is 22
      return TWC.MVC.Router.setFocus("#num_12") if id is 32
      TWC.MVC.Router.setFocus("#character_" + (id - 1))
      o.setSelectionRange pos, pos
      @correctCaretPos()

    moveCharacterUp: ->
      o = document.getElementById @input
      pos = o.selectionStart
      id = TWC.MVC.Router.getFocus()
      id = parseInt(id.split("_")[1])
      return TWC.MVC.Router.setFocus("#character_22") if id is 32
      return TWC.MVC.Router.setFocus("#character_24") if id is 33
      return TWC.MVC.Router.setFocus("#character_29") if id is 34
      return TWC.MVC.Router.setFocus("#character_31") if id is 35
      if id < 10
        @closeView()
      TWC.MVC.Router.setFocus("#character_" + (id - 11))
      o.setSelectionRange pos, pos
      @correctCaretPos()

    moveCharacterDown: ->
      o = document.getElementById @input
      pos = o.selectionStart
      id = TWC.MVC.Router.getFocus()
      id = parseInt(id.split("_")[1])
      return TWC.MVC.Router.setFocus("#character_31") if id is 21
      return TWC.MVC.Router.setFocus("#character_32") if id in [22,23]
      return TWC.MVC.Router.setFocus("#character_33") if id in [24,25,26,27,28]
      return TWC.MVC.Router.setFocus("#character_34") if id in [29,30]
      return TWC.MVC.Router.setFocus("#character_35") if id is 31

      TWC.MVC.Router.setFocus("#character_" + (id + 11))
      o.setSelectionRange pos, pos
      @correctCaretPos()

    ##numpad navi
    moveNumUp: ->
      o = document.getElementById @input
      pos = o.selectionStart
      id = TWC.MVC.Router.getFocus()
      id = parseInt(id.split("_")[1])
      if id < 4
        @closeView()
      TWC.MVC.Router.setFocus("#num_" + (id - 3))
      o.setSelectionRange pos, pos
      @correctCaretPos()

    moveNumDown: ->
      o = document.getElementById @input
      pos = o.selectionStart
      id = TWC.MVC.Router.getFocus()
      id = parseInt(id.split("_")[1])
      return if id > 9
      TWC.MVC.Router.setFocus("#num_" + (id + 3))
      o.setSelectionRange pos, pos
      @correctCaretPos()

    moveNumLeft: ->
      o = document.getElementById @input
      pos = o.selectionStart
      id = TWC.MVC.Router.getFocus()
      id = parseInt(id.split("_")[1])
      return TWC.MVC.Router.setFocus("#character_10") if id is 1 and not @numbersOnly
      return TWC.MVC.Router.setFocus("#character_21") if id is 4 and not @numbersOnly
      return TWC.MVC.Router.setFocus("#character_31") if id is 7 and not @numbersOnly
      return TWC.MVC.Router.setFocus("#character_35") if id is 10 and not @numbersOnly
      #extra for blur
      TWC.MVC.Router.setFocus("#num_"+ (id - 1))
      o.setSelectionRange pos, pos
      @correctCaretPos()

    moveNumRight: ->
      o = document.getElementById @input
      pos = o.selectionStart
      id = TWC.MVC.Router.getFocus()
      id = parseInt(id.split("_")[1])
      return TWC.MVC.Router.setFocus("#character_0") if id is 3 and not @numbersOnly
      return TWC.MVC.Router.setFocus("#character_11") if id is 6 and not @numbersOnly
      return TWC.MVC.Router.setFocus("#character_22") if id is 9 and not @numbersOnly
      return TWC.MVC.Router.setFocus("#character_32") if id is 12 and not @numbersOnly
      #extra for blur
      return TWC.MVC.Router.setFocus("#num_11") if id is 10
      return TWC.MVC.Router.setFocus("#num_12") if id is 11
      TWC.MVC.Router.setFocus("#num_"+ (id + 1))
      o.setSelectionRange pos, pos
      @correctCaretPos()

    onReturn: ->
      @closeView()

    closeView: ->
      TWC.SDK.Input.get().removeFromBlocked("red", "green", "blue", "yellow")
      active = @getActive()
      if active.uid is "login-view"
        $('#loginContainer').removeClass('moveUp')
      $(@el).removeClass('fadein')
      $(@el).removeClass('ime')
      active.closeKeyboard?($("##{@input}").val())
      @unload()

    #extra navigation
    gotoKeyboard: ->
      return if !@disabled
      $("##{@input}").blur()
      TWC.MVC.Router.setFocus("#character_0")
      @correctCaretPos()

    backtoKeyboard: ->
      return if !@disabled
      TWC.MVC.Router.setFocus("#num_12")
      @correctCaretPos()

    gotoInput: ->
      @unload()

    gotoClose: ->
      TWC.MVC.Router.setFocus("#backButton")

    sendValueUpdateToSearch: (keyword) ->
      active = @getActive()
      active.valueIsChanged?(keyword)

    getActive: ->
      if @currentPopup?
        active = TWC.MVC.Router.find(@currentPopup)
      else
        active = TWC.MVC.Router.getActivePage()
      active

    unload: ->
      $("#popup").hide() if not @currentPopup?
      $(@el).hide()
      TWC.MVC.Router.popup = @currentPopup

    set1: -> @fillInputField("1")
    set2: -> @fillInputField("2")
    set3: -> @fillInputField("3")
    set4: -> @fillInputField("4")
    set5: -> @fillInputField("5")
    set6: -> @fillInputField("6")
    set7: -> @fillInputField("7")
    set8: -> @fillInputField("8")
    set9: -> @fillInputField("9")
    set0: -> @fillInputField("0")

  keyboardView.get()
