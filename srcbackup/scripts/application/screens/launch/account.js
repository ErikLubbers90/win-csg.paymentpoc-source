// Generated by CoffeeScript 1.12.4
var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

define(["framework", "config", "jquery", "backbone", "moment", "../../models/baseModel", "i18n!../../../lang/nls/manifest", "i18n!../../../lang/nls/regionsettings", "application/api/authentication", "application/api/cd/CDBaseRequest", "application/api/cd/CDSubscriberManagement", "../../popups/generic/alert", "../../models/analytics/omnituretracker"], function(TWC, Config, $, Backbone, Moment, BaseModel, Manifest, RegionSettings, Authentication, CDBaseRequest, CDSubscriberManagement, Alert, OmnitureTracker) {
  var accountModel, accountView;
  accountModel = BaseModel.extend({
    defaults: {
      "manifest": Manifest
    }
  });
  accountView = (function(superClass) {
    extend(accountView, superClass);

    function accountView() {
      return accountView.__super__.constructor.apply(this, arguments);
    }

    accountView.prototype.uid = 'account-view';

    accountView.prototype.el = "#screen";

    accountView.prototype.type = "page";

    accountView.prototype.model = new accountModel();

    accountView.prototype.external = {
      html: {
        url: "skin/templates/screens/launch/account.tpl"
      }
    };

    accountView.prototype.events = {
      "enter #returnPage": "goBack",
      "enter #browseMoviesButton": "showStorefront",
      "enter #browseLibraryButton": "showLibrary"
    };

    accountView.prototype.defaultFocus = "";

    accountView.prototype.onload = function(promo) {
      var deviceId, deviceSerial, deviceUtils;
      if (promo == null) {
        promo = false;
      }
      deviceUtils = TWC.SDK.Utils.get();
      deviceId = deviceUtils.getDeviceId();
      deviceSerial = deviceUtils.getSerial();
      this.promo = promo;
      return CDSubscriberManagement.generateDeviceRendezvousCode(deviceId, deviceSerial, (function(_this) {
        return function(error, code, deviceModel1) {
          var messageAlert;
          _this.deviceModel = deviceModel1;
          if (code) {
            _this.renderIframe(code);
            _this.checkRendezvousCode(code, _this.deviceModel);
            return code;
          }
          if (error) {
            messageAlert = new Alert();
            messageAlert.load({
              data: ["", Manifest.error_message, Manifest.ok]
            });
            $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"]);
          }
        };
      })(this));
    };

    accountView.prototype.showStorefront = function() {
      return TWC.MVC.Router.load("storefrontpage-view", {
        data: []
      });
    };

    accountView.prototype.showLibrary = function() {
      return TWC.MVC.Router.load("locker-view", {
        data: []
      });
    };

    accountView.prototype.renderSuccess = function() {
      $("#mainMenu").show();
      if (this.promo) {
        return this.goToRedemption();
      } else {
        $("#launchContent").hide();
        $("#successContent").show();
        return TWC.MVC.Router.setFocus("#browseMoviesButton");
      }
    };

    accountView.prototype.checkRendezvousCode = function(code, deviceModel) {
      return this.checkInterval = setInterval((function(_this) {
        return function() {
          return CDSubscriberManagement.verifyDeviceRendezvousCode(code, deviceModel, function(error, success, code, device) {
            if (error) {
              console.log("there was an error");
            }
            if (success) {
              clearInterval(_this.checkInterval);
              _this.checkInterval = null;
              return _this.renderSuccess();
            }
          });
        };
      })(this), 8000);
    };

    accountView.prototype.renderIframe = function(code) {
      var url;
      this.code = code;
      url = Config.iframe_account_url + "?code=" + code;
      $("#accountFrame").attr("src", url);
      return $("#mainMenu").hide();
    };

    accountView.prototype.goBack = function() {
      return TWC.MVC.History.popState();
    };

    accountView.prototype.goToRedemption = function() {
      if (Authentication.isLoggedIn()) {
        return TWC.MVC.Router.load("redemption-view", {
          data: []
        });
      }
    };

    return accountView;

  })(TWC.MVC.View);
  return accountView.get();
});
