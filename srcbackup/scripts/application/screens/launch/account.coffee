define [
  "framework",
  "config",
  "jquery",
  "backbone",
  "moment",
  "../../models/baseModel",
  "i18n!../../../lang/nls/manifest",
  "i18n!../../../lang/nls/regionsettings",
  "application/api/authentication",
  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDSubscriberManagement",
  "../../popups/generic/alert",
  "../../models/analytics/omnituretracker"
], (TWC, Config, $, Backbone, Moment, BaseModel, Manifest, RegionSettings, Authentication, CDBaseRequest, CDSubscriberManagement, Alert, OmnitureTracker) ->


  accountModel = BaseModel.extend
    defaults:
      "manifest": Manifest,      
    
  class accountView extends TWC.MVC.View
    uid: 'account-view'

    el: "#screen"

    type: "page"

    model: new accountModel()

    external:
      html:
        url:"skin/templates/screens/launch/account.tpl"

    events:
      "enter #returnPage":   "goBack"
      "enter #browseMoviesButton" : "showStorefront"
      "enter #browseLibraryButton" : "showLibrary"


    defaultFocus: ""


    onload: (promo = false)->
      deviceUtils = TWC.SDK.Utils.get()
      deviceId =  deviceUtils.getDeviceId()
      deviceSerial = deviceUtils.getSerial()
      @promo = promo

      CDSubscriberManagement.generateDeviceRendezvousCode deviceId, deviceSerial, (error, code, @deviceModel)=>
        if code
          @renderIframe(code)
          @checkRendezvousCode(code, @deviceModel)
          return code
        if error
          messageAlert = new Alert()
          messageAlert.load
            data: ["", Manifest.error_message, Manifest.ok]
          $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])          
          return      


    showStorefront: ->
      TWC.MVC.Router.load "storefrontpage-view",
        data: []

    showLibrary: ->
      TWC.MVC.Router.load "locker-view",
        data: []     


    renderSuccess: ->
      $("#mainMenu").show()
      if @promo
        @goToRedemption()
      else
        $("#launchContent").hide()
        $("#successContent").show()
        TWC.MVC.Router.setFocus "#browseMoviesButton"


    checkRendezvousCode: (code, deviceModel) ->
      @checkInterval = setInterval ()=>
        CDSubscriberManagement.verifyDeviceRendezvousCode code, deviceModel, (error, success, code, device)=>
          if error
            #TODO handle error
            console.log "there was an error"
          if success
            clearInterval(@checkInterval)
            @checkInterval = null
            @renderSuccess()
      , 8000

    renderIframe: (code) ->
      @code = code
      url = "#{Config.iframe_account_url}?code=#{code}"
      $("#accountFrame").attr("src", url)

      $("#mainMenu").hide()

    goBack: ->
      TWC.MVC.History.popState()
      
    
    goToRedemption: ->
      if Authentication.isLoggedIn()
        TWC.MVC.Router.load "redemption-view",
          data: []  
      

    

  accountView.get()
