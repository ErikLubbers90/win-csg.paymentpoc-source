var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

define(["config", "settingPages", "framework", "application/models/baseModel", "application/api/cd/CDPaymentManagement", "application/api/cd/CDSubscriberManagement", "jquery", "i18n!../../lang/nls/manifest", "i18n!../../lang/nls/license"], function(Config, ConfigSettings, TWC, BaseModel, CDPaymentManagement, CDSubscriberManagement, $, Manifest, License) {
  var homeModel, homeView;
  homeModel = BaseModel.extend({
    defaults: {
      "manifest": Manifest,
      "settings": ConfigSettings,
      "license": License,
      "submenuItems": []
    }
  });
  homeView = (function(superClass) {
    var firstLoad, focusedInput;

    extend(homeView, superClass);

    function homeView() {
      return homeView.__super__.constructor.apply(this, arguments);
    }

    homeView.prototype.uid = 'home-view';

    homeView.prototype.el = '#screen';

    homeView.prototype.type = 'page';

    focusedInput = false;

    firstLoad = true;

    homeView.prototype.defaultFocus = "#azureToken";

    homeView.prototype.model = new homeModel();

    homeView.prototype.external = {
      html: {
        url: "skin/templates/screens/home.tpl"
      }
    };

    homeView.prototype.events = {
      "enter #leftButton": "retrieveUWPToken",
      "enter #rightButton": "getStoreId",
      "enter #submitCSGOrder": "submitCSGOrder",
      "enter #submitWindowsOrder": "submitWindowsOrder"
    };

    homeView.prototype.onload = function() {
      log("onload homeView");
      if (typeof Windows !== "undefined" && Windows !== null) {
        return this.context = Windows.Services.Store.StoreContext.getDefault();
      }
    };

    homeView.prototype.onRenderComplete = function() {
      $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"]);
      return TWC.MVC.Router.setActive(this.uid);
    };

    homeView.prototype.retrieveUWPToken = function() {
      return CDSubscriberManagement.createSession((function(_this) {
        return function() {
          return CDPaymentManagement.retrieveUwpAccessTokens(2, function(error, data) {
            if ((data != null) && (data.AccessToken != null) && (data.CollectionsToken != null)) {
              $("#azureToken").val(data.AccessToken);
              return $("#collectionsToken").val(data.CollectionsToken);
            }
          });
        };
      })(this));
    };

    homeView.prototype.getStoreId = function() {
      var azureToken;
      $("#azureError").text("");
      azureToken = $("#azureToken").val();
      if (azureToken === "") {
        azureToken = null;
      }
      if (this.context != null) {
        return this.context.getCustomerCollectionsIdAsync(azureToken, publisherUserID).done(function(result) {
          log(result);
          $("#storeid").text(result);
          this.storeId = result;
          if ((result != null) || result === "") {
            return $("#azureError").text("Something went wrong with getting the store Id");
          }
        });
      }
    };

    homeView.prototype.submitCSGOrder = function() {};

    homeView.prototype.submitWindowsOrder = function() {
      var productStoreId;
      $("#windowsMessage").text("");
      $("#windowsError").text("");
      productStoreId = $("#productStoreID").val();
      if ((productStoreId != null) && (this.context != null)) {
        return this.context.requestPurchaseAsync(productStoreId).done(function(result) {
          var extendedError;
          extendedError = "";
          if (result.extendedError) {
            if (result.extendedError === (0x803f6107 | 0)) {
              extendedError = "This sample has not been properly configured.";
            } else {
              extendedError = "Error while making purchase: " + result.extendedError;
            }
          }
          switch (result.status) {
            case Windows.Services.Store.StorePurchaseStatus.alreadyPurchased:
              return $("#windowsMessage").text('You already bought this AddOn.');
            case Windows.Services.Store.StorePurchaseStatus.succeeded:
              return $("#windowsMessage").text('You bought ' + item.title);
            case Windows.Services.Store.StorePurchaseStatus.notPurchased:
              return $("#windowsError").text("Product was not purchased, it may have been canceled. ExtendedError: " + extendedError);
            case Windows.Services.Store.StorePurchaseStatus.networkError:
              return $("#windowsError").text("Product was not purchased due to a network error. ExtendedError: " + extendedError);
            case Windows.Services.Store.StorePurchaseStatus.serverError:
              return $("#windowsError").text("Product was not purchased due to a server error. ExtendedError: " + extendedError);
            default:
              $("#windowsError").text("Product was not purchased due to an unknown error. ExtendedError: " + extendedError);
              break;
          }
        });
      }
    };

    return homeView;

  })(TWC.MVC.View);
  return homeView.get();
});
