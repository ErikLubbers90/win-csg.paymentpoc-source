define [
  "framework",
  "jquery",
  "settingPages",
  "config",
  "../../models/baseModel",
  "application/api/cd/models/CDStoreFrontPage",
  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDSubscriberManagement",
  "application/api/cd/CDPaymentManagement",
  "application/api/authentication",
  "i18n!../../../lang/nls/manifest",
  "i18n!../../../lang/nls/regionsettings",
  "../../popups/generic/alert"
], (TWC, $, ConfigSettings, Config, BaseModel, CDStoreFrontPageModel, CDBaseRequest, CDSubscriberManagement, CDPaymentManagement, Authentication, Manifest, RegionSettings, Alert) ->

  pinModel = BaseModel.extend
    defaults:
      "manifest": Manifest,

  class pinView extends TWC.MVC.View
    uid: 'pin-view'

    el: "#screen"

    type: 'page'

    model: new pinModel()

    #page:
    #setPin - set new pin
    #usePin - confirm if use wants to use pin
    #paymentError - show error if user does not have associated payment
    #passwordConfirmation - confirm password on pin change
    page: "usePin"

    maxPinLength: 4

    external:
      html:
        url:"skin/templates/screens/login/pin.tpl"

    events:
      "enter #setPinButton": "setPin"
      "enter #confirmButton": "confirmPin"
      "enter #skipButton": "skipPin"
      "enter #changePinButton": "changePin"
      "enter #closeButton": "closePage"
      "enter #screenCloseButton": "closePage"
      "enter #passwordCancelButton": "cancelPinConfirm"
      "enter #passwordChangePinButton": "confirmPinChange"
      "enter #passwordReminder": "passwordReminder"

      "enter #helpButton" : "goToHelp"

      "enter #nextButton": "next"
      "enter #forgotButton": "forgotPin"
      "enter #backButton": "back"
      "enter #resetPin": "resetPin"


    zones: [
      {uid:"menu-zone", data:[]},
      {uid:"keyboard-zone", data:["pinNumber", true]}
    ]

    onload: (@name, @password, @page, @loginCallback, @failCallback = null) ->
      @model.set("fullscreen", true)


    onRenderComplete: ->
      @renderPage(@page)
      $("#mainMenu").hide()

      $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])



    onZoneRenderComplete: ->
      switch @page
        when "usePin"
          $("#ime").hide()
          TWC.MVC.Router.setActive "pin-view"
#          TWC.MVC.Router.setFocus "#skipButton"
          TWC.MVC.Router.setFocus "#confirmButton"
        when "setPin"
          TWC.MVC.Router.setActive "keyboard-zone"
          TWC.MVC.Router.setFocus "#num_5"
        when "paymentError"
          $("#ime").hide()
          TWC.MVC.Router.setActive "pin-view"
          TWC.MVC.Router.setFocus "#closeButton"
        when "confirmPassword"
          $("#inputField").focus()
        when "purchasePin", "updatePaymentInstrumentPin"
          TWC.MVC.Router.setActive "keyboard-zone"
          TWC.MVC.Router.setFocus "#num_5"
        when "purchaseSetPin"
          TWC.MVC.Router.setActive "keyboard-zone"
          TWC.MVC.Router.setFocus "#num_5"
        when "purchaseConfirmPassword"
          $("#inputField").focus()
        when "resetPinPassword"
          $("#inputField").focus()

    setCaretPos: (@caretPos)->
      @caretPos = 0 if @caretPos < 0
      $("#pin .pinfield.active").removeClass('active')
      $("#pin .pinfield").eq(@caretPos).addClass "active"


    valueIsChanged: (keyword) ->
      if @zones[1].data[0] is "pinNumber"
        newPos = @caretPos+1
        @setCaretPos(newPos)
        for i in [0..4]
          break if keyword.length <= i
          $("#pin .pinfield").eq(i).text(keyword.charAt(i))

      if @page is "setPin"
        if keyword.length >= @maxPinLength
          $("#setPinButton").removeClass("inactive")
          if isNaN(@caretPos) or @caretPos >= @maxPinLength
            TWC.MVC.Router.setActive "pin-view"
            TWC.MVC.Router.setFocus "#setPinButton"
        else
          $("#setPinButton").addClass("inactive")
      else if @page in ["purchasePin", "purchaseSetPin", "updatePaymentInstrumentPin"]
        if keyword.length >= @maxPinLength
          $("#nextButton").removeClass("inactive")
          if isNaN(@caretPos) or @caretPos >= @maxPinLength
            TWC.MVC.Router.setActive "pin-view"
            TWC.MVC.Router.setFocus "#nextButton"
        else
          $("#nextButton").addClass("inactive")


    renderPage: (@page)->
      log "Pin page rendered: ", @page
      if @page is "usePin"
        $(".pinContainer").removeClass("setPin").removeClass("paymentError").removeClass("confirmPassword").addClass("usePin")
        $(".title").text(Manifest.pin_welcome)
        $(".subtitle").text(Manifest.pin_confirm_subtitle)
        $("#loginResult").hide()
#        @updateButtons(["confirmButton", "changePinButton", "skipButton"])
        @updateButtons(["confirmButton", "changePinButton"])
        $("#ime").hide()
        $("#pin").hide()
#        TWC.MVC.Router.setFocus "#skipButton"
        TWC.MVC.Router.setFocus "#confirmButton"
      else if @page is "setPin"
        $(".pinContainer").removeClass("usePin").removeClass("paymentError").removeClass("confirmPassword").addClass("setPin")
        $(".title").text(Manifest.pin_confirmation)
        $(".subtitle").text(Manifest.pin_confirmation_subtitle)
        $("#loginResult").hide()
#        @updateButtons(["setPinButton", "skipButton"])
        @updateButtons(["setPinButton"])
        @zones[1].data = ["pinNumber", true]
        @refreshZones()
        $("#ime").show()
        $("#pin").show()
#        TWC.MVC.Router.setFocus "#skipButton"
        TWC.MVC.Router.setFocus "#setPinButton"
      else if @page is "paymentError"
        $(".pinContainer").removeClass("usePin").removeClass("setPin").removeClass("confirmPassword").addClass("paymentError")
        $(".title").text(Manifest.pin_payment_error)
        $(".subtitle").html(Manifest.pin_payment_error_message)
        $("#loginResult").hide()
        @updateButtons(["closeButton"])
        $("#ime").hide()
        $("#pin").hide()
        TWC.MVC.Router.setFocus "#closeButton"
      else if @page is "confirmPassword"
        $(".pinContainer").removeClass("usePin").removeClass("setPin").removeClass("paymentError").addClass("confirmPassword")
        $(".title").text(Manifest.pin_confirmation)
        $(".subtitle").html(Manifest.pin_confirmation_text)
        $("#loginResult").hide()
        @updateButtons(["passwordCancelButton", "passwordChangePinButton"])
        $("#inputField").attr("placeholder", Manifest.login_password_placeholder).val("").attr("type", "password").show()
        $("#ime").hide()
        $("#pin").hide()
      else if @page in ["purchasePin", "updatePaymentInstrumentPin"]
        title = if @page is "updatePaymentInstrumentPin" then Manifest.payment_method else Manifest.payment_purchase + " " + @name
        subtitle = if @page is "updatePaymentInstrumentPin" then Manifest.pin_enter_pin_to_set_default_payment_method else Manifest.pin_enter_pin_to_confirm
        $(".pinContainer").removeClass().addClass("pinContainer").addClass("purchase")
        $(".title").text(title)
        $(".subtitle").text(subtitle)
        $("#loginResult").hide()
        @updateButtons(["nextButton", "forgotButton", "backButton"])
        $("#nextButton").addClass("inactive")
        @zones[1].data = ["pinNumber", true]
        @refreshZones()
        @clearPin()
        $("#ime").hide()
        $("#pin").show()
        TWC.MVC.Router.setFocus "#backButton"
      else if @page is "updatePaymentInstrumentPin"
        $(".pinContainer").removeClass().addClass("pinContainer").addClass("purchase")
        $(".title").text(Manifest.payment_method)
        $(".subtitle").text(Manifest.pin_enter_pin_to_confirm)
        $("#loginResult").hide()
        @updateButtons(["nextButton", "forgotButton", "backButton"])
        $("#nextButton").addClass("inactive")
        @zones[1].data = ["pinNumber", true]
        @refreshZones()
        @clearPin()
        $("#ime").hide()
        $("#pin").show()
        TWC.MVC.Router.setFocus "#backButton"
      else if @page is "purchaseSetPin"
        $(".pinContainer").removeClass().addClass("pinContainer").addClass("purchase")
        $(".title").text(Manifest.pin_create_new_pin)
        $(".subtitle").text(Manifest.pin_enter_new_pin)
        $("#loginResult").hide()
        @updateButtons(["nextButton", "backButton"])
        $("#nextButton").addClass("inactive")
        @zones[1].data = ["pinNumber", true]
        @refreshZones()
        @clearPin()
        $("#ime").hide()
        $("#pin").show()
        TWC.MVC.Router.setFocus "#backButton"
      else if @page is "purchaseConfirmPassword"
        $(".pinContainer").removeClass("usePin").removeClass("setPin").removeClass("paymentError").addClass("confirmPassword")
        $(".title").text(Manifest.pin_confirmation)
        $(".subtitle").html(Manifest.pin_enter_password_to_reset_pin)
        $("#loginResult").hide()
        @updateButtons(["backButton", "nextButton"])
        $("#inputField").attr("placeholder", Manifest.login_password_placeholder).val("").attr("type", "password").show()
        $("#ime").hide()
        $("#pin").hide()
      else if @page is "resetPinPassword"
        $(".pinContainer").removeClass("usePin").removeClass("setPin").removeClass("paymentError").addClass("confirmPassword")
        $(".title").text(Manifest.pin_disable_pin)
        $(".subtitle").html(Manifest.pin_enter_password_to_disable_pin)
        $("#loginResult").hide()
        @updateButtons(["passwordReminder", "resetPin"])
        $("#inputField").attr("placeholder", Manifest.login_password_placeholder).val("").attr("type", "password").show()
        $("#ime").hide()
        $("#pin").hide()
      else if @page is "reminderSent"
        $(".pinContainer").removeClass("usePin").removeClass("setPin").removeClass("confirmPassword").addClass("paymentError")
        $("#loginResult").hide()
        $(".title").html(Manifest.login_password_reminder_title)
        $(".subtitle").text(Manifest.login_password_reminder_subtitle)
        $(".code").hide()
        $("#ime").hide()
        @updateButtons(["backButton"])
        $("#inputField").hide()
        TWC.MVC.Router.setFocus "#backButton"


    clearPin: ()->
      $("#pinNumber").val("")
      $(".pinfield").text("")

    registerDevice: (devicePinEnabled)->
      deviceUtils = TWC.SDK.Utils.get()
      deviceId =  deviceUtils.getDeviceId()
      deviceSerial = deviceUtils.getSerial()
      deviceNickname = deviceUtils.getDeviceName(Config.platform)

      CDSubscriberManagement.registerDevice @name, @password, deviceId, deviceSerial, deviceNickname, devicePinEnabled, (error)=>
        #Error device registration
        if error
          if error.Code is 117 and error.Subcode
            message = @getSubCodeError(error.Subcode)
          else
            message = @getError(error.Code)

          messageAlert = new Alert()
          messageAlert.load
            data: [Manifest.popup_error_title, message, Manifest.popup_exit_close]
          return Authentication.logout()

        #Currency set in regionsettings
        currency = RegionSettings.default_payment_instruments_currency
        #retrieve payment instruments and show error is there isn't any
        CDPaymentManagement.retrieveWallet currency, (error, data)=>
          if not error
            @subscriberPaymentInstruments = _.filter(data.PaymentInstruments, (paymentInstrument) -> paymentInstrument.Type in Config.supportedPaymentOptions )
            if @subscriberPaymentInstruments.length is 0
              @renderPage("paymentError")
            else
              @loginCallback?()

          $(TWC.MVC.View.eventbus).trigger("loading", [false])

    confirmPinChange: ->
      @updateDevicePin ()=>
        @registerDevice(true)

    resetPin: ->
      $(TWC.MVC.View.eventbus).trigger('loading', [true])
      @password = $("#inputField").val()
      CDSubscriberManagement.updateDevicePin null, @password, (error, response) =>
        if error
          @handleLoginError(error)
          @clearPin()
          $("#inputField").val("")
        else
          @loginCallback?()

    updateDevicePin: (callback)->
      @password = $("#inputField").val()
      CDSubscriberManagement.updateDevicePin @pinNumber, @password, (error, response) =>
        if error
          @handleLoginError(error)
          @clearPin()
          $("#inputField").val("")
        else
          callback?()

    getSubCodeError: (subcodeError) ->
      switch subcodeError
      #specify subcodes related to login errors
        when 11701 then message = Manifest.error_message_invalid_login #AccessDeniedLoginInvalid
        when 11702 then message = Manifest.login_revoked_popup #AccessDeniedLoginRevoked
        when 11703 then message = Manifest.invalid_password_entered #AccessDeniedInvalidPassword
        when 11704 then message = Manifest.login_session_expired #AccessDeniedInactive
        when 11705 then message = Manifest.error_household_access_denied #AccessDeniedInsufficientAccess
        when 11706 then message = Manifest.change_password_invalid_current_password #AccessDeniedInvalidPasswordChallengeResponse
        when 11707 then message = Manifest.player_content_access_denied_message #AccessDeniedBusinessUnitNotFound
        when 11708 then message = Manifest.player_content_access_denied_message #AccessDeniedDayAndTimeDenied
        when 11709 then message = Manifest.player_content_access_denied_message #AccessDeniedBusinessUnitDisabled
        when 11710 then message = Manifest.error_subscriber_not_active #AccessDeniedInsufficientSubscriberAccess
        when 11711 then message = Manifest.error_household_access_denied #AccessDeniedSubscriberInputDisabled
        when 11712 then message = Manifest.error_household_access_denied #AccessDeniedInsufficientBusinessUnitAccess
        when 11713 then message = Manifest.error_household_access_denied #AccessDeniedTokenInvalid
        when 11714 then message = Manifest.player_content_access_denied_message #AccessDeniedAnonymousProxyServerDetected
        when 11715 then message = Manifest.error_household_access_denied #AccessDeniedInvalidLocation
        when 11716 then message = Manifest.error_message_general_error #Unspecified
        when 11717 then message = Manifest.error_login_suspended_account_message #AccessDeniedTemporaryPasswordExpired
        when 11718 then message = Manifest.error_message_invalid_login #The device session promotion pin was invalid
        when 11719 then message = Manifest.error_message_invalid_login #Invalid oAuth header
        when 11720 then message = Manifest.status_pending #AccessDeniedPendingApproval
      message

    getError: (errorCode) ->
      switch errorCode
      #specify common login errors and codes related to device registration
        when 117 then message = Manifest.error_message_invalid_login #AccessDeniedLoginInvalid
        when 118 then message = Manifest.error_message_email_address_invalid #InvalidProperty
        when 804 then message = Manifest.already_logged_in #DeviceAlreadyAssociated
        when 805 then message = Manifest.error_message_device_not_available #DeviceNotAvailable
        when 806 then message = Manifest.device_not_found #DeviceNotFound
        when 810 then message = Manifest.error_message_device_not_available #InvalidPhysicalDeviceType
        when 811 then message = Manifest.max_devices_exceeded #AssociationLimitViolation
        when 812 then message = Manifest.duplicate_device_error #DuplicateDeviceNickname
        when 813 then message = Manifest.error_registering_tv #ExternalDeviceRegistrationFailure
      #global error message, error code is not defined
        else message = Manifest.error_occurred
      message

    handleLoginError: (error)->
      $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])

      #if there is a subcode defined in the error result we can determine a more accurate description
      if error.Code is 117 and error.Subcode
        message = @getSubCodeError(error.Subcode)

        $("#loginResult").html(message).show()

      else if error.Code
        message = @getError(error.Code)

        $("#loginResult").html(message).show()
      $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])


    #Cancel pin confirmation, go back to set pin page
    cancelPinConfirm: ->
      @clearPin()
      @renderPage("setPin")


    closePage: ->

      #verify if user has added a payment method in between screens
      if @page is "paymentError"
        #Currency set in regionsettings
        currency = RegionSettings.default_payment_instruments_currency
        #retrieve payment instruments and show error is there isn't any
        CDPaymentManagement.retrieveWallet currency, (error, data)=>
          if not error
            @subscriberPaymentInstruments = _.filter(data.PaymentInstruments, (paymentInstrument) -> paymentInstrument.Type in Config.supportedPaymentOptions )
            if @subscriberPaymentInstruments.length is 0
              TWC.MVC.Router.load "settings-view",
                data: [3]
            else
              if @failCallback?
                @failCallback()
      else
        if @failCallback?
          @failCallback()
        else
          #No payment method, go to settings
          TWC.MVC.Router.load "settings-view",
            data: [3]

    back: ->
      if @failCallback?
        return @failCallback()

      if @page is "purchasePin"
        TWC.MVC.History.popState()
      else if @page is "updatePaymentInstrumentPin"
        TWC.MVC.Router.load "settings-view",
          data: [3]
      else if @page is "purchaseSetPin"
        @renderPage("purchasePin")
      else if @page is "purchaseConfirmPassword"
        @renderPage "purchaseSetPin"
      else if @page is "reminderSent"
        @renderPage "resetPinPassword"

    next: ->
      @pinNumber = $("#pinNumber").val() if @page isnt "purchaseConfirmPassword"
      return if @pinNumber.length isnt 4

      if @page in ["purchasePin", "updatePaymentInstrumentPin"]
        @promoteDeviceSession()
      else if @page is "purchaseSetPin"
        @renderPage("purchaseConfirmPassword")
      else if @page is "purchaseConfirmPassword"
        @updateDevicePin ()=>

          # When update device pin call completed the new PIN has not been set yet in the backend
          # Workaround by waiting some time for the new PIN to be saved in the backend
          setTimeout =>
            @promoteDeviceSession()
          , 3000

    promoteDeviceSession: ->
      CDSubscriberManagement.promoteDeviceSession(@pinNumber, (error, response) =>
        if not error
          # Current session has been converted to a fully authenticated session (Auth Type 5)
          #overwrite the authenticated session id with a new one returned from the call
          CDBaseRequest.sessionId = response.SessionId if response?.SessionId
          @loginCallback?()

        else
          # Something went wrong, pincode incorrect?
          @clearPin()
          $("#loginResult").html(Manifest.validate_device_pin_error).show()
          $(TWC.MVC.View.eventbus).trigger("loading", false)
      )

    setPin: ()->
      @pinNumber = $("#pinNumber").val()
      return if @pinNumber.length isnt 4
      @renderPage("confirmPassword")

    skipPin: ()->
      $(TWC.MVC.View.eventbus).trigger("loading", [true])
      @registerDevice(false)

    changePin: ()->
      @renderPage("setPin")

    forgotPin: ()->
      @renderPage("purchaseSetPin")

    confirmPin: ()->
      $(TWC.MVC.View.eventbus).trigger("loading", [true])
      @registerDevice(true)

    passwordReminder: ()->
      @name = Authentication.getUserEmail()
      CDSubscriberManagement.generatePassword @name, (error, status, response)=>
      if error?
        #TODO handle error
        log("error", error)
        return
      @renderPage("reminderSent")



    updateButtons: (visibleButtons)->
      $(".buttons .button").addClass("disable")
      for e in visibleButtons
        $("##{e}").removeClass("disable")

    #Focus events

    focusUp: ->
      TWC.MVC.Router.find("keyboard-zone").focusFromBottom()

    focusDown: ->
      if @page is "setPin"
        @focusKeyboard()
      if @page is "paymentError"
        @focusHelpButton()

    focusLeft: ->
      focusEl = TWC.MVC.Router.getFocus()
      if focusEl is "#skipButton"
        if @page is "usePin"
          TWC.MVC.Router.setFocus "#changePinButton"
        else if @page is "setPin"
          if $("#setPinButton").hasClass("inactive")
            @focusKeyboard()
          else
            TWC.MVC.Router.setFocus "#setPinButton"
      else if focusEl is "#forgotButton"
        if $("#nextButton").hasClass("inactive")
          @focusKeyboard()
        else
          TWC.MVC.Router.setFocus "#nextButton"
      else if focusEl is "#backButton"
        if @page is "purchaseSetPin"
          if $("#nextButton").hasClass("inactive")
            @focusKeyboard()
          else
            TWC.MVC.Router.setFocus "#nextButton"
        else if @page is "purchaseConfirmPassword"
          TWC.MVC.Router.setFocus "#nextButton"
        else
          TWC.MVC.Router.setFocus "#forgotButton"

    focusRight: ->
      focusEl = TWC.MVC.Router.getFocus()
      if focusEl is "#nextButton"
        if @page in ["purchaseSetPin", "purchaseConfirmPassword"]
          TWC.MVC.Router.setFocus "#backButton"
        else
          TWC.MVC.Router.setFocus "#forgotButton"

    focusHelpButton: ->
      TWC.MVC.Router.setFocus "#helpButton"

    focusKeyboard: ->
      if @page in ["setPin", "purchasePin", "purchaseSetPin", "updatePaymentInstrumentPin"]
        TWC.MVC.Router.setActive "keyboard-zone"
        TWC.MVC.Router.setFocus "#num_3"
      else
        $("#inputField").focus()

    focusHelpUp: ->
      if @page is "paymentError"
        TWC.MVC.Router.setFocus "#closeButton"
      else
        TWC.MVC.Router.setFocus "#continueButton"

    goToHelp: ->
      TWC.MVC.Router.load "settings-view",
        data: [0]

    focusUpFromKeyboard: ->
      if @page is "setPin"
        TWC.MVC.Router.setActive "pin-view"
        if $("#setPinButton").hasClass("inactive")
          TWC.MVC.Router.setFocus "#skipButton"
        else
          TWC.MVC.Router.setFocus "#setPinButton"

    focusRightFromKeyboard: ->
      if @page is "setPin"
        TWC.MVC.Router.setActive "pin-view"
        if $("#setPinButton").hasClass("inactive")
          TWC.MVC.Router.setFocus "#skipButton"
        else
          TWC.MVC.Router.setFocus "#setPinButton"
      else if @page in ["purchasePin", "updatePaymentInstrumentPin"]
        TWC.MVC.Router.setActive "pin-view"
        if $("#nextButton").hasClass("inactive")
          TWC.MVC.Router.setFocus "#forgotButton"
        else
          TWC.MVC.Router.setFocus "#nextButton"
      else if @page is "purchaseSetPin"
        TWC.MVC.Router.setActive "pin-view"
        if $("#nextButton").hasClass("inactive")
          TWC.MVC.Router.setFocus "#backButton"
        else
          TWC.MVC.Router.setFocus "#nextButton"

    focusDownFromKeyboard: ->
      if @page is "confirmPassword"
        TWC.MVC.Router.setActive "pin-view"
        TWC.MVC.Router.setFocus "#passwordChangePinButton"
      else if @page is "purchaseConfirmPassword"
        TWC.MVC.Router.setActive "pin-view"
        TWC.MVC.Router.setFocus "#nextButton"
      else if @page is "resetPinPassword"
        TWC.MVC.Router.setActive "pin-view"
        TWC.MVC.Router.setFocus "#resetPin"



    moveToContent: ->
      if @page is "usePin"
        TWC.MVC.Router.setActive "pin-view"
        TWC.MVC.Router.setFocus "#confirmButton"
      else if @page is "setPin"
        TWC.MVC.Router.setActive "keyboard-zone"
        TWC.MVC.Router.setFocus "#num_1"
      else if @page is "paymentError"
        TWC.MVC.Router.setActive "pin-view"
        TWC.MVC.Router.setFocus "#cancelButton"
      else if @page in ["confirmPassword", "purchaseConfirmPassword", "resetPinPassword"]
        $("#inputField").focus()
      else if @page is "paymentError"
        TWC.MVC.Router.setActive "pin-view"
        TWC.MVC.Router.setFocus "#closeButton"




  pinView.get()

