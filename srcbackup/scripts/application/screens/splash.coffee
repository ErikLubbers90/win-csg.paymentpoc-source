define [
  "framework"
  "config",
  "application/screens/loader",
  "jquery"
], (TWC, Config, Loader, $) ->

  # **splash_init** (class) 
  class Splash extends TWC.SDK.Base
    # **constructor** (function)
    #the below function will execute for all platforms on initialization of the application

    timer : null
    index :0
    forceShow : false

    # Spinner speed
    frames : 3
    timeout : 200

    constructor: ->
      log "SPLASH: init"
      #enable splash screen
      @splashscreen(true)

      #bind the splash event
      $(TWC.MVC.View.eventbus).on "splashscreen", (e, bool) =>
        @splash_event = not bool
        @status()

    # **splash_event** (bool)
    #keeps track of the splash event, when event is fired switches to true
    splash_event: false

    # **delay** (bool)
    #when delay is enabled, splash will not hide before minimum splash time as set in config
    #even if splash false event is fired
    delay: true

    # **status** (function)
    #function to check if splashscreen can be disabled vased on paramters
    status: (force=false)->
      @splashscreen(false) if (@active and @splash_event and not @delay) or (@active and force)

    # **splashscreen** (function)
    #enable/disbale function for the splashscreen
    splashscreen: (@active) ->
      if @active
        #set timeout for hiding splashscreen after maximum timeout
        @max_timer = setTimeout(( => @status(true); $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"]) ), Config.splash.maxTime)

        #set timeout for hiding splashscreen after minimum timeout
        @min_timer = setTimeout(( => @delay = false; @status()), Config.splash.minTime)

        $("#loader").addClass("splash")

        #trigger a forced loading, loading will not be hidden untill "reset" call
        $(TWC.MVC.View.eventbus).trigger("loading", [true, "keep"])

        #bind splash video events
        $("#splashPlayer").on "oncanplay", ->
          $(this).poster.style.visibility = 'visible'

        $("#splashPlayer").on "onloadstart", ->
          $(this).poster.style.visibility = 'hidden'

        $("#splashPlayer").on "ended", ->
          $(this)[0].pause()

        log "SPLASH: show"

      else
        # clear delay timers
        clearTimeout(@max_timer)
        clearTimeout(@min_timer)

        # reset loader to normal behavior
        $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])

        $("#loader").removeClass("splash")

        #ide splash screen
        $("#splash").hide()

        $("#splashPlayer").off()

        log "SPLASH: hide"

  Splash.get()