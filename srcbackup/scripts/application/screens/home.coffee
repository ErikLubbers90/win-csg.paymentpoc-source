define [
  "config",
  "settingPages",
  "framework",
  "application/models/baseModel",
  "application/api/cd/CDPaymentManagement",
  "application/api/cd/CDSubscriberManagement",
  "jquery",
  "i18n!../../lang/nls/manifest",
  "i18n!../../lang/nls/license",
], (Config, ConfigSettings, TWC, BaseModel, CDPaymentManagement, CDSubscriberManagement, $, Manifest, License) ->

  homeModel = BaseModel.extend
    defaults:
      "manifest": Manifest,
      "settings": ConfigSettings,
      "license":License,
      "submenuItems": []

  class homeView extends TWC.MVC.View
    uid: 'home-view'

    el: '#screen'

    type: 'page'

    focusedInput = false

    firstLoad = true

    defaultFocus: "#azureToken"

    model: new homeModel()

    external:
      html:
        url:"skin/templates/screens/home.tpl"

    events:
      "enter #leftButton" : "retrieveUWPToken"
      "enter #rightButton" : "getStoreId"
      "enter #submitCSGOrder" : "submitCSGOrder"
      "enter #submitWindowsOrder" : "submitWindowsOrder"

    onload: ->
      log "onload homeView"
      if Windows?
        @context = Windows.Services.Store.StoreContext.getDefault();

    onRenderComplete: ->
      $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])    #disable loader if it was still running e.g. during error
      TWC.MVC.Router.setActive @uid

    retrieveUWPToken: ->
      CDSubscriberManagement.createSession =>
        CDPaymentManagement.retrieveUwpAccessTokens 2, (error, data)=>
          if data? and data.AccessToken? and data.CollectionsToken?
            $("#azureToken").val(data.AccessToken)
            $("#collectionsToken").val(data.CollectionsToken)

    getStoreId: ->
      $("#azureError").text("")
      azureToken = $("#azureToken").val()
      # publisherUserID = $("#publisherUserID").val()
      if azureToken == ""
        azureToken=null
      # if publisherUserID == ""
        # publisherUserID=null
      if @context?
        @context.getCustomerCollectionsIdAsync(azureToken, publisherUserID).done (result) ->
          log result
          $("#storeid").text(result)
          @storeId = result
          if result? || result==""
            $("#azureError").text("Something went wrong with getting the store Id")

    submitCSGOrder: ->

    submitWindowsOrder: ->
      $("#windowsMessage").text("")
      $("#windowsError").text("")
      productStoreId = $("#productStoreID").val()
      if productStoreId? and @context?
        @context.requestPurchaseAsync(productStoreId).done (result) ->
          extendedError = ""
          if result.extendedError
            if result.extendedError == (0x803f6107 | 0)
              extendedError = "This sample has not been properly configured."
            else
              extendedError = "Error while making purchase: " + result.extendedError
          switch result.status
            when Windows.Services.Store.StorePurchaseStatus.alreadyPurchased
              $("#windowsMessage").text('You already bought this AddOn.')
            when Windows.Services.Store.StorePurchaseStatus.succeeded
              $("#windowsMessage").text('You bought ' + item.title)
            when Windows.Services.Store.StorePurchaseStatus.notPurchased
              $("#windowsError").text("Product was not purchased, it may have been canceled. ExtendedError: " + extendedError)
            when Windows.Services.Store.StorePurchaseStatus.networkError
              $("#windowsError").text("Product was not purchased due to a network error. ExtendedError: " + extendedError)
            when Windows.Services.Store.StorePurchaseStatus.serverError
              $("#windowsError").text("Product was not purchased due to a server error. ExtendedError: " + extendedError)
            else
              $("#windowsError").text("Product was not purchased due to an unknown error. ExtendedError: " + extendedError)
              break

  homeView.get()
