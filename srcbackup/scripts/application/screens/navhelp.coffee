define [
  "framework",
  "../models/baseModel",
  "application/api/authentication",
  "i18n!../../lang/nls/manifest",
  "jquery"
], (TWC, BaseModel, Authentication, Login, Manifest, $) ->

  navHelpModel = BaseModel.extend
    defaults:
      "manifest": Manifest
      menu: []
      authenticated: false


    parseMenu: (authenticated) ->
      @.set
        menu:
          red: if authenticated then Manifest.navhelp_library else Manifest.navhelp_login
          green: Manifest.navhelp_settings
          yellow: Manifest.navhelp_films
          blue: Manifest.navhelp_search


  class navHelp extends TWC.MVC.View
    uid: 'navhelp-zone'
    
    el: "#navigationHelp"

    type: "zone"

    external:
      html:
        url:"skin/templates/screens/navhelp/navhelp.tpl"
 
    model: new navHelpModel()

    events:
      "enter #exit" : "onExit"
      "enter #back" : "onReturn"

    onload: ->
      @isLoggedIn = Authentication.isLoggedIn()
      @model.parseMenu(@isLoggedIn)

    onExit: ->
      $("body").trigger("exit",["hard"])
      
    onReturn: ->
      $("body").trigger "return"

    onRenderComplete: ->
      log "onrendercomplete"
      $("#navHelpExit").show()
      if TWC.MVC.History.size() <= 1
        $("#navHelpExit").hide()
 
    focusUp: ->
      log "Focus last selected"  #todo fix this

  navHelp.get()