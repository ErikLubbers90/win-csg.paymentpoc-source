var indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

define(["jquery", "underscore", "framework", "config", "application/popups/error/error", "i18n!../../../lang/nls/manifest", "./models/CDSubscriber", "./models/CDDevice"], function($, _, TWC, Config, Error, Manifest, CDSubscriber, CDDevice) {
  var CDBaseRequest;
  return CDBaseRequest = (function() {
    function CDBaseRequest() {}

    CDBaseRequest.endpointTemplate = _.template("<%=protocol%>://<%=subDomain%>.<%=CDDomain%><%=request%>");

    CDBaseRequest.authenticationTypeEnum = {
      NONE: 1,
      ANONYMOUS: 2,
      UNAUTHENTICATED: 3,
      DEVICE_AUTHENTICATED: 4,
      USER_AUTHENTICATED: 5
    };

    CDBaseRequest.authenticationType = CDBaseRequest.authenticationTypeEnum.NONE;

    CDBaseRequest.sessionId = null;

    CDBaseRequest.subscriber = new CDSubscriber();

    CDBaseRequest.allowPinlessPurchase = true;

    CDBaseRequest.guidanceRatingsCategories = [];

    CDBaseRequest.sessionCountry = null;

    CDBaseRequest.pingSessionInterval = 540000;

    CDBaseRequest.remainingDeviceAssociations = null;

    CDBaseRequest.device = new CDDevice();

    CDBaseRequest.request = function(type, subDomain, requestUrl, requestData, callback, sessionEnabled, timeout) {
      var country, distributionChannelId, error, key, language, ref, ref1, requestGETInfo, requestHeaders, requestInfo, requestProtocol, sessionId, url, value;
      if (sessionEnabled == null) {
        sessionEnabled = true;
      }
      if (timeout == null) {
        timeout = 30000;
      }
      error = false;
      requestHeaders = {};
      requestProtocol = "https";
      subDomain = "qa4." + subDomain;
      language = locale && (ref = locale.language, indexOf.call(Config.availableLanguages, ref) >= 0) ? locale.language : Config.defaultLanguage;
      country = locale && (ref1 = locale.country, indexOf.call(Config.availableCountries, ref1) >= 0) ? locale.country : Config.defaultCountry;
      distributionChannelId = Config.csg[Config.environment].DistributionChannelId;
      sessionId = CDBaseRequest.sessionId || "";
      if (requestUrl === "/Subscriber/CreateSession") {
        sessionId = "";
      }
      requestInfo = {
        "CD-DeviceType": Config[Config.platform].deviceType,
        "CD-SystemId": Config.csg[Config.environment].SystemId,
        "CD-DistributionChannel": _.isObject(distributionChannelId) ? distributionChannelId[Config.platform] : distributionChannelId,
        "CD-Language": language + "-" + country
      };
      requestInfo["CD-JsonLongAsString"] = true;
      if (sessionEnabled && sessionId) {
        requestInfo["CD-SessionId"] = sessionId;
      }
      switch (type) {
        case "POST":
          requestHeaders = requestInfo;
          break;
        case "GET":
          requestGETInfo = {};
          for (key in requestInfo) {
            value = requestInfo[key];
            requestGETInfo[key.replace("CD-", "CD")] = value;
          }
          requestUrl = _.template(requestUrl)(requestGETInfo);
          requestData = void 0;
      }
      url = (Config[Config.platform].http_prefix ? Config[Config.platform].http_prefix : "") + CDBaseRequest.endpointTemplate({
        protocol: requestProtocol,
        subDomain: subDomain,
        request: requestUrl,
        CDDomain: Config.csg[Config.environment].CDDomain
      });
      log("URL:", url);
      return $.ajax({
        type: type,
        url: url,
        timeout: timeout,
        data: JSON.stringify(requestData),
        headers: requestHeaders,
        success: (function(_this) {
          return function(data) {
            if ((data != null ? data.Fault : void 0) != null) {
              error = CDBaseRequest.verifyCDError(data.Fault, callback);
              if (!error.handled) {
                return typeof callback === "function" ? callback(error) : void 0;
              }
            } else {
              return typeof callback === "function" ? callback(error, data) : void 0;
            }
          };
        })(this),
        error: (function(_this) {
          return function(XHR, textStatus, error) {
            log("CDBaseRequest: network error environment: " + Config.environment + ", statuscode: " + XHR.status + ", requestUrl: " + requestUrl + ", status: " + textStatus);
            if (requestUrl === "/Subscriber/PingSession" || requestUrl === "/Subscriber/UpdateContentProgress") {
              return;
            }
            return $(TWC.MVC.View.eventbus).trigger("error", [
              {
                type: "http"
              }
            ]);
          };
        })(this)
      });
    };

    CDBaseRequest.verifyCDError = function(error, callback) {
      var CDSubscriberManagement, errorAlert, errorObj;
      log("verifyCDError: ", error);
      errorAlert = new Error();
      errorObj = false;
      error.handled = false;
      switch (error.Code) {
        case 806:
          error.handled = true;
          CDSubscriberManagement = require("application/api/cd/CDSubscriberManagement");
          CDSubscriberManagement.logout(function() {
            return typeof callback === "function" ? callback(error) : void 0;
          });
          break;
        case 109:
        case 27:
          error.handled = true;
          CDBaseRequest.sessionId = null;
          CDSubscriberManagement = require("application/api/cd/CDSubscriberManagement");
          CDSubscriberManagement.createSession(callback);
          break;
        case 20:
        case 21:
        case 22:
        case 23:
        case 26:
        case 28:
        case 104:
        case 112:
        case 920:
          errorObj = {
            type: "http",
            visibleDescription: error.Message
          };
          break;
        case 24:
          errorObj = {
            type: "http",
            visibleDescription: Manifest.tv_server_too_busy_message
          };
          break;
        case 25:
          errorObj = {
            type: "http",
            visibleDescription: Manifest.tv_server_too_busy_message
          };
          break;
        case 124:
          errorObj = {
            type: "http",
            visibleDescription: Manifest.error_household_access_denied
          };
      }
      if (errorObj) {
        errorAlert.load({
          data: [errorObj]
        });
      }
      return error;
    };

    return CDBaseRequest;

  })();
});
