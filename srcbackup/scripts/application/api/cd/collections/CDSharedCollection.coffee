define [
  "backbone",
  "underscore",
  "../CDBaseRequest"
], (Backbone, _, CDBaseRequest)->

  # General function to retrieve collection data
  {
    fetch: (options) ->
      options = if options then _.clone(options) else {}
      if (options.parse is undefined) then options.parse = true
      collection  = this
      success = options.success
      options.success = (resp) ->
        method = if options.reset then 'reset' else 'set'
        collection[method](resp, options)
        if (success) then success(collection, resp, options)
        collection.trigger('sync', collection, resp, options)

      error = options.error
      options.error = (resp) ->
        if (error) then error(collection, resp, options)
        collection.trigger('error', collection, resp, options)

      onError = false
      results = {}

      # Completion handler to be executed when all requests have been completed
      onCompleted = ->
        if(onError)
          options.error()
        else
          options.success(results)

      # Loop through all requests and store responsedata to be returned after all request are completed
      # Resultobject format: { reqName1: responseData1, reqName2: responseData2, ... }
      synchroniseRequests = _.after(_.keys(@requestObjects).length, onCompleted)
      _.each @requestObjects, (reqObject, reqName) =>
        CDBaseRequest.request reqObject.protocol, reqObject.subDomain, reqObject.requestUrl, reqObject.requestData, (error, response)=>
          if(error)
            onError = true
          else
            results[reqName] = response
            synchroniseRequests()
  }
