// Generated by CoffeeScript 1.12.3
define(["backbone", "underscore", "./CDSharedCollection", "../models/CDProduct", "../models/CDBundle"], function(Backbone, _, CDSharedCollection, CDProductModel, CDBundleModel) {
  var CDSearchSuggestionCollection;
  CDSearchSuggestionCollection = Backbone.Collection.extend({
    model: function(attr, options) {
      switch (attr.StructureType) {
        case 1:
          return new CDProductModel(attr);
          break;
        case 2:
        case 4:
          return new CDBundleModel(attr);
          break;
        default:
          return new CDProductModel(attr);
          break;
      }
    },
    requestObjects: {
      metadata: {
        protocol: "GET",
        subDomain: "metadata",
        requestBaseUrl: "/SearchSuggestions/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/IncludeProducts/True"
      }
    },
    initialize: function(attributes, options) {
      if (options.keyword) {
        return this.requestObjects.metadata.requestUrl = this.requestObjects.metadata.requestBaseUrl + "/SearchString/" + options.keyword;
      }
    },
    parse: function(response, options) {
      var results, searchSuggestionsProducts;
      searchSuggestionsProducts = response.metadata.Products;
      results = [];
      if (searchSuggestionsProducts) {
        results = searchSuggestionsProducts;
      }
      return results;
    }
  });
  _.extend(CDSearchSuggestionCollection.prototype, CDSharedCollection);
  return CDSearchSuggestionCollection;
});
