define [
  "backbone",
  "underscore",
  "../models/CDProduct",
  "./CDSharedCollection",
  "../CDBaseRequest",
], (Backbone, _, CDProductModel, CDSharedCollection, CDBaseRequest)->

  CDWishlistCollection = Backbone.Collection.extend({
    WishlistId: ""
    model: (attr,options) ->
      return new CDProductModel(attr,{reset:true, parse:false})

    requestObjects:{
      "SearchWishlistProducts":{
        protocol: "POST",
        subDomain: "services",
        requestUrl: "/Subscriber/SearchWishlistProducts",
        requestData: { }
      }
    },

    sortBy: 2,
    sortDirection: 2

    parse: (response, options)->
      results = []
      if response.SearchWishlistProducts?.WishlistProducts?
        results = (wishlistProduct.Product for wishlistProduct in response.SearchWishlistProducts.WishlistProducts)
      return results

    refresh: (callback) ->
      error = false
      wishListId = null #can not re-use id if a user logs-out and logs-in in the same tv session
      wishListName = "spsultra_wishlist"

      loadProducts = =>
        if(!wishListId)
          error = "no wishlist id defined"
          callback?(error)

        @WishlistId = wishListId
        @requestObjects.SearchWishlistProducts.requestData = {
          WishlistId: wishListId,
          IncludePurchased: true
          SortBy: @sortBy
          SortDirection: @sortDirection
        }

        model = @
        model.fetch
          reset:true,
          success: ->
            callback?(error)

      CDBaseRequest.request "POST", "services", "/Subscriber/SearchWishlists", {}, (error, response) =>
        if response and response.Wishlists?
          wishListId = wishList.Id for wishList in response.Wishlists when wishList.Name is wishListName

        if not wishListId
          CDBaseRequest.request "POST", "services", "/Subscriber/CreateWishlist", {
            "Wishlist": {
              "Comments" : wishListName, "Name" : wishListName, "Status" : 1
            }
          }, (error, create_response) =>
            if create_response and create_response.Wishlist
              wishListId = create_response.Wishlist.Id
            loadProducts()
        else
            loadProducts()

    checkIfInWishList: (productId, callback) ->
      @refresh =>
        inWishList = !!@.findWhere({ Id: productId })
        callback?(inWishList)

    addToWishList: (productId, callback) ->
        wishListId = @WishlistId
        CDBaseRequest.request "POST", "services", "/Subscriber/UpdateWishlistProducts", {
          WishlistId:wishListId
          WishlistProductsToAddOrUpdate:[{
            "ProductId":productId,
            "Status": 1,
            "Priority":1,
            "DesiredQuantity" : 1
          }]
        }, (error, response) =>
          callback?(error, response.WishlistProductsAddedOrUpdated?.length is 1)

    removeFromWishList: (productId, callback) ->
        wishListId = @WishlistId

        CDBaseRequest.request "POST", "services", "/Subscriber/SearchWishlistProducts", { WishlistId: wishListId, IncludePurchased: true}, (error, response) =>
          wishListProductId = null
          if response.WishlistProducts?
            wishListProductId = wishlistProduct.Id for wishlistProduct in response.WishlistProducts when wishlistProduct.Product.Id is productId

          CDBaseRequest.request "POST", "services", "/Subscriber/UpdateWishlistProducts", {
            WishlistId:wishListId
            WishlistProductsToRemove:[wishListProductId]
          }, (error, response) =>
            callback?(error, response.WishlistProductsAddedOrUpdated?.length is 0)

  })
  _.extend(CDWishlistCollection.prototype, CDSharedCollection)
  return new CDWishlistCollection() #make singleton