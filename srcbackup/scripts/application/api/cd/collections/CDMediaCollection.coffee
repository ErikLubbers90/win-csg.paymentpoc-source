define [
  "backbone",
  "underscore",
  "../models/CDMedia"
], (Backbone, _, CDMedia)->

  CDMediaCollection = Backbone.Collection.extend({
    model: CDMedia
  })