define [
  "backbone",
  "underscore",
  "../models/CDPlaylist"
], (Backbone, _, CDPlaylist)->

  CDPlaylistCollection = Backbone.Collection.extend({
    model: CDPlaylist
  })