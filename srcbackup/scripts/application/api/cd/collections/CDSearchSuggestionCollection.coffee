define [
  "backbone",
  "underscore",
  "./CDSharedCollection",
  "../models/CDProduct"
  "../models/CDBundle"
], (Backbone, _, CDSharedCollection, CDProductModel, CDBundleModel)->

  # Backbone model with search suggestion
  CDSearchSuggestionCollection = Backbone.Collection.extend({

      model: (attr,options) ->
        #1 - Al a Carte
        #2 - Bundle
        #3 - Picklist
        #4 - Episodic Bundle
        #5 - Series Container/Marketing Container
        switch(attr.StructureType)
          when 1      #product
            return new CDProductModel(attr)
            break
          when 2, 4  #Bundle/Episodic Bundle
            return new CDBundleModel(attr)
            break
          else
            return new CDProductModel(attr)
            break

      requestObjects:{
        metadata:{
          protocol: "GET",
          subDomain: "metadata",
          requestBaseUrl: "/SearchSuggestions/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/IncludeProducts/True"
        }
      },

      initialize: (attributes, options) ->
        if options.keyword
          @requestObjects.metadata.requestUrl = "#{@requestObjects.metadata.requestBaseUrl}/SearchString/#{options.keyword}"

      parse: (response, options)->
        searchSuggestionsProducts = response.metadata.Products

        results = []
        if searchSuggestionsProducts
          results = searchSuggestionsProducts

        return results

  })
  _.extend(CDSearchSuggestionCollection.prototype, CDSharedCollection)
  return CDSearchSuggestionCollection
