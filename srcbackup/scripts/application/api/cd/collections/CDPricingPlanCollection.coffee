define [
  "backbone",
  "underscore",
  "../models/CDPricingPlan"
], (Backbone, _, CDPricingPlan)->

  CDPricingPlanCollection = Backbone.Collection.extend({
    model: CDPricingPlan
  })