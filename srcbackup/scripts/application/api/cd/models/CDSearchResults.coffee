define [
  "backbone",
  "underscore",
  "./CDSharedModel",
  "../collections/CDProductCollection"
], (Backbone, _, CDSharedModel, CDProductCollection)->

  # Backbone model with search results
  CDSearchResultsModel = Backbone.Model.extend({
    relations: {
      "Products": CDProductCollection
    },
    defaults:{
      #"TotalPages":0
      "Products":[]
      "TotalProducts": 0
      "TotalProductsPerPage": 0
    },

    requestObjects:{
      searchResults:{
        protocol: "POST",
        subDomain: "services",
        requestUrl: "/Subscriber/SearchProducts"
        requestData: {}
      }
    },
    pageNumber:1
    pageSize:16
    pageSizeCorrection: 1
    initialize: (attributes, options) ->
      if options.pageSize? then @pageSize = options.pageSize
      if options.pageNumber? then @pageNumber = options.pageNumber
      if options.categories? then @categories = options.categories
      if options.guidanceRating? then @guidanceRating = options.guidanceRating

      @requestObjects.searchResults.requestData = {
        SearchString: if options.keyword? then options.keyword else ""
        PageSize: @pageSize
        PageNumber: @pageNumber
#            SortBy: options.sort
        Categories: @categories
        GuidanceRatings: @guidanceRating
#            People: options.people
      }

    parse: (response, options)->
      results = {}

      searchResults = response.searchResults
      #results.TotalPages = searchResults.PageCount if searchResults?.PageCount
      results.Products = searchResults.Products.slice(0,@pageSize) if searchResults?.Products
      results.TotalProducts = searchResults.RecordCount if searchResults?.RecordCount
      results.TotalProductsPerPage = @pageSize * @pageSizeCorrection

      return results
  })
  _.extend(CDSearchResultsModel.prototype, CDSharedModel)
  return CDSearchResultsModel

