define [
  "backbone",
  "framework"
], (Backbone, TWC)->

  # Backbone model with subscriber information
  CDSubscriber = Backbone.Model.extend({
    defaults: {
      Login:"",
      FirstName:"",
      MiddleName:"",
      LastName:"",
      HomeCountry:"",
      SubscriberId:"",
      SubscriberLanguage:"",
      HouseholdMemberSettings: {
        ProductSearchEnabled: true,
        PurchaseEnabled: true
      },
      LastSuccessfulLogin:"",
      TermsAndConditionsAccepted:"",
      SubscriberExternalReference:""
    },
    initialize: (attributes, options)->
      #load device data from storage if it is initialized empty
      if not attributes
        @loadFromStorage()
        #else store new data to storage
      else
        @saveToStorage()
    loadFromStorage: ->
      storage = TWC.SDK.Storage.get()
      jsonModel = storage.getItem("CDSubscriber")
      if jsonModel
        model = JSON.parse(jsonModel)
        @.set(model)
    saveToStorage: ->
      model = @.toJSON()
      jsonModel = JSON.stringify(model)
      storage = TWC.SDK.Storage.get()
      storage.setItem("CDSubscriber", jsonModel)
    clearStorage: ->
      storage = TWC.SDK.Storage.get()
      storage.removeItem("CDSubscriber")
  })