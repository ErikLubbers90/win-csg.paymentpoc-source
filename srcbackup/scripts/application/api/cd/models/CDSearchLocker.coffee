define [
  "backbone",
  "underscore",
  "moment",
  "./CDSharedModel",
  "application/api/cd/models/CDFilters",
  "application/api/cd/models/CDPricingPlan",
  "../collections/CDProductCollection"
], (Backbone, _, Moment, CDSharedModel, CDFiltersModel, CDPricingPlanModel, CDProductCollection)->

  # Backbone model with bucket information
  CDSearchLockerPageModel = Backbone.Model.extend({
    relations: {
      "Products": CDProductCollection
      "AllProducts": CDProductCollection
    },
    defaults:{
      "ContextId":"",
      "AllProducts": []
      "Products":[]
      "TotalProducts": 0
      "TotalProductsPerPage": 0
    },
    requestObjects:{
      SearchLocker: {
        protocol: "POST",
        timeout: 60000,
        subDomain: "services",
        requestUrl: "/Subscriber/SearchLocker",
        requestData: {
          PageSize: 21,
          PageNumber: 1,
          IncludeEntitlementContext: true,
          IncludeViewingContext: true,
          SortBy:2,             #sort by OrderDate
          SortDirection:2,      #order Descending (newest date first)
          StatusFilter:1,       #return active entitlements only
        }
      }
    },
    initialize: (attributes, options) ->

      if options.pageSize? then @pageSize = options.pageSize
      if options.pageNumber? then @pageNumber = options.pageNumber
      if options.categories?
        @categories = [options.categories]
      else
        @categories = []

#      if options.guidanceRating? then @guidanceRating = options.guidanceRating
      if options.sortBy? then @sortBy = options.sortBy
      if options.sortDirection? then @sortDirection = options.sortDirection
      if options.deliveryCapability? then @deliveryCapability = options.deliveryCapability
      if options.deliveryCapabilityGroup? then @deliveryCapabilityGroup = options.deliveryCapabilityGroup

      _.extend(@requestObjects.SearchLocker.requestData, {
        PageSize: @pageSize
        PageNumber: @pageNumber
        DeliveryCapability: @deliveryCapability
        LockerSource: 4
        SortBy: @sortBy
        SortDirection: @sortDirection
#        Categories:@categories
        DeliveryCapabilityGroupCode: @deliveryCapabilityGroup
#        GuidanceRatings: @guidanceRating
#            People: options.people
      })
      if @categories.length > 0
        @requestObjects.SearchLocker.requestData["Categories"] = @categories
      else
        if @requestObjects.SearchLocker.requestData["Categories"]
          delete @requestObjects.SearchLocker.requestData["Categories"]


    parse: (response, options)->
      results = {}
      products = []

      searchResults = response.SearchLocker
      lockerItems = if searchResults?.LockerItems then searchResults.LockerItems else []
      _.each lockerItems, (lockerItem)=>
        product = lockerItem.Product
        product.PricingPlan = lockerItem.PricingPlan
        product.ProductContext = lockerItem.ProductContext
#        if product.ProductContext.EntitledPricingPlanId?

        lockerItemPricingPlanModel = new CDPricingPlanModel(lockerItem.PricingPlan)

        # Get rental expirationdate and inject as property in model
        if lockerItem.ProductContext?.PurchasePolicies?
          purchasePolicies =  lockerItem.ProductContext.PurchasePolicies
          product.LicenseExpirationTimeString = ""

          for policy in purchasePolicies
            if policy.ActionCode is 20 and policy.ExpirationDate?
              remainingTimeInMin = Moment(policy.ExpirationDate).diff(Moment(), "minutes")
              product.LicenseExpirationTimeString = lockerItemPricingPlanModel.getLicenseExpirationTime(remainingTimeInMin)
              break
        else
          product.LicenseExpirationTimeString = ""

        # debug
#        product.LicenseExpirationTimeString = new CDPricingPlanModel(lockerItem.PricingPlan).getLicenseExpirationTime(Moment("2016-09-21T15:00:00.000Z").diff(Moment(), "minutes"))

        products.push(product)

      #results.TotalPages = searchResults.PageCount if searchResults?.PageCount
      results.Products = products.slice(0,@pageSize) if products.length
      results.TotalProducts = searchResults.RecordCount if searchResults?.RecordCount

      results.TotalProductsPerPage = @pageSize

      return results
  })
  _.extend(CDSearchLockerPageModel.prototype, CDSharedModel)
  return CDSearchLockerPageModel
