define [
  "config",
  "backbone",
  "underscore",
  "numeral",
  "moment",
  "i18n!../../../../lang/nls/manifest",
], (Config, Backbone, _, Numeral, Moment, Manifest)->

  CDPricingPlan = Backbone.Model.extend({
    currencyTable:Config.currencyTable,
    relations: {
      "DeliveryCapabilityActions": Backbone.Collection
      "ExternalReferences": Backbone.Collection
      "AdditionalProperties": Backbone.Collection
      "PurchaseRewards": Backbone.Collection
    },
    defaults:{
      AdditionalInformationText: ""
      ChargeAmount: 0
      Currency: ""
      Default: false
      DeliveryCapabilityActions: []
      Description: ""
      ExternalReferences: []
      Giftable: false
      LicenseFirstPlayExpirationDays: 0
      Name: ""
      PurchaseRewards: []
      RenewalChargeAmount: 0
      RequiresCoupon: false
      ShipsFree: false
      Standalone: false
      Type: 0
      UserOnly: false

      #context
      DiscountedAmount:0
    }

    getPrice: ->
      currency = @.get("Currency")
      price = @.get("DiscountedAmount")
      currencySymbol = if currency in _.keys(@currencyTable) then @currencyTable[currency] else currency
      "#{currencySymbol} #{Numeral(price).format('0,0.00')}"

    getOriginalPrice: ->
      currency = @.get("Currency")
      price = @.get("ChargeAmount")
      currencySymbol = if currency in _.keys(@currencyTable) then @currencyTable[currency] else currency
      "#{currencySymbol} #{Numeral(price).format('0,0.00')}"

    isDiscounted: ->
      return @.get("DiscountedAmount") isnt @.get("ChargeAmount")

    getLoyaltyPoints: ->
      purchaseRewardsCollection = @.get("PurchaseRewards")
      pointsModel = []
      points = purchaseRewardsCollection.find (loyaltyPointsModel) =>
        pointsModel.loyaltyPoints = loyaltyPointsModel.get("LoyaltyPointAmount")
      return points = if pointsModel.loyaltyPoints then pointsModel.loyaltyPoints else 0

    getQuality: ->
      switch @getType()
        when "BUYSD", "RENTSD"
          "SD"
        when "BUYHD", "RENTHD"
          "HD"
        when "BUYUHD", "RENTUHD"
          "UHD"
        when "BUYHDR", "RENTHDR"
          "HDR"
        else
          false

    isHDR: ->
      return @getQuality() is "HDR"

    isRental: ->
      deliveryCapabilityActions = @.get("DeliveryCapabilityActions")

      # iter delivery capability actions
      for deliveryCapabilityActionModel in deliveryCapabilityActions.models
        streamActionCode = Config[Config.platform].streamActionCode

        # continue if incorrect stream action
        if streamActionCode isnt (deliveryCapabilityActionModel.get("Code")+"")
          continue

        # Rental when MaxHoursPostInitialAccess and MaxHoursPostPurchase are set
#        if deliveryCapabilityActionModel.get("MaxHoursPostInitialAccess")? and deliveryCapabilityActionModel.get("MaxHoursPostPurchase")?
#          return true

        if _.filter(deliveryCapabilityActionModel.get("DeliveryCapabilities"), (dc) => return "#{dc.Code}" is Config.csg[Config.environment].RentalDeliveryCapabilityId).length
          return true

      # no match
      return false


    getType: ->
      isRental = @isRental()
      purchaseType = if isRental then "RENT" else "BUY"
      deliveryCapabilityActions = @.get("DeliveryCapabilityActions")

      # iter delivery capability actions
      for deliveryCapabilityActionModel in deliveryCapabilityActions.models
        streamActionCode = Config[Config.platform].streamActionCode

        # continue if incorrect stream action
        if streamActionCode isnt (deliveryCapabilityActionModel.get("Code")+"")
          continue

        # continue if no deliveryCapabilities
        deliveryCapabilities = deliveryCapabilityActionModel.get("DeliveryCapabilities")
        if not deliveryCapabilities
          continue

        # iter supported delivery capability codes in order of quality (high->low)
        for qualityType, streamCode of Config[Config.platform].deliveryCapabilityCodes

          # try to find match in pricing plan delivery capability codes
          for deliveryCapability in deliveryCapabilities

            # verify if we have a match
            if (deliveryCapability["Code"]+"") is streamCode
              return purchaseType+qualityType

      # no match return null
      return null

    equalsType: (type)->
      return @getType() is type

    isAvailable: ()->
      dateNow = Moment().valueOf()
      if @.get("AvailabilityStart")? and Moment(@.get("AvailabilityStart")).valueOf() > dateNow then return false
      if @.get("AvailabilityEnd")? and Moment(@.get("AvailabilityEnd")).valueOf() < dateNow then return false
      true

    getLicenseExpirationTime: (remainingLicenseTimeInMin) ->
      licenseExpirationTimeString = ""
      timeLabel = ""

      remainingLicenseTimeInHrs = remainingLicenseTimeInMin/60

      momentDurationObj = Moment.duration(remainingLicenseTimeInMin, "minutes")
      licenseExpirationTimeDays = parseInt(momentDurationObj.days()) + "d"
      licenseExpirationTimeHrs = parseInt(momentDurationObj.hours()) + "h"
      # if parseInt(momentDurationObj.days()) is 0
      #   licenseExpirationTimeHrs = licenseExpirationTimeHrs + "h"
      #

      licenseExpirationTimeMin = parseInt(momentDurationObj.minutes()) + "m"

      ###
      if greater than 1 day example format - 1d:13:21m LEFT
      if less than 1 day example format - 22h:21m LEFT
      ###

      if remainingLicenseTimeInMin > 0
        # Expiration time greater than 1 day
        if remainingLicenseTimeInHrs > 24
          remainingExpirationTime = "#{licenseExpirationTimeDays}:#{licenseExpirationTimeHrs}:#{licenseExpirationTimeMin}"

        # Expiration time less than 24 hrs and greater than 1 hour
        else if remainingLicenseTimeInHrs > 1
          remainingExpirationTime = "#{licenseExpirationTimeHrs}:#{licenseExpirationTimeMin}"

        # Expiration time less than 1 hour
        else
          timeLabel = " #{Manifest.license_expiration_time_min}"
          remainingExpirationTime = "#{licenseExpirationTimeMin}"

        licenseExpirationTimeString = """#{remainingExpirationTime} #{Manifest.license_expiration_time_left}"""

      return licenseExpirationTimeString

  })
