define [
  "backbone",
  "underscore"
  "../CDBaseRequest"
], (Backbone, _, CDBaseRequest)->

  # General function to retrieve model data
  {
    fetch: (options) ->

      options = if options then _.clone(options) else {}
      if (options.parse is undefined) then options.parse = true
      model   = this
      success = options.success
      options.success = (resp) ->
        if (!model.set(model.parse(resp, options), options)) then return false
        if (success) then success(model, resp, options)
        model.trigger('sync', model, resp, options)

      error = options.error
      options.error = (resp) ->
        if (error) then error(model, resp, options)
        model.trigger('error', model, resp, options)

      onError = false
      results = {}

      # Completion handler to be executed when all requests have been completed
      onCompleted = ->
        if(onError)
          options.error()
        else
          options.success(results)

      # Loop through all requests and store responsedata to be returned after all request are completed
      # Resultobject format: { reqName1: responseData1, reqName2: responseData2, ... }
      synchroniseRequests = _.after(_.keys(@requestObjects).length, onCompleted)
      _.each @requestObjects, (reqObject, reqName) =>

        CDBaseRequest.request reqObject.protocol, reqObject.subDomain, reqObject.requestUrl, reqObject.requestData, (error, response)=>
          if(error)
            onError = true
          else
            results[reqName] = response
            synchroniseRequests()
        , true, reqObject.timeout

  }