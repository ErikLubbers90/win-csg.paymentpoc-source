define [
  "config",
  "backbone",
  "underscore",
  "./CDSharedModel",
  "../collections/CDPlaylistCollection",
  "../collections/CDProductCollection"
], (Config, Backbone, _, CDSharedModel, CDPlaylistCollection, CDProductCollection)->

  # Backbone model with bucket information
  CDStoreFrontPageModel = Backbone.Model.extend({
    relations: {
      "Playlists": CDPlaylistCollection
      "FeaturedPlaylist": CDProductCollection
    },
    defaults:{
      "Name":"",
      "BackgroundImageUrl":""
      "Playlists": [],
      "TotalProductsPerPage": 6,    # Total products visible on page
      "FeaturedPlaylistLimit": 4,
      "ProductsPlaylistLimit": 54   # Max number of products visible for each playlist
    },
    requestObjects:{
      "metadata":{
        protocol: "GET",
        subDomain: "metadata",
        requestBaseUrl: "/StorefrontPage/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>",
        requestData: {}
      },
      "context":{
        protocol: "GET",
        subDomain: "services",
        requestBaseUrl: "/Subscriber/RetrieveStorefrontPageContext/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>",
        requestData: {}
      }
    },

    initialize: (attributes, options) ->
      if(options and options.subMenuId  )
        @requestObjects.metadata.requestUrl = "#{@requestObjects.metadata.requestBaseUrl}/Id/#{options.subMenuId}"
        @requestObjects.context.requestUrl = "#{@requestObjects.context.requestBaseUrl}/Id/#{options.subMenuId}"
      else
        @requestObjects.metadata.requestUrl = "#{@requestObjects.metadata.requestBaseUrl}"
        @requestObjects.context.requestUrl = "#{@requestObjects.context.requestBaseUrl}"

    parse: (response, options)->
      currentPage = response.metadata.Page
      pages = response.metadata.Pages
      products = response.metadata.Products

      # Products which are not available for the current user (and should be filtered out)
      nonQualifiedProducts = if response.context?.NonQualifiedProductIds? then response.context.NonQualifiedProductIds else []

      results = {}
      results.id = currentPage.Id
      results.Name = currentPage.Name
      results.Playlists = []
      results.FeaturedPlaylist = []
      results.SubMenuItems = pages

      buckets = currentPage.Buckets

      results.featuredItemsEnabled = true
      if currentPage.FeaturedItems? and currentPage.FeaturedItems.length > 0 and Config.enableFeaturedItems #and currentPage.Id is pages[0].Id
        featuredItems = currentPage.FeaturedItems

        resultFeatured = {}
        resultFeatured.Products = (( _.findWhere(products, {Key: featuredItem.ProductId})["Value"]) for featuredItem in featuredItems)
        results.FeaturedPlaylist = resultFeatured.Products
      else
        results.featuredItemsEnabled = false

      for bucket, index in buckets
        if index > 14 then continue #LIMIT STOREFRONT BUCKETS TO MAX 15
        resultBucket = {}
        resultBucket.Name = bucket.Name
        resultBucket.CategoryId = bucket.CategoryId if bucket.CategoryId?
        resultProducts = ( (_.findWhere(products, {Key: product})["Value"]) for product in bucket.Products when product not in nonQualifiedProducts)

        resultBucket.Products = []
        for item in resultProducts
          if results.FeaturedPlaylist.length < 4 and results.featuredItemsEnabled
            results.FeaturedPlaylist.push(item)
          else
            resultBucket.Products.push(item)

        if resultBucket.Products.length > 0
          results.Playlists.push(resultBucket)

      return results
  })
  _.extend(CDStoreFrontPageModel.prototype, CDSharedModel)
  return CDStoreFrontPageModel
