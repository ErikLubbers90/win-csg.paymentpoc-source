define [
  "backbone",
  "underscore",
  "./CDSharedModel",
  "./CDViewContent",
  "../CDBaseRequest",
  "../CDSubscriberManagement",
  "../collections/CDProductCollection",
  "../collections/CDMediaCollection",
  "../collections/CDPricingPlanCollection"
  "../collections/CDBundleProductCollection"
], (Backbone, _, CDSharedModel, CDViewContent, CDBaseRequest, CDSubscriberManagement, CDProductCollection, CDMediaCollection, CDPricingPlanCollection, CDBundleProductCollection)->

  CDSeries = Backbone.Model.extend({

    relations: {
      AvailableMedia: CDMediaCollection
      Categories: Backbone.Collection
      GuidanceRatings: Backbone.Collection
      BundleChildren : CDBundleProductCollection
    },
    defaults: {
      AdTags: ""
      AllowMultiplePurchases: false
      AvailableMedia: []
      BundleChildren: []
      Categories: []
      DetailImageUrl: ""
      Genre: ""
      GuidanceRatings: []
      ImageUrl: ""
      IndexName: ""
      LineOfBusiness: 0
      Name: ""
      PresentationTypeCode: 0
      Recommendations: []
      ReferenceDate: ""
      Standalone: true
      StructureType: 2
      Suggestions: []
      ThumbnailUrl: ""
      Previews: []

      #context
      EntitledPricingPlanId: 0
      LockerItemId: 0
      SubscriberProductId: ""
    }

    requestObjects:{
      "metadata":{
        protocol: "GET",
        subDomain: "metadata",
        requestBaseUrl: "/Product/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/Id/{seriesId}",
        requestData: {}
      },
      "context":{
        protocol: "POST",
        subDomain: "services",
        requestUrl: "/Subscriber/RetrieveProductContext",
        requestData: { }
      }
    },

    initialize: ->
      @.on('change:ImageUrl', @.updateFeaturedImages, @) #listen for changes to update featured image urls
      @updateFeaturedImages()                            #trigger initial insert manually


    parse: (response, options)->
      product = response.metadata.Product
      context = response.context.ProductContext

      result = _.clone(product)
      #parse the bundle children
      result.BundleChildren = @parseChildren(product.ReferencedProducts, product.BundleChildren)



      #remove none used fields
      delete result["ReferencedProducts"]

      return result

    parseChildren: (ReferencedProducts, BundleChildren)->
      childProducts = []

      for bundleChild in BundleChildren
        #get the bundleChild metadata and context
        childProductMetadata = _.findWhere(ReferencedProducts, {Key: bundleChild.ProductId})["Value"]

        #combine child product metadata and context
        childProduct = _.extend({}, childProductMetadata)

        #filter pricing plans down to user order-able pricing plan for child product
        childProduct.PricingPlans = bundleChild.PricingPlans

        #remove none used fields
        delete childProduct["OrderablePricingPlans"]

        childProducts.push(childProduct)

      return childProducts

    refresh: (callback) ->
      error = false
      seriesId = @.get("Id")
      if(!seriesId)
        error = "no series id defined"
        callback?(error)

      @requestObjects.metadata.requestUrl = @requestObjects.metadata.requestBaseUrl.replace("{seriesId}", seriesId)
      if CDSubscriberManagement.isLoggedIn()
        @requestObjects.context.requestData = {
          ProductId: seriesId,
#          IncludeEntitlementContext: true,
#          IncludeViewingContext: true,
#          IncludeOrderablePricingPlans: true
        }
      else
        @requestObjects.context.requestData = {
          ProductId: seriesId,
#          IncludeOrderablePricingPlans: true
        }

      model = @
      model.fetch
        reset:true,
        success: ->
          callback?(error)

    updateFeaturedImages: ()->
      imageUrl = @.get("ImageUrl")
      @.set("FeaturedImageBig", imageUrl.replace("{id}", "featured_selected"))
      @.set("FeaturedImageLandscape", imageUrl.replace("{id}", "featured_notselected"))
      @.set("FeaturedImageSmall", imageUrl.replace("{id}", "featured_secondcolumn"))
      @.set("BackgroundImage", imageUrl.replace("{id}", "background_focus"))
      @.set("BackgroundImageBlurred", imageUrl.replace("{id}", "background_blur"))




  })
  _.extend(CDSeries.prototype, CDSharedModel);
  return CDSeries