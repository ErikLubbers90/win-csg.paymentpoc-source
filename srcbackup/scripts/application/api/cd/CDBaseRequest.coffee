define [
  "jquery"
  "underscore"
  "framework"
  "config"
  "application/popups/error/error",
  "i18n!../../../lang/nls/manifest"
  "./models/CDSubscriber"
  "./models/CDDevice"
], ($, _, TWC, Config, Error, Manifest, CDSubscriber, CDDevice)->

  # abstract class to manage CD API requests
  class CDBaseRequest
    # @property [Function] template method to generate API endpoint
    @endpointTemplate: _.template("<%=protocol%>://<%=subDomain%>.<%=CDDomain%><%=request%>")

    # @property [Object] enum object of authentication types
    @authenticationTypeEnum : {NONE:1, ANONYMOUS:2, UNAUTHENTICATED:3, DEVICE_AUTHENTICATED:4, USER_AUTHENTICATED:5 }

    # @property [Integer<authenticationTypeEnum>] current authentication state
    @authenticationType: CDBaseRequest.authenticationTypeEnum.NONE

    # @property [String] current session id
    @sessionId: null

    # @property [Object<CDSubscriber>] object containing information about the current subscriber
    @subscriber : new CDSubscriber()

    # @property [Boolean] allow a registered device to make a purchase with a PIN
    @allowPinlessPurchase: true

    # @property [Array] list of subscriber preferred guidance rating categories
    @guidanceRatingsCategories: []

    # @property [String] country associated to the session
    @sessionCountry: null

    # @property [Integer] interval time to ping the server to keep the session alive
    @pingSessionInterval: 540000 #1000*60*9

    # @property [Integer] remaining devices a subscriber can register to his account
    @remainingDeviceAssociations: null

    # @property [Object<CDDevice>] object containing information about the current device
    @device: new CDDevice()

    # helper function to send request to the CD api server
    #
    # @param type [String] POST or GET request
    # @param subDomain [String] section of the api to call
    # @param requestUrl [String] request url without the domain
    # @param requestData [Object] request json object to send as POST data
    # @param callback [Function(error, response)] function called when this method is completed
    @request: (type, subDomain, requestUrl, requestData, callback, sessionEnabled = true, timeout=30000)->
      error = false
      requestHeaders = {}
      requestProtocol = "https"

      subDomain = "qa4."+subDomain

      #i18n locale plugin
      language = if locale and locale.language in Config.availableLanguages then locale.language else Config.defaultLanguage
      country = if locale and locale.country in Config.availableCountries then locale.country else Config.defaultCountry

      distributionChannelId = Config.csg[Config.environment].DistributionChannelId

      sessionId = CDBaseRequest.sessionId || ""
      sessionId = "" if requestUrl is "/Subscriber/CreateSession"

      requestInfo = {
        "CD-DeviceType": Config[Config.platform].deviceType,
        "CD-SystemId": Config.csg[Config.environment].SystemId,
        "CD-DistributionChannel": if _.isObject(distributionChannelId) then distributionChannelId[Config.platform] else distributionChannelId,
        "CD-Language": "#{language}-#{country}",
#        "CD-SessionId": CDBaseRequest.sessionId || "",
#        "CD-SubscriberId":CDBaseRequest.subscriber.get("SubscriberId")
      }
      requestInfo["CD-JsonLongAsString"] = true
      if sessionEnabled and sessionId
        requestInfo["CD-SessionId"] =  sessionId




      switch type
        when "POST"
          requestHeaders = requestInfo
        when "GET"
          requestGETInfo = {}
          requestGETInfo[key.replace("CD-", "CD")] = value for key, value of requestInfo
          requestUrl = _.template(requestUrl)(requestGETInfo)
          requestData = undefined

      url = (if Config[Config.platform].http_prefix then Config[Config.platform].http_prefix else "")+CDBaseRequest.endpointTemplate({
        protocol:requestProtocol,
        subDomain:subDomain,
        request:requestUrl,
        CDDomain:Config.csg[Config.environment].CDDomain
      })
      log "URL:" ,url
      $.ajax({
        type: type,
        url: url,
        timeout:timeout,
        data: JSON.stringify(requestData),
        headers:requestHeaders,
        success: (data)=>
          if data?.Fault?
            error = CDBaseRequest.verifyCDError(data.Fault, callback)
            if not error.handled then callback?(error)
          else
            callback?(error, data)

        error: (XHR, textStatus, error) =>
          log("CDBaseRequest: network error environment: #{Config.environment}, statuscode: #{XHR.status}, requestUrl: #{requestUrl}, status: #{textStatus}")

          #do not trigger error on ping session requests or content progress
          return if requestUrl in ["/Subscriber/PingSession" ,"/Subscriber/UpdateContentProgress"]


          $(TWC.MVC.View.eventbus).trigger("error", [{type:"http"}])
      })

    # helper function to verify an error returned from the API
    #
    # @param data [Object] response object from api with error
    # @return [string] string of the error occured
    @verifyCDError: (error, callback)->
      log("verifyCDError: ", error)

      errorAlert = new Error()
      errorObj = false
      error.handled = false
      switch error.Code
        when 806  #DeviceNotFound
          error.handled = true
          CDSubscriberManagement = require("application/api/cd/CDSubscriberManagement");
          CDSubscriberManagement.logout ->
            callback?(error)
        when 109, 27 #SessionExpired, No Session
          error.handled = true
          CDBaseRequest.sessionId = null
          CDSubscriberManagement = require("application/api/cd/CDSubscriberManagement");
          CDSubscriberManagement.createSession(callback)
        #specify common error codes and alert the message
        when 20, 21, 22, 23, 26, 28, 104, 112, 920 #No manifest definition, show english description
          errorObj =
            type: "http"
            visibleDescription: error.Message
        when 24 #SystemBusy
          errorObj =
            type: "http"
            visibleDescription: Manifest.tv_server_too_busy_message
        when 25 #SystemBusySessionLimit
          errorObj =
            type: "http"
            visibleDescription: Manifest.tv_server_too_busy_message
        when 124 #DateAndTimeDenied
          errorObj =
            type: "http"
            visibleDescription: Manifest.error_household_access_denied

      if errorObj
        errorAlert.load
          data: [errorObj]

      return error
