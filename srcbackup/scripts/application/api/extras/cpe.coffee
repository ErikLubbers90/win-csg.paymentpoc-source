define (require, exports, module) ->

  jschannel = require("vendor/jschannel/jschannel")
  $ = require("jquery")

  TWC = require("framework")
  Config = require("config")
  authentication = require("../authentication")
  CDProduct = require("application/api/cd/models/CDProduct")

  class CPE_package
    @channel = null
    @$iframe = null
    @contentId = null
    @productId = null
    @locale = "en"


    constructor: (@contentId, @productId, @locale = "en") ->

    ###
      private methods
    ###
    _process_channel_promise: (trans, promise)->
      promise.done ()->
        log "[CHANNEL] promise done", [arguments...]
        trans.complete([arguments...])
      promise.fail ()->
        log "[CHANNEL] promise fail", [arguments...]
        trans.error([arguments...])
      trans.delayReturn(true)

    _addListeners: ->

      @channel.bind "account.signIn", (trans) =>
        log("[CHANNEL] account.signIn")
        promise = @account_signIn()
        @_process_channel_promise(trans, promise)

      @channel.bind "account.signOut", (trans) =>
        log("[CHANNEL] account.signOut")
        promise = @account_signOut()
        @_process_channel_promise(trans, promise)

      @channel.bind "account.isSignedIn", (trans) =>
        log("[CHANNEL] account.isSignedIn")
        promise = @account_isSignedIn()
        @_process_channel_promise(trans, promise)

      @channel.bind "content.getAvailability", (trans) =>
        log("[CHANNEL] content.getAvailability")
        promise = @content_getAvailability()
        @_process_channel_promise(trans, promise)

      @channel.bind "content.acquire", (trans) =>
        log("[CHANNEL] content.acquire")
        promise = @content_acquire()
        @_process_channel_promise(trans, promise)

      @channel.bind "player.initialise", (trans, params) =>
        log("[CHANNEL] player.initialise")
        promise = @player_initialise(params)
        @_process_channel_promise(trans, promise)

      @channel.bind "package.enabled", (trans) =>
        log("[CHANNEL] package.enabled")
        @packageEnabled = true
        if not @isHidden
          @_show()
        else
          # CPE iframe is hidden, disable it
          setTimeout () =>
            @disable()
          , 1000
#          @enableCallback()
          $(TWC.MVC.View.eventbus).trigger('loading', false)
        return true

      @channel.bind "package.disabled", (trans) =>
        @packageEnabled = false
        log("[CHANNEL] package.disabled")
        @_hide()
        return true

      @channel.bind "package.terminated", (trans) =>
        log("[CHANNEL] package.terminated")
        trans.complete()
        @_destroy()

      @channel.bind "player.play", (trans) =>
        log("[CHANNEL] player.play")
        @player_play()
        return true

      @channel.bind "player.play_pause", (trans) =>
        log("[CHANNEL] player.play_pause")
        @player_playPause()
        return true

      @channel.bind "player.pause", (trans) =>
        log("[CHANNEL] player.pause")
        @player_pause()
        return true

      @channel.bind "player.stop", (trans) =>
        log("[CHANNEL] player.stop")
        @player_stop()
        return true

      @channel.bind "player.setVisibleControls", (trans, params) =>
        log("[CHANNEL] player.setVisibleControls")
        @player_setVisibleControls(params)
        return true

      @channel.bind "player.jumpTo", (trans, params) =>
        log("[CHANNEL] player.jumpTo")
        @player_jumpTo(params)
        return true

      # show controls when page is visible again (restored)
      $(document).on 'visibilitychange.cpe', =>
        if document.hidden
          log "CPE: Suspend"
          if @packageEnabled
            @keepOn = true
            @disable()
        else
          log "CPE: Restore"
          if @keepOn
            @enable()
            @keepOn = false

    _addHistoryListeners: ->
      @_removeHistoryListeners()
      $(TWC.MVC.View.eventbus).trigger('returnhandling', [false]) #remove history listener on the sps app and bind it to cpe
      $(document.body).on "return.cpehistory", =>
        return if not @channel?
        @channel.call
          method:"package.backButton",
          params: {},
          success: ->
          error: ->

    _removeHistoryListeners: ->
      $(TWC.MVC.View.eventbus).trigger('returnhandling', [true]) #re-add history listener on the sps app and remove it from cpe
      $(document.body).off('.cpehistory')

    _removeListeners: ->
      $(document).off('.cpe')

    _show: (callback)->
      $(TWC.MVC.View.eventbus).trigger('loading', false)
      @$iframe[0].contentWindow.focus() #set focus to CPE
      @$iframe.css({"visibility":"visible"})
      callback?()

    _hide: (callback)->
      window.focus()                    #set focus to app
      @$iframe.css({"visibility":"hidden"})
      callback?()

    _destroy: (callback)->
      if @$iframe
        @$iframe.remove()
      if @channel
        @channel.destroy()
      window.focus()
      @_removeHistoryListeners()
      @_removeListeners()
      @channel = null
      @$iframe = null
      callback?()

    ###
     management methods
    ###
    initialize: (callback, @isHidden = false) ->
      $(TWC.MVC.View.eventbus).trigger('loading', true)
      @_destroy()
      @$iframe = $("<iframe>")
      @$iframe.addClass("cpe")
      @$iframe.one "load", =>
        @_addHistoryListeners()
        @channel = jschannel.build
          window: @$iframe[0].contentWindow,
          origin: "*",
          scope: "sps-ultra",
          onReady: =>
            @channel.call
              method:"package.initialize",
              params: { contentId:@contentId, locale:@locale, environment: Config.environment},
              success: =>
                @_addListeners()
                callback?()
              error: ->
      if Config.platform is "core"
        @$iframe.attr({"src":"extras/dist/index.html"})
      else
        @$iframe.attr({"src":"ms-appx-web:///src/extras/dist/index.html"})

      @$iframe.appendTo(window.document.body)

    getMetadata: (callback)->
      @initialize(null, true)
      @enableCallback = ()=>
        setTimeout ()=>
          @disable()
          @player_getMetadata (metadata)=>
            callback?(metadata)
            @terminate()
        , 2000

    enable: (callback)->
      return if not @channel?
      @channel.call
        method:"package.enable",
        success: ->
          callback?()
        error: ->
      @_show()
      @_addHistoryListeners()

    disable: (callback)->
      return if not @channel?
      @channel.call
        method:"package.disable",
        success: ->
          callback?()
        error: ->
      @_hide()
      @_removeHistoryListeners()

    terminate: (callback)->
      return if not @channel?
      @channel.call
        method:"package.terminate",
        success: ->
          @_destroy()
        error: ->

    ###
      callback methods
    ###
    account_signIn: ->
      deferred = $.Deferred()
      #TODO link to app
      historySize = TWC.MVC.History.size()
      TWC.MVC.Router.load "login-view",
        data: [
          ()=>
            @enable()
            TWC.MVC.History.reset(historySize)
            TWC.MVC.History.popState()
            username = authentication.getUserEmail()
            if authentication.isLoggedIn() and username?
              deferred.resolve(true, username)
            else
              deferred.reject()
          ,
          ()=>
            @enable()
            TWC.MVC.History.reset(historySize)
            TWC.MVC.History.popState()
            username = authentication.getUserEmail()
            if authentication.isLoggedIn() and username?
              deferred.resolve(true, username)
            else
              deferred.reject()
        ]
        callback: ()=>
          @disable()
      # deferred.reject()
      return deferred.promise()

    account_signOut: ->
      deferred = $.Deferred()
      authentication.logout ()=>
        if authentication.isLoggedIn()
          deferred.reject()
        else
          deferred.resolve(false)
      return deferred.promise()

    account_isSignedIn: ->
      deferred = $.Deferred()
      isLoggedIn = authentication.isLoggedIn()
      username = authentication.getUserEmail()
      if isLoggedIn and username?
        deferred.resolve(true, username)
      else
        deferred.resolve(false)

      return deferred.promise()

    content_getAvailability: ->
      deferred = $.Deferred()
      productModel = new CDProduct({Id:@productId})
      productModel.refresh ()=>

        buyPricingPlan = productModel.get("OrderPricingPlans").find (pricingPlan)->
          !pricingPlan.isRental()

        rentPricingPlan = productModel.get("OrderPricingPlans").find (pricingPlan)->
          pricingPlan.isRental()

        free = false
        rented = false
        purchased = false
        availableForRent = if rentPricingPlan then true else false
        availableForPurchase = if buyPricingPlan then true else false

        entitledPricingPlan = productModel.get("PricingPlans").findWhere({ Id : productModel.get("EntitledPricingPlanId")})
        if entitledPricingPlan?
          if entitledPricingPlan.isRental()
            rented = true
          else
            purchased = true

        if entitledPricingPlan? and !entitledPricingPlan.isAvailable()
          deferred.resolve(free, false, false, false, false)
        else
          deferred.resolve(free, rented, purchased, availableForRent, availableForPurchase)

      return deferred.promise()

    content_acquire: ->
      deferred = $.Deferred()
      @disable()
      historySize = TWC.MVC.History.size()
      TWC.MVC.Router.find("productDetail-view").purchaseProduct null, false
        , (purchaseSucceed)=>
          @enable()
          if TWC.MVC.History.size() > historySize
            TWC.MVC.History.reset(historySize)
            TWC.MVC.History.popState()
          deferred.resolve(false, purchaseSucceed)
        , (error)=>
          @enable()
          if TWC.MVC.History.size() > historySize
            TWC.MVC.History.reset(historySize)
            TWC.MVC.History.popState()
          deferred.reject()
      return deferred.promise()

    #Player Calls

    player_initialise: (params)->
      deferred = $.Deferred()
      playerId = params[0]

      objArray = []
      if typeof params[1] is 'string'
        #build up to the same obj type structure
        videoObject = {}
        videoObject.url = params[1]
        objArray.push(videoObject)
        videoArray = [{"stream":objArray}];
      else
        videoArray = params[1]

      returnCallback = ()=>
        setTimeout ()=>
          @enable()
        , 500
      TWC.MVC.Router.find("productDetail-view").playExtraVideo playerId, videoArray, @, ()=>
        TWC.MVC.History.popState()
        returnCallback()
      , ()=>
        @disable()
        deferred.resolve(true)

      return deferred.promise()

    player_getMetadata: (callback)->
      return callback?() if not @channel?
      @channel.call
        method: "playermgmt.getMetadata",
        success: (data)->
          callback?(data)
        error: ->
          callback?([])

    #Basic player API

    player_play: ()->
      if TWC.MVC.Router.find("extravideoplayer-view")?
        TWC.MVC.Router.find("extravideoplayer-view").play()

    player_playPause: ()->
      if TWC.MVC.Router.find("extravideoplayer-view")?
        TWC.MVC.Router.find("extravideoplayer-view").togglePause()

    player_pause: ()->
      if TWC.MVC.Router.find("extravideoplayer-view")?
        console.log "pause", TWC.MVC.Router.find("extravideoplayer-view")
        TWC.MVC.Router.find("extravideoplayer-view").pause()

    player_stop: ()->
      if TWC.MVC.Router.find("extravideoplayer-view")?
        TWC.MVC.Router.find("extravideoplayer-view").stop()

    player_setVisibleControls: (params)->
      controlsVisible = params[0]
      if TWC.MVC.Router.find("extravideoplayer-view")?
        TWC.MVC.Router.find("extravideoplayer-view").setVisibleControls(controlsVisible)

    player_jumpTo: (params)->
      Time = params[0]
      if TWC.MVC.Router.find("extravideoplayer-view")?
        TWC.MVC.Router.find("extravideoplayer-view").jumpTo(time)

    player_onProgress: (playerId, time)->
      return if not @channel?
      @channel.call
        method: "player.#{playerId}.onProgress",
        params: [time]
        success: ()->
          console.log "player.onProgress success"
        error: (error)->
          console.log "player.onProgress error"


    player_onPlayStateChange: (playerId, isPlaying)->
      return if not @channel?
      @channel.call
        method: "player.#{playerId}.onPlayStateChange",
        params: [isPlaying]
        success: ()->
          console.log "player.playStateChange success"
        error: (error)->
          console.log "player.playStateChange error"




  module.exports = CPE_package
