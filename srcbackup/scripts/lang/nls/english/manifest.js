define({
//    /* Translations used in app */
    library: "Library",
    search: "Search",
    favorites: "Favorites",
    settings: "Settings",
    browse: "Browse",
    next: "Next",
    continue: "Continue",
    back: "Back",
    close: "Close",
    cancel: "Cancel",
    ok: "Ok",

    //Logout
    logout_confirmation:"Logout Confirmation",
    logout_descripition: "Are you sure you want to log out?",

    //Settings
    settings_my_account: "My account",
    settings_welcome: "Welcome ",
    settings_full_account_management: "For additional account management tools, go online to <a class='white' href='http://www.sonypicturesstore.com'>www.SonyPicturesStore.com</a>",
    settings_logged_in_as: "Logged in as:",
    logout: "Log out",
    settings_payment_method_on_file: "Payment method on file:",
    settings_no_payment_method: "You have no payment method on file. In order to make a purchase you will need to enter a credit card or PayPal account at <a class='white' href='http://www.sonypicturesstore.com'>www.SonyPicturesStore.com</a>",
    setting_change_pin: "Change PIN",
    settings_added_to_library: "<span class='white'>%TITLE%</span> added to your UltraViolet Library",
    settings_watch_now: "Watch now",
    settings_enter_another_code: "Enter another UltraViolet code",
    settings_go_to_ultraviolet_library: "Go To My Ultraviolet Library",
    settings_redeem: "Redeem",
    settings_enter_uv_code: "Enter your UltraViolet redemption code below to add your movie or TV show to your Library",
    settings_uv_description: "By entering your redemption code you consent to the use and disclosure of certain information under the terms of our Privacy Policy and Terms of Service including automated disclosure of digital information that identifies the content (e.g. its title and that the transaction occurred at this site) to (i) the company that coordinates UltraViolet rights (ii) each third-party service linked with your UltraViolet account and (iii) users associated with your UltraViolet account.",
    settings_manage_uv_account_description: "Your ULTRA and Sony Pictures Store account is currently linked to the UltraViolet Library for {uv_username}. If you would like to manage your UltraViolet Library or unlink your account, please go to <a href='https://www.myuv.com'>https://www.myuv.com</a>.",
    settings_manage_uv_account_not_linked_description: "Your account is <u>not linked</u> to an UltraViolet Library. To link to an UltraViolet Library, please go to <a class='white' href='http://SonyPicturesStore.com/ultrareg'>SonyPicturesStore.com/ultrareg</a> and log in with your email and password.",
    settings_help_about: "Help & About",
    settings_faqs: "FAQ's",
    settings_account: "Account",
    settings_terms_of_service: "Terms of Service",
    settings_terms_of_use: "Terms of Use",
    settings_privacy_policy: "Privacy Policy",

    settings_contact: "Contact Us",
    settings_contact_title: "Call, email or chat with a Customer Care Representative between 5am to 9pm ET Monday-Friday",
    settings_contact_phone: "Phone",
    settings_contact_phone_value: "(800) 860-2878",
    settings_contact_email: "Email",
    settings_contact_email_value: "consumer@sphecustomersupport.sony.com",
    settings_contact_chat: "Live Chat",
    settings_contact_chat_value: "visit <a class='white' href='http://www.sonypicturesstore.com'>www.sonypicturesstore.com</a> to start a live chat with us",
    settings_copyright: "&copy; 2016 Sony Pictures Home Entertainment Inc. All Rights Reserved.",

    //Login
    login_cancel: "Cancel",
    login_next: "Next",
    login_forgot_password: "Forgot Password",
    login_login: "Log In",
    login_log_into_your_account: "Returning users: Enter your email address",
    login_enter_email: "<b>NEW USERS:</b><br/>Please click NEXT to generate an activation code which will be used to complete your registration.",
    login_email_placeholder: "Email address",
    login_enter_your_password: "Enter password for your email address",
    login_password_placeholder: "Password",
    login_not_recognized_email: "Here's the Activation Code for <span class='grey'>{email}</span>",
    login_not_recognized_email_long: "Here's the Activation Code for <br/><span class='grey'>{email}</span>",
    login_go_online_with_code: "Go online to <a class='white' href='http://SonyPicturesStore.com/ultrareg'>SonyPicturesStore.com/ultrareg</a> and enter this activation code to complete your account setup and device registration.<br/><br/>Once account setup is complete, please return to the ULTRA app to log in with your email and password.",
    login_password_reminder_title: "We're very sorry you're having trouble",
    login_password_reminder_subtitle: "We have sent an email with a link to reset your password. Please contact Customer Support if you do not receive an email soon.",
    login_confirmation_text : "This feature requires you to have an account. Would you like to create an account or log in?",

    //Pin
    pin_confirmation: "PIN Confirmation",
    pin_confirmation_subtitle: "Please confirm your PIN",
    pin_skip: "Skip",
    pin_set_pin: "Set PIN",
    pin_set_purchase_pin: "Set Purchase PIN",
    pin_welcome: "Welcome!",
    pin_confirm_subtitle: "Please confirm the Purchase PIN associated with your account.",
    pin_confirm: "Confirm",
    pin_change_pin: "Change PIN",
    pin_payment_error: "Update Payment Information",
    pin_payment_error_message: "It looks like you don't have a payment method associated with this account.<br/><br/>Please go to <b>SonyPicturesStore.com/ultrareg</b> to enter your credit card or PayPal information for purchases in the ULTRA app.",
    pin_confirmation_text: "Enter your password to continue with your new PIN",
    pin_forgot_pin: "Forgot PIN",
    pin_enter_pin_to_confirm: "Enter PIN to confirm",
    pin_enter_pin_to_set_default_payment_method: "Enter PIN to confirm default payment method",
    pin_disable_pin: "Disable PIN",
    pin_enter_password_to_disable_pin: "Please enter your password to disable PIN",
    pin_enter_password_to_reset_pin: "Enter your password to verify your account and reset your PIN",
    pin_create_new_pin: "Create New PIN",
    pin_enter_new_pin: "Enter New PIN",

    //Popups
    popup_exit_title: "Are you sure you would like to exit?",
    popup_exit_close: "Close",
    popup_exit_cancel:  "Cancel",
    popup_error_title: "Error",
    popup_error_retry: "Try again",
    popup_error_return: "Return",
    popup_error_playback: "Uh oh! An error occurred during playback of the video",
    popup_error_connection: "Oops! A connection error has occurred. Please reconnect to the internet and try again.",
    popup_error_close: "Close",
    popup_confirm: "Yes",
    popup_no: "No",
    popup_forgot_password_description: "Your password will be reset and a temporary password will be sent to the email address on file. Would you like to continue?",

    //Product detail
    people_actor: "Cast",
    people_directors: "Directed by",
    genre: "Genre",
    rating_reason: "Rating reason",
    studio: "Studio",
    product_language: "Language",
    min: "min",
    play_button: "Play",
    buy_button: "Buy 4K Ultra HD",
    buy_button_single_quality: "Buy from",
    rent_button: "Rent 4K Ultra HD",
    upgrade_button: "Upgrade to 4K Ultra HD",
    watch_preview: "Trailer",
    extras_button: "Extras",
    preview_extras_button: "Preview Extras",
    favorites_button: "Favorites",
    more_button: "More",
    less_button: "Less",
    view_seasons_button: "View All Seasons",
    season: "Season",
    episode: "Episode",
    previous_episode: "Previous Episode",
    next_episode: "Next Episode",
    view_season_button: "View All Episodes",
    explore_clips: "Explore clips from the film",
    explore_images: "Explore images from the film",
    available_on: "Available on {X}",
    license_expiration_time_min: "min",
    license_expiration_time_days: "days",
    license_expiration_time_left: "LEFT",

    //Payment
    payment_confirm_purchase: "Confirm purchase",
    payment_order_total: "Order Total",
    payment_method: "Payment Method",
    payment_change_payment: "Change payment",
    payment_purchase: "Purchase",
    payment_select_payment:  "Select your preferred payment method",
    payment_method_error: "Payment Method Error",
    payment_promo_code: "Promo Code",
    payment_submit: "Submit",
    payment_remove_code: "Remove code",
    payment_discount: "Discount",
    payment_sales_tax: "Sales Tax",
    payment_already_purchased: "This title has already been purchased.",
    invalid_coupon_code: "Invalid coupon code:",
    invalid_redemption_code: "Invalid Redemption Code",
    payment_promo_subtitle: "Please enter your promo code to apply to this purchase",
    payment_success_title: "Thank You For Your Purchase",
    payment_success_description: "Please select CONTINUE to go back to the previous screen.",
    payment_please_wait: "Please wait while your transaction is being processed.",
    payment_success_watch_now: "Watch Now",
    payment_success_continue: "Continue",
    payment_rental_description: "When renting, you have 30 days to start watching this video, and 48 hours to finish once started.",

    help: "Help",
    help_message:  "If you need help with your activation code, please visit the Help screen under the settings menu.",
    help_message_password:  "If you have any questions or need help with your password, please click HELP.",
    help_message_activation:  "If you need help with your activation code, please visit the Help screen under the settings menu.",
    help_message_promo:  "If you have any questions or need help with your promo code, please click HELP.",

    //Favorites
    recently_added: "Recently Added",
    previously_added: "Previously Added",
    a_to_z: "A-Z",
    z_to_a: "Z-A",

    //Player
    resume_watching: "Resume video at",
    resume: "Resume",
    start_over: "Start from beginning",
    beginning_in: "Beginning in ",
    seconds: "seconds...",
    language: "Language Settings",
    languages: "Languages",
    subtitles: "Subtitles",
    subtitles_language: "Language",

    subtitles_menu_settings: "SUBTITLE SETTINGS",
    subtitles_font_settings: "FONT SETTINGS",
    subtitles_background_settings: "BACKGROUND AND WINDOW",

    subtitles_reset: "Reset Subtitle Values",
    subtitles_text_opacity: "Text Opacity",
    subtitles_text_size: "Text Size",
    subtitles_text_font_family: "Font Family",
    subtitles_text_style:"Text Edge Style",
    subtitles_background_color:"Background Color",
    subtitles_background_opacity:"Background Opacity",
    subtitles_window_color: "Border Color",
    subtitles_window_opacity: "Border Opacity",

    subtitle_example_text: "The quick brown fox jumps over the lazy dog.",


    subtitles_color: "Text Color",
    subtitles_white: "white",
    subtitles_black: "black",
    subtitles_red: "red",
    subtitles_green: "green",
    subtitles_blue: "blue",
    subtitles_yellow: "yellow",
    subtitles_magenta: "magenta",
    subtitles_cyan: "cyan",
    audio: "Audio",
    on: "On",
    off: "Off",
    none: "None",
    resume_welcome_back: "Welcome Back!",
    resume_what_do: "What would you like to do?",
    resume_return_title_page: "Return to Title Page",
    resume_continue_playback: "Resume Playback",

    //Error messages
    code_redemption_code_error: "Not recognized as a valid code.",
    redemption_code_required: "Please enter a redemption code",
    validate_device_pin_error: "PIN not recognized.",
    error_message: "Oops! Something went wrong! Please try again.",
    error_household_access_denied: "Access to household-account denied",
    error_login_suspended_account_message:  "We're sorry. Your account has been suspended. Please contact our customer support.",
    error_message_additional_property_mismatch_must_meet_condition: "should meet the following condition",
    error_message_additional_property_value_of: "Value of {0}",
    error_message_additional_property_value_required: "For {0} is a value required",
    error_message_address2_invalid: "The address is not valid",
    error_message_birth_date_invalid: "The birth date is not valid",
    error_message_birth_date_required: "Please enter your birth date",
    error_message_city_invalid: "The city is not valid",
    error_message_credential_incorrect_password: "The credentials are incorrect. Please try again.",
    error_message_email_address_invalid:  "The email address is not valid",
    error_message_enter_valid_date: "Please enter a valid date",
    error_message_enter_valid_email: "Please enter a valid email address",
    error_message_enter_valid_uv_password: "Please enter a valid password for your UltraViolet account (min. 6 letter or numbers or the following non-alphanumeric characters)",
    error_message_enter_valid_uv_username: "Please enter a valid username for your UltraViolet account (min. 6 letter or numbers or the following non-alphanumeric characters)",
    error_message_exception: "Your request cannot be processed. Please try again.",
    error_message_facebook_login_used: "The specified Facebook account is already linked to another account",
    error_message_gender_required: "Please select a gender.",
    error_message_general_error: "An error has occurred. Please try again.",
    error_message_income_level_required: "An income level is required",
    error_message_incorrect_password: "The specified password is incorrect. Please try again.",
    error_message_invalid_full_name: "The name is not valid.",
    error_message_invalid_login: "The username or password is incorrect. Please contact our customer support",
    error_message_invalid_login_simple: "The username or password is incorrect.",
    error_message_invalid_ship_to_name: "The delivery address is not valid",
    error_message_lead_source_required: "Lead source is required",
    error_message_login_exists: "The username {0} already exists",
    error_message_login_not_authenticated: "You are not authenticated. Did you forget your password?",
    error_message_mismatched_email: "Email and email confirmation do not match",
    error_message_mismatched_passwords: "Password and Confirm Password do not match",
    error_message_mismatched_uv_passwords: "Password and Confirm Password do not match",
    error_message_must_accept_terms_and_conditions: "Please accept our terms and conditions to proceed",
    error_message_name_maxlength: "The name has exceeded the maximum length",
    error_message_new_passwords_do_not_match: "The new passwords do not match",
    error_message_no_guidance_rating: "You have not selected a rating",
    error_message_order_cancel_failed: "The purchase cannot be canceled at this time.",
    error_message_password_contains_username: "An UltraViolet password must not contain 5 or more consecutive characters of the UltraViolet username",
    error_message_password_generation_failed: "Error: Password could not be generated.",
    error_message_password_invalid_login: "The email address is invalid. Please contact customer service if you forgot your email address.",
    error_message_password_length: "Your password must be at least {0} characters",
    error_message_password_maxlength: "Your password must not be longer than {0} characters",
    error_message_password_revoked: "Your password has been revoked. Please contact customer service.",
    error_message_payment_loading_error: "Your payment information could not be loaded.",
    error_message_preferred_language_required: "Please select your preferred language.",
    error_message_send_email_failure: "There was an error when sending the order confirmation. Please check your e-mail address.",
    error_message_temporary_password_sent: "A temporary password has been sent. Please contact customer support if you have not received an email",
    error_message_unable_to_remove_address: "The address could not be removed.",
    error_message_unable_to_remove_payment: "Unfortunately, the payment could not be undone, because it refers to an existing subscription.",
    error_message_uv_must_accept_terms_and_conditions: "You have to accept tbe UltraViolet terms and conditions.",
    error_occurred: "Oops! Something went wrong.",
    error_registering_tv: "Oops! There was an error in registering your TV",
    error_removing_device: "Oops! There was an error when trying to remove this device",
    error_subscriber_not_active: "The current subscriber is not active.",
    already_logged_in: "You're already logged in",
    change_password_invalid_current_password: "Your email could not be changed because you entered an incorrect password.",
    duplicate_device_error: "Device already exists",
    invalid_password_entered: "Your password is incorrect",
    login_revoked_popup: "Your account has been deactivated. Please contact Customer Support on our Help page on <a href='http://www.SonyPicturesStore.com'>www.SonyPicturesStore.com</a>.",
    login_session_expired: "Your session has expired due to inactivity or you have logged in on another browser. Please log in again. Thank you!",
    max_devices_exceeded: "The allowed maximum number of devices has been reached.",
    player_content_access_denied_message: "Access to this video is denied.",
    status_pending: "Pending",
    tv_server_too_busy_message: "Oops! It's very busy at the moment. Please contact Customer Support or try again later."

});

