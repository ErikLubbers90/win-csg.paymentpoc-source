define ({
  license_content:
      "<p>Hello, and welcome to our TalkTalk TV Store Terms and Conditions.</p>" +
      "<p>Yes, this is the \"legal\" part of our service. We appreciate you stopping by to read these terms; itís important that you do.</p>" +
      "<p>We asked our legal team to keep these terms as straight forward as possible and we hope that the terms below achieve this. These terms set out important details about TalkTalk TV Store, our commitment to you and what you can expect when using our service. We are always keen to receive feedback and you can provide specific feedback on these terms and conditions, make any complaints or ask any questions, by contacting us.</p>" +

      "<h3>Terms and conditions</h3>" +
      "<p>Our terms and conditions were last updated on 5th January 2016.</p>" +
      "<p>Our website, www.talktalktvstore.co.uk, and our service are operated by TalkTalk Entertainment Limited (\"we\", \"us\" or \"our\"). Further information about us, including how to contact us, is set out in Section 14.</p>" +
      "<p>By using our website and service (via our app or otherwise), and each time you buy or rent movie or TV content, you agree to these terms. Please also read the Privacy and Cookies Policy on our website, which sets out how we will collect, process and use your information. If you donít agree to any of these terms or our privacy policy, weíd kindly ask that you donít use our website, app or service.</p>" +
      "<p>We will update our terms from time to time and the latest version will appear on our website with the date that it was updated. By using the service after any changes have been posted, you agree to the new terms.</p>" +

      "<h3>1. Your account</h3>" +
      "<p>To use certain functions on our service, including buying or renting movie and TV content, you will need to set up an account with us. You need to be 18 or over to create an account and must be resident in the United Kingdom. If you are over 13 but under 18 then you can register to use our service only with your parent or guardianís permission. You are responsible for your account, including making sure that your details are correct and kept up to date and ensuring that your password is secure. You are also responsible for all activity that takes place on your account.</p>" +
      "<p>If you think your account is being used by anyone else, please contact us immediately. If we believe your account has been compromised, we may suspend your account and we will contact you to try and resolve the problem.</p>" +
      "<p>If you have any problems creating an account, signing in or updating your details, please have a look at the help pages on our website.</p>" +

      "<h3>2. Accessing our service</h3>" +
      "<p>You can access our service and watch movie and TV content either through our website or our free apps. To watch movie and TV content on your device through our app you will first need to download and install the app to your device (see the help pages on our website (https://support.blinkbox.com) for how to do this). To watch content on our website then you will need a compatible Internet browser and the latest Silverlight Player installed, or a HTML5 capable browser.</p>" +
      "<p>Although we are working to ensure that our service is compatible across various devices, we cannot guarantee that our service will work with all devices. Please, therefore, check that your device is compatible with the service before you buy or rent any movie or TV content. Currently you are able to watch movie and TV content on various tablets, Blu-ray players, set-top boxes, games consoles and smart TVs through our app. If you have any questions or any problems checking your device or Internet browser compatibility or installing our app on your device, please have a look at the devices page on our website (https://support.blinkbox.com/hc/en-gb/categories/200157283) or contact us. Please note that our services include elements which allow our software to be downloaded on to your computer or devices and interact with other programs or equipment.</p>" +
      "<p>Where you download (rather than stream) movie or TV content to your computer or laptop device then you will need Windows 8.1 or above with our app installed, available in the Windows App Store.</p>" +
      "<p>We will do our best to make sure that our website, app and services are uninterrupted and that our website, app and content are error free, although we canít guarantee this. If we need to suspend or restrict access to, or update, our service or content, we will do our best to minimise any disruption to you. We recommend you install virus protection software on any devices you use to access our service.</p>" +
      "<p>Please remember, when accessing the TalkTalk TV Store service over your mobile network, you will be using your data allowance. If you are using 3G or 4G to watch movie or TV content, please make sure you are aware of the amount of data in your pay as you go or contract allowance. Please also remember that when you download content, this will use up file space on your device. We are not responsible for any additional charges you may incur in using our service.</p>" +

      "<h3>3. Content on the website and app</h3>" +
      "<p>We may change or remove content or parts of our website or app.</p>"+
      "<p>The legal rights (including the intellectual property rights) in our website and app, any content on them (including the movie and TV content) and the compilation of this content is owned by us, or licensed to us by third parties. Our website, app and content, including the compilation of the content, are protected by international copyright laws and database rights.</p>"+
      "<p>Where we provide links from our website or app to other sites, this will be for information purposes only. We have no control over, or responsibility for, the content on those sites or resources.</p>"+

      "<h3>4. Your use of the service</h3>" +
      "<p>Please do not share movie and TV content on peer-to-peer networks or other file sharing networks or websites. Weíre happy for you to use the content on our website and app, and the movie and TV content you buy or rent from us for your own personal use in accordance with these terms and provided that you do not remove any copyright notices. You may not use our trade marks, logos or other intellectual property and you may not use the movie and TV content you buy or rent, or any other content on our website or app for any commercial, promotional, institutional, corporate or teaching purposes.</p>" +
      "<p>Please don't copy, publish or otherwise seek to exploit our service or our content or use our service for any illegal purpose or in any way that causes damage to the service, is harmful to other users or which interferes with the copyright protection technology we use.</p>" +
      "<p>Please don't use or try to use our service or our content in any way which would breach these terms or otherwise abuse or take advantage of the service.</p>" +
      "<p>We will take the action we think is necessary to protect our service and our customers (which may include us suspending, restricting or terminating your account and access to our service and content). You understand that if you breach these terms then we may decide to remove your access to content you have previously rented or purchased from us.</p>" +
      "<p>For licensing reasons, we use geo-blocking technology to prevent you from accessing our service from outside of the UK and Ireland.</p>" +

      "<h3>5. Placing an order</h3>" +
      "<p>Once you have registered with our service you will be able to choose from a huge range of movie and TV content to buy or rent from us.</p>" +
      "<p>In order to buy or rent a movie or TV programme, you will need to sign in to your account and click on the ‘Rent’ or ‘Buy’ buttons next to the desired content. You will then be asked to submit your payment details in accordance with our accepted payment methods. Our order process allows you to check and amend any errors before submitting your order. Please note that once you click on the ìConfirm secure ‘payment’ button, we will process your payment details and you will not be able to cancel your order, unless you have a legal right to do so. When you place an order you will be able to choose whether you want to store your payment details so that you don’t have to re-enter them each time you buy or rent content. For more information on how to store your payment details and how to buy or rent content, please see the relevant help pages on our website.</p>" +

      "<h3>6. Availability, prices and payment</h3>" +
      "<p>The availability and prices of our movie and TV content may change from time to time. Any changes to the prices will not affect any order that we have confirmed. We do our best to make sure that availability and pricing are correct, but, if we make a mistake and can’t supply you with your selected movie or TV content, we will let you know via email and we won't process your order.</p>" +
      "<p>Once you have placed your order in accordance with Section 5, we will take payment from your provided payment method. All credit/debit card holders are subject to validation checks and authorisation by the card issuer. If the issuer of your card refuses to authorise payment we can’t accept your order. We are not responsible for your card issuer or bank charging you as a result of our processing of your credit/debit card payment in accordance with your order.</p>" +
      "<p>After you have submitted your order, it has been accepted by us and payment has been received, you will receive an e-mail confirming the details of your order and your movie or TV content will be automatically delivered to your account. The contract between us and you for the purchase or rental of the content will only be formed once we confirm that your payment has been processed.</p>" +

      "<h3>7. Buying and renting content</h3>" +
      "<p>When you rent movie or TV content from us, you'll have 30 days to start watching it. Once you've started watching (either by streaming or downloading) then you’ll have 48 hours to watch it as many times as you like. Once either of these time periods have elapsed then the content will automatically be deleted from your account (and where it has been downloaded, from your device) and you will no longer be entitled to watch it. You can watch rented content by way of streaming on up to two devices linked with your account, but not at the same time. Unfortunately, you won't be able to exchange these devices later, so if you'd like to watch your rental on a third device, you'd need to rent the title again. If you would prefer to download a temporary copy of your rented content then you can do so on one of your devices only.</p>" +
      "<p>When you buy movie or TV content from us, you'll be able to watch it whenever and as many times as you like on up to five devices linked with your account. You'll only be able to watch your purchased content on one device at a time and we may ask you to download your purchased content to make sure you have a permanent copy of it. You'll be able to swap up to three of your five devices every 90 days, and we'll automatically refresh your limit every 90 days. For instructions and further information about device limits and how to manage your devices, please see the relevant help pages on our website (https://support.blinkbox.com/hc/en-gb/articles/202595298-Watching-on-multiple-devices).</p>" +
      "<p>Where our service allows you to watch your rented or purchased content offline, you will first need to be in the UK with access to a UK-based Internet connection in order to initially download and validate the content before you can watch it offline. In order to continue watching the content offline, you will then need to reconnect to a UK-based Internet connection at least once every 30 days in order to revalidate the content. Also, if your content is available to download then you won't be able to move it onto another device or burn it to a DVD.</p>" +
      "<p>Sometimes, suppliers of content to our service might place certain rules and restrictions on the use of particular movie and TV content, such as the number and type of devices which can access the content or the duration of access. For example, some suppliers only allow us to provide you with access to standard definition content when you watch the content offline, even where you have purchased or rented the content in high definition. There may also be times when we have to remove certain devices or platforms from being able to access the service. We will do our best to let you know of any of these changes, usage rules and restrictions, where necessary.</p>" +
      "<p>Some of the movie and TV content on our service may only be suitable for persons over a particular age. These age restrictions are displayed on the information pages for each movie or TV programme. Please do not watch content or allow others to watch it if you or they are not old enough. Providing age-appropriate content is important to us and if we find out that the content you buy or rent from us is being watched by people not old enough to do so then we may decide to suspend or terminate your access to our service.</p>" +
      "<p>You can create an UltraViolet account at www.uvvu.com. If you choose to link an UltraViolet account to the account you have with us then you will be able (for a fee determined by us or for free) to: (i) redeem codes through our service which you acquired from eligible Blu-ray Discs or DVDs that included an offer for a digital copy (please see the terms and conditions featured on the disc packaging for more information); (ii) purchase UltraViolet-enabled content on our service and add it to your UltraViolet library; and/or (iii) access your UltraViolet library through our service. Your use of any such content is subject to these Terms and Conditions, any other terms presented when redeeming, purchasing or accessing such content and the terms and conditions at www.uvvu.com.</p>" +

      "<h3>8. Vouchers</h3>" +
      "<p>Movie & TV Cards and Voucher Codes can be redeemed at blinkbox.com/redeem and the credit can be used on all devices except via the iPad and Xbox apps.</p>" +
      "<p>Movie & TV Cards and Voucher Codes cannot be redeemed for cash, returned for refund, exchanged (except where required by law), resold or applied to another account.</p>" +
      "<p>Unfortunately we are unable to provide refunds or compensation for lost, stolen, destroyed Movie & TV Cards or where your Movie & TV Card is used without permission.</p>" +

      "<h3>9. Your right to cancel the contract</h3>" +
      "<p>If you buy or rent movie or TV content by mistake or have changed your mind, you may have a legal right to cancel the transaction and obtain a refund. You can cancel your transaction at any time within 14 days of completing your order. You must notify us of your decision to cancel your transaction within this time and can do so by contacting us (https://support.blinkbox.com/hc/en-gb/requests/new). With so much choice, it can be difficult to make up your mind and so if you aren't sure about your purchase or rental then we're here to help; please get in touch.</p>" +
      "<p>If you do decide to cancel your transaction and have told us within the 14-day period, then we will reimburse your purchase or rental fee to you within 14 days of you telling us and, unless you agree otherwise, we will use the same means of payment as you used for the order.</p>" +
      "<p>Please be aware that, while we will always help to find a solution to your concerns about a particular purchase or rental, if you start to download or stream your content, you might not be able to cancel your transaction as above if you agreed when making your transaction that we could start providing it to you within the 14-day period mentioned above and acknowledged that your right to cancel would be lost in these circumstances. Again, if you have any questions about your purchase or rental, please contact us (https://support.blinkbox.com/hc/en-gb/requests/new).</p>" +
      "<p>Unfortunately there may be some people who try to take advantage or abuse this policy and so, if we think that abuse is occurring, for example if a disproportionate and excessive number of refunds are requested, then we may decide to investigate further and take appropriate steps to stop such abuse (which may include us suspending or restricting an account and access to our service).</p>" +
      "<p>This does not affect your legal rights, including if the movie or TV content is faulty or is not as described, please see Section 10.</p>" +

      "<h3>10. Replacements and refunds</h3>" +
      "<p>If your movie or TV content has not appeared in your account after we have confirmed your purchase or rental, please check the help pages on our website (https://support.blinkbox.com) for assistance.</p>" +
      "<p>Please contact us (https://support.blinkbox.com/hc/en-gb/requests/new) if your movie or TV programme appears to be faulty. We will check your copy and if it is faulty, offer you a replacement or a refund.</p>" +
      "<p>Please note that we are not responsible for any lack of functionality or failure to provide any of service or content on the service, or any loss of content or data that is due to your equipment, devices or Internet connection.</p>" +
      "<p>These terms do not affect your legal rights. For more information about your legal rights in relation to any content purchased or rented that is faulty or is not as described, you may wish to visit your local Citizens Advice Bureau or Trading Standards Office.</p>" +

      "<h3>11. Our legal obligations</h3>" +
      "<p>If we don't comply with our obligations to you under these terms or are negligent in providing the services to you, we are only responsible for any loss or damage that you suffer that was a ‘foreseeable result’ of such breach or negligence. Loss or damage is a foreseeable result if it is an obvious consequence of our failure to comply with these terms or if it was contemplated by us and you at the time we entered into the contract. Our total liability for any loss or damage caused shall be a maximum of £5,000.</p>" +
      "<p>We only supply movie and TV content for your domestic and private use. You agree not to use the content for any commercial, business or re-sale purposes and we have no liability to you for any loss of profit, loss of business, business interruption or loss of business opportunity.</p>" +
      "<p>Nothing in these terms limits or excludes our liability for: (i) death or personal injury caused by our negligence; (ii) fraudulent misrepresentation; or (iii) any other liability that cannot be excluded by law.</p>" +

      "<h3>12. Other important terms</h3>" +
      "<p>You agree to compensate and reimburse us fully for any claims or legal proceedings which are brought against us as a result of your breach of these terms and conditions or misuse of our service.</p>" +
      "<p>This contract is between you and us. No other person has any rights to enforce any of its terms.</p>" +
      "<p>You may not transfer your rights or your obligations under these terms to anyone else without our written permission. We may transfer our rights and obligations to another organisation, but this will not affect your rights or our obligations to you under these terms.</p>" +
      "<p>Each of the paragraphs in these terms operates separately. If any court or relevant authority decides that any of them are unlawful or unenforceable, the remaining paragraphs will remain in full force and effect.</p>" +
      "<p>These terms, and any contract between us, are in the English language. Your use of the service and any content purchased or rented via the service, including any dispute or claim arising out of or in connection with it, will be governed by English law. We and you both agree that the courts of England and Wales shall have the non-exclusive jurisdiction. However, if you are resident in Northern Ireland, you may also bring proceedings in Northern Ireland, and if you are a resident in Scotland, you may also bring proceedings in Scotland.</p>" +

      "<h3>13. Contact us and further information</h3>" +
      "<p>If you have any feedback, questions or complaints or any requests for technical support, then please refer to the help pages on our website (https://support.blinkbox.com).</p>" +

      "<p>TalkTalk Entertainment Limited is a company registered in England and Wales and our Company Number is 05829251. We are part of the TalkTalk Group and our registered office is at 11 Evesham Street, London W11 4AR. Our main trading address is at 28 Kirby Street, London EC1N 8TE.</p>",



    privacy_content:
        "<h3>Our privacy policy</h3>" +
        "<p>The aim of this policy is to provide you with information about: What information we collect from you and why; How we use this information; and How you can access and manage your information. We are committed to protecting and preserving your information, being transparent about what data we hold and how we use it.</p>" +

        "<h3>1. Scope</h3>" +
        "<p>This policy applies to anyone who uses the websites of, or who buys or uses any of the services provided by the TalkTalk group and its companies, which includes the brands TalkTalk and TalkTalk Business.</p>" +
        "<p>When we refer to 'we' or 'our' or 'TalkTalk' we are referring to TalkTalk Telecom Group Plc and its subsidiaries, (including TalkTalk Communications Limited and TalkTalk Telecom Limited), or any of them, as the context requires.</p>" +

        "<h3>2. Overview</h3>" +
        "<p>We collect information about you when you buy or use any of our services including services which have transferred to us when we have bought another business, or from other organisations such as credit agencies or other organisations we use to provide services to you, for example Qube. We may share this information between us so we can provide the services you order and manage your account.</p>" +
        "<p>By using any of our services or visiting our websites you agree to our use of your information as set out in this privacy policy. This privacy policy forms part of and should be read in conjunction with the terms and conditions on our websites and any additional terms you are provided with in relation to our services.</p>" +

        "<h3>3. Information we collect</h3>" +

        "<p>1. Information you give us</p>" +
        "<p>When you place an order with us for any of our services we will need certain information to process your order. This may be information such as your name, private/business email or postal address, telephone or mobile number, date of 	birth, financial or credit card information to help us identify you and to provide 	a service to you. We may ask for other information that relates to the service you 	are using or ordering.</p>" +
        "<p>When you contact us to discuss your services we may ask for certain information to 	be able to confirm your identity, check our records and answer your questions quickly and accurately.</p>" +
        "<p>If you complete any survey or enter any competitions we may ask for information about you, which we will make clear to you at the time and for the purpose we will be using this information.</p>" +

        "<p>2. Information we automatically collect</p>" +
        "<p>We will automatically collect information:</p>" +
        "<p>*When you use our services, such as the amount of time you spend online, the channels and programmes you watch and record, the websites you visit, or when you make a call the number, destination and length of your call, which we need to help manage our network and for billing purposes.<br/>" +
        "*When you visit our websites or use our mobile applications, we may collect and process information about your usage of these by using 'cookies' and other similar technologies (see Cookies section below) to help us make improvements to the websites and to the services we make available.<br/>" +
        "*When you download or use mobile applications created by us and, where applicable, have requested or consented to location services, we may receive information about your location and your mobile device, including a unique identifier foryour device. We may use this information to provide you with location-based services, such as search results, and other personalised content. Most mobile devices allow you to turn off location services. Our mobile application does not collect precise information about the location of your mobile device.</p>" +

        "<p>3. Information we receive from other sources</p>" +
        "<p>We may receive personal information about you from third parties, such as companies contracted by us to provide services to you, other telecommunications operators, marketing organisations, and credit reference agencies (CRA's) or fraud prevention agencies (FPA's) ñ see credit checks section below.</p>" +

        "<h3>4. How we use information</h3>" +
        "<p>The information we collect helps us to better understand what you need from us and to improve the provision of our services to you.</p>"+
        "<p>*verify your identity when you use our services or contact us;<br/>"+
        "*process your enquiries, orders or applications, for example when assessing an application, we may use automated decision making systems and to provide your services;<br/>"+
        "*carry out credit checks and to manage your accounts - see credit checks section below;<br/>"+
        "*monitor, record, store and use any telephone, e-mail or other electronic communications with you for training purposes, so that we can check any instructions given to us and to improve the quality of our customer service, and in order to meet our legal and regulatory obligations;<br/>"+
        "*where you have agreed, provide you with information about other TalkTalk services, offers or products which you may be interested in ñ see Marketing preferences	section below;<br/>"+
        "*tell you about changes to our websites, services or terms and conditions;<br/>"+
        "*carry out any marketing analysis, profiling or create statistical or testing	information to help us personalise the services we offer you and to understand what	our customers want;<br/>"+
        "*recover any monies you may owe to us for using our services;<br/>"+
        "*analyse of our services with the aim of improving them;<br/>"+
        "*prevent or detect a crime, fraud or misuse of, or damage to our network, and to investigate where we believe any of these have occurred; and<br/>"+
        "*monitor network traffic from time to time for the purposes of backup and problem solving, for example our automated system may monitor email subjects to help with spam and malware detection.</p>"+
        "<p>Your data may also be used for other purposes for which you give your specific permission or, in very limited circumstances, when required by law or where permitted under the terms of the Data Protection Act 1998.</p>"+

        "<h3>5. Credit checks</h3>" +
        "<p>When you apply to buy products and services from us we may carry out a credit check. This means we may need to check certain records about you, these may include:</p>"+
        "<p>*our own records;<br/>"+
        "*records at CRAs. Information on applications for our products and services will be sent to CRAs. When CRAs receive a search from us they will place a search footprint on your credit file that may be seen by other organisations. They supply to us both public (including the electoral register) and shared credit and fraud prevention information. If you tell us about a spouse or financial associate, we will link 	your records together. CRAs also link your records together, and these searches 	will be recorded by CRAs. We may disclose information about how you run your accounts to the CRAs;<br/>"+
        "*records at FPAs. We and other organisations may access and use from other	countries the information recorded by FPAs; or<br/>"+
        "*if you are a director of an organisation, we will seek confirmation from CRAs that	the residential address that you provide is the same as that shown on the	restricted register of directors' usual addresses at Companies House.<br/>"+
        "<p>It is important that you give us accurate information. If you give us false or inaccurate data and we suspect fraud we will record this with the FPAs.</p>"+

        "<h3>6. Sharing information</h3>" +
        "<p>We will only share your information with organisations outside TalkTalk:</p>" +
        "<p>*with your consent if we are using information for a purpose other than as set out in this Privacy Policy;<br/>" +
        "*involved in the running or managing of your accounts or providing services to you 	for us (e.g. customer support, or a courier company if you have asked us to send something to you);<br/>" +
        "*to help us improve the services we are providing;<br/>" +
        "*as part of the process of selling one or more of our businesses;<br/>" +
        "*as part of current or future legal proceedings;<br/>" +
        "*in response to properly made requests from law enforcement agencies for the 	prevention and detection of a crime, for the purpose of safeguarding national	security or when the law requires us to, such as in response to a court order or other lawful demand or powers contained in legislation; or<br/>" +
        "*as part of current or future legal proceedings;<br/>" +
        "*as part of current or future legal proceedings;<br/>" +
        "*as part of current or future legal proceedings;<br/>" +
        "*in response to properly made requests from regulatory bodies such as the	Information Commissioners Office and Ofcom, for example where you contact Ofcom 	asking them to investigate a complaint in respect of the provision of our services 	to you, they may request information from us to enable them to investigate and make 	a decision in respect of this matter. We will need to provide them with the relevant information we hold relating to your account. Where we share your information with third parties they are required to follow our	express instructions in respect of the use of your personal information and they 	must comply with the requirements of the Data Protection Act 1998 or any other 	relevant legislation to protect your information and keep it secure.</p>" +
        "<p>From time to time these other people and organisations may be outside the European Economic Area in countries that do not always have the same standard of data protection laws as the UK. However, we will have a contract in place to ensure that your information is adequately protected and we will remain bound by our obligations under the Data Protection Act 1998 even when your personal information is processed outside the European Economic Area.</p>" +

        "<h3>7. How long do we hold your information</h3>" +
        "<p>The time period for which we keep information varies according to what the information is used for. Unless there is a specific legal requirement for us to keep information, we will keep your information for as long as it is relevant and useful for the purpose for which it was collected (and which you agreed to).</p>" +
        "<p>For example, when you contact us we may monitor and record your communications with us to use this information for training and quality purposes, and to meet our legal and regulatory requirements. Where we record calls, these call recordings are only held for a limited period of time before we delete them permanently.</p>" +
        "<p>We will continue to hold information about you if you do not become our customer, your application is declined or after you have closed your account or terminated your services with us. We will only hold such information for such periods as is necessary for the purpose of dealing with enquiries, offering TalkTalk products and services you may be interested in, complying with any legal obligation and for crime and fraud prevention and detection.</p>" +
        "<p>The law requires us to keep certain information about how you use our services for a period of 12 months. This information may be used by certain law enforcement agencies to prevent and detect crime and to protect national security. We will only disclose this information to them when we are legally required to.</p>" +

        "<h3>8. Accessing your information</h3>" +
        "<p>The Data Protection Act 1998 entitles you find out what information we hold about you. If you want to find out what information we hold you will need to submit a request in writing to c/o The Data Controller, Data Protection Office, PO Box 390, Southampton SO30 9AQ together with a cheque for £10 (to cover our costs of processing the information) made payable to:</p>" +
        "<p>*if you are a business customer: TalkTalk Communications Limited or<br/>" +
        "*if you are a residential customer: TalkTalk Telecom Limited.</p>" +
        "<p>We may ask you to provide us with proof of your identity to make sure we are giving your information to the right person.</p>" +
        "<p>To help us process your request you will need to provide the following information:</p>" +
        "<p>*account number(s);<br/>" +
        "*telephone number(s);<br/>" +
        "*address; and<br/>" +
        "*date and time (if requesting a call recording).</p>" +
        "<p>If any of your information is incorrect or your personal details have changed you can either:</p>" +
        "<p>*notify us in writing, and we will update your details; or<br/>" +
        "*update your details within your online account.</p>" +

        "<h3>9. Marketing preferences</h3>" +
        "<p>Where you have agreed to us contacting you either when you joined or via your preferences on your online account, we will contact you with details of products, services and special offers that we believe you may be interested in. If you change your mind and do not want to us to send you marketing messages you can do this in a number of ways:</p>" +
        "<p>*calling our customer services on the phone number set out in the Contact Us section below;</br>" +
        "*writing to us at the contact details set out in the Contact Us section below;<br/>" +
        "*changing your cookie settings, see Cookies section below.</p>" +
        "<p>If you notify us in any of the above ways we will stop sending you marketing messages, but we will still need to send you service related messages including changes to services or terms and conditions.</p>" +

        "<h3>10. Cookies</h3>" +
        "<p>Our websites and mobile application use cookies. Cookies collect information about your use of our website and any of our mobile applications you download, including things like your connection speed, details of your operating system, the time and duration of your visit and your IP address. The information collected by cookies enables us to understand the use of our site, including the number of visitors we have, the pages viewed per session, time exposed to particular pages etc. This in turn helps us to provide you with a better experience, since we can evaluate the level of interest in the content of our website and tailor it accordingly. We will not attempt to personally identify you from your IP address unless required to as a matter of law or regulation or in order to protect our or our other customers' rights.</p>" +
        "<p>Most browsers automatically accept cookies. You can set your browser options so that you will not receive cookies and you can also delete existing cookies from your browser. However, you may find that some parts of the site will not function properly if you disable cookies.</p>" +
        "<p>If you'd like to learn how to manage these cookies and 'opt in' and 'out' of different types:</p>" +
        "<p>*If you are a TalkTalk Telecom customer please see Cookies Policy<br/>" +
        "*If you are a TalkTalk Business customer please see Cookies Policy<br/>" +
        "*If you are a TalkTalk TV Store customer please see Cookies Policy</p>" +

        "<h3>11. Children and TalkTalk</h3>" +
        "<p>We do not specifically collect information about children and believe that children should always get the consent of their parents before giving out any personal information. Where children are specifically encouraged to participate on our websites or using our services (including using our mobile service), they will be advised that they should seek parental permission before then continue to with the activity. We recommend that parents actively participate and where possible monitor their children's use of the internet and the website ñ see Homesafe and Worksafe section below.</p>" +

        "<h3>12. Homesafe and Worksafe</h3>" +
        "<p>Homesafe (TalkTalk Telecom Customers)</p>" +
        "<p>We will keep a record if you have activated or de-activated Homesafe. Information held after deactivation will be only for operational purposes to enable us to identify or trace a fault if required. We do not keep a record of the websites you have sought to block.</p>" +
        "<p>Worksafe (TalkTalk Business Customers)</p>" +
        "<p>We will keep a record of the following information if you have activated Worksafe to enable it to operate in accordance with your instructions:</p>" +
        "<p>*your activation of anti-malware and web filtering;<br/>" +
        "*the website categories you wish to block;<br/>" +
        "*websites you have added to you 'Whitelist' and 'Blacklist'; and<br/>" +
        "*hours of the day you wish the filters to operate.</p>" +

        "<h3>13. Protecting your information</h3>" +
        "<p>We take protecting your data seriously, and will do our utmost to employ appropriate organisational and technical security measures to protect your against unauthorised disclosure or processing.</p>" +
        "<p>Unfortunately we cannot guarantee the security of transmitting information via the internet. We have tried to create a secure and reliable website and mobile application for our users. However, we have no responsibility or liability for the security of personal information transmitted via the internet.</p>" +

        "<h3>14. Changes to privacy policy</h3>" +
        "<p>Please note that this policy will be reviewed and may change from time to time. The revised policy will be posted to this page so that you are always aware of the information we collect, how we use it and under what circumstances we disclose it.</p>" +

        "<h3>15. Contact us</h3>" +
        "<p>If you feel we have breached your privacy, want us to update your marketing preferences, or amend your information, please contact us either:</p>" +
        "<p>If you are a customer of TalkTalk Business</p>" +
        "<p>In writing at:</p>" +
        "<p>TalkTalk Business<br/>" +
        "Customer Services<br/>" +
        "PO Box 136<br/>" +
        "Birchwood<br/>" +
        "Warrington<br/>" +
        "WA3 7BH</p>" +
        "<p>By phone at:<br/>" +
        "0800 083 3003</p>" +
        "<p>If you are a customer of TalkTalk Telecom</p>" +
        "<p>In writing at:</br>" +
        "TalkTalk Telecom Limited<br/>" +
        "TalkTalk Customer Relations Department<br/>" +
        "P.O. Box 346<br/>" +
        "Southampton<br/>" +
        "SO30 2PW</p>" +
        "<p>By phone at:<br/>" +
        "0870 444 1820</p>"
});
