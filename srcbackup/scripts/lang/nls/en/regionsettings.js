define ({
    audio_language:"en",
    subtitle_active:false,
    subtitle_language:"en",
    languageCodeTable:{
        "de"  : "German",
        "deu" : "German",
        "ger" : "German",
        "en"  : "English",
        "eng" : "English",
        "fr"  : "French",
        "fra" : "French",
        "fre" : "French"
    },
    ime_default_layout: "abc"
});