define(["i18n!lang/nls/manifest"], function(Manifest) {
    return {
        pages: [
            {
                id: 0,
                menuTitle: Manifest.settings_help_about,
                loginStatus: false
            },
            {
                id: 1,
                menuTitle: Manifest.settings_faqs,
                loginStatus: false
            },
            {
                id: 2,
                menuTitle: Manifest.settings_redeem,
                loginStatus: true
            },
            {
                id: 3,
                menuTitle: Manifest.settings_account,
                loginStatus: true
            },
            {
                id: 4,
                menuTitle: Manifest.settings_terms_of_service,
                loginStatus: false
            },
            {
                id: 5,
                menuTitle: Manifest.settings_privacy_policy,
                loginStatus: false
            }
        ]
    };
});
