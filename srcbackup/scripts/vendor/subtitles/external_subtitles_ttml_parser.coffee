define (require, exports, module) ->
  TWC = require("framework")
  _ = require("underscore")
  $ = require("jquery")

  ###*
  # TTML parser plug-in
  # Parses subtitle files in the TTML format.
  # Times may be absolute to the timeline or relative
  #   Absolute times are ISO 8601 format (hh:mm:ss[.mmm])
  #   Relative times are a fraction followed by a unit metric (d.ddu)
  #     Relative times are relative to the time given on the parent node
  # Styling information is ignored
  # Xml is the file contents to be processed
  #
  # @param {Object} data
  #
  # Example:
    <tt xmlns:tts="http://www.w3.org/2006/04/ttaf1#styling" xmlns="http://www.w3.org/2006/04/ttaf1">
      <body region="subtitleArea">
        <div>
          <p xml:id="subtitle1" begin="0.76s" end="3.45s">
            It seems a paradox, does it not,
          </p>
        </div>
      </body>
    </tt>
  ###

  rWhitespace = /^[\s]+|[\s]+$/gm
  rLineBreak = /(?:\r\n|\r|\n)/gm
  rValidXMLChars = /[^\x09\x0A\x0D\x20-\xFF]/g

  # Parse the children of the given node
  parseChildren = (node, timeOffset, region) ->
    currNode = node.firstChild
    currRegion = getNodeRegion(node, region)
    retVal = []
    newOffset = undefined
    while currNode
      if currNode.nodeType == 1
        if currNode.nodeName == 'p'
          # p is a textual node, process contents as subtitle
          retVal.push parseNode(currNode, timeOffset, currRegion)
        else if currNode.nodeName == 'div'
          # div is container for subtitles, recurse
          newOffset = timeOffset + toSeconds(currNode.getAttribute('begin'))
          retVal.push.apply retVal, parseChildren(currNode, newOffset, currRegion)
      currNode = currNode.nextSibling
    retVal

  # Get the "region" attribute of a node, to know where to put the subtitles
  getNodeRegion = (node, defaultTo) ->
    region = node.getAttribute('region')
    if region != null
      region
    else
      defaultTo or ''

  # Parse a node for text content
  parseNode = (node, timeOffset, region) ->
    sub = {}
    # Trim left and right whitespace from text and convert non-explicit line breaks
    sub.text = (node.textContent or node.text).replace(rWhitespace, '').replace(rLineBreak, '<br />').replace(rValidXMLChars, '')
    sub.id = node.getAttribute('xml:id') or node.getAttribute('id')
    sub.start = toSeconds(node.getAttribute('begin'), timeOffset)
    sub.end = toSeconds(node.getAttribute('end'), timeOffset)
    sub.target = getNodeRegion(node, region)
    if sub.end < 0
      # No end given, infer duration if possible
      # Otherwise, give end as MAX_VALUE
      sub.end = toSeconds(node.getAttribute('duration'), 0)
      if sub.end >= 0
        sub.end += sub.start
      else
        sub.end = Number.MAX_VALUE
    sub

  # Convert time expression to SS.mmm
  # Expression may be absolute to timeline (hh:mm:ss.ms)
  #   or relative ( decimal followed by metric ) ex: 3.4s, 5.7m
  # Returns -1 if invalid
  toSeconds = (t_in, offset) ->
    i = undefined
    if !t_in
      return -1
    try
      return toSecondsAbs(t_in) + (offset or 0)
    catch e
      i = getMetricIndex(t_in)
      return parseFloat(t_in.substring(0, i)) * getMultipler(t_in.substring(i)) + (offset or 0)

    return

    # global method to parse timestring from ISO 8601 format (hh:mm:ss[.mmm]) to seconds
    #
    # @param [String] timeStr ISO 8601 format (hh:mm:ss[.mmm])
    # @param [Integer] framerate
    # @return [Integer] seconds
  toSecondsAbs = (timeStr, framerate) ->

      # Hours and minutes are optional
      # Seconds must be specified
      # Seconds can be followed by milliseconds OR by the frame information
      validTimeFormat = /^(-?[0-9]+:){0,2}[0-9]+([.;:][0-9]+)?$/
      errorMessage = 'Invalid time format'
      digitPairs = undefined
      lastIndex = undefined
      lastPair = undefined
      firstPair = undefined
      frameInfo = undefined
      frameTime = undefined
      if typeof timeStr == 'number'
        return timeStr
      if typeof timeStr == 'string' and !validTimeFormat.test(timeStr)
        throw "invalid time format"
      digitPairs = timeStr.split(':')

      negativeValue = false
      if timeStr.charAt(0) is "-"
        negativeValue = true

      # Fix last element ;
      lastIndex = digitPairs.length - 1
      lastPair = digitPairs[lastIndex]
      if lastPair.indexOf(';') > -1
        frameInfo = lastPair.split(';')
        frameTime = 0
        if framerate and typeof framerate == 'number'
          frameTime = parseFloat(frameInfo[1], 10) / framerate
        digitPairs[lastIndex] = parseInt(frameInfo[0], 10) + frameTime

      # Fix last element :
      if lastIndex is 3
        numeral = (parseInt(digitPairs[3]) / 60).toFixed(2) #convert to milli seconds
        digitPairs[2] = parseFloat(digitPairs[2]) + parseFloat(numeral)
        delete digitPairs[3]
        digitPairs.pop()

      firstPair = digitPairs[0]
      endValue = {
      1: parseFloat(firstPair, 10)
      2: parseInt(firstPair, 10) * 60 + parseFloat(digitPairs[1], 10)
      3: parseInt(firstPair, 10) * 3600 + parseInt(digitPairs[1], 10) * 60 + parseFloat(digitPairs[2], 10)
      }[digitPairs.length or 1]

      endValue = endValue * -1 if negativeValue
      return endValue

  # In a time string such as 3.4ms, get the index of the first character (m) of the time metric (ms)
  getMetricIndex = (t_in) ->
    i = t_in.length - 1
    while i >= 0 and t_in[i] <= '9' and t_in[i] >= '0'
      i--
    return i

  # Determine multiplier for metric relative to seconds
  getMultipler = (metric) ->
    return {
    'h': 3600
    'm': 60
    's': 1
    'ms': 0.001
    }[metric] or -1


  module.exports = {
    # parse ttml subtitle xml
    #
    # @param [String] data response from XHR request contain ttml xml data
    # @return [Array] array with subtitle objects
    parseSubtitle : (xml) ->
      returnData = []
      node = undefined

      # Null checks
      if !xml
        return returnData

      #try to parse the string to xml document if data is not xml document yet
      if !xml.documentElement
        xml = $.parseXML(xml)

        #check if parsing is successful
        if !xml or !xml.documentElement
          return returnData

      node = xml.documentElement.firstChild
      if !node
        return returnData

      # Find body tag
      while node.nodeName != 'body'
        node = node.nextSibling
      if node
        returnData = parseChildren(node, 0)

      return returnData
  }
