define (require, exports, module) ->
  TWC = require("framework")
  _ = require("underscore")
  $ = require("jquery")

  ###
   * SRT popcorn parser plug-in
   * Parses subtitle files in the SRT format.
   * Times are expected in HH:MM:SS,MIL format, though HH:MM:SS.MIL also supported
   * Ignore styling, which may occur after the end time or in-text
   * While not part of the "official" spec, majority of players support HTML and SSA styling tags
   * SSA-style tags are stripped, HTML style tags are left for the browser to handle:
   *    HTML: <font>, <b>, <i>, <u>, <s>
   *    SSA:  \N or \n, {\cmdArg1}, {\cmd(arg1, arg2, ...)}
   * Data parameter is given by Popcorn, will need a text.
   * Text is the file contents to be parsed
   *
   * @param {Object} data
   *
   * Example:
    1
    00:00:25,712 --> 00:00:30.399
    This text is <font color="red">RED</font> and has not been {\pos(142,120)} positioned.
This takes \Nup three \nentire lines.
This contains nested <b>bold, <i>italic, <u>underline</u> and <s>strike-through</s></u></i></b> HTML tags
    Unclosed but <b>supported tags are left in
    <ggg>Unsupported</ggg> HTML tags are left in, even if <hhh>not closed.
SSA tags with {\i1} would open and close italicize {\i0}, but are stripped
  Multiple {\pos(142,120)\b1}SSA tags are stripped
###

  # [HH:]MM:SS.mmm string to SS.mmm float
  # Throws exception if invalid

  toSeconds = (t_in) ->
    t = t_in.split( ':' );

    try
      s = t[2].split( ',' );

      # Just in case a . is decimal seperator
      if  s.length is 1
        s = t[2].split( '.' )

      return parseFloat( t[0], 10 ) * 3600 + parseFloat( t[1], 10 ) * 60 + parseFloat( s[0], 10 ) + parseFloat( s[1], 10 ) / 1000
    catch e
      return 0

  createTrack = (name, attributes) ->
    track = {}
    track[name] = attributes
    track

  nextNonEmptyLine = (linesArray, position) ->
    idx = position
    while ( !linesArray[idx] )
      idx++

    idx

  lastNonEmptyLine = ( linesArray )->
    idx = linesArray.length - 1;

    while ( idx >= 0 && !linesArray[idx] )
      idx--
    return idx;


  # parse ttml subtitle xml
  #
  # @param [String] data response from XHR request contain ttml xml data
  # @return [Array] array with subtitle objects
  module.exports = {
    parseSubtitle : (data, options) ->
      #declare needed variables
      retObj = {
        title: "",
        remote: "",
        data: []
      }
      subs = []
      i = 0
      idx = 0

      # Here is where the magic happens
      # Split on line breaks
      lines = if data.text? then data.text.split(/(?:\r\n|\r|\n)/gm) else data.split(/(?:\r\n|\r|\n)/gm)

      #    lines = data.text.split( /(?:\r\n|\r|\n)/gm );
      endIdx = lastNonEmptyLine(lines) + 1;
      while i < endIdx
        sub = {}
        text = []

        i = nextNonEmptyLine(lines, i)
        sub.id = parseInt(lines[i++], 10)

        # Split on '-->' delimiter, trimming spaces as well
        time = lines[i++].split(/[\t ]*-->[\t ]*/);

        sub.start = toSeconds(time[0])

        # So as to trim positioning information from end
        idx = time[1].indexOf(" ")
        if idx isnt -1
          time[1] = time[1].substr(0, idx)

        sub.end = toSeconds(time[1])

        # Build single line of text from multi-line subtitle in file
        while i < endIdx && lines[i]
          text.push(lines[i++])


        # Join into 1 line, SSA-style linebreaks
        # Strip out other SSA-style tags
        sub.text = text.join("\\N").replace(/\{(\\[\w]+\(?([\w\d]+,?)+\)?)+\}/gi, "")

        # Escape HTML entities
        sub.text = sub.text.replace(/</g, "&lt;").replace(/>/g, "&gt;")

        # Unescape great than and less than when it makes a valid html tag of a supported style (font, b, u, s, i)
        # Modified version of regex from Phil Haack's blog: http://haacked.com/archive/2004/10/25/usingregularexpressionstomatchhtml.aspx
        # Later modified by kev: http://kevin.deldycke.com/2007/03/ultimate-regular-expression-for-html-tag-parsing-with-php/
        sub.text = sub.text.replace(/&lt;(\/?(font|b|u|i|s))((\s+(\w|\w[\w\-]*\w)(\s*=\s*(?:\".*?\"|'.*?'|[^'\">\s]+))?)+\s*|\s*)(\/?)&gt;/gi,
          "<$1$3$7>")
        sub.text = sub.text.replace(/\\N/gi, "<br />")

        if options && options[ "target" ]
          sub.target = options[ "target" ]

        subs.push(sub)
        i++

      return subs
    }


  return true