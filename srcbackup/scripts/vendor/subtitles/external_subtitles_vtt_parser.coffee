define (require, exports, module) ->
  TWC = require("framework")
  _ = require("underscore")
  $ = require("jquery")

  ###*
  # WebVTT popcorn parser plug-in
  # Parses subtitle files in the WebVTT format.
  # Specification here: http://www.whatwg.org/specs/web-apps/current-work/webvtt.html
  # Styles which appear after timing information are presently ignored.
  # Inline styling tags follow HTML conventions and are left in for the browser to handle (or ignore if VTT-specific)
  # Data parameter is given by Popcorn, text property holds file contents.
  # Text is the file contents to be parsed
  #
  # @param {Object} data
  #
  # Example:
    00:32.500 --> 00:00:33.500 A:start S:50% D:vertical L:98%
    <v Neil DeGrass Tyson><i>Laughs</i>
  ###

  # [HH:]MM:SS.mmm string to SS.mmm float
  # Throws exception if invalid
  toSeconds = (t_in) ->
    t = t_in.split(':')
    l = t_in.length
    time = undefined
    # Invalid time string provided
    if l != 12 and l != 9
      throw 'Bad cue'
    l = t.length - 1
    try
      time = parseInt(t[l - 1], 10) * 60 + parseFloat(t[l], 10)
      # Hours were given
      if l == 2
        time += parseInt(t[0], 10) * 3600
    catch e
      throw 'Bad cue'
    return time

  createTrack = (name, attributes) ->
    track = {}
    track[name] = attributes
    return track

  parseCueHeader = (line) ->
    lineSegments = undefined
    args = undefined
    sub = {}
    rToken = /-->/
    rWhitespace = /[\t ]+/
    if !line or line.indexOf('-->') == -1
      throw 'Bad cue'
    lineSegments = line.replace(rToken, ' --> ').split(rWhitespace)
    if lineSegments.length < 2
      throw 'Bad cue'
    sub.id = line
    sub.start = toSeconds(lineSegments[0])
    sub.end = toSeconds(lineSegments[2])
    return sub

  skipWhitespace = (lines, len, i) ->
    #ignore lines, which contains the ID
    while i < len and (!lines[i] or lines[i].match(/^\d+$/))
      i++
    return i

  skipNonWhitespace = (lines, len, i) ->
    while i < len and lines[i]
      i++
    return i

  # parse webvtt subtitle
  #
  # @param [String] data response from XHR request contain webvtt text data
  # @return [Array] array with subtitle objects
  module.exports = {
    parseSubtitle : (data) ->
      log "PLAYER: VTT parseSubtitle"
      retObj = []
      subs = []
      i = 0
      len = 0
      lines = undefined
      text = undefined
      sub = undefined
      rNewLine = /(?:\r\n|\r|\n)/gm
      # Here is where the magic happens
      # Split on line breaks
      lines = if data.text? then data.text.split(rNewLine) else data.split(rNewLine)
      len = lines.length
      # Check for BOF token
  #    if len == 0 or lines[0] != 'WEBVTT'
      if len == 0
        return retObj
      i++
      while i < len
        text = []
        try
          i = skipWhitespace(lines, len, i)
          sub = parseCueHeader(lines[i++])
          # Build single line of text from multi-line subtitle in file
          while i < len and lines[i]

            text.push lines[i++]
          # Join lines together to one and build subtitle text
  #        text = text.replace(/[^\x09\x0A\x0D\x20-\xFF]/g, "")

          sub.text = text.join('<br />')
          subs.push sub
        catch e
          i = skipNonWhitespace(lines, len, i)
      retObj = subs

      return retObj
  }


