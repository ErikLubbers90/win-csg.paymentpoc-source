// Generated by CoffeeScript 1.12.3
define(function(require, exports, module) {
  var $, TWC, _, createTrack, parseCueHeader, skipNonWhitespace, skipWhitespace, toSeconds;
  TWC = require("framework");
  _ = require("underscore");
  $ = require("jquery");

  /**
   * WebVTT popcorn parser plug-in
   * Parses subtitle files in the WebVTT format.
   * Specification here: http://www.whatwg.org/specs/web-apps/current-work/webvtt.html
   * Styles which appear after timing information are presently ignored.
   * Inline styling tags follow HTML conventions and are left in for the browser to handle (or ignore if VTT-specific)
   * Data parameter is given by Popcorn, text property holds file contents.
   * Text is the file contents to be parsed
   *
   * @param {Object} data
   *
   * Example:
    00:32.500 --> 00:00:33.500 A:start S:50% D:vertical L:98%
    <v Neil DeGrass Tyson><i>Laughs</i>
   */
  toSeconds = function(t_in) {
    var e, l, t, time;
    t = t_in.split(':');
    l = t_in.length;
    time = void 0;
    if (l !== 12 && l !== 9) {
      throw 'Bad cue';
    }
    l = t.length - 1;
    try {
      time = parseInt(t[l - 1], 10) * 60 + parseFloat(t[l], 10);
      if (l === 2) {
        time += parseInt(t[0], 10) * 3600;
      }
    } catch (error) {
      e = error;
      throw 'Bad cue';
    }
    return time;
  };
  createTrack = function(name, attributes) {
    var track;
    track = {};
    track[name] = attributes;
    return track;
  };
  parseCueHeader = function(line) {
    var args, lineSegments, rToken, rWhitespace, sub;
    lineSegments = void 0;
    args = void 0;
    sub = {};
    rToken = /-->/;
    rWhitespace = /[\t ]+/;
    if (!line || line.indexOf('-->') === -1) {
      throw 'Bad cue';
    }
    lineSegments = line.replace(rToken, ' --> ').split(rWhitespace);
    if (lineSegments.length < 2) {
      throw 'Bad cue';
    }
    sub.id = line;
    sub.start = toSeconds(lineSegments[0]);
    sub.end = toSeconds(lineSegments[2]);
    return sub;
  };
  skipWhitespace = function(lines, len, i) {
    while (i < len && (!lines[i] || lines[i].match(/^\d+$/))) {
      i++;
    }
    return i;
  };
  skipNonWhitespace = function(lines, len, i) {
    while (i < len && lines[i]) {
      i++;
    }
    return i;
  };
  return module.exports = {
    parseSubtitle: function(data) {
      var e, i, len, lines, rNewLine, retObj, sub, subs, text;
      log("PLAYER: VTT parseSubtitle");
      retObj = [];
      subs = [];
      i = 0;
      len = 0;
      lines = void 0;
      text = void 0;
      sub = void 0;
      rNewLine = /(?:\r\n|\r|\n)/gm;
      lines = data.text != null ? data.text.split(rNewLine) : data.split(rNewLine);
      len = lines.length;
      if (len === 0) {
        return retObj;
      }
      i++;
      while (i < len) {
        text = [];
        try {
          i = skipWhitespace(lines, len, i);
          sub = parseCueHeader(lines[i++]);
          while (i < len && lines[i]) {
            text.push(lines[i++]);
          }
          sub.text = text.join('<br />');
          subs.push(sub);
        } catch (error) {
          e = error;
          i = skipNonWhitespace(lines, len, i);
        }
      }
      retObj = subs;
      return retObj;
    }
  };
});
