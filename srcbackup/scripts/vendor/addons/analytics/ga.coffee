define [
  "underscore",
  "jquery"
  "framework"
], (_, $, framework) ->
  # This module allows to use Google Analytics Tracking in Apps
  # https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide
  #
  # @example how to track with Google Analytics
  # 
  #   ga = new GA()
  #   ga.init("UA-XXXXX-X", "NOS", "apps.nos.nl")
  #   
  #   //Pageview
  #   ga.createPageView("Main")
  #
  #   //Pageview deeplink
  #   ga.createPageView("EK/Live")
  #
  #   //Event basic
  #   ga.createEvent(Video, Click)
  #
  #   //Event with label
  #   ga.createEvent(Video, Play, 'Gone With the Wind')
  #
  #   //Event with label and value
  #   ga.createEvent('video', 'Video Load Time', 'Gone With the Wind', downloadTime)
  #
  class GA
    #Initiate new GA instance
    #
    # @param [String] UAcode to use with GA
    # @param [String] app app id to use with GA
    # @param [String] domain domain to use with GA
    # @param [Boolean] ssl when true fire all request over https (optional)
    init: (@UAcode, @app, @domain, @debug=false) ->
      log "GA: init - #{@UAcode}"
      @cid = @getClientId()                   # unique cookie for session

    # execute GA request and provide object as extra params
    #
    # @param [Object] obj GA parameters to add to the request
    execute: (obj)->
      baseData =
        v:1                       # Version.
        tid:@UAcode               # Tracking ID / Property ID.
        cid:@cid                  # Anonymous Client ID.
        z:new Date().getTime()    # Cache buster
        aip:1                     # Anonymize IP

      payload = _.extend({}, baseData, obj)

      $.ajax({
        url: "https://www.google-analytics.com/collect",
        method:"POST",
        data:payload
      })

    # create an GA event, one can provide optionally label and value
    #
    # @param [String] category 
    # @param [String] action 
    # @param [String] label (optional)
    # @param [String] value (optional)
    # @param [Array] custom dimension indices
    # @param [Array] custom dimension values
    # @param [String] user ID (optional)
    createEvent: (category, action, label="", value="", customDimensionIndices, customDimensionValues, uid="")->
      log "GA: event - #{category}, #{action} #{label} #{value}"
      return false if @debug

      params =
        t:  "event"        # Pageview hit type.
        ec: category       # Event Category. Required.
        ea: action         # Event Action. Required.
        el: label          # Event label.

      _.each customDimensionIndices, (cdIndexValue, index) ->
        params["cd#{cdIndexValue}"] = customDimensionValues[index]

      params.ev = parseInt(value) if !isNaN(parseFloat(value)) and isFinite(value) #only add value if param is valid integer
      params.uid = uid if uid.length > 0

      @execute(params)

    # create an GA pageview, param as aray to provide deeplinks
    #
    # @param [String] view 
    createPageView: (view)->
      log "GA: pageview - #{view}"
      return false if @debug

      params =
        t:  "pageview"            # Pageview hit type.
        dh: @domain               # Document hostname.
        dp: "/#{@app}/#{view}"    # Page.
        dt: view                  # Title.

      @execute(params)

    # get a client id reference
    getClientId: ->
      storage = framework.SDK.Storage.get()
      cid = storage.getItem("cid")
      if not cid
        cid = "" + Math.round(2147483647 * Math.random())
        storage.setItem("cid", cid)
      return cid

  new GA()