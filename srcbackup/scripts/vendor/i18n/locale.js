// Returns locale
var locale = (function() {
  // Browser
  country = "nl"
  isoLanguage = navigator.language.toLocaleLowerCase();
  language = isoLanguage.split("-")[0];
  userAgent = new String(navigator.userAgent);

  if (/maple/i.test(userAgent)) {

    // Samsung
    params = window.location.href.split("?")[1].split("&")

    for (param in params) {
      paramParts = param.split("=");

      key = paramParts[0]
      value = paramParts[1]

      if (key == "country")
        country = value;

      if (key == "lang")
        language = value.split("-")[0];
    }

  } else if (/tizen/i.test(userAgent)) {

    // Tizen
    try {
      tizen.systeminfo.getPropertyValue
      "LOCALE", function (systemLocaleInfo) {
        language = systemLocaleInfo.language.toLocaleLowerCase().split("_")[0]

        //country = systemLocaleInfo.country      // Does not return the correct value yet according to Samsung
        countryConfigKey = webapis.productinfo.ProductInfoConfigKey.CONFIG_KEY_SERVICE_COUNTRY;
        country = webapis.productinfo.getSystemConfig(countryConfigKey).toLocaleLowerCase();
      }
    } catch (err) {
      console.log("Failed to execute: tizen.systeminfo.getPropertyValue(\"LOCALE\")");
    }

  } else if (/netcast/i.test(userAgent)) {

    // LG
    device = document.getElementById("device");
    if (device) country = device.tvCountry2;
    if (device) language = device.tvLanguage2.split("-")[0];

  } else if (/webos/i.test(userAgent)) {

    // WebOS
    webOS.service.request("luna://com.webos.settingsservice", {
      method: "getSystemSettings",
      parameters: {
        "keys": ["localeInfo"],
        "subscribe": true
      },
      onComplete: function (inResponse) {
        language = inResponse.returnValue.locales.TV.split("-")[0];
      }
    });

    webOS.service.request("luna://com.webos.settingsservice", {
      method: "getSystemSettings",
      parameters: {
        "keys": ["smartServiceCountryCode2"],
        "subscribe": true
      },
      onComplete: function (inResponse) {
        country = inResponse.returnValue;
      }
    });

  }

  return { iso: isoLanguage, language: language, country: country };
})(this);