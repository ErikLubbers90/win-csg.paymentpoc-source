<div id="container" class="background">
  <h1>UWP Tokens</h1>
    <div id="leftButton" class="button" data-down="#azureToken"><div class="glow"></div>Retrieve UWP Token</div><br/>
    <div style="width:60%; height:45px">
      Access Token:
      <input style="float:right" id="azureToken" data-down="#collectionsToken"/><br/>
    </div>
    <div style="width:60%; height:45px">
      Collections Token:
      <input style="float:right" id="collectionsToken" data-down="#leftButton"/><br/>
    </div>
    <!-- <div style="width:60%; height:45px">
      Publisher User ID:
      <input style="float:right" id="publisherUserID" data-down="#leftButton"/><br/>
    </div> -->
    <div id="rightButton" class="button" data-up="#azureToken"><div class="glow"></div>Retrieve Store ID</div><br/>
    <div style="width:60%; height:55px">
      Windows store ID:
      <span style="float:right" id="storeid"/>
    </div>
    <div id="azureError" style="color:red;"/>
  <h1>Windows Store Product</h1>
  <div style="width:60%; height:45px">
    Product Store ID:
    <input style="float:right" id="productStoreID" data-up="#rightButton" data-down="#submitWindowsOrder"/>
  </div>
  <div id="submitWindowsOrder" class="button" data-up="#rightButton"><div class="glow"></div>Purchase product</div><br/>
  <div id="windowsError" style="color:red;"/>
  <div id="windowsMessage"/>
  <h1>CSG Product</h1>
  <div style="width:60%; height:45px">
    Product:
    <input style="float:right" id="sku" data-up="#leftButton" data-down="#pricingplanid"/>
  </div>
  <div style="width:60%; height:45px">
    Pricing Plan ID:
    <input style="float:right" id="pricingplanid" data-up="#sku" data-down="#paymentinstrumentid"/>
  </div>
  <div style="width:60%; height:45px">
    Payment Instrument ID:
    <input style="float:right" id="paymentinstrumentid" data-up="#pricingplanid"/>
  </div>
  <div id="submitCSGOrder" class="button" data-up="#paymentinstrumentid"><div class="glow"></div>Submit order to CSG</div><span style="font-size:15px;">*Not implemented yet, awaiting changes to SubmitOrder API</span><br/>
</div>
