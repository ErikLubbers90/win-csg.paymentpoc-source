<div class="content login register">
    <div id="loginContent" class="fullscreen">
        <div id="logo"></div>
        <div id="screenCloseButton" class="menu-sound closeButton" data-down="defaultFocus"><span class='buttonText'><%- manifest.close %></span></div>
        <div class="loginContainer">
          <div class="header">
              <div class="title grey">Endpoint</div>
          </div>
          <div class="divider"></div>
          <div id="loginResult"></div>
          <input id="inputFieldEndpoint" data-down="focusDownFromKeyboard" placeholder="" /><br />
          <div id="currentEndpoint">
            TestTest
          </div>
          <div class="buttons">
              <div id="setEndpoint" data-up="focusUp" data-down="focusDown" data-left="defaultFocus" data-right="defaultFocus" class="button"><div class="glow"></div>Set endpoint</div>
          </div>
          <div class="header">
              <div class="title grey">SystemID</div>
          </div>
          <div class="divider"></div>
          <div id="loginResult"></div>
          <input id="inputFieldSystemID" data-down="focusDownFromKeyboard" placeholder="" /><br />
          <div id="currentSystemID">
            TestTestID
          </div>
          <div class="buttons">
              <div id="setEndpoint" data-up="focusUp" data-down="focusDown" data-left="defaultFocus" data-right="defaultFocus" class="button"><div class="glow"></div>Set endpoint</div>
          </div>
            <div class="header">
                <div class="title grey"><%- manifest.register_log_into_your_account %></div>
            </div>
            <div class="divider"></div>
            <div id="loginResult"></div>
            <input id="inputFieldEmail" data-down="focusDownFromKeyboard" placeholder="Email Address" /><br />
            <input id="inputFieldPassword" data-down="focusDownFromKeyboard" placeholder="Password" />
            <div class="buttons">
                <div id="forgetPasswordButton" data-up="focusUp" data-down="focusDown" data-left="defaultFocus" data-right="defaultFocus" class="button disabled"><div class="glow"></div><%- manifest.login_forgot_password %></div>
                <div id="loginButton" data-up="focusUp" data-down="focusDown" data-left="defaultFocus" class="button disabled"><div class="glow"></div><%- manifest.login_login %></div>
            </div>
            <div class="disclaimer"><%= manifest.register_disclaimer %></div>
            <div class="divider second"></div>
            <div class="noAccount">
                <div class="extraBlockTitle"><%= manifest.register_dont_have_an_account %></div>
                <div id="createAccount" data-up="focusUp" data-down="focusDown" data-left="defaultFocus" data-right="defaultFocus" class="button"><div class="glow"></div><%= manifest.register_create_account %></div>
            </div>

            <div class="redeemPromoCode">
                <div class="extraBlockTitle"><%= manifest.register_do_you_have_a_code %></div>
                <div id="redeemPromotionCode" data-up="focusUp" data-down="focusDown" data-left="defaultFocus" class="button"><div class="glow"></div><%= manifest.register_redeem_promotion_code %></div>
            </div>
        </div>
    </div>
</div>
