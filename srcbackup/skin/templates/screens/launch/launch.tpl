<div class="content login">
    <div id="launchContent" class="fullscreen">
        <div id="logo"></div>        
        <div class="header"><div class="title"><%- manifest.lets_get_started %></div></div>
        <div class="buttonsContainer">                        
            <div id="loginOption" class="menuBlock" data-right="#promoOption"><p><%= manifest.launch_i_have_account %></p></div>
            <div id="promoOption" class="menuBlock" data-left="#loginOption" data-right="#browseOption"><p><%= manifest.launch_i_have_a_code %></p></div>
            <div id="browseOption" class="menuBlock" data-left="#promoOption"><p><%= manifest.launch_i_want_to_browse %></p></div>            
        </div>
    </div>
</div>

