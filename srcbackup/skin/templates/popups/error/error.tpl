<div id="popupContainer">
    <h1><%= manifest.popup_error_title %></h1>
    <div class="errorMessage"></div>
    <div id="popupErrorButtons">
        <div id="leftButton" class="" data-right="#rightButton"><%= manifest.popup_error_close %></div>
        <div id="rightButton" class="" data-left="#leftButton"><%= manifest.popup_error_retry %></div>
    </div>
</div>