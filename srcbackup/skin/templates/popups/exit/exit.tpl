<div id="popupContainer">
    <h1><%= manifest.popup_exit_title %></h1>
    <div id="popupButtons">
        <div id="leftButton" class="" data-right="#rightButton"><%= manifest.popup_exit_cancel %></div>
        <div id="rightButton" class="" data-left="#leftButton"><%= manifest.popup_exit_close %></div>
    </div>
</div>