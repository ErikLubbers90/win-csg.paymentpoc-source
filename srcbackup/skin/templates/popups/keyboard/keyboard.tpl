<div id="closeKeyboard"></div>
<div id="keyboard">
    <div id="keyboardScreen">
        <div id="keyboardFrame">
            <div class="characters">          
                <ul>
                    <li id="character_30"
                        class="button smallbutton switchchars start"
                        data-left="moveCharacterLeft"
                        data-right="moveCharacterRight"
                        data-up="moveCharacterUp"
                        data-down="moveCharacterDown">#$%</li>
                    <li id="character_31"
                          class="button submit "
                          data-left="moveCharacterLeft" 
                          data-right="moveCharacterRight" 
                          data-up="moveCharacterUp" 
                          data-down="moveCharacterDown">OK</li>
                    <li id="character_32"
                          class="button caps" 
                          data-left="moveCharacterLeft" 
                          data-right="moveCharacterRight" 
                          data-up="moveCharacterUp" 
                          data-down="moveCharacterDown">caps</li>
                    <li id="character_33"
                          class="button space"
                          data-left="moveCharacterLeft"
                          data-right="moveCharacterRight"
                          data-up="moveCharacterUp"
                          data-down="moveCharacterDown"></li>
                    <li id="character_34"
                        class="button return"
                        data-left="moveCharacterLeft"
                        data-right="moveCharacterRight"
                        data-up="moveCharacterUp"
                        data-down="moveCharacterDown">Backspace</li>
                    <li id="character_35"
                        class="button switchlan"
                        data-left="moveCharacterLeft"
                        data-right="moveCharacterRight"
                        data-up="moveCharacterUp"
                        data-down="moveCharacterDown">azerty</li>
                  </ul>
            </div>
            <div class="numpad">          
               <ul>                
                   <li id="num_1" class="numbers" data-left="moveNumLeft" data-right="defaultFocus" data-up="moveNumUp" data-down="moveNumDown">1</li>       
                   <li id="num_2" class="numbers" data-left="moveNumLeft" data-right="defaultFocus" data-up="moveNumUp" data-down="moveNumDown">2</li>       
                   <li id="num_3" class="numbers" data-left="moveNumLeft" data-right="moveNumRight" data-up="moveNumUp" data-down="moveNumDown">3</li>                                     
                   <li id="num_4" class="numbers" data-left="moveNumLeft" data-right="defaultFocus" data-up="moveNumUp" data-down="moveNumDown">4</li>       
                   <li id="num_5" class="numbers" data-left="moveNumLeft" data-right="defaultFocus" data-up="moveNumUp" data-down="moveNumDown">5</li>       
                   <li id="num_6" class="numbers" data-left="moveNumLeft" data-right="moveNumRight" data-up="moveNumUp" data-down="moveNumDown">6</li>                 
                   <li id="num_7" class="numbers" data-left="moveNumLeft" data-right="defaultFocus" data-up="moveNumUp" data-down="moveNumDown">7</li>                 
                   <li id="num_8" class="numbers" data-left="moveNumLeft" data-right="defaultFocus" data-up="moveNumUp" data-down="moveNumDown">8</li>                 
                   <li id="num_9" class="numbers" data-left="moveNumLeft" data-right="moveNumRight" data-up="moveNumUp" data-down="moveNumDown">9</li>                 
                   <li id="num_10" class="characterMoveCarot" data-left="moveNumLeft" data-right="moveNumRight" data-up="moveNumUp" data-down="moveNumDown"> < </li>
                   <li id="num_11" class="characterMoveCarot" data-left="moveNumLeft" data-right="moveNumRight" data-up="moveNumUp" data-down="moveNumDown"> > </li>
                   <li id="num_12" class="numbers" data-left="moveNumLeft" data-right="moveNumRight" data-up="moveNumUp" data-down="moveNumDown">0</li>
               </ul>
            </div>
        </div>
        <div class="" id="backButton" data-up="backtoKeyboard">
            <div class="close"></div><div class="closeTxt">Close</div>
        </div>
    </div>
</div>
    <div class="fakeCursor">^</div>
