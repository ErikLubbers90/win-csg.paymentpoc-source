<div id="popupContainer">
    <h1><%= manifest.popup_unsubscribe_title %></h1>
    <div class="userInformation"></div>
    <div id="popupUnsubscribeButtons">
        <div id="leftButton" class="" data-right="#rightButton"><%= manifest.popup_unsubscribe_cancel %></div>
        <div id="rightButton" class="" data-left="#leftButton"><%= manifest.popup_unsubscribe_confirm %></div>
    </div>
</div>