module.exports = function (grunt) {

    //load modules
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-text-replace');
    grunt.loadNpmTasks('grunt-multi');
    grunt.loadNpmTasks('grunt-zipup');
    grunt.loadNpmTasks('grunt-cleanempty');

    grunt.loadTasks('tasks');

    var fs = require('fs');
    var today  = grunt.template.today('yyyymmdd');
    var config = grunt.file.readJSON('./package.json');
    config.platforms = (grunt.option('platform') ? [grunt.option('platform')] : config.platforms);
    config.environment = (grunt.option('environment') ? [grunt.option('environment')] : config.environment);
    config.packageUrl = (grunt.option('packageUrl') ? [grunt.option('packageUrl')] : config.packageUrl);

    //generate list of platforms to package
    config.package_platforms = (function(){
      var pp = []
      for (j = 0; j < config.package_platforms.length; j++){
        platform = config.package_platforms[j];
        var exists = grunt.file.exists("tasks/"+platform+"/package.js");
        if (exists) pp.push(platform);
      }
      return pp;
    })();

    //Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        //run the same task multiple times in parallel with different configuration
        multi : {

            //optimize task, build project
            optimize: {
                options: {
                    vars: {
                        platform_list: config.platforms,
                        environment: config.environment
                    },
                    config: {
                        config : function(vars, rawConfig){
                            config.optimize = "none";
                            config.platform = vars.platform_list;
                            config.environment = config.environment;
                            return config;
                        },
                        environment :  '<%= environment %>',
                        platform: '<%= platform_list %>',
                        resolution: function(vars, rawConfig) {
                            return config.resolutions[vars.platform_list][1];
                        },
                        today: today
                    },
                    tasks: ['clean:build', 'copy:build', 'replace:index', 'less:skin', 'requirejs:javascript','cleanempty', 'platformTask']
                }
            },

            //compress task, build and compress project
            compress: {
                options: {
                    vars: {
                        platform_list: config.platforms,
                        environment: config.environment
                    },
                    config: {
                        config : function(vars, rawConfig){
                            config.optimize = "uglify2";
                            config.platform = vars.platform_list;
                            config.environment = config.environment;
                            return config;
                        },
                        environment :  '<%= environment %>',
                        platform: '<%= platform_list %>',
                        resolution: function(vars, rawConfig) {
                            return config.resolutions[vars.platform_list][1];
                        },
                        today: today
                    },
                    tasks: ['clean:build', 'copy:build', 'replace:index', 'replace:cssMin', 'less:skin','cssmin','clean:build_skin', 'requirejs:javascript','cleanempty', 'platformTask' ]
                }
            },

            //deploy task, build, compress and zip project
            deploy: {
                options: {
                    vars: {
                        platform_list: config.platforms,
                        environment: config.environment
                    },
                    config: {
                        config : function(vars, rawConfig){
                            config.optimize = "uglify2";
                            config.platform = vars.platform_list;
                            config.environment = config.environment;
                            return config;
                        },
                        environment :  '<%= environment %>',
                        platform: '<%= platform_list %>',
                        resolution: function(vars, rawConfig) {
                            return config.resolutions[vars.platform_list][1];
                        },
                        today: today
                    },
                    tasks: ['clean:build', 'copy:build', 'replace:index', 'replace:cssMin', 'less:skin','cssmin', 'clean:build_skin','requirejs:javascript','cleanempty', 'platformTask', 'zipup:build']
                }
            },

            //package task, generate dynamic build
            package: {
                options: {
                    vars: {
                        platform_list: config.package_platforms,
                        environment: config.environment
                    },
                    config: {
                        config : function(vars, rawConfig){
                            config.optimize = "uglify2";
                            config.platform = vars.platform_list;
                            config.environment = config.environment;
                            config.packageUrl = config.packageUrl;
                            return config;
                        },
                        environment :  '<%= environment %>',
                        platform: '<%= platform_list %>',
                        resolution: function(vars, rawConfig) {
                            return config.resolutions[vars.platform_list][1];
                        },
                        today: today
                    },
                    tasks: ['clean:package', 'copy:package', 'platformPackageTask', 'zipup:package']
                }
            }
        },

        //clean build directory
        clean: {
            build_skin: {
                src: ['build/<%= platform %>/skin/**/*.css', '!build/<%= platform %>/skin/**/*.min.css']
            },
            build: {
                src: ['build/<%= platform %>/']
            },
            package: {
                src: ['package/<%= platform %>/']
            }
        },

        //copy actions
        copy: {

            //construct the platform build dir
            build: {
              files: [
                  //copy main source files, exlcuding script and skin
                  {cwd: 'src', src: ['**/*', '!scripts/**/*', '!skin/**/*'], dest: 'build/<%= platform %>/', expand: true},

                  //copy skin files
                  {cwd: 'src/skin/<%= resolution %>/', src: ['**/*'], dest: 'build/<%= platform %>/skin/<%= resolution %>/', expand: true},
                  {cwd: 'src/skin/fonts/', src: ['**/*'], dest: 'build/<%= platform %>/skin/fonts/', expand: true},
                  {cwd: 'src/skin/video/', src: ['**/*'], dest: 'build/<%= platform %>/skin/video/', expand: true},
                  {cwd: 'src/skin/audio/', src: ['**/*'], dest: 'build/<%= platform %>/skin/audio/', expand: true},
                  {cwd: 'src/skin/templates/', src: ['**/*'], dest: 'build/<%= platform %>/skin/templates/', expand: true},

                  //copy script files
                  {cwd: 'src/scripts/lang', src: ['**/*'], dest: 'build/<%= platform %>/scripts/lang/', expand: true},
                  {cwd: 'src/scripts/vendor/subtitles', src: ['**/*.js'], dest: 'build/<%= platform %>/scripts/vendor/subtitles/', expand: true},
                  {cwd: 'src/scripts/vendor/i18n', src: ['locale.js'], dest: 'build/<%= platform %>/scripts/vendor/i18n/', expand: true},
				  {cwd: 'src/scripts/vendor/hasplayer', src: ['**/*.js'], dest: 'build/<%= platform %>/scripts/vendor/hasplayer/', expand: true},
				  {cwd: 'src/scripts/vendor/win10', src: ['**/*.js'], dest: 'build/<%= platform %>/scripts/vendor/win10/', expand: true},

                  //copy platform specific files
                  {cwd: 'tasks/<%= platform %>/include', src: ['**/*'], dest: 'build/<%= platform %>/', expand: true},
              ]
            },
            package: {
              files: [
                  //copy main source files, exlcuding script and skin
                  {cwd: 'src', src: ['**/*', '!scripts/**/*', '!skin/**/*'], dest: 'package/<%= platform %>/', expand: true},
                  //copy platform specific files
                  {cwd: 'tasks/<%= platform %>/include', src: ['**/*'], dest: 'package/<%= platform %>/', expand: true},
              ]
            }
        },

        //tasks for text replace actions
        replace: {
            //update index.html to support built JS and add platform specific css files
            index: {
                src: ['build/<%= platform %>/index.html'],
                overwrite: true,
                replacements: [{
                    from: "src=\"scripts/vendor/require/require.js\"",
                    to: "src=\"scripts/application.js\""
                },{
                    from: "<link href=\"skin/less/main.css\" rel=\"stylesheet\" type=\"text/css\" />",
                    to: "<link href=\"skin/<%= resolution %>/main.css\" rel=\"stylesheet\" type=\"text/css\" />\n        <link href=\"skin/<%= resolution %>/main.<%= platform %>.css\" rel=\"stylesheet\" type=\"text/css\" />"
                }]
            },

            //update index.html to support compressed css files
            cssMin: {
                src: ['build/<%= platform %>/index.html'],
                overwrite: true,
                replacements: [{
                    from: "<link href=\"skin/<%= resolution %>/main.css\" rel=\"stylesheet\" type=\"text/css\" />",
                    to:   "<link href=\"skin/<%= resolution %>/main.min.css\" rel=\"stylesheet\" type=\"text/css\" />"
                },{
                    from: "<link href=\"skin/<%= resolution %>/main.<%= platform %>.css\" rel=\"stylesheet\" type=\"text/css\" />",
                    to:   "<link href=\"skin/<%= resolution %>/main.<%= platform %>.min.css\" rel=\"stylesheet\" type=\"text/css\" />"
                }]
            },

            //update config.js with package.json values
            config: {
                src: ['src/scripts/config.js'],
                overwrite: true,
                replacements: [{
                    from: /"title": "(.*)"/,
                    to:   "\"title\": \""+ config.title +"\""
                },{
                    from: /"build": "(.*)"/,
                    to:   "\"build\": \""+ config.build +"\""
                },{
                    from: /"version": "(.*)"/,
                    to:   "\"version\": \""+ config.version +"\""
                }]
            }
        },
        //task to compress css files
        cssmin:{
            main: {
                options:{
                    rebase:true,
                    relativeTo:'build/<%= platform %>/'
                },
                files: {
                    'build/<%= platform %>/skin/<%= resolution %>/main.min.css': ['build/<%= platform %>/skin/<%= resolution %>/main.css'],
                    'build/<%= platform %>/skin/<%= resolution %>/main.<%= platform %>.min.css': ['build/<%= platform %>/skin/<%= resolution %>/main.<%= platform %>.css']
                }
            }
        },

        //task to combine and compress js files
        requirejs: {
            javascript: {
                options: {
                    //Find more details https://github.com/jrburke/r.js/blob/master/build/example.build.js
                    mainConfigFile: "src/scripts/bootstrap.js",
                    paths: {
                        "twc-universal-sdk": "framework/twc-universal-sdk-<%= platform %>",
                        "framework": "framework/twc-universal-mvc",
                    },
                    baseUrl: "src/scripts",
                    name: "bootstrap",
                    generateSourceMaps: true,
                    preserveLicenseComments: false,
                    optimize: "<%= config.optimize %>",
                    include: ["vendor/require/require"],
                    out: "build/<%= platform %>/scripts/application.js",
                    waitSeconds:15,
                    onBuildWrite: function (moduleName, path, contents) {
                        resolution = config.resolutions[grunt.config.get("platform")];
                        contents = contents.replace(/"resolution": \[(.*)\]/, "\"resolution\": \[\""+ resolution[0] +"\", \""+ resolution[1] +"\"\]");
                        contents = contents.replace(/"environment": "(.*)"/, "\"environment\": \""+ grunt.config.get("environment") +"\"");
                        contents = contents.replace(/"platform": "(.*)"/, "\"platform\": \""+ grunt.config.get("platform") +"\"");
                        contents = contents.replace(/"debug": (.*),/, "\"debug\": "+ config.debug +",");
                        return contents;
                    }
                }
            }
        },

        //task to convert less files to css, linking the platform resolution
        less: {
            skin: {
                options: {
                    modifyVars: {
                        'screen-scale':"<%= parseInt(resolution)/1080 %>",
                         'screen-width':"<%= resolution %>"
                    },
                    ieCompat: false
                },
                files: [{
                    cwd: 'src/skin/less/',
                    src: ['**/*.less'],
                    dest: 'build/<%= platform %>/skin/<%= resolution %>/',
                    expand: true,
                    ext: '.css',
                    extDot: 'last'
                }]
            }
        },

        //task to execute platform specific build script
        platformTask: {options:{platform: '<%= platform %>'}},
        platformPackageTask: {options:{platform: '<%= platform %>'}},

        //task to zip the build project
        zipup: {
            //build
            build: {
                appName: '<%= config.name %>',
                suffix: '<%= platform == "tizen" ? "wgt":"zip" %>',
                version: '<%= config.version %>.<%= config.build %>_<%= platform %>',
                datetime: today,
                environment: '<%= config.environment %>',
                template: '{{appName}}_{{environment}}_{{version}}_{{datetime}}.{{suffix}}',
                addGitCommitId: false,
                files: [
                    {
                        cwd: 'build/<%= platform %>',
                        src: '**',
                        expand: true
                    }
                ],
                outDir: 'build/'
            },
            //package
            package: {
                appName: '<%= config.name %>',
                suffix: '<%= platform == "tizen" ? "wgt": platform == "webos" ? "ipk" : "zip" %>',
                version: '<%= config.version %>.<%= config.build %>_<%= platform %>',
                datetime: today,
                environment: '<%= config.environment %>',
                template: 'PACKAGE_{{appName}}_{{environment}}_{{version}}.{{suffix}}',
                addGitCommitId: false,
                files: [
                    {
                        cwd: 'package/<%= platform %>',
                        src: '**',
                        expand: true
                    }
                ],
                outDir: 'package/'
            }
        },

        //remove all empty folders from project build
        cleanempty: {
            options: {
                files:false,
                folders:true
            },
            src: {
                cwd: 'build/<%= platform %>',
                src: '**',
                expand: true
            }
        },
    });

    //MAIN GRUNT TASKS
    grunt.registerTask('optimize', ['replace:config', 'multi:optimize']);  //grunt optimize - build project
    grunt.registerTask('compress', ['replace:config', 'multi:compress']);  //grunt compress - build project and compress JS+CSS
    grunt.registerTask('deploy',   ['replace:config', 'multi:deploy']);    //grunt deploy   - build project and compress JS+CSS and zip package
    grunt.registerTask('package',   ['multi:package']);    //grunt deploy   - build project and compress JS+CSS and zip package

};
