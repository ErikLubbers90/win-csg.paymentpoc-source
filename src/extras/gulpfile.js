/**
 * Created by bartvandenende on 16/12/15.
 */
// Gulp Dependencies
var gulp = require('gulp');
var html = require('html-browserify');
var browserify = require('browserify');
var stringify = require('stringify');
var source = require("vinyl-source-stream");
var uglify = require("gulp-uglify");
var rename = require("gulp-rename");

gulp.task('browserify', function() {
    var b = browserify();
    b.transform(stringify(['.html']));
    b.add('./js/app.js');
    return b.bundle().on('error', function(err){
        // print the error (can replace with gulp-util)
        console.log(err.message);
        // end this stream
        this.emit('end');
    })
        .pipe(source('app.bundle.js'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('compress', function() {
    return gulp.src('dist/app.bundle.js')
        .pipe(uglify().on('error', function(e) { console.log('\x07',e.message); return this.end(); }))
        .pipe(rename({
            extname: '.min.js'
        }))
        .pipe(gulp.dest('dist'));
});


gulp.task('watch', function() {
    var files = ['js/**/*.js', '!js/app.bundle.js'];
    gulp.watch(files, ['browserify', 'compress']);
});
