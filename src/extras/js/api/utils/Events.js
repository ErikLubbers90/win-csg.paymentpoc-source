'use strict';

/*
 * Implements the Content Access API group.
 */
var Listener = (function() {
	/**
	 * Construct a new Listener and identify it with the specified API group.
	 *
	 * @param group
	 * @returns ListenerMgr
	 */
	function Listener(group) {
		this.group = group;
		this.listeners = [];
	}

	// prototype values are static defaults. The constructor MUST create it's own
	// instance-specific property
	Listener.prototype.group = '';
	Listener.prototype.listeners = [];

	// Methods required by API...

	/**
	 * Adds the listener to the listener list. The listener is registered for all
	 * event notifications associated with this interface.
	 *
	 * @param listener
	 * @returns true if the listener was added successfully
	 */
	Listener.prototype.addListener = function(listener) {
		// check for duplicates
		if (this.listeners.indexOf(listener) < 1) {
			this.listeners.push(listener);
			return true;
		} else {
			return false;
		}
	};

	/**
	 * Removes the listener from the listener list.
	 *
	 * @param listener
	 */
	Listener.prototype.removeListener = function(listener) {
		var loc = this.listeners.indexOf(listener);
		if (loc >= 0) {
			this.listeners.splice(loc, 1);
		}
	};

	// Methods NOT required by API....

	/**
	 *
	 * @param eventCode
	 */
	Listener.prototype.notify = function() {
		var cnt;
		for (cnt = 0; cnt < this.listeners.length; cnt++) {
			this.listeners[cnt].eventNotification.apply(this.listeners[cnt].eventNotification, arguments);
		}
	};

	Listener.prototype.countListener = function() {
		window.console.log(this.group + " has " + this.listeners.length);
	};

	return Listener;

})();

module.exports = {
	Listener:Listener
};