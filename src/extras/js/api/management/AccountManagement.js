'use strict';

var Config = require("../../config.js");
var Events = require("../utils/Events.js");
var Channel = require("../utils/Channel.js");
var API = require("../API.js");

/*
 * Implements the Account Access API group.
 */
var AccountManager = (function() {
    function AccountManager() {

        this.listener = new Events.Listener("Account");
        this.username = null;
        this.isLoggedIn = false;

    }

    AccountManager.prototype.signIn = function(_reqestId) {
        var statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_PENDING);
        var self = this;

        Channel.call({
            method: "account.signIn",
            success: function(resp){
                var isLoggedIn = resp[0];
                var username = resp[1];
//                console.log("[signIn success]", isLoggedIn, username);
                self.isLoggedIn = isLoggedIn;
                if(username) self.username = username;
                self.listener.notify(isLoggedIn ? ACCNT_SIGNED_IN : ACCNT_SIGNED_OUT, _reqestId);
                statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);
            },
            error: function(error, message){
                console.log("[sigIn Error] go back to previous state");
                self.listener.notify(ACCNT_SIGNED_OUT, _reqestId);
                statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);
            }
        });

        return statusDesc;
    };

    AccountManager.prototype.signOut = function(_reqestId) {
        var statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_PENDING);
        var self = this;

        Channel.call({
            method: "account.signOut",
            success: function(resp){
                var isLoggedIn = resp[0];
                self.isLoggedIn = isLoggedIn;
                self.username = null;
                self.listener.notify(isLoggedIn ? ACCNT_SIGNED_IN : ACCNT_SIGNED_OUT, _reqestId);
                statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);
            },
            error: function(error, message){
                console.log("[signOut error]", error, message)
            }
        });

        return statusDesc;
    };

    AccountManager.prototype.isSignedIn = function(_callback) {
        var statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_PENDING);
        var self = this;

        Channel.call({
            method: "account.isSignedIn",
            success: function(resp){
                var isLoggedIn = resp[0];
                var username = resp[1];
//                console.log("[isSignedIn success]", isLoggedIn, username);
                self.isLoggedIn = isLoggedIn;
                if(username) self.username = username;
                if ((_callback !== undefined) && (_callback != null)) _callback(isLoggedIn);
                statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);
            },
            error: function(error, message){
                console.log("[isSignedIn error]", error, message)
            }
        });

        return statusDesc;
    };

    /**
     * Request for information regarding the properties of a signed-in consumer
     * (i.e., screen name, age, avatar, etc.). Properties are specified in terms
     * of key-value pairs (KVP). The available properties are specific to each
     * retailer. Account properties are, for a package, immutable and read-only.
     * That is to say, only the retailers framework may include or modify the
     * account properties. * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * <h3>API Changes:</h3>
     * <ol>
     * <li>use of JSON instead of KVP</li>
     * <li>return NULL is user if not logged in</li>
     * </ol>
     */
    AccountManager.prototype.getAccountProperties = function() {
        return this.isLoggedIn ? {
            username : this.username
        } : {};
    };

    /**
     * Request for information regarding the user interface preferences of a
     * signed-in consumer (i.e., layout format, language, font size). Properties
     * are specified in terms of key-value pairs (KVP). The available properties
     * may be specific to each retailers framework. Unlike the
     * AccountProperties, the preferences are mutable. *
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * <h3>API Changes:</h3>
     * <ol>
     * <li>use of JSON instead of KVP</li>
     * <li>return retailer-specific default if user is not logged in</li>
     * </ol>
     */
    AccountManager.prototype.getPreferences = function() {
        return {
            lang : API.PackageMgmt.locale
        }
    };

    AccountManager.prototype.addListener = function(_listener) {
        this.listener.addListener(_listener);

    };

    AccountManager.prototype.removeListener = function(_listener) {
        this.listener.removeListener(_listener);
    };

    return AccountManager;
})();

module.exports = AccountManager;