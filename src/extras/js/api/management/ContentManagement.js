'use strict';

var Config = require("../../config.js");
var Events = require("../utils/Events.js");
var Channel = require("../utils/Channel.js");
var API = require("../API.js");

/*
 * Implements the Content Access API group.
 */
var ContentManager = (function() {

    function ContentManager() {
        this.listener = new Events.Listener("Content");
    }

    /**
     * Checks the consumer's access rights to the specified content from the
     * CpxFramework. This may include identification of any restrictions that
     * may apply. Possible restrictions might include if it must be purchased or
     * rented prior to playback, if there is a time limit on the availability,
     * or if a limited number of playbacks are allowed. The value returned will
     * be an array containing all applicable codes.
     *
     * The function can be invoked either synchronously or asynchronously. When
     * a <tt>_requestId</tt> is provided by the caller, the function results
     * will be returned asynchronously via an Access Event notification. If the
     * <tt>_requestId</tt> is undefined or null, the results will be returned
     * synchronously as the <tt>context</tt> argument of the returned
     * <tt>StatusDescriptor</tt>
     */
    ContentManager.prototype.getAvailability = function(_contentId, _callback) {
        var rights = [];
        var statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_IN_PROGRESS);

        Channel.call({
            method: "content.getAvailability",
            parameters: _contentId,
            success: function(resp){
                var Free = resp[0];
                var Rented = resp[1];
                var Purchased = resp[2];
                var availableForRent = resp[3];
                var availableForPurchase = resp[4];

                if (Free) rights.push(ACC_AVAIL_FREE);
                if (Rented) rights.push(ACC_AVAIL_RENTED);
                if (Purchased) rights.push(ACC_AVAIL_BOUGHT);
                if (availableForRent) rights.push(ACC_AVAIL_2RENT);
                if (availableForPurchase) rights.push(ACC_AVAIL_2BUY);

                if (_callback !== undefined && (_callback != null)) _callback(_contentId, rights);
                statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);
                statusDesc.context = rights;
            },
            error: function(error, message){
                console.log("[getAvailability error]", error, message)
            }
        });

        return statusDesc;
    };

    ContentManager.prototype.acquire = function(_contentId, _requestTag) {
        var self = this;
        var statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_IN_PROGRESS);

        Channel.call({
            method: "content.acquire",
            parameters: _contentId,
            success: function (resp) {
                var isRented =  resp[0];
                var isPurchased = resp[1];
                var eventCode = [];
                if (isRented) eventCode.push(ACC_AVAIL_RENTED);
                if (isPurchased) eventCode.push(ACC_AVAIL_BOUGHT);
                self.listener.notify(_contentId, eventCode, _requestTag, CC_OP_COMPLETED);
                statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);
            },
            error: function(error, message){
                console.log("[acquire error]", error, message)
                self.listener.notify(_contentId, [], _requestTag, CC_OP_COMPLETED);
                statusDesc = new StatusDescriptor(LEVEL_INFO, CC_OP_COMPLETED);

            }

        });

        return statusDesc;
    };

    ContentManager.prototype.addListener = function(_listener) {
        this.listener.addListener(_listener);
    };

    ContentManager.prototype.removeListener = function(_listener) {
        this.listener.removeListener(_listener);
    };

    return ContentManager;
})();

module.exports = ContentManager;