'use strict';

var Config = require("../../config.js");
var Events = require("../utils/Events.js");
var Channel = require("../utils/Channel.js");

var Player = (function() {

    function Player(_contentId, _playerId, _basePath) {
        this._playerId = _playerId;
        this._basePath = _basePath;
        this._contentId = _contentId;
        this.listener = new Events.Listener("Player-"+_playerId);
        this.currentTime = 0;
        this.x = 0;
        this.y = 0;
        this.width = 1920;
        this.heigth = 1080;
        this.fullscreen = true;
        this.isPlaying = false;
        var self = this;
        Channel.call({
            method:"player.initialise",
            params: [self._playerId, self._contentId],
            success: function(resp) {
                console.log("Player.initialize successful");
            },
            error: function(error, message){
                console.log("Player.initialize error", error);
            }
        });
        Channel.bind("player."+self._playerId+".onProgress", function(trans, currentTime) {
            self.currentTime = currentTime;
        });
        Channel.bind("player." + self._playerId + ".getMetadata", function(trans) {
            window.CPX_Package.getMetadata(function(metadata) {
                try{
                    metadata = JSON.parse(metadata)
                }catch(error){
                    metadata = [];
                }
                trans.complete(metadata);
            });
            trans.delayReturn(true);
        });
        Channel.bind("player."+self._playerId+".onDisplayChange", function(trans, x, y, width, heigth) {
            self.x = x;
            self.y = y;
            self.width = width;
            self.heigth = heigth;
        });
        Channel.bind("player."+self._playerId+".onPlayStateChange", function(trans, isPlaying) {
            self.isPlaying = isPlaying;
        });
        Channel.bind("player."+_playerId+".onFullScreen", function(trans, fullscreen) {
            self.fullscreen = fullscreen;
        });
    }

    // ================ BASIC SubGroup =========================
    /**
     * Start playback using currently specified rate and direction.
     */
    Player.prototype.play = function() {
        Channel.call({
            method:"player.play",
            success: function(resp) {
                console.log("Player.play successful");
            },
            error: function(error, message){
                console.log("Player.play error", error);
            }
        });
        return RESP_OK;
    };

    /**
     * Restart or pause playback depending on current state. If restarting,
     * playback will resume at last location using currently specified rate and
     * direction.
     */
    Player.prototype.togglePause = function() {
        Channel.call({
            method:"player.play_pause",
            success: function(resp) {
                console.log("Player.play_pause successful");
            },
            error: function(error, message){
                console.log("Player.play_pause error", error);
            }
        });
        return RESP_OK;
    };

    /**
     */
    Player.prototype.setPaused = function(pause) {
        Channel.call({
            method:"player.pause",
            success: function(resp) {
                console.log("Player.pause successful");
            },
            error: function(error, message){
                console.log("Player.pause error", error);
            }
        });

        return RESP_OK;
    };

    /**
     */
    Player.prototype.isPaused = function() {
        return self.isPlaying;
    };

    /**
     * Stops playback and resets to the media stream to its initial state.
     */
    Player.prototype.stop = function() {
        Channel.call({
            method:"player.stop",
            success: function(resp) {
                console.log("Player.stop successful");
            },
            error: function(error, message){
                console.log("Player.stop error", error);
            }
        });
        return RESP_OK;
    };

    // ================ Geometry SubGroup ============================

    Player.prototype.setPlayerContainer = function(container, successCallback, errorCallback) {
        console.log("setPlayerContainer", container, successCallback, errorCallback);
    };

    Player.prototype.setFullScreen = function(enable) {
        //Player is always fullscreen
        if(enable){
            return RESP_OK;
        }else{
            return RESP_FAILED;
        }
    };

    Player.prototype.isFullScreen = function() {
        return self.fullscreen;
    };

    /**
     */
    Player.prototype.getPlayerGeometry = function() {
        var geometry            = {};
        geometry.playerHeight   = this.heigth;
        geometry.playerWidth    = this.width;
        geometry.videoHeight    = this.heigth;
        geometry.videoWidth     = this.width;
        geometry.videoOffsetX   = this.x;
        geometry.videoOffsetY   = this.y;
        geometry.safeAreaHeight = Math.round(this.heigth*-0.9);
        geometry.safeAreaWidth  = Math.round(this.width*-0.9);
        geometry.safeAreaOffsetX = Math.round(this.width*-0.1/2);
        geometry.safeAreaOffsetY = Math.round(this.heigth*-0.1/2);
        return geometry;
    };

    Player.prototype.setVisibleControls = function(show) {
        Channel.call({
            method:"player.setVisibleControls",
            params: [show],
            success: function(resp) {
                console.log("Player.setVisibleControls successful");
            },
            error: function(error, message){
                console.log("Player.setVisibleControls error", error);
            }
        });
    };

    /*
     * returns the current playback time, in seconds.
     */
    Player.prototype.getCurrentTime = function() {
        return this.currentTime;
    };

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Player.prototype.jumpTo = function(position){
        /*
         * ISSUE: is the <video> element (i.e., 'player') in a readyState that
         * allows the setting of the currentTime property?
         */
        Channel.call({
            method:"player.jumpTo",
            params: [position],
            success: function(resp) {
                console.log("Player.jumpTo successful");
            },
            error: function(error, message){
                console.log("Player.jumpTo error", error);
            }
        });
        return RESP_OK;
    };

    Player.prototype.addListener = function(listener) {
        this.listener.addListener(_listener);
    };

    Player.prototype.removeListener = function(listener) {
        this.listener.removeListener(_listener);
    };

    Player.prototype.terminate = function(listener) {
        Channel.call({ method:"player."+self._playerId+".terminate", success:function(){}, error:function(){} })
    };
    return Player;
})();

module.exports = Player;