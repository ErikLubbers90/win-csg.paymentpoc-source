'use strict';

var Config = require("../../config.js");
var Events = require("../utils/Events.js");
var Channel = require("../utils/Channel.js");

/**
 * Created by Motion Picture Laboratories, Inc. (2014)
 *
 * This work is licensed under a Creative Commons Attribution (CC BY) 3.0
 * Unported License.
 */
var PackageManager = (function() {
    function PackageManager() {
        this.locale = Config.defaultLang;
        this.environment = "production";
        this.isBooting = false;
        this.isHidden = false;
        this.isTerminating = false;
        var self = this;

        Channel.bind("package.initialize", function(trans, params) {
            console.log("package.initialize", params.contentId);
            if (params.locale != null) self.locale = params.locale;
            if (params.contentId != null) self.contentId = params.contentId;
            if (params.environment != null) self.environment = params.environment;
            if (params.isHidden != null)  self.isHidden = params.isHidden;
            if (params.alternativeStream != null) self.alternativeStream = params.alternativeStream;

            self.initialize();
            return;
        });
        Channel.bind("package.terminate", function(trans) {
            console.log("package.terminate");
            self.disable();
            self.terminate();
            return
        });
        Channel.bind("package.enable", function(trans) {
            console.log("package.enable");
            self.enable();
            return
        });
        Channel.bind("package.disable", function(trans) {
            console.log("package.disable");
            self.disable();
            return
        });
        Channel.bind("package.getStatus", function(trans) {
            console.log("package.getStatus");
            self.getState();
            return
        });

        Channel.bind("package.backButton", function(trans) {
            var event=document.createEvent("Events");
            event.initEvent("keydown",true,true);
            event.keyCode='8';
            event.which='8';
            document.body.dispatchEvent(event);           //manually trigger backspace event
        });

    }

    /**
     * @returns
     */
    PackageManager.prototype.getSupportedAPIs = function() {
        // supported cpe packae features (see chapter 4.2.1.1)
        return [
            GRP_PM_LC,      //Package Management - Lifecycle
            GRP_CA_AV,      //Content Access - Availability
            GRP_CA_EVT,     //Content Access - Access Event
            GRP_AA_B,       //Account Access - Basic
            GRP_AA_EVT,     //Account Access - Account Event
            GRP_PI_LC,      //Player Interaction - Lifecycle
            GRP_PI_B,       //Player Interaction - Basic
            GRP_PI_EVT,     //Player Interaction - Player Event
            GRP_PI_GEOM     //Player Interaction - Geometry
        ];
    };

    PackageManager.prototype.status = function(_statusDesc) {
        if (_statusDesc.level == LEVEL_ERROR) {
            window.console.error(_statusDesc.message);
        } else if (_statusDesc.level == LEVEL_INFO) {
            if (_statusDesc.completionCode == CC_STATE_CHANGE) {
                var curPState = this.getState();
                var context = {
                    metadata:{
                        featureContentId: "movie",
                        trailerContentId: "trailer",
                        titleContentId: this.contentId,
                        featureVariant: this.alternativeStream ? [{
                            id: this.alternativeStream.value,
                            name: this.alternativeStream.key
                        }] : []
                    }
                };
                window.console.log("STATE_CHANGE: Package " + JSON.stringify(context)
                    + " is now " + P_STATE_Label[curPState]);
                switch (curPState) {
                    case P_STATE_LOADED:
                        window.CPX_Package.initialize(context,
                            window.CpxFramework, window.document
                                .getElementById(Config.packageDiv));
                        break;
                    case P_STATE_INIT:
                        this.isBooting = true;
                        break;
                    case P_STATE_AVAILABLE:
                        // when init is called (only boot) auto enable the package
                        if(this.isBooting && !this.isHidden) {
                            this.isBooting = false;
                            this.enable();
                        // when package disable is called
                        } else {
                            Channel.call({method: "package.disabled", success: function () {}, error: function () {}});
                        }
                        break;
                    case P_STATE_RUNNING:
                        Channel.call({ method: "package.enabled", success:function(){}, error:function(){}});
                        break;
                    case P_STATE_FAILED:
                        break;
                    case P_STATE_EXITING:
                        break;
                    case P_STATE_TERMINATED:
                        Channel.call({ method: "package.terminated", success:function(){}, error:function(){}});
                        break;
                }
            }
        }
    };

    PackageManager.prototype.getEnvironmentDesc = function() {
        /*
         * Contents may vary from Framework to Framework and should be specified and
         * agreed to as part of the integration process.
         */
        var envDesc = {};
        envDesc['lang'] = this.locale;
        envDesc["baseDir"] = Config.baseDir[this.environment];
        envDesc['frameworkId'] = Config.retailerID;
        envDesc['frameworkVer'] = Config.frameworkVer;
        envDesc['apiVer'] = Config.apiVer;
        return envDesc;
    };

    //
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ........ Bridge functions ......
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    PackageManager.prototype.initialize = function() {
        var env = this.getEnvironmentDesc();
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = (window.packageOverride ? window.packageOverride : env["baseDir"]+"/"+this.contentId+"/cpep-package-0_1.js");
        window.document.body.appendChild(script);
    };
    PackageManager.prototype.enable = function() {
        return window.CPX_Package.enable();
    };
    PackageManager.prototype.disable = function() {
        return window.CPX_Package.disable();
    };
    PackageManager.prototype.terminate = function() {
        this.isTerminating = true;
        return window.CPX_Package.terminate();
    };
    PackageManager.prototype.getState = function() {
        return window.CPX_Package.getState();
    };

    return PackageManager;
})();

module.exports = PackageManager;
