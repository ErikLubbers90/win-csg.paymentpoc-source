module.exports = {
    baseDir: {
        "production": "http://movietouch.sony.com.edgesuite.net/UltraSPS/Packages",
        "development": "http://r60-cpe.s3-website-us-east-1.amazonaws.com/hosting/radius60/cpx/dev/bravia/packages"
    },
    packageDiv: "packDiv1",
    defaultLang: "en",
    retailerID: "SONY:SPS:ULTRA",
    frameworkVer: "0.10.a",
    apiVer: "0.93",
    context: "sps-ultra"
};