'use strict';

//add prefix to console.log to identify extra logs
var orginal_log = console.log;
console.log = function(){
    var mainArguments = Array.prototype.slice.call(arguments);
    orginal_log.apply(console, ["EXTRA:"].concat(mainArguments));
};

//load API
var API = window.CpxFramework = require('./api/API.js');
