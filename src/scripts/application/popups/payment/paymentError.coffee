define [
  "framework",
  "settingPages",
  "config",
  "jquery",
  "application/helpers/submenuHelper",
  "../../models/baseModel",
  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDSubscriberManagement",
  "application/api/authentication",
  "i18n!../../../lang/nls/manifest"
], (TWC, ConfigSettings, Config, $, MenuHelper, BaseModel, CDBaseRequest, CDSubscriberManagement, Authentication, Manifest) ->

  paymentErrorModel = BaseModel.extend
    defaults:
      "manifest": Manifest,

  class paymentErrorView extends TWC.MVC.View
    uid: 'paymenterror-popup'

    el: "#popup"

    type: 'popup'

    model: new paymentErrorModel()

    defaultFocus: "#paymentOption-1"

    external:
      html:
        url:"skin/templates/popups/payment/paymenterror.tpl"

    events:
      "enter .payment-option": "changePaymentOption"
      "enter #continueButton": "close"
      "enter #popupCloseButton": "close"

    onload: (@paymentInstruments, @loginCallback) ->
      @model.set("paymentInstruments", @paymentInstruments)

    onRenderComplete: ->
      setTimeout () =>
        $('#popup').addClass('fadein')
        $(TWC.MVC.View.eventbus).trigger("loading", [false])
      , 100

    close: ->
      $('#popup').removeClass('fadein')
      setTimeout ()=>
        @unload()
      , 500

  paymentErrorView.get()

