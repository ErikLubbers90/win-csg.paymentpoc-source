define [
  "framework"
  "../generic/alertWithOptions"
  "application/api/authentication"
  "i18n!../../../lang/nls/manifest"
  "application/models/baseModel"
  "application/helpers/submenuHelper"
], (TWC, alertWithOptions, Authentication, Manifest, baseModel, MenuHelper) ->

  class loginConfirmationAlert extends alertWithOptions

    onload:(@isFocusInMainMenu, @confirmationCallback, @isFocusInSettingsSubmenu = false, @cancelCallback = null, texts = null)->
      @model = new baseModel();
      if not texts?
        texts =
          title: Manifest.login_confirmation_text
          confirm: Manifest.popup_confirm
          cancel: Manifest.popup_no

      @model.set("title", texts.title)
      @model.set("description", "")
      @model.set("leftButtonText", texts.confirm)
      @model.set("rightButtonText", texts.cancel)

    leftButtonAction: ->
      @close =>
        @confirmationCallback?()

    rightButtonAction: ->
      @cancelCallback?()
      @close()
      TWC.MVC.Router.find("menu-zone").fixSelectedMenuItem(@isFocusInMainMenu)
      if @isFocusInSettingsSubmenu
        TWC.MVC.Router.find("settings-view").fixSelectedSubmenuItem()
