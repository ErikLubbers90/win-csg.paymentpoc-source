define [
  "jquery"
  "underscore"
  "./cd/CDSubscriberManagement"
  "./cd/CDBaseRequest"
], ($, _, CDSubscriberManagement, CDBaseRequest)->

  # generic authentication class for juke app
  class Authentication
    # Automatic login call

    #
    # @param callback [Function(error)] function called when this method is completed
    @silentLogin: (callback) ->
      CDSubscriberManagement.createSession (error)=>
        callback?(error)

    # Login with user credentials
    #
    # @param username [String] username of the subscriber
    # @param password [String] password of the subscriber
    # @param devicePinStatusCheck [Function(error] device pin status check
    # @param callback [Function(error)] function called when this method is completed
    @manualLogin: (username, password, callback) ->
      #login with CD API
      CDSubscriberManagement.userLogin username, password, (error)=>
        callback?(error)

    # Logout the current subscriber
    #
    # @param callback [Function(error)] function called when this method is completed
    @logout: (callback) ->
      CDSubscriberManagement.logout (error)->
        callback?(error)

    # Returns the current login state of the user
    #
    # @return bool is user logged in
    @isLoggedIn: ->
      CDSubscriberManagement.isLoggedIn()

    @isValidUser: (username, callback)->
      CDSubscriberManagement.isValidUser username, (error)=>
        callback?(error)

    @getUserEmail: ->
      if CDBaseRequest.subscriber
        return CDBaseRequest.subscriber.get("Login")

    @getUserId: ->
      if CDBaseRequest.subscriber
        console.log "subscriber", CDBaseRequest.subscriber
        return CDBaseRequest.subscriber.get("SubscriberId")


