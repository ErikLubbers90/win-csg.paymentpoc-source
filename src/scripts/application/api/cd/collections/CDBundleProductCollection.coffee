define [
  "backbone",
  "underscore",
  "../models/CDProduct",
], (Backbone, _, CDProductModel)->

  CDBundleProductCollection = Backbone.Collection.extend({
    model: CDProductModel
  })