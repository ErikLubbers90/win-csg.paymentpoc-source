define [
  "backbone",
  "underscore",
  "../models/CDProduct",
  "./CDSharedCollection",
  "../CDBaseRequest",
], (Backbone, _, CDProductModel, CDSharedCollection, CDBaseRequest)->

  CDWishlistCollection = Backbone.Collection.extend({
    WishlistId: ""
    model: (attr,options) ->
      return new CDProductModel(attr,{reset:true, parse:false})

    requestObjects:{
      "SearchWishlistProducts":{
        protocol: "POST",
        subDomain: "services",
        requestUrl: "/Subscriber/SearchList",
        requestData: { }
      }
    },

    sortBy: 2,
    sortDirection: 2

    parse: (response, options)->
      results = []
      if response.SearchWishlistProducts?.Products?
        results = (wishlistProduct for wishlistProduct in response.SearchWishlistProducts.Products)
      return results

    refresh: (callback) ->
      error = false
      wishListId = null #can not re-use id if a user logs-out and logs-in in the same tv session
      listName = "msh_wishlist"

      loadProducts = =>
        if(!@listId)
          error = "no wishlist id defined"
          callback?(error)

        @requestObjects.SearchWishlistProducts.requestData = {
          WishlistId: @listId,
          IncludePurchased: true
          SortBy: @sortBy
          SortDirection: @sortDirection
        }

        model = @
        model.fetch
          reset:true,
          success: ->
            callback?(error)

      CDBaseRequest.request "POST", "services", "/Subscriber/SearchLists", {}, (error, response) =>
        if response and response.Lists?
          listId = list.Id for list in response.Lists when list.Name is listName

        if not listId
          @listId = null
          CDBaseRequest.request "POST", "services", "/Subscriber/UpdateList", {
            "List": {
              "Comments" : listName, "Name" : listName, "Status" : 1
            }
          }, (error, create_response) =>
            if create_response and create_response.List
              listId = create_response.List.Id
            @listId = listId

            loadProducts()
        else
            @listId = listId
            loadProducts()

    checkIfInWishList: (productId, callback) ->
      @refresh =>
        inWishList = !!@.findWhere({ Id: productId })
        callback?(inWishList)

    addToWishList: (productId, callback) ->
        wishListId = @listId
        CDBaseRequest.request "POST", "services", "/Subscriber/UpdateWishlistProducts", {
          WishlistId:wishListId
          WishlistProductsToAddOrUpdate:[{
            "ProductId":productId,
            "Status": 1,
            "Priority":1,
            "DesiredQuantity" : 1
          }]
        }, (error, response) =>
          callback?(error, response.WishlistProductsAddedOrUpdated?.length is 1)

    removeFromWishList: (productId, callback) ->
        wishListId = @listId

        CDBaseRequest.request "POST", "services", "/Subscriber/SearchWishlistProducts", { WishlistId: wishListId, IncludePurchased: true}, (error, response) =>
          wishListProductId = null
          if response.WishlistProducts?
            wishListProductId = wishlistProduct.Id for wishlistProduct in response.WishlistProducts when wishlistProduct.Product.Id is productId

          CDBaseRequest.request "POST", "services", "/Subscriber/UpdateWishlistProducts", {
            WishlistId:wishListId
            WishlistProductsToRemove:[wishListProductId]
          }, (error, response) =>
            callback?(error, response.WishlistProductsAddedOrUpdated?.length is 0)

  })
  _.extend(CDWishlistCollection.prototype, CDSharedCollection)
  return new CDWishlistCollection() #make singleton