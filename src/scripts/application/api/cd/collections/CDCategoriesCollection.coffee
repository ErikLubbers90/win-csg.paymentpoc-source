define [
  "backbone",
  "underscore",
  "../models/CDCategory",
], (Backbone, _, CDCategoryModel)->

  CDCategoriesCollection = Backbone.Collection.extend({
    model: CDCategoryModel
  })