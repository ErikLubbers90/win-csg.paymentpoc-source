define [
  "backbone",
  "underscore",
  "../models/CDProduct",
  "../models/CDBundle"
], (Backbone, _, CDProductModel, CDBundleModel)->

  CDProductCollection = Backbone.Collection.extend({
    model: (attr,options) ->

      _.extend(options, {parse:false})

      #1 - Al a Carte
      #2 - Bundle
      #3 - Picklist
      #4 - Episodic Bundle
      #5 - Series Container/Marketing Container
      switch(attr.StructureType)
        when 1      #product
          return new CDProductModel(attr,options)
          break
        when 2, 4  #Bundle/Episodic Bundle
          return new CDBundleModel(attr,options)
          break
        else
          return new CDProductModel(attr,options)
          break
  })