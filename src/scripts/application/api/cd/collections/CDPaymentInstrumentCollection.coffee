define [
  "config",
  "backbone"
  "framework",
  "application/api/cd/CDPaymentManagement"
], (Config, Backbone, TWC, CDPaymentManagement)->

  # Backbone collection with payment instruments information
  CDPaymentInstrumentCollection = Backbone.Collection.extend({
    initialize: (data, options) ->
      @currency = options.currency if options.currency

#    model: Backbone.Model
    fetch: (options) ->
      options = if options then _.clone(options) else {}
      if (options.parse is undefined) then options.parse = true
      collection  = this
      success = options.success
      options.success = (resp) ->
        method = if options.reset then 'reset' else 'set'
        collection[method](resp, options)
        if (success) then success(collection, resp, options)
        collection.trigger('sync', collection, resp, options)

      error = options.error
      options.error = (resp) ->
        if (error) then error(collection, resp, options)
        collection.trigger('error', collection, resp, options)

      CDPaymentManagement.retrieveWallet @currency, (error, response) =>
        if not error
          options.success?(response)
        else
          options.error?(error)

    parse:(data)->
      @subscriberPaymentInstruments = _.filter(data.PaymentInstruments, (paymentInstrument) -> paymentInstrument.Type in Config.supportedPaymentOptions )
      @subscriberPaymentInstruments

    getDefaultPaymentInstrument: ->
      _.findWhere(@subscriberPaymentInstruments, {"Default": true})
  })