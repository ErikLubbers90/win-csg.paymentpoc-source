define [
  "backbone",
  "underscore",
  "../models/CDProduct",
  "./CDSharedCollection",
  "../CDBaseRequest"
], (Backbone, _, CDProduct, CDSharedCollection, CDBaseRequest)->

  CDFavoritesCollection = Backbone.Collection.extend({
    model: (attr,options) ->
      return new CDProduct(attr,{reset:true, parse:false})

    requestObjects:{
      favorites: {
        protocol: "POST",
        subDomain: "services",
        requestUrl: "/Subscriber/SearchFavoriteProducts"
      }
    },

    parse: (response, options)->
      result = []

      if response.favorites
        result = (favorite.Product for favorite in response.favorites.FavoriteProducts)
      return result
  })
  _.extend(CDFavoritesCollection.prototype, CDSharedCollection)
  return CDFavoritesCollection