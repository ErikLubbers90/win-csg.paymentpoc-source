define [
  "jquery"
  "underscore"
  "framework"
  "config"
  "./CDBaseRequest"
  "./models/CDDevice"
  "./models/CDSubscriber"
], ($, _, TWC, Config, CDBaseRequest, CDDevice, CDSubscriber)->

  # class to manage CD subscriber and session
  class CDSubscriberManagement
    @newSession: true

    # Helper method to execute login request
    # @private
    #
    # @param dataObj [Object] object with attributes to pass to the server
    # @param callback [Function(error)] function called when this method is completed
    @loginRequest: (dataObj, callback) ->
      CDBaseRequest.request "POST", "services", "/Subscriber/CreateSession", dataObj, (error, response)=>
        if not error

          #new session management since invision 16.4
          if response.SessionId? and (0 < response.SessionId.length <= 38)
            @newSession = false
            log "CDSubscriberManagement - detected GUID based session"
          else
            log "CDSubscriberManagement - detected Token based session"

          #save new session data
          CDBaseRequest.sessionId = response.SessionId
          CDBaseRequest.authenticationType = response.AuthenticationType
          CDBaseRequest.guidanceRatingsCategories = response.GuidanceRatingsCategories
          CDBaseRequest.sessionCountry = response.Country
          CDBaseRequest.allowPinlessPurchase = response.AllowPinlessPurchase if response.AllowPinlessPurchase?

          #save new/updated user data
          CDBaseRequest.subscriber = new CDSubscriber(response.SessionSummary)

          @setPingSession() if not @newSession

        callback?(error)

    # Login with user credentials
    #
    # @param username [String] username of the subscriber
    # @param password [String] password of the subscriber
    # @param deviceSetPinCheck [Function(error)] set the device PIN
    # @param callback [Function(error)] function called when this method is completed
    @userLogin: (username, password, callback) ->
      log "CDSubscriberManagement - userLogin"
      #login create user authenticated session
      requestObj = {
          Login:username,
          Password:password
      }
      @loginRequest requestObj, (error) =>

        callback?(error)

    # Login with device credentials
    #
    # @param deviceId [String] unique id of the current device
    # @param authenticationKey [String] authentication token for the current device
    # @param callback [Function(error)] function called when this method is completed
    @deviceLogin: (deviceId, authenticationKey, callback) ->
      log "CDSubscriberManagement - deviceLogin"
      requestObj = {
        DeviceCredentials: {
          DeviceId: deviceId,
          AuthenticationKey: authenticationKey
        }
      }
      @loginRequest(requestObj, callback)

    # Login without credentials (anonymous)
    #
    # @param callback [Function(error)] function called when this method is completed
    @anonymousLogin: (callback) ->
      log "CDSubscriberManagement - anonymousLogin"
      requestObj = {
          Login:"hema.sampath@csgi.com",
          Password:"qaTester1"
      }
      # requestObj = {}
      # CDBaseRequest.authenticationType = CDBaseRequest.authenticationTypeEnum.ANONYMOUS
      @loginRequest(requestObj, callback)

    # Logout the current subscriber
    #
    # @param callback [Function(error)] function called when this method is completed
    @logout: (callback) ->
      log "CDSubscriberManagement - logout"
      #unregister the current device
      @unRegisterDevice (error) =>
        #clear all subscriber data
        CDBaseRequest.subscriber.clearStorage()
        CDBaseRequest.subscriber = new CDSubscriber()

        #clear session
        CDBaseRequest.sessionId = null
        CDBaseRequest.sessionCountry = null

        #create an anonymous session
        @anonymousLogin(callback)

    # Register the current device with the subscriber account
    #
    # @param username [String] username of the subscriber
    # @param password [String] password of the subscriber
    # @param deviceId [String] unique id of the current device
    # @param deviceSerial [String] serial number of the current device
    # @param deviceNickname [String] nickname of the current device
    # @param callback [Function(error)] function called when this method is completed
    @registerDevice: (username, password, deviceId, deviceSerial, deviceNickname, devicePinEnabled, callback) ->
      log "CDSubscriberManagement - registerDevice"
      createSession = true  #create new device based session when device is created
      distributionChannelId = Config.csg[Config.environment].DistributionChannelId
      CDBaseRequest.request "POST", "services", "/Subscriber/RegisterDevice", {
        "Login": username,
        "Password": password,
        "DistributionChannel": if _.isObject(distributionChannelId) then distributionChannelId[Config.platform] else distributionChannelId,
        "PhysicalDevice": {
          "DeviceId": deviceId,
          "DeviceTypeCode": Config[Config.platform].deviceType,
          "PhysicalDeviceTypeCode": Config[Config.platform].physicalDeviceCode,
          "SerialNumber": deviceSerial
        },
        "Nickname": deviceNickname,
        "CreateSession": createSession,
        "AllowPinlessPurchase": !devicePinEnabled
      }, (error, response)=>
        if not error
          #save device information
          CDBaseRequest.remainingDeviceAssociations = response.RemainingDeviceAssociations
          CDBaseRequest.device = new CDDevice(response.PhysicalDevice)

          #save new session data
          if createSession
            CDBaseRequest.allowPinlessPurchase = !devicePinEnabled
            CDBaseRequest.sessionId = response.SessionId
            CDBaseRequest.authenticationType = CDBaseRequest.authenticationTypeEnum.DEVICE_AUTHENTICATED

          #save new/updated user data
          CDBaseRequest.subscriber = new CDSubscriber(response.SessionSummary)

        callback?(error)

    # unregister the current device from the subscriber account
    #
    # @param callback [Function(error)] function called when this method is completed
    @unRegisterDevice: (callback) ->

      #if the user is not logged in directly reset all stored device data and call callback method
      if not @isLoggedIn()
        CDBaseRequest.remainingDeviceAssociations = null
        CDBaseRequest.device.clearStorage()
        CDBaseRequest.device = new CDDevice()
        return callback?()

      log "CDSubscriberManagement - unRegisterDevice"
      CDBaseRequest.request "POST", "services", "/Subscriber/UnregisterDevice", {
          "DeviceId": CDBaseRequest.device.get("DeviceId"),
          "AuthenticationKey": CDBaseRequest.device.get("AuthenticationKey")
        }, (error, response)=>
          CDBaseRequest.remainingDeviceAssociations = null
          CDBaseRequest.device.clearStorage()
          CDBaseRequest.device = new CDDevice()
          callback?(error)

    # Create a session
    #
    # @param callback [Function(error)] function called when this method is completed
    @createSession: (callback) ->
      log "CDSubscriberManagement - createSession"
      deviceId = CDBaseRequest.device.get("DeviceId")
      authenticationKey = CDBaseRequest.device.get("AuthenticationKey")

      #if device id and authenticationKey available create a device session
      if deviceId && authenticationKey
        @deviceLogin deviceId, authenticationKey, (error)=>
          if error and not error.handled
            @anonymousLogin(callback)
          else
            callback?(error)

      #else create a anonymous session
      else
        @anonymousLogin(callback)

      @setPingSession()



    @setPingSession: ->
      #clear previous running session ping timer
      if @_pingSessionTimer
        clearInterval(@_pingSessionTimer)
        @_pingSessionTimer = null

      #create session ping timer
      if not @newSession
        @_pingSessionTimer = setInterval( =>
          @pingSession() #call the ping session
        , CDBaseRequest.pingSessionInterval)

    # send a ping request to the server to keep the session alive.
    @pingSession: ->
      if CDBaseRequest.sessionId
        CDBaseRequest.request "POST", "services", "/Subscriber/PingSession", {}, (error, response) =>

    # Returns the current login state of the user
    #
    # @return bool is user logged in
    @isLoggedIn: ->
      CDBaseRequest.authenticationType > CDBaseRequest.authenticationTypeEnum.UNAUTHENTICATED

    # Returns the current login state of the user
    #
    # @param username [String] username of the  user
    # @param callback [Function(error, success)] function called when this method is completed
    @isValidUser: (username, callback)->
      log "CDSubscriberManagement - validateCredentials"
      CDBaseRequest.request "POST", "services", "/Subscriber/ValidateCredentials", {
        "Login":username
      }, (error, response) ->
        if error
          callback?(error, true)
        else
          callback?(error, false)

    # Determine whether a subscriber has a device session promotion pin
    #
    # @param callback [Function(error, hasPin)] function called when this method is completed
    @retrieveDevicePinStatus: (callback) ->

      log "CDSubscriberManagement - retrieveDevicePinStatus"
      CDBaseRequest.request "POST", "services", "/Subscriber/RetrieveDevicePinStatus", {}, (error, response) ->
        if not error and response?.HasDevicePin?
          callback?(error, response.HasDevicePin)
        else
          callback?(error, false)

    # Update a subscribers device session promotion pin
    #
    # @param devicePin String pass null as value to remove devicepin
    # @param password String password of the user
    # @param callback [Function(error, response)] function called when this method is completed
    @updateDevicePin: (devicePin, password, callback) ->

      log "CDSubscriberManagement - updateDevicePin"
      CDBaseRequest.request "POST", "services", "/Subscriber/updateDevicePin", {
        "DevicePin": devicePin,
        "Password": password
      }, callback

    # Request a device Rendezvous Code
    #
    # @param deviceId [String] unique id of the current device
    # @param deviceSerial [String] serial number of the current device
    # @param callback [Function(error, RendezvousCode, CDDevice)] function called when this method is completed
    @generateDeviceRendezvousCode: (deviceId, deviceSerial, callback) ->

      log "CDSubscriberManagement - GenerateDeviceRendezvousCode"
      CDBaseRequest.request "POST", "services", "/Subscriber/GenerateDeviceRendezvousCode", {
        "DeviceId": deviceId,
        "DeviceType": Config[Config.platform].deviceType,
        "PhysicalDeviceType": Config[Config.platform].physicalDeviceCode,
        "SerialNumber":deviceSerial
      }, (error, response)=>
        if not error
          deviceModel = new CDDevice({
            AuthenticationKey: response.AuthenticationKey,
            DeviceId: deviceId,
            DeviceTypeCode: Config[Config.platform].deviceType,
            PhysicalDeviceTypeCode: Config[Config.platform].physicalDeviceCode,
            SerialNumber: deviceSerial
          })

          callback?(error, response.RendezvousCode, deviceModel)
        else
          callback?(error)
      , false

    # Check the status of the Rendezvous code
    # on success new session is automatically created, when code is expired automatically a new one is created
    #
    # @param rendezvousCode [String] unique id of the RendezvousCode
    # @param CDDevice [CDDevice] CDDevice model
    # @param callback [Function(error, success, RendezvousCode, CDDevice)] function called when this method is completed
    @verifyDeviceRendezvousCode: (rendezvousCode, deviceModel, callback) ->

      log "CDSubscriberManagement - RetrieveDeviceRendezvousStatus"
      CDBaseRequest.request "POST", "services", "/Subscriber/RetrieveDeviceRendezvousStatus", {
        "RendezvousCode": rendezvousCode
      }, (error, response)=>
        if not error
          switch response.Status
            when 1 #success
              CDBaseRequest.device = deviceModel
              @createSession =>
                callback?(error, true)
              break
            when 3, 4 #expired/invalid
              deviceUtils = TWC.SDK.Utils.get()
              deviceId =  deviceUtils.getDeviceId()
              deviceSerial = deviceUtils.getSerial()
              @generateDeviceRendezvousCode deviceId, deviceSerial, (error, newRendezvousCode, newDeviceModel)=>
                callback?(error, false, newRendezvousCode, newDeviceModel)
              break
            else
              callback?(error, false, rendezvousCode, deviceModel)
        else
          callback?(error)
      , false

    # Generate new user password and mail it to the user's email address
    #
    # @param username [String] username of the  user
    # @param callback [Function(error, success, response)] function called when this method is completed
    @generatePassword: (username, callback) ->

      log "CDSubsciberManagement - GeneratePassword"
      CDBaseRequest.request "POST", "services", "/Subscriber/GeneratePassword", {
        "Login": username
      }, (error, response)=>
        if not error
          callback?(error, true, response)
        else
          callback?(error)
      , false

    # Promote a device session to fully-authenticated (type 5)
    #
    # @param devicePin [String] the subscribers device session promotion pin
    # @param callback [Function(error, response)] function called when this method is completed
    @promoteDeviceSession: (devicePin, callback) ->
      log "CDSubsciberManagement - promoteDeviceSession"
      CDBaseRequest.request "POST", "services", "/Subscriber/PromoteDeviceSession", {
        "DevicePin": devicePin
      }, (error, response)->
        if not error
          CDBaseRequest.authenticationType = CDBaseRequest.authenticationTypeEnum.USER_AUTHENTICATED
          #overwrite the authenticated session id with a new one returned from the call
          CDBaseRequest.sessionId = response.SessionId if response?.SessionId

        callback?(error)

    # Revert a device session to authenticated (type 4)
    #
    # @param callback [Function(error)] function called when this method is completed
    @degradeDeviceSession: (callback) ->
      @.createSession(callback)

    # Retrieve an UltraViolet Account and User
    #
    # @param callback [Function(error)] function called when this method is completed
    @retrieveUvUser: (callback) ->
      log "CDSubsciberManagement - retrieveUvUser"
      CDBaseRequest.request "POST", "services", "/Subscriber/RetrieveUvUser", {
      }, (error, response)->
        if not error
          callback?(error, response)
        else
          callback?(error)
