define [
  "backbone",
  "underscore",
  "../collections/CDProductCollection"
], (Backbone, _, CDProductCollection)->

  CDPlaylist = Backbone.Model.extend({
    relations: {
      "Products": CDProductCollection
    },
    defaults:{
      "Name":""
    }
  })