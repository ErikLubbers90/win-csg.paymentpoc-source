define [
    "backbone",
    "underscore",
    "./CDSharedModel",
    "./CDViewContent",
    "../CDBaseRequest",
    "../collections/CDCategoriesCollection",
    "application/helpers/submenuHelper"
  ], (Backbone, _, CDSharedModel, CDViewContent, CDBaseRequest, CDCategoriesCollection, SubMenuHelper)->

  CDFilters = Backbone.Model.extend({
    relations: {
      Categories : CDCategoriesCollection
      People  : Backbone.Collection
      GuidanceRatings  : Backbone.Collection
      Sort  : Backbone.Collection
    },
    defaults:{
      Categories : []
      People  : []
      GuidanceRatings  : []
      Sort  : []
    },
    requestObjects:{
      categoryData:{
        protocol: "GET",
        subDomain: "metadata",
        requestUrl: "/Categories/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>"
      },
#      peopleData:{
#        protocol: "GET",
#        subDomain: "metadata",
#        requestUrl: "/PeopleForBrowse/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>"
#      }
#      sortCategoryData:{
#        protocol: "GET",
#        subDomain: "metadata",
#        requestUrl: "/Codes/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/Type/294"
#      },
#      sortData:{
#        protocol: "GET",
#        subDomain: "metadata",
#        requestUrl: "/Codes/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/Type/295"
#      }
      ratingCategoryData:{
        protocol: "GET",
        subDomain: "metadata",
        requestUrl: "/Codes/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/Type/32",
        requestData: {}
      },
#      ratingCategoryCountryData:{
#        protocol: "GET",
#        subDomain: "metadata",
#        requestUrl: "/Codes/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/Type/314",
#        requestData: {}
#      },
      ratingData:{
        protocol: "GET",
        subDomain: "metadata",
        requestUrl: "/Codes/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/Type/33",
        requestData: {}
      }
    },

    initialize: (attributes, options) ->

    #do not refresh data since filter data is singleton
    refresh: (callback) ->
      callback?()

    parse: (response, options)->
      result = {}

      result.People = []#response.peopleData.People
      result.Categories = response.categoryData.Categories
      result.Sort = []#@parseSort(response.sortCategoryData, response.sortData)
      result.GuidanceRatings = @parseGuidanceRating(response.ratingCategoryData, response.ratingData)
      SubMenuHelper.setCategoriesFilter(({Name: cat.Name, Id: cat.Id} for cat in result.Categories))

      return result

    parseSort: (sortCategory, sort) ->
      []

    parseGuidanceRating: (ratingCategory, rating) ->
      matchedGuidanceRatings = [];

      #get all the ratings for the found rating systems
      for ratingCode in rating.Codes
        categoryCode = _.findWhere(ratingCode.AdditionalProperties, {Key:"category_code"})?.Value
        if parseInt(categoryCode) in CDBaseRequest.guidanceRatingsCategories
          CategoryName = _.findWhere(ratingCategory.Codes, {Value:categoryCode})?.Name or ""
          matchedGuidanceRatings.push({Description: ratingCode.Description, ratingCategoryName: CategoryName, Name: ratingCode.Name, Id: ratingCode.Value})

      return matchedGuidanceRatings

    getNormalizedCategories: ->
      categories = []
      @.get("Categories").each (categoryModel) ->
        categories.push {Name: categoryModel.get("Name"), Id: categoryModel.get("Id")}
      return categories

    getIDForExternalReference: (externalReference)->
      @categoryId = ""
      @.get("Categories").each (categoryModel) =>
        if categoryModel.get("ExternalReference") is externalReference
          @categoryId = categoryModel.get("Id")
        else
          categoryModel.get("Children").each (subCategoryModel) =>
            @categoryId = subCategoryModel.get("Id") if subCategoryModel.get("ExternalReference") is externalReference
            true
        true
      return @categoryId
  })
  _.extend(CDFilters.prototype, CDSharedModel)
  return new CDFilters()