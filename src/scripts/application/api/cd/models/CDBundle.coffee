define [
  "backbone",
  "underscore",
  "moment",
  "./CDFilters",
  "./CDSharedModel",
  "./CDViewContent",
  "../CDBaseRequest",
  "../CDSubscriberManagement",
  "../collections/CDProductCollection",
  "../collections/CDMediaCollection",
  "../collections/CDPricingPlanCollection"
  "../collections/CDBundleProductCollection"
], (Backbone, _, Moment, CDFilters, CDSharedModel, CDViewContent, CDBaseRequest, CDSubscriberManagement, CDProductCollection, CDMediaCollection, CDPricingPlanCollection, CDBundleProductCollection)->

  CDBundle = Backbone.Model.extend({

    relations: {
      AvailableMedia: CDMediaCollection
      Categories: Backbone.Collection
      GuidanceRatings: Backbone.Collection
      PricingPlans : CDPricingPlanCollection
      Previews: Backbone.Collection
      OrderPricingPlans : CDPricingPlanCollection
      BundleChildren : CDBundleProductCollection
    },
    defaults: {
      AdTags: ""
      AllowMultiplePurchases: false
      AvailableMedia: []
      BundleChildren: []
      Categories: []
      DetailImageUrl: ""
      Genre: ""
      GuidanceRatings: []
      ImageUrl: ""
      IndexName: ""
      LineOfBusiness: 0
      Name: ""
      PresentationTypeCode: 0
      PricingPlans: []
      ProductionCompany: ""
      OrderPricingPlans: []
      RatingReason: ""
      Recommendations: []
      Runtime: 0
      ReleaseDate: ""
      ReferenceDate: ""
      Standalone: true
      SeasonNumber: ""
      SeriesName: ""
      StructureType: 2
      Suggestions: []
      Studio: ""
      ThumbnailUrl: ""
      Previews: []

      #context
      EntitledPricingPlanId: 0
      LockerItemId: 0
      SubscriberProductId: ""
    }

    requestObjects:{
      "metadata":{
        protocol: "GET",
        subDomain: "metadata",
        requestBaseUrl: "/Product/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/Id/{bundleId}",
        requestData: {}
      },
      "context":{
        protocol: "POST",
        subDomain: "services",
        requestUrl: "/Subscriber/RetrieveProductContext",
        requestData: { }
      }
    },

    initialize: ->
      @.on('change:ImageUrl', @.updateFeaturedImages, @) #listen for changes to update featured image urls
      @updateFeaturedImages()                            #trigger initial insert manually

    parse: (response, options)->
      product = response.metadata.Product
      context = response.context.ProductContext

      result = _.clone(product)
      result.AvailableMedia = _.pluck(product.AvailableMedia, 'Value')
      result.People = _.groupBy(product.People, "Title")
      result.GuidanceRatings = @parseGuidanceRating(product.GuidanceRatings)
      result.ReleaseDate = Moment(product.ReleaseDate).format("YYYY")
      result.RatingReason = if product.RatingReason? then product.RatingReason else ""

      #filter pricing plans down to user order-able pricing plan for bundle
      result.PricingPlans
      result.OrderPricingPlans = @parseOrderPricingPlans(product.PricingPlans, context.OrderablePricingPlans)

      # add relevant context properties to result
      result.EntitledPricingPlanId = context.EntitledPricingPlanId if context?.EntitledPricingPlanId
      result.LockerItemId = context.LockerItemId if context?.LockerItemId
      result.SubscriberProductId = context.SubscriberProductId if context?.SubscriberProductId

      #parse the bundle children
      result.BundleChildren = @parseChildren(product.ReferencedProducts, product.BundleChildren, context.EpisodicContext or context.BundleContexts)


      #remove none used fields
      delete result["ReferencedProducts"]

      return result

    parseChildren: (ReferencedProducts, BundleChildren, childContext)->
      childProducts = []
      validContext = []
      for context in childContext
        validContext.push switch context.StructureType
          when 1 #match bundle with product
            {
              ProductId: context.ProductContext.ProductId
              OrderablePricingPlans: context.OrderablePricingPlans
            }
          when 2 #match bundle with bundle
            {
              ProductId: context.BundleContexts.ProductId
              OrderablePricingPlans: context.OrderablePricingPlans
            }
          when 4 #match bundle with episodic bundle
            {
              ProductId: context.EpisodicContext.ProductId
              OrderablePricingPlans: context.OrderablePricingPlans
            }
          else   #episodic bundle
            context

      for bundleChild in BundleChildren
        #get the bundleChild metadata and context
        childProductContext = _.findWhere(validContext, {ProductId: bundleChild.ProductId})
        childProductMetadata = _.findWhere(ReferencedProducts, {Key: bundleChild.ProductId})["Value"]

        #combine child product metadata and context
        childProduct = _.extend({}, childProductMetadata, childProductContext)

        #filter pricing plans down to user order-able pricing plan for child product
        childProduct.PricingPlans = bundleChild.PricingPlans
        childProduct.OrderPricingPlans = @parseOrderPricingPlans(bundleChild.PricingPlans, childProductContext["OrderablePricingPlans"])

        #remove none used fields
        delete childProduct["OrderablePricingPlans"]

        childProducts.push(childProduct)

      return childProducts

    parseOrderPricingPlans: (PricingPlans, OrderAblePricingPlans=[])->
      subscriberPricingPlans = []
      dateNow = Moment().valueOf()
      for PricingPlan in PricingPlans
        OrderAblePricingPlan = _.findWhere(OrderAblePricingPlans, {Id: PricingPlan.Id})
        if OrderAblePricingPlan

          #filter out pricing plans that are disabled/expired
          if PricingPlan.OfferingEnd? and PricingPlan.OfferingEnd.valueOf() < dateNow then continue

          subscriberPricingPlans.push _.extend({}, PricingPlan, OrderAblePricingPlan)
      return subscriberPricingPlans


    parseGuidanceRating: (GuidanceRatings = [])->
      return GuidanceRatings if GuidanceRatings.length is 0
      matchedGuidanceRatings = [];

      guidanceRatingInfo = CDFilters.get("GuidanceRatings")
      #try to find subscriber rating code
      subscriberGuidanceRatingsCodes = CDBaseRequest.guidanceRatingsCategories
      for subscriberGuidanceRatingsCode in subscriberGuidanceRatingsCodes
        guidanceRating = _.findWhere(GuidanceRatings, {CategoryCode: subscriberGuidanceRatingsCode});
        if guidanceRating
          ratingInfo = guidanceRatingInfo.find (o)->
            return o.get("Id") == "" + guidanceRating.Code
          guidanceRating.Description = ratingInfo.get("Description") if ratingInfo?
          matchedGuidanceRatings.push(guidanceRating)

      return matchedGuidanceRatings

    refresh: (callback) ->
      error = false
      bundleId = @.get("Id")
      if(!bundleId)
        error = "no bundle id defined"
        callback?(error)

      @requestObjects.metadata.requestUrl = @requestObjects.metadata.requestBaseUrl.replace("{bundleId}", bundleId)
      if CDSubscriberManagement.isLoggedIn()
        @requestObjects.context.requestData = {
          ProductId: bundleId,
          IncludeEntitlementContext: true,
          IncludeViewingContext: true,
          IncludeOrderablePricingPlans: true
        }
      else
        @requestObjects.context.requestData = {
          ProductId: bundleId,
          IncludeOrderablePricingPlans: true
        }

      model = @
      model.fetch
        reset:true,
        success: ->
          callback?(error)

    ###
    Extras methods
    ###
    hasExtras: ()->
      extrasId = _.find @.get("AdditionalProperties"), (e)->
        e.Name is "TVExtrasID"
      return extrasId?

    getExtrasId: ()->
      extrasId = _.find @.get("AdditionalProperties"), (e)->
        e.Name is "ExtrasId"

      return extrasId.Values[0]


    updateFeaturedImages: ()->
      imageUrl = @.get("ImageUrl")
      @.set("FeaturedImageBig", imageUrl.replace("{id}", "featured_selected"))
      @.set("FeaturedImageLandscape", imageUrl.replace("{id}", "featured_notselected"))
      @.set("FeaturedImageSmall", imageUrl.replace("{id}", "featured_secondcolumn"))
      @.set("ThumbnailLandscape", imageUrl.replace("{id}", "thumbnail_landscape"))
      @.set("BackgroundImage", imageUrl.replace("{id}", "background_focus"))
      @.set("BackgroundImageBlurred", imageUrl.replace("{id}", "background_blur"))
      @.set("FeaturedLargeBackground", imageUrl.replace("{id}", "featured_selected_bg"))
      @.set("FeaturedLargeForeground", imageUrl.replace("{id}.jpg", "featured_selected_transparent.png"))
      @.set("FeaturedSmallBackground", imageUrl.replace("{id}", "featured_secondcolumn_bg"))
      @.set("FeaturedSmallForeground", imageUrl.replace("{id}.jpg", "featured_secondcolumn_transparent.png"))


  })
  _.extend(CDBundle.prototype, CDSharedModel);
  return CDBundle