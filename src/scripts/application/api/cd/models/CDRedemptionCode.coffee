define [
    "config",
    "backbone",
    "underscore",
    "./CDSharedModel",
    "application/api/cd/collections/CDProductCollection",
    "application/api/cd/CDPaymentManagement"
  ], (Config, Backbone, _, CDSharedModel, CDProductCollection, CDPaymentManagement)->

  CDRedemptionCode = Backbone.Model.extend({

    relations: {
      Products: CDProductCollection
    },
    defaults: {
      PageCount: 0
      Products: []
      RecordCount: 0
    },
    requestObjects:{
      Redemption: {
        protocol: "POST",
        subDomain: "services",
        requestUrl: "/Subscriber/SearchProductsByCoupon",
        requestData: {}
      }
    },

    initialize: (attributes, options) ->
      if options.redemptionCode?
        @requestObjects.Redemption.requestData = {
          RedemptionCode: options.redemptionCode
        }

    parse: (response)->
      results = {}

      if response.Coupon?
        results.Coupon = new Backbone.Model()

      results.Products = response.Products if response.Products?
      results.PageCount = response.PageCount if response.PageCount?
      results.RecordCount = response.RecordCount if response.RecordCount?

      return results
  })