define [
  "backbone"
  "framework"
], (Backbone, TWC)->

  # Backbone model with device information
  CDDevice = Backbone.Model.extend({
    defaults: {
      AddDate: "",
      AuthenticationKey: "",
      DeviceId: "",
      DeviceTypeCode: 0,
      DeviceTypeName: "",
      Id: 0,
      ModDate: "",
      PhysicalDeviceTypeCode: 0,
      ProductLobCode: 0,
      SerialNumber: "",
      Status: 0,
      StatusName: ""
    },
    initialize: (attributes, options)->
      #load device data from storage if it is initialized empty
      if not attributes
        @loadFromStorage()
      #else store new data to storage
      else
        @saveToStorage()
    loadFromStorage: ->
      storage = TWC.SDK.Storage.get()
      jsonModel = storage.getItem("CDDevice")
      if jsonModel
        model = JSON.parse(jsonModel)
        @.set(model)
    saveToStorage: ->
      model = @.toJSON()
      jsonModel = JSON.stringify(model)
      storage = TWC.SDK.Storage.get()
      storage.setItem("CDDevice", jsonModel)
    clearStorage: ->
      storage = TWC.SDK.Storage.get()
      storage.removeItem("CDDevice")
  })