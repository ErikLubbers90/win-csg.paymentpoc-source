define [
  "backbone",
  "underscore",
  "./CDSharedModel",
  "./CDFilters",
  "../collections/CDProductCollection"
], (Backbone, _, CDSharedModel, CDFiltersModel, CDProductCollection)->

  # Backbone model with search results
  CDSearchProductsModel = Backbone.Model.extend({
    relations: {
      "Products": CDProductCollection
    },
    defaults:{
      #"TotalPages":0
      "Products":[]
      "TotalProducts": 0
      "TotalProductsPerPage": 0
    },

    requestObjects:{
      searchResults:{
        protocol: "POST",
        subDomain: "services",
        requestUrl: "/Subscriber/SearchProducts"
        requestData: {}
      }
    },
    pageNumber:1
    pageSize:18
    pageSizeCorrection: 1
    sortBy: 1                 # 1:Name, 2:PeerRating, 3:ReferenceDate, 4:Relevance (default)
    sortDirection: 1          # 1:Ascending (default), 2:Descending
    initialize: (attributes, options) ->
      if options.pageSize? then @pageSize = options.pageSize
      if options.sortBy? then @sortBy = options.sortBy
      if options.sortDirection? then @sortDirection = options.sortDirection
      if options.pageNumber? then @pageNumber = options.pageNumber
      if options.categories? then @categories = options.categories
      if options.guidanceRating? then @guidanceRating = options.guidanceRating

      @requestObjects.searchResults.requestData = {
        SearchString: if options.keyword? then options.keyword else ""
        PageSize: @pageSize
        PageNumber: @pageNumber
        SortBy: @sortBy
        SortDirection: @sortDirection
        Categories: if options.categories? then options.categories else [CDFiltersModel.getIDForExternalReference("Sony_Bravia_Search")]
        GuidanceRatings: @guidanceRating
#            People: options.people
      }

    parse: (response, options)->
      results = {}

      searchResults = response.searchResults
      #results.TotalPages = searchResults.PageCount if searchResults?.PageCount
      results.Products = searchResults.Products.slice(0,@pageSize) if searchResults?.Products
      results.TotalProducts = searchResults.RecordCount if searchResults?.RecordCount
      results.TotalProductsPerPage = @pageSize * @pageSizeCorrection

      return results
  })
  _.extend(CDSearchProductsModel.prototype, CDSharedModel)
  return CDSearchProductsModel
