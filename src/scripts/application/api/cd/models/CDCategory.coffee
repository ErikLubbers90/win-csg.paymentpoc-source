define [
  "backbone",
  "underscore"
], (Backbone, _)->

  CDCategory = Backbone.Model.extend({
    relations: {
      Children : Backbone.Collection
    },
    defaults:{
      Name: ""
      Children: []
    }
  })