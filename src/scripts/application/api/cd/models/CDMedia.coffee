define [
  "backbone",
  "underscore"
], (Backbone, _)->

  CDMedia = Backbone.Model.extend({
    defaults:{
      DisplayName: ""
      MediaUrl: ""
      Name: ""
      Type: 0
    }
  })