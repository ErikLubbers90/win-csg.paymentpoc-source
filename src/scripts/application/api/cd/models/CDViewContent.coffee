define [
  "backbone",
  "underscore",
  "config",
  "../CDBaseRequest",
  "../collections/CDMediaCollection"
], (Backbone, _, Config, CDBaseRequest, CDMediaCollection)->

  CDViewContent = Backbone.Model.extend({
    relations:{
      AvailableMedia: CDMediaCollection
      UpdatedPolicies: Backbone.Collection
      GuidanceRatings: Backbone.Collection
    },
    defaults:{
      AssociatedDeliveryCapability: 0
      AvailableMedia: []
      ContentUrl: ""
      LicenseRequestToken: ""
      MediaId: 0
      ProgressSeconds: 0
      UpdatedPolicies: []
      ViewContentReference: ""
      LicenceRequestUrl: "http://services.#{Config.csg[Config.environment].CDDomain}/v5.6/PlayReadyRightsManager.asmx"
    }

    activateClosedCaptions: (callback) ->
      CDBaseRequest.request "GET", "metadata", "/Media/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/Id/#{@.get("MediaId")}", {}, (error, response)=>
        if error
          callback?(error)
        else
          externalMarker =  response.Media[Config.closedCaptionsMapping.Container]
          externalType = _.findWhere(externalMarker[0], {ExternalTypeCode: Config.closedCaptionsMapping.ExternalTypeCode})
          externalTypeField = _.findWhere(externalType.Fields,{ExternalTypeFieldCode: Config.closedCaptionsMapping.ExternalTypeFieldCode}) if externalType and _.isObject(externalType)
          captionsUrl = externalTypeField?.FieldValue or false
          subtitles = []
          if captionsUrl
            subtitles.push {"id":"cc", "url":captionsUrl}
          callback?(error, subtitles)

    getExternalSubtitles: (callback)->
      CDBaseRequest.request "GET", "metadata", "/Media/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/Id/#{@.get("MediaId")}", {}, (error, response)=>
      #CDBaseRequest.request "GET", "metadata", "/Media/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/Id/90966", {}, (error, response)=>
        if error
          callback?(error)
        else
          captions = response.Media.ClosedCaptions?.ClosedCaptionSettings or []
          subtitles = []
          for caption in captions
            id = if caption.Language.split("-").length > 1 then caption.Language.split("-")[0] else caption.Language
            url = caption.Url #NOTE: 247 servers do you have CORS enabled, could result in issue
            forced = if caption.Type is 4 then true else false  #true if subtitle is narrative version
            subtitles.push {"language":id, "url":url, "forced": forced}
          callback?(error, subtitles)

    getMetaInformation: (callback)->
      CDBaseRequest.request "GET", "metadata", "/Media/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/Id/#{@.get("MediaId")}", {}, (error, response)=>
        #CDBaseRequest.request "GET", "metadata", "/Media/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/Id/90966", {}, (error, response)=>
        if error
          callback?(error)
        else
          chapterData = response.Media.Chapters or []
          chapters = []
          effectiveEndSeconds = response.Media.EffectiveEndSeconds or 0
          for chapter in chapterData
            position = chapter.PositionSeconds or 0
            url = chapter.ThumbnailUrl
            chapters.push {"Position":position, "ThumbnailUrl":url}
          callback?(error, chapters, effectiveEndSeconds)

  })