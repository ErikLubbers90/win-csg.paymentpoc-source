define [
    "config",
    "backbone",
    "underscore",
    "framework",
    "moment",
    "./CDSharedModel",
    "./CDViewContent",
    "./CDFilters",
    "../CDBaseRequest",
    "../CDSubscriberManagement",
    "../collections/CDMediaCollection",
    "../collections/CDPricingPlanCollection"
], (Config, Backbone, _, TWC, Moment, CDSharedModel, CDViewContent, CDFilters, CDBaseRequest, CDSubscriberManagement, CDMediaCollection, CDPricingPlanCollection)->

  CDProduct = Backbone.Model.extend({

    relations: {
      AvailableMedia: CDMediaCollection
      Categories: Backbone.Collection
      GuidanceRatings: Backbone.Collection
      People: Backbone.Collection
      Previews: Backbone.Collection
      PricingPlans : CDPricingPlanCollection
      OrderPricingPlans : CDPricingPlanCollection
      PurchasePolicies: Backbone.Collection
    },
    defaults: {
      AdTags: ""
      AllowMultiplePurchases: false
      AspectRatio: ""
      AvailableMedia: []
      BoxOfficeGross: 0
      Categories: []
      ClosedCaptioning: false
      ContentLanguages: ""
      Copyright: ""
      Description: ""
      DetailImageUrl: ""
      DistributorName: ""
      GuidanceRatings: []
      ImageUrl: ""
      IndexName: ""
      LineOfBusiness: 0
      LicenseExpirationTimeString: "" 
      Name: ""
      Owner: ""
      People: {}
      PlotSummary: ""
      PresentationTypeCode: 0
      Previews: []
      PricingPlans: []
      OrderPricingPlans: []
      ProductionCompany: ""
      ProductionCountry: ""
      ProductionStatus: ""
      Quality: []
      Suggestions: []
      RatedR: false
      RatingReason: ""
      ReferenceDate: ""
      RelatedVideos: []
      RelatedImages: []
      ReleaseDate: ""
      Runtime: 0
      SeasonId: ""
      SeriesId: ""
      ShortDescription: ""
      SnippetDescription: ""
      Standalone: true
      StructureType: 1
      Studio: ""
      ThumbnailUrl: ""

      #context
      ContentProgressDate: ""
      ContentProgressPercentage: 0
      ContentProgressSeconds: 0
      EntitledPricingPlanId: 0
      Expired: false
      LockerItemId: 0
      PurchasePolicies: []
      SubscriberProductId: ""
      ViewingComplete: false
    }

    requestObjects:{
      "metadata":{
        protocol: "GET",
        subDomain: "metadata",
        requestBaseUrl: "/Product/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/Id/{productId}",
        requestData: {}
      },
      "context":{
        protocol: "POST",
        subDomain: "services",
        requestUrl: "/Subscriber/RetrieveProductContext",
        requestData: { }
      }
    },

    initialize: ->
      @.on('change:ImageUrl', @.updateFeaturedImages, @) #listen for changes to update featured image urls
      @updateFeaturedImages()                            #trigger initial insert manually

    parse: (response, options)->
      product = response.metadata.Product
      context = response.context.ProductContext
      result = _.clone(product)
      result.AvailableMedia = _.pluck(product.AvailableMedia, 'Value')
      result.People = _.groupBy(result.People, "Title")
      result.RatedR = @isRatedR(result.GuidanceRatings)
      result.GuidanceRatings = @parseGuidanceRating(result.GuidanceRatings)
      result.ReleaseDate = Moment(product.ReleaseDate).format("YYYY")
      result.RatingReason = "#{product.RatingReason[0].toUpperCase() + product.RatingReason.substring(1)}" if product.RatingReason? and product.RatingReason[0]


      #filter pricing plans down to user order-able pricing plan
      result.PricingPlans = product.PricingPlans
      result.OrderPricingPlans = @parseOrderPricingPlans(product.PricingPlans, context.OrderablePricingPlans)
      if product.RelatedMedia
        result.RelatedImages = _.filter product.RelatedMedia, (media)->
          media.Type is 1
        result.RelatedVideos = _.filter product.RelatedMedia, (media)->
          media.Type is 2

      # add relevant content to recommended products
      result.Suggestions = @parseSuggestions(product.ReferencedProducts, product.Suggestions) if  product.Suggestions

      # add relevant context properties to result
      result.ContentProgressDate = context.ContentProgressDate if context?.ContentProgressDate
      result.ContentProgressPercentage = context.ContentProgressPercentage if context?.ContentProgressPercentage
      result.ContentProgressSeconds = context.ContentProgressSeconds if context?.ContentProgressSeconds
      result.EntitledPricingPlanId = context.EntitledPricingPlanId if context?.EntitledPricingPlanId
      result.Expired = context.Expired if context?.Expired
      result.LockerItemId = context.LockerItemId if context?.LockerItemId
      result.PurchasePolicies = context.PurchasePolicies if context?.PurchasePolicies
      result.SubscriberProductId = context.SubscriberProductId if context?.SubscriberProductId
      result.ViewingComplete = context.ViewingComplete if context?.ViewingComplete
      result.Quality = @parseQuality(result.EntitledPricingPlanId, result.PricingPlans, result.OrderPricingPlans)

      bestAvailableQuality = ""
      for quality in result.Quality
        if "HDR" in result.Quality
          bestAvailableQuality = "HDR"
        else if "UHD" in result.Quality
          bestAvailableQuality = "UHD"
        else if "HD" in result.Quality
          bestAvailableQuality = "HD"
        else if "SD" in result.Quality
          bestAvailableQuality = "SD"

      result.AvailableQuality = if bestAvailableQuality in ["UHD", "HDR"] then "4K UHD" else bestAvailableQuality

      result.AvailableHDR = if bestAvailableQuality is "HDR" then "HDR" else ""
      result

    parseQuality: (entitledPricingPlanId, pricingPlans, OrderPricingPlans) ->
      qualities = []

      # parse available video quality SD/HD, if the user has an entitlement only show the entitled quality
      if entitledPricingPlanId?
        pricingPlansCollection = new CDPricingPlanCollection(pricingPlans)
        entitledPricingPlan = pricingPlansCollection.findWhere({"Id": entitledPricingPlanId})
        if entitledPricingPlan?
          quality = entitledPricingPlan.getQuality()
          if quality
            qualities.push quality

      else
        orderAblePricingPlansCollection = new CDPricingPlanCollection(OrderPricingPlans)
        availableQualities = []
        orderAblePricingPlansCollection.each (pricingPlan) ->
          quality = pricingPlan.getQuality()
          availableQualities.push quality if quality
        qualities = _.uniq(availableQualities)
      qualities

    parseOrderPricingPlans: (PricingPlans, OrderAblePricingPlans=[])->
      subscriberPricingPlans = []
      dateNow = Moment().valueOf()
      for PricingPlan in PricingPlans

        #some clients have pricing plan type and quality combined in the description "{type}_{quality}"
#        if PricingPlan.Description?.indexOf("_") > -1
#          [PricingPlan.AdditionalInformationText, PricingPlan.Description] = PricingPlan.Description.split("_")

        #find which pricing plan can be ordered
        OrderAblePricingPlan = _.findWhere(OrderAblePricingPlans, {Id: PricingPlan.Id})

        if OrderAblePricingPlan

          #filter out pricing plans that are disabled/expired
          if PricingPlan.OfferingEnd? and Moment(PricingPlan.OfferingEnd).valueOf() < dateNow then continue

          #filter out pricing plans that are not yet offered
          if PricingPlan.OfferingStart? and Moment(PricingPlan.OfferingStart).valueOf() > dateNow then continue

          # SPS: filter out UltraNotOrderable
          if _.find(PricingPlan.AdditionalProperties, (AdditionalProperty) ->
            return AdditionalProperty.Id is Config.csg[Config.environment].NotOrderable and AdditionalProperty.Values[0] is "True"
          )  then continue



          subscriberPricingPlans.push _.extend({}, PricingPlan, OrderAblePricingPlan)
      return subscriberPricingPlans

    parseSuggestions: (ReferencedProducts, Suggestions)->
      products = []
      for suggestedProductId in Suggestions
        suggestedProduct = _.findWhere(ReferencedProducts, {Key: suggestedProductId})["Value"]
        products.push(suggestedProduct)
      return products

    parseGuidanceRating: (GuidanceRatings = [])->
      return GuidanceRatings if GuidanceRatings.length is 0
      matchedGuidanceRatings = [];

      guidanceRatingInfo = CDFilters.get("GuidanceRatings")
      #try to find subscriber rating code
      subscriberGuidanceRatingsCodes = CDBaseRequest.guidanceRatingsCategories
      for subscriberGuidanceRatingsCode in subscriberGuidanceRatingsCodes
        guidanceRating = _.findWhere(GuidanceRatings, {CategoryCode: subscriberGuidanceRatingsCode});
        if guidanceRating
          ratingInfo = guidanceRatingInfo.find (o)->
            return o.get("Id") == "" + guidanceRating.Code
          guidanceRating.Description = ratingInfo.get("Description") if ratingInfo?
          matchedGuidanceRatings.push(guidanceRating)

      return matchedGuidanceRatings

    isRatedR: (GuidanceRatings = [])->
      if(GuidanceRatings.length)
        for rating in GuidanceRatings
          if rating.CategoryCode == 1 and rating.ExternalCode == "R"
            return true;
      return false;

    refresh: (callback) ->
      error = false
      productId = @.get("Id")
      if(!productId)
        error = "no product id defined"
        callback?(error)

      @requestObjects.metadata.requestUrl = @requestObjects.metadata.requestBaseUrl.replace("{productId}", productId)
      if CDSubscriberManagement.isLoggedIn()
        @requestObjects.context.requestData = {
          ProductId: productId,
          IncludeEntitlementContext: true,
          IncludeViewingContext: true,
          IncludeOrderablePricingPlans: true
        }
      else
        @requestObjects.context.requestData = {
          ProductId: productId,
          IncludeOrderablePricingPlans: true
        }

      model = @
      model.fetch
        reset:true,
        success: ()->
          callback?(error)

    ###
      Playback methods
    ###
    getPlaybackInformation: (callback, isTrailer=false, isRelatedMedia=false, isTenMinutePreview = false, isBonus = false, leanbackPosisiton = 0)->
      videoQuality = @.get("Quality")
      deliveryCode = null
      #fallback to UHD from HDR if HDR not available
      if videoQuality[0] in ["HDR", "UHD"]
        platformUtils = TWC.SDK.Utils.get()
        if platformUtils.isHDR() and videoQuality[0] is "HDR"
          #if quality is HDR and device supports HDR, then use HDR
          deliveryCode = Config[Config.platform].deliveryCapabilityCodes["HDR"]
        else if platformUtils.isUHD()
          #otherwise use UHD if device supports UHD
          deliveryCode = Config[Config.platform].deliveryCapabilityCodes["UHD"]
        else
          deliveryCode = Config[Config.platform].deliveryCapabilityCodes["HD"]
      else
        deliveryCode = Config[Config.platform].deliveryCapabilityCodes[videoQuality[0]]

      if isTenMinutePreview
        requestData = {
          "PreviewId" : @get10MinutePreviewId(),
          "ProductId" : @.get("Id"),
          "ViewContentType": 1
        }

      else if isBonus
        relatedMedia = @.get("RelatedMedia")
        bonusId = relatedMedia[leanbackPosisiton].Id
        requestData = {
          "ProductId" : @.get("Id"),
          "MediaId" : bonusId
          "ViewContentType": 2
        }

      else if isTrailer
        requestData = {
          "ProductId" : @.get("Id"),
          "PreviewId" : @.get("Previews").at(0).get("Id"),
          "ViewContentType": 1
        }
      else if isRelatedMedia
        requestData = {
          "PricingPlanId" : @.get("EntitledPricingPlanId"),
          "ProductId" : @.get("Id"),
          "MediaId" : parseInt(isRelatedMedia)
        }
      else
        requestData = {
          "PricingPlanId" : @.get("EntitledPricingPlanId"),
          "ProductId" : @.get("Id"),
          "DeliveryCapability" : deliveryCode
        }

      CDBaseRequest.request "POST", "services", "/Subscriber/ViewContent", requestData, (error, response)=>
        if error
          callback?(error)
        else
          callback?(error, new CDViewContent(response))

    get10MinutePreviewId: ->
      id = 0
      platformUtils = TWC.SDK.Utils.get()
      if @.get("Previews").length
        for preview in @.get("Previews").models
          if preview.get("Description")
            if preview.get("Description")=="HDR" and platformUtils.isHDR()
              id = preview.get("Id")
              return id
            else if preview.get("Description")=="SDR"
              id = preview.get("Id")
      id

    updateContentProgressRequest: (callback, time, stopped) ->

      #do not complete the request without a time
      return if !time

      viewingComplete = false
      #when viewer stops the stream this flag needs to be reset for multiple stream viewing check
      streamingComplete = stopped

      #viewing is complete when 95% is finished
      currentTimeInMins = (parseInt(time) / 60)
      totalRuntimeInMins = @.get("Runtime")
      viewingCompleteThreshold = (parseInt(totalRuntimeInMins) * 0.95)
      if currentTimeInMins > viewingCompleteThreshold
        viewingComplete = true
        streamingComplete = true
        time = 0


      submitData = {
        "ProductId" : @.get("Id"),
        "ProgressSeconds" : time,
        "ViewingComplete" : viewingComplete,
        "StreamingComplete" : streamingComplete,
        "PricingPlanId" : @.get("EntitledPricingPlanId")
      }

      CDBaseRequest.request "POST", "services", "/Subscriber/UpdateContentProgress", submitData, (error, response)=>
        if error
          log "error:" , error, response
          callback?(error)
        else
          #no direct user response needed
          log "success:" , response



    ###
      Favorites methods
    ###
    # Check if current product is in subscriber wish list
    #
    # @param productId String id of the product
    # @param callback [Function(error, inWhichList)] function called when this method is completed
    checkIfInFavorites: (productId, callback) ->
      CDBaseRequest.request "POST", "services", "/Subscriber/SearchFavoriteProducts", {
        ProductNameSearchString: "#{@.get("Name")}"
      }, (error, response)=>
        inFavorites = false
        if response.FavoriteProducts?.length > 0

          foundProduct = _.findWhere(response.FavoriteProducts, {ProductId: productId})
          inFavorites = foundProduct?
        callback?(inFavorites)

    addToFavorites: (callback) ->
      wishListId = CDWishlistCollection.listId
      CDBaseRequest.request "POST", "services", "/Subscriber/UpdateList", {
        AddAsFollow: true

        ItemsToAdd: [{
          "ProductId":@.get("Id")
        }]
        List:[{
          "Id":wishListId
        }]
      }, (error, response)=>
        if not response.Fault then success = true else false
        callback?(error, success)


    removeFromFavorites: (callback) ->
      listId = CDWishlistCollection.listId

      CDBaseRequest.request "POST", "services", "/Subscriber/SearchList", { Id: listId, IncludeEntitlementContext: true}, (error, response) =>
        productId = null
        if response.List?
          productId = list.ProductId for list in response.List.Items when list.ProductId is @.get("Id")
          # productId = ProductId for product in response.List.Items when product.ProductId is @.get("Id")
        CDBaseRequest.request "POST", "services", "/Subscriber/updateList", {
          AddAsFollow: false
          ItemsToRemove: [{
            "ProductId":productId
          }]
          List:[{
            "Id":listId
          }]
      }, (error, response)=>
        if not response.Fault then success = true else false
        callback?(error, success)

    ###
      Extras methods
    ###
    hasExtras: ()->
      extrasId = _.find @.get("AdditionalProperties"), (e)->
        e.Name is "TVExtrasID"
      return extrasId?

    getExtrasId: ()->
      extrasId = _.find @.get("AdditionalProperties"), (e)->
        e.Name is "TVExtrasID"
      return extrasId.Values[0]

    ###
      SONY
    ###
    updateFeaturedImages: ()->
      imageUrl = @.get("ImageUrl")
      @.set("FeaturedImageBig", imageUrl.replace("{id}", "featured_selected"))
      @.set("FeaturedImageLandscape", imageUrl.replace("{id}", "featured_notselected"))
      @.set("FeaturedImageSmall", imageUrl.replace("{id}", "featured_secondcolumn"))
      @.set("BackgroundImage", imageUrl.replace("{id}", "background_focus"))
      @.set("BackgroundImageBlurred", imageUrl.replace("{id}", "background_blur"))
      @.set("FeaturedLargeBackground", imageUrl.replace("{id}", "featured_selected_bg"))
      @.set("FeaturedLargeForeground", imageUrl.replace("{id}.jpg", "featured_selected_transparent.png"))
      @.set("FeaturedSmallBackground", imageUrl.replace("{id}", "featured_secondcolumn_bg"))
      @.set("FeaturedSmallForeground", imageUrl.replace("{id}.jpg", "featured_secondcolumn_transparent.png"))

      if @.get("EpisodeNumber")?
        episodeNumber = @.get("EpisodeNumber")
        episodeText = if episodeNumber < 10 then "0#{episodeNumber}" else episodeNumber
        @.set("ThumbnailLandscape", imageUrl.replace("{id}.jpg", "thumbnail_landscape_ep#{episodeText}.jpg"))

    ###
      Series episode information
    ###
    isSeries: ->
      return @.get("SeriesId") isnt ""

    getRelatedChapters: (callback)->
      return [] if not @isSeries
      parentBundle = new CDBundle(@.get("SeriesId"))

    getAlternativeStream: ->
      platformUtils = TWC.SDK.Utils.get()
      if platformUtils.isHDR() and @.get("AvailableMedia").findWhere({Description: "60FPSHDR"})
        return {
          key: "60FPSHDR",
          value: @.get("AvailableMedia").findWhere({Description: "60FPSHDR"}).get("Id")
        }
      else if @.get("AvailableMedia").findWhere({Description: "60FPSSDR"})
        return {
          key: "60FPSSDR",
          value: @.get("AvailableMedia").findWhere({Description: "60FPSHDR"}).get("Id")
        }
      else
        return false

  })
  _.extend(CDProduct.prototype, CDSharedModel)
  return CDProduct
