define [
  "config",
  "backbone",
  "underscore",
  "numeral"
  "application/popups/generic/alert",
  "application/api/cd/CDPaymentManagement",
  "i18n!../../../../lang/nls/manifest",
], (Config, Backbone, _, Numeral,  Alert, CDPaymentManagement, Manifest)->

  CDOrderQuote = Backbone.Model.extend({
    currencyTable:Config.currencyTable,
    defaults:{
      Currency: ""
      AppliedCoupons: ""
      AppliedDiscounts: ""
      DiscountAmount: 0
      GrossAmount: 0
      Items: []
      SubTotalAmount: 0
      TaxAmount: 0
      TaxItems: []
      TotalAmount: 0
      PaymentInstruments: null
    }

    initialize: (attributes, options) ->
      if options.pricingPlanId? then @pricingPlanId = options.pricingPlanId
      if options.productId? then @productId = options.productId
      if options.paymentInstrumentIds? then @paymentInstrumentIds = options.paymentInstrumentIds
      if options.promotionCodes? then @promotionCodes = options.promotionCodes
      if options.errorCallback? then @errorCallback = options.errorCallback
      if options.defaultPaymentInstrumentId? then @defaultPaymentInstrumentId = options.defaultPaymentInstrumentId

    fetch: (options) ->
      options = if options then _.clone(options) else {}
      if (options.parse is undefined) then options.parse = true
      model   = this
      success = options.success
      options.success = (resp) ->
        if (!model.set(model.parse(resp, options), options)) then return false
        if (success) then success(model, resp, options)
        model.trigger('sync', model, resp, options)

      error = options.error
      options.error = (resp) ->
        if (error) then error(model, resp, options)
        model.trigger('error', model, resp, options)

      CDPaymentManagement.calculateOrderQuote @paymentInstrumentIds, @pricingPlanId, @productId, @promotionCodes, (error, response) =>

        # Insufficient balance check
        if error.Code is 603 and _.indexOf(@paymentInstrumentIds, @defaultPaymentInstrumentId) < 0
          @paymentInstrumentIds.push @defaultPaymentInstrumentId

          CDPaymentManagement.calculateOrderQuote @paymentInstrumentIds, @pricingPlanId, @productId, @promotionCodes, (error, response) =>
            if not error and response?
              options.success?(response)
            else
              @errorHandler(error)

        else if error
          @errorHandler(error)
        else
          options.success?(response)

    refresh : (callback) ->
      collection = @
      collection.fetch({
        reset:true,
        success: ->
          callback?()
        error: (model, error) =>
          @errorHandler(error)
      })

    errorHandler: (error) ->
      messageAlert = new Alert()
      if error.Code is 603
        #insufficient funds error check
        message = Manifest.payment_insufficient_funds_message    #error.Message
        title = Manifest.payment_insufficient_funds_title
        back = null
      else
        #generic
        message = CDPaymentManagement.orderErrors(error)
        title = Manifest.popup_error_title
        back = @errorCallback

      messageAlert.load
        data: [title, message, Manifest.popup_exit_close, back]

    parse: (response)->
      results = {}

      orderQuote = if response.Quote? then response.Quote else {}
      results = _.clone(orderQuote)

      results.Currency = orderQuote.Currency if orderQuote.Currency?
      results.PaymentInstruments = @amountAsPrice(response.PaymentInstruments, results.Currency) if response.PaymentInstruments?.length
      results.AppliedDiscounts = orderQuote.AppliedDiscounts if orderQuote.AppliedDiscounts?
      results.DiscountAmount = orderQuote.DiscountAmount if orderQuote.DiscountAmount?
      results.DiscountAmountAsPrice = @.getPrice(orderQuote.DiscountAmount, results.Currency) if orderQuote.DiscountAmount?
      results.GrossAmount = @.getPrice(orderQuote.GrossAmount, results.Currency) if orderQuote.GrossAmount?
      results.Items = orderQuote.Items if orderQuote.Items?
      results.SubTotalAmount = @.getPrice(orderQuote.SubTotalAmount, results.Currency) if orderQuote.SubTotalAmount?
      results.TaxAmount = @.getPrice(orderQuote.TaxAmount, results.Currency) if orderQuote.TaxAmount?
      results.TaxItems = @amountAsPrice(orderQuote.TaxItems, results.Currency) if orderQuote.TaxItems?
      results.TotalAmount = orderQuote.TotalAmount if orderQuote.TotalAmount?
      results.TotalAmountAsPrice = @.getPrice(orderQuote.TotalAmount, results.Currency) if orderQuote.TotalAmount?
      return results

    # Go through all items in the list and add the price (currency symbol + amount)
    amountAsPrice: (items, currency) ->
      _.each(items,
        (item) =>
          item.AmountAsPrice = @getPrice(item.Amount, currency)
      )
      items

    getPrice: (price, currency) ->
      currencySymbol = if currency in _.keys(@currencyTable) then @currencyTable[currency] else currency
      "#{currencySymbol} #{Numeral(price).format('0,0.00')}"
  })