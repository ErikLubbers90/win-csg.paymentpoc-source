// Generated by CoffeeScript 1.12.4
define(["backbone", "underscore", "moment", "./CDFilters", "./CDSharedModel", "./CDViewContent", "../CDBaseRequest", "../CDSubscriberManagement", "../collections/CDProductCollection", "../collections/CDMediaCollection", "../collections/CDPricingPlanCollection", "../collections/CDBundleProductCollection"], function(Backbone, _, Moment, CDFilters, CDSharedModel, CDViewContent, CDBaseRequest, CDSubscriberManagement, CDProductCollection, CDMediaCollection, CDPricingPlanCollection, CDBundleProductCollection) {
  var CDBundle;
  CDBundle = Backbone.Model.extend({
    relations: {
      AvailableMedia: CDMediaCollection,
      Categories: Backbone.Collection,
      GuidanceRatings: Backbone.Collection,
      PricingPlans: CDPricingPlanCollection,
      Previews: Backbone.Collection,
      OrderPricingPlans: CDPricingPlanCollection,
      BundleChildren: CDBundleProductCollection
    },
    defaults: {
      AdTags: "",
      AllowMultiplePurchases: false,
      AvailableMedia: [],
      BundleChildren: [],
      Categories: [],
      DetailImageUrl: "",
      Genre: "",
      GuidanceRatings: [],
      ImageUrl: "",
      IndexName: "",
      LineOfBusiness: 0,
      Name: "",
      PresentationTypeCode: 0,
      PricingPlans: [],
      ProductionCompany: "",
      OrderPricingPlans: [],
      RatingReason: "",
      Recommendations: [],
      Runtime: 0,
      ReleaseDate: "",
      ReferenceDate: "",
      Standalone: true,
      SeasonNumber: "",
      SeriesName: "",
      StructureType: 2,
      Suggestions: [],
      Studio: "",
      ThumbnailUrl: "",
      Previews: [],
      EntitledPricingPlanId: 0,
      LockerItemId: 0,
      SubscriberProductId: ""
    },
    requestObjects: {
      "metadata": {
        protocol: "GET",
        subDomain: "metadata",
        requestBaseUrl: "/Product/SystemId/<%-CDSystemId%>/Language/<%-CDLanguage%>/DeviceType/<%-CDDeviceType%>/DistributionChannel/<%-CDDistributionChannel%>/Id/{bundleId}",
        requestData: {}
      },
      "context": {
        protocol: "POST",
        subDomain: "services",
        requestUrl: "/Subscriber/RetrieveProductContext",
        requestData: {}
      }
    },
    initialize: function() {
      this.on('change:ImageUrl', this.updateFeaturedImages, this);
      return this.updateFeaturedImages();
    },
    parse: function(response, options) {
      var context, product, result;
      product = response.metadata.Product;
      context = response.context.ProductContext;
      result = _.clone(product);
      result.AvailableMedia = _.pluck(product.AvailableMedia, 'Value');
      result.People = _.groupBy(product.People, "Title");
      result.GuidanceRatings = this.parseGuidanceRating(product.GuidanceRatings);
      result.ReleaseDate = Moment(product.ReleaseDate).format("YYYY");
      result.RatingReason = product.RatingReason != null ? product.RatingReason : "";
      result.PricingPlans;
      result.OrderPricingPlans = this.parseOrderPricingPlans(product.PricingPlans, context.OrderablePricingPlans);
      if (context != null ? context.EntitledPricingPlanId : void 0) {
        result.EntitledPricingPlanId = context.EntitledPricingPlanId;
      }
      if (context != null ? context.LockerItemId : void 0) {
        result.LockerItemId = context.LockerItemId;
      }
      if (context != null ? context.SubscriberProductId : void 0) {
        result.SubscriberProductId = context.SubscriberProductId;
      }
      result.BundleChildren = this.parseChildren(product.ReferencedProducts, product.BundleChildren, context.EpisodicContext || context.BundleContexts);
      delete result["ReferencedProducts"];
      return result;
    },
    parseChildren: function(ReferencedProducts, BundleChildren, childContext) {
      var bundleChild, childProduct, childProductContext, childProductMetadata, childProducts, context, i, j, len, len1, validContext;
      childProducts = [];
      validContext = [];
      for (i = 0, len = childContext.length; i < len; i++) {
        context = childContext[i];
        validContext.push((function() {
          switch (context.StructureType) {
            case 1:
              return {
                ProductId: context.ProductContext.ProductId,
                OrderablePricingPlans: context.OrderablePricingPlans
              };
            case 2:
              return {
                ProductId: context.BundleContexts.ProductId,
                OrderablePricingPlans: context.OrderablePricingPlans
              };
            case 4:
              return {
                ProductId: context.EpisodicContext.ProductId,
                OrderablePricingPlans: context.OrderablePricingPlans
              };
            default:
              return context;
          }
        })());
      }
      for (j = 0, len1 = BundleChildren.length; j < len1; j++) {
        bundleChild = BundleChildren[j];
        childProductContext = _.findWhere(validContext, {
          ProductId: bundleChild.ProductId
        });
        childProductMetadata = _.findWhere(ReferencedProducts, {
          Key: bundleChild.ProductId
        })["Value"];
        childProduct = _.extend({}, childProductMetadata, childProductContext);
        childProduct.PricingPlans = bundleChild.PricingPlans;
        childProduct.OrderPricingPlans = this.parseOrderPricingPlans(bundleChild.PricingPlans, childProductContext["OrderablePricingPlans"]);
        delete childProduct["OrderablePricingPlans"];
        childProducts.push(childProduct);
      }
      return childProducts;
    },
    parseOrderPricingPlans: function(PricingPlans, OrderAblePricingPlans) {
      var OrderAblePricingPlan, PricingPlan, dateNow, i, len, subscriberPricingPlans;
      if (OrderAblePricingPlans == null) {
        OrderAblePricingPlans = [];
      }
      subscriberPricingPlans = [];
      dateNow = Moment().valueOf();
      for (i = 0, len = PricingPlans.length; i < len; i++) {
        PricingPlan = PricingPlans[i];
        OrderAblePricingPlan = _.findWhere(OrderAblePricingPlans, {
          Id: PricingPlan.Id
        });
        if (OrderAblePricingPlan) {
          if ((PricingPlan.OfferingEnd != null) && PricingPlan.OfferingEnd.valueOf() < dateNow) {
            continue;
          }
          subscriberPricingPlans.push(_.extend({}, PricingPlan, OrderAblePricingPlan));
        }
      }
      return subscriberPricingPlans;
    },
    parseGuidanceRating: function(GuidanceRatings) {
      var guidanceRating, guidanceRatingInfo, i, len, matchedGuidanceRatings, ratingInfo, subscriberGuidanceRatingsCode, subscriberGuidanceRatingsCodes;
      if (GuidanceRatings == null) {
        GuidanceRatings = [];
      }
      if (GuidanceRatings.length === 0) {
        return GuidanceRatings;
      }
      matchedGuidanceRatings = [];
      guidanceRatingInfo = CDFilters.get("GuidanceRatings");
      subscriberGuidanceRatingsCodes = CDBaseRequest.guidanceRatingsCategories;
      for (i = 0, len = subscriberGuidanceRatingsCodes.length; i < len; i++) {
        subscriberGuidanceRatingsCode = subscriberGuidanceRatingsCodes[i];
        guidanceRating = _.findWhere(GuidanceRatings, {
          CategoryCode: subscriberGuidanceRatingsCode
        });
        if (guidanceRating) {
          ratingInfo = guidanceRatingInfo.find(function(o) {
            return o.get("Id") === "" + guidanceRating.Code;
          });
          if (ratingInfo != null) {
            guidanceRating.Description = ratingInfo.get("Description");
          }
          matchedGuidanceRatings.push(guidanceRating);
        }
      }
      return matchedGuidanceRatings;
    },
    refresh: function(callback) {
      var bundleId, error, model;
      error = false;
      bundleId = this.get("Id");
      if (!bundleId) {
        error = "no bundle id defined";
        if (typeof callback === "function") {
          callback(error);
        }
      }
      this.requestObjects.metadata.requestUrl = this.requestObjects.metadata.requestBaseUrl.replace("{bundleId}", bundleId);
      if (CDSubscriberManagement.isLoggedIn()) {
        this.requestObjects.context.requestData = {
          ProductId: bundleId,
          IncludeEntitlementContext: true,
          IncludeViewingContext: true,
          IncludeOrderablePricingPlans: true
        };
      } else {
        this.requestObjects.context.requestData = {
          ProductId: bundleId,
          IncludeOrderablePricingPlans: true
        };
      }
      model = this;
      return model.fetch({
        reset: true,
        success: function() {
          return typeof callback === "function" ? callback(error) : void 0;
        }
      });
    },

    /*
    Extras methods
     */
    hasExtras: function() {
      var extrasId;
      extrasId = _.find(this.get("AdditionalProperties"), function(e) {
        return e.Name === "TVExtrasID";
      });
      return extrasId != null;
    },
    getExtrasId: function() {
      var extrasId;
      extrasId = _.find(this.get("AdditionalProperties"), function(e) {
        return e.Name === "ExtrasId";
      });
      return extrasId.Values[0];
    },
    updateFeaturedImages: function() {
      var imageUrl;
      imageUrl = this.get("ImageUrl");
      this.set("FeaturedImageBig", imageUrl.replace("{id}", "featured_selected"));
      this.set("FeaturedImageLandscape", imageUrl.replace("{id}", "featured_notselected"));
      this.set("FeaturedImageSmall", imageUrl.replace("{id}", "featured_secondcolumn"));
      this.set("ThumbnailLandscape", imageUrl.replace("{id}", "thumbnail_landscape"));
      this.set("BackgroundImage", imageUrl.replace("{id}", "background_focus"));
      this.set("BackgroundImageBlurred", imageUrl.replace("{id}", "background_blur"));
      this.set("FeaturedLargeBackground", imageUrl.replace("{id}", "featured_selected_bg"));
      this.set("FeaturedLargeForeground", imageUrl.replace("{id}.jpg", "featured_selected_transparent.png"));
      this.set("FeaturedSmallBackground", imageUrl.replace("{id}", "featured_secondcolumn_bg"));
      return this.set("FeaturedSmallForeground", imageUrl.replace("{id}.jpg", "featured_secondcolumn_transparent.png"));
    }
  });
  _.extend(CDBundle.prototype, CDSharedModel);
  return CDBundle;
});
