define [
  "jquery",
  "underscore",
  "framework",
  "config",
  "i18n!../../../lang/nls/manifest",
  "./CDBaseRequest",
  "./CDSubscriberManagement",
  "../authentication"
], ($, _, TWC, Config, Manifest, CDBaseRequest, CDSubscriberManagement, Authentication)->

  # class to manage CD payment
  class CDPaymentManagement

    # Retrieve payment instruments from subscriber account. Returned payment instruments will not contain detail entities
    #
    # @param tokenType [int] null for just an AccessToken, 1 for also Accesstoken + PurchaseToken and 2 for Accesstoken + CollectionsToken
    # @param callback [Function(error, response)] function called when this method is completed
    @retrieveUwpAccessTokens: (tokenType, callback) ->

      log "CDPaymentManagement - retrieveUwpAccessTokens"

      requestParams = {}
      if tokenType
        requestParams =
          "TokenType": tokenType

      CDBaseRequest.request "POST", "services", "/Subscriber/RetrieveUwpAccessTokens", requestParams, callback

    # Retrieve payment instruments from subscriber account. Returned payment instruments will not contain detail entities
    #
    # @param currency [String] currency when specified, only payment instruments available for the specified currency will be returned
    # @param callback [Function(error, response)] function called when this method is completed
    @retrieveWallet: (currency="USD", callback) ->

      log "CDPaymentManagement - retrieveWallet"
      CDBaseRequest.request "POST", "services", "/Subscriber/RetrieveWallet", {
        "Currency": currency
      }, (error, data) ->
        callback(error, data)

    # Retrieve payment instrument from subscriber account. Returned payment instrument will contain full details
    #
    # @param paymentInstrumentId [Integer] Identifier of the subscriber payment instrument to retrieve
    # @param callback [Function(error, response)] function called when this method is completed
    @retrievePaymentInstrument: (paymentInstrumentId, callback) ->

      log "CDPaymentManagement - retrievePaymentInstrument"
      CDBaseRequest.request "POST", "services", "/Subscriber/RetrievePaymentInstrument", {
        "Id": paymentInstrumentId
      }, callback

    # Update existing payment instrument on subscriber account
    #
    # @param paymentInstrument [Object] PaymentInstrument to be updated
    # @param callback [Function(error, response)] function called when this method is completed
    @updatePaymentInstrument: (paymentInstrument=null, callback) ->

      log "CDPaymentManagement - updatePaymentInstrument"

      return if not paymentInstrument

      requestParams =
        "PaymentInstrument": paymentInstrument

      CDBaseRequest.request "POST", "services", "/Subscriber/UpdatePaymentInstrument", requestParams, callback

    # Preview charge amount for requested shopping cart including shipping and handling costs and taxes.
    # Available quote for new and renew order types.
    # Requires an authenticated session to quote renew order types
    #
    # @param paymentInstrumentIds [Array] collection of payment instrument identifiers used to fulfill order payment
    # @param pricingPlanId [String] identifier of pricing plan to order
    # @param productId [String] identifier of product to order
    # @param promotionCodes [Array] collection of coupon redemption codes to apply to the shopping cart
    # @param callback [Function(error, response)] function called when this method is completed
    @calculateOrderQuote: (paymentInstrumentIds, pricingPlanId, productId, promotionCodes=[], callback) ->

      log "CDPaymentManagement - calculateOrderQuote"

      shoppingCart =
        Items: [
          "PricingPlanId": pricingPlanId
          "ProductId": productId
          "Quantity": 1
        ]

      requestParams =
        "ShoppingCart": shoppingCart

      if paymentInstrumentIds?.length then requestParams.PaymentInstrumentIds = paymentInstrumentIds
      if promotionCodes.length then requestParams.RedemptionCodes = promotionCodes

      CDBaseRequest.request "POST", "services", "/Subscriber/CalculateOrderQuote", requestParams, callback

    # Submit an order for requested shopping cart products
    #
    # @param paymentInstrumentIds [Array] collection of payment instrument identifiers used to fulfill order payment
    # @param pricingPlanId [String] identifier of pricing plan to order
    # @param productId [String] identifier of product to order
    # @param promotionCodes [Array] collection of coupon redemption codes to apply to the shopping cart
    # @param callback [Function(error, response)] function called when this method is completed
    @submitOrder: (paymentInstruments, pricingPlanId, productIds = [], promotionCodes=[], callback) ->

      if productIds.length > 1
        items = []
        for productId in productIds
          items.push("PricingPlanId": pricingPlanId, "ProductId": productId, "Quantity": 1)
      else
        items = ["PricingPlanId": pricingPlanId, "ProductId": productIds[0], "Quantity": 1]
      #multiple product ids for the shopping cart
      shoppingCart =
        Items:
          items

      requestParams =
        "ShoppingCart": shoppingCart

      if paymentInstruments?.length then requestParams.PaymentInstruments = paymentInstruments
      if promotionCodes.length then requestParams.RedemptionCodes = promotionCodes

      CDBaseRequest.request "POST", "services", "/Subscriber/SubmitOrder", requestParams, callback

    # Submit an upgrade for requested shopping cart products
    #
    # @param paymentInstrumentIds [Array] collection of payment instrument identifiers used to fulfill order payment
    # @param pricingPlanId [String] identifier of pricing plan to order
    # @param productId [String] identifier of product to order
    # @param promotionCodes [Array] collection of coupon redemption codes to apply to the shopping cart
    # @param callback [Function(error, response)] function called when this method is completed
    @submitUpgradeOrder: (paymentInstrumentIds, pricingPlanId, productId, lockerItemId, promotionCodes=[], callback) ->

      upgradeItem =
        "PricingPlanId": pricingPlanId
        "ProductId": productId

      requestParams =
        "LockerItemId": lockerItemId
        "UpgradeItem": upgradeItem

      if paymentInstrumentIds?.length then requestParams.PaymentInstrumentIds = paymentInstrumentIds
      if promotionCodes.length then requestParams.RedemptionCodes = promotionCodes

      CDBaseRequest.request "POST", "services", "/Subscriber/SubmitUpgradeOrder", requestParams, callback


    # Search products available for redemption with a coupon redemption code
    #
    # @param redemptionCode [String] redemption code
    # @param callback [Function(error, response)] function called when this method is completed
    @searchProductsByCoupon: (redemptionCode, callback) ->

      CDBaseRequest.request "POST", "services", "/Subscriber/SearchProductsByCoupon", {
        "RedemptionCode": redemptionCode
      }, callback

    # Order errors
    #
    # @param error [Object] error received when order failed
    @orderErrors: (error) ->
      #TODO complete translations!!

      if error.Code is 117 and error.Subcode
        switch error.Subcode
          #specify subcodes related to login errors
          when 11701 then message = Manifest.error_message_invalid_login #AccessDeniedLoginInvalid
          when 11702 then message = Manifest.login_revoked_popup #AccessDeniedLoginRevoked
          when 11703 then message = Manifest.invalid_password_entered #AccessDeniedInvalidPassword
          when 11704 then message = Manifest.login_session_expired #AccessDeniedInactive
          when 11705 then message = Manifest.error_household_access_denied #AccessDeniedInsufficientAccess
          when 11706 then message = Manifest.change_password_invalid_current_password #AccessDeniedInvalidPasswordChallengeResponse
          when 11707 then message = Manifest.player_content_access_denied_message #AccessDeniedBusinessUnitNotFound
          when 11708 then message = Manifest.player_content_access_denied_message #AccessDeniedDayAndTimeDenied
          when 11709 then message = Manifest.player_content_access_denied_message #AccessDeniedBusinessUnitDisabled
          when 11710 then message = Manifest.error_subscriber_not_active #AccessDeniedInsufficientSubscriberAccess
          when 11711 then message = Manifest.error_household_access_denied #AccessDeniedSubscriberInputDisabled
          when 11712 then message = Manifest.error_household_access_denied #AccessDeniedInsufficientBusinessUnitAccess
          when 11713 then message = Manifest.error_household_access_denied #AccessDeniedTokenInvalid
          when 11714 then message = Manifest.player_content_access_denied_message #AccessDeniedAnonymousProxyServerDetected
          when 11715 then message = Manifest.error_household_access_denied #AccessDeniedInvalidLocation
          when 11716 then message = Manifest.error_message_general_error #Unspecified
          when 11717 then message = Manifest.error_login_suspended_account_message #AccessDeniedTemporaryPasswordExpired
          when 11718 then message = Manifest.error_message_invalid_login #The device session promotion pin was invalid
          when 11719 then message = Manifest.error_message_invalid_login #Invalid oAuth header
          when 11720 then message = Manifest.status_pending #AccessDeniedPendingApproval

      else if error.Code
        switch error.Code
          when 118 then message = Manifest.payment_instrument_required #PaymentInstrumentRequired
          when 611 then message = Manifest.payment_instrument_required #PaymentInstrumentNotFound
          when 616 then message = Manifest.payment_error_message_tax_failure #RevenueTaxFailure
          when 617 then message = Manifest.payment_error_message_transaction_failure #TransactionFailure
          when 607 then message = Manifest.invalid_payment_instrument_error_message #InvalidPaymentRequest
          when 606 then message = Manifest.invalid_payment_instrument_error_message #InvalidPaymentInstrument
          #when 603 then message = Manifest.payment_error_message_insufficient_balance #InsufficientBalance
          when 623 then message = Manifest.payment_error_message_spending_limit #SpendingLimitViolation
          when 920 then message = Manifest.error_message #ProductNotFound
          when 916 then message = Manifest.cannot_select_pricing_plan #PricingPlanNotFound
          when 943 then message = Manifest.error_message #ShippingMethodNotFound
          when 1014 then message = Manifest.payment_error_message_expired_payment #InvalidOrderPayment
          when 1015 then message = Manifest.error_message #InvalidOrderPaymentInstrumentAmount
          when 1019 then message = Manifest.shopping_cart_invalid_subscription_quantity #InvalidShoppingCart
          when 1010 then message = Manifest.error_message #InvalidInstanceProperty
          when 1011 then message = Manifest.error_message #InvalidItemCurrency
          when 1012 then message = Manifest.error_message #InvalidItemOrderingType
          when 1028 then message = Manifest.error_message #ProductNotOrderable
          when 1034 then message = Manifest.error_message #SubscriptionNotOrderable
          when 1021 then message = Manifest.error_message #InvalidSubscriberProductStatus
          when 1009 then message = Manifest.error_message #InvalidDiscount
          when 1008 then message = Manifest.error_message_device_not_available #InvalidDevice
          when 1006 then message = Manifest.error_message_device_not_available #DeviceRequired
          when 1040 then message = Manifest.redemption_code_required #CouponRequired
          when 1039 then message = Manifest.invalid_payment_instrument_error_message #InvalidSubscriptionPaymentInstrument
          when 1018 then message = Manifest.ordering_invalid_shipping_method_for_address #InvalidShippingMethod
          when 1049 then message = Manifest.already_purchased #ProductAlreadyPurchased
          when 1101 then message = Manifest.address_validation_failed #AddressValidationFailed
          when 1100 then message = Manifest.error_message_address_invalid #AddressNotFound
          when 1144 then message = Manifest.invalid_captcha_entry #CaptchaVerificationFailed
          when 213 then message = Manifest.invalid_redemption_code #InvalidExternalRedemptionRequest
          when 1052 then message = Manifest.invalid_redemption_code #InvalidExternalRedemptionRequest
          when 1038 then message = Manifest.invalid_redemption_code #InvalidCouponRedemption
          when 1106 then message = Manifest.no_existing_shipping_addresses #InvalidBillingAddress
          else
            message = Manifest.error_message
      message


    # Update paypal data
    #
    # @param paymentInstruments [Array] collection of the payment instruments
    # @param callback [Function(error, response)] function called when this method is completed
    @updatePaypalData: (paymentInstruments, callback)->
      #Get PayPal payment instruments
      paypalItems = []
      if paymentInstruments.length > 0
        for instrument in paymentInstruments
          if instrument.TypeName is "PayPal Account"
            paypalItems.push(instrument)

      if paypalItems.length is 0
        callback(paymentInstruments)
      else
        #if there are paypal instruments, load the detailed information for them
        onCompleted = ()=>
          #after paypal info loaded update the data array
          for paypalInfo in paypalData
            for i in [0...paymentInstruments.length]
              if paypalInfo.PaymentInstrument.Id is paymentInstruments[i].Id
                paymentInstruments[i] = paypalInfo.PaymentInstrument
          callback(paymentInstruments)

        synchroniseRequests = _.after(paypalItems.length, onCompleted)
        paypalData = []
        for paypalItem in paypalItems
          @retrievePaymentInstrument paypalItem.Id, (error, data)=>
            if not error
              paypalData.push(data)
            synchroniseRequests()
