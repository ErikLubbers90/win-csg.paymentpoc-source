define [
  "framework",
  "jquery",
  "config",
  "application/helpers/submenuHelper",
  "application/helpers/backgroundHelper",
  "application/api/cd/models/CDSearchProducts",
  "application/api/cd/models/CDStoreFrontPage",
  "application/api/authentication",
  "../../models/analytics/omnituretracker",
  "i18n!../../../lang/nls/manifest"
], (TWC, $, Config, SubMenuHelper, BackgroundHelper, CDSearchProductsModel, CDStoreFrontPageModel, Authentication, OmnitureTracker, Manifest) ->

  class StorefrontpageView extends TWC.MVC.View
    uid: 'storefrontpage-view'

    el: "#screen"

    type: 'page'

    # Used to keep track of current playlist vertical scroll position
    scrollPosVert: 0

    # Max visible products displayed for each playlist
    totalProductsPerPage: 18
    renderedPages: []

    generatedRules: []

    currentPage: 0
    totalPages:1

    defaultFocus: "#playlist_product-0"

    external:
      html:
        url:"skin/templates/screens/storefrontpage/storefrontpage.tpl"

    zones: [
      {uid:"menu-zone", data:[]}
    ]

    events:
      "enter    .playlistContent .product" : "showProductDetail"
      "enter    #subMenuItems .menuButton" : "showSubMenuContent"
      "enter    #navigationLeft" : "prevPageNavAction"
      "enter    #navigationRight" : "nextPageNavAction"
      "swipeLeft .playlistContent": "prevPageNavAction"
      "swipeRight .playlistContent": "nextPageNavAction"

    onload: (@subMenuSelectedId, @selectedItemId = null, @leftPosition = 0, @currentPage = 0) ->

      @renderedPages = []
      @scrollPosVert = 0
      @totalPages = 1
      @initialFocus = @selectedItemId

      # Create new model and pass the subMenuSelectedId to request the data
      @model = new CDStoreFrontPageModel({}, {subMenuId:@subMenuSelectedId})
      BackgroundHelper.enableBlur()

      # Pass manifest to be used in template
      @model.set
        manifest:Manifest

    onunload: ->
      # Reset vertical scroll position
      @scrollPosVert = 0

    # Debug only, overwrite template to display data passed to template
#    template: (html, data) ->
#      return super

    onRenderComplete: ->
      $("#mainMenu").show()
      SubMenuHelper.setStoreFrontPages(@model.get("SubMenuItems"))
      SubMenuHelper.setSelectedMenuItem("browse")

      # When empty (first time load) then retrieve first category
      if not @subMenuSelectedId? or @subMenuSelectedId.length is 0
        @subMenuCategories = SubMenuHelper.getStoreFrontPages()
        @subMenuSelectedId = @subMenuCategories[0]?.Id

      SubMenuHelper.setSelectedStorefrontPage(@subMenuSelectedId)
      SubMenuHelper.setSubMenuContext(SubMenuHelper.CONTEXT_STOREFRONT)

      @initStorefront()

      if @leftPosition is 0
        @contentLeft = $("#subBar").width()
      else
        @contentLeft = @leftPosition

      $playlistContent = $(".playlistContent")
      $playlistContent.css({
        "-o-transform":"translate(" + @contentLeft + "px, 0)",
        "-webkit-transform":"translate3d(" + @contentLeft + "px, 0, 0)",
      })

      # Set focus
      if $(".playlistContent .product:eq(0)").length > 0
        TWC.MVC.Router.setFocus(".playlistContent .product:eq(0)")
      else
        TWC.MVC.Router.setFocus("#subMenuItems .submenu:eq(0)")

      $playlistContent.addClass("visibleProducts")
      TWC.MVC.History.reset()

    onZoneRenderComplete: ->
      # Clear selected submenu
      $("#subMenuItems .menuButton.submenu").removeClass("select")

      # Set submenu selected
      $subMenuEl = $("#subMenuItems .menuButton.submenu[data-id='#{@subMenuSelectedId}']")
      $subMenuEl.addClass("select")

      @setFocusToProduct()

      pageName = $subMenuEl.text().toLowerCase().replace(/[^A-Za-z]/g, "")
      prefix = if not Authentication.isLoggedIn() then "unauthenticated." else ""

      totalProducts = @getTotalProducts(0)
      ot = OmnitureTracker.get()
      ot.trackPageView("#{prefix}browse.#{pageName}.html")

    setFocusToProduct: ->
      TWC.MVC.Router.setActive "storefrontpage-view"
      if @initialFocus?
        @selectedItemId = @initialFocus
        @initialFocus = null
      if @selectedItemId?
        TWC.MVC.Router.setFocus "##{@selectedItemId}"
        @updateBackground(parseInt(@selectedItemId.split("-")[1]))
      else
        TWC.MVC.Router.setFocus @defaultFocus
        @updateBackground(parseInt(@defaultFocus.split("-")[1]))

    initStorefront: ->
      $(TWC.MVC.View.eventbus).trigger('loading', [true, "forced"])
      # determine all pages to render
      if @model.get("featuredItemsEnabled")
        startPage = 1
        @renderedPages = [0]
      else
        startPage = 0
        @renderedPages = []

      endPage = @currentPage + 1
      pagesToRender = [startPage..endPage]

      # Initialize page navigation
      @displayPageNavigation(@currentPage, endPage)

      # render pages synchronously one by one
      render = =>
        if pagesToRender.length is 0
          $(TWC.MVC.View.eventbus).trigger('loading', [false, "reset"])

          active = TWC.MVC.Router.getActive()
          if active.type isnt "popup"
            @setFocusToProduct()
          return
        page = pagesToRender.shift()
        dfr = @renderPage(page)
        dfr.always ->
          render()
      render()

    # Show/hide page navigation
    displayPageNavigation: (currentPage=0, totalPages=1)->
      if currentPage is 0
        $("#navigationLeft").hide();
      else
        $("#navigationLeft").show();

      if totalPages > 1
        $("#navigationRight").show();

      if @model.get("featuredItemsEnabled")
        if currentPage is totalPages
          $("#navigationRight").hide()
      else
        if currentPage > 0 and currentPage+1 >= totalPages
          $("#navigationRight").hide()

    focusUp: ->
      $el = $(TWC.MVC.Router.getFocus())

      return if !$el

      # Current focus on product
      if @focusIsOnProduct($el)
        focusedProductIndex = parseInt($el.attr("id").split("-")[1])
        featuredProductCount = @getFeaturedProductCount()

        # Focus on next playlist row if current rowindex less than 4
        if $el.hasClass("featured")
          if focusedProductIndex % 2 > 0
            @updateBackground(focusedProductIndex-1)
            TWC.MVC.Router.setFocus($el.prev())
        else
          if (focusedProductIndex - featuredProductCount) % 18 >= 6
            @updateBackground(focusedProductIndex-6)
            if $("#playlist_product-#{focusedProductIndex-6}").length
              TWC.MVC.Router.setFocus $("#playlist_product-#{focusedProductIndex-6}")


    focusDown: ->
      $el = $(TWC.MVC.Router.getFocus())

      return if !$el

      # Current focus on product
      if @focusIsOnProduct($el)
        focusedProductIndex = parseInt($el.attr("id").split("-")[1])

        # Focus on next playlist row if current rowindex less than 4
        focusEnabled = false
        featuredProductCount = @getFeaturedProductCount()
        if focusedProductIndex < 4 and featuredProductCount is 4
          if focusedProductIndex % 2 < 1
            @updateBackground(focusedProductIndex+1)
            TWC.MVC.Router.setFocus($el.next())
        else
          if (focusedProductIndex - featuredProductCount) % 18 < 12
            @updateBackground(focusedProductIndex+6)
            if $("#playlist_product-#{focusedProductIndex+6}").length
              TWC.MVC.Router.setFocus $("#playlist_product-#{focusedProductIndex+6}")

    focusLeft: (callback=null)->
      $el = $(TWC.MVC.Router.getFocus())
      return if !$el

      # Current focus on product
      if @focusIsOnProduct($el)
        focusedProductIndex = parseInt($el.attr("id").split("-")[1])
        featuredProductCount = @getFeaturedProductCount()

        # Product focus
        if ((featuredProductCount is 4 and focusedProductIndex > 1) \
            or (featuredProductCount is 0 and focusedProductIndex not in [0,6,12]))

          # Focus on index of next playlist row if exists
          rowCnt = if focusedProductIndex < 5 and featuredProductCount is 4 then 2 else 1

          # Page transition required
          if (focusedProductIndex-featuredProductCount) % 18 in [0, 6, 12] and @currentPage > 0

            # block if prev page is not rendered yet
            return if (@currentPage-1) not in @renderedPages

            # go to prev page
            @toPrevPage(focusedProductIndex, featuredProductCount)
            callback?()

          else
            if $(".playlistContent .product:not('.disable'):eq(#{focusedProductIndex-rowCnt})").length
              $nextFocus = $(".playlistContent .product:not('.disable'):eq(#{focusedProductIndex-rowCnt})")

              @updateBackground(focusedProductIndex-rowCnt)
              TWC.MVC.Router.setFocus($nextFocus)
            else
              @focusMenu()
        else
          @focusMenu()

    focusRight: (callback=null)->
      $el = $(TWC.MVC.Router.getFocus())
      return if !$el

      # Current focus on product
      if @focusIsOnProduct($el)
        focusedProductIndex = parseInt($el.attr("id").split("-")[1])
        #calculate the next focusable element, if on featured block, then skip 2, otherwise 3
        rowCnt = if $el.hasClass("featured") then 2 else 1
        featuredProductCount = if @model.get("featuredItemsEnabled") then 4 else 0

        #Page transition required
        if ((focusedProductIndex in [2,3] and featuredProductCount is 4) \
            or ((focusedProductIndex-featuredProductCount) % 18 in [5, 11, 17]) and @currentPage < @totalPages)

          # block if next page is not rendered yet
          return if (@currentPage+1) not in @renderedPages

          # render upfollowing page to next page
          @renderPage(@currentPage + 2)

          # do not transition if there are no more pages
          return if (@currentPage+(if @model.get("featuredItemsEnabled") then 0 else 1)) >= @totalPages

          # go to next page
          @toNextPage(focusedProductIndex)
          callback?()

        # set focus to next product
        else
          if $(".playlistContent .product:not('.disable'):eq(#{focusedProductIndex+rowCnt})").length

            #cancel new focus if on last page and on last item
            return if (focusedProductIndex-featuredProductCount) % 18 in [5, 11, 17] and @currentPage is @totalPages

            $nextFocus = $(".playlistContent .product:not('.disable'):eq(#{focusedProductIndex+rowCnt})")
            @updateBackground(focusedProductIndex+rowCnt)
            TWC.MVC.Router.setFocus($nextFocus)

    toPrevPage: (focusIndex, totalFeaturedProducts) ->
      # start transition to next page
      TWC.SDK.Input.get().setBlockAll(true)

      # render upfollowing page to prev page
      @renderPage(@currentPage - 2)

      if @currentPage is 1 and totalFeaturedProducts is 4
        scrollWidth = $("#playlist_page-0").outerWidth(false)
        $nextFocus = if focusIndex is 4 then $("#playlist_product-2") else $("#playlist_product-3")
      else
        scrollWidth = $("#playlist_page-1").outerWidth(true)
        if $("#playlist_product-#{focusIndex-13}").length
          $nextFocus = $("#playlist_product-#{focusIndex-13}")
        else
          $nextFocus = $("#playlist_page-#{@currentPage} .product:last-child")

      nextPosition = parseInt($nextFocus.attr("id").split("-")[1])

      if @currentPage is 1
        @contentLeft = $("#subBar").width()
      else
        @contentLeft += scrollWidth

      $(".playlistContent").css({
        "-o-transform": "translate(" + @contentLeft + "px, 0)",
        "-webkit-transform": "translate3d(" + @contentLeft + "px, 0, 0)"})
      setTimeout ()->
        TWC.SDK.Input.get().setBlockAll(false)
      , 500

      @currentPage--
      @updateBackground(nextPosition)
      @displayPageNavigation(@currentPage, @totalPages)
      TWC.MVC.Router.setFocus($nextFocus)

    toNextPage: (focusIndex) ->
      # start transition to next page
      TWC.SDK.Input.get().setBlockAll(true)

      if focusIndex in [2,3]
        scrollWidth = $("#playlist_page-0").outerWidth(false)
        $nextFocus = if focusIndex is 2 then $("#playlist_product-4") else $("#playlist_product-10")
      else
        scrollWidth = $("#playlist_page-1").outerWidth(true)
        if $("#playlist_product-#{focusIndex+13}").length
          $nextFocus = $("#playlist_product-#{focusIndex+13}")
        else
          $nextFocus = $("#playlist_page-#{@currentPage+1} .product:first-child")
      nextPosition = parseInt($nextFocus.attr("id").split("-")[1])
      if @currentPage is 0 then scrollWidth = scrollWidth - Math.round((Config.resolution[0]/1920) * 77)

      @contentLeft -= scrollWidth
      $(".playlistContent").css({
        "-o-transform": "translate(" + @contentLeft + "px, 0)",
        "-webkit-transform": "translate3d(" + @contentLeft + "px, 0, 0)"})
      setTimeout ()->
        TWC.SDK.Input.get().setBlockAll(false)
      , 500

      @currentPage++
      @updateBackground(nextPosition)
      @displayPageNavigation(@currentPage, @totalPages)
      TWC.MVC.Router.setFocus($nextFocus)

    # Action triggered by click/touch
    prevPageNavAction: ->
      # Set focus index manually to first product on the current page
      TWC.MVC.Router.setFocus $("#playlist_page-#{@currentPage} .product:eq(0)")

      @focusLeft(->
        TWC.MVC.Router.unFocus()
      )

    # Action triggered by click/touch
    nextPageNavAction: ->
      if @model.get("featuredItemsEnabled") and @currentPage is 0
        # Set focus manually to last featured item
        TWC.MVC.Router.setFocus $(".playlistContent .playlist-col.featured:last .featured.product:first")
      else
        # Set focus manually to last product on the current page
        TWC.MVC.Router.setFocus $("#playlist_page-#{@currentPage} .product:eq(5)")

      @focusRight(->
        TWC.MVC.Router.unFocus()
      )

    focusTimeout : null
    focus: (id)->
      stringId = if typeof id is "string" then id else "#"+$(id).attr("id")
      if stringId.indexOf("#playlist_product-") isnt -1
        @selectedItemId = stringId.substring(1)

      if @model.get("featuredItemsEnabled")
        $(".product.animate").removeClass("animate")
        clearTimeout(@focusTimeout)
        if stringId in ["#playlist_product-0", "#playlist_product-1", "#playlist_product-2", "#playlist_product-3"]
          @focusTimeout = setTimeout ()->
            $(stringId).addClass("animate")
          , 2000

      super

    getRandomInRange: (min, max)->
      Math.floor(Math.random() * (max - min + 1)) + min

    getRandomFloat: (min, max)->
      Math.random() * (max - min) + min

    timeoutArr: []

    animateFeaturedProduct: (stringId)->
      return
      #update keyframe animation
      ss = document.styleSheets
      foregroundRule = null
      backgroundRule = null
      return if pos in @generatedRules
      for ssi in ss
        for cssi in ssi.cssRules
          if cssi.type is window.CSSRule.WEBKIT_KEYFRAMES_RULE and cssi.name is "foregroundAnimation#{pos}"
            foregroundRule = cssi
          if cssi.type is window.CSSRule.WEBKIT_KEYFRAMES_RULE and cssi.name is "backgroundAnimation#{pos}"
            backgroundRule = cssi

      if foregroundRule? and $("#playlist_product-#{pos} .foreground-asset").length > 0
        randomScale35 = @getRandomFloat(1.1,1.25)
        randomScale70 = @getRandomFloat(0.75,0.9)
        foregroundRule.deleteRule("35%")
        foregroundRule.deleteRule("70%")
        foregroundRule.appendRule("35% {     -o-transform:scale(#{randomScale35},#{randomScale35}); -webkit-transform: scale3d(#{randomScale35},#{randomScale35},1); }")
        foregroundRule.appendRule("70% {     -o-transform:scale(#{randomScale70},#{randomScale70}); -webkit-transform: scale3d(#{randomScale70},#{randomScale70},1); }")

      if backgroundRule? and $("#playlist_product-#{pos} .background-asset").length > 0
        for i in [1..9]
          backgroundRule.deleteRule(i*10 + "%")
          randomPosX = @getRandomInRange(-30,30)
          randomPosY = @getRandomInRange(-30,30)
          backgroundRule.appendRule("#{i*10}% {     -o-transform:translate(#{randomPosX}px,#{randomPosY}px); -webkit-transform: translate3d(#{randomPosX}px,#{randomPosY}px,0px); }")

      @generatedRules.push(pos)

    resizeFeaturedProduct: (pos)->
      updateProduct = (id, action) ->
        reverse = if action is "large" then "small" else "large"
        $(id).addClass(action).removeClass(reverse)

      updateProduct("#playlist_product-0", if pos is 0 then "large" else "small")
      updateProduct("#playlist_product-1", if pos is 1 then "large" else "small")

    updateBackground: (idx, blurred = true, timeout = 1500)->
      log("updateBackground", idx, blurred, timeout)

      if idx < 4 and @model.get("featuredItemsEnabled")
        activeProduct = @model.get("FeaturedPlaylist").at(idx)
        url = if activeProduct.get("BackgroundImage") then activeProduct.get("BackgroundImage") else "./skin/#{Config.resolution[1]}/media/theme/placeholder.jpg"
        blurUrl = if activeProduct.get("BackgroundImageBlurred") then activeProduct.get("BackgroundImageBlurred") else "./skin/#{Config.resolution[1]}/media/theme/placeholder.jpg"
      else
        #activeProduct = @model.get("Playlists").at(0).get("Products").at(idx-4)
        url = "./skin/#{Config.resolution[1]}/media/theme/placeholder.jpg"
        blurUrl = "./skin/#{Config.resolution[1]}/media/theme/placeholder.jpg"

      BackgroundHelper.setBackgroundImage(url, blurUrl, blurred, timeout)

    showProductDetail: (el) ->
      $el = $(el.target)

      if $el.get(0).nodeName is "IMG"
        $el = $el.parents(".product")
      productId = $el.data("id")
      productType = $el.data("type")
      currentLeft = parseInt($(".playlistContent").css("left"))
      idx = parseInt($el.attr("id").split("-")[1])
      @updateBackground(idx, false, 1000)

      $(".playlistContent").removeClass("visibleProducts")

      TWC.SDK.Input.get().setBlockAll(true)
      setTimeout ()=>

        switch productType
          when 1
            # Product/episode
            page = 'productDetail-view'
          when 2, 4
            # Bundle/season
            page = 'bundleDetail-view'
          when 5
            # Series collection
            page =  'seasonlist-view'
          else
            page = 'productDetail-view'

        @updateHistory(@subMenuSelectedId, $el.attr("id"), @contentLeft, @currentPage)

        TWC.SDK.Input.get().setBlockAll(false)
        TWC.MVC.Router.load page,
          data: [productId]
      , 400

    showSubMenuContent: (el) ->

      $el = $(el.target)
      selectedSubMenuId = $el.data("id")

      if selectedSubMenuId isnt @subMenuSelectedId
        $(".playlistContent").removeClass("visibleProducts")
        SubMenuHelper.setSelectedStorefrontPage(selectedSubMenuId)
        TWC.SDK.Input.get().setBlockAll(true)
        setTimeout ()=>
          TWC.SDK.Input.get().setBlockAll(false)
          TWC.MVC.Router.load @uid,
            data: [selectedSubMenuId]
            callback: ->
              TWC.MVC.History.reset()
        , 400

    buttonIsDisabled: ($el) ->
      $el.hasClass("disable")

    focusIsOnPlaylistButtons: ($focusedEl) ->
      $focusedEl.parent().hasClass("playlistButtons")

    focusIsOnFeaturedPlaylistButtons: ($focusedEl) ->
      $focusedEl.hasClass("button") and $focusedEl.hasClass("featured")

    focusIsOnProduct: ($focusedEl) ->
      $focusedEl.hasClass("product")

    focusIsOnFeaturedProduct: ($focusedEl) ->
      $focusedEl.hasClass("product") and $focusedEl.hasClass("featured")

    updateLastFocused: ->
      if $(".playlist.active").length > 0
        @lastFocused = @activeFocus = $(".playlist.active .playlistContent .product:eq(0)")
      else
        @lastFocused = $(".playlist.active .playlistHeader .playlistButtons .button:not('.disable'):eq(0)")

    # Returns the total of products in the playlist
    getTotalProducts: (playlistIndex) ->
      #if featured exists playlistindex is not correct because featured is added before
      if $("#content .playlist:eq(0)").hasClass "featured"
        playlistIndex -= 1
      @model.get("Playlists").at(playlistIndex).get("Products").length

    focusMenu: ->
      if $(".menuButton.submenu.select").length
        TWC.MVC.Router.setFocus ".menuButton.submenu.select"
      else if $("#submenu_0")
        TWC.MVC.Router.setFocus "#submenu_0"
      else
        TWC.MVC.Router.setFocus "#browse"

    moveToMainMenu: ->
      TWC.MVC.Router.setActive "menu-zone"
      TWC.MVC.Router.setFocus "#browse"

    moveToContent: ->
      if @currentPage > 0
        i = (@currentPage+(if @model.get("featuredItemsEnabled") then -1 else 0)) * @totalProductsPerPage
        i += if @currentPage > 0 and @model.get("featuredItemsEnabled") then 4 else 0
      else
        i = 0
      @updateBackground(i)
      TWC.MVC.Router.setFocus "#playlist_product-#{i}"

    moveToMenu: (focusOnLast=false) ->
      if focusOnLast
        TWC.MVC.Router.setFocus $("#subMenuItems .menuButton:last")
      else
        if $("#submenu_0")
          TWC.MVC.Router.setFocus "#submenu_0"
        else
          TWC.MVC.Router.setFocus "#browse"

    # Transform product collection to HTML
    getPageContent: (productCollection, page, fillDiv = false) ->
      pageHtml = []
      pCollection = productCollection.slice(0, Math.min(productCollection.length, @totalProductsPerPage))
      i = (page+(if @model.get("featuredItemsEnabled") then -1 else 0)) * @totalProductsPerPage

      _.each pCollection, (product) =>
        if product.get('Name').length > 20 then name = "<marquee scrolldelay=125>#{product.get('Name')}</marquee>" else name = product.get('Name')
        playlistClass = "playlist-page"+(if page is 0 and not @model.get("featuredItemsEnabled") then ' left-margin' else '')
        pageHtml.push("<div id=\"playlist_page-" + page + "\" class=\""+playlistClass+"\">") if i % @totalProductsPerPage == 0 and not fillDiv
        index = i + (if @model.get("featuredItemsEnabled") then 4 else 0)
        pageHtml.push """
          <div id="playlist_product-#{index}" class="product content-sound" data-left="focusLeft" data-right="focusRight"
              #{if (i % 18 >= 6) then 'data-up="focusUp" ' else ''}
              #{if (i % 18 < 12) then 'data-down="focusDown" ' else ''}
              data-id="#{product.get("Id")}"" data-type="#{product.get("StructureType")}">
            <span class="productTitle">#{name}</span>
            <div class="productBorder"></div>
            <img src="#{product.get("ThumbnailUrl")}" alt="#{product.get("Name")}" />
          </div>
          """

        pageHtml.push("</div>") if i % @totalProductsPerPage is @totalProductsPerPage-1 and not fillDiv
        i++
      pageHtml.push("</div>") if i % @totalProductsPerPage isnt @totalProductsPerPage-1 and not fillDiv
      pageHtml.join("")

    renderPage: (nextPage) ->

      dfd = $.Deferred()
      if nextPage in @renderedPages
        dfd.reject()
      else if (0 <= nextPage <= @totalPages) and (nextPage not in @renderedPages)
        category = @model.get("Playlists").at(0).get("CategoryId")
        categoryProductSearchModel = new CDSearchProductsModel({}, {
          pageNumber:if @model.get("featuredItemsEnabled") then nextPage else nextPage+1,
          pageSize:@totalProductsPerPage,
          sortBy: 2,
          sortDirection:2,
          categories: [category]
        })
        categoryProductSearchModel.fetch
          success: (model, data, xhr)=>
            @totalPages = Math.ceil(model.get("TotalProducts")/@totalProductsPerPage)
            productCollection = model.get("Products")
            pageContent = @getPageContent(productCollection.models, nextPage)
            $(".playlistContent")
              .append(pageContent)
              .ready =>
                @renderedPages.push(nextPage)
                dfd.resolve()
      else
        dfd.reject()
      return dfd.promise()

    getFeaturedProductCount: ->
      return (if @model.get("featuredItemsEnabled") then 4 else 0)

  StorefrontpageView.get()
