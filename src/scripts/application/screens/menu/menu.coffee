define [
    "framework",
    "../../models/baseModel",
    "jquery",
    "config",
    "application/api/authentication",
    "application/helpers/submenuHelper",
    "application/popups/generic/alert",
    "application/popups/login/loginConfirmation",
    "i18n!../../../lang/nls/manifest"],
(TWC, BaseModel, $, Config, Authentication, MenuHelper, Alert, LoginConfirmation, Manifest) ->

  menuModel = BaseModel.extend
    defaults:
      "manifest": Manifest

  class menuView extends TWC.MVC.View
    #reference id, should be unique
    uid: "menu-zone"

    #dom element of reference
    el: "#mainMenu"

    model: new menuModel()

    type: "zone"

    # dynamic elements, keys will be used as object method
    external:
      html:
        url:"skin/templates/screens/menu/menu.tpl"

    # pages which does not require mouse backbutton navigation (top left)
    disabledBackBtnPages: ['storefrontpage-view','locker-view','favorites-view','search-view','settings-view']

    # events
    events:
      "enter #settings"     : "showSettings"
      "enter #browse"       : "showFilms"
      "enter #library"      : "showLibrary"
      "enter #search"       : "showSearch"
      "enter #favorites"    : "showFavorites"
      "enter #backButton"   : "goBack"

    onload: ->

    onRenderComplete: ->
      log "render menu zone"

      #force select state to correct menu item from menu helper
      item = MenuHelper.getSelectedMenuItem()
      $("#mainMenu .menuButton.select").removeClass("select")
      @select("##{item}")
      @unfocus()

      # Prepare back button
      if $("#backButton").html().length is 0
        $("#backButton").html("<span class='buttonText'>#{Manifest.back}</span>")

      if TWC.MVC.Router.getActivePage()?.uid not in @disabledBackBtnPages and TWC.MVC.History.size() > 1
        $("#backButton").show()
      else
        $("#backButton").hide()

      # Vertical center menu
      menuHeight = $("#mainMenuItems .menuButton:first").height()*$("#mainMenuItems .menuButton").length
      $("#mainMenuItems").css("height", "#{menuHeight}px")
      $("#mainMenuItems").css("top", "50%").css("margin-top", "-#{menuHeight / 2}px")

    defaultAction: ->
      log "not yet available"

    focusDown: ->
      @moveToSubmenu()

    moveToSubmenu: ->
      TWC.MVC.Router.setActive @parent.uid
      if $(".menuButton.submenu.select").length
        TWC.MVC.Router.setFocus ".menuButton.submenu.select"
      else
        if $(".menuButton.submenu").length
          TWC.MVC.Router.setFocus ".menuButton.submenu:first"
        else
          @parent.moveToContent?()

    focusBack: ->
      if TWC.MVC.Router.getActivePage()?.uid not in @disabledBackBtnPages and TWC.MVC.History.size() > 1
        TWC.MVC.Router.setFocus $("#backButton")

    backLeft: ->
      if $("#mainMenu .menuButton").length
        TWC.MVC.Router.setFocus $("#mainMenu .menuButton:first")

    backRight: ->
      if @parent.moveToContent?()
        TWC.MVC.Router.setActive @parent.uid
        @parent.moveToContent()

    backDown: ->
      @moveToSubmenu()

    setSelectedMenuItem: (item) ->
      $("#mainMenuItems .menuButton.select").removeClass("select")
      $("##{item}").addClass('select')

    fixSelectedMenuItem: (isFocusInMainMenu) ->
      item = MenuHelper.getSelectedMenuItem()
      $("#mainMenu .menuButton.select").removeClass("select")
      lastFocused = @lastFocused
      @select("##{item}")
      @unfocus()
      if isFocusInMainMenu
        TWC.MVC.Router.setActive "menu-zone"
        @focus("#{lastFocused}")

    showLogin: ->
      if not Authentication.isLoggedIn()
        TWC.MVC.Router.load 'register-view',
          data: [
            ()=>
              TWC.MVC.Router.load 'settings-view',
              callback: =>
                TWC.MVC.History.reset(1)

          ],
          callback: =>
            @unselect()
            TWC.MVC.History.reset(1)

    showFilms: ->
      TWC.MVC.Router.setSelect(this)
      TWC.MVC.Router.load 'storefrontpage-view',
        callback: =>
          TWC.MVC.History.reset(1)

    showSettings: ->
      TWC.MVC.Router.setSelect(this)
      TWC.MVC.Router.load 'settings-view'
#        callback: =>
#          TWC.MVC.History.reset(1)


    showFavorites: ->
      if Config.useFavorites
        if Authentication.isLoggedIn()
          TWC.MVC.Router.setSelect(this)
          TWC.MVC.Router.load 'favorites-view',
            callback: =>
              TWC.MVC.History.reset(1)
        else
          loginConfirmation = new LoginConfirmation()
          loginConfirmation.load
            data: [true, ()=>
              TWC.MVC.Router.load 'register-view',
                data: [
                  ()=>
                    TWC.MVC.Router.setSelect(this)
                    TWC.MVC.Router.load 'favorites-view',
                      callback: =>
                        TWC.MVC.History.reset(1)
                ],
                callback: =>
                  TWC.MVC.History.reset(1)
            ]
      else
        if Authentication.isLoggedIn()
          TWC.MVC.Router.load 'settings-view',
            data: [2]
            callback: =>
              TWC.MVC.History.reset(1)
        else
          loginConfirmation = new LoginConfirmation()
          loginConfirmation.load
            data: [true, ()=>
              TWC.MVC.Router.load 'register-view',
                data: [
                  ()=>
                    TWC.MVC.Router.load 'settings-view',
                      data: [2]
                      callback: =>
                        TWC.MVC.History.reset(1)
                ],
                callback: =>
                  TWC.MVC.History.reset(1)
            ]




    showSearch: ->
      TWC.MVC.Router.setSelect(this)
      TWC.MVC.Router.load 'search-view',
        callback: =>
          TWC.MVC.History.reset(1)
#
    showLibrary: ->
      if Authentication.isLoggedIn()
        TWC.MVC.Router.setSelect(this)
        TWC.MVC.Router.load 'locker-view',
          callback: =>
            TWC.MVC.History.reset(1)
      else
        loginConfirmation = new LoginConfirmation()
        loginConfirmation.load
          data: [true, ()=>
            TWC.MVC.Router.load 'register-view',
              data: [
                ()=>
                  TWC.MVC.Router.setSelect(this)
                  TWC.MVC.Router.load 'locker-view',
                    callback: =>
                      TWC.MVC.History.reset(1)
              ],
              callback: =>
                TWC.MVC.History.reset(1)
          ]

    goBack: ->
      activePage = TWC.MVC.Router.getActivePage()
      if activePage?.uid not in @disabledBackBtnPages
        if activePage.onReturn?
          activePage.onReturn()
        else
          $('body').trigger "return"

    #custom menu rendering
    render: (callback)->
      log "VIEW: ["+@uid+"] render (start)11111"
      @disableEvents()
      @enableEvents()
      #dont change active view for zones while they are the part of normal view (unless they have set active focus) - only the popups cant set active
      if @type in ["page", "popup"] or (@defaultFocus and @type is "zone")
        TWC.MVC.Router.setActive(@uid)
        # execute for all views (including zones) that actually have something to focus on
        TWC.MVC.Router.setSelect(TWC.MVC.Router.getFocus(), true) if @defaultFocus # force select if @defaultFocus isnt null

      # render on complete
      @onRenderComplete()

      # execute render callback
      callback?()


  menuView.get()
