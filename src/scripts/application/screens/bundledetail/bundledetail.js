// Generated by CoffeeScript 1.12.4
var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty,
  indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

define(["framework", "config", "jquery", "backbone", "moment", "i18n!../../../lang/nls/manifest", "i18n!../../../lang/nls/regionsettings", "application/helpers/submenuHelper", "application/helpers/backgroundHelper", "application/api/authentication", "application/api/cd/collections/CDWishlistCollection", "application/api/cd/CDBaseRequest", "application/api/cd/CDSubscriberManagement", "application/api/cd/CDPaymentManagement", "../../api/cd/models/CDBundle", "../../screens/menu/menu", "../../popups/login/loginConfirmation", "../../popups/generic/alert", "../../models/analytics/omnituretracker", "../navhelp"], function(TWC, Config, $, Backbone, Moment, Manifest, RegionSettings, SubMenuHelper, BackgroundHelper, Authentication, CDWishlistCollection, CDBaseRequest, CDSubscriberManagement, CDPaymentManagement, CDBundleModel, TopMenu, LoginConfirmation, Alert, OmnitureTracker, NavHelp) {
  var bundleDetailView;
  bundleDetailView = (function(superClass) {
    var forceAddToFavorites;

    extend(bundleDetailView, superClass);

    function bundleDetailView() {
      return bundleDetailView.__super__.constructor.apply(this, arguments);
    }

    bundleDetailView.prototype.uid = 'bundleDetail-view';

    bundleDetailView.prototype.el = "#screen";

    bundleDetailView.prototype.type = "page";

    bundleDetailView.prototype.fullscreen = false;

    bundleDetailView.prototype.isInFavorites = false;

    bundleDetailView.prototype.entitledPricingPlanIsRental = false;

    forceAddToFavorites = false;

    bundleDetailView.prototype.external = {
      html: {
        url: "skin/templates/screens/bundledetail/bundledetail.tpl"
      }
    };

    bundleDetailView.prototype.events = {
      "enter #subMenuItems .menuButton": "showSubMenuContent",
      "enter #productFavoritesButton": "useFavoritesButton",
      "enter #productPlayButton": "playVideo",
      "enter #watchPreviewButton": "playPreview",
      "enter #productPurchaseButton": "purchaseProduct",
      "enter #productUpgradeButton": "upgradeProduct",
      "enter #productSeasonsButton": "openSeasons",
      "enter #productMoreInfoButton": "toggleFullscreen",
      "enter .episode-thumbnail": "openEpisode",
      "enter #navigationTop": "scrollUp",
      "enter #navigationBottom": "scrollDown"
    };

    bundleDetailView.prototype.zones = [
      {
        uid: "menu-zone",
        data: []
      }
    ];

    bundleDetailView.prototype.onload = function(prodId, toggleClass, focusItem) {
      var authenticated;
      this.prodId = prodId;
      this.toggleClass = toggleClass != null ? toggleClass : "overlay";
      this.focusItem = focusItem != null ? focusItem : null;
      authenticated = Authentication.isLoggedIn();
      return this.model = new CDBundleModel({
        Id: this.prodId,
        manifest: Manifest,
        SubMenuTitle: SubMenuHelper.getSubMenuTitle(),
        SubMenuOptionalVisible: SubMenuHelper.getSubMenuOptionalVisible(),
        SubMenuItems: SubMenuHelper.getContextMenu(),
        authenticated: authenticated
      });
    };

    bundleDetailView.prototype.onunload = function() {
      $(".productWindow").off();
      return this.entitledPricingPlanIsRental = false;
    };

    bundleDetailView.prototype.onRenderComplete = function() {
      var $subMenuEl, episodeListHeight, episodeRowCnt, scrollHeight;
      $(TWC.MVC.View.eventbus).trigger("loading", [true, "keep"]);
      BackgroundHelper.setBackgroundImage(this.model.get("BackgroundImage"), this.model.get("BackgroundImageBlurred"), false, 0);
      $("#mainMenu").show();
      this.fullscreen = this.toggleClass !== "overlay";
      $subMenuEl = $("#subMenuItems .menuButton.submenu[data-id='" + (SubMenuHelper.getContextMenuSelected()) + "']");
      $subMenuEl.addClass("select");
      this.formattedTitle = this.model.get("Name").toLowerCase().replace(/[^A-Za-z0-9]/g, "");
      this.ot = OmnitureTracker.get();
      this.ot.trackPageView(this.formattedTitle + ".detail.html");
      if ($(".episode-thumbnail").length) {
        episodeListHeight = $(".episode-thumbnail").outerHeight(true);
        episodeRowCnt = Math.ceil($(".episode-thumbnail").length / 3);
        console.log("h, cnt=", episodeListHeight, episodeRowCnt);
        $(".episodeList").css("height", (episodeListHeight * episodeRowCnt) + "px");
      }
      scrollHeight = $(".productWindow").get(0).scrollHeight - $(".productWindow").outerHeight(true);
      if (scrollHeight > 0) {
        $(".navigationContainer").show();
      }
      return $(".productWindow").on("scroll", (function(_this) {
        return function(e) {
          return _this.onScroll(e, scrollHeight);
        };
      })(this));
    };

    bundleDetailView.prototype.onScroll = function(e, scrollHeight) {
      if ($(".productWindow").scrollTop() === 0) {
        $("#navigationTop").hide();
      } else {
        $("#navigationTop").show();
      }
      if (Math.abs($(".productWindow").scrollTop() - scrollHeight) < 3) {
        return $("#navigationBottom").hide();
      } else {
        return $("#navigationBottom").show();
      }
    };

    bundleDetailView.prototype.updateButtons = function(callback) {
      var entitledPricingPlan, entitledQuality, expirationDate, hasFirstAccessDate, i, isLicenseAvailable, len, licenseExpirationTimeString, policy, purchasePolicies, ref, remainingTimeInMin, rentedMovieIsExpired;
      this.buyHDPricingPlan = this.model.get("OrderPricingPlans").find(function(pricingPlan) {
        return pricingPlan.equalsType("BUYHD");
      });
      this.buySDPricingPlan = this.model.get("OrderPricingPlans").find(function(pricingPlan) {
        return pricingPlan.equalsType("BUYSD");
      });
      this.buyUHDPricingPlan = this.model.get("OrderPricingPlans").find(function(pricingPlan) {
        return pricingPlan.equalsType("BUYUHD-HDR");
      });
      if (this.buyUHDPricingPlan == null) {
        this.buyUHDPricingPlan = this.model.get("OrderPricingPlans").find(function(pricingPlan) {
          return pricingPlan.equalsType("BUYUHD");
        });
      }
      this.rentHDPricingPlan = this.model.get("OrderPricingPlans").find(function(pricingPlan) {
        return pricingPlan.equalsType("RENTHD");
      });
      this.rentSDPricingPlan = this.model.get("OrderPricingPlans").find(function(pricingPlan) {
        return pricingPlan.equalsType("RENTSD");
      });
      this.rentUHDPricingPlan = this.model.get("OrderPricingPlans").find(function(pricingPlan) {
        return pricingPlan.equalsType("RENTHDR");
      });
      entitledPricingPlan = this.model.get("PricingPlans").findWhere({
        Id: this.model.get("EntitledPricingPlanId")
      });
      entitledQuality = entitledPricingPlan != null ? entitledPricingPlan.getQuality() : null;
      this.entitledPricingPlanIsRental = (entitledPricingPlan != null ? typeof entitledPricingPlan.isRental === "function" ? entitledPricingPlan.isRental() : void 0 : void 0) ? entitledPricingPlan != null ? typeof entitledPricingPlan.isRental === "function" ? entitledPricingPlan.isRental() : void 0 : void 0 : false;
      if (this.model.get("SeriesId") != null) {
        this.enableButton($("#productSeasonsButton"));
      }
      if ((this.model.get("Previews") != null) && this.model.get("Previews").length > 0) {
        this.enableButton($("#watchPreviewButton"));
      }
      if (Authentication.isLoggedIn()) {
        if (entitledPricingPlan != null) {
          if (entitledPricingPlan.isRental()) {
            purchasePolicies = this.model.get("PurchasePolicies");
            ref = purchasePolicies.models;
            for (i = 0, len = ref.length; i < len; i++) {
              policy = ref[i];
              if (policy.get("ActionCode") === 20) {
                isLicenseAvailable = (policy.get("Available") != null) && policy.get("Available") ? true : false;
                hasFirstAccessDate = (policy.get("FirstAccessDate") != null) && policy.get("FirstAccessDate") ? true : false;
                expirationDate = policy.get("ExpirationDate");
                break;
              }
            }
            rentedMovieIsExpired = !isLicenseAvailable ? true : false;
            remainingTimeInMin = Moment(expirationDate).diff(Moment(), "minutes");
            if (!rentedMovieIsExpired) {
              this.enableButton($("#productRentPlayButton"));
              licenseExpirationTimeString = entitledPricingPlan.getLicenseExpirationTime(remainingTimeInMin);
              if (licenseExpirationTimeString.length > 0) {
                $("#product .remainingWatchTimeLeft").html(licenseExpirationTimeString);
              }
            } else {
              this.showRentalButtons(entitledPricingPlan);
            }
          }
        } else {
          this.showRentalButtons(entitledPricingPlan);
        }
        if (this.buyUHDPricingPlan) {
          if (entitledPricingPlan != null) {
            if (entitledQuality === "SD") {
              $("#productUpgradeButton").html("<span class='highlight'> " + Manifest.upgrade_button + " " + (this.buyUHDPricingPlan.getPrice()) + "</span>");
              if ((Config.enableUpgrade != null) && Config.enableUpgrade) {
                this.enableButton($("#productUpgradeButton"));
              }
              this.enableButton($("#productPlayButton"));
              this.updateExtraButtons(["locked", "hidden"]);
            } else if (entitledQuality === "HD") {
              $("#productPlayButton").removeClass("disable");
              $("#productUpgradeButton").html("<span class='highlight'>" + Manifest.upgrade_button + " " + (this.buyUHDPricingPlan.getPrice()) + " </span>");
              if ((Config.enableUpgrade != null) && Config.enableUpgrade) {
                this.enableButton($("#productUpgradeButton"));
              }
              this.enableButton($("#productPlayButton"));
              this.updateExtraButtons(["hidden", "visible"]);
            } else {
              this.enableButton($("#productPlayButton"));
              this.updateExtraButtons(["hidden", "visible"]);
            }
          } else {
            if ((Config.enablePurchase != null) && Config.enablePurchase) {
              $("#productPurchaseButton").html("<span class='highlight'>" + (this.buyUHDPricingPlan.getOriginalPrice()) + "</span> " + Manifest.buy_button);
              this.enableButton($("#productPurchaseButton"));
              this.updateExtraButtons(["visible", "hidden"]);
            }
          }
        } else if (this.buyHDPricingPlan) {
          if (entitledPricingPlan != null) {
            if (entitledQuality === "SD") {
              this.enableButton($("#productPlayButton"));
              this.updateExtraButtons(["locked", "hidden"]);
            } else {
              this.enableButton($("#productPlayButton"));
              this.updateExtraButtons(["visible", "visible"]);
            }
          } else {
            if ((Config.enablePurchase != null) && Config.enablePurchase) {
              $("#productPurchaseButton").html("<span class='highlight'>" + (this.buyHDPricingPlan.getOriginalPrice()) + "</span> " + Manifest.buy_button);
              this.updateExtraButtons(["visible", "hidden"]);
            }
          }
        }
      } else {
        this.updateExtraButtons(["visible", "hidden"]);
        if (this.buyUHDPricingPlan && (Config.enablePurchase != null) && Config.enablePurchase) {
          $("#productPurchaseButton").html("<span class='highlight'>" + (this.buyUHDPricingPlan.getOriginalPrice()) + "</span> " + Manifest.buy_button);
          this.enableButton($("#productPurchaseButton"));
        }
      }
      return typeof callback === "function" ? callback() : void 0;
    };

    bundleDetailView.prototype.showRentalButtons = function(entitledPricingPlan) {
      if (this.rentUHDPricingPlan) {
        if (entitledPricingPlan != null) {
          this.enableButton($("#productPlayButton"));
          return this.updateExtraButtons(["hidden", "visible"]);
        } else {
          $("#productRentButton").html("<span class='highlight'>" + (this.rentUHDPricingPlan.getOriginalPrice()) + "</span> " + Manifest.rent_button);
          this.enableButton($("#productRentButton"));
          return this.updateExtraButtons(["visible", "hidden"]);
        }
      } else if (this.rentHDPricingPlan) {
        if (entitledPricingPlan != null) {
          this.enableButton($("#productPlayButton"));
          return this.updateExtraButtons(["visible", "visible"]);
        } else {
          $("#productRentButton").html("<span class='highlight'>" + (this.rentHDPricingPlan.getOriginalPrice()) + "</span> " + Manifest.rent_button);
          return this.updateExtraButtons(["visible", "hidden"]);
        }
      }
    };

    bundleDetailView.prototype.updateExtraButtons = function(settings) {
      var previewExtrasStatus, viewExtrasStatus;
      if (!this.model.hasExtras()) {
        return;
      }
      previewExtrasStatus = settings[0];
      viewExtrasStatus = settings[1];
      if (!this.entitledPricingPlanIsRental) {
        switch (previewExtrasStatus) {
          case "locked":
            $("#productPreviewExtrasButton").removeClass("disable").addClass("locked");
            break;
          case "visible":
            $("#productPreviewExtrasButton").removeClass("disable");
        }
        switch (viewExtrasStatus) {
          case "visible":
            return $("#productPreviewExtrasButton").removeClass("disable");
        }
      }
    };

    bundleDetailView.prototype.setInitFocus = function() {
      if (this.focusItem !== null) {
        return TWC.MVC.Router.setFocus(this.focusItem);
      } else {
        if ($(".buttonContainer .button:not(.disable):first").length) {
          return TWC.MVC.Router.setFocus($(".buttonContainer .button:not(.disable):first"));
        } else {
          return this.moveToMenu();
        }
      }
    };

    bundleDetailView.prototype.onZoneRenderComplete = function() {
      TWC.MVC.Router.setActive(this.uid);
      if (this.toggleClass !== "overlay") {
        $("#product").removeClass("overlay").addClass(this.toggleClass);
      } else {
        $("#product").addClass("overlay");
      }
      if (Config.useFavorites) {
        this.updateFavoritesButton();
      }
      return this.updateButtons((function(_this) {
        return function() {
          setTimeout(function() {
            return _this.setInitFocus();
          }, 250);
          return $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"]);
        };
      })(this));
    };

    bundleDetailView.prototype.onReturn = function() {
      BackgroundHelper.enableBlur();
      $("#product").removeClass("overlay").removeClass("fullscreen");
      return setTimeout((function(_this) {
        return function() {
          return TWC.MVC.History.popState();
        };
      })(this), 300);
    };

    bundleDetailView.prototype.openSeasons = function() {
      var seriesId;
      seriesId = this.model.get("SeriesId");
      return TWC.MVC.Router.load("seasonlist-view", {
        data: [seriesId]
      });
    };

    bundleDetailView.prototype.openEpisode = function(el) {
      var $el, page, productId, productType;
      $el = $(el.target);
      productId = $el.data("id");
      productType = $el.data("type");
      switch (productType) {
        case 1:
          page = 'productDetail-view';
          break;
        case 2:
        case 4:
          page = 'bundleDetail-view';
          break;
        default:
          page = 'productDetail-view';
      }
      return TWC.MVC.Router.load(page, {
        data: [productId]
      });
    };

    bundleDetailView.prototype.updateFavoritesButton = function() {
      var status;
      if (Authentication.isLoggedIn()) {
        $("#productFavoritesButton").fadeTo(0, 0.4);
        this.blockFavoritesButton = true;
        $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"]);
        $("#productFavoritesButton").removeClass("disable");
        return CDWishlistCollection.checkIfInWishList(this.model.get("Id"), (function(_this) {
          return function(isInFavorites) {
            var status;
            _this.isInFavorites = isInFavorites;
            $("#productFavoritesButton").removeClass("removeFromFavorites").removeClass("addToFavorites");
            status = _this.isInFavorites ? "removeFromFavorites" : status = "addToFavorites";
            $("#productFavoritesButton").addClass(status);
            if (_this.forceAddToFavorites && !_this.isInFavorites) {
              _this.useFavoritesButton();
            }
            _this.forceAddToFavorites = false;
            _this.blockFavoritesButton = false;
            return $("#productFavoritesButton").fadeTo(0, 1);
          };
        })(this));
      } else {
        $("#productFavoritesButton").removeClass("disable");
        $("#productFavoritesButton").removeClass("removeFromFavorites").removeClass("addToFavorites");
        status = "addToFavorites";
        $("#productFavoritesButton").addClass(status);
        return this.blockFavoritesButton = false;
      }
    };

    bundleDetailView.prototype.showConfirmationPopup = function(title, message) {
      var confirmAlert;
      confirmAlert = new Alert();
      return confirmAlert.load({
        data: [title, message, Manifest.popup_exit_close]
      });
    };

    bundleDetailView.prototype.addToFavorites = function() {
      return this.forceAddToFavorites = true;
    };

    bundleDetailView.prototype.useFavoritesButton = function() {
      var historySize, loginConfirmation;
      if (this.blockFavoritesButton) {
        return;
      }
      this.ot.trackOutboundClick("ultraapp.com/detail", this.formattedTitle + "_favorites_button");
      if (!Authentication.isLoggedIn()) {
        historySize = TWC.MVC.History.size();
        loginConfirmation = new LoginConfirmation();
        return loginConfirmation.load({
          data: [
            false, (function(_this) {
              return function() {
                return TWC.MVC.Router.load("login-view", {
                  data: [
                    function() {
                      TWC.MVC.History.reset(historySize);
                      return TWC.MVC.History.popState(function() {
                        return TWC.MVC.Router.find("bundleDetail-view").addToFavorites();
                      });
                    }
                  ]
                });
              };
            })(this)
          ]
        });
      } else {
        $(TWC.MVC.View.eventbus).trigger("loading", true);
        if (this.isInFavorites) {
          return CDWishlistCollection.removeFromWishList(this.model.get("Id"), (function(_this) {
            return function(error, response) {
              var isRemoved;
              isRemoved = response.Fault == null;
              if (isRemoved) {
                _this.isInFavorites = false;
                $(TWC.MVC.View.eventbus).trigger("loading", false);
                $("#productFavoritesButton").removeClass("removeFromFavorites");
                return $("#productFavoritesButton").addClass("addToFavorites");
              }
            };
          })(this));
        } else {
          return CDWishlistCollection.addToWishList(this.model.get("Id"), (function(_this) {
            return function(error, response) {
              var isAdded;
              isAdded = response.Fault == null;
              if (isAdded) {
                _this.isInFavorites = true;
                $(TWC.MVC.View.eventbus).trigger("loading", false);
                $("#productFavoritesButton").removeClass("addToFavorites");
                return $("#productFavoritesButton").addClass("removeFromFavorites");
              }
            };
          })(this));
        }
      }
    };

    bundleDetailView.prototype.focusUp = function() {
      var $el, currentPos;
      $el = $(TWC.MVC.Router.getFocus());
      if ($el.hasClass("button")) {
        if (this.fullscreen) {
          this.toggleFullscreen();
        } else {
          this.focusBack();
        }
        if (!$("#productPlayButton").hasClass("disable")) {
          return TWC.MVC.Router.setFocus("#productPlayButton");
        }
      } else if ($el.hasClass("episode-thumbnail")) {
        currentPos = parseInt($el.attr("id").split("-")[1]);
        if (currentPos < 3) {
          return TWC.MVC.Router.setFocus($(".buttonContainer .button:not(.disable):first"));
        } else {
          return this.focusThumbnailUp(currentPos);
        }
      }
    };

    bundleDetailView.prototype.focusDown = function() {
      var $el, currentPos;
      $el = $(TWC.MVC.Router.getFocus());
      if ($el.attr("id") === "productPlayButton") {
        return TWC.MVC.Router.setFocus($(".buttonContainer .button:not(.disable):first"));
      } else if ($el.hasClass("button")) {
        if (this.fullscreen) {
          if ($("#episode-0").length > 0) {
            return TWC.MVC.Router.setFocus("#episode-0");
          }
        } else {
          return this.toggleFullscreen();
        }
      } else if ($el.hasClass("episode-thumbnail")) {
        currentPos = parseInt($el.attr("id").split("-")[1]);
        return this.focusThumbnailDown(currentPos);
      }
    };

    bundleDetailView.prototype.focusLeft = function() {
      var $el, currentPos;
      $el = $(TWC.MVC.Router.getFocus());
      if ($el.attr("id") === "productPlayButton") {
        return this.moveToMenu();
      } else if ($el.hasClass("button")) {
        if ($el.prevAll(":not(.disable)").length > 0) {
          return TWC.MVC.Router.setFocus($el.prevAll(":not(.disable):first"));
        } else {
          return this.moveToMenu();
        }
      } else if ($el.hasClass("episode-thumbnail")) {
        currentPos = parseInt($el.attr("id").split("-")[1]);
        if (currentPos === 0) {
          return TWC.MVC.Router.setFocus($(".buttonContainer .button:not(.disable):first"));
        }
        if ($("#episode-" + (currentPos - 1)).length > 0) {
          TWC.MVC.Router.setFocus($("#episode-" + (currentPos - 1)));
        }
        if (currentPos > 5) {
          if ((currentPos - 3) % 3 === 0) {
            return this.focusThumbnailUp(currentPos - 2, false);
          }
        }
      }
    };

    bundleDetailView.prototype.focusRight = function() {
      var $el, currentPos;
      $el = $(TWC.MVC.Router.getFocus());
      if ($el.hasClass("button")) {
        return TWC.MVC.Router.setFocus($el.nextAll(":not(.disable):first"));
      } else if ($el.hasClass("episode-thumbnail")) {
        currentPos = parseInt($el.attr("id").split("-")[1]);
        if ($("#episode-" + (currentPos + 1)).length > 0) {
          TWC.MVC.Router.setFocus($("#episode-" + (currentPos + 1)));
        }
        if ((currentPos + 1) % 3 === 0) {
          return this.focusThumbnailDown(currentPos - 2, false);
        }
      }
    };

    bundleDetailView.prototype.focusBack = function() {
      return TWC.MVC.Router.setFocus($("#backButton"));
    };

    bundleDetailView.prototype.backLeft = function() {
      return this.moveToMainMenu();
    };

    bundleDetailView.prototype.backRight = function() {
      return this.moveToContent();
    };

    bundleDetailView.prototype.backDown = function() {
      return this.moveToMenu();
    };

    bundleDetailView.prototype.scrollOffset = 0;

    bundleDetailView.prototype.focusThumbnailDown = function(pos, focus) {
      var currentRow, totalRows;
      if (focus == null) {
        focus = true;
      }
      currentRow = Math.floor(pos / 3);
      totalRows = Math.ceil(this.model.get("BundleChildren").length / 3);
      if (currentRow === totalRows - 1) {
        return;
      }
      if (currentRow - this.scrollOffset === 1) {
        this.scrollOffset++;
        $("#product").removeClass().addClass("episodesOffset-" + this.scrollOffset);
      }
      if (focus) {
        if ($("#episode-" + (pos + 3)).length > 0) {
          return TWC.MVC.Router.setFocus($("#episode-" + (pos + 3)));
        }
      }
    };

    bundleDetailView.prototype.focusThumbnailUp = function(pos, focus) {
      var currentRow;
      if (focus == null) {
        focus = true;
      }
      currentRow = Math.floor(pos / 3);
      if (currentRow === 0) {
        return;
      }
      if (currentRow === this.scrollOffset) {
        this.scrollOffset--;
        if (this.scrollOffset === 0) {
          $("#product").removeClass().addClass("fullscreen");
        } else {
          $("#product").removeClass().addClass("episodesOffset-" + this.scrollOffset);
        }
      }
      if (focus) {
        if ($("#episode-" + (pos - 3)).length > 0) {
          return TWC.MVC.Router.setFocus($("#episode-" + (pos - 3)));
        }
      }
    };

    bundleDetailView.prototype.enableButton = function($el) {
      return $el.removeClass("disable");
    };

    bundleDetailView.prototype.showExtrasButton = function() {
      if (this.model.hasExtras()) {
        return this.enableButton($("#productExtrasButton"));
      }
    };

    bundleDetailView.prototype.showSubMenuContent = function(el) {
      var $el;
      $el = $(el.target);
      if (SubMenuHelper.getSubMenuContext() === SubMenuHelper.CONTEXT_STOREFRONT) {
        BackgroundHelper.enableBlur();
        $("#product").removeClass("overlay").removeClass("fullscreen");
        return setTimeout((function(_this) {
          return function() {
            return TWC.MVC.Router.load("storefrontpage-view", {
              data: [$el.data("id")],
              callback: function() {
                return TWC.MVC.History.reset();
              }
            });
          };
        })(this), 300);
      } else if (SubMenuHelper.getSubMenuContext() === SubMenuHelper.CONTEXT_LOCKER) {
        return TWC.MVC.Router.load("locker-view", {
          data: [$el.data("id")]
        });
      } else if (SubMenuHelper.getSubMenuContext() === SubMenuHelper.CONTEXT_SEARCH) {
        return TWC.MVC.Router.load("video-search-view", {
          data: ["", [], $el.data("id")]
        });
      } else if (SubMenuHelper.getSubMenuContext() === SubMenuHelper.CONTEXT_FAVORITES) {
        return TWC.MVC.Router.load("favorites-view", {
          data: [$el.data("id")]
        });
      }
    };

    bundleDetailView.prototype.toggleFullscreen = function() {
      this.fullscreen = !this.fullscreen;
      if (this.fullscreen) {
        $("#productMoreInfoButton .highlight").text(Manifest.less_button);
        return $("#product").removeClass("overlay").addClass("fullscreen");
      } else {
        $("#productMoreInfoButton .highlight").text(Manifest.more_button);
        return $("#product").removeClass("fullscreen").addClass("overlay");
      }
    };

    bundleDetailView.prototype.openExtraMedia = function() {
      var $el, pos, videoModel;
      $el = $(TWC.MVC.Router.getFocus());
      pos = parseInt($el.attr("id").split("-")[1]);
      this.updateHistory(this.model.get("Id"), $("#product").attr("class"), TWC.MVC.Router.getFocus());
      if ($el.hasClass("video")) {
        videoModel = this.model.get("RelatedVideos")[pos];
        return TWC.MVC.Router.load("videoplayer-view", {
          data: [videoModel, false, true]
        });
      } else {
        return TWC.MVC.Router.load("imageviewer-view", {
          data: [this.model.get("RelatedImages"), pos]
        });
      }
    };

    bundleDetailView.prototype.playVideo = function() {
      return TWC.MVC.Router.load('videoplayer-view', {
        data: [this.model]
      });
    };

    bundleDetailView.prototype.playPreview = function() {
      return TWC.MVC.Router.load('videoplayer-view', {
        data: [this.model, true]
      });
    };

    bundleDetailView.prototype.returnToLoginScreen = function(type) {
      var loginScreenDetails;
      loginScreenDetails = {};
      loginScreenDetails.productId = this.prodId;
      loginScreenDetails.returnValue = true;
      loginScreenDetails.purchaseType = type;
      return loginScreenDetails.productType = "product";
    };

    bundleDetailView.prototype.upgradeProduct = function(e) {
      return this.purchaseProduct(e, true);
    };

    bundleDetailView.prototype.purchaseProduct = function(e, upgrade, purchaseCallback, purchaseFailCallback, pricingPlan) {
      var historySize, loginConfirmation, purchaseProduct;
      if (upgrade == null) {
        upgrade = false;
      }
      if (purchaseCallback == null) {
        purchaseCallback = null;
      }
      if (purchaseFailCallback == null) {
        purchaseFailCallback = null;
      }
      if (pricingPlan == null) {
        pricingPlan = null;
      }
      this.ot.trackOutboundClickToBuy("ultraapp.com/confirmpurchase", "4kultra_" + this.formattedTitle + "_button");
      if (Authentication.isLoggedIn()) {
        $(TWC.MVC.View.eventbus).trigger("loading", [true]);
        purchaseProduct = (function(_this) {
          return function() {
            var currency;
            currency = RegionSettings.default_payment_instruments_currency;
            return CDPaymentManagement.retrieveWallet(currency, function(error, data) {
              return CDPaymentManagement.updatePaypalData(data.PaymentInstruments, function(paymentInstruments) {
                data.PaymentInstruments = paymentInstruments;
                if (!error) {
                  paymentInstruments = _.filter(data.PaymentInstruments, function(paymentInstrument) {
                    var ref;
                    return ref = paymentInstrument.Type, indexOf.call(Config.supportedPaymentOptions, ref) >= 0;
                  });
                  if (paymentInstruments.length === 0) {
                    return TWC.MVC.Router.load("pin-view", {
                      data: [
                        "", "", "paymentError", function() {
                          TWC.MVC.History.reset(2);
                          return TWC.MVC.History.popState();
                        }
                      ]
                    });
                  } else {
                    return TWC.MVC.Router.load("paymentconfirmation-view", {
                      data: [_this.model, _this.buyUHDPricingPlan, paymentInstruments, "", upgrade, null, purchaseCallback, purchaseFailCallback]
                    });
                  }
                }
              });
            });
          };
        })(this);
        if (Config.enablePaymentPin) {
          return CDSubscriberManagement.retrieveDevicePinStatus((function(_this) {
            return function(error, hasPin) {
              if (hasPin) {
                return TWC.MVC.Router.load("pin-view", {
                  data: [_this.model.get("Name"), "", "purchasePin", purchaseProduct, purchaseFailCallback]
                });
              } else {
                return purchaseProduct();
              }
            };
          })(this));
        } else {
          return purchaseProduct();
        }
      } else {
        historySize = TWC.MVC.History.size();
        loginConfirmation = new LoginConfirmation();
        return loginConfirmation.load({
          data: [
            false, (function(_this) {
              return function() {
                return TWC.MVC.Router.load("login-view", {
                  data: [
                    function() {
                      return _this.model.refresh(function() {
                        var entitledPricingPlan, messageAlert;
                        entitledPricingPlan = _this.model.get("PricingPlans").findWhere({
                          Id: _this.model.get("EntitledPricingPlanId")
                        });
                        if (entitledPricingPlan != null) {
                          messageAlert = new Alert();
                          return messageAlert.load({
                            data: [
                              "", Manifest.payment_already_purchased, Manifest.ok, function() {
                                if (purchaseCallback != null) {
                                  return purchaseCallback();
                                }
                                TWC.MVC.History.reset(historySize);
                                return TWC.MVC.History.popState();
                              }
                            ]
                          });
                        } else {
                          return _this.purchaseProduct(e, upgrade, purchaseCallback, purchaseFailCallback);
                        }
                      });
                    }, function() {
                      return typeof purchaseFailCallback === "function" ? purchaseFailCallback() : void 0;
                    }
                  ]
                });
              };
            })(this), false, (function(_this) {
              return function() {
                return typeof purchaseFailCallback === "function" ? purchaseFailCallback() : void 0;
              };
            })(this)
          ]
        });
      }
    };

    bundleDetailView.prototype.moveToMenu = function(focusOnLast) {
      if (focusOnLast == null) {
        focusOnLast = false;
      }
      if (focusOnLast) {
        return TWC.MVC.Router.setFocus($("#subMenuItems .menuButton:last"));
      } else {
        if ($("#submenu_0").length) {
          return TWC.MVC.Router.setFocus("#submenu_0");
        } else {
          return this.moveToMainMenu();
        }
      }
    };

    bundleDetailView.prototype.moveToMainMenu = function() {
      TWC.MVC.Router.setActive("menu-zone");
      return TWC.MVC.Router.setFocus("#browse");
    };

    bundleDetailView.prototype.moveToContent = function() {
      if ($(".buttonContainer .button:not(.disable):first").length) {
        return TWC.MVC.Router.setFocus($(".buttonContainer .button:not(.disable):first"));
      } else if ($("#favoritesButton:not(.disable)") && $("#noTrailerAvailable").hasClass("disable")) {
        return TWC.MVC.Router.setFocus($("#favoritesButton"));
      } else if ($("#pointerNaviUp").is(":visible")) {
        return TWC.MVC.Router.setFocus("#pointerNaviUp");
      } else if ($("#pointerNaviDown").is(":visible")) {
        return TWC.MVC.Router.setFocus("#pointerNaviDown");
      } else if (this.recommendedProductsAvailable) {
        return TWC.MVC.Router.setFocus("recommendedProduct_0");
      }
    };

    bundleDetailView.prototype.refresh = function() {
      var authenticated;
      authenticated = Authentication.isLoggedIn();
      this.model = new CDBundleModel({
        Id: this.prodId,
        manifest: Manifest,
        SubMenuTitle: SubMenuHelper.getSubMenuTitle(),
        SubMenuOptionalVisible: SubMenuHelper.getSubMenuOptionalVisible(),
        SubMenuItems: SubMenuHelper.getContextMenu(),
        authenticated: authenticated
      });
      return bundleDetailView.__super__.refresh.apply(this, arguments);
    };

    bundleDetailView.prototype.scrollUp = function() {
      return $(".productWindow").animate({
        scrollTop: $(".productWindow").scrollTop() - 500
      });
    };

    bundleDetailView.prototype.scrollDown = function() {
      return $(".productWindow").animate({
        scrollTop: $(".productWindow").scrollTop() + 500
      });
    };

    return bundleDetailView;

  })(TWC.MVC.View);
  return bundleDetailView.get();
});
