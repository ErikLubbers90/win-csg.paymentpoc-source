define [
  "framework",
  "config",
  "jquery",
  "backbone",
  "moment",
  "i18n!../../../lang/nls/manifest",
  "i18n!../../../lang/nls/regionsettings",
  "application/helpers/submenuHelper",
  "application/helpers/backgroundHelper",
  "application/api/authentication",
  "application/api/cd/collections/CDWishlistCollection",
  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDSubscriberManagement",
  "application/api/cd/CDPaymentManagement",
  "../../api/cd/models/CDBundle",
  "../../screens/menu/menu",
  "../../popups/login/loginConfirmation",
  "../../popups/generic/alert",
  "../../models/analytics/omnituretracker",
  "../navhelp"
], (TWC, Config, $, Backbone, Moment, Manifest, RegionSettings, SubMenuHelper, BackgroundHelper, Authentication, CDWishlistCollection, CDBaseRequest, CDSubscriberManagement, CDPaymentManagement, CDBundleModel, TopMenu, LoginConfirmation, Alert, OmnitureTracker, NavHelp) ->

  class bundleDetailView extends TWC.MVC.View
    uid: 'bundleDetail-view'

    el: "#screen"

    type: "page"

    fullscreen: false

    isInFavorites: false

    entitledPricingPlanIsRental: false

    forceAddToFavorites = false

    external:
      html:
        url:"skin/templates/screens/bundledetail/bundledetail.tpl"

    events:
      "enter #subMenuItems .menuButton": "showSubMenuContent"
      "enter #productFavoritesButton": "useFavoritesButton"
      "enter #productPlayButton": "playVideo"
      "enter #watchPreviewButton": "playPreview"
      "enter #productPurchaseButton": "purchaseProduct"
      "enter #productUpgradeButton": "upgradeProduct"
      "enter #productSeasonsButton": "openSeasons"
#      "enter #productRentButton": "showRentOrderPopup"
#      "enter #productExtrasButton": "showExtras"
      "enter #productMoreInfoButton": "toggleFullscreen"
      "enter .episode-thumbnail" : "openEpisode"
      "enter #navigationTop": "scrollUp"
      "enter #navigationBottom": "scrollDown"


    zones: [
      {uid:"menu-zone", data:[]},
    ]

    onload: (@prodId, @toggleClass = "overlay", @focusItem = null) ->
      authenticated = Authentication.isLoggedIn()
      @model = new CDBundleModel({
        Id:@prodId,
        manifest: Manifest,
        SubMenuTitle: SubMenuHelper.getSubMenuTitle(),
        SubMenuOptionalVisible: SubMenuHelper.getSubMenuOptionalVisible(),
        SubMenuItems: SubMenuHelper.getContextMenu(),
        authenticated: authenticated
      })

    onunload: ->
      $(".productWindow").off()
      @entitledPricingPlanIsRental = false

    onRenderComplete: ->
      $(TWC.MVC.View.eventbus).trigger("loading", [true, "keep"])
      BackgroundHelper.setBackgroundImage(@model.get("BackgroundImage"), @model.get("BackgroundImageBlurred"), false, 0)

      $("#mainMenu").show()

      @fullscreen = @toggleClass isnt "overlay"

      # Set submenu selected
      $subMenuEl = $("#subMenuItems .menuButton.submenu[data-id='#{SubMenuHelper.getContextMenuSelected()}']")
      $subMenuEl.addClass "select"

      @formattedTitle = @model.get("Name").toLowerCase().replace(/[^A-Za-z0-9]/g, "")
      @ot = OmnitureTracker.get()
      @ot.trackPageView("#{@formattedTitle}.detail.html")

      if $(".episode-thumbnail").length
        episodeListHeight = $(".episode-thumbnail").outerHeight(true)
        episodeRowCnt = Math.ceil($(".episode-thumbnail").length / 3)
        console.log("h, cnt=", episodeListHeight, episodeRowCnt)
        $(".episodeList").css("height", (episodeListHeight*episodeRowCnt) + "px")

      scrollHeight = $(".productWindow").get(0).scrollHeight - $(".productWindow").outerHeight(true);
      $(".navigationContainer").show() if scrollHeight > 0
      $(".productWindow").on "scroll", (e)=>
        @onScroll(e, scrollHeight)

    onScroll:(e, scrollHeight)->
      if $(".productWindow").scrollTop() is 0
        $("#navigationTop").hide()
      else
        $("#navigationTop").show()

      if Math.abs( $(".productWindow").scrollTop() - scrollHeight) < 3
        $("#navigationBottom").hide()
      else
        $("#navigationBottom").show()


    updateButtons: (callback) ->
      # Get pricing plans
      @buyHDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
        pricingPlan.equalsType("BUYHD")
      @buySDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
        pricingPlan.equalsType("BUYSD")
      @buyUHDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
        pricingPlan.equalsType("BUYUHD-HDR")
      if not @buyUHDPricingPlan?
        @buyUHDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
          pricingPlan.equalsType("BUYUHD")
      @rentHDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
        pricingPlan.equalsType("RENTHD")
      @rentSDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
        pricingPlan.equalsType("RENTSD")
      @rentUHDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
        pricingPlan.equalsType("RENTHDR")


      # Get entitled pricing plan
      entitledPricingPlan = @model.get("PricingPlans").findWhere({ Id : @model.get("EntitledPricingPlanId")})
      entitledQuality = if entitledPricingPlan? then entitledPricingPlan.getQuality() else null
      @entitledPricingPlanIsRental = if entitledPricingPlan?.isRental?() then entitledPricingPlan?.isRental?() else false

      #View all seasons button
      if @model.get("SeriesId")?
        @enableButton( $("#productSeasonsButton"))

      # Preview button
      if @model.get("Previews")? and @model.get("Previews").length > 0
        @enableButton($("#watchPreviewButton"))

      if Authentication.isLoggedIn() #user logged in

        # in case it is a rental entitlement show the expiration time and rental button
        if entitledPricingPlan?
          if entitledPricingPlan.isRental()
            purchasePolicies =  @model.get("PurchasePolicies")

            for policy in purchasePolicies.models
              if policy.get("ActionCode") is 20
                isLicenseAvailable = if policy.get("Available")? and policy.get("Available") then true else false
                hasFirstAccessDate = if policy.get("FirstAccessDate")? and policy.get("FirstAccessDate") then true else false
                expirationDate = policy.get("ExpirationDate")
                break

            # Expired                  : no license available and has first accessdate
            # Rented movie not started : license available and no first accessdate
            # Rented movie started     : license available and has first accessdate
            rentedMovieIsExpired = if not isLicenseAvailable then true else false
            remainingTimeInMin = Moment(expirationDate).diff(Moment(), "minutes")

            if not rentedMovieIsExpired
              @enableButton($("#productRentPlayButton"))

              # rented movie available
              licenseExpirationTimeString = entitledPricingPlan.getLicenseExpirationTime(remainingTimeInMin)

              # Show license remaining time
              if licenseExpirationTimeString.length > 0 then $("#product .remainingWatchTimeLeft").html(licenseExpirationTimeString)

            else
              # Rented movie expired allow a user to rent it again
              @showRentalButtons(entitledPricingPlan)
        else
          @showRentalButtons(entitledPricingPlan)

        if @buyUHDPricingPlan #UHD available
          if entitledPricingPlan? #user owns content
            if entitledQuality is "SD" 
#              console.log('uhd plan:', @buyUHDPricingPlan)
              $("#productUpgradeButton").html("<span class='highlight'> #{Manifest.upgrade_button} #{@buyUHDPricingPlan.getPrice()}</span>" )
              @enableButton($("#productUpgradeButton")) if Config.enableUpgrade? and Config.enableUpgrade 
              @enableButton($("#productPlayButton"))
              @updateExtraButtons(["locked", "hidden"])
#              console.log "LI - UHD pricing plan available, owned SD", entitledPricingPlan, entitledQuality
            else if entitledQuality is "HD"
              $("#productPlayButton").removeClass("disable")
              $("#productUpgradeButton").html("<span class='highlight'>#{Manifest.upgrade_button} #{@buyUHDPricingPlan.getPrice()} </span>" )
              @enableButton($("#productUpgradeButton")) if Config.enableUpgrade? and Config.enableUpgrade
              @enableButton($("#productPlayButton"))
              @updateExtraButtons(["hidden", "visible"])
#              console.log "LI - UHD pricing plan available, owned HD", entitledPricingPlan, entitledQuality
            else
              @enableButton($("#productPlayButton"))
              @updateExtraButtons(["hidden", "visible"])
#              console.log "LI - UHD pricing plan available, owned UHD", entitledPricingPlan, entitledQuality
          else
            if Config.enablePurchase? and Config.enablePurchase
              $("#productPurchaseButton").html("<span class='highlight'>#{@buyUHDPricingPlan.getOriginalPrice()}</span> #{Manifest.buy_button}" )
              @enableButton($("#productPurchaseButton"))
              @updateExtraButtons(["visible", "hidden"])
  #            console.log "LI - UHD pricing plan available, not owned"

        else if @buyHDPricingPlan #only HD is available
          if entitledPricingPlan? #user owns content
            if entitledQuality is "SD"
              @enableButton($("#productPlayButton"))
              @updateExtraButtons(["locked", "hidden"])
#              console.log "LI - UHD pricing plan available, owned SD", entitledPricingPlan, entitledQuality
            else  #owns HD
              @enableButton($("#productPlayButton"))
              @updateExtraButtons(["visible", "visible"])
#              console.log "LI - UHD pricing plan available, owned HD", entitledPricingPlan, entitledQuality
          else
            if Config.enablePurchase? and Config.enablePurchase
              $("#productPurchaseButton").html("<span class='highlight'>#{@buyHDPricingPlan.getOriginalPrice()}</span> #{Manifest.buy_button}" )
              @updateExtraButtons(["visible", "hidden"])
      else
        @updateExtraButtons(["visible", "hidden"])
        if @buyUHDPricingPlan and Config.enablePurchase? and Config.enablePurchase
          $("#productPurchaseButton").html("<span class='highlight'>#{@buyUHDPricingPlan.getOriginalPrice()}</span> #{Manifest.buy_button}" )
          @enableButton($("#productPurchaseButton"))

      callback?()


    showRentalButtons: (entitledPricingPlan)->
      if @rentUHDPricingPlan
        if entitledPricingPlan? #user owns content
          @enableButton($("#productPlayButton"))
          @updateExtraButtons(["hidden", "visible"])
        else
          $("#productRentButton").html("<span class='highlight'>#{@rentUHDPricingPlan.getOriginalPrice()}</span> #{Manifest.rent_button}" )
          @enableButton($("#productRentButton"))
          @updateExtraButtons(["visible", "hidden"])

      else if @rentHDPricingPlan
        if entitledPricingPlan? #user owns content
          @enableButton($("#productPlayButton"))
          @updateExtraButtons(["visible", "visible"])
        else
          #            console.log("buyHDPricingPlan", @buyHDPricingPlan)
          $("#productRentButton").html("<span class='highlight'>#{@rentHDPricingPlan.getOriginalPrice()}</span> #{Manifest.rent_button}" )
          @updateExtraButtons(["visible", "hidden"])
    #            console.log "LI - UHD pricing plan available, not owned"
    #          console.log "LI - UHD pricing plan not available"

#        console.log "LO - logged out"

#      #if user returns to detail page directly from login screen open buy/rent popup,
#      #if entitledPricingPlan exists no need to show popup
#      if @openPaymentPopup and Config.enablePricesWhenUnauthenticated and not entitledPricingPlan
#        if @purchaseType is "buy" then @showBuyOrderPopup() else @showRentOrderPopup()

    updateExtraButtons: (settings)->
      return if not @model.hasExtras()
      previewExtrasStatus = settings[0]
      viewExtrasStatus = settings[1]

      # Do not display extra buttons if product is rental
      if not @entitledPricingPlanIsRental
        switch previewExtrasStatus
          when "locked" then $("#productPreviewExtrasButton").removeClass("disable").addClass("locked")
          when "visible" then $("#productPreviewExtrasButton").removeClass("disable")

        switch viewExtrasStatus
          when "visible" then $("#productPreviewExtrasButton").removeClass("disable")

    setInitFocus: ->
      if @focusItem isnt null
        TWC.MVC.Router.setFocus @focusItem
      else
        if $(".buttonContainer .button:not(.disable):first").length
          TWC.MVC.Router.setFocus $(".buttonContainer .button:not(.disable):first")
        else
          @moveToMenu()

    onZoneRenderComplete: ->
      TWC.MVC.Router.setActive @uid


      if @toggleClass isnt "overlay"
        $("#product").removeClass("overlay").addClass(@toggleClass)
      else
        $("#product").addClass("overlay")

      #Favorites button
      @updateFavoritesButton() if Config.useFavorites

      @updateButtons(=>
        setTimeout =>
          @setInitFocus()
        , 250
        $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])
      )

    onReturn: ->
      BackgroundHelper.enableBlur()

      $("#product").removeClass("overlay").removeClass("fullscreen")
      setTimeout ()=>
        TWC.MVC.History.popState()
      , 300


    openSeasons: ()->
      seriesId = @model.get("SeriesId")
      TWC.MVC.Router.load "seasonlist-view",
        data: [seriesId]

    openEpisode: (el) ->
      $el = $(el.target)
      productId = $el.data("id")
      productType = $el.data("type")

      switch productType
        when 1
        # Product/episode
          page = 'productDetail-view'
        when 2, 4
        # Bundle/season
          page = 'bundleDetail-view'
        else
          page = 'productDetail-view'

      TWC.MVC.Router.load page,
        data: [productId]

    updateFavoritesButton: ()->

      if Authentication.isLoggedIn()

        $("#productFavoritesButton").fadeTo(0, 0.4)
        @blockFavoritesButton = true

        $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])

        $("#productFavoritesButton").removeClass("disable")    # Enable the button after action is available

        #check if product is present in favorites and change button status accordingly
        CDWishlistCollection.checkIfInWishList  @model.get("Id"), (@isInFavorites) =>

          $("#productFavoritesButton").removeClass("removeFromFavorites").removeClass("addToFavorites")
          status = if @isInFavorites then "removeFromFavorites" else status = "addToFavorites"
          $("#productFavoritesButton").addClass(status)
          if @forceAddToFavorites and not @isInFavorites
            @useFavoritesButton()
          @forceAddToFavorites = false
          @blockFavoritesButton = false

          $("#productFavoritesButton").fadeTo(0, 1)

      else
        $("#productFavoritesButton").removeClass("disable")    # Enable the button after action is available
        $("#productFavoritesButton").removeClass("removeFromFavorites").removeClass("addToFavorites")
        status = "addToFavorites"
        $("#productFavoritesButton").addClass(status)
        @blockFavoritesButton = false






    # Display favorites add/remove success popup
    showConfirmationPopup: (title, message) ->
      confirmAlert = new Alert()
      confirmAlert.load
        data: [title, message, Manifest.popup_exit_close]

    addToFavorites: ->
      @forceAddToFavorites = true

    # Add to or delete from favorites
    useFavoritesButton: ->
      return if @blockFavoritesButton
      @ot.trackOutboundClick("ultraapp.com/detail", "#{@formattedTitle}_favorites_button")
      if not Authentication.isLoggedIn()
        historySize = TWC.MVC.History.size()
        loginConfirmation = new LoginConfirmation()
        loginConfirmation.load
          data: [false, ()=>
            TWC.MVC.Router.load "login-view",
              data: [
                ()=>
                  TWC.MVC.History.reset(historySize)
                  TWC.MVC.History.popState ()=>
                    TWC.MVC.Router.find("bundleDetail-view").addToFavorites()
              ]
          ]
      else
        $(TWC.MVC.View.eventbus).trigger("loading", true)
        if @isInFavorites
          CDWishlistCollection.removeFromWishList @model.get("Id"), (error, response) =>
            # if request successful response.Fault will be null
            isRemoved = not response.Fault?
            if isRemoved
              @isInFavorites = false
              $(TWC.MVC.View.eventbus).trigger("loading", false)
              $("#productFavoritesButton").removeClass("removeFromFavorites")
              $("#productFavoritesButton").addClass("addToFavorites")
          #            @showConfirmationPopup(Manifest.removed_from_favorites, "")
        else
          CDWishlistCollection.addToWishList @model.get("Id"), (error, response) =>
            # if request successful response.Fault will be null
            isAdded = not response.Fault?
            if isAdded
              @isInFavorites = true
              $(TWC.MVC.View.eventbus).trigger("loading", false)
              $("#productFavoritesButton").removeClass("addToFavorites")
              $("#productFavoritesButton").addClass("removeFromFavorites")
    #            @showConfirmationPopup(Manifest.added_to_favorites, "")

    focusUp: ->
      $el = $(TWC.MVC.Router.getFocus())
      if($el.hasClass("button"))
        if @fullscreen
          @toggleFullscreen()
        else
          @focusBack()
        if( not $("#productPlayButton").hasClass("disable"))
          TWC.MVC.Router.setFocus "#productPlayButton"
      else if $el.hasClass("episode-thumbnail")
        currentPos = parseInt($el.attr("id").split("-")[1])
        if currentPos < 3
          TWC.MVC.Router.setFocus $(".buttonContainer .button:not(.disable):first")
        else
          @focusThumbnailUp(currentPos)


    focusDown: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.attr("id") is "productPlayButton"
        TWC.MVC.Router.setFocus $(".buttonContainer .button:not(.disable):first")
      else if $el.hasClass("button")
        if @fullscreen
          return TWC.MVC.Router.setFocus "#episode-0" if $("#episode-0").length > 0
        else
          @toggleFullscreen()
      else if $el.hasClass("episode-thumbnail")
        currentPos = parseInt($el.attr("id").split("-")[1])
        @focusThumbnailDown(currentPos)

    focusLeft: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.attr("id") is "productPlayButton"
        @moveToMenu()
      else if $el.hasClass("button")
        if $el.prevAll(":not(.disable)").length > 0
          TWC.MVC.Router.setFocus $el.prevAll(":not(.disable):first")
        else
          @moveToMenu()
      else if $el.hasClass("episode-thumbnail")
        currentPos = parseInt($el.attr("id").split("-")[1])
        return TWC.MVC.Router.setFocus $(".buttonContainer .button:not(.disable):first") if currentPos is 0
        TWC.MVC.Router.setFocus $("#episode-#{currentPos-1}") if $("#episode-#{currentPos-1}").length > 0

        #move row up if 1st item
        if currentPos > 5
          if (currentPos - 3) % 3 is 0
            @focusThumbnailUp(currentPos - 2, false)

    focusRight: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("button")
        TWC.MVC.Router.setFocus $el.nextAll(":not(.disable):first")
      else if $el.hasClass("episode-thumbnail")
        currentPos = parseInt($el.attr("id").split("-")[1])
        TWC.MVC.Router.setFocus $("#episode-#{currentPos+1}") if $("#episode-#{currentPos+1}").length > 0
        #move row down if 3rd item
        if (currentPos + 1) % 3 is 0
          @focusThumbnailDown(currentPos - 2, false)

    focusBack: ->
      TWC.MVC.Router.setFocus $("#backButton")

    backLeft: ->
      @moveToMainMenu()

    backRight: ->
      @moveToContent()

    backDown: ->
      @moveToMenu()

    scrollOffset: 0

    focusThumbnailDown: (pos, focus = true)->
      currentRow = Math.floor(pos / 3)
      totalRows = Math.ceil(@model.get("BundleChildren").length / 3)
      return if currentRow is totalRows-1

      if currentRow - @scrollOffset is 1
        @scrollOffset++
        $("#product").removeClass().addClass("episodesOffset-#{@scrollOffset}")

      if focus
        TWC.MVC.Router.setFocus $("#episode-#{pos+3}") if $("#episode-#{pos+3}").length > 0


    focusThumbnailUp: (pos, focus = true)->
      currentRow = Math.floor(pos / 3)
      return if currentRow is 0


      if currentRow is @scrollOffset
        @scrollOffset--
        if @scrollOffset is 0
          $("#product").removeClass().addClass("fullscreen")
        else
          $("#product").removeClass().addClass("episodesOffset-#{@scrollOffset}")

      if focus
        TWC.MVC.Router.setFocus $("#episode-#{pos-3}") if $("#episode-#{pos-3}").length > 0


    enableButton: ($el) ->
      $el.removeClass("disable")

    showExtrasButton: ()->
      if @model.hasExtras()
        @enableButton($("#productExtrasButton"))


    showSubMenuContent: (el)->
      $el = $(el.target)

      if SubMenuHelper.getSubMenuContext() is SubMenuHelper.CONTEXT_STOREFRONT

        BackgroundHelper.enableBlur()
        $("#product").removeClass("overlay").removeClass("fullscreen")
        setTimeout ()=>
          # TWC.MVC.History.popState()
          TWC.MVC.Router.load "storefrontpage-view",
            data: [$el.data("id")]
            callback: ->
              TWC.MVC.History.reset()
        , 300

      else if SubMenuHelper.getSubMenuContext() is SubMenuHelper.CONTEXT_LOCKER

        TWC.MVC.Router.load "locker-view",
          data: [$el.data("id")]

      else if SubMenuHelper.getSubMenuContext() is SubMenuHelper.CONTEXT_SEARCH

        TWC.MVC.Router.load "video-search-view",
          data: ["", [], $el.data("id")]

      else if SubMenuHelper.getSubMenuContext() is SubMenuHelper.CONTEXT_FAVORITES

        TWC.MVC.Router.load "favorites-view",
          data: [$el.data("id")]

    toggleFullscreen: ->
      @fullscreen = not @fullscreen
      if @fullscreen
        $("#productMoreInfoButton .highlight").text(Manifest.less_button)
        $("#product").removeClass("overlay").addClass("fullscreen")
      else
        $("#productMoreInfoButton .highlight").text(Manifest.more_button)
        $("#product").removeClass("fullscreen").addClass("overlay")

    openExtraMedia: ->
      $el = $(TWC.MVC.Router.getFocus())
      pos = parseInt($el.attr("id").split("-")[1])

      @updateHistory(@model.get("Id"), $("#product").attr("class"), TWC.MVC.Router.getFocus())
      if($el.hasClass("video"))
        videoModel = @model.get("RelatedVideos")[pos]
        TWC.MVC.Router.load "videoplayer-view",
          data: [videoModel, false, true]
      else
        TWC.MVC.Router.load "imageviewer-view",
          data: [@model.get("RelatedImages"), pos]


    playVideo: ->
      TWC.MVC.Router.load 'videoplayer-view',
        data: [@model]


    playPreview: ->
      #add param true to tell the videoploayer that its a trailer
      TWC.MVC.Router.load 'videoplayer-view',
        data: [@model, true]

#    showExtras: ->
#      TWC.MVC.Router.load 'extras-view',
#        data: [@model]


    returnToLoginScreen: (type) ->
      loginScreenDetails = {}
      loginScreenDetails.productId = @prodId
      loginScreenDetails.returnValue = true
      loginScreenDetails.purchaseType = type
      loginScreenDetails.productType  = "product"

    upgradeProduct: (e)->
      @purchaseProduct(e, true)

    purchaseProduct: (e, upgrade = false, purchaseCallback=null, purchaseFailCallback = null, pricingPlan = null)->
      @ot.trackOutboundClickToBuy("ultraapp.com/confirmpurchase", "4kultra_#{@formattedTitle}_button")
      if Authentication.isLoggedIn()
        $(TWC.MVC.View.eventbus).trigger("loading", [true])
        purchaseProduct = ()=>
          currency = RegionSettings.default_payment_instruments_currency
          CDPaymentManagement.retrieveWallet currency, (error, data)=>
            CDPaymentManagement.updatePaypalData data.PaymentInstruments, (paymentInstruments)=>
              data.PaymentInstruments = paymentInstruments
              if not error
                paymentInstruments = _.filter(data.PaymentInstruments, (paymentInstrument) -> paymentInstrument.Type in Config.supportedPaymentOptions )
                if paymentInstruments.length is 0
                  TWC.MVC.Router.load "pin-view",
                    data: [
                      "", "", "paymentError", ()->
                        TWC.MVC.History.reset(2)
                        TWC.MVC.History.popState()
                    ]
                else
                  TWC.MVC.Router.load "paymentconfirmation-view",
                    data: [@model, @buyUHDPricingPlan, paymentInstruments, "", upgrade, null, purchaseCallback, purchaseFailCallback]


        if Config.enablePaymentPin
          CDSubscriberManagement.retrieveDevicePinStatus (error, hasPin) =>
            if hasPin
              TWC.MVC.Router.load "pin-view",
                data: [
                  @model.get("Name"), "", "purchasePin", purchaseProduct, purchaseFailCallback
                ]
            else
              purchaseProduct()
        else
          purchaseProduct()

      else
        historySize = TWC.MVC.History.size()
        loginConfirmation = new LoginConfirmation()
        loginConfirmation.load
          data: [false
            , ()=>
                TWC.MVC.Router.load "login-view",
                  data: [
                    ()=>
                      @model.refresh =>
                        entitledPricingPlan = @model.get("PricingPlans").findWhere({ Id : @model.get("EntitledPricingPlanId")})
                        if entitledPricingPlan?
                          messageAlert = new Alert();
                          messageAlert.load
                            data: ["", Manifest.payment_already_purchased, Manifest.ok, ()=>
                              return purchaseCallback() if purchaseCallback?
                              TWC.MVC.History.reset(historySize)
                              TWC.MVC.History.popState()
                            ]
                        else
                          @purchaseProduct(e, upgrade, purchaseCallback, purchaseFailCallback)
                  , ()=>
                      purchaseFailCallback?()
                  ]
            , false
            , ()=>
                purchaseFailCallback?()
          ]

    moveToMenu: (focusOnLast=false) ->
      if focusOnLast
        TWC.MVC.Router.setFocus $("#subMenuItems .menuButton:last")
      else
        if $("#submenu_0").length
          TWC.MVC.Router.setFocus "#submenu_0"
        else
          @moveToMainMenu()

    moveToMainMenu: ->
      TWC.MVC.Router.setActive "menu-zone"
      TWC.MVC.Router.setFocus "#browse"

    moveToContent: ->
      if $(".buttonContainer .button:not(.disable):first").length
        TWC.MVC.Router.setFocus $(".buttonContainer .button:not(.disable):first")
      else if $("#favoritesButton:not(.disable)") and $("#noTrailerAvailable").hasClass("disable")
        TWC.MVC.Router.setFocus $("#favoritesButton")
      else if $("#pointerNaviUp").is(":visible")
        TWC.MVC.Router.setFocus("#pointerNaviUp")
      else if $("#pointerNaviDown").is(":visible")
        TWC.MVC.Router.setFocus("#pointerNaviDown")
      else if @recommendedProductsAvailable
        TWC.MVC.Router.setFocus("recommendedProduct_0")

    refresh: ->
      authenticated = Authentication.isLoggedIn()
      @model = new CDBundleModel({Id:@prodId, manifest: Manifest, SubMenuTitle: SubMenuHelper.getSubMenuTitle(), SubMenuOptionalVisible: SubMenuHelper.getSubMenuOptionalVisible(), SubMenuItems: SubMenuHelper.getContextMenu(), authenticated: authenticated})
      super

    scrollUp: ->
      $(".productWindow").animate({scrollTop : $(".productWindow").scrollTop() - 500})

    scrollDown: ->
      $(".productWindow").animate({scrollTop: $(".productWindow").scrollTop() + 500})


  bundleDetailView.get()
