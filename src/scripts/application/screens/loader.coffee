# ## module: *loading*

define [
  "framework",
  "jquery"
], (TWC, $)->

  # **loading** (class) 
  class Loader extends TWC.SDK.Base
   
    # **constructor** (function)
    constructor: ->
      log "LOADING: init"

      # Init vars
      @timer = null
      @forceShow = false

      #subscribe to loading publisher
      force = null
      $(TWC.MVC.View.eventbus).on "loading", (e, bool, force) =>
        # use the force for video player screen when you want to see loader until the video starts playing
        switch force
          when "keep"
            @forceShow = true
          when "reset"
            @forceShow = false

        if bool
          @set() unless @timer
        else
          @unset()

    # **set** (function)   
    # enable loader 
    set: ->
      #show container div
      $("#loader").show()

      #block RCU buttons
      TWC.SDK.Input.get().setBlockAll(true)

    # **unset** (function)   
    # disable loader 
    unset: ->
      #can not hide loader if it is forced
      return false if @forceShow
      
      #enable controls
      TWC.SDK.Input.get().setBlockAll(false)
      
      #hide loader
      $("#loader").hide()
      log "LOADER: hide"
      #clear animation timer
      return if not @timer
      clearInterval(@timer)
      @timer = null
        
  Loader.get()