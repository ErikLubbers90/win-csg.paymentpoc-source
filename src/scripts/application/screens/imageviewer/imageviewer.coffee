define [
  "framework",
  "config",
  "jquery",
  "moment",
  "application/models/baseModel",
  "i18n!../../../lang/nls/manifest",
], (TWC, Config, $, Moment, baseModel, Manifest) ->


  class imageViewerView extends TWC.MVC.View
    uid: 'imageviewer-view'

    el: "#screen"

    type: "page"

    currentIdx: 0

    defaultFocus: "#buttonRight"

    renderedImages: []

    scrollOffset: 0

    isHidden: false
    timeoutId: null

    external:
      html:
        url:"skin/templates/screens/imageviewer/imageviewer.tpl"

    events:
      "enter #buttonLeft": "showPrevImage"
      "enter #buttonRight": "showNextImage"
      "enter #screenCloseButton": "close"

    onload: (@images, @currentIdx = 0) ->
      @model = new baseModel()
      @model.set("Images", @images)
      @model.set("CurrentIdx", @currentIdx)
      @model.set("manifest", Manifest)
      @totalCnt = @images.length

    onRenderComplete: ->
      $("#mainMenu").hide()
      @renderedImages.push(@currentIdx)
      if @currentIdx is 0
        $("#buttonRight").removeClass("disable")
        @renderImage([1, 2])
      else if @currentIdx is @totalCnt-1
        $("#buttonLeft").removeClass("disable")
        @renderImage([@currentIdx-2, @currentIdx-1])
      else
        $("#buttonLeft").removeClass("disable")
        $("#buttonRight").removeClass("disable")
        @renderImage([@currentIdx-1, @currentIdx+1])

      if @currentIdx > 4
        @scrollOffset = @currentIdx - 4
        $container = $("#thumbnails")
        scrollWidth = $(".leanback-image").outerWidth(true)
        $container.css({
          "-webkit-transform": "translate3d(" + (@scrollOffset*-scrollWidth) + "px, 0, 0)",
          "-o-transform": "translate(" + (@scrollOffset*-scrollWidth) + "px, 0)"
        });


      TWC.MVC.Router.setFocus "#leanbackImage-#{@currentIdx}"
      @autoHide()

    #preload images
    renderImage: (indexes)->
      #append to beginning of the list or the end

      for idx in indexes
        do (idx)=>
          return if idx >= @images.length
          $img = $("<img>").attr('src', @images[idx].MediaUrl)
          $img.one "load", =>
            newImg = $("<img/>")
            newImg.attr("src", @images[idx].MediaUrl)
            if @renderedImages.length is 0 or @renderedImages[0] < idx
              $("#slider-items ul").append( $("<li id='image-#{idx}' class='slider-image'/>").append(newImg)  )
            else
              $("#slider-items ul").prepend( $("<li id='image-#{idx}' class='slider-image'/>").append(newImg)  )
            @renderedImages.push(idx)


    showNextImage: ->
      return if @currentIdx is @totalCnt-1
      @autoHide()

      #scroll content if applicable
      if @currentIdx - @scrollOffset is 4
        $container = $("#thumbnails")
        scrollWidth = $(".leanback-image").outerWidth(true)
        @scrollOffset++
        $container.css({
          "-o-transform": "translate(" + (@scrollOffset*-scrollWidth) + "px, 0)",
          "-webkit-transform": "translate3d(" + (@scrollOffset*-scrollWidth) + "px, 0, 0)"});

      @currentIdx++

      $(".slider-image.active").removeClass("active")
      $("#image-#{@currentIdx}").addClass("active")

      #preload next image
      nextIdx = @currentIdx + 1

      if nextIdx isnt @totalCnt and nextIdx not in @renderedImages
        @renderImage([nextIdx])

      #updateArrows
      if @currentIdx is @totalCnt-1
        $("#buttonRight").addClass("disable")

      $("#buttonLeft").removeClass("disable") if $("#buttonLeft").hasClass("disable")

      TWC.MVC.Router.setFocus "#leanbackImage-#{@currentIdx}"



    showPrevImage: ->
      @autoHide()
      return if @currentIdx is 0

      #scroll content if applicable
      if @currentIdx is @scrollOffset
        $container = $("#thumbnails")
        scrollWidth = $(".leanback-image").outerWidth(true)
        @scrollOffset--
        $container.css({
          "-o-transform": "translate(" + (@scrollOffset*-scrollWidth) + "px, 0)",
          "-webkit-transform": "translate3d(" + (@scrollOffset*-scrollWidth) + "px, 0, 0)"});

      @currentIdx--

      $(".slider-image.active").removeClass("active")
      $("#image-#{@currentIdx}").addClass("active")

      #preload prev image
      prevIdx = @currentIdx - 1
      if prevIdx >= 0 and prevIdx not in @renderedImages
        @renderImage([prevIdx])

      if @currentIdx is 0
        $("#buttonLeft").addClass("disable")

      $("#buttonRight").removeClass("disable") if $("#buttonRight").hasClass("disable")
      TWC.MVC.Router.setFocus "#leanbackImage-#{@currentIdx}"

    hide: ->
      $("#buttonLeft").css({
        "-o-transform":"translate(-200px, 0px)",
        "-webkit-transform":"translate3d(-200px, 0px, 0)"})
      $("#buttonRight").css({
        "-o-transform":"translate(200px, 0px)",
        "-webkit-transform":"translate3d(200px, 0px, 0)"})
      $("#thumbnails").css({
        "-o-transform":"translate(0px, 300px)"
        "-webkit-transform":"translate3d(0px, 300px, 0)"})
      @isHidden = true

    show: ->
      clearTimeout(@timeoutId) if @timeoutId
      if @isHidden
        $("#buttonLeft").css({
          "-o-transform":"translate3d(0px, 0px)",
          "-webkit-transform":"translate3d(0px, 0px, 0)"})
        $("#buttonRight").css({
          "-o-transform":"translate(0px, 0px)",
          "-webkit-transform":"translate3d(0px, 0px, 0)"})
        $("#thumbnails").css({
          "-o-transform":"translate(0px, 0px)",
          "-webkit-transform":"translate3d(0px, 0px, 0)"})
        @isHidden = false

    autoHide: ->
      @show()
      if not @isHidden
        # after timeout hide controls
        clearTimeout(@timeoutId) if @timeoutId
        @timeoutId = setTimeout( (=>@hide()), 5000)

    #focus events
    focusLeft: ->
      $el = $(TWC.MVC.Router.getFocus())
      TWC.MVC.Router.setFocus($el.prevAll().not(".disable").first()) if $el.prevAll().not(".disable").length

    focusRight: ->
      $el = $(TWC.MVC.Router.getFocus())
      TWC.MVC.Router.setFocus($el.nextAll().not(".disable").first()) if $el.nextAll().not(".disable").length

    close: ->
      TWC.MVC.History.popState()

  imageViewerView.get()

