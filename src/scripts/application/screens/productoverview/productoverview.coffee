define [
  "framework",
  "jquery",
  "config",
  "application/api/authentication",
  "application/api/cd/collections/CDProductCollection",
  "application/models/productOverviewModel",
  "i18n!../../../lang/nls/manifest"
], (TWC, $, Config, Authentication, CDProductCollection, ProductOverviewModel, Manifest) ->

  class productOverviewView extends TWC.MVC.View
    uid: 'productoverview-view'

    type: 'page'

    # Max visible products displayed
    totalProductsPerPage: 18

    totalPages: 0

    # Currentpage default set to 1, not indexbased (!)
    currentPage: 1

    totalProducts: 0

    renderedPages: [1,2]

    # last index of first row
    lastIndexOfProductRow: 7

    contentLeft: 0

    # If the page contain a filter
    hasFilter: false

    external:
      html:
        url:"skin/templates/screens/productoverview/productoverview.tpl"

    focusUp: ->
      $el = $(TWC.MVC.Router.getFocus())

      return if !$el

      # Current focus on product
      if @focusIsOnProduct($el)
        focusedProductIndex = parseInt($el.attr("id").split("-")[1])
        # Focus on next playlist row if current rowindex less than 4
        if focusedProductIndex % @totalProductsPerPage >= 6
          if $("#playlist_product-#{focusedProductIndex-6}").length
            TWC.MVC.Router.setFocus $("#playlist_product-#{focusedProductIndex-6}")


    focusDown: ->
      $el = $(TWC.MVC.Router.getFocus())

      return if !$el

      # Current focus on product
      if @focusIsOnProduct($el)
        focusedProductIndex = parseInt($el.attr("id").split("-")[1])
        # Focus on next playlist row if current rowindex less than 4
        if focusedProductIndex % @totalProductsPerPage < 12
          if $("#playlist_product-#{focusedProductIndex+6}").length
            TWC.MVC.Router.setFocus $("#playlist_product-#{focusedProductIndex+6}")


    focusLeft: (callback=null)->
      $el = $(TWC.MVC.Router.getFocus())
      return if !$el

      # Current focus on product
      if @focusIsOnProduct($el)
        focusedProductIndex = parseInt($el.attr("id").split("-")[1])

        # Focus on index of next playlist row if exists
        rowCnt = 1
        if focusedProductIndex % @totalProductsPerPage in [0, 6, 12] and @currentPage > 1
          return if (@currentPage-1) not in @renderedPages
          TWC.SDK.Input.get().setBlockAll(true)
          scrollWidth = $("#playlist_page-0").outerWidth(true)
          @contentLeft += scrollWidth
          if @contentLeft > 0 then @contentLeft = 0
          $(".playlistContent").css({
            "-o-transform": "translate(" + @contentLeft + "px, 0)"
            "-webkit-transform": "translate3d(" + @contentLeft + "px, 0, 0)"
          })
          if $("#playlist_product-#{focusedProductIndex-13}").length
            $nextFocus = $("#playlist_product-#{focusedProductIndex-13}")
          else
            $nextFocus = $("#playlist_page-#{@currentPage-1} .product:last-child")
          @currentPage--
          @renderPage($el, "left")
          @displayPageNavigation(@currentPage, @totalPages)
          TWC.MVC.Router.setFocus($nextFocus)
          setTimeout ()=>
            TWC.SDK.Input.get().setBlockAll(false)
            callback?()
          , 600

        else
          if focusedProductIndex % @totalProductsPerPage in [0, 6, 12] and @currentPage is 1
            @focusMenu()
          else
            if $("#playlist_product-#{focusedProductIndex-rowCnt}").length
              $nextFocus = $("#playlist_product-#{focusedProductIndex-rowCnt}")
              TWC.MVC.Router.setFocus($nextFocus)
            else
              @focusMenu()

    focusRight: (callback=null)->
      $el = $(TWC.MVC.Router.getFocus())
      return if !$el

      # Current focus on product
      if @focusIsOnProduct($el)
        focusedProductIndex = parseInt($el.attr("id").split("-")[1])
        #calculate the next focusable element, if on featured block, then skip 2, otherwise 3
        rowCnt = 1
        if focusedProductIndex % @totalProductsPerPage in [5, 11, 17] and @currentPage < @totalPages
          return if (@currentPage+1) not in @renderedPages
          TWC.SDK.Input.get().setBlockAll(true)
          scrollWidth = $("#playlist_page-0").outerWidth(true)
          if @contentLeft is 0 then scrollWidth = scrollWidth - Math.round((Config.resolution[0] / 1920) * 77)
          @contentLeft -= scrollWidth
          $(".playlistContent").css({
            "-o-transform": "translate(" + @contentLeft + "px, 0)"
            "-webkit-transform": "translate3d(" + @contentLeft + "px, 0, 0)"
          })
          if $("#playlist_product-#{focusedProductIndex+13}").length
            $nextFocus = $("#playlist_product-#{focusedProductIndex+13}")
          else
            $nextFocus = $("#playlist_page-#{@currentPage} .product:first-child")
          @currentPage++
          @renderPage($el, "right")
          @displayPageNavigation(@currentPage, @totalPages)
          TWC.MVC.Router.setFocus($nextFocus)
          setTimeout ()=>
            TWC.SDK.Input.get().setBlockAll(false)
            callback?()
          , 600
        else
          if $("#playlist_product-#{focusedProductIndex+rowCnt}").length
            $nextFocus = $("#playlist_product-#{focusedProductIndex+rowCnt}")
            TWC.MVC.Router.setFocus($nextFocus)

    # Action triggered by click/touch
    prevPageNavAction: ->
      # Set focus index manually to first product on the current page
      TWC.MVC.Router.setFocus $("#playlist_page-#{@currentPage-1} .product:eq(0)")

      @focusLeft(->
        TWC.MVC.Router.unFocus()
      )

    # Action triggered by click/touch
    nextPageNavAction: ->
      if @model.get("featuredItemsEnabled") and @currentPage is 0
        # Set focus manually to last featured item
        TWC.MVC.Router.setFocus $(".playlistContent .playlist-col.featured:last .featured.product:first")

      else
        # Set focus manually to last product on the current page
        TWC.MVC.Router.setFocus $("#playlist_page-#{@currentPage - 1} .product:eq(5)")

      @focusRight(->
        TWC.MVC.Router.unFocus()
      )



    # Show/hide page navigation
    displayPageNavigation: (currentPage=0, totalPages=1)->
      if currentPage is 1
        $("#navigationLeft").hide();
      else
        $("#navigationLeft").show();

      if totalPages > 1
        $("#navigationRight").show();

      if currentPage is totalPages
        $("#navigationRight").hide();

    # Transform product collection to HTML
    getPageContent: (productCollection, page, fillDiv = false) ->
      pageHtml = []
      pCollection = productCollection.slice(0, Math.min(productCollection.length, @totalProductsPerPage))
      i = (page-1) * @totalProductsPerPage
      currentPage = Math.floor(i / @totalProductsPerPage)
      _.each pCollection, (product) =>

        if product.get('Name').length > 20 then name = "<marquee scrolldelay=125>#{product.get('Name')}</marquee>" else name = product.get('Name')

        pageHtml.push("<div id=\"playlist_page-" + currentPage++ + "\" class=\"playlist-page\">") if i % @totalProductsPerPage == 0 and not fillDiv
        pageHtml.push(
          "<div id=\"playlist_product-#{i}\" class=\"product content-sound\" data-left=\"focusLeft\" data-right=\"focusRight\" data-up=\"focusUp\" data-down=\"focusDown\" data-id=\"#{product.get("Id")}\" data-type=\"#{product.get("StructureType")}\">
            <span class=\"productTitle\">#{name}</span>
            <p class=\"remainingWatchTimeLeft\">#{product.get('LicenseExpirationTimeString')}</p>
            <div class=\"productBorder\"></div>
            <img src=\"#{product.get("ThumbnailUrl")}\" alt=\"#{product.get("Name")}\" />
            </div>")
        pageHtml.push("</div>") if i % @totalProductsPerPage is @totalProductsPerPage-1 and not fillDiv
        i++
      pageHtml.push("</div>") if i % @totalProductsPerPage isnt @totalProductsPerPage-1 and not fillDiv
      pageHtml.join("")

#    renderPage: ($el, dir) ->
#      updatedProductsCollection = new CDProductCollection(@productCollection.slice(@currentPage*@totalProductsPerPage-@totalProductsPerPage, @currentPage*@totalProductsPerPage))
#
#      # Update model
#      @model.set
#        Products: updatedProductsCollection
#
#      # Generate html new content
#      pageContent = @getPageContent(updatedProductsCollection.models, @currentPage)
#
#      # Set html content
#      if dir is "right"
#        if @currentPage not in @renderedPages
#          @renderedPages.push(@currentPage)
#          $(".playlistContent").append(pageContent)


      # Animate product container
#      scrollWidth = $(".playlistContent .product:first").width() + $(".playlistContent .playlist_col:first").css("margin-right")
#      scrollWidth = $nextFocus.width() + parseInt($el.parent().css("margin-left"))
#
#
#      scrollPosHor = if dir is "right" then scrollWidth*-6 else scrollWidth*6
#      @contentLeft += scrollPosHor
#      $(".playlistContent").css({
#        "-o-transform":"translate("+@contentLeft+"px, 0px)"
#        "-webkit-transform":"translate3d("+@contentLeft+"px, 0px, 0)"
#      })



    showProductDetail: ->
      $el = $("#{TWC.MVC.Router.getFocus()}")
      productId = $el.data("id")
      productType = $el.data("type")

      switch productType
        when 1
          # Product/episode
          page = 'productDetail-view'
        when 2, 4
          # Bundle/season
          page = 'bundleDetail-view'
        else
          page = 'productDetail-view'
      @selectedItemId = TWC.MVC.Router.getFocus()
      @updateHistory(@subMenuSelectedId, @currentPage, @contentLeft, @selectedItemId)
      TWC.MVC.Router.load page,
        data: [productId]

    buttonIsDisabled: ($el) ->
      $el.hasClass "disable"

    focusIsOnPlaylistButtons: ($focusedEl) ->
      $focusedEl.parent().hasClass "playlistButtons"

    focusIsOnProduct: ($focusedEl) ->
      $focusedEl.hasClass "product"

    focusIsOnSearchHistoryItem: ($el) ->
      $el.hasClass "searchListItem"

    focusIsOnClearHistoryButton: ($el) ->
      $el.hasClass "clearHistory"

    isLastPage: ->
      @currentPage is @totalPages

    isFirstPage: ->
      @currentPage is 1


    showSubMenuContent: (el)->
      $el = $(el.target)
      TWC.MVC.Router.load "storefrontpage-view",
        data: [$el.data("id")]

    focusMenu: ->
      if $(".menuButton.submenu.select").length
        TWC.MVC.Router.setFocus ".menuButton.submenu.select"
      else if $("#submenu_0")
        TWC.MVC.Router.setFocus "#submenu_0"
      else
        @moveToMainMenu()

    moveToMainMenu: ->
      TWC.MVC.Router.setActive "menu-zone"
      TWC.MVC.Router.setFocus '#' + $(".menuButton.select").attr("id")

    moveToContent: ->
      TWC.MVC.Router.setFocus ".playlistContent .product:not('.disable'):first"
