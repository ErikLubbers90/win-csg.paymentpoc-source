define [
  "framework",
  "config",
  "jquery",
  "backbone",
  "i18n!../../../lang/nls/manifest",
  "application/helpers/submenuHelper",
  "application/helpers/backgroundHelper",
  "application/api/cd/collections/CDProductCollection",
  "application/api/cd/collections/CDWishlistCollection",
  "application/api/cd/models/CDProduct",
  "application/models/baseModel",
  "application/models/productOverviewModel",
  "application/screens/productoverview/productoverview",
  "../../models/analytics/omnituretracker",
  "application/screens/navhelp"
], (TWC, Config, $, Backbone, Manifest, SubMenuHelper, BackgroundHelper, CDProductCollection, CDWishlistCollection, CDProductModel, BaseModel, ProductOverviewModel, ProductOverview, OmnitureTracker, NavHelp) ->

  class favoritesModel extends BaseModel
    relations:
      Products: CDProductCollection

    defaults:
      "Name": Manifest.topmenu_favourite
      "Products": []
      "TotalProductsPerPage": 18
      "TotalProducts": 0
      "manifest": Manifest
      "subMenuItems": []
      "productCollection": []

    parse:(resp,options) ->
      data =
        Products: resp
        TotalProducts: CDWishlistCollection.length
      return data


    fetch: (options) ->
      CDWishlistCollection.refresh =>
        products = []
        products =
        model   = this
        success = options.success
        options.success = (resp) ->
          parsedData = model.parse(resp, options)
          #to prevent to show multiple products, we need to reset the Products
          model.get("Products").reset() if model.get("Products")?
          if (!model.set(parsedData, options)) then return false
          if (success) then success(model, resp, options)
          model.trigger('sync', model, resp, options)

        currentPage = @.get("CurrentPage")
        if currentPage in [0,1]
          resp = CDWishlistCollection.slice(0, 36)
        else
          resp = CDWishlistCollection.slice(currentPage*@.get('totalProductsPerPage'), (currentPage+1)*@.get('totalProductsPerPage'))

#        @.set
#          Products: products
#          TotalProducts: CDWishlistCollection.length
        options.success?(resp)


  class favoritesView extends ProductOverview
    uid: 'favorites-view'

    el: "#screen"

    type: "page"

    # Max visible products displayed
    totalProductsPerPage: 18

    availablePages: [1, 2]

    favoritesSubmenu: [
      {Name: Manifest.recently_added, Id: 1, Optional: false, ReverseEnabled: true, NormalName: Manifest.recently_added, ReverseName: Manifest.previously_added, Filters: {SortBy:1, SortDirection: 2}},
      {Name: Manifest.a_to_z, Id: 2, Optional: false, ReverseEnabled: true, NormalName: Manifest.a_to_z, ReverseName: Manifest.z_to_a, Filters: {SortBy:4, SortDirection:1}},
    ]

    external:
      html:
        url:"skin/templates/screens/productoverview/productoverview.tpl"

    events:
#      "enter    #productOverview .playlistButtons .button.pageLeft"     : "showPrevPage"
#      "enter    #productOverview .playlistButtons .button.pageRight"    : "showNextPage"
      "enter    .menuButton.submenu"                                    : "showCategory"
      "enter    .product"                                               : "showProductDetail"
      "enter    #navigationLeft"                                        : "prevPageNavAction"
      "enter    #navigationRight"                                       : "nextPageNavAction"
      "swipeLeft .playlistContent"                                      : "prevPageNavAction"
      "swipeRight .playlistContent"                                     : "nextPageNavAction"

    zones: [
      {uid:"menu-zone", data:[]}
    ]

    onload: (@subMenuSelectedId = null, @currentPage=1, @contentLeft=0, @selectedItemId=null) ->
      # Create new model
      @model = new favoritesModel()
      SubMenuHelper.setFavoritesPages(@favoritesSubmenu)

      @model.set
        CurrentPage: if @currentPage is 2 then @currentPage - 2  else @currentPage-1
        totalProductsPerPage: @totalProductsPerPage

      @productCollection = new CDProductCollection()

      # If submenu id is available, set sorting options
      if @subMenuSelectedId?
        favSubmenuArray = (favMenuItem for favMenuItem in @favoritesSubmenu when favMenuItem.Id is @subMenuSelectedId)
        if favSubmenuArray?.length
          CDWishlistCollection.sortBy = favSubmenuArray[0].Filters.SortBy
          CDWishlistCollection.sortDirection = favSubmenuArray[0].Filters.SortDirection
      else
        @subMenuSelectedId = if @favoritesSubmenu.length > 0 then @favoritesSubmenu[0].Id


    template:(html, data) ->
      return _.template(html)({data:data, SubMenuOptionalVisible:false, SubMenuTitle: "Favorites", SubMenuItems: SubMenuHelper.getFavoritesPages(), manifest:Manifest})

    onRenderComplete: ->
      BackgroundHelper.setBackgroundImage("./skin/#{Config.resolution[1]}/media/theme/placeholder.jpg", "./skin/#{Config.resolution[1]}/media/theme/placeholder.jpg", false, 0, true)
      $("#mainMenu").show()

      productCollection = @model.get("Products")
      @productCollection.add(productCollection.models) #add models for page 1 and 2

      @totalProducts = @model.get("TotalProducts")
      @lastIndexOfProductRow = @model.get("TotalProductsPerPage")/2 - 1
      @totalPages = Math.ceil @totalProducts/@model.get("TotalProductsPerPage")

      if @currentPage in [1,2]
        @availablePages = [1, 2]  #initially called 1 page of double the page size so we have info on page 1 and 2
        @renderedPages = [1,2]
      else
        @availablePages = [@currentPage]
        @renderedPages = [@currentPage]


      $playlistContent = $(".playlistContent")
      $playlistContent.addClass("noAnimation")
      $playlistContent.css({
        "-o-transform": "translate(" + @contentLeft + "px, 0)"
        "-webkit-transform": "translate3d(" + @contentLeft + "px, 0, 0)"
      })

      SubMenuHelper.setSelectedMenuItem("favorites")
      SubMenuHelper.setSelectedFavoritesPage(@subMenuSelectedId)
      SubMenuHelper.setSubMenuContext(SubMenuHelper.CONTEXT_FAVORITES)


      setTimeout ->
        $(".playlistContent").removeClass("noAnimation")
      , 1500

      $playlistContent.addClass("visibleProducts")

      @displayPageNavigation(@currentPage, @totalPages)


    onZoneRenderComplete: ->
      $subMenuEl = $("#subMenuItems .menuButton.submenu[data-id='#{@subMenuSelectedId}']")
      TWC.MVC.Router.setSelect $subMenuEl

      if @selectedItemId?
        @renderPage(null, "right")
        @renderPage(null, "left")
        TWC.MVC.Router.setFocus @selectedItemId
      else if $(".playlistContent .product:not(.disable):first").length > 0
        TWC.MVC.Router.setFocus $(".playlistContent .product:not(.disable):first")
      else
        TWC.MVC.Router.setFocus("#subMenuItems .submenu:first")

      pageName = $subMenuEl.text().toLowerCase().replace(/[^A-Za-z]/g, "")
      if pageName in ["recentlyadded", "az"]
        ot = OmnitureTracker.get()
        ot.trackPageView("favorite.#{pageName}.html")


    showCategory: ->
      $el = $(TWC.MVC.Router.getFocus())
      pos = parseInt($el.attr("id").split("_")[1])
      subMenuItems = SubMenuHelper.getFavoritesPages()
      selectedSubMenuItem = subMenuItems[pos]

      prevSelected = @subMenuSelectedId
      @subMenuSelectedId = if SubMenuHelper.getFavoritesPages().length > 0 then $el.data("id")

      $subMenuEl = $("#subMenuItems .menuButton.submenu[data-id='#{@subMenuSelectedId}']")
      TWC.MVC.Router.setSelect $subMenuEl
      if prevSelected is @subMenuSelectedId and @favoritesSubmenu[pos].ReverseEnabled
        @favoritesSubmenu[pos].Filters.SortDirection = 3 - @favoritesSubmenu[pos].Filters.SortDirection
        if @favoritesSubmenu[pos].Name is @favoritesSubmenu[pos].NormalName
          @favoritesSubmenu[pos].Name = @favoritesSubmenu[pos].ReverseName
        else
          @favoritesSubmenu[pos].Name = @favoritesSubmenu[pos].NormalName

      @currentPage = 1
      @contentLeft = 0
      @renderedPages = [1,2]


      $(TWC.MVC.View.eventbus).trigger('loading', [true])
      CDWishlistCollection.sortBy = @favoritesSubmenu[pos].Filters.SortBy
      CDWishlistCollection.sortDirection = @favoritesSubmenu[pos].Filters.SortDirection
      @refresh()

    renderPage: ($el, dir) ->
      #if user wants to next / previous page but this page is not available yet cancel the action
      if @currentPage not in @availablePages
        if dir is "right"
          @currentPage++
        else
          @currentPage--

        #animate to page
      else
        if dir is "right"
          #pre-load next page instead of current page
          nextPage = @currentPage + 1


          if (nextPage) <= @totalPages and (nextPage) not in @availablePages
            resp = CDWishlistCollection.slice((nextPage-1)*@totalProductsPerPage, nextPage*@totalProductsPerPage)
            result = @productCollection.add(resp)
            @availablePages.push(nextPage)

            # Update model
            if nextPage not in @renderedPages
              @renderedPages.push(nextPage)

              # Generate html new content
              pageContent = @getPageContent(resp, nextPage)
              # Set html content
              $(".playlistContent").append(pageContent)
        else
          #pre-load prev page
          prevPage = @currentPage - 1

          if prevPage >= 1 and prevPage not in @availablePages

            resp = CDWishlistCollection.slice((prevPage-1)*@totalProductsPerPage, prevPage*@totalProductsPerPage)
            result = @productCollection.unshift(resp)
            @availablePages.push(prevPage)

            # Update model
            if prevPage not in @renderedPages
              @renderedPages.push(prevPage)

              # Generate html new content
              pageContent = @getPageContent(resp, prevPage, true)

              # Set html content
              $(".playlistContent #playlist_page-#{prevPage-1}").append(pageContent)


  favoritesView.get()
