define [
  "framework",
  "config",
  "application/models/baseModel",
  "jquery",
  "i18n!../../../lang/nls/manifest",
  "application/popups/generic/alert",
  "application/api/cd/models/CDSearchLocker",
  "application/api/cd/models/CDFilters",
  "application/api/cd/collections/CDProductCollection",
  "application/screens/productoverview/productoverview",
  "application/helpers/submenuHelper",
  "application/helpers/backgroundHelper",
  "../../models/analytics/omnituretracker"
], (TWC, Config, BaseModel, $, Manifest, Alert, CDSearchLockerModel, CDFiltersModel, CDProductCollection,  ProductOverview, SubMenuHelper, BackgroundHelper, OmnitureTracker) ->

  class lockerView extends ProductOverview
    uid: 'locker-view'

    el: '#screen'

    type: 'page'

    external:
      html:
        url:"skin/templates/screens/productoverview/productoverview.tpl"

    zones: [
      {uid:"menu-zone", data: []}
    ]

    lockerSubmenu: []

    lockerItemCnt: 0


    events:
      "enter    .menuButton.submenu"                                    : "showCategory"
      "enter    .product"                                               : "showProductDetail"
      "enter    #navigationLeft"                                        : "prevPageNavAction"
      "enter    #navigationRight"                                       : "nextPageNavAction"
      "swipeLeft .playlistContent"                                      : "prevPageNavAction"
      "swipeRight .playlistContent"                                     : "nextPageNavAction"

    initLockerSubmenu: ()->
      return if @lockerSubmenu.length > 0
      # DeliveryCapabilityGroup 1 : My Rentals, DeliveryCapabilityGroup 3 : My Purchases
      rentalDeliveryCapabilityId = Config.csg[Config.environment].RentalDeliveryCapabilityId
      @lockerSubmenu = [
        {Name: "Recently Added", Id: 1, Optional: false, ReverseEnabled: true, NormalName: "Recently Added", ReverseName:"Previously Added", Filters: {SortBy:2, SortDirection: 2, LockerSource:4}},
        {Name: "Rental", Id: 18, Optional: false, Filters: {LockerSource:4, DeliveryCapabilityGroup:1, DeliveryCapability: rentalDeliveryCapabilityId}},
        {Name: "A-Z", Id: 2, Optional: false, ReverseEnabled: true, NormalName: "A-Z", ReverseName: "Z-A", Filters: {SortBy:1, SortDirection:1, Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia")}},
        {Name: "Action", Id: 3, Optional: true, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_Action")}},
        {Name: "Biography", Id: 4, Optional: true, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_Biography")}},
        {Name: "Comedy", Id: 5, Optional: true, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_Comedy")}},
        {Name: "Documentary", Id: 6, Optional: true, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_Documentary")}},
        {Name: "Drama", Id: 7, Optional: true, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_Drama")}},
        {Name: "Educational", Id: 8, Optional: true, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_Educational")}},
        {Name: "Family", Id: 9, Optional: true, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_Family")}},
        {Name: "Fantasy", Id: 10, Optional: true, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_Fantasy")}},
        {Name: "Foreign", Id: 11, Optional: true, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_Foreign")}},
        {Name: "History", Id: 12, Optional: true, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_History")}},
        {Name: "Horror", Id: 13, Optional: true, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_Horror")}},
        {Name: "Music", Id: 14, Optional: true, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_Music")}},
        {Name: "Sci-Fi", Id: 15, Optional: true, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_Sci-Fi")}},
        {Name: "Special Interest", Id: 16, Optional: true, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_Special_Interest")}},
        {Name: "TV", Id: 17, Optional: false, Filters: {Categories:CDFiltersModel.getIDForExternalReference("Sony_Bravia_TV")}}

      ]

    onload: (@subMenuSelectedId = null, @currentPage=1, @contentLeft=0, @selectedItemId=null, @setNewCategory = false) ->

      $(TWC.MVC.View.eventbus).trigger("loading", [true, "keep"])

      @initLockerSubmenu()

      @productCollection = new CDProductCollection()
      @availablePages = []

      if not @subMenuSelectedId
        @subMenuSelectedId = if @lockerSubmenu.length > 0 then @lockerSubmenu[0].Id

      selectedSubmenu = _.findWhere(@lockerSubmenu, {Id:@subMenuSelectedId})

      @sortBy = selectedSubmenu.Filters.SortBy if selectedSubmenu.Filters.SortBy?
      @sortDirection = selectedSubmenu.Filters.SortDirection if selectedSubmenu.Filters.SortDirection?
      @categories  = selectedSubmenu.Filters.Categories if selectedSubmenu.Filters.Categories?

      @deliveryCapability = if selectedSubmenu.Filters.DeliveryCapability? then selectedSubmenu.Filters.DeliveryCapability else null
      @deliveryCapabilityGroup = if selectedSubmenu.Filters.DeliveryCapabilityGroup? then selectedSubmenu.Filters.DeliveryCapabilityGroup else null

      #reset categories after user navigates away from locker and returns
      @categories = null if @selectedItemId is null and @setNewCategory is false

      lockerOptions = {
        keyword: "",
        pageNumber: if @currentPage in [1,2] then 1 else @currentPage,
        pageSize: if @currentPage in [1,2] then @totalProductsPerPage*2 else @totalProductsPerPage #request 2 page size for initial request to improve user experience
        categories:if @categories then @categories else null
        sortBy: if @sortBy? then @sortBy else null
        sortDirection: if @sortDirection? then @sortDirection else null
        deliveryCapability: if @deliveryCapability? then @deliveryCapability else null
        deliveryCapabilityGroup: if @deliveryCapabilityGroup? then @deliveryCapabilityGroup else null
      }

      @model = new CDSearchLockerModel({}, lockerOptions)

      SubMenuHelper.setLockerPages(@lockerSubmenu)

      @model.pageSizeCorrection = 0.5   #correct for double page size on initial request
      @model.set
        Name: Manifest.topmenu_library
        CurrentPage: if @currentPage is 2 then @currentPage - 2  else @currentPage-1
        totalProductsPerPage: @totalProductsPerPage


    onunload: ->
      @currentPage = 1

    template:(html, data) ->

      SubMenuHelper.setTotalProductsInLockerCollection(@model.get("TotalProducts"))

      if SubMenuHelper.getTotalProductsInLockerCollection() > 30
        optionalSubmenuVisible = true
        SubMenuHelper.setSubMenuOptionalVisible(true)
      else
        optionalSubmenuVisible = false

      return _.template(html)({data:data, SubMenuTitle: "Library", SubMenuOptionalVisible: optionalSubmenuVisible, SubMenuItems: SubMenuHelper.getLockerPages(), manifest:Manifest})

    onRenderComplete: ->
      if not @model.categories? or @model.categories.length is 0
        @lockerItemCnt = @model.get("TotalProducts")
      BackgroundHelper.setBackgroundImage("./skin/#{Config.resolution[1]}/media/theme/placeholder.jpg", "./skin/#{Config.resolution[1]}/media/theme/placeholder.jpg", false, 0, true)
      $("#mainMenu").show()

      @totalPages = Math.ceil(@model.get("TotalProducts") / @totalProductsPerPage)
      productCollection = @model.get("Products")
      @productCollection.add(productCollection.models) #add models for page 1 and 2
      if @currentPage in [1,2]
        @availablePages = [1, 2]  #initially called 1 page of double the page size so we have info on page 1 and 2
        @renderedPages = [1,2]
      else
        @availablePages = [@currentPage]
        @renderedPages = [@currentPage]
      # Disable non-visible products
      $(".playlistContent .product:eq(#{@totalProductsPerPage-1})").nextAll().addClass("disable")

      $playlistContent = $(".playlistContent")
      $playlistContent.addClass("noAnimation")
      $playlistContent.css({
        "-o-transform": "translate(" + @contentLeft + "px, 0)"
        "-webkit-transform": "translate3d(" + @contentLeft + "px, 0, 0)"
      })

      SubMenuHelper.setSelectedMenuItem("library")
      SubMenuHelper.setSelectedLockerPage(@subMenuSelectedId)
      SubMenuHelper.setSubMenuContext(SubMenuHelper.CONTEXT_LOCKER)


#      @productCollection.each (productModel) ->
#        console.log ">>>>", productModel
#        if productModel.get("PricingPlan")?
#          entitledPricingPlan = productModel.get("PricingPlans").findWhere({ Id : productModel.get("PricingPlan")})
#          console.log ">>>>", productModel.get("PricingPlans")


#      entitledPricingPlan = @model.get("PricingPlans").findWhere({ Id : @model.get("EntitledPricingPlanId")})
#      entitledQuality = if entitledPricingPlan? then entitledPricingPlan.getQuality() else null
#      productIsAvailable = if entitledPricingPlan? then entitledPricingPlan.isAvailable() else false


      setTimeout ->
        $(".playlistContent").removeClass("noAnimation")
      , 1500

      $playlistContent.addClass("visibleProducts")

      #handle empty locker
      if @productCollection.models.length is 0
        $("#playlist_pageRight").addClass "disable"
        $("#productOverview .playlistContentContainer").hide()

        $("#noResults").html(Manifest.no_recent_purchases)
        $("#noResults").show()
        $("#productOverview .pagingIndicator").hide()

      @displayPageNavigation(@currentPage, @totalPages)

    onZoneRenderComplete: ->
      $subMenuEl = $("#subMenuItems .menuButton.submenu[data-id='#{@subMenuSelectedId}']")
      TWC.MVC.Router.setSelect $subMenuEl

      #set focus to first item
      if @setNewCategory
        if $(".playlistContent .product:first").length
          TWC.MVC.Router.setFocus ".playlistContent .product:first"
        else
          TWC.MVC.Router.setFocus $("#subMenuItems .submenu:first")

      else if @selectedItemId? and !@setNewCategory
        #preload prev/next pages
        @renderPage(null, "left")
        @renderPage(null, "right")
        TWC.MVC.Router.setFocus @selectedItemId

      else if !@selectedItemId? and !@setNewCategory
        if $(".playlistContent .product:first").length
          TWC.MVC.Router.setFocus ".playlistContent .product:first"
        else
          TWC.MVC.Router.setFocus $("#subMenuItems .submenu:first")

      else if $("#subMenuItems .submenu").length
        # Focus on submenu
        if $("#subMenuItems .submenu.select").length
          TWC.MVC.Router.setFocus $("#subMenuItems .submenu.select")
        else
          TWC.MVC.Router.setFocus $("#subMenuItems .submenu:first")
      else
        # Focus on mainmenu
        TWC.MVC.Router.setFocus $("#topMenuItems .menuButton.select")

      $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])

      pageName = $subMenuEl.text().toLowerCase().replace(/[^A-Za-z]/g, "")
      if pageName in ["recentlyadded", "az", "tv"]
        ot = OmnitureTracker.get()
        ot.trackPageView("library.#{pageName}.html")


    showCategory: ->
      $el = $(TWC.MVC.Router.getFocus())
      pos = parseInt($el.attr("id").split("_")[1])
      subMenuItems = SubMenuHelper.getLockerPages()
      selectedSubMenuItem = subMenuItems[pos]

      prevSelected = @subMenuSelectedId
      @subMenuSelectedId = if SubMenuHelper.getLockerPages().length > 0 then $el.data("id")
      $subMenuEl = $("#subMenuItems .menuButton.submenu[data-id='#{@subMenuSelectedId}']")
      TWC.MVC.Router.setSelect $subMenuEl

      if @subMenuSelectedId then @setNewCategory = true

      #check if current submenu item is reversable
      if prevSelected is @subMenuSelectedId and @lockerSubmenu[pos].ReverseEnabled
        @lockerSubmenu[pos].Filters.SortDirection = 3 - @lockerSubmenu[pos].Filters.SortDirection
        if @lockerSubmenu[pos].Name is @lockerSubmenu[pos].NormalName
          @lockerSubmenu[pos].Name = @lockerSubmenu[pos].ReverseName
        else
          @lockerSubmenu[pos].Name = @lockerSubmenu[pos].NormalName

      @currentPage = 1
      @contentLeft = 0
      @renderedPages = [1,2]


      #load product list
      $(TWC.MVC.View.eventbus).trigger('loading', [true])

      lockerResultsModel = new CDSearchLockerModel({}, {
        pageNumber:@currentPage
        pageSize:@totalProductsPerPage*2
        categories: selectedSubMenuItem.Filters.Categories if selectedSubMenuItem.Filters.Categories?
        sortBy: selectedSubMenuItem.Filters.SortBy if selectedSubMenuItem.Filters.SortBy?
        sortDirection: selectedSubMenuItem.Filters.SortDirection if selectedSubMenuItem.Filters.SortDirection?
        deliveryCapabilityGroup: selectedSubMenuItem.Filters.DeliveryCapabilityGroup if selectedSubMenuItem.Filters.DeliveryCapabilityGroup?
        deliveryCapability: selectedSubMenuItem.Filters.DeliveryCapability if selectedSubMenuItem.Filters.DeliveryCapability?
      })


      lockerResultsModel.fetch
        success: (model, data, xhr)=>
          @model = model
          @model.pageSizeCorrection = 0.5   #correct for double page size on initial request
          @model.set
            Name: Manifest.topmenu_library
            CurrentPage: if @currentPage is 2 then @currentPage - 2  else @currentPage-1
            totalProductsPerPage: @totalProductsPerPage

          @render()


    renderPage: ($el, dir) ->
      #if user wants to next / previous page but this page is not available yet cancel the action
      if @currentPage not in @availablePages
        if dir is "right"
          @currentPage++
        else
          @currentPage--

        #animate to page
      else
        if dir is "right"

          #pre-load next page instead of current page
          nextPage = @currentPage + 1

          if (nextPage) <= @totalPages and (nextPage) not in @availablePages


            selectedSubMenuItem = _.findWhere(SubMenuHelper.getLockerPages(), {Id:@subMenuSelectedId})
            lockerResultsModel = new CDSearchLockerModel({}, {
              pageNumber:nextPage,
              pageSize:@totalProductsPerPage
              categories: selectedSubMenuItem.Filters.Categories if selectedSubMenuItem.Filters.Categories?
              sortBy: selectedSubMenuItem.Filters.SortBy if selectedSubMenuItem.Filters.SortBy?
              sortDirection: selectedSubMenuItem.Filters.SortDirection if selectedSubMenuItem.Filters.SortDirection?
            })

            lockerResultsModel.fetch
              success: (collection, data, xhr)=>
                productCollection = collection.get("Products")
                result = @productCollection.add(productCollection.models)
                @availablePages.push(nextPage)

                # Set html content
                pageContent = @getPageContent(productCollection.models, nextPage)
                $(".playlistContent").append(pageContent)
                @renderedPages.push(nextPage)


        else
          #pre-load next page instead of current page
          prevPage = @currentPage - 1

          if prevPage >= 1 and prevPage not in @availablePages

            selectedSubMenuItem = _.findWhere(SubMenuHelper.getLockerPages(), {Id:@subMenuSelectedId})

            lockerResultsModel = new CDSearchLockerModel({}, {
              pageNumber:prevPage,
              pageSize:@totalProductsPerPage
              categories: selectedSubMenuItem.Filters.Categories if selectedSubMenuItem.Filters.Categories?
              sortBy: selectedSubMenuItem.Filters.SortBy if selectedSubMenuItem.Filters.SortBy?
              sortDirection: selectedSubMenuItem.Filters.SortDirection if selectedSubMenuItem.Filters.SortDirection?
            })

            lockerResultsModel.fetch
              success: (collection, data, xhr)=>
                productCollection = collection.get("Products")
                result = @productCollection.unshift(productCollection.models)
                @availablePages.push(prevPage)

                # Set html content
                pageContent = @getPageContent(productCollection.models, prevPage, true)
                $(".playlistContent #playlist_page-#{prevPage-1}").append(pageContent)
                @renderedPages.push(prevPage) #?


  lockerView.get()
