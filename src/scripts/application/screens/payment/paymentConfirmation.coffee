define [
  "framework",
  "settingPages",
  "config",
  "jquery",
  "application/helpers/submenuHelper",
  "../../models/baseModel",
  "application/popups/error/error",
  "application/api/cd/models/CDOrderQuote",
  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDSubscriberManagement",
  "application/api/cd/CDPaymentManagement",
  "application/api/authentication",
  "../../models/analytics/omnituretracker",
  "i18n!../../../lang/nls/manifest"
], (TWC, ConfigSettings, Config, $, MenuHelper, BaseModel, Error, CDOrderQuote, CDBaseRequest, CDSubscriberManagement, CDPaymentManagement, Authentication, OmnitureTracker, Manifest) ->

  class paymentConfirmationView extends TWC.MVC.View
    uid: 'paymentconfirmation-view'

    el: "#screen"

    type: 'page'

    defaultFocus: "#purchaseButton"

    external:
      html:
        url:"skin/templates/screens/payment/paymentconfirmation.tpl"

    events:
      "enter #purchaseButton" : "submitOrderAction"
      "enter #cancelButton" : "cancel"
      "enter #screenCloseButton" : "cancel"
      "enter #changePayment": "openPaymentOptions"
      "enter #promoButton": "setPromoCode"
      "enter #removePromoButton": "removePromotion"

    onload: (@productModel, @pricingPlan, @paymentInstruments, @promotionCode = "", @upgradeProduct = false, @selectedPaymentInstrument = null, @extrasCallback = null, @purchaseFailCallback = null) ->
      for paymentInstrument in @paymentInstruments
        if paymentInstrument.TypeName is "Credit Card"
          paymentInstrument.CardType = paymentInstrument.Name.split("-")[0]
          paymentInstrument.LastPartOfID = paymentInstrument.Name.split("-")[1]

      if not @selectedPaymentInstrument
        @selectedPaymentInstrument = _.findWhere(@paymentInstruments, {Default:true})

      if not @selectedPaymentInstrument and @paymentInstruments.length > 0
        @selectedPaymentInstrument = @paymentInstruments[0]

      orderPaymentInstrumentIds = [@selectedPaymentInstrument.Id]
      @model = new CDOrderQuote({}, {
        pricingPlanId: @pricingPlan.get("Id"),
        productId: @productModel.get("Id"),
        paymentInstrumentIds: orderPaymentInstrumentIds,
        defaultPaymentInstrumentId: @selectedPaymentInstrument.Id,
        promotionCodes: if @promotionCode isnt "" then [@promotionCode] else []
        errorCallback: @onOrderError
      })

    template:(html, data) ->
      _.template(html)(
        data: data
        manifest: Manifest
        ProductTitle: @productModel.get("Name")
        ProductThumbnail: @productModel.get("ThumbnailUrl")
        PaymentInstruments: @paymentInstruments
        SelectedPaymentInstrument: @selectedPaymentInstrument
      )


    onRenderComplete: ->
      $("#mainMenu").hide()
      if @promotionCode isnt ""
        $("#removePromoButton").removeClass("disable")

      # Show rental period description
      if @pricingPlan.isRental()
        $("#thumbContainer .rentalDescription").removeClass("disable")

      @ot = OmnitureTracker.get()
      @formattedTitle = @productModel.get("Name").toLowerCase().replace(/[^A-Za-z0-9]/g, "")
      @ot.trackPageView("#{@formattedTitle}.confirmpurchase.html")



    focusLeft: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.attr("id") is "cancelButton"
        if @promotionCode is ""
          TWC.MVC.Router.setFocus "#promoButton"
        else
          TWC.MVC.Router.setFocus "#removePromoButton"


    focusRight: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.attr("id") is "promoButton"
        if @promotionCode is ""
          TWC.MVC.Router.setFocus "#cancelButton"
        else
          TWC.MVC.Router.setFocus "#removePromoButton"

    focusUp: ->
      if $("#changePayment").length > 0
        TWC.MVC.Router.setFocus "#changePayment"

    openPaymentOptions: ->
      @ot.trackOutboundClick('ultraapp.com/selectpayment', "#{@formattedTitle}_changepayment_button")

      $(TWC.MVC.View.eventbus).trigger("loading", true)

      TWC.MVC.Router.load "changepayment-popup",
        data: [@paymentInstruments, @selectedPaymentInstrument.Id]

    setPaymentInstrumentIds: (ids)->
      @selectedPaymentInstrument = _.findWhere(@paymentInstruments, {Id:ids.selectedId})
      $(".payment-item").addClass('disable')
      $(".payment-item[data-paymentid=#{@selectedPaymentInstrument.Id}]").removeClass("disable")

    onOrderError: =>
      $(TWC.MVC.View.eventbus).trigger("loading", true)
      TWC.MVC.Router.load "paymenterror-popup",
        data: []

    removePromotion: ->
      @ot.trackOutboundClick('ultraapp.com/confirmpurchase', "#{@formattedTitle}_removecode_button")
      @updateHistory(@productModel, @pricingPlan, @paymentInstruments, "", @upgradeProduct, @selectedPaymentInstrument)
      @promotionCode = ""

      if @selectedPaymentInstrument is null
        @selectedPaymentInstrument = _.findWhere(@paymentInstruments, {Default:true})
      orderPaymentInstrumentIds = [@selectedPaymentInstrument.Id]

      @model = new CDOrderQuote({}, {
        pricingPlanId: @pricingPlan.get("Id"),
        productId: @productModel.get("Id"),
        paymentInstrumentIds: orderPaymentInstrumentIds,
        defaultPaymentInstrumentId: @selectedPaymentInstrument.Id,
        promotionCodes: ""
        errorCallback: @onOrderError
      })

      @refresh()

    submitOrderAction: ->
      @ot.trackOutboundClickToBuy('ultraapp.com/confirmpurchase', "#{@formattedTitle}_purchase_button")

      $(TWC.MVC.View.eventbus).trigger("loading", true)
      # Check if selected payment option has sufficient balance for the order, otherwise add the default payment as second payment option
      orderPaymentInstrumentIds = [@selectedPaymentInstrument.Id]
      pricingPlanId = @pricingPlan.get("Id")
      promotionCodes = if @promotionCode then [@promotionCode] else ""
      if @upgradeProduct
        CDPaymentManagement.submitUpgradeOrder(orderPaymentInstrumentIds, pricingPlanId, @productModel.get("Id"), @productModel.get("LockerItemId"), promotionCodes, (error, response) =>
          #        console.log("submitorder", orderPaymentInstrumentIds, pricingPlanId, @productModel.get("Id"), response, error)
          # Revert session level back to 4 after completing order
          CDSubscriberManagement.degradeDeviceSession =>

            if not error
              TWC.MVC.Router.load "paymentsuccess-view",
                data: [@productModel, @extrasCallback]

            else
              message = CDPaymentManagement.orderErrors(error)
              errorAlert = new Error()
              errorAlert.load
                data: [message]

            $(TWC.MVC.View.eventbus).trigger("loading", false)
        )
      else
        CDPaymentManagement.submitOrder(orderPaymentInstrumentIds, pricingPlanId, [@productModel.get("Id")], promotionCodes, (error, response) =>
  #        console.log("submitorder", orderPaymentInstrumentIds, pricingPlanId, @productModel.get("Id"), response, error)
          # Revert session level back to 4 after completing order
          CDSubscriberManagement.degradeDeviceSession =>

            if not error
              TWC.MVC.Router.load "paymentsuccess-view",
                data: [@productModel, @extrasCallback]

            else
              message = CDPaymentManagement.orderErrors(error)
              errorAlert = new Error()
              errorAlert.load
                data: [message]

            $(TWC.MVC.View.eventbus).trigger("loading", false)
        )

    setPromoCode: ->
      @updateHistory(@productModel, @pricingPlan, @paymentInstruments, @promotionCode, @upgradeProduct, @selectedPaymentInstrument)
      TWC.MVC.Router.load "promocode-view",
        data: [
          @pricingPlan.get("Id"),
          @productModel.get("Id"),
          @orderPaymentInstrumentIds,
          @selectedPaymentInstrument.Id,
          (code)=>
            @promotionCode = code
            @updateHistory(@productModel, @pricingPlan, @paymentInstruments, @promotionCode, @upgradeProduct, @selectedPaymentInstrument)
            TWC.MVC.History.popState()
        ]

    cancel: ->
      @ot.trackOutboundClick('ultraapp.com/detail', "#{@formattedTitle}_cancel_button")
      CDSubscriberManagement.degradeDeviceSession (error)=>
        if error? and error isnt false
          message = CDPaymentManagement.orderErrors(error)
          errorAlert = new Error()
          errorAlert.load
            data: [message]
        else
          historyResetPosition = 2
          found = false
          for i in [0...TWC.MVC.History.size()]
            if TWC.MVC.History.history[i].uid is "productDetail-view"
              historyResetPosition = i+1
              found = true

            if not found and TWC.MVC.History.history[i].uid is "bundleDetail-view"
              historyResetPosition = i+1

          # Do not reset history after cancelling purchase when at extras
          if @extrasCallback?
            @extrasCallback(false)
          else
            TWC.MVC.History.reset(historyResetPosition)
            TWC.MVC.History.popState()



  paymentConfirmationView.get()
