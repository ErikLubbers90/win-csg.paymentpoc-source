define [
  "framework",
  "settingPages",
  "config",
  "jquery",
  "application/helpers/submenuHelper",
  "../../models/baseModel",
  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDSubscriberManagement",
  "application/api/authentication",
  "../../models/analytics/omnituretracker",
  "i18n!../../../lang/nls/manifest"
], (TWC, ConfigSettings, Config, $, MenuHelper, BaseModel, CDBaseRequest, CDSubscriberManagement, Authentication, OmnitureTracker, Manifest) ->

  paymentSuccessModel = BaseModel.extend
    defaults:
      "manifest": Manifest,

  class paymentSuccessView extends TWC.MVC.View
    uid: 'paymentsuccess-view'

    el: "#screen"

    type: 'page'

    model: new paymentSuccessModel()

    defaultFocus: "#continueButton"

    external:
      html:
        url:"skin/templates/screens/payment/paymentsuccess.tpl"

    events:
      "enter #watchNowButton" : "watchNow"
      "enter #continueButton" : "continue"

    onload: (@productModel, @extrasCallback = null) ->
      console.log "onload payment confirmation"

    template:(html, data) ->
      _.template(html)(
        data: data,
        productThumbnail: if @productModel.get("ThumbnailUrl")? then @productModel.get("ThumbnailUrl") else "",
        manifest: Manifest
      )

    onRenderComplete: ->

      #grey out button for 5 seconds to let the backend process the purchase and change message
      $("#continueButton").fadeTo(0,0.4)
      @blockContinueButton = true
      $(".success-message").html(Manifest.payment_please_wait)

      if @extrasCallback? or @productModel.get("BundleChildren")?
        $("#watchNowButton").hide()

      setTimeout =>
        @blockContinueButton = false
        $("#continueButton").fadeTo(0,1)
        $(".success-message").html(Manifest.payment_success_description)
        if @extrasCallback? or @productModel.get("BundleChildren")?
          TWC.MVC.Router.setFocus "#continueButton"
      , 5000


      @historyResetPosition = 2
      found = false
      for i in [0...TWC.MVC.History.size()]
        if TWC.MVC.History.history[i].uid is "productDetail-view"
          @historyResetPosition = i+1
          found = true

        if not found and TWC.MVC.History.history[i].uid is "bundleDetail-view"
          @historyResetPosition = i+1

      TWC.MVC.History.reset(@historyResetPosition)
      @ot = OmnitureTracker.get()
      @formattedTitle = @productModel.get("Name").toLowerCase().replace(/[^A-Za-z0-9]/g, "")
      @ot.trackPageView("thankyou.html")

    watchNow: ->
      $(TWC.MVC.View.eventbus).trigger("loading", [true])
      setTimeout ()=>
        @productModel.refresh =>
          TWC.MVC.Router.load "videoplayer-view",
            data: [@productModel, false, false, [], [], 0, "#{@formattedTitle}_watchnow"]

            callback: ()=>
              TWC.MVC.History.reset(@historyResetPosition)
      , 2000

    continue: ->
      return if @blockContinueButton
      @ot.trackOutboundClick('ultraapp.com/detail', 'continue_button')
      TWC.MVC.History.popState()
      @extrasCallback?(true)

  paymentSuccessView.get()

