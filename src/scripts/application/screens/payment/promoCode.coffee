define [
  "framework",
  "settingPages",
  "config",
  "jquery",
  "../../models/baseModel",
  "application/api/cd/CDPaymentManagement",
  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDSubscriberManagement",
  "application/api/authentication",
  "../../models/analytics/omnituretracker",
  "i18n!../../../lang/nls/manifest"
], (TWC, ConfigSettings, Config, $, BaseModel, CDPaymentManagement, CDBaseRequest, CDSubscriberManagement, Authentication, OmnitureTracker, Manifest) ->

  promoCodeModel = BaseModel.extend
    defaults:
      "manifest": Manifest,

  class promoCodeView extends TWC.MVC.View
    uid: 'promocode-view'

    el: "#screen"

    type: 'page'

    model: new promoCodeModel()

    external:
      html:
        url:"skin/templates/screens/payment/promocode.tpl"

    events:
      "enter #submitButton": "submitPromoCode"
      "enter #cancelButton": "goBack"
      "enter #helpButton": "goToHelp"
      "enter #screenCloseButton" : "goBack"

    zones: [
      {uid:"menu-zone", data:[]}
    ]

    onload: (@pricingPlanId, @productId, @paymentInstrumentIds, @defaultPaymentInstrumentId, @promoSuccessCallback) ->


    onRenderComplete: ->
      $("#mainMenu").hide()
      $("#loginResult").hide()
      $("#ime").show()
      @ot = OmnitureTracker.get()
      @ot.trackPageView('promocode.html')

    onZoneRenderComplete: ->
#      TWC.MVC.Router.setActive "keyboard-zone"
#      TWC.MVC.Router.setFocus "#character_0"
      $("#inputField").focus()


    submitPromoCode: ->
      @ot.trackOutboundClick('ultraapp.com/confirmpurchase','promocodesubmit_button')
      promoCode = $("#inputField").val()
      if promoCode.length is 0
        @showErrorMessage("")
        return

      CDPaymentManagement.calculateOrderQuote @paymentInstrumentIds, @pricingPlanId, @productId, [promoCode], (error, response)=>
        if error
          @showErrorMessage(promoCode)
        else
          if response.InvalidRedemptionCodes? or not response.Quote.AppliedCoupons? or response.Quote.AppliedCoupons.length is 0
            @showErrorMessage(promoCode)
          else
            @promoSuccessCallback?(promoCode)


    showErrorMessage: (code)->
      $("#inputField").val("")
      $(".subtitle").hide()
      code = code.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');

      $("#loginResult").html("#{Manifest.invalid_coupon_code} <br/> #{code}").show()
      $(".helpContainer").removeClass("disable")


    goBack: ->
      TWC.MVC.History.popState()

    goToHelp: ->
      TWC.MVC.Router.load "settings-view",
        data: [0]

    #Focus events
    focusUp: ->
      $("#inputField").focus()

    focusDown: ->
      if not $(".helpContainer").hasClass('disable')
        TWC.MVC.Router.setFocus "#helpButton"

    focusDownFromKeyboard: ->
      TWC.MVC.Router.setActive "promocode-view"
      TWC.MVC.Router.setFocus "#submitButton"


  promoCodeView.get()

