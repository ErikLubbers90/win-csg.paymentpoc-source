define [
  "config",
  "settingPages",
  "framework",
  "application/models/baseModel",
  "application/helpers/submenuHelper",
  "application/api/cd/CDPaymentManagement",
  "application/api/cd/CDSubscriberManagement",
  "application/api/cd/collections/CDProductCollection",
  "application/api/cd/models/CDProduct",
  "jquery",
  "i18n!../../../lang/nls/manifest",
  "i18n!../../../lang/nls/settings",
  "i18n!../../../lang/nls/license",
  "i18n!../../../lang/nls/regionsettings",
#  "application/popups/unsubscribe/unsubscribe",
  "application/api/authentication",
#  "application/popups/payment/setDevicePin"
  "application/api/cd/CDBaseRequest",
  "application/popups/generic/alert",
  "application/popups/generic/alertWithOptions",
  "application/popups/login/loginConfirmation",
  "../../models/analytics/omnituretracker"
], (Config, ConfigSettings, TWC, BaseModel, MenuHelper, CDPaymentManagement, CDSubscriberManagement, CDProductCollection, CDProductModel, $, Manifest, SettingsManifest, License, RegionSettings, Authentication, CDBaseRequest, Alert, AlertWithOptions, LoginConfirmation, OmnitureTracker) ->

  settingsModel = BaseModel.extend
    defaults:
      "manifest": Manifest,
      "settings": ConfigSettings,
      "license":License,
      "SubMenuItems": []
      "settingsManifest": SettingsManifest


    parseSubmenu: (items, authenticated) ->
      # Create copy of items to prevent source to be overwritten
      itemsCopy = items.concat()
      env = ""
      env = " - " + Config.environment if Config.environment is "development"

      @.set
        SubMenuItems: itemsCopy
        authenticated: authenticated
        version: Config.version
        build: Config.build + env


  class settingsView extends TWC.MVC.View
    uid: 'settings-view'

    el: '#screen'

    type: 'page'

    model: new settingsModel()


    external:
      html:
        url:"skin/templates/screens/settings/settings.tpl"

    zones: [
      {uid:"menu-zone", data: []},
    ]

    currentFaqPosition: 0

    defaultPaymentInstrumentId: null

    bundleRedemption: false
    singleBundleItem: false
    redemptionSuccess: false

    redemptionCodeLength: 16

    redemptionSuccessFocus: null

    events:
      "enter .redemption .button" : "validateCode"
      "enter .account #button_account" : "accountAction"
      "enter .account #reset_pin" : "resetPin"
      "enter .account #disable_pin" : "disablePin"
      "enter .account #set_pin": "setPin"
      "enter .submenu" : "showSettingsContent"
      "enter .redemption-success #redemptionBackButton": "showRedemptionAgain"
      "enter .redemption-success #goToMovieButton": "goToMovie"
      "enter .payment-methods .payment-button": "selectDefaultPaymentMethod"
      "enter .redemptionPlaylistContent .product": "openProduct"
      "enter #privacyPageRight" : "showNextPage"
      "enter #privacyPageLeft" : "showPrevPage"
      "enter #termsOfUsePageRight" : "showNextPage"
      "enter #termsOfUsePageLeft" : "showPrevPage"
      "enter #termsOfServicePageRight" : "showNextPage"
      "enter #termsOfServicePageLeft" : "showPrevPage"
      "swipeRight .content": "showNextPage"
      "swipeLeft .content": "showPrevPage"
      "swipeUp .faq.settingsContainer .settingsItems": "faqClickUp"
      "swipeDown .faq.settingsContainer .settingsItems": "faqClickDown"
      "enter .faq-item": "clickFaqItem"
      "click .faq-container .faq-item": "clickFaqItem"
      "return" : "onReturn"
      "enter #navigationTop": "faqClickUp"
      "enter #navigationBottom": "faqClickDown"

    swipeLeft: ->
      console.log "left"

    onload: (@activeSettingsPage="", @useremail = null, @loginCallback = null, @returnCallback = null)->
      log "onload settingsView"
      #get menu items from settings manifest
      @parseSettingsMenu()
      @ot = OmnitureTracker.get()


    onunload: ->
      @defaultPaymentInstrumentId = null
      @redemptionSuccessFocus = null

    clearRedemptionSuccesList: ->
      $(".redemptionPlaylistContent").html("")

    updateAccountContent: ->
      account = ConfigSettings.pages[3]
      if Authentication.isLoggedIn()
        deviceUtils = TWC.SDK.Utils.get()
        $(".account #button_account").html account.button
        subscriber = CDBaseRequest.subscriber.toJSON()
        $(".account .first_name").text(subscriber.FirstName)
        $(".account .username").text(subscriber.Login)
        currency = RegionSettings.default_payment_instruments_currency
        CDPaymentManagement.retrieveWallet currency, (error, data)=>
          CDPaymentManagement.updatePaypalData data.PaymentInstruments, (paymentInstruments)=>
            data.PaymentInstruments = paymentInstruments
            if not error
              hasPin = false
              CDSubscriberManagement.retrieveDevicePinStatus (error, hasPin)=>
                if not error

                  @subscriberPaymentInstruments = _.filter(data.PaymentInstruments, (paymentInstrument) -> paymentInstrument.Type in Config.supportedPaymentOptions )
                  if @subscriberPaymentInstruments.length is 0 and Config.enablePaymentOptions
                    $(".payment-title").text( Manifest.settings_payment_method_on_file )
                    $(".payment-methods").html( "<p>" + Manifest.settings_no_payment_method + "</p>")
                    $(".payment-methods a").click (e) ->
                      url = $(this).attr('href')
                      window.open(url, '_blank');
                  else if Config.enablePaymentOptions
                    $(".payment-title").text( Manifest.settings_payment_method_on_file )
                    if @subscriberPaymentInstruments.length is 1
                      $(".payment-methods").html( "<p>" + @formatPaymentInstrument(@subscriberPaymentInstruments[0]) + "</p>")
                    else
                      i = 0
                      $(".payment-methods").empty()
                      $(".payment-methods").html("<div class='payment-scroll-container'></div>")
                      @paymentInstrumentOffset = 0
                      for instrument in @subscriberPaymentInstruments
                        button = []
                        button.push("<button id='payment-#{i}' class='button payment-button light' data-left='focusLeft' " + (if i < @subscriberPaymentInstruments.length-1 then "data-right='focusRight'" else "") + " data-down='focusDown' data-up='focusUp' data-paymentinstrument-id='#{instrument.Id}'>")
                        button.push(@formatPaymentInstrument(instrument))
                        button.push("</button>")
                        $(".payment-methods .payment-scroll-container").append(button.join(""))

                        # Set default selected paymentinstrument
                        if instrument.Default? and instrument.Default
                          @defaultPaymentInstrumentId = instrument.Id
                          $("#payment-#{i}").addClass("select")
                        i++
                  if Config.enablePaymentPin
                    $(".pin-settings").addClass('visible')
                    if hasPin
                      $(".pin-title").text("You are using a PIN for purchase on this device")
                      $("#reset_pin").removeClass("disable")
    #                  $("#disable_pin").removeClass("disable")
                    else
                      $(".pin-title").text("Lorem ipsum would you like to set a PIN for futrure purchases on this device?");
                      $("#set_pin").removeClass("disable")
                  else
                    $(".payment-button").removeAttr("data-down")

        # Retrieve uv user data and set uv account management description
        CDSubscriberManagement.retrieveUvUser( (error, uvUserData) =>
          uvImg = "<span class=\"uv-icon\"></span>"

          if not error
            uvUsername = uvUserData?.User?.Username
            uvAccountDescription = Manifest.settings_manage_uv_account_description.replace("{uv_username}", uvUsername)
            if $("#settings_3").height() > $("#screen").height()
              paddingTop = ($("#screen").height() - ($("#settings_3").height() - parseInt($("#settings_3 .content").css("padding-top")))) / 2
              $("#settings_3 .content").css("padding-top", "#{paddingTop}px")
          else
            # UV not linked to account
            uvAccountDescription = Manifest.settings_manage_uv_account_not_linked_description

          $("#settings_3 .uv-account-description").html("#{uvImg}#{uvAccountDescription}")
          $("#settings_3 .uv-account-description a").click (e) =>
            url = $("#settings_3 .uv-account-description a").attr('href')
            @openURI(url)
            #window.open(url, '_blank');
        )

      else
        $(".account #reset_pin").hide()


    formatPaymentInstrument: (instrument)->
      if instrument.TypeName is "Credit Card"
        return "#{instrument.Name.split("-")[0]} ****-****-****-#{instrument.Name.split("-")[1]}"
      else if instrument.TypeName is "PayPal Account"
        infoText = if instrument.PayPalAccount? and instrument.PayPalAccount.UserName? then instrument.PayPalAccount.UserName.stub(26) else instrument.TypeName
        return "<span class='paypal-icon'></span> <span class='lowercase'>#{ infoText }</span>"
      else
        return instrument.Name


    parseSettingsMenu: ->
      @menuItems = []
      #insert other submenu pages from settings manifest
      content = ConfigSettings.pages
      for item in content
        @menuItems.push {Name: item.menuTitle, id: item.id, loginStatus: item.loginStatus}


      #add menu item to array
      @model.parseSubmenu(@menuItems, Authentication.isLoggedIn())


    onReturn: ->
      #exception if user hits help button accidentally during login procedure
      if @useremail
        userEmail = @useremail
        @useremail = null
        TWC.MVC.Router.load "register-view",
          data: [@loginCallback, @returnCallback, 2, userEmail]
      else
        TWC.MVC.History.reset()
        TWC.MVC.History.popState()


    onRenderComplete: ->
      $("#mainMenu").show()

      @currentFaqPosition = 0
      $(".settingsContainer").hide()
      $(".paginator").hide()
      #exception for account state
      if @activeSettingsPage isnt ""
        @currentMenuItem = "submenu_" + @activeSettingsPage
        TWC.MVC.Router.setFocus("#submenu_#{@activeSettingsPage}")
        TWC.MVC.Router.setSelect("#submenu_#{@activeSettingsPage}")
        $("#settings_#{@activeSettingsPage}").show()
        pageToLoad = @activeSettingsPage
        @activeSettingsPage = ""

        @loadSettingsPage(pageToLoad)
      else
        @currentMenuItem = "submenu_0"
        TWC.MVC.Router.setSelect("#submenu_0")
        TWC.MVC.Router.setFocus("#submenu_0")
        $("#settings_0").show()
        @ot.trackPageView("settings.help.html")

      @fillContent()  #TOS
      @fillFaqAnswers()
      MenuHelper.setSelectedMenuItem("settings")

      # Enable click action links help & about page
      $("a").click (e) =>
        console.log(e.target.href)
        url = e.target.href
        if url.indexOf("mailto:") > -1
          window.location = url
        else
          @openURI(url)
          #window.open(url, '_blank');

    onZoneRenderComplete: ->
      activeSettingsPageId = $("#content .settingsContainer:visible").attr("id")
      activeSettingsPageIndex = parseInt(activeSettingsPageId.split("_")[1])

      #focus on active page
      TWC.MVC.Router.setActive "settings-view"
      switch activeSettingsPageIndex
        when 0
          TWC.MVC.Router.setFocus "#submenu_0"
        when 2
          $("#inputField").focus()
        when 3
          TWC.MVC.Router.setFocus "#submenu_3"

    fillContent: ->
      content = SettingsManifest.privacy_policy
      @totalPagesArr = {};
      for e in ["#settings_4", "#settings_5"]
        if e is "#settings_4" then content = SettingsManifest.terms_of_service
        if e is "#settings_5" then content = SettingsManifest.privacy_policy
        $(e + ".serviceText .contentText").addClass "columns"
        $(e + ".serviceText .contentText").append content
        if Config.resolution[0] is "1920"
          wordsPerPage = 510
        else
          if e is "#settings_4"
            wordsPerPage = 530
          else if e is "#settings_5"
            wordsPerPage = 550

        contentWidth = $(e + ".serviceText .content").width()
        columnGap = Math.round(0.05208333333333*contentWidth)
        @currentPage = 1
        @totalPagesArr[e] = Math.ceil(@wordCount(content) / wordsPerPage)

        totalWidth = (parseInt(contentWidth)*@totalPagesArr[e])-columnGap
        $(e + ".serviceText .contentText").css({
          "columnCount":@totalPagesArr[e]*2
          "columnGap":columnGap
          "width":totalWidth
        }).show()

    fillFaqAnswers: ->
      for faq in $(".faq-item .answer")
        $(faq).html($(faq).text())


    #Navigation handling for the tos section that contain multiple pages
    showNextPage: ->
      return if @blockedForAnimation
      @blockedForAnimation = true
      activeSettingsPageId = $("#content .settingsContainer:visible").attr("id")
      activeSettingsPageIndex = parseInt(activeSettingsPageId.split("_")[1])
      activeParentId = "#settings_" + activeSettingsPageIndex
      $el = $(TWC.MVC.Router.getFocus())
      # Workaround menu unselected after click to next page
      $("##{@currentMenuItem}").addClass "select"

      contentWidth = $(activeParentId+".serviceText .content").width()
      pos = parseInt($(activeParentId+".serviceText .contentText").css("marginLeft"))
      if (pos * -1) > (contentWidth * (@totalPages - 2))
        return
      else
        pos -= contentWidth

      $("#{activeParentId} .contentText").css("marginLeft":"#{pos}px")
      @updatePageNumberAndIndicators(1)

      setTimeout ()=>
        @blockedForAnimation = false
      , 300


    showPrevPage: ->
      return if @blockedForAnimation

      return if @currentPage is 1
      @blockedForAnimation = true
      activeSettingsPageId = $("#content .settingsContainer:visible").attr("id")
      activeSettingsPageIndex = parseInt(activeSettingsPageId.split("_")[1])
      activeParentId = "#settings_" + activeSettingsPageIndex

      # Workaround menu unselected after click to next page
      $("##{@currentMenuItem}").addClass "select"

      contentWidth = $(activeParentId+".serviceText .content").width()
      pos = parseInt($(activeParentId+".serviceText .contentText").css("marginLeft"))
      return if pos is 0
      pos += contentWidth
      $("#{activeParentId} .contentText").css("marginLeft":"#{pos}px")
      @updatePageNumberAndIndicators(-1)

      setTimeout ()=>
        @blockedForAnimation = false
      , 300

    updatePageNumberAndIndicators: (data) ->
      activeSettingsPageId = $("#content .settingsContainer:visible").attr("id")
      activeSettingsPageIndex = parseInt(activeSettingsPageId.split("_")[1])
      activeParentId = "#settings_" + activeSettingsPageIndex
      if data is 1
        @currentPage += 1
      else if data is -1
        @currentPage -= 1

      $(".pageNumber").html(@currentPage + " / " + @totalPages)


      if @currentPage is 1
        $("#{activeParentId}.serviceText .pageLeft").addClass "disable"
        TWC.MVC.Router.setFocus($("#{activeParentId}.serviceText .pageRight"))
      else if @currentPage is @totalPages
        $("#{activeParentId}.serviceText .pageRight").addClass "disable"
        TWC.MVC.Router.setFocus($("#{activeParentId}.serviceText .pageLeft"))
      #
      if @currentPage > 1
        $("#{activeParentId}.serviceText .pageLeft").removeClass "disable"
      if @currentPage < @totalPages
        $("#{activeParentId}.serviceText .pageRight").removeClass "disable"


    wordCount: (string) ->
      matches = string.match(/\S+/g)
      return matches.length

    valueIsChanged: (value)->
      for i in [0..value.length]
        $("#redemptionChar-#{i} .text").text(value.charAt(i))
      for i in [value.length..@redemptionCodeLength]
        $("#redemptionChar-#{i} .text").text("")
      if value.length is @redemptionCodeLength
        TWC.MVC.Router.setActive "settings-view"
        TWC.MVC.Router.setFocus "#submitButton"


    #navigation for action buttons
    focusKeyboard: ->
      TWC.MVC.Router.setActive "settings-view"
      TWC.MVC.Router.setFocus "#submitButton"

    focusLeft: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("payment-button")
        pos = parseInt(TWC.MVC.Router.getFocus().split("-")[1])
        if pos is 0
          @moveToMenu()
        else
          @paymentInstrumentOffset--
          prevLeftPos = $("#payment-#{pos-1}").offset().left - $(".payment-scroll-container").offset().left
          $(".payment-scroll-container").css({
            "-o-transform":"translate(" + -prevLeftPos + "px, 0)",
            "-webkit-transform":"translate3d(" + -prevLeftPos + "px, 0, 0)",
          })
          TWC.MVC.Router.setFocus "#payment-#{pos-1}"

    focusRight: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("payment-button")
        pos = parseInt(TWC.MVC.Router.getFocus().split("-")[1])
        return if not $("#payment-#{pos+1}").length
        @paymentInstrumentOffset++
        nextLeftPos = $("#payment-#{pos+1}").offset().left - $(".payment-scroll-container").offset().left
        $(".payment-scroll-container").css({
          "-o-transform":"translate(" + -nextLeftPos + "px, 0)",
          "-webkit-transform":"translate3d(" + -nextLeftPos + "px, 0, 0)",
        })
        TWC.MVC.Router.setFocus "#payment-#{pos+1}"

    focusUp: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("pin-button")
        if $(".button.payment-button").length > 0
          TWC.MVC.Router.setFocus "#"+$("#payment-#{@paymentInstrumentOffset}").attr('id')
        else
          TWC.MVC.Router.setFocus "#button_account"
      if $el.hasClass("payment-button")
          TWC.MVC.Router.setFocus "#button_account"



    focusDown: ->
      $el = $(TWC.MVC.Router.getFocus())

      if $el.hasClass("payment-button")
        if $(".button.pin-button").length > 0
          TWC.MVC.Router.setFocus "#"+$(".button.pin-button:not(.disable):first").attr('id')


      else if $el.attr("id") is "button_account"
        if $(".button.payment-button").length > 0
          TWC.MVC.Router.setFocus "#"+$("#payment-#{@paymentInstrumentOffset}").attr('id')
        else if $(".button.pin-button").length > 0
          TWC.MVC.Router.setFocus "#"+$(".button.pin-button:not(.disable):first").attr('id')

    faqFocusUp: ->
      return if @currentFaqPosition is 0
      @currentFaqPosition--
      TWC.MVC.Router.setFocus ".faq-item:eq(#{@currentFaqPosition})"
      TWC.MVC.Router.setSelect ".faq-item:eq(#{@currentFaqPosition})"
      scrollTop=0
      for d,i in $(".faq-scroll .faq-item:lt(#{@currentFaqPosition})")
        scrollTop += $(d).outerHeight(true)
      scrollTop=scrollTop*-1
      $(".faq-scroll").css({
        "-o-transform": "translate(0, " + scrollTop + "px)",
        "-webkit-transform": "translate3d(0, " + scrollTop + "px, 0)"})
      @showHideNavigation()

    faqFocusDown: ->
      return if @currentFaqPosition is SettingsManifest.faq_content.length-1
      @currentFaqPosition++
      TWC.MVC.Router.setFocus ".faq-item:eq(#{@currentFaqPosition})"
      TWC.MVC.Router.setSelect ".faq-item:eq(#{@currentFaqPosition})"
      scrollTop=0
      for d,i in $(".faq-scroll .faq-item:lt(#{@currentFaqPosition})")
        scrollTop += $(d).outerHeight(true)
      scrollTop=scrollTop*-1
      $(".faq-scroll").css({
        "-o-transform": "translate(0, " + scrollTop + "px)",
        "-webkit-transform": "translate3d(0, " + scrollTop + "px, 0)"})
      @showHideNavigation()

    faqClickUp: ->
      @currentFaqPosition-=2
      if @currentFaqPosition<1
        @currentFaqPosition=1
      @faqFocusUp()

    faqClickDown: ->
      @currentFaqPosition+=2
      if @currentFaqPosition>SettingsManifest.faq_content.length-2
        @currentFaqPosition=SettingsManifest.faq_content.length-2
      @faqFocusDown()

    showHideNavigation: ->
      if @currentFaqPosition == 0
        $("#navigationTop").hide()
      else
        $("#navigationTop").show()

      if @currentFaqPosition == SettingsManifest.faq_content.length-1
        $("#navigationBottom").hide()
      else
        $("#navigationBottom").show()

      clearTimeout(@mouseActiveTimer)
      $(".navigationContainer").hide()

    clickFaqItem: (e)->
      #console.log @currentFaqPosition
      # Open selected faq question
      target = e.target
      if !$(target).hasClass("faq-item")
        target = $(target).parent()
      @selectFaqItem(target)

    openURI:(url) ->
      uri = new Windows.Foundation.Uri(url)
      try
        Windows.System.Launcher.launchUriAsync(uri)
      catch e
        log e

    selectFaqItem: (target)->
      TWC.MVC.Router.setSelect "#submenu_1"
      @currentFaqPosition = $(target).index()
      TWC.MVC.Router.setFocus ".faq-item:eq(#{@currentFaqPosition})"
      if $(target).hasClass("open")
        $(target).removeClass("open")
        $(target).find(".question .sign").html("+")
      else
        $(target).addClass("open")
        $(target).find(".question .sign").html("-")

    inputFocusDown: ->
      TWC.MVC.Router.setFocus ".redemption .button"

    paginatorRight: ->
      activeSettingsPageId = $("#content .settingsContainer:visible").attr("id")
      activeSettingsPageIndex = parseInt(activeSettingsPageId.split("_")[1])

      if !$("#settings_#{activeSettingsPageIndex}.serviceText .pageRight").hasClass("disable")
        TWC.MVC.Router.setFocus($("#settings_#{activeSettingsPageIndex}.serviceText .pageRight"))

    paginatorLeft: ->
      activeSettingsPageId = $("#content .settingsContainer:visible").attr("id")
      activeSettingsPageIndex = parseInt(activeSettingsPageId.split("_")[1])
      if !$("#settings_#{activeSettingsPageIndex}.serviceText .pageLeft").hasClass("disable")
        TWC.MVC.Router.setFocus($("#settings_#{activeSettingsPageIndex}.serviceText .pageLeft"))
      else
        @moveToMenu()

    #specific functions
    setPin: ->
      #allow user to reset device PIN
      if Authentication.isLoggedIn()
        subscriber = CDBaseRequest.subscriber.toJSON()

        @updateHistory(3)
        TWC.MVC.Router.load "pin-view",
          data: [subscriber.login, "", "setPin", ()=>
            TWC.MVC.History.reset(2)
            TWC.MVC.History.popState()
          ]

    resetPin: ->
      #allow user to reset device PIN
      if Authentication.isLoggedIn()
        subscriber = CDBaseRequest.subscriber.toJSON()

        @updateHistory(3)
        TWC.MVC.Router.load "pin-view",
          data: [subscriber.login, "", "setPin", ()=>
            TWC.MVC.History.reset(2)
            TWC.MVC.History.popState()
          ]


    disablePin: ->
      #allow user to reset device PIN
      if Authentication.isLoggedIn()
        subscriber = CDBaseRequest.subscriber.toJSON()

        TWC.MVC.Router.load "pin-view",
          data: [subscriber.login, "null", "resetPinPassword", ()=>
            TWC.MVC.History.reset(2)
            TWC.MVC.History.popState()
          ]


    accountAction: ->
      #Open a logout popup instead of content
      if Authentication.isLoggedIn()

        @ot.trackOutboundClick('ultraapp.com/settings','logout_button')

        # Workaround menu unselected after click to next page
        confirmationAlert = new AlertWithOptions()
        confirmationAlert.leftButtonAction = ()->
          @close()

        confirmationAlert.rightButtonAction = ()->
          @close ()->
            Authentication.logout()
            TWC.MVC.History.reset(1)
            TWC.MVC.Router.load "settings-view",
              data: [0]

        confirmationAlert.load
          data: [Manifest.logout_confirmation, Manifest.logout_descripition, Manifest.logout, Manifest.cancel]

    fixSelectState:(selected, focused)->
      TWC.MVC.Router.setSelect selected
      TWC.MVC.Router.setFocus focused

    validateCode: ->
      @clearRedemptionSuccesList()
      @ot.trackOutboundClick('ultraapp.com/redeem','settings_redeem_button')

      redemptionCode = $("#inputField").val().substr(0, @redemptionCodeLength)

      redemptionCode = "MULT111122223333"
      # Check if a redemptioncode has been filled in
      if redemptionCode.length is 0
        $(".redemption .actionResult").html(Manifest.redemption_code_required)
        @fixSelectState("#submenu_2", "#submitButton")
        return
      if redemptionCode.length <= 4
        $("#inputField").val("")
        $(".redemption-char .text").text("")
        $(".redemption .actionResult").html(Manifest.invalid_redemption_code)

        @fixSelectState("#submenu_2", "#submitButton")
        return

      $(TWC.MVC.View.eventbus).trigger("loading", true)

      # Search for product using the redemptioncode
      CDPaymentManagement.searchProductsByCoupon(redemptionCode, (couponError, couponResponse) =>
        if not couponError
          if couponResponse?.Products?.length and couponResponse.Applications?.length

            productIds = []
            bundleRedemption = false
            applicationsProduct = couponResponse.Applications[0] #get first product information
            pricingPlanId = applicationsProduct.PricingPlanIds[0]
            productInformation = couponResponse.Products[0]
            products = couponResponse.Products
            productTitle = productInformation.Name  #get first products name for the success popup
            productThumbnail = if productInformation.ThumbnailUrl? then productInformation.ThumbnailUrl else ""

            #if promotioncode has multiple products we need the product id's for the submitorder later on
            if couponResponse.Applications.length > 1
                @singleBundleItem = false
                bundleRedemption = true
                for prodId in couponResponse.Applications
                  productIds.push(prodId.ProductId)
            #only one product or bundled product
            else
              if couponResponse.Products[0].StructureType in [2,4]
                #single bundled product - check used for success screen to disable some options
                @singleBundleItem = true
              productIds.push(applicationsProduct.ProductId)

             # Submit order (does not require a paymentinstrument, only redemptioncode)
            CDPaymentManagement.submitOrder([], pricingPlanId, productIds, [redemptionCode], (redemptionError, redemptionResponse) =>
              if redemptionError
                $("#inputField").val("")
                $(".redemption-char .text").text("")
                $(".redemption .actionResult").html(CDPaymentManagement.orderErrors(redemptionError))
                @fixSelectState("#submenu_2", "#submitButton")
                $(TWC.MVC.View.eventbus).trigger("loading", false)
              else
                @showRedemptionSuccess(productTitle, productIds[0], productThumbnail, bundleRedemption, products)
              )
          else
            $("#inputField").val("")
            $(".redemption-char .text").text("")
            # No products found
            $(".redemption .actionResult").html(Manifest.code_redemption_code_error)
            @fixSelectState("#submenu_2", "#submitButton")
            $(TWC.MVC.View.eventbus).trigger("loading", false)

        else
          $("#inputField").val("")
          $(".redemption-char .text").text("")
          $(".redemption .actionResult").html(CDPaymentManagement.orderErrors(couponError))
          @fixSelectState("#submenu_2", "#submitButton")
          $(TWC.MVC.View.eventbus).trigger("loading", false)
      )


    #global functions
    moveToContent: ->
      #cases when user can navigate to page
      id = $(".focus").data("id")
      activeSettingsPageId = $("#content .settingsContainer:visible").attr("id")
      activeSettingsPageIndex = activeSettingsPageId.split("_")[1]

      #focus on active page
      switch activeSettingsPageIndex
        when "1"
          TWC.MVC.Router.setFocus(".faq-item:first")
        when "2"
          $("#inputField").focus()
        when "2b"
          TWC.MVC.Router.setFocus(".buttons #redemptionBackButton")
        when "3"
          TWC.MVC.Router.setFocus(".account #button_account")
        when "4"
          if !$("#settings_6.serviceText .pageRight").hasClass("disable")
            return TWC.MVC.Router.setFocus("#settings_4.serviceText .pageRight")
          if !$("#settings_6.serviceText .pageLeft").hasClass("disable")
            return TWC.MVC.Router.setFocus("#settings_4.serviceText .pageLeft")
        when "5"
          if !$("#settings_5.serviceText .pageRight").hasClass("disable")
            return TWC.MVC.Router.setFocus("#settings_5.serviceText .pageRight")
          if !$("#settings_5.serviceText .pageLeft").hasClass("disable")
            return TWC.MVC.Router.setFocus("#settings_5.serviceText .pageLeft")

        else
          log "no action assigned to this item"

    moveToMainMenu: ->
      TWC.MVC.Router.setActive "menu-zone"
      TWC.MVC.Router.setFocus "#settings"

    moveToMenu: (focusOnLast=false) ->
      if focusOnLast
        TWC.MVC.Router.setActive "settings-view"
        TWC.MVC.Router.setFocus $("#subMenuItems .menuButton:last")
      else
        TWC.MVC.Router.setActive "settings-view"
        if $("#subMenuItems .menuButton.select").length > 0
          TWC.MVC.Router.setFocus("#" + $("#subMenuItems .menuButton.select").attr("id"))
        else
          TWC.MVC.Router.setFocus("#" + $("#subMenuItems .menuButton:first").attr("id"))

    showRedemptionSuccess: (productTitle, @redemptionProductId, productThumbnail, bundleRedemption, products)->
      @ot.trackPageView('settings.redeem.confirm.html')

      TWC.MVC.Router.find("keyboard-zone").setRedemptionSuccestoTrue?()

      # Remove img tag if available
      $(".content .top img").remove()
      $(".settingsContainer").hide()
      if bundleRedemption
        @bundleRedemption = true
#        $("#goToMovieButton").html(Manifest.settings_go_to_ultraviolet_library)
      $("#settings_2b").show()
      $(".redemption-success h1").html(Manifest.settings_added_to_library.replace("%TITLE%", productTitle))
      if products
        i = 0
        for product in products
          $(".redemptionPlaylistContent").append("""<div id="playlist_product-#{i}" data-title="#{product.Name}" class="product content-sound" data-left="itemFocusLeft" data-right="itemFocusRight" data-down="itemFocusDown" data-id="#{product.Id}" data-type="#{product.StructureType}">
          <div class="productBorder"></div>
          <img src="#{product.ThumbnailUrl}" alt="#{product.Name}" /></div>""")
          i++

      else
        if productThumbnail.length then $(".content .top").prepend("<img src=\"#{productThumbnail}\"/>")



      TWC.MVC.Router.setSelect "#submenu_2"
      TWC.MVC.Router.setFocus "#redemptionBackButton"
      $(TWC.MVC.View.eventbus).trigger("loading", [false])
      @updateRedemptionTitle()

    showRedemptionAgain: ->
      $(".settingsContainer").hide()
      $("#settings_2").show()
      $(".redemption .actionResult").html("")
      $("#inputField").val("")
      @valueIsChanged("")

      TWC.MVC.Router.find("keyboard-zone").setRedemptionSuccestoFalse?()

      TWC.MVC.Router.setSelect "#submenu_2"

      $("#inputField").focus()
      @ot.trackOutboundClick('ultraapp.com/detail','settings_redeem_enteranother_button')

    goToMovie: ->
      @ot.trackOutboundClick('ultraapp.com/videoplayer','settings_redeem_gotomovie_button')
      if @bundleRedemption or @singleBundleItem
        TWC.MVC.Router.load 'locker-view'
      else
        TWC.MVC.Router.load "productDetail-view",
          data: [@redemptionProductId]

    selectDefaultPaymentMethod: ->
      $focus = $(TWC.MVC.Router.getFocus())
      focusPaymentId = $focus.data("paymentinstrument-id")

      # No action required if current default payment method selected
      return if @defaultPaymentInstrumentId? and @defaultPaymentInstrumentId is focusPaymentId

      @ot.trackOutboundClick('ultraapp.com/settings','changepayment_button')

      # Remove selected state payment method
      $(".payment-methods .payment-button.select").removeClass("select")

      # Retrieve full payment instrument data
      CDPaymentManagement.retrievePaymentInstrument(focusPaymentId, (error, paymentInstrumentData)=>
        if not error

          # Requires fully authenticated session
          TWC.MVC.Router.load "register-view",
            data: [
              ()=>
                if paymentInstrumentData.PaymentInstrument?
                  # Set new default payment method
                  paymentInstrumentData.PaymentInstrument.Default = true

                  # Update payment instrument
                  CDPaymentManagement.updatePaymentInstrument(paymentInstrumentData.PaymentInstrument, (error, updatedPaymentInstrumentData)=>

                    # Update default payment instrument was successful
                    if not error

                      # Revert session level back to 4 after completing order
                      CDSubscriberManagement.degradeDeviceSession =>

                      # Reload account page
                        TWC.MVC.Router.load "settings-view",
                          data: [3]
                  )
              , ()=>
                log "Return callback"
              , 5
            ]
      )

    showSettingsContent: ->
      $("#subMenuItems .menuButton.submenu:not('.focus')").removeClass "select"
      $(".paginator").hide()

      id = $(TWC.MVC.Router.getFocus()).data("id")
      loginRequired = $(TWC.MVC.Router.getFocus()).data("loginrequired")
      if loginRequired and not Authentication.isLoggedIn()
        loginConfirmation = new LoginConfirmation()
        loginConfirmation.load
          data: [false, ()=>
            TWC.MVC.Router.load "register-view",
              data: [
                ()=>
                  TWC.MVC.Router.load 'settings-view',
                    data: [id]
                    callback: ()->
                      TWC.MVC.History.reset(1)
              ]
          , true
          ]
      else
        @loadSettingsPage(id, true)

    fixSelectedSubmenuItem: ()->
      activeSettingsPageId = $("#content .settingsContainer:visible").attr("id")
      activeSettingsPageIndex = parseInt(activeSettingsPageId.split("_")[1])

      $("#subMenuItems .menuButton.submenu.select").removeClass "select"
      focusItem = @lastFocused
      @select("#submenu_#{activeSettingsPageIndex}")
      @focus(focusItem)

    loadSettingsPage: (id, showPage = false)->
      if id is 3
        @updateAccountContent()

      else if id is 2
        log(@zones)
        $("#inputField").on "keydown", (e)=>
          val = $("#inputField").val()
          key = e.keyCode
          return (((key>=65 && key<=90) || (key>=48 && key<=57 && !e.shiftKey)) && val.length<16) || key ==8
        $("#inputField").on "keyup", (e)=>
          newVal = $("#inputField").val()
          newVal = newVal.replace(/[^\w]/gi, "")
          if newVal.length > 16
            newVal = newVal.substring(0, 16)
          $("#inputField").val(newVal)
          console.log("newValue=", newVal)
          @valueIsChanged(newVal)

        setTimeout ()=>
          $("#inputField").focus()
        , 5000

        $(".redemption-char").on "click", (e)=>
          $("#inputField").focus()

      else if id in [4,5,6] #tos
        @totalPages = @totalPagesArr["#settings_" + id]
        @currentPage = 1
        e = "#settings_#{id}"
        $(e + ".serviceText .paginator").show()
        $(e + ".serviceText .pageNumber").html(@currentPage + " / " + @totalPages)
        $(e + " .contentText").css("marginLeft":"0px")

      if showPage
        @currentMenuItem = "submenu_#{id}"
        $(".settingsContainer").hide()
        $("#settings_#{id}").show()
        if id in [4,5,6]
          @updatePageNumberAndIndicators(0)

      pageName = ""
      switch id
        when 0 then pageName = "help"
        when 1 then pageName = "faq"
        when 2 then pageName = "redeem"
        when 3 then pageName = "account"
        when 4 then pageName = "tos"
        when 5 then pageName = "privacypolicy"

      if pageName isnt ""
        @ot.trackPageView("settings.#{pageName}.html")


    updateRedemptionTitle: ($el = null)->
      if $el is null
        $el = $("#playlist_product-0")
      title = $el.data("title")
      $(".redemptionTitle").html(Manifest.settings_added_to_library.replace("%TITLE%", title))

    itemFocusLeft: ->
      $el = $(TWC.MVC.Router.getFocus())
      return if !$el

      # Current focus on product
      if @focusIsOnProduct($el)
        focusedProductIndex = parseInt($el.attr("id").split("-")[1])

        # Focus on next playlist row if current rowindex less than 4
        if focusedProductIndex > 0
          # Focus on index of next playlist row if exists
          rowCnt = 1

          if $(".redemptionPlaylistContent .product:not('.disable'):eq(#{focusedProductIndex-rowCnt})").length
            $nextFocus = $(".redemptionPlaylistContent .product:not('.disable'):eq(#{focusedProductIndex-rowCnt})")

            #width of the item
            scrollWidth = $nextFocus.width() + parseInt($nextFocus.css("margin-right"))

            newFocussedIndex = focusedProductIndex - 1
            #if next item is not fully visible
            if newFocussedIndex > 4
              newPos = (scrollWidth * (newFocussedIndex - 6)) * -1
              $(".redemptionPlaylistContent").css({
                "-o-transform": "translate(" + newPos + "px, 0)"
                "-webkit-transform": "translate3d(" + newPos + "px, 0, 0)",
              })

            TWC.MVC.Router.setFocus($nextFocus)
            @updateRedemptionTitle($nextFocus)
          else
            @moveToMenu()
        else
          @moveToMenu()


    itemFocusRight: ->
      $el = $(TWC.MVC.Router.getFocus())
      return if !$el

      # Current focus on product
      if @focusIsOnProduct($el)
        focusedProductIndex = parseInt($el.attr("id").split("-")[1])

        #calculate the next focusable element, if on featured block, then skip 2, otherwise 3
        rowCnt = 1

        if $(".redemptionPlaylistContent .product:not('.disable'):eq(#{focusedProductIndex+rowCnt})").length
          $nextFocus = $(".redemptionPlaylistContent .product:not('.disable'):eq(#{focusedProductIndex+rowCnt})")

          #width of the item
          scrollWidth = $nextFocus.width() + parseInt($nextFocus.css("margin-right"))

          newFocussedIndex = focusedProductIndex + 1
          #if next item is not fully visible
          if newFocussedIndex > 5
            newPos = (scrollWidth * (newFocussedIndex - 6)) * -1
            $(".redemptionPlaylistContent").css({
              "-o-transform": "translate(" + newPos + "px, 0)",
              "-webkit-transform": "translate3d(" + newPos + "px, 0, 0)"
            })

          TWC.MVC.Router.setFocus($nextFocus)
          @updateRedemptionTitle($nextFocus)


    itemFocusUp: ->
      return if @singleBundleItem
      if @redemptionSuccessFocus?
        focus = @redemptionSuccessFocus
        @redemptionSuccessFocus = null
      else
        focus = "#playlist_product-0"

      TWC.MVC.Router.setFocus(focus)

    itemFocusDown: ->
      @redemptionSuccessFocus = TWC.MVC.Router.getFocus()
      TWC.MVC.Router.setFocus("#redemptionBackButton")

    openProduct: ->
      $el = $(TWC.MVC.Router.getFocus())
      productId  = $el.data("id")
      productType = $el.data("type")

      return if @singleBundleItem
      $(".playlistContent").removeClass("visibleProducts")
      TWC.SDK.Input.get().setBlockAll(true)
      setTimeout ()=>
        switch productType
          when 1
          # Product/episode
            page = 'productDetail-view'
          when 2, 4
          # Bundle/season
            page = 'bundleDetail-view'
          when 5
          # Series collection
            page =  'seasonlist-view'
          else
            page = 'productDetail-view'

        TWC.SDK.Input.get().setBlockAll(false)
        TWC.MVC.Router.load page,
          data: [productId]
      , 400


    focusIsOnProduct: ($focusedEl) ->
      $focusedEl.hasClass "product"


  settingsView.get()
