define [
  "framework",
  "jquery",
  "backbone",
  "i18n!../../../lang/nls/manifest",
  "application/api/cd/models/CDSearchProducts",
  "application/api/cd/collections/CDProductCollection",
  "application/screens/productoverview/productoverview",
  "application/models/baseModel",
], (TWC, $, Backbone, Manifest, CDSearchProductsModel, CDProductCollection, ProductOverview, BaseModel) ->

  emptySearchResults = BaseModel.extend
    defaults:
      manifest: Manifest
      Products: []

  class searchView extends ProductOverview
    uid: 'searchresults-zone'

    el: "#searchResults"

    type: "zone"

    external:
      html:
        url:"skin/templates/screens/search/searchresults.tpl"

    events:
      "enter    .product" : "showProductDetail"
      "swipeLeft .playlistContent"                                      : "prevPageNavAction"
      "swipeRight .playlistContent"                                     : "nextPageNavAction"

    currentPage: 1
    currentFirstProduct: 0
    totalProductsPerPage: 18
    renderedPages: [1]
    availablePages: [1,2]

    onload: (@searchKeyword = "", @contentLeft = 0)->
      # Create new model
      @productCollection = new CDProductCollection()
      @currentPage = 1
      @currentFirstProduct = 0

      if @searchKeyword.length >= 2
        @model = new CDSearchProductsModel({}, {
          keyword: @searchKeyword,
          pageNumber:@currentPage,
          pageSize:@totalProductsPerPage*2,  #request 2 page size for initial request to improve user experience
        })
      else
        @model = new emptySearchResults()
      @model.set("keyword", @searchKeyword)

    onRenderComplete: ->
      $("#mainMenu").show()

      $playlistContent = $(".playlistContent")
      $playlistContent.addClass("noAnimation")
      @contentLeft=0 if not @contentLeft?
      $playlistContent.css({
        "-o-transform": "translate(" + @contentLeft + "px, 0)",
        "-webkit-transform": "translate3d(" + @contentLeft + "px, 0, 0)"}
      )

      setTimeout ->
        $(".playlistContent").removeClass("noAnimation")
      , 1500

      $playlistContent.addClass("visibleProducts")
      productCollection = @model.get("Products")
      @productCollection.add(productCollection.models) #add models for page 1 and 2
      @totalPages = Math.ceil(@model.get("TotalProducts") / @totalProductsPerPage)
      @displayPageNavigation()


    #focus events
    focusFirst: ->
      if $("#playlist_product-0").length
        @currentFirstProduct = 0
        for e in $(".playlistContent .product")
          if $(e).position().left + @contentLeft > 0
            TWC.MVC.Router.setActive "searchresults-zone"
            TWC.MVC.Router.setFocus "#" + $(e).attr("id")
            return

    focusDown: ->
      @parent.focusKeyboard?()

    focusLeft: ->
      $el = $(TWC.MVC.Router.getFocus())

      return if !$el

      # Current focus on product
      if @focusIsOnProduct($el)
        focusedProductIndex = parseInt($el.attr("id").split("-")[1])

        # Focus on next playlist row if current rowindex less than 4
        if focusedProductIndex > 0
          # Focus on index of next playlist row if exists
          rowCnt = 1

          if $(".playlistContent .product:not('.disable'):eq(#{focusedProductIndex-rowCnt})").length
            $nextFocus = $(".playlistContent .product:not('.disable'):eq(#{focusedProductIndex-rowCnt})")

            #width of the container
            containerWidth = $("#viewPort").width()
            #end X position of the next element
            nextStartXPos = $nextFocus.position().left + @contentLeft

            #if next item is not fully visible
            if nextStartXPos < 0
              scrollWidth = ($nextFocus.width() + parseInt($nextFocus.css("margin-right"))) *6
              @contentLeft += scrollWidth
              $(".playlistContent").css({
                "-o-transform": "translate(" + @contentLeft + "px, 0)"
                "-webkit-transform": "translate3d(" + @contentLeft + "px, 0, 0)",
              })
              @currentFirstProduct-=6
              @displayPageNavigation()

            TWC.MVC.Router.setFocus($nextFocus)
          else
            @focusMenu()
        else
          @focusMenu()


    focusRight: ->
      $el = $(TWC.MVC.Router.getFocus())
      return if !$el

      # Current focus on product
      if @focusIsOnProduct($el)
        focusedProductIndex = parseInt($el.attr("id").split("-")[1])
        #calculate the next focusable element, if on featured block, then skip 2, otherwise 3
        rowCnt = 1

        if $(".playlistContent .product:not('.disable'):eq(#{focusedProductIndex+rowCnt})").length
          $nextFocus = $(".playlistContent .product:not('.disable'):eq(#{focusedProductIndex+rowCnt})")

          #width of the container
          containerWidth = $("#content").width()
          #end X position of the next element
          nextEndXPos = $nextFocus.position().left + $nextFocus.width() + @contentLeft

          #if next item is not fully visible
          if nextEndXPos > containerWidth
            scrollWidth = ($nextFocus.width() + parseInt($nextFocus.css("margin-right"))) *6
            @contentLeft -= scrollWidth
            $(".playlistContent").css({
              "-o-transform": "translate(" + @contentLeft + "px, 0)",
              "-webkit-transform": "translate3d(" + @contentLeft + "px, 0, 0)"
            })
            @currentFirstProduct+=6
            @displayPageNavigation()

          TWC.MVC.Router.setFocus($nextFocus)
          if (@currentFirstProduct % (@totalProductsPerPage - @totalProductsPerPage/3) == 0) and @currentPage < @totalPages
            @currentPage++
            @renderPage("el", "right")

    # Transform product collection to HTML
    getPageContent: (productCollection) ->
      pageHtml = []
      pCollection = productCollection.slice(0, Math.min(productCollection.length, @totalProductsPerPage))
      i = (@currentPage-1) * @totalProductsPerPage
      _.each pCollection, (product) ->
        pageHtml.push(
          "<div id=\"playlist_product-#{i}\" class=\"product\" data-left=\"focusLeft\" data-right=\"focusRight\" data-up=\"focusUp\" data-down=\"focusDown\" data-id=\"#{product.get("Id")}\" data-type=\"#{product.get("StructureType")}\">
                      <span class=\"productTitle\">#{product.get('Name')}</span>
                      <div class=\"productBorder\"></div>
                      <img src=\"#{product.get("ThumbnailUrl")}\" alt=\"#{product.get("Name")}\" />
                      </div>")
        i++
      pageHtml.join("")

    renderPage: ($el, dir) ->
      if @currentPage not in @availablePages
        if dir is "right"
          @currentPage++
        else
          @currentPage--

      #animate to page
      else
        #pre-load next page instead of current page
        nextPage = @currentPage + 1
        if (nextPage) <= @totalPages and (nextPage) not in @availablePages
          searchResultsModel = new CDSearchProductsModel({}, {
            keyword: @searchKeyword,
            pageNumber:nextPage,
            pageSize:@totalProductsPerPage,
          })

          searchResultsModel.fetch
            success: (collection, data, xhr)=>
              productCollection = collection.get("Products")
              result = @productCollection.add(productCollection.models)
              @availablePages.push(nextPage)

      updatedProductsCollection = new CDProductCollection(@productCollection.slice(@currentPage*@totalProductsPerPage-@totalProductsPerPage, @currentPage*@totalProductsPerPage))


      # Set html content
      if dir is "right"
        if @currentPage not in @renderedPages
          # Generate html new content
          pageContent = @getPageContent(updatedProductsCollection.models)

          @renderedPages.push(@currentPage)
          $(".playlistContent").append(pageContent)


    focusMenu: ->
      @parent.moveToMainMenu?()


    showProductDetail: ->
      $el = $("#{TWC.MVC.Router.getFocus()}")
      productId = $el.data("id")
      productType = $el.data("type")

      switch productType
        when 1
        # Product/episode
          page = 'productDetail-view'
        when 2, 4
        # Bundle/season
          page = 'bundleDetail-view'
        else
          page = 'productDetail-view'

      @updateHistory(@searchKeyword, @contentLeft)
      @parent.updateHistory(@searchKeyword, @contentLeft)
      TWC.MVC.Router.load page,
        data: [productId]

    # Action triggered by click/touch
    prevPageNavAction: ->
      # Set focus index manually to first product on the current page
      TWC.MVC.Router.setFocus $(".playlistContent .product:not('.disable'):eq(#{@currentFirstProduct})")

      @focusLeft(->
        TWC.MVC.Router.unFocus()
      )

    # Action triggered by click/touch
    nextPageNavAction: ->
      # Set focus manually to last product on the current page
      TWC.MVC.Router.setFocus $(".playlistContent .product:not('.disable'):eq(#{@currentFirstProduct+5})")

      @focusRight(->
        TWC.MVC.Router.unFocus()
      )

    # Show/hide page navigation
    displayPageNavigation: ()->
      if @currentFirstProduct==0
        $("#navigationLeft").hide();
      else
        $("#navigationLeft").show();

      if @model.get("TotalProducts") > @totalProductsPerPage/3
        $("#navigationRight").show();

      if !@model.get("TotalProducts")? or @model.get("TotalProducts") - @currentFirstProduct < @totalProductsPerPage/3
        $("#navigationRight").hide();


  searchView.get()
