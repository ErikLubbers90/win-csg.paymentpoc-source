define [
  "framework",
  "jquery",
  "backbone",
  "config",
  "i18n!../../../lang/nls/manifest",
  "application/helpers/submenuHelper",
  "application/helpers/backgroundHelper",
  "application/api/cd/models/CDProduct",
  "application/screens/search/searchresults"
  "../../models/analytics/omnituretracker",
  "application/models/baseModel"
], (TWC, $, Backbone, Config, Manifest, SubMenuHelper, BackgroundHelper, CDProductModel, SearchResults, OmnitureTracker, BaseModel) ->

  searchModel = BaseModel.extend
    defaults:
      "manifest": Manifest


  class searchView extends TWC.MVC.View
    uid: 'search-view'

    el: "#screen"

    type: "page"

    external:
      html:
        url:"skin/templates/screens/search/search.tpl"

    model: new searchModel()

    events:
      "enter    .product"                                               : "showProductDetail"
      "enter    #navigationLeft"                                        : "prevPageNavAction"
      "enter    #navigationRight"                                       : "nextPageNavAction"

    zones: [
      {uid:"menu-zone", data:[]},
      {uid:"searchresults-zone", data:[""]},
    ]

    onload: (@keyword="", @contentLeft=0)->

    template:(html, data) ->
      return _.template(html)({data:data, SubMenuTitle: "Search", manifest:Manifest})

    onRenderComplete: ->
      BackgroundHelper.setBackgroundImage("./skin/#{Config.resolution[1]}/media/theme/placeholder.jpg", "./skin/#{Config.resolution[1]}/media/theme/placeholder.jpg", false, 0, true)
      $("#mainMenu").show()
      $firstLoad = true
      SubMenuHelper.setSelectedMenuItem("search")
      if @keyword.length > 0
        $("#searchInput").val(@keyword)
        return if @keyword.length < 3
        @zones[1].data = [@keyword, @contentLeft]

      $("#searchInput").on "keydown", (e)=>
        key = e.keyCode
        val = $("#searchInput").val()
        # only allow: if length is <25: letters, numbers space. always: backspace, delete, keyleft, keyright
        return (((key>=65 && key<=90) || (key>=48 && key<=57 && !e.shiftKey) || key==32) && val.length<25) || key ==8 || key == 46 || key == 37 || key ==39

      $("#searchInput").on "keyup", (e)=>
        val = $("#searchInput").val()
        val = val.replace(/[^\w\s]/gi, "")
        if val.length > 25
          val = val.substring(0, 25)
        $("#searchInput").val(val);
        if @keypressTimeout?
          clearTimeout(@keypressTimeout)
          @keypressTimeout = null
        @keypressTimeout = setTimeout ()=>
          @keypressTimeout = null
          val = $("#searchInput").val()
          if val.length>25
            val=val.substring(0,25)
          @valueIsChanged(val)
        , 1000

      SubMenuHelper.setSubMenuContext(SubMenuHelper.CONTEXT_SEARCH)

    onZoneRenderComplete: ->
      if @firstLoad
        @firstLoad = false
        TWC.MVC.Router.setActive "keyboard-zone"
        TWC.MVC.Router.setFocus "character_0"
      ot = OmnitureTracker.get()
      ot.trackPageView("search.html")


    #search events
    valueIsChanged: (@keyword)->
      return if @keyword.length < 3
      @zones[1].data = [@keyword, @contentLeft = 0]
      @refreshZone(1)


    onunload: ->
      @keyword = ""
      @zones[1].data = [@keyword, @contentLeft = 0]
      @refreshZone(1)

      super


    #focus events

    focusUpFromKeyboard: ->
      console.log "focus up from keyboard"
      TWC.MVC.Router.find("searchresults-zone").focusFirst?()

    focusLeftFromKeyboard: ->
      @moveToMainMenu()

    focusKeyboard: ->
#      @moveToContent()

    moveToContent: ->
#      TWC.MVC.Router.setActive "keyboard-zone"
#      TWC.MVC.Router.setFocus "#character_0"

    moveToMainMenu: ->
      TWC.MVC.Router.setActive "menu-zone"
      TWC.MVC.Router.setFocus "#search"

    # Action triggered by click/touch
    prevPageNavAction: ->
      TWC.MVC.Router.find("searchresults-zone").prevPageNavAction?()

    # Action triggered by click/touch
    nextPageNavAction: ->
      TWC.MVC.Router.find("searchresults-zone").nextPageNavAction?()


  searchView.get()
