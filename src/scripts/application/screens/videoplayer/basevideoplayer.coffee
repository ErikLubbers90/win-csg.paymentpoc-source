define [
  "framework",
  "application/screens/videoplayer/togglecontrols",
  "application/popups/error/error",
  "application/popups/generic/alertWithOptions",
  "application/popups/generic/alert",
  "application/screens/videoplayer/leanback",
  "../../api/cd/models/CDProduct",
  "config",
  "i18n!../../../lang/nls/manifest",
  "i18n!../../../lang/nls/regionsettings",
  "application/api/extras/cpe",
  "./languageOverlay"
  "../../api/cd/CDBaseRequest",
  "../../api/authentication",
  "application/helpers/audiomanager",
  "application/helpers/audiomanagerHelper",
  "application/api/cd/CDPaymentManagement",
  "application/api/cd/CDSubscriberManagement",
  "application/models/analytics/omnituretracker",
  "application/models/analytics/akamai",
  "jquery"
], (TWC, ToggleControls, Error, AlertWithOptions, Alert, Leanback, CDProductModel, Config, Manifest, RegionSettings, CPE, LanguageOverlay, CDBaseRequest, Authentication, AudioManager, AudioManagerHelper, CDPaymentManagement, CDSubscriberManagement, OmnitureTracker, AkamaiTracker, $) ->

  class playerVideoplayerModel extends TWC.MVC.Model
    uid: 'player-videoplayer-model'
    data: {
      menu: []
    }

  class playerVideoplayerView extends TWC.MVC.View
    uid: 'videoplayer-view'
    el: '#screen'
    type: 'page'

    isTrailer: false
    startTime: 0

    chapters: []
    chaptersLoaded: false
    chaptersAvailable: false
    chapterOffset: 0
    chapterTimeout: null
    doubleClickTimer: null

    currentSeekerProgressTime: 0

    selectedLangaugeIso: null
    selectedSubtitleIso: null

    forcedTextTracksAvailable: false
    forcedTextTracks: []

    blockSubtitles: false


    leanbackEnabled: false
    autoplayEnalbed: false
    isDRM: false
    isRental: false

    started: false

    extrasEnabled: false
    messagesLoaded: false

    CPE: null
    playerId: null

    started: false

    model: new playerVideoplayerModel()
    defaultFocus: "#play"
    events:
      "_play"                 : "rc_play"
      "_pause"                : "rc_pause"
      "play_pause"            : "rc_play_pause"
      "stop"                  : "rc_stop"
      "forward"               : "rc_forward"
      "rewind"                : "rc_rewind"
      "fastforward"           : "rc_fastforward"
      "fastrewind"            : "rc_fastrewind"
      "cc"                    : "showSubtitleSettings"
      "enter #play"           : "togglePause"
      "enter #stop"           : "stop"
      "enter #forward"        : "forward"
      "enter #rewind"         : "rewind"
      "enter #subs"           : "showSubtitleSettings"
      "enter #audio"          : "showAudioSettings"
      "enter #extras"         : "toggleExtras"
      "enter #forwardChapter" : "forwardChapter"
      "enter #rewindChapter"  : "rewindChapter"
      "enter #close-button"   : "closeVideo"
      "enter #videoWrapper"   : "hideAutoplay"
      "enter #triviaVideo"    : "openTriviaVideo"
      "enter #playerBackButton" : "onReturn"
      "click .chapter-point" : "clickChapterPoint"
      "enter #chapter-thumb-container" : "goToSelectedChapter"
      "enter #leftPreviewButton"  : "previewButtonLeftAction"
      "enter #rightPreviewButton"  : "previewButtonRightAction"
      "enter #previewContinueButton" : "previewContinueAction"
      "enter #previewBackButton" : "stop"

    external:
      html:
        url:"skin/templates/screens/player/videoplayer.tpl"


    onload: (model, @isTrailer=false, @isExtra=false, @messages = [], leanbackVideos = [], leanbackPosition = 0, @trackingName = "", @episodeChaining = false, @resumeTime = 0, @extrasEnabled = false, @tenMinutePreview = false, @bonusVideo = false) ->

      @akamaiTracker = null
      @extraSubtitles = false
      @extraVideoPlayer = false
      @videoTitle = ""
      @videoYear = ""
      @productModel = model
      @leanbackVideos = leanbackVideos
      @leanbackPosition = leanbackPosition
      @audioTrackChannels = []
      @chaptersLoaded = false
      @chaptersAvailable = false
      @leanbackEnabled = false
      @zones = []
      @isDRM = false
      isRelatedMedia = false

      @ot = OmnitureTracker.get()

      log "resumeplaycallback - loading"

      entitledPricingPlan = @productModel.get("PricingPlans").findWhere({ Id : @productModel.get("EntitledPricingPlanId")})
      if entitledPricingPlan?
        @isRental = entitledPricingPlan.isRental()

      #retrieve pricingplans for purchase options
      if @tenMinutePreview
        @getPricingPlans()
        @videoTitle = @productModel.get("Name")
        @startTime = 0



      # Keep loading indicator active until player is ready and playing (or in case of error)
      $(TWC.MVC.View.eventbus).trigger('loading', [true, "keep"])
      if @isExtra
        @videoTitle = @productModel.Name
        @startTime = 0

        if leanbackVideos.length > 0
          @zones.push {uid:"leanback-zone", data:[leanbackVideos, leanbackPosition]}
          @leanbackEnabled = true

        @playerConfig = {}
        @playerConfig.url = model.MediaUrl
        @playerConfig.drm = null
        @playerConfig.mime = switch true
          when /.mp4/.test(@playerConfig.url) then "video/mp4"
          when /.ism/.test(@playerConfig.url) then "application/vnd.ms-sstr+xml"
          when /.m3u8/.test(@playerConfig.url) then "application/vnd.apple.mpegurl"
          when /.mpd/.test(@playerConfig.url) then "application/dash+xml"
          else "video/mp4"


      #only start playback in the onRenderComplete for the extras, because there is no API call, so DOM is not ready at this point
      else
        if leanbackVideos.length > 0
          @seasonEpisodes = leanbackVideos
          @autoplayVisible = false
          @autoplayEnabled = @verifyIfNextEpisodeAvailable(@seasonEpisodes, model)

        #use resumeTime after returning from an extras video
        if @resumeTime > 0
          startTime = 0
        else
          startTime = @productModel.get("ContentProgressSeconds")

        if @episodeChaining
          startTime = 0

        if @tenMinutePreview
          startTime = 0

        if @bonusVideo
          startTime = 0

        @showResumePopup startTime, (resumeTime) =>
          $(TWC.MVC.View.eventbus).trigger('loading', [true, "keep"])

          @productModel.getPlaybackInformation((error, model) =>
            if not error
              model.getMetaInformation (error, chapters, effectiveEndSeconds)=>
                if not error
                  @chapters = chapters
                  if @chapters.length > 0 and not @isTrailer
                    @chaptersAvailable = true
                    $("#forwardChapter").removeClass("disable")
                    $("#rewindChapter").removeClass("disable")
                  @videoTitle = @productModel.get("Name")
                  @videoYear = @productModel.get("ReleaseDate")
                  @startTime = parseInt(model.get("ProgressSeconds")) or 0

                  if @resumeTime > 0
                    @startTime = 0

                  #disable starting time if in sequential episode chain
                  @startTime = 0 if @episodeChaining

                  @effectiveEndSeconds = effectiveEndSeconds

                  playerConfig = {}
                  playerConfig.url = model.get("ContentUrl")
                  playerConfig.mime = switch true
                    when /.mp4/.test(playerConfig.url) then "video/mp4"
                    when /.ism/.test(playerConfig.url) then "application/vnd.ms-sstr+xml"
                    when /.m3u8/.test(playerConfig.url) then "application/vnd.apple.mpegurl"
                    when /.mpd/.test(playerConfig.url) then "application/dash+xml"
                    else "video/mp4"

                  if not @isTrailer
                    playerConfig.startTime = resumeTime

                  #use resumeTime if available
                  if @resumeTime > 0
                    playerConfig.startTime = @resumeTime


                  if model.get("LicenseRequestToken")
                    @isDRM = true
                    type = "playready"
                    if window.MSMediaKeys.isTypeSupported("com.microsoft.playready.hardware", "")
                        type = "playreadyhardware"
                    playerConfig.drm = {
                      type: type
                      parameters: {
                        playReadyInitiatorUrl: model.get("LicenceRequestUrl")
                        customData: model.get("LicenseRequestToken")
                        webInitiatorUrl:"http://test.sony.twcapps.com/webinitiator.php"
                      }
                    }

                  @startPlayback(playerConfig, model, false)
                else
                  $(TWC.MVC.View.eventbus).trigger('loading', [false, "reset"])
                  messageAlert = new Alert()
                  if error.Message?
                    messageAlert.load
                      data: [Manifest.popup_error_title, error.Message, Manifest.popup_exit_close, => TWC.MVC.History.popState()]

            else
              $(TWC.MVC.View.eventbus).trigger('loading', [false, "reset"])
              messageAlert = new Alert()
              if error.Message?
                messageAlert.load
                  data: [Manifest.popup_error_title, error.Message, Manifest.popup_exit_close, => TWC.MVC.History.popState()]

          , @isTrailer, isRelatedMedia, @tenMinutePreview, @bonusVideo, @leanbackPosition)



    onReturn: ->
      @sendNewProductId()
      if @akamaiTracker?
        @akamaiTracker.handlePlayEnd('Play.Stop.Detected')
      if @CPE
        @extrasEnabled = false
        @CPE.terminate()
        @CPE = null
        TWC.MVC.History.popState()
      else
        return TWC.MVC.History.popState()


    sendNewProductId: ->
      detailView = TWC.MVC.Router.find("productDetail-view")
      detailView.updateProductId?(@productModel.get("Id"))


    onRenderComplete: ->
      # Disable audio manager
      AudioManager.destroy()
      AudioManagerHelper.stopTimerInitAudioManager()


      if @tenMinutePreview
        $(".playerTimes").hide()
        $(".previewPlayerTimes").show()
        setTimeout () =>
          @updateToPreview()
        , 3000

      $("#mainMenu").hide()
      $("#backgroundContainer").addClass('vplayer')
      if @isExtra
        @startPlayback(@playerConfig, @productModel)

      if @leanbackEnabled
        $(".vpContainer").addClass("leanback")
      else
        $(".vpContainer").removeClass("leanback")

      @initDraggableProgressbar()

      #disable colored keys during playback
      log "onRenderComplete videoplayer"
      @subtitleOptionsArray = null
      @audioOptionsArray = null
      TWC.SDK.Input.get().addToBlocked("red", "green", "blue", "yellow")

      if @isTrailer
        $(".buttonControls").addClass("trailer")


      #Load and start the CPE
      if not @extraVideoPlayer and not @isRental

        # Do not show extras button for production env
        if not @isTrailer and not @isExtra and @productModel.hasExtras() and Config.environment is "development"
          #enable CPE
          @playerId = "player-div"
          @CPE = new CPE(@productModel.getExtrasId(), @productModel.get("Id"), "en")

          @CPE.initialize  =>
            setTimeout () =>
              if @CPE?
                @CPE.player_getMetadata (metadata) =>
                  @messagesLoaded = true
                  @messages = metadata
                  if @messages.length > 0
                    $("#extras").removeClass("disable")
            , 5500

            #fallback if first call failed due to loading issues
            setTimeout () =>
              if @CPE? and @messages.length is 0
                @CPE.player_getMetadata (metadata) =>
                  @messagesLoaded = true
                  @messages = metadata
                  if @messages.length > 0
                    $("#extras").removeClass("disable")
            , 8500
          , true

      #No need to load CPE if originating from extra player
      else
        @checkForSubtitles() if @extraSubtitles
        if @messages? and @messages.length > 0
          $("#extras").removeClass("disable")
          $("#extras").addClass("active")

    startPlayback: (@playerConfig, model, fromExtras)->

      #load trivia directly if originating from extra player
      @loadTriviaMessages() if fromExtras

      @setSubscribes()
      @initAkamaiTracker()

      if Config.platform in ["core"]
        @playerConfig =
          url: "http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_30fps_normal.mp4"
          mime: "video/mp4"
      if @isTrailer or @isExtra
        @player = new TWC.SDK.Player
          src: @playerConfig.url,
          parent: '#vpPlayer',
          mime: @playerConfig.mime,
          width: Config.resolution[0],
          height: Config.resolution[1],
          drm: @playerConfig.drm,
          top:0,
          left:0,
          autoplay: true
      else
        #set default subtitle and audio track language if no preference is set
        storage = TWC.SDK.Storage.get()
        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_LANGUAGE) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_LANGUAGE, RegionSettings.subtitle_language)
        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_ACTIVE) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_ACTIVE, JSON.stringify(RegionSettings.subtitle_active))
        if storage.getItem(TWC.SDK.Player.STORAGE_AUDIO_LANGUAGE) is null then storage.setItem(TWC.SDK.Player.STORAGE_AUDIO_LANGUAGE, RegionSettings.audio_language)

        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_COLOR) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_COLOR, "#FFFFFF")
        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_FONT) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_FONT, "opensans")
        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_STYLE) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_STYLE, "1px 1px 1px #888888")
        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_SIZE) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_SIZE, "30")
        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_OPACITY) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_OPACITY, "1")

        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_BORDER_COLOR) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_BORDER_COLOR, "")
        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_BACKGROUND_COLOR) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_BACKGROUND_COLOR, "")

        subtitleOptions = {}
        subtitleOptions.language = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_LANGUAGE)
        subtitleOptions.active = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_ACTIVE)
        subtitleOptions.color = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_COLOR)
        subtitleOptions.font = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_FONT)
        subtitleOptions.style = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_STYLE)
        subtitleOptions.size = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_SIZE)
        subtitleOptions.opacity = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_OPACITY)
        subtitleOptions.borderColor = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_BORDER_COLOR)
        subtitleOptions.backgroundColor = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_BACKGROUND_COLOR)


        #Continue playback after extra content
        if @resumeTime? and @resumeTime > 0
          time = @resumeTime
        else
          time = @playerConfig.startTime or 0

        #test player settings for browser testing
        if Config.platform in ["core"]
          @player = new TWC.SDK.Player
            src: "http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_30fps_normal.mp4",
            parent: '#vpPlayer',
            mime: "video/mp4",
            width: Config.resolution[0],
            height: Config.resolution[1],
            top:0,
            left:0,
            time: time,
            autoplay: true,
            supportLanguage: true,
            supportSubtitles: true,

        else
          #platform player settings with drm active for media playback
          @player = new TWC.SDK.Player
            src: @playerConfig.url,
            parent: '#vpPlayer',
            mime: @playerConfig.mime,
            drm: @playerConfig.drm,
            width: Config.resolution[0],
            height: Config.resolution[1],
            top:0,
            left:0,
            time: time,
            autoplay: true,
            supportLanguage: true,
            supportSubtitles: true,

        # verify if external subtitles are available
        model.getExternalSubtitles (error, result) =>

          @parseType = false
          if result and result.length > 0

            textTracks = []
            @forcedTextTracks = []


            #filter out forced subs
            for textTrack,index in result
              #keep tags for the forced subtitles
              if textTrack.forced is true
                @forcedTextTracksAvailable = true
                @forcedTextTracks.push {"language":textTrack.language, "index": index, "url": textTrack.url, "forced": textTrack.forced }

              textTracks.push {"language":textTrack.language, "url": textTrack.url, "index": index, "forced": false }


            parserType = textTrack.url
            parserType = parserType.substring(parserType.lastIndexOf(".")+1);

            parserType = "ttml" if parserType in ["dfxp", "xml", "txt", "itt"]
            @parseType = parserType

            # use html5 renderer
            if Config.platform in ["core", "samsung", "lg", "webos", "tizen", "sony", "androidtv", "windows10"]
              require [
                "vendor/subtitles/external_subtitles",
                "vendor/subtitles/external_subtitles_#{parserType}_parser"], (ExSubtitles, Parser)=>
                @player = _.extend(@player, ExSubtitles, Parser)
                @player.setExternalSubtitles(textTracks)
                @setSubtitleOptions(subtitleOptions)
                @verifyCaptionAndAudioSupport()


              # use native renderer
            else
              @player.setExternalSubtitles(textTracks)
              @parseType = false
              @verifyCaptionAndAudioSupport()



        @verifyCaptionAndAudioSupport()


      #events to execute when video player goes hidden or appears
      @player.suspendPlayCallback = ()=>
        #close all popups
        @languageOverlay.close() if @languageOverlay

        @sendUpdateContentProgressRequest(true)

      @player.resumePlayCallback = (callback) =>

        if @isDRM
          @getNewPlayerData(callback)
          if @player
            #compare times, if times are the same show resume playback popup. If user has continued playback, do nothing
            time1 = @player.currentTime() if @player.currentTime()
            @checkTimeTimeout = setTimeout () =>
              time2 = @player.currentTime()
              if time1 is time2
                #playback is paused, show popup to attend user to continue
                @showResumePlaybackAlert(time1)
                @toggle.autoHide()
                clearTimeout(@checkTimeTimeout)
            , 4000

        else
          callback?()

      @onPlayerInitialised()


    showResumePlaybackAlert: (time)=>
      resumePlaybackAlert = new AlertWithOptions()
      resumePlaybackAlert.leftButtonAction = =>
        resumePlaybackAlert.close ()=>
          @play()
          setTimeout ()=>
            @jumpTo(time)
          , 100
      resumePlaybackAlert.rightButtonAction = =>
        resumePlaybackAlert.close ()=>
          @stop()
      resumePlaybackAlert.load
        data: [Manifest.resume_welcome_back, Manifest.resume_what_do, Manifest.resume_return_title_page, Manifest.resume_continue_playback, "#leftButton"]



    #get new url and drm to resume playback after suspend
    getNewPlayerData: (callback) =>
      model = @productModel
      CDBaseRequest.sessionId = null
      CDSubscriberManagement = require("application/api/cd/CDSubscriberManagement");
      isRelatedMedia = false

      getPlaybackInfoAndSetIt = ()=>
        @productModel.getPlaybackInformation((error, model) =>
          if not error
            sourceUrl = model.get("ContentUrl")
            if model.get("LicenseRequestToken")
              type = "playready"
              if window.MSMediaKeys.isTypeSupported("com.microsoft.playready.hardware", "")
                  type = "playreadyhardware"
              drmObject = {
                type: type
                parameters: {
                  playReadyInitiatorUrl: model.get("LicenceRequestUrl")
                  customData: model.get("LicenseRequestToken")
                }
              }
            callback(sourceUrl, drmObject)

          else
            callback(false)
        , @isTrailer, isRelatedMedia, @tenMinutePreview, @bonusVideo, @leanbackPosition)


      if Authentication.isLoggedIn()
        CDSubscriberManagement.createSession (err, data) =>
          getPlaybackInfoAndSetIt()
      else
        getPlaybackInfoAndSetIt()

    onPlayerInitialised: ->
      $("body").addClass("nobackground")
      log "onPlayerInitialised"
      #settings after player is initialised
      if not @isTrailer and not @isExtra and not @tenMinutePreview
        #start content update progress calls to the api
        @startUpdateContentProgressRequest()

      #set video title in header
      $(".title").text(@videoTitle)
      $(".year").text(@videoYear)

      #activate the toggleControls
      @toggle = new ToggleControls
        parent: "videoplayer-view"
        vpControl: $('.vpContainer')
        vpHeader: $("#header")

      TWC.MVC.Router.setActive @uid
      TWC.MVC.Router.setFocus "#play"
      $(TWC.MVC.View.eventbus).trigger('loading', [false, "reset"])

      #if extras enabled on load
      if @extrasEnabled
        $("#extras").addClass("active")
        if @messages? and @messages.length > 0
          $("#extras").removeClass("disable")
          @initTriviaMessages()

    setSubtitleOptions: (options) ->
      #set character styling
      $("#subtitleContainer .background .subtitleContent").css("color", options.color) if options.color?
      $("#subtitleContainer .background .subtitleContent").css("font-family", options.font) if options.font?
      $("#subtitleContainer .background .subtitleContent").css("opacity", options.opacity) if options.opacity?
      $("#subtitleContainer .background .subtitleContent").css("font-size", "#{options.size}px") if options.size?
      $("#subtitleContainer .background .subtitleContent").css("text-shadow", options.style) if options.style?

      #set background styling, exception for border due to css settings needed if nothing is defined.
      if options.borderColor?
        if options.borderColor is "undefined" then borderColorValue = "rgba(0,0,0,0)" else borderColorValue = options.borderColor
        $("#subtitleContainer .background").css("border-color", borderColorValue) if options.borderColor?

      $("#subtitleContainer .background").css("background-color", options.backgroundColor)


    verifyIfNextEpisodeAvailable: (seasonEpisodes, currentEpisode)->
      @nextEpisode = null
      for i in [0...seasonEpisodes.length]
        @nextEpisode = seasonEpisodes[i+1] if seasonEpisodes[i].get("Id") is currentEpisode.get("Id") and seasonEpisodes[i+1]?
      return false if not @nextEpisode?
      return @nextEpisode.get("EntitledPricingPlanId") isnt 0


    #preview display
    updateToPreview: ->
      @previewStep = 1
      @previewPricingPlan = null
      leftButtonText = null
      rightButtonText = null
      if @buyUHDPricingPlan?
        leftButtonText = Manifest.ten_minute_preview_buy.replace("{price}", @buyUHDPricingPlan.getPrice())
      if @rentUHDPricingPlan?
        rightButtonText = Manifest.ten_minute_preview_rent.replace("{price}", @rentUHDPricingPlan.getPrice())
      purchasePreviewText = Manifest.ten_minute_preview_to_continue_watching.replace("{title}", @videoTitle)
      $("#purchasePreviewProduct").html(purchasePreviewText)
      if leftButtonText?
        $("#leftPreviewButton").html(leftButtonText)
      else
        $("#leftPreviewButton").hide()
      if rightButtonText?
        $("#rightPreviewButton").html(rightButtonText)
      else
        $("#rightPreviewButton").hide()
      $(".purchaseOptions").show()
      $("#purchasePreviewProduct").show()


    showTenMinutePreviewOptions: ->
      @updateToPreview()

    hideTenMinutePreviewOptions: ->
      @updateToPreview()
      $(".purchaseOptions").hide()
      $("#purchasePreviewProduct").hide()


    updateToConfirmPurchase: ->
      @previewStep = 2
      purchasePreviewText = Manifest.ten_minute_preview_please_confirm.replace("{title}", @videoTitle).replace("{price}", @buyUHDPricingPlan.getPrice())
      $("#purchasePreviewProduct").html(purchasePreviewText)
      $("#leftPreviewButton").show()
      $("#rightPreviewButton").show()
      $("#leftPreviewButton").html(Manifest.ten_minute_preview_confirm)
      $("#rightPreviewButton").html(Manifest.ten_minute_preview_cancel)
      @previewPricingPlan = @buyUHDPricingPlan
      #pause the video if user continues with payment
      if $("#play").hasClass('pause')
        @previewPlayState = "playing"
        @pause()
      else
        @previewPlayState = "paused"


    updateToConfirmRental: ->
      @previewStep = 2
      purchasePreviewText = Manifest.ten_minute_preview_please_confirm_rental.replace("{title}", @videoTitle).replace("{price}", @rentUHDPricingPlan.getPrice())
      $("#leftPreviewButton").show()
      $("#rightPreviewButton").show()
      $("#purchasePreviewProduct").html(purchasePreviewText)
      $("#leftPreviewButton").html(Manifest.ten_minute_preview_confirm)
      $("#rightPreviewButton").html(Manifest.ten_minute_preview_cancel)
      @previewPricingPlan = @rentUHDPricingPlan

    previewButtonLeftAction: ->
      if @previewStep is 1
        if Authentication.isLoggedIn()
          return @updateToConfirmPurchase()   #update content in preview bar
        else
          @loginToConfirmPayment(false)
      if @previewStep is 2
        return @purchaseProduct(false, null, null, @previewPricingPlan)

    previewButtonRightAction: ->
      if @previewStep is 1
        if Authentication.isLoggedIn()
          return @updateToConfirmRental()    #update content in preview bar
        else
          @loginToConfirmPayment(true)
      if @previewStep is 2
        @play() if @previewPlayState is "playing"
        return @updateToPreview()

    loginToConfirmPayment: (isRental)->
      currentTime = @player.currentTime()
      historySize = TWC.MVC.History.size()
      TWC.MVC.Router.load "register-view",
        callback: ()=>
          @ot.trackPageView('purchaselogin.html')
        data: [
          ()=>
            @productModel.refresh =>
              productIsOwned = false
              entitledPricingPlan = @productModel.get("PricingPlans").findWhere({ Id : @productModel.get("EntitledPricingPlanId")})
              if entitledPricingPlan? then productIsOwned = true
              TWC.MVC.Router.load "videoplayer-view",
                data: [
                  @productModel,
                  false,
                  false,
                  [],
                  [],
                  0,
                  @trackingName,
                  @episodeChaining,
                  currentTime,
                  @extrasEnabled,
                  @tenMinutePreview and not productIsOwned,
                  @bonusVideo
                ],
                callback: ()=>
                  TWC.MVC.History.reset(historySize - 1)
                  if not productIsOwned
                    setTimeout ()=>
                      if isRental then @updateToConfirmRental() else @updateToConfirmPurchase()
                      TWC.MVC.Router.setFocus("#rightPreviewButton")
                    , 3500
                  else
                    messageAlert = new Alert()
                    $(TWC.MVC.View.eventbus).trigger("loading", [false])
                    messageAlert.load
                      data: ["", Manifest.payment_already_purchased, Manifest.ok, =>
                        @play()
                        TWC.MVC.Router.setFocus "#play"
                      ]
                      callback: ()=>
                        setTimeout ()=>
                          TWC.MVC.Router.setFocus "#leftButton"
                        , 2000
        ]

    getPricingPlans: ->

      @buyUHDPricingPlan = @productModel.get("OrderPricingPlans").find (pricingPlan)->
        pricingPlan.equalsType("BUYHDR")
      @rentUHDPricingPlan = @productModel.get("OrderPricingPlans").find (pricingPlan)->
        pricingPlan.equalsType("RENTHDR")

      if not @buyUHDPricingPlan?
        @buyUHDPricingPlan = @productModel.get("OrderPricingPlans").find (pricingPlan)->
          pricingPlan.equalsType("BUYUHD")

      if not @rentUHDPricingPlan?
        @rentUHDPricingPlan = @productModel.get("OrderPricingPlans").find (pricingPlan)->
          pricingPlan.equalsType("RENTUHD")






    purchaseProduct: (upgrade = false, purchaseCallback=null, purchaseFailCallback = null, selectedPricingPlan = null) =>

      #check if product is an upgrade or not
      entitledPricingPlan = @productModel.get("PricingPlans").findWhere({ Id : @model.get("EntitledPricingPlanId")})
      pricingPlanIsRental = selectedPricingPlan.isRental()
      entitledQuality = if entitledPricingPlan? then entitledPricingPlan.getQuality() else null
      upgrade = true if entitledQuality is "SD" or entitledQuality is "HD"

      if Authentication.isLoggedIn()

        formattedTitle = @productModel.get("Name").toLowerCase().replace(/[^A-Za-z0-9]/g, "")
        if pricingPlanIsRental
          @ot.trackOutboundClickToBuy("ultraapp.com/confirmpurchase", "vod_#{formattedTitle}_button")
        else
          @ot.trackOutboundClickToBuy("ultraapp.com/confirmpurchase", "4kultra_#{formattedTitle}_button")

        $(TWC.MVC.View.eventbus).trigger("loading", [true])

        currency = RegionSettings.default_payment_instruments_currency
        CDPaymentManagement.retrieveWallet currency, (error, data)=>
          CDPaymentManagement.updatePaypalData data.PaymentInstruments, (paymentInstruments)=>
            data.PaymentInstruments = paymentInstruments
            if not error
              paymentInstruments = _.filter(data.PaymentInstruments, (paymentInstrument) -> paymentInstrument.Type in Config.supportedPaymentOptions )

              if paymentInstruments.length is 0
                messageAlert = new Alert()
                $(TWC.MVC.View.eventbus).trigger("loading", [false])
                messageAlert.load
                  data: ["Sorry", Manifest.ten_minute_preview_no_payment_instrument, Manifest.popup_exit_close, => @play()]

              else
                selectedPaymentInstrument = _.findWhere(paymentInstruments, {Default:true})
                orderPaymentInstrumentIds = [selectedPaymentInstrument.Id]
                pricingPlanId = selectedPricingPlan.get("Id")
                promotionCodes = ""
                selectedPrice = selectedPricingPlan.getPrice()

                if upgrade
                  CDPaymentManagement.submitUpgradeOrder(orderPaymentInstrumentIds, pricingPlanId, @productModel.get("Id"), @productModel.get("LockerItemId"), promotionCodes, (error, response) =>
                    CDSubscriberManagement.degradeDeviceSession =>
                      if not error
                        TWC.SDK.Input.get().addToBlocked("_play", "_pause", "play_pause", "stop", "forward", "rewind" ,"fastforward" ,"fastrewind", "cc", "return")
                        @hideTenMinutePreviewOptions()
                        $(TWC.MVC.View.eventbus).trigger("loading", false)
                        $("#purchaseSuccesfull .thumb").html("<img src='" + @productModel.get('ThumbnailUrl') + "'>")
                        $("#purchaseSuccesfull").show()
                        if pricingPlanIsRental
                          $("#purchaseSuccesfull .content p").html(Manifest.ten_minute_preview_thank_you_rental.replace("{title}", @productModel.get("Name")).replace("{price}", selectedPrice))
                        else
                          $("#purchaseSuccesfull .content p").html(Manifest.ten_minute_preview_thank_you_purchase.replace("{title}", @productModel.get("Name")).replace("{price}", selectedPrice))
                        TWC.MVC.Router.setFocus("#previewContinueButton")
                      else
                        message = CDPaymentManagement.orderErrors(error)
                        errorAlert = new Error()
                        errorAlert.load
                          data: [message]

                      $(TWC.MVC.View.eventbus).trigger("loading", false)
                  )
                else
                  CDPaymentManagement.submitOrder(orderPaymentInstrumentIds, pricingPlanId, [@productModel.get("Id")], promotionCodes, (error, response) =>
                    CDSubscriberManagement.degradeDeviceSession =>
                      if not error
                        TWC.SDK.Input.get().addToBlocked("_play", "_pause", "play_pause", "stop", "forward", "rewind" ,"fastforward" ,"fastrewind", "cc", "return")
                        @hideTenMinutePreviewOptions()
                        $(TWC.MVC.View.eventbus).trigger("loading", false)
                        $("#purchaseSuccesfull .thumb").html("<img src='" + @productModel.get('ThumbnailUrl') + "'>")
                        $("#purchaseSuccesfull").show()
                        if pricingPlanIsRental
                          $("#purchaseSuccesfull .content p").html(Manifest.ten_minute_preview_thank_you_rental.replace("{title}", @productModel.get("Name")).replace("{price}", selectedPrice))
                        else
                          $("#purchaseSuccesfull .content p").html(Manifest.ten_minute_preview_thank_you_purchase.replace("{title}", @productModel.get("Name")).replace("{price}", selectedPrice))
                        TWC.MVC.Router.setFocus("#previewContinueButton")
                      else
                        message = CDPaymentManagement.orderErrors(error)
                        errorAlert = new Error()
                        errorAlert.load
                          data: [message]

                      $(TWC.MVC.View.eventbus).trigger("loading", false)
                  )


    previewContinueAction: ->
      TWC.SDK.Input.get().removeFromBlocked("_play", "_pause", "play_pause", "stop", "forward", "fastforward" ,"fastrewind", "cc", "return")
      $("#purchaseSuccesfull").hide()

      formattedTitle = @productModel.get("Name").toLowerCase().replace(/[^A-Za-z0-9]/g, "")
      resumeTime = @currentTime

      #refresh model and load the player again with the recently purchased content
      @productModel.refresh =>
        TWC.MVC.Router.load 'videoplayer-view',
          data: [@productModel, false, false, [], [], 0, "#{formattedTitle}_movie", false, resumeTime, false, false]
          callback: ()->
            #remove all history till the product detail page
            TWC.MVC.History.reset(TWC.MVC.History.size()-2)



    toggleCC: ->
      #check if subtitle are active and (de)activate it plus the button
      state = @player.subtitleActive()
      if state is true
        #disable subtitles
        $("#cc").removeClass("active")
        @player.subtitleActive(false)
      else
        #enable subtitles
        $("#cc").addClass("active")
        @player.subtitleTrack("cc")
        @player.subtitleActive(true)


    toggleExtras: ()->
      if @extrasEnabled
        @extrasEnabled = false
        $("#extras").removeClass("active")
        $("#triviaContainer .content").removeAttr("id")
        $("#triviaContainer").hide().find(".message").empty()
      else
        @extrasEnabled = true
        $("#extras").addClass("active")
        if @messages? and @messages.length > 0
          @initTriviaMessages()


    loadTriviaMessages: ()->
      return if @isTrailer or @isExtra or @tenMinutePreview
      if @CPE? and @playerId?
        @extrasEnabled = true
        $("#extras").addClass("active")
        @CPE.player_getMetadata (metadata)=>
          @messagesLoaded = true
          @messages = metadata
          if @messages? and @messages.length > 0
            $("#extras").removeClass("disable")
            @initTriviaMessages()



    verifyCaptionAndAudioSupport: ->
      audioTracks = @player.allAudioTracks() or []  #returns object {language, index}
      subtitleTracks = @player.allSubtitleTracks() or []

      if subtitleTracks.length > 0
        $("#subs").removeClass("disable")
      # Enable audio track switch button on development environment only
      if audioTracks.length > 1 and Config.environment is "development"
        $("#audio").removeClass("disable")


      if not @player.subtitleActive()
        @setForcedTextTrack() if @forcedTextTracksAvailable



    setForcedTextTrack: ->
      #if enabled and available forced text track is set when user disables subtitles (narrative)
      return if not Config.enableForcedSubtitle
      currentAudioTrack = @player.audioTrack()
      return if not currentAudioTrack

      for forcedTextTrack in @forcedTextTracks
        if forcedTextTrack.language is currentAudioTrack.language
          @player.subtitleTrack(forcedTextTrack, true)


    setVisiblePlayerButtonsWidth: ->
      width = 0
      $(".buttonControls .playerButton:visible").each (key, val) ->
        width += $(val).outerWidth() + parseInt($(val).css("margin-right"))
      # $(".buttonControls").width(width)

    showAudioSettings: ->
      @showLanguageSettings(true)

    showSubtitleSettings: ->
      return if @isTrailer
      @showLanguageSettings(false)


    showLanguageSettings: (audioSettings) ->


      @hideTenMinutePreviewOptions() if @tenMinutePreview

      if $("#play").hasClass('pause') then playState = "playing" else playState = "paused"
      @pause() if playState = "playing"

      @toggle.show() # temp. disable player control autohide

      audioTracks = []
      subtitleTracks = []
      audioActive = false
      subtitleActive = false

      subtitleOptions = {}
      storage = TWC.SDK.Storage.get()

      subtitleOptions.color = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_COLOR)
      subtitleOptions.font = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_FONT)
      subtitleOptions.style = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_STYLE)
      subtitleOptions.size = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_SIZE)
      subtitleOptions.opacity = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_OPACITY)
      subtitleOptions.borderColor = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_BORDER_COLOR)
      subtitleOptions.backgroundColor = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_BACKGROUND_COLOR)

      if audioSettings
        audioTracks = @player.allAudioTracks() or []  #returns object {language, index}
        audioActive = @player.audioTrack() or false
      else
        subtitleTracks = @player.allSubtitleTracks() or []
        #combine forced tracks to result
        for subtitle in subtitleTracks
          subtitle.forced = false
          if @forcedTextTracksAvailable
            for forcedSubtitle in @forcedTextTracks
              if forcedSubtitle.index is subtitle.index
                subtitle.forced = true

        subtitleActive = if @player.subtitleActive() then (@player.subtitleTrack() or false) else false

      @languageOverlay = new LanguageOverlay()
      @languageOverlay.saveAudioConfig = (audioObject, channels)=>
        if audioObject
          @player.audioTrack(audioObject)
        @verifyCaptionAndAudioSupport()

      @languageOverlay.saveSubtitleConfig = (subtitle)=>
        if subtitle
          @player.subtitleTrack(subtitle)
          @player.subtitleActive(true)
        else
          @player.subtitleActive(false)

        @verifyCaptionAndAudioSupport()

      @languageOverlay.saveSubtitleColorConfig = (color)=>
        TWC.SDK.Storage.get().setItem(TWC.SDK.Player.STORAGE_SUBTITLE_COLOR, color.color)

        #if Config.platform is "androidtv" then @player.setSubtitleColor(color.color)
        $("#subtitleContainer .background span").css("color",  color.color)
        #@player.subtitleActive(false)
        @verifyCaptionAndAudioSupport()


      @languageOverlay.saveSubtitleOptionsConfig = (options) =>

        #save character styling options
        TWC.SDK.Storage.get().setItem(TWC.SDK.Player.STORAGE_SUBTITLE_COLOR, options.color)
        TWC.SDK.Storage.get().setItem(TWC.SDK.Player.STORAGE_SUBTITLE_SIZE, options.size)
        TWC.SDK.Storage.get().setItem(TWC.SDK.Player.STORAGE_SUBTITLE_FONT, options.font)
        TWC.SDK.Storage.get().setItem(TWC.SDK.Player.STORAGE_SUBTITLE_OPACITY, options.opacity)
        TWC.SDK.Storage.get().setItem(TWC.SDK.Player.STORAGE_SUBTITLE_STYLE, options.style)

        #save background styling options
        TWC.SDK.Storage.get().setItem(TWC.SDK.Player.STORAGE_SUBTITLE_BORDER_COLOR, options.borderColor)
        TWC.SDK.Storage.get().setItem(TWC.SDK.Player.STORAGE_SUBTITLE_BACKGROUND_COLOR, options.backgroundColor)

        #set the options
        @setSubtitleOptions(options)


      @languageOverlay.onCompleted = ()=>
        @toggle.autoHide() # enable player control autohide
        @unselect()
        TWC.MVC.Router.setFocus if audioSettings then "#audio" else "#subs"

        @languageOverlay = null

        @showTenMinutePreviewOptions() if @tenMinutePreview

        #continue playback if playstate was playing
        if playState?
          setTimeout ()=>
            log("play movie!")
            @play() if playState = "playing"
          , 3000
      #add dts labels (channels)
      for audioTrack, index in audioTracks
        audioTrack.channels = @audioTrackChannels[index]

      if audioSettings
        @languageOverlay.load
          data: [audioSettings, audioTracks, audioActive, null]
      else
        @languageOverlay.load
          data: [audioSettings, subtitleTracks, subtitleActive, subtitleOptions, @parseType]


    startUpdateContentProgressRequest: ->
      @updateContentProgressInterval = setInterval =>
        @sendUpdateContentProgressRequest(false)
      , 60000 #one minute interval

    sendUpdateContentProgressRequest: (stopped) ->
      return if @isTrailer or @isExtra or @tenMinutePreview

      #add time parameter to updateContentProgressRequest
      if @player
        if @player.currentTime()
          time = Math.round(@player.currentTime())
      else
        time = @startTime
      @productModel.updateContentProgressRequest((error, result) =>
        log "updateContentProgressRequest - updated: ", error, result
      , time, stopped)


    isControlsHidden: ->
      if @toggle? then @toggle.options.isHidden else false

    setSubscribes: ->
      $(TWC.MVC.View.eventbus).on 'player.onWaiting.videoPlayer', (e)=>
        if !@updateVideoStillInterval
          $(TWC.MVC.View.eventbus).trigger('loading', true)

        if @akamaiTracker?
          @akamaiTracker.handleBufferStart()

      $(TWC.MVC.View.eventbus).on 'player.onInit.videoPlayer', (e)=>
        $(TWC.MVC.View.eventbus).trigger('loading', true)
        if @akamaiTracker?
          @akamaiTracker.handleSessionInit()

      $(TWC.MVC.View.eventbus).on "player.onReady.videoPlayer", (e) =>
        @player.aspectRatio("original")
        @verifyCaptionAndAudioSupport() if not @isTrailer and not @isExtra
        if @trackingName isnt ""
          @ot.trackVideo(@trackingName, 'start')
        if @akamaiTracker?
          if @player.getCurrentBitrate?() then bitrate = @player.getCurrentBitrate() else null
          @akamaiTracker.handleBitRateSwitch(bitrate) if bitrate

      $(TWC.MVC.View.eventbus).on 'player.onProgress.videoPlayer', (e, time)=>
        @updateTime(time)
        if @CPE? and @extrasEnabled
          @CPE.player_onProgress @playerId, time

      $(TWC.MVC.View.eventbus).on 'player.onPlay.videoPlayer', (e)=>
        @blockSubtitles = false
        if @updateVideoStillInterval
          return @pause()
        $(TWC.MVC.View.eventbus).trigger('loading', [false, "reset"])

        if $("#play").hasClass('pause') then playState = "playing" else playState = "paused"

        $('#play').addClass('pause')
        if @CPE? and @extrasEnabled
          @CPE.player_onPlayStateChange @playerId, true
        if @akamaiTracker?
          @akamaiTracker.handlePlaying()

      $(TWC.MVC.View.eventbus).on 'player.onPause.videoPlayer', (e)=>
        $(TWC.MVC.View.eventbus).trigger('loading', false)
        $('#play').removeClass('pause')
        if @CPE? and @extrasEnabled
          @CPE.player_onPlayStateChange @playerId, false
        if @akamaiTracker?
          @akamaiTracker.handlePause()

      $(TWC.MVC.View.eventbus).on 'player.onTrackInformationUpdated.videoPlayer', (e)=>
        @verifyCaptionAndAudioSupport() if not @isTrailer and not @isExtra

      # on video finnish correct focus and time display
      $(TWC.MVC.View.eventbus).on "player.onEnded.videoPlayer", =>
        log "videoplayer onended"

        if @tenMinutePreview
          @toggle.show()
          TWC.MVC.Router.setFocus("#leftPreviewButton")
          return

        clearInterval(@updateContentProgressInterval)

        @updateContentProgressInterval = null

        $($('#vpVideoTime span')[0]).html "00:00"
        $('#vpVideoDuration').html "00:00"
        $('#videoProgress').css("width", "0%")
        if @akamaiTracker?
          @akamaiTracker.handlePlayEnd('Play.End.Detected')

        @sendNewProductId()
        TWC.MVC.History.popState()

      $(TWC.MVC.View.eventbus).on "player.onStop.videoPlayer", =>
        clearInterval(@updateContentProgressInterval)
        @updateContentProgressInterval = null
        if @akamaiTracker?
          @akamaiTracker.handlePlayEnd('Play.Stop.Detected')

        @sendNewProductId()
        TWC.MVC.History.popState()

      # error handling
      $(TWC.MVC.View.eventbus).on 'player.onError.videoPlayer', (err) =>
        log "video error: ", "" + err
        clearInterval(@updateContentProgressInterval)
        if @player
          @pause()
        @updateContentProgressInterval = null
        $(TWC.MVC.View.eventbus).trigger('loading', [false, "reset"])
        $(TWC.MVC.View.eventbus).trigger 'error',
          type: "player"
          msg: err
        if @akamaiTracker?
          @akamaiTracker.handleError(err)

      # Handle resolution change
      $(TWC.MVC.View.eventbus).on 'player.onResolutionChanged.videoPlayer', (e, width, height) =>
        if !@activeVideoFormat
          @activeVideoFormat={}
        @activeVideoFormat.width=width;
        @activeVideoFormat.height=height;
        @activeVideoFormat.bitrate=0;
        hdrText = ""
        if not @isExtra and not @isTrailer

          hdrText = ""
          if @tenMinutePreview
            if @productModel.get("Previews").length
              platformUtils = TWC.SDK.Utils.get()
              for preview in @productModel.get("Previews").models
                if preview.get("Description")
                  if preview.get("Description")=="HDR" and platformUtils.isHDR()
                    hdrText = " +HDR"
                    break
                  else if preview.get("Description")=="SDR"
                    hdrText = ""
          else
            entitledPricingPlan = @productModel.get("PricingPlans").findWhere({ Id : @productModel.get("EntitledPricingPlanId")})
            platformUtils = TWC.SDK.Utils.get()
            if entitledPricingPlan.isHDR()
              if platformUtils.isHDR()
                hdrText = " +HDR"

        if height == 2160
            resolution = "4K UHD" + hdrText
        else if height == 1080
            resolution = "HD" + hdrText
        else if height == 720
            resolution = "HD"
        else if height == 480 or height == 360
            resolution = "SD"
        $(".quality").text(resolution)


        if @player.getCurrentBitrate?() then bitrate = @player.getCurrentBitrate() else null
        if @akamaiTracker?
          @akamaiTracker.handleBitRateSwitch(bitrate) if bitrate


      $(TWC.MVC.View.eventbus).on 'player.subtitleOff.videoPlayer', (e, subtitle) =>
        @processSubtitle(false, subtitle)

      $(TWC.MVC.View.eventbus).on 'player.subtitleOn.videoPlayer', (e, subtitle) =>
        @processSubtitle(true, subtitle)

      $(TWC.MVC.View.eventbus).on 'player.triviaMessageOff.videoPlayer', (e, message) =>
        @processTriviaMessage(false, message)

      $(TWC.MVC.View.eventbus).on 'player.triviaMessageOn.videoPlayer', (e, message) =>
        return if not @extrasEnabled
        @processTriviaMessage(true, message)


    processSubtitle: (visible, subtitle) ->
      return if @blockSubtitles
      subtitleText = subtitle?.text or ""

      #cannot hash certain ascii characters, need to remove them first
      rValidXMLChars = /[^\x09\x0A\x0D\x20-\xFF]/g
      hashSubtitleText = subtitleText.replace(rValidXMLChars, '')
      hash = btoa(hashSubtitleText)
      hash = hash.replace(/\=/g, "").replace(/\+/g, "").replace(/\//g, "")

      #show new subtitle
      if visible and subtitleText and hash
        $("#subtitleContainer").show()
        $("#subtitleContainer .background .subtitleContent").prepend("<p id=\"#{hash}\">#{subtitleText}</p>")
        $("#subtitleContainer .background").css("visibility", "visible")
      #remove existing subtitle
      else if not visible and subtitleText and hash
        $("#subtitleContainer .background .subtitleContent p##{hash}").remove()
        $("#subtitleContainer .background").css("visibility", "hidden")
      #disable all subtitles
      else
        $("#subtitleContainer").hide().find(".background .subtitleContent").empty()
        $("#subtitleContainer .background").css("visibility", "hidden")

    processTriviaMessage: (visible, message) ->
      #show new subtitle
      if visible
        log "TRIVIA show message"
        if message.contentType is "trivia" and message.description?
          $("#triviaContainer").show()
          $("#triviaContainer").removeClass("video").addClass("trivia")
          $("#triviaContainer .message").text(message.description)
        else if message.contentType in ["clip", "video", "videos", "deleted scene", "multiangle"] and message.title?
          $("#triviaContainer").show()
          $("#triviaContainer").removeClass("trivia").addClass("video")
          $("#triviaContainer .content").attr("id", "triviaVideo")
          $("#triviaContainer .content").attr("data-url", message.url)
          $("#triviaContainer .thumbnail").css("background-image", "url('#{message.thumbnail}')")
          $("#triviaContainer .message").html("<div class='videoType'>#{message.contentType}</div><div class='videoDescription'>#{message.title}</div><div class='watch'>Watch Now</div>")
          TWC.MVC.Router.setFocus "#triviaVideo"

      #remove existing subtitle
      else if not visible and message
        $("#triviaContainer .content").removeAttr("id")
        $("#triviaContainer").hide().find(".message").empty()
        TWC.MVC.Router.setFocus @lastFocused
      #disabled all subtitles
      else
        $("#triviaContainer .content").removeAttr("id")
        $("#triviaContainer").hide().find(".message").empty()

    initTriviaMessages: ->
      @messagesEnabled = true

      visibleMessages = []
      @triviaMessageActive = true
      $(TWC.SDK.Base.eventbus).off("._triviaMessages")
      $(TWC.SDK.Base.eventbus).trigger("player.triviaMessageOff")

      $(TWC.MVC.View.eventbus).on 'player.onProgress._triviaMessages.videoPlayer', (e, time)=>
        return if !@messagesEnabled

        #clean old visible messages
        for visibleMessage, index in visibleMessages
          if visibleMessage and parseFloat(time) >= visibleMessage.playbackOutTime
            $(TWC.SDK.Base.eventbus).trigger("player.triviaMessageOff", [visibleMessage])
            visibleMessages.shift()

        #check for new visible subtitle
        new_message = _.find @messages, (message) ->
          return (message.playbackInTime <= time <= message.playbackOutTime) and (message not in visibleMessages)

        if new_message
          visibleMessages.push new_message
          $(TWC.SDK.Base.eventbus).trigger "player.triviaMessageOn", [new_message]


    onunload: ->
      $("#backgroundContainer").removeClass('vplayer')
      log "onunload - videoplayer"



      #send a streaming complete flag and clear the updatecontentprogress interval
      @sendUpdateContentProgressRequest(true)


      $("#screen").removeAttr("style") if @autoplayTimer

      clearInterval(@updateContentProgressInterval)
      clearInterval(@progressSeekInterval)
      clearInterval(@autoplayTimer)
      clearTimeout(@checkTimeTimeout)

      @currentSeekerProgressTime = null
      @updateContentProgressInterval = null
      @progressSeekInterval = null
      @skipToSeekTime = null
      @autoplayTimer = null
      @CPE = null
      @playerId = null

      #unsubscribe from player waiting event
      $(TWC.MVC.View.eventbus).off('.videoPlayer')
      $(TWC.MVC.View.eventbus).trigger('loading', [false, "reset"])

      #force to clear toggle timeout
      clearTimeout(@toggle.options.timeoutId) if @toggle?

      #enable colored keys after exiting videoplayer
      TWC.SDK.Input.get().removeFromBlocked("red", "green", "blue", "yellow")

      #reset player and toggle
      @toggle?.destroy?()
      @player?.destroy?()
      @player = null

      #enable app background
      $("body").removeClass("nobackground")


      AudioManager.initialize()
      AudioManagerHelper.restartAudioManagerTimer()

    template:(html, data) ->
      return _.template(html)({data:data, manifest:Manifest})

    parseTime: (totalSec)->
      hours   = parseInt( totalSec / 3600 ) % 24
      minutes = parseInt( totalSec / 60 ) % 60
      seconds = parseInt( totalSec % 60, 10)
      result = (if hours is 0 then "00:" else if hours < 10 then "0" + hours + ":" else hours + ":") + (if minutes < 10 then "0" + minutes else minutes) + ":" + (if seconds  < 10 then "0" + seconds else seconds);

    parsePreviewTime: (totalSec)->
      hours   = parseInt( totalSec / 3600 ) % 24
      minutes = parseInt( totalSec / 60 ) % 60
      seconds = parseInt( totalSec % 60, 10)
      result = (if hours is 0 then "" else if hours < 10 then "0" + hours + ":" else hours + ":") + (if minutes < 10 then "0" + minutes else minutes) + ":" + (if seconds  < 10 then "0" + seconds else seconds);

    parseTriviaTime: (totalSec)->
      hours   = parseInt( totalSec / 3600 ) % 24
      minutes = parseInt( totalSec / 60 ) % 60
      seconds = parseInt( totalSec % 60, 10)
      result = (if hours is 0 then "" else if hours < 10 then "0" + hours + ":" else hours + ":") + (if minutes < 10 then "0" + minutes else minutes) + ":" + (if seconds  < 10 then "0" + seconds else seconds);

    showResumePopup: (startTime, callback) ->
      if startTime > 0 and (not @resumeTime? or @resumeTime is 0) and not @isTrailer
        $(TWC.MVC.View.eventbus).trigger('loading', [false, "reset"])

        #show popup
        title = "#{Manifest.resume_watching} #{@parseTime(startTime)} ?"

        resumePlayerAlert = new AlertWithOptions()
        resumePlayerAlert.leftButtonAction = ->
          resumePlayerAlert.close ()=>
            callback?(startTime)
        resumePlayerAlert.rightButtonAction = ->
          resumePlayerAlert.close ()=>
            callback?(0)
        resumePlayerAlert.load
          data: [title, "", Manifest.start_over, Manifest.resume, "#leftButton"]

        # Override default onreturn method
        resumePlayerAlert.onReturn = ->
          # Block return, users should choose one of the options
          resumePlayerAlert.close ()=>
            TWC.MVC.History.popState()

      else
        callback?(0)

    updateTime: (time) ->
      @currentTime = time

      @updateDebugText()
      duration = @player?.duration()

      if not @chaptersLoaded and duration > 0 and @chapters.length > 0
        @renderChapters(duration)

      if @autoplayEnabled and @effectiveEndSeconds > 0 and time > @effectiveEndSeconds and not @autoplayVisible
        @showAutoplay()

      $('#curTime').html @parseTime(time)
      $('#totalTime').html @parseTime(duration)
      $('#video-progress').css("width", (if (time / duration*100) > 100 then 100 else (time / duration*100)) + "%")
      $('#progress-dot').css("left", (if (time / duration*100) > 100 then 100 else (time / duration*100)) + "%")
      if @tenMinutePreview
        $("#remainingTime").html @parsePreviewTime(duration - time)



    showAutoplay: ->
      @autoplayVisible = true
      @player.width(1330)
      @player.height(745)
      @player.top(167)
      @player.left(18.5)
      $("#overlay").hide()
      $("#screen").css("background-image", "url('skin/" + Config.resolution[1] + "/media/videoplayer/autoplay_bg.png')")
      imgSrc = @nextEpisode.get("ThumbnailLandscape")
      description = if @nextEpisode.get("Description").length > 0 then @nextEpisode.get("Description").stub(850) else @nextEpisode.get("ShortDescription").stub(850)
      $autoPlayControls = $("<div class='autoplay-controls'></div>")
      $autoPlayControls.append("<div class='counter'>" + Manifest.beginning_in + "<span class='timer'>10</span> " + Manifest.seconds + "</div>")
      $autoPlayControls.append("<img src='" + imgSrc + "'/>")
      $autoPlayControls.append("<div class='autoplay-title'>#{@nextEpisode.get('Name')}</div>")
      $autoPlayControls.append("<div class='autoplay-subtitle'>Episode #{@nextEpisode.get('EpisodeNumber')}</div>")
      $autoPlayControls.append("<div class='autoplay-description'>#{description}</div>")
      $autoPlayControls.append("<div id='close-button' data-left='#videoWrapper' class='button'>" + Manifest.close + "</div>")
      $("#screen").append($autoPlayControls)
      $("#screen").append("<div id='videoWrapper' data-right='#close-button'>")
      TWC.MVC.Router.setFocus("#videoWrapper")
      @autoplayCnt = 10
      @autoplayTimer = setInterval ()=>
        if @autoplayCnt is 0
          clearInterval(@autoplayTimer)
          $("#screen").css("background", "none")
          $(".autoplay-controls").hide()
          TWC.MVC.Router.load "videoplayer-view",
            data: [@nextEpisode, false, false, [], @seasonEpisodes, null, null, true]
            callback: ()=>
              TWC.MVC.History.reset(TWC.MVC.History.size()-2)
          return
        @autoplayCnt--
        $(".counter .timer").text(@autoplayCnt)
      , 1000

    hideAutoplay: ->
      clearInterval(@autoplayTimer)
      @autoplayTimer = 0
      @player.width(Config.resolution[0])
      @player.height(Config.resolution[1])
      @player.top(0)
      @player.left(0)
      $("#overlay").show()
      $("#screen").css("background", "none")
      $(".autoplay-controls").hide()
      TWC.MVC.Router.setFocus("#play")

    closeVideo: ->
      clearInterval(@autoplayTimer)
      $("#screen").css("background", "none")
      $(".autoplay-controls").hide()
      @sendNewProductId()
      TWC.MVC.History.popState()

    updateSeekerTime: (direction, speed) ->
      @toggle.show()
      @toggle.options.enabled = false
      duration = @player.duration()

      @progressSeekDirection = direction

      currentVideoProgressTime = parseInt(@player.currentTime())
      @currentSeekerProgressTime = currentVideoProgressTime if !@currentSeekerProgressTime

      #test to see if image can change with seeking
      if Config.platform in ["core"]
        if !@updateVideoStillInterval
          @updateVideoStillInterval = setInterval =>
            @updateVideoStill(@skipToSeekTime)
          , 1000

      #set interval to update the time and the progress bar
      clearInterval(@progressSeekInterval)

      speedInterval = speed
      @progressSeekInterval = setInterval =>
        if direction is "forward"
          @currentSeekerProgressTime += speedInterval
          if @currentSeekerProgressTime > duration
            return
        else
          @currentSeekerProgressTime -= speedInterval
          if @currentSeekerProgressTime < 0
            return

        @updateTime(@currentSeekerProgressTime)
        #@skipToSeekTime is used to determine where to go to when continuing playback
        @skipToSeekTime = @currentSeekerProgressTime
      , 250

    updateVideoStill: (time) ->
      if @progressSeekInterval
        @player.currentTime(time)
      else
        clearInterval(@updateVideoStillInterval)
        @updateVideoStillInterval = null


    changeVideoSpeed: (direction, speed) ->
      #controls the speed and speedlabel in the player controls bar
      newSpeed = parseInt(speed) * 2
      $("##{direction} .speed").html("#{newSpeed}x")
      $("##{direction} .speed").show()
      if newSpeed > 64
        newSpeed = 1
        $("##{direction} .speed").html("#{newSpeed}x")
        @goToSeekPositionAndContinue()
        return
      @updateSeekerTime(direction, newSpeed)
      return newSpeed

    resetVideoSpeed: ->
      $("#rewind .speed, #forward .speed").html("1x")
      clearInterval(@updateVideoStillInterval)
      @updateVideoStillInterval = null

    renderChapters: (duration)->
      return if not @chaptersAvailable
      @chaptersLoaded = true
      if Config.platform is "core"
        duration = @chapters[@chapters.length-1].Position
      else
        duration = @player.duration()
      i=0
      for ch in @chapters
        left = (ch.Position / duration)*100
        $("#chapters").append("<li id='chapter-#{i}' class='chapter-point' style='left:#{left}%'><span class='marker'></span></li>")
        i++

    leanbackVisible: false

    toggleLeanback: (visible)->
      if visible
        @toggle.show() #temp disable auto-hide
        @leanbackVisible = true
        $(".vpContainer").addClass("leanback-visible") if @leanbackEnabled
      else
        @toggle.autoHide()
        @leanbackVisible = false
        $(".vpContainer").removeClass("leanback-visible") if @leanbackEnabled

    focusDown: ->
      return @toggle.autoHide() if @isControlsHidden()
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("headerButton")
        TWC.MVC.Router.setFocus "#play"
      else if $el.attr("id") is "triviaVideo"
        return TWC.MVC.Router.setFocus "#forwardChapter" if not $("#forwardChapter").hasClass("disable")
        TWC.MVC.Router.setFocus "#forward"
      else if $el.attr("id") is "playerBackButton"
        if not $("#rewindChapter").hasClass("disable")
          TWC.MVC.Router.setFocus $("#rewindChapter")
        else
          TWC.MVC.Router.setFocus $("#rewind")
      else
        if @leanbackEnabled
          @toggleLeanback(true)
          TWC.MVC.Router.find("leanback-zone").focusFirst()
        else
          @hideControls()


    focusUp: ->
      if @tenMinutePreview
        if @buyUHDPricingPlan?
          if $("#leftPreviewButton").length > 0
            return TWC.MVC.Router.setFocus("#leftPreviewButton")
        if @rentUHDPricingPlan?
          if $("#rightPreviewButton").length > 0
            return TWC.MVC.Router.setFocus("#rightPreviewButton")

    focusUpFromLeanback:->
      @toggleLeanback(false)
      TWC.MVC.Router.setActive "videoplayer-view"
      TWC.MVC.Router.setFocus "#play"

    focusLeft: (e)->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("headerButton")
        if $($el).prevAll(".headerButton:not(.disable)").length > 0
          TWC.MVC.Router.setFocus $($el).prevAll(".headerButton:not(.disable):first")
      else if $el.attr('id') is "rewind"
        TWC.MVC.Router.setFocus $("#rewindChapter") if not $("#rewindChapter").hasClass("disable")

      else if $el.attr('id') is "subs"
        if not $("#extras").hasClass("disable")
          TWC.MVC.Router.setFocus $("#extras")
        else if not $("#forwardChapter").hasClass("disable")
          TWC.MVC.Router.setFocus $("#forwardChapter")
        else
          TWC.MVC.Router.setFocus $("#forward")
      else if $el.attr('id') is "audio"
        if not $("#subs").hasClass("disable")
          TWC.MVC.Router.setFocus $("#subs")
        else if not $("#extras").hasClass("disable")
          TWC.MVC.Router.setFocus $("#extras")
        else
          if not $("#forwardChapter").hasClass("disable")
            TWC.MVC.Router.setFocus $("#forwardChapter")
          else
            TWC.MVC.Router.setFocus $("#forward")
      else if $el.attr('id') is "extras"
        if not $("#forwardChapter").hasClass("disable")
          TWC.MVC.Router.setFocus $("#forwardChapter")
        else
          TWC.MVC.Router.setFocus $("#forward")



    focusRight: (e)->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("headerButton")
        if $($el).nextAll(".headerButton:not(.disable)").length > 0
          TWC.MVC.Router.setFocus $($el).nextAll(".headerButton:not(.disable):first")
      else if $el.attr('id') is "forward"
        if not $("#forwardChapter").hasClass("disable")
          return TWC.MVC.Router.setFocus $("#forwardChapter")

        if not $("#extras").hasClass("disable")
          return TWC.MVC.Router.setFocus $("#extras")
        if not $("#subs").hasClass("disable")
          return TWC.MVC.Router.setFocus $("#subs")
        if not $("#audio").hasClass("disable")
          return TWC.MVC.Router.setFocus $("#audio")
        if $("#triviaVideo").length
          return TWC.MVC.Router.setFocus "#triviaVideo"
      else if $el.attr('id') is "forwardChapter"
        if not $("#extras").hasClass("disable")
          return TWC.MVC.Router.setFocus $("#extras")
        if not $("#subs").hasClass("disable")
          return TWC.MVC.Router.setFocus $("#subs")
        if not $("#audio").hasClass("disable")
          return TWC.MVC.Router.setFocus $("#audio")
        if $("#triviaVideo").length
          return TWC.MVC.Router.setFocus "#triviaVideo"
      else if $el.attr('id') is "subs"
        if not $("#audio").hasClass("disable")
          return TWC.MVC.Router.setFocus $("#audio")
        if $("#triviaVideo").length
          return TWC.MVC.Router.setFocus "#triviaVideo"
      else if $el.attr('id') is "extras"
        if not $("#subs").hasClass("disable")
          return TWC.MVC.Router.setFocus $("#subs")
        if not $("#audio").hasClass("disable")
          return TWC.MVC.Router.setFocus $("#audio")

    hideControls: ->
      return if @progressSeekDirection
      @toggle.hide()

    togglePause: ->
      return @toggle.autoHide() if @isControlsHidden()
      return if @verifyActiveSeekInProgress()
      @sendUpdateContentProgressRequest(false)
      if $('#play').hasClass('pause')
        @pause()
      else
        @play()

    play: ->
      if @player?
        $('#play').addClass('pause')
        @player.play()

    pause: ->
      if @player?
        $('#play').removeClass('pause')
        @player.pause()

    stop: ->
      TWC.SDK.Input.get().removeFromBlocked("_play", "_pause", "play_pause", "stop", "forward", "rewind", "fastforward" ,"fastrewind", "cc", "return")
      return @toggle.autoHide() if @isControlsHidden()
      if @player
        @sendUpdateContentProgressRequest(true)
        @player.stop()
      else
        @sendNewProductId()
        TWC.MVC.History.popState()

    rewind: ->
      clearTimeout(@checkTimeTimeout)
      if @progressSeekDirection is "forward"
        @goToSeekPositionAndContinue()
        return
      return @toggle.autoHide() if @isControlsHidden()

      #clear subtitle if by chance it was present on screen during initialisation
      @processSubtitle(false, "")

      @sendUpdateContentProgressRequest(false)
      if $('#play').hasClass('pause')
        @player.pause()
      currentSpeed = parseInt($("#rewind .speed").text())
      @toggleLeanback(true)
      @changeVideoSpeed("rewind", currentSpeed)

    forward: ->
      clearTimeout(@checkTimeTimeout)
      if @progressSeekDirection is "rewind"
        @goToSeekPositionAndContinue()
        return
      return @toggle.autoHide() if @isControlsHidden()


      @sendUpdateContentProgressRequest(false)
      if $('#play').hasClass('pause')
        @player.pause()
      currentSpeed = parseInt($("#forward .speed").text())
      @toggleLeanback(true)
      @changeVideoSpeed("forward", currentSpeed)

      #clear subtitle if by chance it was present on screen during initialisation
      @processSubtitle(false, "")

    forwardChapter: ->
      clearTimeout(@checkTimeTimeout)
      return if not @chaptersAvailable
      return @toggle.autoHide() if @isControlsHidden()

      #clear subtitle if by chance it was present on screen during initialisation
      @processSubtitle(false, "")
      @blockSubtitles = true
      if @verifyActiveSeekInProgress(false)
        @goToSeekPosition()


      @chapterOffset++
      #update chapter bubble
      pos =  @getNextChapter(0) + (@chapterOffset - 1)
      $(".chapter-point.active").removeClass("active")
      $("#chapter-#{pos}").addClass("active")

      clearTimeout(@chapteroffsetBackwardTimeout)
      clearTimeout(@chapteroffsetForwardTimeout)

      #stop playback after users skips last chapter
      if @getNextChapter(@chapterOffset) > @chapters.length
        @chapterOffset = 0
        @stop()
        return

      @chapteroffsetForwardTimeout = setTimeout () =>
        nextChapter = @getNextChapter(@chapterOffset - 1)
        if nextChapter is -1
          @chapterOffset = 0
          @stop()
          return
        @goToChapter(nextChapter)
        @chapterOffset = 0
      , 1500



    rewindChapter: ->

      clearTimeout(@checkTimeTimeout)
      return if not @chaptersAvailable
      return @toggle.autoHide() if @isControlsHidden()

      #clear subtitle if by chance it was present on screen during initialisation
      @processSubtitle(false, "")
      @blockSubtitles = true

      if @verifyActiveSeekInProgress(false)
        @goToSeekPosition()


      @chapterOffset++
      #update chapter bubble
      $(".chapter-point.active").removeClass("active")
      pos = @getPrevChapter(0) - (@chapterOffset - 1)
      $("#chapter-#{pos}").addClass("active")

      clearTimeout(@chapteroffsetForwardTimeout)
      clearTimeout(@chapteroffsetBackwardTimeout)
      @chapteroffsetBackwardTimeout = setTimeout () =>
        previousChapter = @getPrevChapter(@chapterOffset - 1)
        @goToChapter(previousChapter)
        @chapterOffset = 0
      , 1500


    goToChapter: (pos) ->
      #emulate chapters in the core version
      duration = @player.duration()
      if Config.platform is "core"

        @player.currentTime( (@chapters[pos].Position / @chapters[@chapters.length-1].Position) * duration )
        leftVal = parseInt($(".progress").css("margin-left")) + Math.max(0, ((@chapters[pos].Position / @chapters[@chapters.length-1].Position) * $(".progress").width() - $("#chapter-thumb").width() / 2))
        leftVal = Math.min(leftVal, $(".progress").width() - $("#chapter-thumb").width())
      else
        @player.pause()
        @player.currentTime( @chapters[pos].Position )
        @player.play()

        leftVal = parseInt($(".progress").css("margin-left")) + Math.max(0, ((@chapters[pos].Position / duration) * $(".progress").width() - $("#chapter-thumb").width() / 2))
        leftVal = Math.min(leftVal, parseInt($(".progress").css("margin-left")) + $(".progress").width() - $("#chapter-thumb").width())

      $img = $("<img>").attr('src', @chapters[pos].ThumbnailUrl)
      $img.one "load", =>
        posText = @parseTime(@chapters[pos].Position)
        $(".chapter-info").text("Chapter #{pos+1} - #{posText}")
        $("#chapter-arrow-down").css("margin-left", $("#chapter-thumb").width() / 2)

        #if thumbnail is on the left, then move the arrow further to the left if required
        if leftVal is parseInt($(".progress").css("margin-left"))
          newValue = Math.max(14, (@chapters[pos].Position / duration) * $(".progress").width())
          $("#chapter-arrow-down").css("margin-left", newValue)


        #if thumbnail is on the right, then move the arrow further to the right if required
        if leftVal is parseInt($(".progress").css("margin-left")) + $(".progress").width() - $("#chapter-thumb").width()
          newValue = Math.min($("#chapter-thumb").width()-14, ((@chapters[pos].Position / duration) * $(".progress").width()) - ($(".progress").width() - $("#chapter-thumb").width()))
          $("#chapter-arrow-down").css("margin-left", newValue)


        $("#chapter-thumb").attr('src', @chapters[pos].ThumbnailUrl).parent().css("left", leftVal).stop().show()


      if @chapterTimeout isnt null
        clearTimeout(@chapterTimeout)
        @chapterTimeout = null
        $(".chapter-point.active").removeClass("active")

      $("#chapter-#{pos}").addClass("active")
      @chapterTimeout = setTimeout ()=>
        $("#chapter-#{pos}").removeClass("active")
        $("#chapter-thumb-container").fadeOut(500)
        @chapterTimeout = null
      , 4000
      @sendUpdateContentProgressRequest(false)

    getNextChapter: (chapterOffset)->
      i = 0
      for ch in @chapters
        if Config.platform is "core"
          if (ch.Position / @chapters[@chapters.length-1].Position) * @player.duration() > @currentTime
            return Math.max(0, i + chapterOffset)
        else
          if ch.Position > @currentTime
            return Math.max(0, i + chapterOffset)
        i++
      return -1

    getPrevChapter: (chapterOffset)->
      i = 0
      for i in [@chapters.length-1...0]
        ch = @chapters[i]
        if Config.platform is "core"
          if (ch.Position / @chapters[@chapters.length-1].Position) * @player.duration() < @currentTime #go back to prev chanpter in the first 1 sec
            return Math.max(0, i - chapterOffset)
        else
          if ch.Position < @currentTime #go back to prev chapter in the first 1 sec
            return Math.max(0, i - chapterOffset)
      return 0

    clickChapterPoint: (e)->
      item = $(e.target)
      item = $(item).parent() if $(item).get(0).nodeName == "SPAN"
      pos = parseInt($(item).attr("id").split("-")[1])
      @showChapterThumbnail(pos)





    verifyActiveSeekInProgress: (goToSeekPosition = true)->
      if @progressSeekInterval
        @goToSeekPositionAndContinue() if goToSeekPosition
        return true
      else
        return false

    goToSeekPositionAndContinue: ->
      #clear the seek interval and move to updated time
      clearInterval(@progressSeekInterval)

      #clear subtitle if by chance it was present on screen during initialisation
      @processSubtitle(false, "")

      $("#forward .speed").hide()
      $("#rewind .speed").hide()
      @player.currentTime(@skipToSeekTime)

      #if trivia is shown, hide it
      @processTriviaMessage(false, false) if @extrasEnabled

      #cancel toggle view
      @toggleLeanback(false)

      #clear the vars
      @progressSeekInterval = null
      @skipToSeekTime = null
      @progressSeekDirection = null
      @currentSeekerProgressTime = null

      # Reset seek videospeed
      @resetVideoSpeed()

      #continue playback
      @player.play()

      @sendUpdateContentProgressRequest(false)
      @toggle.options.enabled = true
      @toggle.autoHide()

    goToSeekPosition: ->
      #clear the seek interval and move to updated time
      clearInterval(@progressSeekInterval)
      $("#forward .speed").hide()
      $("#rewind .speed").hide()
      #      @player.currentTime(@skipToSeekTime)
      @currentTime = @skipToSeekTime

      #clear the vars
      @progressSeekInterval = null
      @skipToSeekTime = null
      @progressSeekDirection = null
      @currentSeekerProgressTime = null

      # Reset seek videospeed
      @resetVideoSpeed()

      @sendUpdateContentProgressRequest(false)
      @toggle.options.enabled = true
      @toggle.autoHide()

    setVisibleControls: (controlsVisible)->
      if contorlsVisible
        @toggle.autoHide()
      else
        @toggle.hide()

    jumpTo: (time)->
      @player.pause()
      @player.currentTime( time )
      @player.play()
      @sendUpdateContentProgressRequest(false)


    rc_play: ->
      return if @verifyActiveSeekInProgress()
      @sendUpdateContentProgressRequest(false)
      @toggle.autoHide() if @isControlsHidden()
      @player.play()
      TWC.MVC.Router.setFocus("#play")


    rc_pause: ->
      return if @verifyActiveSeekInProgress()
      @sendUpdateContentProgressRequest(false)
      @toggle.autoHide() if @isControlsHidden()
      @player.pause()
      TWC.MVC.Router.setFocus("#play")


    rc_play_pause: ->
      return if @verifyActiveSeekInProgress()
      @sendUpdateContentProgressRequest(false)
      @toggle.autoHide() if @isControlsHidden()
      if @player.isPaused()
        @player.play()
      else
        @player.pause()

    rc_stop: ->
      @stop()
      TWC.MVC.Router.setFocus("#stop")

    rc_rewind: ->
      @rewind()
      TWC.MVC.Router.setFocus("#rewind")

    rc_forward: ->
      @forward()
      TWC.MVC.Router.setFocus("#forward")

    rc_fastrewind: ->
      return if not @chaptersAvailable
      @rewindChapter()
      TWC.MVC.Router.setFocus("#rewindChapter")

    rc_fastforward: ->
      return if not @chaptersAvailable
      @forwardChapter()
      TWC.MVC.Router.setFocus("#forwardChapter")

    openTriviaVideo: ()->
      videoUrl = $(TWC.MVC.Router.getFocus()).attr("data-url")
      objArray = []
      videoObject = {}
      videoObject.url = videoUrl
      objArray.push(videoObject)
      videoArray = [{"stream":objArray}];

      #store the resume time, and continue the video when user returns
      @resumeTime = @currentTime
      @updateHistory(@productModel, @isTrailer, @isExtra, @messages, @leanbackVideos, @leanbackPosition, @trackingName, @episodeChaining, @resumeTime, @extrasEnabled)

      TWC.MVC.Router.load 'extravideoplayer-view',
        data: [null, videoArray, @productModel, @CPE, 0, ()=>
          TWC.MVC.History.popState()
        ]

    goBack: ->
      $('body').trigger "return"

    #akamai
    initAkamaiTracker: ->
      return if @akamaiTracker?
      akaPluginCallback = {}
      akaPluginCallback["streamHeadPosition"] = @getStreamHeadPosition
      akaPluginCallback["streamLength"] = @getStreamLength
      akaPluginCallback["streamURL"] = @getStreamURL
#      akaPluginCallback["isLive"] = @getIsLIve
#      akaPluginCallback["videoSize"] = @getVideoSize
#      akaPluginCallback["viewSize"] = @getViewSize
#      akaPluginCallback["serverIP"] = @getServerIP

      # Get entitled pricing plan
      if not @isExtra and not @isTrailer
        entitledPricingPlan = @productModel.get("PricingPlans").findWhere({ Id : @productModel.get("EntitledPricingPlanId")})
        entitledQuality = if entitledPricingPlan? then " " + entitledPricingPlan.getQuality() else ""


      show = true

      if @isTrailer
        title = @productModel.get("Name") + " (Trailer)"
        eventName = @productModel.get("Name") + " (Trailer)"
        category = "Trailer"
      else if @isExtra and not @extraVideoPlayer
        # this is the regular extra video which was available directly from PDP below the image viewer
        # probably it's not available anymore
        title = @videoTitle + " (Extra)"
        eventName = @videoTitle + " (Extra)"
        category = "Extra"
      else if @isExtra and @extraVideoPlayer
        title = if @videoTitle? then @videoTitle else @akamaiUrl
        eventName = if @videoTitle? then @videoTitle else @akamaiUrl
        category = "CPE"
      else
        title =  @productModel.get("Name") + entitledQuality
        eventName =  @productModel.get("Name") + entitledQuality
        category = if @productModel.get("SeasonId") is "" then "Movie" else "Episode"

      akamaiOptions =
        data:
          title: title
          eventName: eventName
          category: category
          contentType: @playerConfig.mime
          device: Config.platform
          viewerId: Authentication.getUserId()


        akaPluginCallback: akaPluginCallback

      if not @isExtra and not @isTrailer and @productModel.get("SeriesName")? and @productModel.get("SeriesName") isnt ""
        akamaiOptions.data.show = @productModel.get("SeriesName")

      @akamaiTracker = new AkamaiTracker()
      @akamaiTracker.init(akamaiOptions)

    #akamai callbacks
    getStreamHeadPosition: =>
      return @currentTime

    getStreamLength: =>
      return @player.duration()

    getStreamURL: =>
      return @playerConfig.url

#    getIsLIve: ->
#      return false
#
#    getVideoSize: ->
#      return Config.resolution[0]
#
#    getViewSize: ->
#      return Config.resolution[1]
#
#    getServerIP: ->
#      return '-'

    # Mouse events

    initDraggableProgressbar: ()->
      start = 0
      progressBarWidth = $("#progressBar").outerWidth(true)
      duration = 0

      $("#progress-dot").on "touchstart", (e)=>
        duration = @player.duration()
        event = e.originalEvent
        if event.touches[0].pageX then start = event.touches[0].pageX else if event.touches[0].clientX then event.touches[0].clientX else console.log("do nothing")
        @player.pause()


      $("#progress-dot").on "touchmove", (e)=>
        e = e || window.event
        event = e.originalEvent
        end = 0
        if event.touches[0].pageX
          end = event.touches[0].pageX
        else
          if event.touches[0].clientX
            end = event.touches[0].clientX

        progressBarStart = $("#progressBar").offset().left
        newProgressBarValue = (((end-progressBarStart) / progressBarWidth)*100)
        newProgressBarValue = Math.min(100, Math.max(0, newProgressBarValue))
        $("#progress-dot").css("left", newProgressBarValue + "%")

        progressBarDotLeft = (newProgressBarValue / 100) * progressBarWidth
        for chapter, pos in @chapters
          if Config.platform is "core"
            chapterPos = (chapter.Position / @chapters[@chapters.length-1].Position) * progressBarWidth
          else
            chapterPos = (chapter.Position / duration) * progressBarWidth

          if Math.abs(chapterPos - progressBarDotLeft) < 10
            @showChapterThumbnail(pos)
            break

      $("#progress-dot").on "touchend", (e)=>
        @pause()
        leftValue = parseInt($("#progress-dot").css("left"));
        newTime = (leftValue / progressBarWidth) * duration
        @player.currentTime(newTime)
        @player.play()

      $("#progress-dot").on "mousedown", (e)=>
        duration = @player.duration()
        progressBarWidth = $("#progressBar").outerWidth(true)
        e = e || window.event
        start = 0
        diff = 0
        if e.pageX then start = e.pageX else if e.clientX then start = e.clientX else console.log('do nothing')
        @player.pause()
        document.body.onmousemove = (e)=>
          e = e || window.event
          end = 0
          if e.pageX
            end = e.pageX
          else
            if e.clientX
              end = e.clientX

          progressBarStart = $("#progressBar").offset().left
          newProgressBarValue = (((end-progressBarStart) / progressBarWidth)*100)
          newProgressBarValue = Math.min(100, Math.max(0, newProgressBarValue))
          $("#progress-dot").css("left", newProgressBarValue + "%")

          progressBarDotLeft = (newProgressBarValue / 100) * progressBarWidth
          for chapter, pos in @chapters
            if Config.platform is "core"
              chapterPos = (chapter.Position / @chapters[@chapters.length-1].Position) * progressBarWidth
            else
              chapterPos = (chapter.Position / duration) * progressBarWidth

            if Math.abs(chapterPos - progressBarDotLeft) < 10
              @showChapterThumbnail(pos)
              break


        document.body.onmouseup = ()=>
          @pause()
          leftValue = parseInt($("#progress-dot").css("left"));
          newTime = (leftValue / progressBarWidth) * duration
          @player.currentTime(newTime)
          @processSubtitle(false, "")
          @player.play()


          # do something with the action here
          # elem has been moved by diff pixels in the X axis
#          $("#progress-dot").css("position", "static")
          document.body.onmousemove = document.body.onmouseup = null

    showChapterThumbnail: (pos)->
      @selectedChapter = pos
      duration = @player.duration()
      if Config.platform is "core"
        leftVal = parseInt($(".progress").css("margin-left")) + Math.max(0, ((@chapters[pos].Position / @chapters[@chapters.length-1].Position) * $(".progress").width() - $("#chapter-thumb").width() / 2))
        leftVal = Math.min(leftVal, parseInt($(".progress").css("margin-left")) + $(".progress").width() - $("#chapter-thumb").width())
      else
        leftVal = parseInt($(".progress").css("margin-left")) + Math.max(0, ((@chapters[pos].Position / duration) * $(".progress").width() - $("#chapter-thumb").width() / 2))
        leftVal = Math.min(leftVal, parseInt($(".progress").css("margin-left")) + $(".progress").width() - $("#chapter-thumb").width())

      $img = $("<img>").attr('src', @chapters[pos].ThumbnailUrl)
      $img.one "load", =>
        posText = @parseTime(@chapters[pos].Position)
        $(".chapter-info").text("Chapter #{pos+1} - #{posText}")
        $("#chapter-arrow-down").css("margin-left", $("#chapter-thumb").width() / 2)

        #if thumbnail is on the left, then move the arrow further to the left if required
        if leftVal is parseInt($(".progress").css("margin-left"))
          newValue = Math.max(14, (@chapters[pos].Position / duration) * $(".progress").width())
          $("#chapter-arrow-down").css("margin-left", newValue)


        #if thumbnail is on the right, then move the arrow further to the right if required
        if leftVal is parseInt($(".progress").css("margin-left")) + $(".progress").width() - $("#chapter-thumb").width()
          newValue = Math.min($("#chapter-thumb").width()-14, ((@chapters[pos].Position / duration) * $(".progress").width()) - ($(".progress").width() - $("#chapter-thumb").width()))
          $("#chapter-arrow-down").css("margin-left", newValue)


        $("#chapter-thumb").attr('src', @chapters[pos].ThumbnailUrl).parent().css("left", leftVal).stop().show()

      if @chapterTimeout isnt null
        clearTimeout(@chapterTimeout)
        @chapterTimeout = null
        $(".chapter-point.active").removeClass("active")

      $("#chapter-#{pos}").addClass("active")
      @selectedChapterTimeout = setTimeout ()=>
        @goToSelectedChapter()
      , 1500

      #@chapterTimeout = setTimeout ()=>
       # $("#chapter-#{pos}").removeClass("active")
       # $("#chapter-thumb-container").fadeOut(500)
       # @chapterTimeout = null
       # @selectedChapter = selectedChapter
       # @selectedChapter = selectedChapter
      #, 4000

    goToSelectedChapter: ()->
      clearTimeout(@selectedChapterTimeout)
      if @selectedChapter
        @goToChapter(@selectedChapter)
        #clear subtitle if by chance it was present on screen during initialisation
        @processSubtitle(false, "")

    updateDebugText: ()->
      if Config.environment is "development"
        $("#debugText").text(@getRenderString())

    getRenderString: ->
      return @getTimeString() + " | " + @getVideoQualityString() + " | " + @getAudioQualityString()
        # + " | " + @getBandwidthString() + " | " + @getVideoCodecCountersString();

    getTimeString: ->
      return @parseTime(@currentTime)

    getVideoQualityString: ->
      if @activeVideoFormat
        if @player.getCurrentBitrate?() then bitrate = @player.getCurrentBitrate() else null
        return "bitrate:"+ Math.round(bitrate / 1000)+"kbps resolution:" + @activeVideoFormat.width + "x" + @activeVideoFormat.height;
      else
        return "bitrate:? resolution:?"

    getAudioQualityString: ->
      if @player?
        audioTrack = @player.audioTrack() if @player.audioTrack?()
        if audioTrack and not @isTrailer
          channelValue = "?"
          channelLanguage = "?"
          channelLanguage = audioTrack.language if audioTrack.language?
          channelValue = audioTrack.channels.value if audioTrack.channels?
          return "bitrate:"+Math.round(@player.getCurrentAudioBitrate() / 1000)+"kbps channels:" + channelValue + " language:" + channelLanguage;

        return "bitrate:? channels:? language:?"
    #return activeAudioFormat == null ? "bitrate:? channels:? language:?" : "bitrate:"+Math.round(activeAudioFormat.bitrate/1000)+"kbps channels:" + activeAudioFormat.audioChannels + " language:" + activeAudioFormat.language;

    getBandwidthString: ->
     #bandwidth = Math.round(@player.getCurrentDownloadBitrate() / 1000);
     #if bandwidth == 0
     #  return "bandwidth:?"
     #else
     #  return "bandwidth:" + bandwidth + "kbps"

    getVideoCodecCountersString: ->
    # CodecCounters codecCounters = debuggable.getCodecCounters();
    # return codecCounters == null ? "" : codecCounters.getDebugString();

# playerVideoplayerView.get()
