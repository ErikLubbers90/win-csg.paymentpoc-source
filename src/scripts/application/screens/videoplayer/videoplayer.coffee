define [
  "framework",
  "application/screens/videoplayer/togglecontrols",
  "application/popups/error/error",
  "application/popups/generic/alertWithOptions",
  "application/popups/generic/alert",
  "application/screens/videoplayer/basevideoplayer",
  "application/screens/videoplayer/leanback",
  "../../api/cd/models/CDProduct",
  "config",
  "i18n!../../../lang/nls/manifest",
  "i18n!../../../lang/nls/regionsettings",
  "./languageOverlay"
  "../../api/cd/CDBaseRequest",
  "jquery"
], (TWC, ToggleControls, Error, AlertWithOptions, Alert, BaseVideoPlayer, Leanback, CDProductModel, Config, Manifest, RegionSettings, LanguageOverlay, CDBaseRequest, $) ->

  BaseVideoPlayer.get()
