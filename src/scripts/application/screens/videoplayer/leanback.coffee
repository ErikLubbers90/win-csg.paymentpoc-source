define [
  "framework",
  "jquery",
  "backbone",
  "i18n!../../../lang/nls/manifest",
  "application/api/cd/models/CDSearchProducts",
  "application/api/cd/collections/CDProductCollection",
  "application/screens/productoverview/productoverview",
  "application/models/baseModel",
], (TWC, $, Backbone, Manifest, CDSearchProductsModel, CDProductCollection, ProductOverview, BaseModel) ->

  leanbackModel = BaseModel.extend
    defaults:
      manifest: Manifest

  class leanbackView extends TWC.MVC.View
    uid: 'leanback-zone'

    el: "#leanback"

    type: "zone"

    currentLeanbackOffset: 0

    html:
      """
        <div class="thumbnail-container">
          <div class="scroll-container">
            <% for(var i=0;i<RelatedVideos.length;i++){ %>
                  <div id="relatedVideo-<%- i %>" data-left="focusLeft" data-right="focusRight" data-up="focusUp" data-down="focusDown" class="extra-thumbnail video">
                      <div class="thumbnail-border"></div>
                      <img src="<%- RelatedVideos[i].ThumbnailUrl %>"/>
                  </div>
            <% } %>
          </div>
        </div>
      """

    model: new leanbackModel()

    zones: []

    events:
      "enter .extra-thumbnail" : "loadExtraVideo"

    onload: (@relatedVideos = [], @activeVideo)->
      @model.set("RelatedVideos", @relatedVideos)
      @model.set("ActiveVideo", @activeVideo)

    loadExtraVideo: ->
      $el = $(TWC.MVC.Router.getFocus())
      pos = parseInt($el.attr("id").split("-")[1])
      selectedVideo = @relatedVideos[pos]
      TWC.MVC.History.history.pop()
      TWC.MVC.Router.load "videoplayer-view",
        data: [selectedVideo, false, true, [], @relatedVideos, pos]

    #Focus events
    focusFirst: ->
      TWC.MVC.Router.setActive "leanback-zone"
      TWC.MVC.Router.setFocus "#relatedVideo-#{@currentLeanbackOffset}"

    focusUp: ->
      @parent.focusUpFromLeanback()

    focusLeft: ->
      $el = $(TWC.MVC.Router.getFocus())
      @focusThumbnailLeft($el)

    focusRight: ->
      $el = $(TWC.MVC.Router.getFocus())
      @focusThumbnailRight($el)

    focusDown: ->
      @parent?.hideControls?()

    focusThumbnailLeft: ($el)->
      pos = parseInt($el.attr("id").split("-")[1])
      return if pos is 0
      $container = $("#leanback .scroll-container")

      if pos is @currentLeanbackOffset
        scrollWidth = $(".extra-thumbnail").outerWidth(true)
        @currentLeanbackOffset--
        $container.css({
          "-o-transform": "translate(" + (@currentLeanbackOffset*-scrollWidth) + "px, 0)"
          "-webkit-transform": "translate3d(" + (@currentLeanbackOffset*-scrollWidth) + "px, 0, 0)"
        });

      TWC.MVC.Router.setFocus "#" + $el.prev().attr("id")


    focusThumbnailRight: ($el)->
      pos = parseInt($el.attr("id").split("-")[1])
      videoCnt = $("#leanback .scroll-container").children().length
      return if pos is videoCnt-1

      $container = $("#leanback .scroll-container")
      if pos - @currentLeanbackOffset is 4
        scrollWidth = $(".extra-thumbnail").outerWidth(true)
        @currentLeanbackOffset++
        $container.css({
          "-o-transform": "translate(" + (@currentLeanbackOffset*-scrollWidth) + "px, 0)"
          "-webkit-transform": "translate3d(" + (@currentLeanbackOffset*-scrollWidth) + "px, 0, 0)"
        });

      TWC.MVC.Router.setFocus "#"+$el.next().attr("id")



  leanbackView.get()
