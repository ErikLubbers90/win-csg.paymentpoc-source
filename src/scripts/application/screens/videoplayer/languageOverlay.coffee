define [
    "framework",
    "underscore",
    "jquery",
    "config",
    "backbone",
    "i18n!../../../lang/nls/manifest",
    "i18n!../../../lang/nls/regionsettings"
], (TWC, _, $, Config, Backbone, Manifest, RegionSettings) ->

  LanguageOverlayModel = Backbone.Model.extend({
    fetch: (options) ->
      options.success()
  })

  class LanguageOverlay extends TWC.MVC.View
    #dom element of reference
    el: "#popup"

    type: "popup"

    events:
      "enter .languageSelect" : "selectAudio"
      "enter .subtitleSelect" : "selectSubtitle"
      "enter .subtitleColor" : "selectSubtitleColor"
      "enter .subtitleOption" : "activateSubtitleOption"
      "enter #resetSubtitleOptions": "resetSubtitleOptions"
      "enter .backgroundOption" : "activateBackgroundOption"
      "enter .subtitleEnabledSelect" : "selectSubtitleEnable"
      "enter #subtitle_settings" : "showSubtitleSettings"
      "enter #font_settings" : "showFontSettings"
      "enter #background_settings" : "showBackgroundSettings"
      "cc"                    : "close"
      "enter #popupCloseButton" : "close"

    currentIndexAudioTrack: 0
    currentIndexSubtitleTrack: 0

    subtitleOptionArray: {}

    manifest: Manifest

    subtitleColors: [
      { "title": Manifest.subtitles_white, "color": "#FFFFFF" },
      { "title": Manifest.subtitles_black, "color": "#000000" },
      { "title": Manifest.subtitles_red, "color": "#FF0000" },
      { "title": Manifest.subtitles_green, "color": "#008000" },
      { "title": Manifest.subtitles_blue, "color": "#0000FF" },
      { "title": Manifest.subtitles_yellow, "color": "#FFFF00" },
      { "title": Manifest.subtitles_magenta, "color": "#FF00FF" },
      { "title": Manifest.subtitles_cyan, "color": "#00FFFF" }
    ]

    subtitleOpacity: [
      { "title": "100", "value": "1" },
      { "title": "75", "value": "0.75" },
      { "title": "50", "value": "0.5" },
      { "title": "25", "value": "0.25" },
      { "title": "0", "value": "0" }
    ]

    subtitleSize: [
      { "title": "Default", "value": "30" },
      { "title": "10", "value": "10" },
      { "title": "14", "value": "14" },
      { "title": "16", "value": "16" },
      { "title": "20", "value": "20" },
      { "title": "36", "value": "36" }
    ]

    subtitleFont: [
      { "title": "comic sans ms", "value": "comicsans" },
      { "title": "times new roman", "value": "times" },
      { "title": "arial", "value": "arial" },
      { "title": "sans", "value": "opensans" },
      { "title": "courier", "value": "courier" }
    ]

    subtitleStyle: [
      { "title": "None", "value": "0px 0px 0px #888888" },
      { "title": "Drop shadow", "value": "1px 1px 1px #888888" },
      { "title": "Raised", "value": "1px -1px 1px #888888" },
      { "title": "Depressed", "value": "0px 0px 5px #FFF" },
      { "title": "Uniform", "value": "0px 0px 5px #888888" }
    ]

    backgroundColor: [
      { "title": "Off", "color" :""},
      { "title": Manifest.subtitles_white, "color": "#FFFFFF" },
      { "title": Manifest.subtitles_black, "color": "#000000" },
      { "title": Manifest.subtitles_red, "color": "#FF0000" },
      { "title": Manifest.subtitles_green, "color": "#008000" },
      { "title": Manifest.subtitles_blue, "color": "#0000FF" },
      { "title": Manifest.subtitles_yellow, "color": "#FFFF00" },
      { "title": Manifest.subtitles_magenta, "color": "#FF00FF" },
      { "title": Manifest.subtitles_cyan, "color": "#00FFFF" }
    ]

    backgroundOpacity: [
      { "title": "100", "value": "1" },
      { "title": "75", "value": "0.75" },
      { "title": "50", "value": "0.50" },
      { "title": "25", "value": "0.25" },
      { "title": "0", "value": "0" }
    ]

    windowColor: [
      { "title": "Off", "color" :""},
      { "title": Manifest.subtitles_white, "color": "#FFFFFF" },
      { "title": Manifest.subtitles_black, "color": "#000000" },
      { "title": Manifest.subtitles_red, "color": "#FF0000" },
      { "title": Manifest.subtitles_green, "color": "#008000" },
      { "title": Manifest.subtitles_blue, "color": "#0000FF" },
      { "title": Manifest.subtitles_yellow, "color": "#FFFF00" },
      { "title": Manifest.subtitles_magenta, "color": "#FF00FF" },
      { "title": Manifest.subtitles_cyan, "color": "#00FFFF" }
    ]

    windowOpacity: [
      { "title": "100", "value": "1" },
      { "title": "75", "value": "0.75" },
      { "title": "50", "value": "0.5" },
      { "title": "25", "value": "0.25" },
      { "title": "0", "value": "0" }
    ]


    html:"""
        <% if(OverlayTypeAudio) { %>
          <div id="popupCloseButton" class="menu-sound closeButton" data-down="defaultFocus"><span class='buttonText'><%- manifest.close %></span></div>
          <div id="overlayTitle">Audio Settings</div>
          <div id="languageSettingsContainer">
          <div class="selector-container">
          <div id="audioSelection">
            <h1><%= manifest.language %></h1>
            <div class="divider"></div>
            <ul id="audioSelectionList">
            <% for(var i=0;i<audioTracks.length;i++){ %>
              <li id="language_<%= i %>" class="languageSelect<%= audioTracks[i].active ? ' activeLanguage' : '' %>" data-iso="<%= audioTracks[i].id %>" data-channels="<%=audioTracks[i].channels %>" data-index="<%=audioTracks[i].index %>" data-left='defaultFocus' data-right='defaultFocus' ><%= audioTracks[i].label %><%= audioTracks[i].channels > 2 ? ' (DTS)' : ''%></li>
            <% } %>
            </ul>
            <div id="audioSelectionArrowLeft" class="prevSelection"></div>
            <div id="audioSelectionArrowRight" class="nextSelection"></div>
          </div>
        <% }else{ %>

          <div id="popupCloseButton" class="menu-sound closeButton" data-left="#background_settings" data-down="defaultFocus"><span class='buttonText'><%- manifest.close %></span></div>

          <div id="overlaySubtitleMenu">
            <div id="subtitle_settings" class="subtitleMenu button" data-left="defaultFocus" data-right="defaultFocus" data-down="focusDown"><%- manifest.subtitles_menu_settings %></div>
            <div id="font_settings" class="disabled subtitleMenu button" data-left="defaultFocus" data-right="defaultFocus" data-down="focusDown"><%- manifest.subtitles_font_settings %></div>
            <div id="background_settings" class="disabled subtitleMenu button" data-left="defaultFocus" data-right="#popupCloseButton" data-down="focusDown"><%- manifest.subtitles_background_settings %></div>
          </div>

          <div id="languageSettingsContainer">
            <div class="selector-container">
              <div id="subtitleSelection">
                <h1><%- manifest.subtitles_language %></h1>
                <div class="divider"></div>
                <ul id="subtitleSelectionList">
                  <li id="subtitle_off" class="subtitleEnabledSelect <%= (!subtitleActive ? 'active' : '')%>" data-left='defaultFocus' data-down='focusDown' data-right='defaultFocus' data-up='focusUp'><%- manifest.none %></li>
                <% for(var j=0;j<subtitleTracks.length;j++){ %>
                  <li id="subtitle_<%= j %>" class="subtitleSelect <%= (subtitleTracks[j].active ? ' active' : '') %>" data-iso="<%= subtitleTracks[j].id %>" data-index="<%= subtitleTracks[j].index %>" data-left='defaultFocus' data-down="focusDown" data-right='defaultFocus' data-up='#subtitle_on'><%= subtitleTracks[j].label %></li>
                <% } %>
                </ul>
                <div id="subtitleSelectionArrowLeft" class="prevSelection"></div>
                <div id="subtitleSelectionArrowRight" class="nextSelection"></div>
              </div>
            </div>
          </div>

          <div id="fontSettingsContainer">
              <div class="font-selection-container">
                  <h1><%- manifest.subtitles_color %></h1>
                  <div class="divider"></div>
                  <ul id="subtitleSelectionList">
                  <% for(var j=0;j<subtitleColors.length;j++){ %>
                    <li id="subtitleColor_<%= j %>" class="subtitleOption <%= (subtitleColors[j].active ? ' active' : '') %>" data-type="color" data-value="<%= subtitleColors[j].color %>" data-left='focusLeftItem' data-right='focusRightItem' data-up='focusUpItem' data-down='focusDownItem'><%= subtitleColors[j].title %></li>
                  <% } %>
                  </ul>
              </div>
              <div class="font-selection-container">
                  <h1><%- manifest.subtitles_text_font_family %></h1>
                  <div class="divider"></div>
                  <ul id="subtitleSelectionList">
                  <% for(var j=0;j<subtitleFont.length;j++){ %>
                    <li id="subtitleFont_<%= j %>" class="subtitleOption <%= (subtitleFont[j].active ? ' active' : '') %>" data-type="font-family" data-value="<%= subtitleFont[j].value %>" data-left='focusLeftItem' data-right='focusRightItem' data-up='focusUpItem' data-down='focusDownItem'><%= subtitleFont[j].title %></li>
                  <% } %>
                  </ul>
              </div>
              <div class="font-selection-container">
                  <h1><%- manifest.subtitles_text_opacity %></h1>
                  <div class="divider"></div>
                  <ul id="subtitleSelectionList">
                  <% for(var j=0;j<subtitleOpacity.length;j++){ %>
                    <li id="subtitleOpacity_<%= j %>" class="subtitleOption <%= (subtitleOpacity[j].active ? ' active' : '') %>" data-type="opacity" data-value="<%= subtitleOpacity[j].value %>" data-left='focusLeftItem' data-right='focusRightItem' data-up='focusUpItem' data-down='focusDownItem'><%= subtitleOpacity[j].title %></li>
                  <% } %>
                  </ul>
              </div>
              <div class="font-selection-container">
                  <h1><%- manifest.subtitles_text_style %></h1>
                  <div class="divider"></div>
                  <ul id="subtitleSelectionList">
                  <% for(var j=0;j<subtitleStyle.length;j++){ %>
                    <li id="subtitleStyle_<%= j %>" class="subtitleOption <%= (subtitleStyle[j].active ? ' active' : '') %>" data-type="text-shadow" data-value="<%= subtitleStyle[j].value %>" data-left='focusLeftItem' data-right='focusRightItem' data-up='focusUpItem' data-down='focusDownItem'><%= subtitleStyle[j].title %></li>
                  <% } %>
                  </ul>
              </div>
              <div class="font-selection-container">
                  <h1><%- manifest.subtitles_text_size %></h1>
                  <div class="divider"></div>
                  <ul id="subtitleSelectionList">
                  <% for(var j=0;j<subtitleSize.length;j++){ %>
                    <li id="subtitleSize_<%= j %>" class="subtitleOption <%= (subtitleSize[j].active ? ' active' : '') %>" data-type="font-size" data-value="<%= subtitleSize[j].value %>" data-left='focusLeftItem' data-right='focusRightItem' data-up='focusUpItem' data-down='focusDownItem'><%= subtitleSize[j].title %></li>
                  <% } %>
                  </ul>
              </div>
          </div>


          <div id="backgroundSettingsContainer">
              <div class="font-selection-container">
                  <h1><%- manifest.subtitles_background_color %></h1>
                  <div class="divider"></div>
                  <ul id="subtitleSelectionList">
                  <% for(var j=0;j<backgroundColor.length;j++){ %>
                    <li id="backgroundColor_<%= j %>" class="backgroundOption backgroundColor  <%= (backgroundColor[j].active ? ' active' : '') %>" data-type="background-color" data-value="<%= backgroundColor[j].color %>" data-left='focusLeftItem' data-right='focusRightItem' data-up='focusUpItem' data-down='focusDownItem'><%= backgroundColor[j].title %></li>
                  <% } %>
                  </ul>
              </div>
              <div class="font-selection-container">
                  <h1><%- manifest.subtitles_window_color %></h1>
                  <div class="divider"></div>
                  <ul id="subtitleSelectionList">
                  <% for(var j=0;j<windowColor.length;j++){ %>
                    <li id="windowColor_<%= j %>" class="backgroundOption windowColor <%= (windowColor[j].active ? ' active' : '') %>" data-type="border-color" data-value="<%= windowColor[j].color %>" data-left='focusLeftItem' data-right='focusRightItem' data-up='focusUpItem' data-down='focusDownItem'><%= windowColor[j].title %></li>
                  <% } %>
                  </ul>
              </div>
              <div class="font-selection-container">
                  <h1><%- manifest.subtitles_background_opacity %></h1>
                  <div class="divider"></div>
                  <ul id="subtitleSelectionList">
                  <% for(var j=0;j<backgroundOpacity.length;j++){ %>
                    <li id="backgroundOpacity_<%= j %>" class="backgroundOption backgroundOpacity <%= (backgroundOpacity[j].active ? ' active' : '') %>" data-type="background-color" data-value="<%= backgroundOpacity[j].value %>" data-left='focusLeftItem' data-right='focusRightItem' data-up='focusUpItem' data-down='focusDownItem'><%= backgroundOpacity[j].title %></li>
                  <% } %>
                  </ul>
              </div>
              <div class="font-selection-container">
                  <h1><%- manifest.subtitles_window_opacity %></h1>
                  <div class="divider"></div>
                  <ul id="subtitleSelectionList">
                  <% for(var j=0;j<windowOpacity.length;j++){ %>
                    <li id="windowOpacity_<%= j %>" class="backgroundOption borderOpacity <%= (windowOpacity[j].active ? ' active' : '') %>" data-type="border-color" data-value="<%= windowOpacity[j].value %>" data-left='focusLeftItem' data-right='focusRightItem' data-up='focusUpItem' data-down='focusDownItem'><%= windowOpacity[j].title %></li>
                  <% } %>
                  </ul>
              </div>
          </div>

          <div id="subtitleExampleField"><div class="background"><div class="subtitleContent"><%- manifest.subtitle_example_text %></div></div></div>

        <% } %>
      </div>
    </div>
    """

    onload: (@audioSettings, tracks=[], active, subtitleOptions, parseType) ->

      @storedSubtitleOptions = subtitleOptions if not null

      @parseType = parseType

      @model = new LanguageOverlayModel()
      @audioInformation= []

      @model.set("OverlayTypeAudio", @audioSettings)
      if @audioSettings
        for track in tracks
          isActive = if active.index is track.index then true else false
          language = track.language
          language = RegionSettings.languageCodeTable[track.language] if track.language in _.keys(RegionSettings.languageCodeTable)
          @audioInformation.push {id:track.language, channels: track.channels, index: track.index, label:language, active:isActive}
        @audioInformation.unshift({id:"NA", "label":Manifest.player_overlay_notavailable, active:true}) if @audioInformation.length is 0
        @model.set("audioTracks", @audioInformation)

      else
        @subtitleInformation= []
        @forcedTrackActive = false
        for track in tracks
          continue if track.language is ""
          language = track.language
          language = RegionSettings.languageCodeTable[language] if language in _.keys(RegionSettings.languageCodeTable)

          #if forced track is present, do not show in selection
          if track.forced is false
            @subtitleInformation.push {id:track.language, label:language, index: track.index, active:(track.language is active.language)}
          else if track.forced is true
            if track.index is active.index
              @forcedTrackActive = true

        #backgroundColor and borderColor have to be separated in order to select the correct values
        if subtitleOptions.backgroundColor.charAt(0) isnt "#" and subtitleOptions.backgroundColor.length > 0
          subtitleOptions.backgroundOpacity = subtitleOptions.backgroundColor.split("(")[1].split(",").pop().slice(0, -1)
          subtitleOptions.backgroundSelectedColor = @rgbToHex(subtitleOptions.backgroundColor).toUpperCase()
        else
          subtitleOptions.backgroundSelectedColor = subtitleOptions.backgroundColor
          subtitleOptions.backgroundOpacity = "1"

        if subtitleOptions.borderColor.charAt(0) isnt "#" and subtitleOptions.borderColor.length > 0
          subtitleOptions.windowOpacity = subtitleOptions.borderColor.split("(")[1].split(",").pop().slice(0, -1)
          subtitleOptions.windowColor = @rgbToHex(subtitleOptions.borderColor).toUpperCase()
        else
          subtitleOptions.windowColor = subtitleOptions.borderColor
          subtitleOptions.windowOpacity = "1"


        for option in @subtitleColors
          if option.color is subtitleOptions.color then option.active = true else option.active = false
        for option in @subtitleOpacity
          if option.value is subtitleOptions.opacity then option.active = true else option.active = false
        for option in @subtitleFont
          if option.value is subtitleOptions.font then option.active = true else option.active = false
        for option in @subtitleStyle
          if option.value is subtitleOptions.style then option.active = true else option.active = false
        for option in @subtitleSize
          if option.value is subtitleOptions.size then option.active = true else option.active = false
        for option in @backgroundColor
          if option.color is subtitleOptions.backgroundSelectedColor then option.active = true else option.active = false
        for option in @backgroundOpacity
          if option.value is subtitleOptions.backgroundOpacity then option.active = true else option.active = false
        for option in @windowColor
          if option.color is subtitleOptions.windowColor then option.active = true else option.active = false
        for option in @windowOpacity
          if option.value is subtitleOptions.windowOpacity then option.active = true else option.active = false


        @model.set("subtitleTracks", @subtitleInformation)

        @model.set("subtitleColors", @subtitleColors)
        @model.set("subtitleOpacity", @subtitleOpacity)
        @model.set("subtitleFont", @subtitleFont)
        @model.set("subtitleSize", @subtitleSize)
        @model.set("subtitleStyle", @subtitleStyle)

        @model.set("backgroundColor", @backgroundColor)
        @model.set("backgroundOpacity", @backgroundOpacity)
        @model.set("windowOpacity", @windowOpacity)
        @model.set("windowColor", @windowColor)

        #if a forced track is active, show as if inactive
        if @forcedTrackActive then @model.set("subtitleActive", false) else @model.set("subtitleActive", active)


    rgbToHex: (rgb) ->
      rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i)
      if rgb and rgb.length == 4 then '#' + ('0' + parseInt(rgb[1], 10).toString(16)).slice(-2) + ('0' + parseInt(rgb[2], 10).toString(16)).slice(-2) + ('0' + parseInt(rgb[3], 10).toString(16)).slice(-2) else ''

    hexToRgb: (hex) ->
      #function returns object {r:value, g:value, b:value}
      shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i
      hex = hex.replace(shorthandRegex, (m, r, g, b) ->
        r + r + g + g + b + b
      )
      result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
      if result
        r: parseInt(result[1], 16)
        g: parseInt(result[2], 16)
        b: parseInt(result[3], 16)
      else
        null


    onRenderComplete: ->
      $(TWC.MVC.View.eventbus).trigger("loading", [false])    #disable loader if it was still running e.g. during error
      $("#popup").addClass("languageOverlay")
      if not @audioSettings
        @showSubtitleSettings()
        if @parseType
          TWC.MVC.Router.setFocus "#subtitle_settings"
          $("#background_settings").removeClass("disabled")
          $("#font_settings").removeClass("disabled")
          if @storedSubtitleOptions
            @updateExampleField()
        else
          TWC.MVC.Router.setFocus "#subtitle_on"
          $("#subtitle_settings").hide()
          $("#subtitleExampleField").hide()
          $("#font_settings").hide()
          $("#background_settings").hide()

      @initSelection()


    resetSubtitleOptions: ->
      return if not @parseType

      #reset to default values
      @storedSubtitleOptions.borderOpacity = null
      @storedSubtitleOptions.backgroundOpacity = null
      @storedSubtitleOptions.borderColor = ""
      @storedSubtitleOptions.backgroundColor = ""
      @storedSubtitleOptions.color = "#FFFFFF"
      @storedSubtitleOptions.opacity = "1"
      @storedSubtitleOptions.size = "30"
      @storedSubtitleOptions.font = "opensans"
      @storedSubtitleOptions.style = "1px 1px 1px #888888"

      @saveSubtitleOption(null, "color", "#FFFFFF")
      @saveSubtitleOption(null, "opacity", "1")
      @saveSubtitleOption(null,"font-size", "30")
      @saveSubtitleOption(null,"font-family", "opensans")
      @saveSubtitleOption(null,"text-shadow", "1px 1px 1px #888888")
      @saveBorderColorOption("border-color", "", false, false)
      @saveBackgroundColorOption("background-color", "", false, false)


    updateExampleField: ->
      #update the example field with the stored subtitle values
      $("#subtitleExampleField .background .subtitleContent").css("color", @storedSubtitleOptions.color)
      $("#subtitleExampleField .background .subtitleContent").css("font-family", @storedSubtitleOptions.font)
      $("#subtitleExampleField .background .subtitleContent").css("font-size", "#{@storedSubtitleOptions.size}px")
      $("#subtitleExampleField .background .subtitleContent").css("opacity", @storedSubtitleOptions.opacity)
      $("#subtitleExampleField .background .subtitleContent").css("text-shadow", @storedSubtitleOptions.style)
      $("#subtitleExampleField .background").css("border-color", @storedSubtitleOptions.borderColor)
      $("#subtitleExampleField .background").css("background-color", @storedSubtitleOptions.backgroundColor)

    showSubtitleSettings: ->
      @activeSubtitleView = 0
      $("#fontSettingsContainer").hide()
      $("#backgroundSettingsContainer").hide()
      $("#subtitleSettingsContainer").show()
      $("#languageSettingsContainer").show()
      $("#overlaySubtitleMenu .active").removeClass("active")
      $("#subtitle_settings").addClass("active")

    showFontSettings: ->
      return if $("#font_settings").hasClass("disabled")
      @activeSubtitleView = 1
      $("#backgroundSettingsContainer").hide()
      $("#subtitleSettingsContainer").hide()
      $("#languageSettingsContainer").hide()
      $("#fontSettingsContainer").show()
      $("#overlaySubtitleMenu .active").removeClass("active")
      $("#font_settings").addClass("active")

    showBackgroundSettings: ->
      return if $("#background_settings").hasClass("disabled")
      @activeSubtitleView = 2
      $("#fontSettingsContainer").hide()
      $("#subtitleSettingsContainer").hide()
      $("#backgroundSettingsContainer").show()
      $("#languageSettingsContainer").hide()
      $("#overlaySubtitleMenu .active").removeClass("active")
      $("#background_settings").addClass("active")

    initSelection: ->
      setTimeout () =>
        $('#popup').addClass('fadein')
        TWC.MVC.Router.setFocus($("#languageSettingsContainer .select:first")) if $("#languageSettingsContainer .select").length
      , 100

      if @audioSettings
        @currentIndexAudioTrack = _.map(@audioInformation, (audioTrack) -> audioTrack.active).indexOf(true)

#        $("#audioSelectionList").css("left", "#{@getScrollPos("#audioSelectionList", @audioInformation, @currentIndexAudioTrack)}px")
        @updateArrows("#audioSelection", $($("#audioSelectionList li").get(@currentIndexAudioTrack)))
        @setFocusLanguageSelector()

      else
        @currentIndexSubtitleTrack = _.map(@subtitleInformation, (subtitleTrack) -> subtitleTrack.active).indexOf(true)
#        $("#subtitleSelectionList").css("left", "#{@getScrollPos("#subtitleSelectionList", @subtitleInformation, @currentIndexSubtitleTrack)}px")
        @updateArrows("#subtitleSelection", $($("#subtitleSelectionList li").get(@currentIndexSubtitleTrack)))

    getScrollPos: (listSelector, trackInfo, selectedIndex) ->
      return
      trackLeftPos = 0
      _.each trackInfo, (value, index) ->
        if index < selectedIndex
          track = $($("#{listSelector} li").get(index))
          trackLeftPos -= (track.outerWidth() + parseInt(track.css("margin-right")))
      trackLeftPos

    onReturn: ->
      @close()

    onCompleted: ->

    close: (callback)->

      @saveSubtitleOptionsConfig(@storedSubtitleOptions) if not @audioSettings
      $('#popup').removeClass('fadein')
      setTimeout ()=>
        $("#popup").removeClass("languageOverlay")
        @unload()
        callback?()
        @onCompleted()
      , 500

    selectAudio: (e) ->
      $el = $(e.target)
      $("#audioSelection li").removeClass("select")
      audioObject = {}
      audioObject.language = $el.attr("data-iso")
      audioObject.index = $el.attr("data-index")
      channels = $el.attr("data-channels")
      audioObject.language = false if $el.attr("data-iso") is "NA"
      @saveAudioConfig(audioObject, channels)
      @close()

    selectSubtitleEnable: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.attr("id") is "subtitle_off"
        $("#subtitle_on").removeClass("active")
        $("#subtitle_off").addClass("active")
        @saveSubtitleConfig(false)
        @close()
      else
        $("#subtitle_off").removeClass("active")
        $("#subtitle_on").addClass("active")
        TWC.MVC.Router.setFocus("#subtitle_0")

    selectSubtitle: (e) ->
      $el = $(e.target)
      $("#subtitleSelection li").removeClass("select")
      if $el.attr("id") is "disabled"
        subtitle = false
      else
        subtitle = []
        subtitle = {"language": $el.attr("data-iso"), "index": $el.attr("data-index")}
      @saveSubtitleConfig(subtitle)
      @close()


    activateSubtitleOption: ->
      $el = $(TWC.MVC.Router.getFocus())
      type = $el.data("type")
      value = $el.data("value")
      @saveSubtitleOption($el, type, value)


    saveSubtitleOption:(element, type, value) ->
      #fill the options array with the selected values
      switch type
        when "color"
          @storedSubtitleOptions.color = value
        when "font-size"
          @storedSubtitleOptions.size = value
        when "opacity"
          @storedSubtitleOptions.opacity = value
        when "text-shadow"
          @storedSubtitleOptions.style = value
        when "font-family"
          @storedSubtitleOptions.font = value


      #update the active state in menu
      @updateFontSettingsSelectedOptions(element, type, value)

      #add the options directly to the example field
      value += "px" if type is "font-size"
      $("#subtitleExampleField .background .subtitleContent").css(type, value)


    updateFontSettingsSelectedOptions: (element, type, value)  ->
      $("[data-type='"+type+"']").removeClass("active")
      $("[data-type='"+type+"'][data-value='" + value + "']").addClass("active")


    activateBackgroundOption:  ->
      $el = $(TWC.MVC.Router.getFocus())
      type = $el.data("type")
      value = $el.data("value")

      if $el.hasClass("borderOpacity") then borderOpacity = true else false
      if $el.hasClass("backgroundOpacity") then backgroundOpacity = true else false

      @saveBackgroundColorOption(type, value, borderOpacity, backgroundOpacity) if type is "background-color"
      @saveBorderColorOption(type, value, borderOpacity, backgroundOpacity) if type is "border-color"


    saveBorderColorOption: (type, value, borderOpacity, backgroundOpacity) ->

      #get the color values to set the rgba if opacity is present
      if borderOpacity
        if $("#subtitleExampleField .background").css("border-left-color").split("(").length > 1
          color = $("#subtitleExampleField .background").css("border-left-color").split("(")[1].slice(0, - 1).split(",")
          @storedSubtitleOptions.borderOpacity = value
          rgbValue = "rgba("+color[0]+","+color[1]+","+color[2]+","+value+")"
          @storedSubtitleOptions.borderColor = rgbValue if @storedSubtitleOptions.borderColor
        @updateBorderSelectedOptions()
      else
        if @storedSubtitleOptions.borderOpacity and value
          rgbValue = @hexToRgb(value)
          rgbValue = "rgba("+rgbValue.r+","+rgbValue.g+","+rgbValue.b+","+@storedSubtitleOptions.borderOpacity+")"
          @storedSubtitleOptions.borderColor = rgbValue
        else
          @storedSubtitleOptions.borderOpacity = "1"
          @storedSubtitleOptions.borderColor = value
        @updateBorderSelectedOptions()

      @updateExampleField()


    saveBackgroundColorOption: (type, value, borderOpacity, backgroundOpacity) ->

      #get the color values to set the rgba if opacity is present
      if backgroundOpacity
        if $("#subtitleExampleField .background").css("background-color").split("(").length > 1
          color = $("#subtitleExampleField .background").css("background-color").split("(")[1].slice(0, - 1).split(",")
          @storedSubtitleOptions.backgroundOpacity = value
          rgbValue = "rgba("+color[0]+","+color[1]+","+color[2]+","+value+")"
          @storedSubtitleOptions.backgroundColor = rgbValue if @storedSubtitleOptions.backgroundColor
        @updateBackgroundSelectedOptions()
      else
        if @storedSubtitleOptions.backgroundOpacity and value
          rgbValue = @hexToRgb(value)
          rgbValue = "rgba("+rgbValue.r+","+rgbValue.g+","+rgbValue.b+","+@storedSubtitleOptions.backgroundOpacity+")"
          @storedSubtitleOptions.backgroundColor = rgbValue
        else
          @storedSubtitleOptions.backgroundOpacity = "1"
          @storedSubtitleOptions.backgroundColor = value
        @updateBackgroundSelectedOptions()

      @updateExampleField()


    updateBackgroundSelectedOptions: ->
      if @storedSubtitleOptions.backgroundColor.charAt(0) isnt "#" and @storedSubtitleOptions.backgroundColor.length > 0
        backgroundOpacity = @storedSubtitleOptions.backgroundColor.split("(")[1].split(",").pop().slice(0, -1)
        backgroundSelectedColor = @rgbToHex(@storedSubtitleOptions.backgroundColor).toUpperCase()
      else
        backgroundSelectedColor = @storedSubtitleOptions.backgroundColor
        backgroundOpacity = @storedSubtitleOptions.backgroundOpacity or "1"

      $("[data-type='background-color']").removeClass("active")
      $(".backgroundColor[data-value='" + backgroundSelectedColor + "']").addClass("active")
      $(".backgroundOpacity[data-value='" + backgroundOpacity + "']").addClass("active")


    updateBorderSelectedOptions: ->
      if @storedSubtitleOptions.borderColor.charAt(0) isnt "#" and @storedSubtitleOptions.borderColor.length > 0
        windowOpacity = @storedSubtitleOptions.borderColor.split("(")[1].split(",").pop().slice(0, -1)
        windowColor = @rgbToHex(@storedSubtitleOptions.borderColor).toUpperCase()
      else
        windowColor = @storedSubtitleOptions.borderColor
        windowOpacity =  @storedSubtitleOptions.borderOpacity or "1"

      $("[data-type='border-color']").removeClass("active")
      $(".windowColor[data-value='" + windowColor + "']").addClass("active")
      $(".borderOpacity[data-value='" + windowOpacity + "']").addClass("active")


    saveAudioConfig : (audio, channels)=>

    saveSubtitleConfig : (subtitle)=>

    setFocusLanguageSelector: ->
      focus = if $($("#audioSelectionList li").get(@currentIndexAudioTrack)).length then $($("#audioSelectionList li").get(@currentIndexAudioTrack)) else $("#audioSelection li:first")
      TWC.MVC.Router.setFocus(focus)

    setFocusSubtitleSelector: ->
      focus = if $($("#subtitleSelectionList li").get(@currentIndexSubtitleTrack)).length then $($("#subtitleSelectionList li").get(@currentIndexSubtitleTrack)) else $("#subtitleSelectionList li:first")
      TWC.MVC.Router.setFocus(focus)

    audioTrackFocusRight: ->
      @scrollAndFocus("#audioSelection", "#audioSelectionList", @currentIndexAudioTrack, "right", => @currentIndexAudioTrack++)

    audioTrackFocusLeft: ->
      @scrollAndFocus("#audioSelection", "#audioSelectionList", @currentIndexAudioTrack, "left", => @currentIndexAudioTrack--)

    subtitleTrackFocusRight: ->
      @scrollAndFocus("#subtitleSelection", "#subtitleSelectionList", @currentIndexSubtitleTrack, "right", => @currentIndexSubtitleTrack++)

    subtitleTrackFocusLeft: ->
      @scrollAndFocus("#subtitleSelection", "#subtitleSelectionList", @currentIndexSubtitleTrack, "left", => @currentIndexSubtitleTrack--)

    scrollAndFocus: (listParentSelector, listSelector, curIndex, dir, callback) ->
      $currentFocus = $($("#{listSelector} li").get(curIndex))
      $next = if dir is "left" then $currentFocus.prev("li") else $currentFocus.next("li")
      if $next.length

        currentleftPos = parseInt($(listSelector).css("left"))

        if dir is "left"
          newPos = $currentFocus.outerWidth() + parseInt($currentFocus.css("margin-right"))
          currentleftPos += newPos
          callback?()
        else
          newPos = $next.outerWidth() + parseInt($next.css("margin-right"))
          currentleftPos -= newPos
          callback?()

        TWC.MVC.Router.setFocus($next)
        @updateArrows(listParentSelector, $next)

    updateArrows: (listParentSelector, $el) ->
      if $el.next("li").length is 0
        $("#{listParentSelector} .nextSelection").hide()
      else
        $("#{listParentSelector} .nextSelection").show()

      if $el.prev("li").length is 0
        $("#{listParentSelector} .prevSelection").hide()
      else
        $("#{listParentSelector} .prevSelection").show()


    focusDownItem: ->
      $el = $(TWC.MVC.Router.getFocus())
      option = $el.attr("id").split("_")[0]
      optionId = parseInt($el.attr("id").split("_")[1])

      switch option
        when "subtitleColor"
          TWC.MVC.Router.setFocus("#subtitleColor_6") if optionId is 0
          TWC.MVC.Router.setFocus("#subtitleColor_7") if optionId in [1,2,3,4,5]
          TWC.MVC.Router.setFocus("#subtitleOpacity_0") if optionId in [6,7]

        when "subtitleOpacity"
          TWC.MVC.Router.setFocus("#subtitleSize_0")
        when "subtitleFont"
          TWC.MVC.Router.setFocus("#subtitleFont_4") if optionId in [0,1,2,3]
          TWC.MVC.Router.setFocus("#subtitleStyle_0") if optionId in [4]
        when "subtitleStyle"
          TWC.MVC.Router.setFocus("#subtitleStyle_4") if optionId in [0, 1,2,3]

        when "backgroundColor"
          TWC.MVC.Router.setFocus("#backgroundColor_7") if optionId is 0
          TWC.MVC.Router.setFocus("#backgroundColor_8") if optionId in [1,2,3,4,5,6]
          TWC.MVC.Router.setFocus("#backgroundOpacity_0") if optionId in [7,8]
        when "windowColor"
          TWC.MVC.Router.setFocus("#windowColor_7") if optionId is 0
          TWC.MVC.Router.setFocus("#windowColor_8") if optionId in [1,2,3,4,5,6]
          TWC.MVC.Router.setFocus("#windowOpacity_0") if optionId in [7,8]


    focusUpItem: ->
      $el = $(TWC.MVC.Router.getFocus())
      option = $el.attr("id").split("_")[0]
      optionId = parseInt($el.attr("id").split("_")[1])

      switch option
        when "subtitleColor"
          TWC.MVC.Router.setFocus("#subtitle_settings") if optionId <= 5
          TWC.MVC.Router.setFocus("#subtitleColor_0") if optionId is 6
          TWC.MVC.Router.setFocus("#subtitleColor_1") if optionId is 7
        when "subtitleOpacity"
          TWC.MVC.Router.setFocus("#subtitleColor_7")
        when "subtitleSize"
          TWC.MVC.Router.setFocus("#subtitleOpacity_0")
        when "subtitleFont"
          TWC.MVC.Router.setFocus("#subtitleFont_0") if optionId is 4
          TWC.MVC.Router.setFocus("#subtitle_settings") if optionId <= 3
        when "subtitleStyle"
          TWC.MVC.Router.setFocus("#subtitleFont_4") if optionId <= 3
          TWC.MVC.Router.setFocus("#subtitleStyle_0") if optionId is 4
        when "backgroundColor"
          TWC.MVC.Router.setFocus("#backgroundColor_0") if optionId is 7
          TWC.MVC.Router.setFocus("#backgroundColor_1") if optionId is 8
          TWC.MVC.Router.setFocus("#subtitle_settings") if optionId in [0,1,2,3,4,5,6]
        when "backgroundOpacity"
          TWC.MVC.Router.setFocus("#backgroundColor_8")
        when "windowColor"
          TWC.MVC.Router.setFocus("#background_settings") if optionId in [0,1,2,3,4,5,6]
          TWC.MVC.Router.setFocus("#windowColor_0") if optionId is 7
          TWC.MVC.Router.setFocus("#windowColor_1") if optionId is 8
        when "windowOpacity"
          TWC.MVC.Router.setFocus("#windowColor_7")

    focusRightItem: ->
      $el = $(TWC.MVC.Router.getFocus())
      option = $el.attr("id").split("_")[0]
      optionId = parseInt($el.attr("id").split("_")[1])

      switch option
        when "subtitleColor"
          TWC.MVC.Router.setFocus("#subtitleFont_0") if optionId in [5,7]
          TWC.MVC.Router.setFocus("#subtitleColor_#{optionId + 1}") if optionId in [0,1,2,3,4,6]
        when "subtitleSize"
          TWC.MVC.Router.setFocus("#subtitleSize_#{optionId + 1}") if optionId in [0,1,2,3,4]
          TWC.MVC.Router.setFocus("#subtitleStyle_0") if optionId is 5
        when "subtitleStyle"
          TWC.MVC.Router.setFocus("#subtitleStyle_#{optionId + 1}") if optionId in [0,1,2]
        when "subtitleOpacity"
          TWC.MVC.Router.setFocus("#subtitleOpacity_#{optionId + 1}") if optionId in [0,1,2,3]
          TWC.MVC.Router.setFocus("#subtitleStyle_0") if optionId is 4
        when "subtitleFont"
          TWC.MVC.Router.setFocus("#subtitleFont_#{optionId + 1}") if optionId in [0,1,2]

        when "backgroundColor"
          TWC.MVC.Router.setFocus("#backgroundColor_#{optionId + 1}") if optionId in [0,1,2,3,4,5,7]
          TWC.MVC.Router.setFocus("#windowColor_0") if optionId in [6,8]
        when "windowColor"
          TWC.MVC.Router.setFocus("#windowColor_#{optionId + 1}") if optionId in [0,1,2,3,4,5,7]
        when "backgroundOpacity"
          TWC.MVC.Router.setFocus("#backgroundOpacity_#{optionId + 1}") if optionId in [0,1,2,3]
          TWC.MVC.Router.setFocus("#windowOpacity_0") if optionId is 4
        when "windowOpacity"
          TWC.MVC.Router.setFocus("#windowOpacity_#{optionId + 1}") if optionId in [0,1,2,3]



    focusLeftItem: ->
      $el = $(TWC.MVC.Router.getFocus())
      option = $el.attr("id").split("_")[0]
      optionId = parseInt($el.attr("id").split("_")[1])

      switch option
        when "subtitleColor"
          TWC.MVC.Router.setFocus("#subtitleColor_#{optionId - 1}") if optionId in [1,2,3,4,5,7]
        when "subtitleSize"
          TWC.MVC.Router.setFocus("#subtitleSize_#{optionId - 1}") if optionId in [1,2,3,4,5]
        when "subtitleStyle"
          TWC.MVC.Router.setFocus("#subtitleOpacity_4") if optionId in [0,4]
          TWC.MVC.Router.setFocus("#subtitleStyle_#{optionId - 1}") if optionId in [1,2,3,5]
        when "subtitleOpacity"
          TWC.MVC.Router.setFocus("#subtitleOpacity_#{optionId - 1}") if optionId in [1,2,3,4,5]
        when "subtitleFont"
          TWC.MVC.Router.setFocus("#subtitleColor_5") if optionId in [0,4]
          TWC.MVC.Router.setFocus("#subtitleFont_#{optionId - 1}") if optionId in [1,2,3]

        when "backgroundColor"
          TWC.MVC.Router.setFocus("#backgroundColor_#{optionId - 1}") if optionId in [1,2,3,4,5,6,8]
        when "windowColor"
          TWC.MVC.Router.setFocus("#backgroundColor_6") if optionId in [0,7]
          TWC.MVC.Router.setFocus("#windowColor_#{optionId - 1}") if optionId in [1,2,3,4,5,6,8]
        when "backgroundOpacity"
          TWC.MVC.Router.setFocus("#backgroundOpacity_#{optionId - 1}") if optionId in [1,2,3,4,5]
        when "windowOpacity"
          TWC.MVC.Router.setFocus("#windowOpacity_#{optionId - 1}") if optionId in [1,2,3,4]
          TWC.MVC.Router.setFocus("#backgroundOpacity_4") if optionId is 0



    focusDownFromMenu: ->
      $el = $(TWC.MVC.Router.getFocus())
      if @activeSubtitleView is 0
        TWC.MVC.Router.setFocus("#subtitle_off")
      if @activeSubtitleView is 1
        TWC.MVC.Router.setFocus("#subtitleColor_0")
      if @activeSubtitleView is 2
        TWC.MVC.Router.setFocus("#backgroundColor_0")

    focusDown: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("subtitleEnabledSelect")
        TWC.MVC.Router.setFocus "#" + $(".subtitleSelect:first").attr('id')
      else if $el.hasClass("subtitleSelect")
        TWC.MVC.Router.setFocus("#resetSubtitleOptions")
      else if $el.hasClass("subtitleMenu")
        if @activeSubtitleView is 0
          TWC.MVC.Router.setFocus("#subtitle_off")
        if @activeSubtitleView is 1
          TWC.MVC.Router.setFocus("#subtitleColor_0")
        if @activeSubtitleView is 2
          TWC.MVC.Router.setFocus("#backgroundColor_0")


    focusUp: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("subtitleEnabledSelect")
        TWC.MVC.Router.setFocus("#subtitle_settings")
      if $el.hasClass("subtitleSelect")
        TWC.MVC.Router.setFocus "#" + $(".subtitleEnabledSelect:first").attr('id')
      if $el.hasClass("subtitleOptionsReset")
        TWC.MVC.Router.setFocus "#" + $(".subtitleSelect:first").attr('id')


