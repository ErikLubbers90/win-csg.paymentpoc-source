define [
  "framework",
  "application/screens/videoplayer/togglecontrols",
  "application/helpers/audiomanager",
  "application/helpers/audiomanagerHelper",
  "application/popups/error/error",
  "application/popups/generic/alert",
  "application/screens/videoplayer/basevideoplayer",
  "../../api/cd/models/CDProduct",
  "config",
  "i18n!../../../lang/nls/manifest",
  "i18n!../../../lang/nls/regionsettings",
  "./languageOverlay"
  "../../api/cd/CDBaseRequest",
  "jquery"
], (TWC, ToggleControls, AudioManager, AudioManagerHelper, Error, Alert, VideoPlayer, CDProductModel, Config, Manifest, RegionSettings, LanguageOverlay, CDBaseRequest, $) ->

  class extraVideoplayerModel extends TWC.MVC.Model
    uid: 'player-extravideoplayer-model'
    data: {
      menu: []
    }


  class extraVideoplayerView extends VideoPlayer
    uid: 'extravideoplayer-view'
    el: '#screen'
    type: 'page'

    isTrailer: false
    startTime: 0

    chapters: []
    chaptersLoaded: false
    chaptersAvailable: false
    chapterTimeout: null
    doubleClickTimer: null

    fromMainPlayer: false
    stopped: false
    chaining: false
    extraSubtitles: false

    currentSeekerProgressTime: 0

    selectedLangaugeIso: null
    selectedSubtitleIso: null
    subtitlesActive: false

    leanbackEnabled: false
    autoplayEnalbed: false


    model: new extraVideoplayerModel()
    defaultFocus: "#play"
    events:
      "_play"                 : "rc_play"
      "_pause"                : "rc_pause"
      "play_pause"            : "rc_play_pause"
      "stop"                  : "rc_stop"
      "forward"               : "rc_forward"
      "rewind"                : "rc_rewind"
      "fastforward"           : "rc_fastforward"
      "fastrewind"            : "rc_fastrewind"
      "cc"                    : "showSubtitleSettings"
      "enter #play"           : "togglePause"
      "enter #stop"           : "stop"
      "enter #forward"        : "forward"
      "enter #rewind"         : "rewind"
      "enter #subs"           : "showSubtitleSettings"
      "enter #audio"          : "showAudioSettings"
      "enter #extras"         : "toggleExtras"
      "enter #forwardChapter" : "forwardChapter"
      "enter #rewindChapter"  : "rewindChapter"
      "enter #close-button"   : "closeVideo"
      "enter #videoWrapper"   : "hideAutoplay"
      "enter #triviaVideo"    : "openTriviaVideo"
      "enter #playerBackButton" : "onReturn"
      "click .chapter-point" : "clickChapterPoint"
      "enter #chapter-thumb-container" : "goToSelectedChapter"

    onReturn: ->
      #change return function based on playback type
      if @chaining and @playlistCount > 0
        $("#extrasBackground").addClass("visible")
        _CPE = @CPE
        TWC.MVC.History.popState()
        @destroyPlayer() #delete the player, then start CPE again
        setTimeout ()=>
          _CPE.enable()
        , 500
      else if @fromMainPlayer
        TWC.MVC.Router.load 'videoplayer-view',
          data: [@productModel, false, false, null, @messages, 0, "", false, @resumeTime, true]
      else
        @returnCallback?()

    onload: (@playerId, @videoArray, @productModel, @CPE = null, @resumeTime = 0, @returnCallback = null, @fromMainPlayer = false, @playlistCount = 0) ->
      @akamaiTracker = null

      @extraVideoPlayer = true
      @chaining = false
      @stopped = false
      isRelatedMedia = false

      #setup and get correct video url for multiple video reel
      if @videoArray.length > 1
        @chaining = true
        videoUrl = @videoArray[parseInt(@playlistCount)].stream[0].url
      else if @videoArray[0].stream[0].variant
        isRelatedMedia = @videoArray[0].stream[0].variant
        videoUrl = "alternative"
      else
        videoUrl = @videoArray[0].stream[0].url

      @akamaiUrl = videoUrl
      #check for subtitles included in the array
      if @videoArray[parseInt(@playlistCount)].subtitles?
        @extraSubtitles = true

      @isTrailer = false
      @isExtra = false
      @videoTitle = ""
      @videoYear = ""
      @audioTrackChannels = []
      @messages = []
      @chaptersLoaded = false
      @chaptersAvailable = false
      @leanbackEnabled = false
      @zones = []

      # Keep loading indicator active until player is ready and playing (or in case of error)
      $(TWC.MVC.View.eventbus).trigger('loading', [true, "keep"])

      if videoUrl.toLowerCase() in ["trailer", "movie", "alternative"]
        @isExtra = false
        if videoUrl.toLowerCase() is "trailer"
          @isTrailer = true

        @showResumePopup @productModel.get("ContentProgressSeconds"), (resumeTime) =>
          $(TWC.MVC.View.eventbus).trigger('loading', [true, "keep"])

          @productModel.getPlaybackInformation((error, model) =>
            if not error
              model.getMetaInformation (error, chapters, effectiveEndSeconds)=>
                if not error
                  @chapters = chapters
                  if @chapters.length > 0 and not @isTrailer
                    @chaptersAvailable = true
                    $("#forwardChapter").removeClass("disable")
                    $("#rewindChapter").removeClass("disable")
                  @videoTitle = @productModel.get("Name")
                  @videoYear = @productModel.get("ReleaseDate")
                  @startTime = parseInt(model.get("ProgressSeconds")) or 0

                  @effectiveEndSeconds = effectiveEndSeconds

                  playerConfig = {}
                  playerConfig.url = model.get("ContentUrl")
                  playerConfig.mime = switch true
                    when /.mp4/.test(playerConfig.url) then "video/mp4"
                    when /.ism/.test(playerConfig.url) then "application/vnd.ms-sstr+xml"
                    when /.m3u8/.test(playerConfig.url) then "application/vnd.apple.mpegurl"
                    when /.mpd/.test(playerConfig.url) then "application/dash+xml"
                    else "video/mp4"

                  if not @isTrailer
                    playerConfig.startTime = resumeTime

                  if model.get("LicenseRequestToken")
                    @isDRM = true
                    playerConfig.drm = {
                      type: "playready"
                      parameters: {
                        playReadyInitiatorUrl: model.get("LicenceRequestUrl")
                        customData: model.get("LicenseRequestToken")
                        webInitiatorUrl:"http://test.sony.twcapps.com/webinitiator.php"
                      }
                    }

                  @startPlayback(playerConfig, model, true)
                else
                  $(TWC.MVC.View.eventbus).trigger('loading', [false, "reset"])
                  messageAlert = new Alert()
                  if error.Message?
                    messageAlert.load
                      data: [Manifest.popup_error_title, error.Message, Manifest.popup_exit_close]
          , @isTrailer, isRelatedMedia)

      else
        @isExtra = true
        @videoTitle = ""
        @startTime = 0

        @playerConfig = {}
        @playerConfig.url = videoUrl
        @playerConfig.drm = null
        @playerConfig.mime = switch true
          when /.mp4/.test(@playerConfig.url) then "video/mp4"
          when /.ism/.test(@playerConfig.url) then "application/vnd.ms-sstr+xml"
          when /.m3u8/.test(@playerConfig.url) then "application/vnd.apple.mpegurl"
          when /.mpd/.test(@playerConfig.url) then "application/dash+xml"
          else "video/mp4"


    checkForSubtitles: ->
      result = @videoArray[parseInt(@playlistCount)].subtitles
      @parseType = false
      if result and result.length > 0
        textTracks = []
        for textTrack,index in result
          textTracks.push {"language":textTrack.language, "url": textTrack.url, "index": index, "forced": false }

        parserType = textTrack.url
        parserType = parserType.substring(parserType.lastIndexOf(".")+1);

        parserType = "ttml" if parserType in ["dfxp", "xml", "itt", "txt"]
        @parseType = parserType

        storage = TWC.SDK.Storage.get()
        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_LANGUAGE) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_LANGUAGE, RegionSettings.subtitle_language)
        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_ACTIVE) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_ACTIVE, JSON.stringify(RegionSettings.subtitle_active))
        if storage.getItem(TWC.SDK.Player.STORAGE_AUDIO_LANGUAGE) is null then storage.setItem(TWC.SDK.Player.STORAGE_AUDIO_LANGUAGE, RegionSettings.audio_language)

        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_COLOR) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_COLOR, "#FFFFFF")
        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_FONT) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_FONT, "opensans")
        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_STYLE) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_STYLE, "1px 1px 1px #888888")
        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_SIZE) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_SIZE, "30")
        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_OPACITY) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_OPACITY, "1")

        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_BORDER_COLOR) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_BORDER_COLOR, "")
        if storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_BACKGROUND_COLOR) is null then storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_BACKGROUND_COLOR, "")

        subtitleOptions = {}
        subtitleOptions.language = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_LANGUAGE)
        subtitleOptions.active = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_ACTIVE)
        subtitleOptions.color = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_COLOR)
        subtitleOptions.font = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_FONT)
        subtitleOptions.style = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_STYLE)
        subtitleOptions.size = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_SIZE)
        subtitleOptions.opacity = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_OPACITY)
        subtitleOptions.borderColor = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_BORDER_COLOR)
        subtitleOptions.backgroundColor = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_BACKGROUND_COLOR)

        # use html5 renderer
        if Config.platform in ["androidtv"]
          require [
            "vendor/subtitles/external_subtitles",
            "vendor/subtitles/external_subtitles_#{parserType}_parser"], (ExSubtitles, Parser)=>
            @player = _.extend(@player, ExSubtitles, Parser)
            @player.setExternalSubtitles(textTracks)
            @setSubtitleOptions(subtitleOptions)
            @player.subtitleActive(true) if subtitleOptions.active in [true, "true"]
            @verifyCaptionAndAudioSupport()


    destroyPlayer: ->
      #unsubscribe from player waiting event
      $(TWC.MVC.View.eventbus).off('.videoPlayer')
      $(TWC.MVC.View.eventbus).trigger('loading', [false, "reset"])

      clearTimeout(@toggle.options.timeoutId) if @toggle?
      clearInterval(@updateContentProgressInterval)
      clearInterval(@progressSeekInterval)

      @currentSeekerProgressTime = null
      @updateContentProgressInterval = null
      @progressSeekInterval = null
      @skipToSeekTime = null
      @playerId = null
      @chaining = false

      TWC.SDK.Input.get().removeFromBlocked("red", "green", "blue", "yellow")

      @player?.stop()
      @player?.destroy()

      $("body").removeClass("nobackground")
      @player = null


    startNewExtrasVideo: (direction) ->
      if direction is "next"
        nextVideo = @playlistCount + 1
        return @stop(null, true) if @videoArray.length is nextVideo
      else #previous
        return @stop(null, true) if @playlistCount is 0
        nextVideo = @playlistCount - 1

      TWC.MVC.Router.load 'extravideoplayer-view',
        data: [@playerId, @videoArray, @productModel, @CPE, 0, @returnCallback, false, nextVideo]
        callback: ()->
          #remove all history till product detail page, don't want to store a reel
          TWC.MVC.History.reset(TWC.MVC.History.size()-2)


    setSubscribes: ->
      $(TWC.MVC.View.eventbus).on 'player.onWaiting.videoPlayer', (e)=>
        if !@updateVideoStillInterval
          $(TWC.MVC.View.eventbus).trigger('loading', true)

      $(TWC.MVC.View.eventbus).on 'player.onInit.videoPlayer', (e)=>
        $(TWC.MVC.View.eventbus).trigger('loading', true)
        @updateTime(0)

      $(TWC.MVC.View.eventbus).on "player.onReady.videoPlayer", (e) =>
        log "onready"
        @player.aspectRatio("original")

        # Workaround missing subtitles menu
        setTimeout =>
          @verifyCaptionAndAudioSupport() if not @isTrailer and not @isExtra
        , 1000

        setTimeout =>
          @initAkamaiTracker()
        , 1


      $(TWC.MVC.View.eventbus).on 'player.onProgress.videoPlayer', (e, time)=>
        @updateTime(time)
        if @CPE?
          @CPE.player_onProgress @playerId, time

      $(TWC.MVC.View.eventbus).on 'player.onPlay.videoPlayer', (e)=>
        if @updateVideoStillInterval
          return @pause()
        $(TWC.MVC.View.eventbus).trigger('loading', [false, "reset"])
        $('#play').addClass('pause')
        if @CPE?
          @CPE.player_onPlayStateChange @playerId, true

        if @akamaiTracker?
          if not @started
            @akamaiTracker.handlePlaying()
            @started = true
          else
            @akamaiTracker.handleResume()


      $(TWC.MVC.View.eventbus).on 'player.onPause.videoPlayer', (e)=>
        $(TWC.MVC.View.eventbus).trigger('loading', false)
        $('#play').removeClass('pause')
        if @CPE?
          @CPE.player_onPlayStateChange @playerId, false
        if @akamaiTracker?
          @akamaiTracker.handlePause()

      # on video finnish correct focus and time display
      $(TWC.MVC.View.eventbus).on "player.onEnded.videoPlayer", =>
        log "videoplayer onended"
        clearInterval(@updateContentProgressInterval)
        @updateContentProgressInterval = null

        $($('#vpVideoTime span')[0]).html "00:00"
        $('#vpVideoDuration').html "00:00"
        $('#videoProgress').css("width", "0%")

        #start next video from the videoArray if present
        return @startNewExtrasVideo("next") if @chaining and not @stopped

        if @akamaiTracker?
          @akamaiTracker.handlePlayEnd('Play.End.Detected')
        #return when playback has ended
        @onReturn()

      $(TWC.MVC.View.eventbus).on "player.onStop.videoPlayer", =>
        clearInterval(@updateContentProgressInterval)
        @updateContentProgressInterval = null
        if @akamaiTracker?
          @akamaiTracker.handlePlayEnd('Play.Stop.Detected')
        @onReturn() if not @chaining or (@chaining and @playlistCount is 0)

      # error handling
      $(TWC.MVC.View.eventbus).on 'player.onError.videoPlayer', (err) =>
        log "video error: ", "" + err
        clearInterval(@updateContentProgressInterval)
        @updateContentProgressInterval = null
        $(TWC.MVC.View.eventbus).trigger('loading', [false, "reset"])
        $(TWC.MVC.View.eventbus).trigger 'error',
          type: "player"
          msg: err
        if @akamaiTracker?
          @akamaiTracker.handleError(err)

      # Handle resolution change
      $(TWC.MVC.View.eventbus).on 'player.onResolutionChanged.videoPlayer', (e, width, height) =>

        hdrText = ""
        if not @isExtra and not @isTrailer
          entitledPricingPlan = @productModel.get("PricingPlans").findWhere({ Id : @productModel.get("EntitledPricingPlanId")})
          platformUtils = TWC.SDK.Utils.get()
          if entitledPricingPlan.isHDR()
            if platformUtils.isHDR()
              hdrText = " +HDR"

        switch height
          when "2160"
            resolution = "4K UHD" + hdrText
          when "1080"
            resolution = "HD" + hdrText
          when "720"
            resolution = "HD"
          when "480","360"
            resolution = "SD"
        $(".quality").text(resolution)

      # error handling

      $(TWC.MVC.View.eventbus).on 'player.subtitleOff.videoPlayer', (e, subtitle) =>
        @processSubtitle(false, subtitle)

      $(TWC.MVC.View.eventbus).on 'player.subtitleOn.videoPlayer', (e, subtitle) =>
        @processSubtitle(true, subtitle)

      $(TWC.MVC.View.eventbus).on 'player.triviaMessageOff.videoPlayer', (e, message) =>
        @processTriviaMessage(false, message)

      $(TWC.MVC.View.eventbus).on 'player.triviaMessageOn.videoPlayer', (e, message) =>
        return if not @extrasEnabled
        @processTriviaMessage(true, message)

    stop: (e, force=false)->
      return @toggle.autoHide() if @isControlsHidden() and not force
      #stop and go back to beginning when in chaining event
      return @onReturn() if @chaining and @playlistCount > 0

      if @player
        @sendUpdateContentProgressRequest(true)
        @stopped = true
        @player.stop()
      else
        @onReturn()

    rc_fastrewind: ->
      if @chaining
        @startNewExtrasVideo("previous")
      else
        super

    rc_fastforward: ->
      if @chaining
        @startNewExtrasVideo("next")
      else
        super

    openTriviaVideo: ()->
      videoUrl = $(TWC.MVC.Router.getFocus()).attr("data-url")
      objArray = []
      videoObject = {}
      videoObject.url = videoUrl
      objArray.push(videoObject)
      videoArray = [{"stream":objArray}];

      #store the resume time, and continue the video when user returns
      @resumeTime = @currentTime
      @updateHistory(@playerId, @videoUrl, @productModel, @CPE, @resumeTime, @returnCallback, false)

      TWC.MVC.Router.load 'extravideoplayer-view',
        data: [null, videoArray, @productModel, @CPE, 0, ()=>
          TWC.MVC.History.popState()
        ]

  extraVideoplayerView.get()
