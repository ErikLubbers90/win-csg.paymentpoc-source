define ["framework",
"jquery",
"config", 
"underscore"],
(TWC, $, Config, _) ->

  class toggleControls extends TWC.SDK.Base

    # **options** (object)
    defaults: 
      timeoutId: null
      timeoutTime: 5000
      isHidden: false
      keepShowing: false
      enabled: true
      logo: null
      vpControl: null
      vpHeader: null
      sensitiveButtons: [
        'enter'
        'right'
        'left'
        'up'
        '_play'
        '_pause'
        'stop'
        'rewind'
        'forward'
        'fastrewind'
        'fastforward'
        '0'
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
      ]
      activeOnHide: null
      activeOnShow: null
      enabled: true
    
    # **constructor** (function)
    # *[options: object]*  
    # set options for toggling and initialize it
    constructor: (options) ->
      @options = _.defaults(options, @defaults)
      
      # bind autohide funtion for each button in sensitive button parameter
      for item in @options.sensitiveButtons
        $('body').on "#{item}.toggleControls", =>
          @autoHide()

      $('body').on "up.toggleControls", =>
        return if @options.keepShowing
        if @doubleUpTimeout
          return @hide()
        @doubleUpTimeout = setTimeout =>
          clearTimeout(@doubleUpTimeout)
          @doubleUpTimeout = null
        , 1000

        if @options.isHidden
          TWC.MVC.Router.setFocus("#subs") if not $("#subs").hasClass("disable")
          TWC.MVC.Router.setFocus("#audio") if not $("#audio").hasClass("disable")
          TWC.MVC.Router.setFocus("#extra") if not $("#extra").hasClass("disable")


      $('body').on "down.toggleControls", =>
        return if @options.keepShowing
        if @doubleDownTimeout
          return @hide()
        @doubleDownTimeout = setTimeout =>
          clearTimeout(@doubleDownTimeout)
          @doubleDownTimeout = null
        , 1000


      # add mouse move event detection for LG after the player     
      $('body').on "mousemove.toggleControls", =>
        @autoHide()

      # if previous video had hidden controls hide them now too
      if @options.isHidden
        @hide()
        
      # activate autohide
      else
        @autoHide()

      # show controls when page is visible again (restored)
      $(document).on 'visibilitychange.toggleControls', =>

        if document.hidden
          log "TOGGLECONTROLS: Suspend"
        else
          log "TOGGLECONTROLS: Restore"
          @autoHide()

    # **destroy** (function)
    destroy: -> 
      @show()
      
      #unbind events to sensitive buttons and mouse
      $('body').off(".toggleControls")
      $(document).off(".toggleControls")

      #clear the timers
      clearTimeout(@options.timeoutId)

    # **show** (function)
    show: ->
      @options.keepShowing = true
      clearTimeout(@options.timeoutId) if @options.timeoutId
      if @options.isHidden
        @options.vpControl.removeClass("hide")
#        @options.vpControl.css({
#          "-o-transform":"translate(0px, 0px)"
#          "-webkit-transform":"translate3d(0px, 0px, 0)"})
        @options.vpHeader.css({
          "-o-transform":"translate(0px, 0px)"
          "-webkit-transform":"translate3d(0px, 0px, 0)"})
#        $("#overlay").css("background-color":"rgba(0,0,0,0.5)")
        #set focus on activeOnShow
        if @options.activeOnShow
          TWC.MVC.Router.setActive @options.parent #TODO: hardcoded
          TWC.MVC.Router.setSelect "#"+@options.activeOnShow, true

        @options.isHidden = false

    # **hide** (function)
    hide: ->
      @options.keepShowing = false
      @options.vpControl.addClass("hide")
#      @options.vpControl.css({
#        "-o-transform":"translate(0px, 1100px)"
#        "-webkit-transform":"translate3d(0px, 1100px, 0)"
#      })
      @options.vpHeader.css({
        "-o-transform":"translate(0px, -360px)"
        "-webkit-transform":"translate3d(0px, -360px, 0)"
      })
#      $("#overlay").css("background-color":"transparent")
      @options.isHidden = true

    # **autoHide** (function)
    autoHide: ->
      # check if popup is open usefull when exit confirmation popup is shown over the player
      return if !@options.enabled

      @show()
      if not @options.isHidden
        @options.keepShowing = false
        # after timeout hide controls
        clearTimeout(@options.timeoutId) if @options.timeoutId
        @options.timeoutId = setTimeout( (=>@hide()), @options.timeoutTime)