define [
  "framework",
  "config",
  "jquery",
  "backbone",
  "moment",
  "i18n!../../../lang/nls/manifest",
  "application/helpers/submenuHelper",
  "application/helpers/backgroundHelper",
  "application/api/authentication",
  "application/api/cd/collections/CDWishlistCollection",
  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDSubscriberManagement",
  "application/api/cd/CDPaymentManagement",
  "../../api/cd/models/CDSeries",
  "../../screens/menu/menu",
  "../navhelp"
], (TWC, Config, $, Backbone, Moment, Manifest, SubMenuHelper, BackgroundHelper, Authentication, CDWishlistCollection, CDBaseRequest, CDSubscriberManagement, CDPaymentManagement, CDSeriesModel, TopMenu, NavHelp) ->

  class seasonListView extends TWC.MVC.View
    uid: 'seasonlist-view'

    el: "#screen"

    type: "page"

    defaultFocus: "#season-0"

    scrollOffset: 0

    external:
      html:
        url:"skin/templates/screens/series/seasonlist.tpl"

    events:
      "enter .season-thumbnail": "openSeason"
      "enter #subMenuItems .menuButton": "showSubMenuContent"

    zones: [
      {uid:"menu-zone", data:[]},
    ]

    onload: (@seriesId, @focusItem = null) ->
      @model = new CDSeriesModel({Id:@seriesId, manifest: Manifest, SubMenuTitle: SubMenuHelper.getSubMenuTitle(), SubMenuOptionalVisible: SubMenuHelper.getSubMenuOptionalVisible(), SubMenuItems: SubMenuHelper.getContextMenu()})


    onRenderComplete: ->
      BackgroundHelper.setBackgroundImage(@model.get("BackgroundImage"), @model.get("BackgroundImageBlurred"), false, 0)
      @seasonCount = @model.get("BundleChildren").models.length
      $("#mainMenu").show()

      @fullscreen = @toggleClass isnt "overlay"

      # Set submenu selected
      $subMenuEl = $("#subMenuItems .menuButton.submenu[data-id='#{SubMenuHelper.getContextMenuSelected()}']")
      $subMenuEl.addClass "select"

    showSubMenuContent: (el)->
      $el = $(el.target)

      if SubMenuHelper.getSubMenuContext() is SubMenuHelper.CONTEXT_STOREFRONT

        BackgroundHelper.enableBlur()
        $("#product").removeClass("overlay").removeClass("fullscreen")
        setTimeout ()=>
          # TWC.MVC.History.popState()
          TWC.MVC.Router.load "storefrontpage-view",
            data: [$el.data("id")]
            callback: ->
              TWC.MVC.History.reset()
        , 300


    moveToMenu: (focusOnLast=false) ->
      if focusOnLast
        TWC.MVC.Router.setFocus $("#subMenuItems .menuButton:last")
      else
        if $("#submenu_0").length
          TWC.MVC.Router.setFocus "#submenu_0"
        else
          @moveToMainMenu()

    openSeason: ->
      $el = $( TWC.MVC.Router.getFocus() )
      pos = parseInt($el.attr("id").split("-")[1])
      season = @model.get("BundleChildren").models[pos]
      TWC.MVC.Router.load "bundleDetail-view",
        data: [season.get("Id")]

    focusLeft: ->
      $el = $(TWC.MVC.Router.getFocus())
      pos = parseInt($el.attr("id").split("-")[1])
      if pos is 0
        @moveToMenu()
        return

      if pos is @scrollOffset
        TWC.SDK.Input.get().setBlockAll(true)
        @scrollOffset--
        newLeft = @scrollOffset * ($(".season-thumbnail").width() + parseInt($(".season-thumbnail").css("margin-right")))
        $(".series-list .scroll-container").css("-webkit-transform", "translate3d(-#{newLeft}px, 0px, 0px)")
        setTimeout ()=>
          TWC.MVC.Router.setFocus "#season-#{pos-1}"
          TWC.SDK.Input.get().setBlockAll(false)
        , 400
      else
        TWC.MVC.Router.setFocus "#season-#{pos-1}"

    focusRight: ->
      $el = $(TWC.MVC.Router.getFocus())
      pos = parseInt($el.attr("id").split("-")[1])
      return if pos is @seasonCount-1
      if pos - @scrollOffset is 5
        TWC.SDK.Input.get().setBlockAll(true)
        @scrollOffset++
        newLeft = @scrollOffset * ($(".season-thumbnail").width() + parseInt($(".season-thumbnail").css("margin-right")))
        $(".series-list .scroll-container").css("-webkit-transform", "translate3d(-#{newLeft}px, 0px, 0px)")
        setTimeout ()=>
          TWC.MVC.Router.setFocus "#season-#{pos+1}"
          TWC.SDK.Input.get().setBlockAll(false)
        , 400
      else
        TWC.MVC.Router.setFocus "#season-#{pos+1}"

    focusBack: ->
      TWC.MVC.Router.setFocus $("#backButton")

    backLeft: ->
      @moveToMainMenu()

    backRight: ->
      @moveToContent()

    backDown: ->
      @moveToMenu()

    moveToMenu: ->
      if $("#submenu_0").length > 0
        TWC.MVC.Router.setFocus "#" + $(".submenu.select").attr("id")
      else
        @moveToMainMenu()

    moveToMainMenu: ->
      TWC.MVC.Router.setActive "menu-zone"
      TWC.MVC.Router.setFocus "#browse"

    moveToContent: ->
      log "moveToContent"
      TWC.MVC.Router.setFocus "#season-0"

  seasonListView.get()
