define [
  "config",
  "settingPages",
  "framework",
  "application/models/baseModel",
  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDPaymentManagement",
  "application/api/cd/CDSubscriberManagement",
  "application/api/authentication",
  "jquery",
  "i18n!../../lang/nls/manifest",
  "i18n!../../lang/nls/license",
], (Config, ConfigSettings, TWC, BaseModel, CDBaseRequest, CDPaymentManagement, CDSubscriberManagement, Authentication, $, Manifest, License) ->

  homeModel = BaseModel.extend
    defaults:
      "manifest": Manifest,
      "settings": ConfigSettings,
      "license":License,
      "submenuItems": []

  class homeView extends TWC.MVC.View
    uid: 'home-view'

    el: '#screen'

    type: 'page'

    focusedInput = false

    firstLoad = true

    defaultFocus: "#azureToken"

    model: new homeModel()

    external:
      html:
        url:"skin/templates/screens/home.tpl"

    events:
      "enter #leftButton" : "retrieveUWPToken"
      "enter #rightButton" : "getStoreId"
      "enter #logout" : "logout"
      "enter #submitCSGOrder" : "submitCSGOrder"
      "enter #submitWindowsOrder" : "submitWindowsOrder"

    onload: ->
      log "onload homeView"
      if Windows?
        @context = Windows.Services.Store.StoreContext.getDefault()
        @properties = Windows.Services.Store.StorePurchaseProperties("Captain America")
        if @context?
          items = ["Consumable", "UnmanagedConsumable", "Durable"]
          @context.getAssociatedStoreProductsAsync(items).done (result) ->
            # if result.extendedError
            #   if result.extendedError == (0x803f6107 | 0)
            Object.getOwnPropertyNames(result.products).forEach (name) ->
              product = result.products[name]
              $("#products").append(product.title + " - " + product.storeId + " <br/>")

    onRenderComplete: ->
      $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])    #disable loader if it was still running e.g. during error
      TWC.MVC.Router.setActive @uid

    logout: ->
      Authentication.logout()
      TWC.MVC.Router.load "register-view",
        data: [null, null, null, null, false]

    retrieveUWPToken: ->
      CDPaymentManagement.retrieveUwpAccessTokens 2, (error, data)=>
        if data? and data.CollectionsToken? #and data.CollectionsToken?
          @token = data.CollectionsToken
          $("#azureToken").val(data.CollectionsToken)
          # $("#collectionsToken").val(data.CollectionsToken)

    getStoreId: ->
      $("#azureError").text("")
      collectionsToken = $("#azureToken").val()
      # collectionsToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6ImEzUU4wQlpTN3M0bk4tQmRyamJGMFlfTGRNTSIsImtpZCI6ImEzUU4wQlpTN3M0bk4tQmRyamJGMFlfTGRNTSJ9.eyJhdWQiOiJodHRwczovL2NzZ3N5c3RlbXMub25taWNyb3NvZnQuY29tL2JiZjkzY2NmLWM2MTAtNDhjMy1iYWUyLTRhYzBiN2EwZjQ2YyIsImlzcyI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0LzE5NGEyNjdiLTgyYmEtNGZhNi1iNmI5LWY5NTk2NmFiMzgxNS8iLCJpYXQiOjE0OTAxODk4NTUsIm5iZiI6MTQ5MDE4OTg1NSwiZXhwIjoxNDkwMTkzNzU1LCJhaW8iOiJBUUFCQUFFQUFBRFJOWVJRM2RoUlNybS00Sy1hZHBDSndHS1Q3bHNjeFRCMVY3dFppSWx0ZHlwbi1PZEU0eUlxRVBtd3NZRkoxbUxpa2VncVN5Y2FlWXFkMUFsYzJtbmlhbkNMWW5CVWFrRkMtU3I5ajAtdk9pQUEiLCJhcHBpZCI6IjQ3ZGQ3NTM3LTQ2ODktNGQ5Yy1hYTE5LWI5M2E3ZjE1YjUxYiIsImFwcGlkYWNyIjoiMSIsImlkcCI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0LzE5NGEyNjdiLTgyYmEtNGZhNi1iNmI5LWY5NTk2NmFiMzgxNS8iLCJvaWQiOiIwYjYxOGZjMy02ZWEwLTRjYzYtYmVmYS1mYTJlZDZjODFlODYiLCJzdWIiOiIwYjYxOGZjMy02ZWEwLTRjYzYtYmVmYS1mYTJlZDZjODFlODYiLCJ0aWQiOiIxOTRhMjY3Yi04MmJhLTRmYTYtYjZiOS1mOTU5NjZhYjM4MTUiLCJ2ZXIiOiIxLjAifQ.G9wUkvjqaUW9gVsAj47mwmDdm4C2hOHR3Wm7Sd9NiwELOv2sCGdi0fOWgpuuoAoBl3bIafqjLz0HpfRfUk3-eggSb3VpGDmGNJYN-ickbE03o4TRRp4iz_OKqq4_GVARQogQRJ8mawqgDeqXFz3i-aj7s_ybxuw5OsvUZZsO-E7bWGuowBcdgOtJXbPAYNfrHLRBYyMt6SQ_uWVjwM5j6S2jPdn26BHTzP7O6MrCrNql4E9EemO2bqYc747tSnx67NkEEU5vPvpnRAXp1VSN7baIvyFaQ3eAhz0LJJDPmBjrJZ67tkzQ62BjJXdycepRsOWhkHJJ1MAL3QDbQX4McA"
      # collectionsToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6ImEzUU4wQlpTN3M0bk4tQmRyamJGMFlfTGRNTSIsImtpZCI6ImEzUU4wQlpTN3M0bk4tQmRyamJGMFlfTGRNTSJ9.eyJhdWQiOiJodHRwczovL2NzZ3N5c3RlbXMub25taWNyb3NvZnQuY29tL2JiZjkzY2NmLWM2MTAtNDhjMy1iYWUyLTRhYzBiN2EwZjQ2Yy9iMmIva2V5cy9jcmVhdGUvY29sbGVjdGlvbnMiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC8xOTRhMjY3Yi04MmJhLTRmYTYtYjZiOS1mOTU5NjZhYjM4MTUvIiwiaWF0IjoxNDkwMTg5ODk0LCJuYmYiOjE0OTAxODk4OTQsImV4cCI6MTQ5MDE5Mzc5NCwiYWlvIjoiQVFBQkFBRUFBQURSTllSUTNkaFJTcm0tNEstYWRwQ0pTazNZTERlRDNVbTJubDJZN3phZTZGekdTMllxekdTajdyWnlRLW5hdVBpUXNHWkMwVmR0ZEx0aHg3R3p6WXI1ZnZ6Z3BnV3M1TGtRbUQ5bEVheExtaUFBIiwiYXBwaWQiOiI0N2RkNzUzNy00Njg5LTRkOWMtYWExOS1iOTNhN2YxNWI1MWIiLCJhcHBpZGFjciI6IjEiLCJpZHAiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC8xOTRhMjY3Yi04MmJhLTRmYTYtYjZiOS1mOTU5NjZhYjM4MTUvIiwib2lkIjoiMGI2MThmYzMtNmVhMC00Y2M2LWJlZmEtZmEyZWQ2YzgxZTg2Iiwic3ViIjoiMGI2MThmYzMtNmVhMC00Y2M2LWJlZmEtZmEyZWQ2YzgxZTg2IiwidGlkIjoiMTk0YTI2N2ItODJiYS00ZmE2LWI2YjktZjk1OTY2YWIzODE1IiwidmVyIjoiMS4wIn0.cys8QzUS3ia-vyw8KDacAVar_hu73zuC3faCagVT7rcvzp-ZY5mPTfoYfxXd3ZU__gZSzoEu06PzV6XF1ZJkLhMkgPFuOmcrf4Gnr03mnxp9kA1VUJGocceUGeIEXDuEA9OYzf1pbsgn3SvjOihRM8U8gfKBYkst_W2mpHprS3ExLQ7B9dh-psuRPL7rbG9sAzTypxL1TNQ0buk5OxLawVQcNn1YrSEVI0G0R5Fzff-BTPYKTdeZjxgHodki94C7du1nRCPRMg22U4ljPlVorPukqc5nr_f8LCd4Dl8cAgPN20vvJ-6JXP2OugOTcQu299Vv4tTW4zzWsHh99lDWEA"
      # publisherUserID = $("#publisherUserID").val()
      if collectionsToken == ""
        collectionsToken=null
      # if publisherUserID == ""
        # publisherUserID=null
      if @context?
        @context.getCustomerCollectionsIdAsync(collectionsToken, CDBaseRequest.sessionId).done (result) ->
          log result
          $("#storeid").text(result)
          @storeId = result
          if !result? || result==""
            $("#azureError").text("Something went wrong with getting the store Id")

    submitCSGOrder: ->
      $("#purchaseMessage").text("")
      $("#purchaseError").text("")
      uwp = {"UwpAccessToken":@token,"WindowsStoreIdKey":@storeId}
      paymentInstrument = {"Type":16,"UwpAccount":uwp}
      paymentInstruments = []
      paymentInstruments.push(paymentInstrument)
      productId = $("#sku").val()
      productIds = []
      if productId == ""
        productId = "329341"
      productIds.push(productId)
      pricingPlanId = $("#pricingplanid").val()
      if pricingPlanId == ""
        pricingPlanId = "4468"
      CDPaymentManagement.submitOrder(paymentInstruments, pricingPlanId, productIds, [], (error, response) =>
        log "SubmitOrder", error, response
        if error?
          $("#purchaseError").text(error.Message)
        else
          $("#purchaseMessage").text("Submit order completed succesfully")
      )


    submitWindowsOrder: ->
      $("#windowsMessage").text("")
      $("#windowsError").text("")
      productStoreId = $("#productStoreID").val()
      if productStoreId == ""
        productStoreId = "9n4x54z69n4s"
      productId = $("#sku").val()
      if productId == ""
        productId = "329341"
      pricingPlanId = $("#pricingplanid").val()
      if pricingPlanId == ""
        pricingPlanId = "4468"
      if productStoreId? and @context? and @properties?
        @properties.extendedJsonData = "{\"DevOfferId\": \" "+CDBaseRequest.subscriber.get("SubscriberId")+"," + productId + "," + pricingPlanId + "\" }"
        productName = $("#productname").val()
        if productName isnt ""
          @properties.name = productName
        @context.requestPurchaseAsync(productStoreId, @properties).done (result) ->
          extendedError = ""
          if result?
            if result.extendedError? and result.extendedError
              if result.extendedError == (0x803f6107 | 0)
                extendedError = "This sample has not been properly configured."
              else
                extendedError = "Error while making purchase: " + result.extendedError
            if result.status?
              switch result.status
                when Windows.Services.Store.StorePurchaseStatus.alreadyPurchased
                  $("#windowsMessage").text('You already bought this AddOn.')
                when Windows.Services.Store.StorePurchaseStatus.succeeded
                  $("#windowsMessage").text('Windows purchase successfully completed.')
                when Windows.Services.Store.StorePurchaseStatus.notPurchased
                  $("#windowsError").text("Product was not purchased, it may have been canceled. ExtendedError: " + extendedError)
                when Windows.Services.Store.StorePurchaseStatus.networkError
                  $("#windowsError").text("Product was not purchased due to a network error. ExtendedError: " + extendedError)
                when Windows.Services.Store.StorePurchaseStatus.serverError
                  $("#windowsError").text("Product was not purchased due to a server error. ExtendedError: " + extendedError)
                else
                  $("#windowsError").text("Product was not purchased due to an unknown error. ExtendedError: " + extendedError)
                  break
            else
              $("#windowsError").text("Product was not purchased due to an unknown error.")
          else
            $("#windowsError").text("Product was not purchased due to an unknown error.")


  homeView.get()
