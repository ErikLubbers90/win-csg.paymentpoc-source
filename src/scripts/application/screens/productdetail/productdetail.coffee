define [
  "framework",
  "config",
  "jquery",
  "backbone",
  "moment",
  "i18n!../../../lang/nls/manifest",
  "i18n!../../../lang/nls/regionsettings",
  "application/helpers/submenuHelper",
  "application/helpers/backgroundHelper",
  "application/api/authentication",
  "application/api/cd/collections/CDWishlistCollection",
  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDSubscriberManagement",
  "application/api/cd/CDPaymentManagement",
  "application/api/extras/cpe",
  "../../api/cd/models/CDProduct",
  "../../api/cd/models/CDBundle",
  "../../screens/menu/menu",
  "../../screens/videoplayer/extravideoplayer",
  "../../popups/login/loginConfirmation",
  "../../popups/generic/alert",
  "../../models/analytics/omnituretracker",
  "../navhelp"
], (TWC, Config, $, Backbone, Moment, Manifest, RegionSettings, SubMenuHelper, BackgroundHelper, Authentication, CDWishlistCollection, CDBaseRequest, CDSubscriberManagement, CDPaymentManagement, CPE, CDProductModel, CDBundleModel, TopMenu, ExtraVideoPlayer, LoginConfirmation, Alert, OmnitureTracker, NavHelp) ->

  class productDetailView extends TWC.MVC.View
    uid: 'productDetail-view'

    el: "#screen"

    type: "page"

    fullscreen: false

    isInFavorites: false

    videoThumbnailOffset: 0

    imageThumbnailOffset: 0

    epsisodeProductId: null

    forceAddToFavorites = false

    setBackgroundImageTimer: null

    entitledPricingPlanIsRental: false

    external:
      html:
        url:"skin/templates/screens/productdetail/productdetail.tpl"

    events:
      "enter #subMenuItems .menuButton": "showSubMenuContent"
      "enter #productFavoritesButton": "useFavoritesButton"
      "enter #productPlayButton": "playVideo"
      "enter #watch10MinPreviewButton": "play10MinPreview"
      "enter #watchPreviewButton": "playPreview"
      "enter #productPurchaseButton": "purchaseProduct"
      "enter #productUpgradeButton": "upgradeProduct"
      "enter #productRentButton": "rentProduct"
      "enter #productExtrasButton": "showExtras"
      "enter #productPreviewExtrasButton": "showExtras"
      "enter #productMoreInfoButton": "toggleFullscreen"
      "enter .extra-thumbnail" : "openExtraMedia"
      "enter #watchSeasonButton": "openSeasonOverview"
      "enter #prevEpisode": "openPreviousEpisode"
      "enter #nextEpisode": "openNextEpisode"
      "enter #navigationLeftVideos": "videosPrev"      
      "enter #navigationRightVideos": "videosNext"
      "swipeLeft .relatedVideos": "videosPrev"
      "swipeRight .relatedVideos": "videosNext"
      "enter #navigationLeftImages": "imagesPrev"
      "enter #navigationRightImages": "imagesNext"
      "swipeLeft .relatedImages": "imagesPrev"
      "swipeRight .relatedImages": "imagesNext"
      "enter #navigationTop": "scrollUp"
      "enter #navigationBottom": "scrollDown"



    zones: [
      {uid:"menu-zone", data:[]},
    ]

    onload: (prodId, @toggleClass = "overlay", @focusItem = null) ->
    
      #during episode chaining the product id will be updated to correct the history flow
      if @epsisodeProductId is null 
        @prodId = prodId
      else
        @prodId = @epsisodeProductId
        @epsisodeProductId = null
    
      authenticated = Authentication.isLoggedIn()
      @model = new CDProductModel({Id:@prodId, manifest: Manifest, SubMenuTitle: SubMenuHelper.getSubMenuTitle(), SubMenuOptionalVisible: SubMenuHelper.getSubMenuOptionalVisible(), SubMenuItems: SubMenuHelper.getContextMenu(), authenticated: authenticated})

    updateProductId: (id = null)->    
      @epsisodeProductId = id   
	
    onunload: ->
      clearTimeout(@setBackgroundImageTimer) if @setBackgroundImageTimer?
      $(".productWindow").off()
      @entitledPricingPlanIsRental = false

    onRenderComplete: ->

      $(TWC.MVC.View.eventbus).trigger("loading", [true, "keep"])
      @textTruncated = @model.get("Description").length > 470 or @model.get("Copyright").length > 220

      $("#mainMenu").show()
      setTimeout ()=>
        BackgroundHelper.disableBlur()
      , 1000
      @fullscreen = @toggleClass isnt "overlay"

      # Set submenu selected
      $subMenuEl = $("#subMenuItems .menuButton.submenu[data-id='#{SubMenuHelper.getContextMenuSelected()}']")
      $subMenuEl.addClass "select"
      @loadRelatedEpisodes()

      @formattedTitle = @model.get("Name").toLowerCase().replace(/[^A-Za-z0-9]/g, "")
      @ot = OmnitureTracker.get()
      @ot.trackPageView("#{@formattedTitle}.detail.html")

      

      #set related video icon display
      if @model.get("RelatedMedia")?
        entitledPricingPlan = @model.get("PricingPlans").findWhere({ Id : @model.get("EntitledPricingPlanId")})
        for mediaItem, key in @model.get("RelatedMedia")
          requiresPurchase = mediaItem.RequiresPurchase
          if requiresPurchase and (not entitledPricingPlan or @model.get("Expired"))
            $("#relatedVideo-#{key}").addClass("locked")

      scrollHeight = $(".productWindow").get(0).scrollHeight - $(".productWindow").outerHeight(true);

      $(".navigationContainer").show() if scrollHeight > 0
      $(".productWindow").on "scroll", (e)=>
        @onScroll(e, scrollHeight)

    onScroll:(e, scrollHeight)->
      if $(".productWindow").scrollTop() is 0
        $("#navigationTop").hide()
      else
        $("#navigationTop").show()

      if Math.abs( $(".productWindow").scrollTop() - scrollHeight) < 3
        $("#navigationBottom").hide()
      else
        $("#navigationBottom").show()
      
      


    loadRelatedEpisodes: ->
      return if @model.get("SeasonId") is ""
      currentEpisode = @model.get("EpisodeNumber")

      #Load season bundle
      seasonModel = new CDBundleModel({Id: @model.get("SeasonId")})
      seasonModel.refresh (data)=>
        @renderRelatedEpisodes(seasonModel.get("BundleChildren"), currentEpisode, seasonModel.get("ThumbnailLandscape"))

    renderRelatedEpisodes: (episodes, currentPos, landscapeThumbnail)->
      @seasonEpisodes = episodes.models
      @prevEpisode = _.find episodes.models, (episode)->
        episode.get("EpisodeNumber") is currentPos - 1

      @nextEpisode = _.find episodes.models, (episode)->
        episode.get("EpisodeNumber") is currentPos + 1

      if @prevEpisode?
        $("#prevEpisode").removeClass("disable").show()
        $("#prevEpisode .episode-number").text(@prevEpisode.get("EpisodeNumber"))
        $("#prevEpisode .episode-name").text(@prevEpisode.get("Name"))
        $("#prevEpisode .episode-runtime").text(@prevEpisode.get("RunTime"))
        $("#prevEpisode img").attr("src", @prevEpisode.get("ThumbnailLandscape"))

      if @nextEpisode?
        $("#nextEpisode").removeClass("disable").show()
        $("#nextEpisode .episode-number").text(@nextEpisode.get("EpisodeNumber"))
        $("#nextEpisode .episode-name").text(@nextEpisode.get("Name"))
        $("#nextEpisode .episode-runtime").text(@nextEpisode.get("RunTime"))
        $("#nextEpisode img").attr("src", @nextEpisode.get("ThumbnailLandscape"))



    updateButtons: (callback)->

      checkForPurchaseOptions = false
      # Get pricing plans
      @buyHDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
        pricingPlan.equalsType("BUYHD")
      @buySDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
        pricingPlan.equalsType("BUYSD")
      @buyUHDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
        pricingPlan.equalsType("BUYHDR")
      if not @buyUHDPricingPlan?
        @buyUHDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
          pricingPlan.equalsType("BUYUHD")
      @rentHDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
        pricingPlan.equalsType("RENTHD")
      @rentSDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
        pricingPlan.equalsType("RENTSD")
      @rentUHDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
        pricingPlan.equalsType("RENTHDR")
      if not @rentUHDPricingPlan?
        @rentUHDPricingPlan = @model.get("OrderPricingPlans").find (pricingPlan)->
          pricingPlan.equalsType("RENTUHD")

      # Get entitled pricing plan
      entitledPricingPlan = @model.get("PricingPlans").findWhere({ Id : @model.get("EntitledPricingPlanId")})
      if entitledPricingPlan? and entitledPricingPlan.isPreviewPricingPlan()
        entitledPricingPlan = null

      entitledQuality = if entitledPricingPlan? then entitledPricingPlan.getQuality() else null
      productIsAvailable = if entitledPricingPlan? then entitledPricingPlan.isAvailable() else false
      @entitledPricingPlanIsRental = if entitledPricingPlan?.isRental?() then entitledPricingPlan?.isRental?() and !@model.get("Expired") else false

      availableButtonText = ""
      #if product getting available in the future
      if entitledPricingPlan? and !productIsAvailable
        dateNow = Moment().valueOf()
        if entitledPricingPlan.get("AvailabilityStart")? and Moment(entitledPricingPlan.get("AvailabilityStart")).valueOf() > dateNow
          availableButtonText = Manifest.available_on.replace("{X}", Moment(entitledPricingPlan.get("AvailabilityStart")).format("MM/DD"))

      # Preview button
      platformUtils = TWC.SDK.Utils.get()
      if @model.get("Previews").length
        for preview in @model.get("Previews").models
          if !preview.get("Description")
            @enableButton($("#watchPreviewButton"))
          else if !(entitledPricingPlan?) or @model.get("Expired")
            if preview.get("Description")=="HDR" and platformUtils.isHDR()
              @enableButton($("#watch10MinPreviewButton"))
            else if preview.get("Description")=="SDR"
              @enableButton($("#watch10MinPreviewButton"))

      if @model.get("SeasonId") != ""
        @enableButton($("#watchSeasonButton"))
        $(".episodeProgressDetail").show()

      if @model.get("SeasonId") is "" then isEpisode = false else true

      #check if product has locker item id, needed to activate upgrade option
      if @model.get("LockerItemId") is 0 then isUpgradable = false else isUpgradable = true

      # in case it is a rental entitlement show the expiration time and rental button
      if entitledPricingPlan?.isRental()
        purchasePolicies =  @model.get("PurchasePolicies")
        

        for policy in purchasePolicies.models
          if policy.get("ActionCode") is 20
            isLicenseAvailable = if policy.get("Available")? and policy.get("Available") then true else false
            hasFirstAccessDate = if policy.get("FirstAccessDate")? and policy.get("FirstAccessDate") then true else false
            expirationDate = policy.get("ExpirationDate")
#                expirationDate = "2016-09-20T15:00:00.000Z"
            break

        # Expired                  : no license available and has first accessdate
        # Rented movie not started : license available and no first accessdate
        # Rented movie started     : license available and has first accessdate
        rentedMovieIsExpired = @model.get("Expired")

        remainingTimeInMin = Moment(expirationDate).diff(Moment(), "minutes")

        if not rentedMovieIsExpired
          @enableButton($("#productPlayButton"))

          # rented movie available
          licenseExpirationTimeString = entitledPricingPlan.getLicenseExpirationTime(remainingTimeInMin)

          #check for purchase option next to the play rental 
          checkForPurchaseOptions = true

          # Show license remaining time
          if licenseExpirationTimeString.length > 0 then $("#product .remainingWatchTimeLeft").html(licenseExpirationTimeString)
        else
          @showRentalButtons(entitledPricingPlan, productIsAvailable, availableButtonText)
      else
        @showRentalButtons(entitledPricingPlan, productIsAvailable, availableButtonText)

      if @buyUHDPricingPlan #UHD available
        
        if entitledPricingPlan? and not @model.get("Expired") #user owns content and entitlement is not expired
          @updateExtraButtons(["hidden", "visible"], productIsAvailable)
          @enableButton($("#productPlayButton")) if productIsAvailable
          if availableButtonText != ""
            $("#availableButton").text(availableButtonText)
            @enableButton($("#availableButton"))
          if entitledQuality is "SD" or entitledQuality is "HD"
            $("#productUpgradeButton").html("<span class='highlight'> #{Manifest.upgrade_button} #{@buyUHDPricingPlan.getPrice()}</span>" )
            @enableButton($("#productUpgradeButton")) if not isEpisode and isUpgradable
        else
          $("#productPurchaseButton").html("<span class='highlight'>#{@buyUHDPricingPlan.getPrice()}</span> #{Manifest.buy_button}" )
          @enableButton($("#productPurchaseButton"))
          @updateExtraButtons(["visible", "hidden"], productIsAvailable)
          if availableButtonText != ""
            $("#availableButton").text(availableButtonText)
            @enableButton($("#availableButton"))

        if checkForPurchaseOptions   #if user owns a rental pp also show the purchase option 
          $("#productPurchaseButton").html("<span class='highlight'>#{@buyUHDPricingPlan.getPrice()}</span> #{Manifest.buy_button}" )
          @enableButton($("#productPurchaseButton"))



      else if @buyHDPricingPlan #only HD is available
        if entitledPricingPlan? and not @model.get("Expired") #user owns content and entitlement is not expired
          @enableButton($("#productPlayButton")) if productIsAvailable
          @updateExtraButtons(["hidden", "visible"], productIsAvailable)
          if availableButtonText != ""
            $("#availableButton").text(availableButtonText)
            @enableButton($("#availableButton"))        
        else
#            console.log("buyHDPricingPlan", @buyHDPricingPlan)
          $("#productPurchaseButton").html("<span class='highlight'>#{@buyHDPricingPlan.getOriginalPrice()}</span> #{Manifest.buy_button}" )
          @updateExtraButtons(["visible", "hidden"], productIsAvailable)
#            console.log "LI - UHD pricing plan available, not owned"
#          console.log "LI - UHD pricing plan not available"

        if checkForPurchaseOptions    #if user owns a rental pp also show the purchase option 
          $("#productPurchaseButton").html("<span class='highlight'>#{@buyHDPricingPlan.getOriginalPrice()}</span> #{Manifest.buy_button}" )
          @updateExtraButtons(["visible", "hidden"], productIsAvailable)

      else
        @enableButton($("#productPlayButton")) if productIsAvailable
        @updateExtraButtons(["visible", "hidden"], productIsAvailable)
        if @buyUHDPricingPlan #UHD available
          $("#productPurchaseButton").html("<span class='highlight'>#{@buyUHDPricingPlan.getOriginalPrice()}</span> #{Manifest.buy_button}" )
          @enableButton($("#productPurchaseButton"))

      callback?()



#        console.log "LO - logged out"

#      #if user returns to detail page directly from login screen open buy/rent popup,
#      #if entitledPricingPlan exists no need to show popup
#      if @openPaymentPopup and Config.enablePricesWhenUnauthenticated and not entitledPricingPlan
#        if @purchaseType is "buy" then @showBuyOrderPopup() else @showRentOrderPopup()


    updateExtraButtons: (settings, productIsAvailable)->
      return if not @model.hasExtras()
      previewExtrasStatus = settings[0]
      viewExtrasStatus = settings[1]

      # Do not display extra buttons if product is rental
      if not @entitledPricingPlanIsRental
        switch previewExtrasStatus
          when "locked" then $("#productPreviewExtrasButton").removeClass("disable").addClass("locked")
          when "visible" then $("#productPreviewExtrasButton").removeClass("disable")

        #if product is not available only show preview extras button
        if !productIsAvailable
          if viewExtrasStatus
            return $("#productPreviewExtrasButton").removeClass("disable")
        else
          switch viewExtrasStatus
            when "visible" then $("#productExtrasButton").removeClass("disable")

    setInitFocus: ->
      if @focusItem isnt null
        TWC.MVC.Router.setFocus @focusItem
      else
        if not $("#productPlayButton").hasClass("disable")
          TWC.MVC.Router.setFocus $("#productPlayButton")
        else if $(".buttonContainer .button:not(.disable):first").length
          TWC.MVC.Router.setFocus $(".buttonContainer .button:not(.disable):first")
        else
          @moveToMenu()

    onZoneRenderComplete: ->
      TWC.MVC.Router.setActive @uid

      if @toggleClass isnt "overlay"
        $("#product").removeClass("overlay").addClass(@toggleClass)
        $("#productDescriptionContainer .productDescription").html(@model.get("Description").stub(730))
        $("#productDescriptionContainer .copyright").text(@model.get("Copyright").stub(380))
      else
        $("#product").addClass("overlay")

      #Favorites button
      @updateFavoritesButton()

      @updateButtons(=>
        # Do not show background image for legacy titles, these products do not have a UHD pricingplan
        if @buyUHDPricingPlan?
          BackgroundHelper.setBackgroundImage(@model.get("BackgroundImage"), @model.get("BackgroundImageBlurred"), false, 0)

          # If background has not been set after three seconds, try again
          clearTimeout(@setBackgroundImageTimer) if @setBackgroundImageTimer?
          @setBackgroundImageTimer = setTimeout =>
            if $("#backgroundContainer img").attr("src") isnt @model.get("BackgroundImage")
              BackgroundHelper.setBackgroundImage(@model.get("BackgroundImage"), @model.get("BackgroundImageBlurred"), false, 0)
          , 3000

        @setInitFocus()
        $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])
      )

    onReturn: ->
      BackgroundHelper.enableBlur()

      $("#product").removeClass("overlay").removeClass("fullscreen")
      setTimeout ()=>
        TWC.MVC.History.popState()
      , 300


    showProductDetail: (el) ->
      $el = $(el.target)
      productId = $el.data("id")
      productType = $el.data("type")
      playlistRow = $(".playlist.active").index()
      playlistPage = $(".playlist.active").data("page")

      switch productType
        when 1
        # Product/episode
          page = 'productDetail-view'
        when 2, 4
        # Bundle/season
          page = 'bundleDetail-view'
        else
          page = 'productDetail-view'

      TWC.MVC.Router.load page,
        data: [productId]

    updateFavoritesButton: ()->
      return if not Config.useFavorites

      if Authentication.isLoggedIn()

        $("#productFavoritesButton").fadeTo(0, 0.4)
        @blockFavoritesButton = true

        $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])

        #check if product is present in favorites and change button status accordingly
        $("#productFavoritesButton").removeClass("disable")    # Enable the button after action is available
        $("#productFavoritesButton").removeClass("removeFromFavorites").removeClass("addToFavorites")

        CDWishlistCollection.checkIfInWishList  @model.get("Id"), (@isInFavorites) =>

          status = if @isInFavorites then "removeFromFavorites" else status = "addToFavorites"
          $("#productFavoritesButton").addClass(status)
          if @forceAddToFavorites and not @isInFavorites
            @useFavoritesButton()
          @forceAddToFavorites = false
          @blockFavoritesButton = false
          $("#productFavoritesButton").fadeTo(0, 1)


      else
        $("#productFavoritesButton").removeClass("disable")    # Enable the button after action is available
        $("#productFavoritesButton").removeClass("removeFromFavorites").removeClass("addToFavorites")
        status = "addToFavorites"
        $("#productFavoritesButton").addClass(status)
        @blockFavoritesButton = false

    # Display favorites add/remove success popup
    showConfirmationPopup: (title, message) ->
      confirmAlert = new Alert()
      confirmAlert.load
        data: [title, message, Manifest.popup_exit_close]

    addToFavorites: ->
      @forceAddToFavorites = true

    # Add to or delete from favorites
    useFavoritesButton: ->
      return if not Config.useFavorites
      return if @blockFavoritesButton
      @ot.trackOutboundClick("ultraapp.com/detail", "#{@formattedTitle}_favorites_button")
      if not Authentication.isLoggedIn()
        historySize = TWC.MVC.History.size()
        loginConfirmation = new LoginConfirmation()
        loginConfirmation.load
          data: [false, ()=>
            TWC.MVC.Router.load "login-view",
              data: [
                ()=>
                  TWC.MVC.History.reset(historySize)
                  TWC.MVC.History.popState ()=>
                    TWC.MVC.Router.find("productDetail-view").addToFavorites()
              ]
          ]
      else
        $(TWC.MVC.View.eventbus).trigger("loading", true)
        if @isInFavorites
          CDWishlistCollection.removeFromWishList @model.get("Id"), (error, response) =>
            # if request successful response.Fault will be null
            isRemoved = not response.Fault?
            if isRemoved
              @isInFavorites = false
              $(TWC.MVC.View.eventbus).trigger("loading", false)
              $("#productFavoritesButton").removeClass("removeFromFavorites")
              $("#productFavoritesButton").addClass("addToFavorites")
  #            @showConfirmationPopup(Manifest.removed_from_favorites, "")
        else
          CDWishlistCollection.addToWishList @model.get("Id"), (error, response) =>
            # if request successful response.Fault will be null
            isAdded = not response.Fault?
            if isAdded
              @isInFavorites = true
              $(TWC.MVC.View.eventbus).trigger("loading", false)
              $("#productFavoritesButton").removeClass("addToFavorites")
              $("#productFavoritesButton").addClass("removeFromFavorites")
  #            @showConfirmationPopup(Manifest.added_to_favorites, "")

    focusUp: ->
      $el = $(TWC.MVC.Router.getFocus())
      if($el.hasClass("button"))
        if @fullscreen
          @toggleFullscreen(true)
      else if $el.hasClass("extra-thumbnail")
        if $el.hasClass("video")
          TWC.MVC.Router.setFocus $(".buttonContainer .button:not(.disable):first")
        else if $el.hasClass("image")
          if $(".extra-thumbnail.video").length > 0
            $("#product").removeClass("extraImages").addClass("fullscreen")
            TWC.MVC.Router.setFocus $("#relatedVideo-#{@videoThumbnailOffset}")
          else
            TWC.MVC.Router.setFocus $(".buttonContainer .button:not(.disable):first")
      else if $el.hasClass("episode-thumbnail")
        TWC.MVC.Router.setFocus $(".buttonContainer .button:not(.disable):first")


    focusDown: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("button")
        if not @fullscreen
          @toggleFullscreen()
        return TWC.MVC.Router.setFocus "#relatedVideo-0" if $("#relatedVideo-0").length > 0
        return TWC.MVC.Router.setFocus "#relatedImage-0" if $("#relatedImage-0").length > 0
        return TWC.MVC.Router.setFocus "#prevEpisode" if $("#prevEpisode").length > 0 and not $("#prevEpisode").hasClass("disable")
        return TWC.MVC.Router.setFocus "#nextEpisode" if $("#nextEpisode").length > 0 and not $("#nextEpisode").hasClass("disable")
      else if $el.hasClass("extra-thumbnail video")
        if $(".extra-thumbnail.image").length > 0
          $("#product").removeClass("fullscreen").addClass("extraImages")
          TWC.MVC.Router.setFocus $("#relatedImage-#{@imageThumbnailOffset}")

    focusLeft: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("button")
        if $el.prevAll(":not(.disable)").length > 0
          TWC.MVC.Router.setFocus $el.prevAll(":not(.disable):first")
        else
          @moveToMenu()
      else if $el.hasClass("extra-thumbnail")
        @focusThumbnailLeft($el)
      else if $el.attr("id") is "nextEpisode"
        return TWC.MVC.Router.setFocus "#prevEpisode" if not $("#prevEpisode").hasClass("disable")
        @moveToMenu()

    focusRight: ->
      $el = $(TWC.MVC.Router.getFocus())
      if $el.hasClass("button")
        TWC.MVC.Router.setFocus $el.nextAll(":not(.disable):first")
      else if $el.hasClass("extra-thumbnail")
        @focusThumbnailRight($el)
      else if $el.attr("id") is "prevEpisode"
        return TWC.MVC.Router.setFocus "#nextEpisode" if not $("#nextEpisode").hasClass("disable")

    focusBack: ->
      TWC.MVC.Router.setFocus $("#backButton")

    backLeft: ->
      @moveToMainMenu()

    backRight: ->
      @moveToContent()

    backDown: ->
      @moveToMenu()

    # Handle mouse clicks on extra thumbnails

    videosPrev: ()->
      pos = @videoThumbnailOffset
      type = "video"
      @animateThumbnailLeft(type, pos)
      TWC.MVC.Router.setFocus "#navigateLeftVideos"

    videosNext: ()->
      pos = @videoThumbnailOffset+3
      type = "video"
      @animateThumbnailRight(type, pos)
      TWC.MVC.Router.setFocus "#navigateRightVideos"


    imagesPrev: ()->
      pos = @imageThumbnailOffset
      type = "image"
      @animateThumbnailLeft(type, pos)
      TWC.MVC.Router.setFocus "#navigateLeftImages"

    imagesNext: ()->
      pos = @imageThumbnailOffset+3
      type = "image"
      @animateThumbnailRight(type, pos)
      TWC.MVC.Router.setFocus "#navigateRightImages"
        


    focusThumbnailLeft: ($el)->
      pos = parseInt($el.attr("id").split("-")[1])
      return if pos is 0
      if $el.hasClass("video")
        $container = $(".relatedVideos .scroll-container")
        currentOffset = @videoThumbnailOffset
      else
        $container = $(".relatedImages .scroll-container")
        currentOffset = @imageThumbnailOffset
      if pos is currentOffset
        scrollWidth = $(".extra-thumbnail").outerWidth(true)
        currentOffset--
        if $el.hasClass("video")
          @videoThumbnailOffset--
        else
          @imageThumbnailOffset--
        $container.css({
          "-o-transform": "translate(" + (currentOffset*-scrollWidth) + "px, 0)"
          "-webkit-transform": "translate3d(" + (currentOffset*-scrollWidth) + "px, 0, 0)"
        });

      TWC.MVC.Router.setFocus "#" + $el.prev().attr("id")


    focusThumbnailRight: ($el)->
      pos = parseInt($el.attr("id").split("-")[1])
      if $el.hasClass("video")
        videoCnt = $(".relatedVideos .scroll-container").children().length
        return if pos is videoCnt-1
        $container = $(".relatedVideos .scroll-container")
        currentOffset = @videoThumbnailOffset
      else
        imageCnt = $(".relatedImages .scroll-container").children().length
        return if pos is imageCnt-1
        $container = $(".relatedImages .scroll-container")
        currentOffset = @imageThumbnailOffset
      if pos - currentOffset is 3
        scrollWidth = $(".extra-thumbnail").outerWidth(true)
        currentOffset++
        if $el.hasClass("video")
          @videoThumbnailOffset++
        else
          @imageThumbnailOffset++
        $container.css({
          "-o-transform": "translate(" + (currentOffset*-scrollWidth) + "px, 0)"
          "-webkit-transform": "translate3d(" + (currentOffset*-scrollWidth) + "px, 0, 0)"
        })

      TWC.MVC.Router.setFocus "#"+$el.next().attr("id")


    enableButton: ($el) ->
      $el.removeClass("disable")

    showExtrasButton: ()->
      if @model.hasExtras()
        @enableButton($("#productExtrasButton"))

    showRentalButtons: (entitledPricingPlan, productIsAvailable, availableButtonText)->
      # Rented movie expired allow a user to rent it again
      if @rentUHDPricingPlan
        if entitledPricingPlan? and not @model.get("Expired") #user owns content and entitlement is not expired
          # this function is not called if its a rental movie which isnt expired
          # That means this if condition is only true if there is a rental pricing plan but the user already bought the content
          # if so, the rentalButtons should not be responsible for displaying the play/extra buttons
          # @enableButton($("#productPlayButton")) if productIsAvailable
          # @updateExtraButtons(["hidden", "visible"], productIsAvailable)
          # if availableButtonText != ""
          #   $("#availableButton").text(availableButtonText)
          #   @enableButton($("#availableButton"))
          #              console.log "LI - UHD pricing plan available, owned UHD", entitledPricingPlan, entitledQuality
        else
          $("#productRentButton").html("<span class='highlight'>#{@rentUHDPricingPlan.getOriginalPrice()}</span> #{Manifest.rent_button}" )
          @enableButton($("#productRentButton"))
          @updateExtraButtons(["visible", "hidden"], productIsAvailable)
          if availableButtonText != ""
            $("#availableButton").text(availableButtonText)
            @enableButton($("#availableButton"))
        #            console.log "LI - UHD pricing plan available, not owned"

      else if @rentHDPricingPlan
        if entitledPricingPlan? and not @model.get("Expired") #user owns content and entitlement is not expired
            # this function is not called if its a rental movie which isnt expired
            # That means this if condition is only true if there is a rental pricing plan but the user already bought the content
            # if so, the rentalButtons should not be responsible for displaying the play/extra buttons
          # @enableButton($("#productPlayButton")) if productIsAvailable
          # @updateExtraButtons(["visible", "visible"], productIsAvailable)
          # if availableButtonText != ""
          #   $("#availableButton").text(availableButtonText)
          #   @enableButton($("#availableButton"))
          #              console.log "LI - UHD pricing plan available, owned HD", entitledPricingPlan, entitledQuality
        else
          #            console.log("buyHDPricingPlan", @buyHDPricingPlan)
          $("#productRentButton").html("<span class='highlight'>#{@rentHDPricingPlan.getOriginalPrice()}</span> #{Manifest.rent_button}" )
          @updateExtraButtons(["visible", "hidden"], productIsAvailable)
      #            console.log "LI - UHD pricing plan available, not owned"
      #          console.log "LI - UHD pricing plan not available"

    showSubMenuContent: (el)->
      $el = $(el.target)

      if SubMenuHelper.getSubMenuContext() is SubMenuHelper.CONTEXT_STOREFRONT

        BackgroundHelper.enableBlur()
        $("#product").removeClass("overlay").removeClass("fullscreen")
        setTimeout ()=>
          # TWC.MVC.History.popState()
          TWC.MVC.Router.load "storefrontpage-view",
            data: [$el.data("id")]
            callback: ->
              TWC.MVC.History.reset()
        , 300

      else if SubMenuHelper.getSubMenuContext() is SubMenuHelper.CONTEXT_LOCKER
        TWC.MVC.Router.load "locker-view",
          data: [$el.data("id"), null, null, null, true]

      else if SubMenuHelper.getSubMenuContext() is SubMenuHelper.CONTEXT_SEARCH

        TWC.MVC.Router.load "video-search-view",
          data: ["", [], $el.data("id")]

      else if SubMenuHelper.getSubMenuContext() is SubMenuHelper.CONTEXT_FAVORITES

        TWC.MVC.Router.load "favorites-view",
          data: [$el.data("id")]

    toggleFullscreen: (withDelay = false) ->
      if @fullscreen
        @fullscreen = !@fullscreen
        $("#productMoreInfoButton .highlight").text(Manifest.more_button)
        $("#product").removeClass("fullscreen").addClass("overlay")
        if withDelay
          setTimeout () =>
            $("#productDescriptionContainer .productDescription").html(@model.get("Description").stub(470))
            $("#productDescriptionContainer .copyright").text(@model.get("Copyright").stub(220))
          , 350
      else if @model.get("SeriesId")!= "" or @model.get("RelatedMedia")?.length > 0 or @textTruncated
        @fullscreen = !@fullscreen
        $("#productMoreInfoButton .highlight").text(Manifest.less_button)
        $("#product").removeClass("overlay").addClass("fullscreen")
        if @model.get("RelatedMedia")?.length > 0
          $("#productDescriptionContainer .productDescription").html(@model.get("Description").stub(730))
          $("#productDescriptionContainer .copyright").text(@model.get("Copyright").stub(380))
        else
          $("#productDescriptionContainer .productDescription").html(@model.get("Description").stub(1730))
          $("#productDescriptionContainer .copyright").text(@model.get("Copyright").stub(880))

    openSeasonOverview: ->
      TWC.MVC.Router.load "bundleDetail-view",
        data: [@model.get("SeasonId")]

    openPreviousEpisode: ->
      TWC.MVC.Router.load "productDetail-view",
        data: [@prevEpisode.get("Id")]

    openNextEpisode: ->
      TWC.MVC.Router.load "productDetail-view",
        data: [@nextEpisode.get("Id")]

    openExtraMedia: ->
      $el = $(TWC.MVC.Router.getFocus())
      pos = parseInt($el.attr("id").split("-")[1])

      for mediaItem, key in @model.get("RelatedMedia")        
        requiresPurchase = mediaItem.RequiresPurchase if key is pos
      entitledPricingPlan = @model.get("PricingPlans").findWhere({ Id : @model.get("EntitledPricingPlanId")})              
      
      @updateHistory(@model.get("Id"), $("#product").attr("class"), TWC.MVC.Router.getFocus())
      if($el.hasClass("video"))
        return if requiresPurchase and (not entitledPricingPlan? or @model.get("Expired"))
        TWC.MVC.Router.load "videoplayer-view",
          data: [@model, false, false, [], [], pos, "", false, 0, false, false, true]
      else
        TWC.MVC.Router.load "imageviewer-view",
          data: [@model.get("RelatedImages"), pos]


    playVideo: ->
      if @nextEpisode?
        TWC.MVC.Router.load 'videoplayer-view',
          data: [@model, false, false, [], @seasonEpisodes, 0, "#{@formattedTitle}_movie"]
      else
        TWC.MVC.Router.load 'videoplayer-view',
          data: [@model, false, false, [], [], 0, "#{@formattedTitle}_movie"]

    playPreview: ->
      #{@formattedTitle}
      #add param true to tell the videoploayer that its a trailer
      TWC.MVC.Router.load 'videoplayer-view',
        data: [@model, true, false, [], [], 0, "#{@formattedTitle}_trailer"]


    play10MinPreview: ->
      loadPlayer = (cb)=>
        #cancel this if purchase has been made
        @model.refresh =>
          entitledPricingPlan = @model.get("PricingPlans").findWhere({ Id : @model.get("EntitledPricingPlanId")})

          if entitledPricingPlan? and not @model.get("Expired")
            messageAlert = new Alert();
            messageAlert.load
              data: ["", Manifest.payment_already_purchased, Manifest.ok, ()=>
                TWC.MVC.History.reset(historySize)
              ]
          else
            TWC.MVC.Router.load 'videoplayer-view',
              data: [@model, false, false, [], [], 0, "#{@formattedTitle}_10mintrailer", false, null, false, true]
              callback: ()=>
                cb?()

      # TODO: update rating check to avoid using the name
#      isLoginRequired = previewPricingPlan.get("Name").indexOf("RatedR") > -1
      # isLoginRequired = true # for now always request logged in user, otherwise the ViewContent request fails
      isLoginRequired = @model.get("RatedR")

      if not Authentication.isLoggedIn() and isLoginRequired
        historySize = TWC.MVC.History.size()
        loginConfirmation = new LoginConfirmation()
        popupTextOverride =
          title: Manifest.login_preview
          confirm: Manifest.login_login
          cancel: Manifest.back
        loginConfirmation.load
          data: [false
          , ()=>
              TWC.MVC.Router.load "login-view",
                callback: ()=>
                  @ot.trackPageView('purchaselogin.html')
                data: [
                  ()=>
                    loadPlayer ()=>
                      TWC.MVC.History.reset(historySize)

                , ()=>
                    console.log("login failed, do nothing")
                ]
          , false
          , ()=>
              console.log("login failed")
          , popupTextOverride
          ]
      else
        loadPlayer()

    playExtraVideo: (playerId, videoObject, CPE, returnCallback, callback)->
      TWC.MVC.Router.load 'extravideoplayer-view',
        data: [playerId, videoObject, @model, CPE, 0, returnCallback]
        callback: ()=>
          callback?()

    showExtras: ->
      if TWC.MVC.Router.getFocus() is "#productPreviewExtrasButton"
        @ot.trackOutboundClick("ultraapp.com/extras", "#{@formattedTitle}_previewextras_button")
      else
        @ot.trackOutboundClick("ultraapp.com/extras", "#{@formattedTitle}_extras_button")

      extras = new CPE(@model.getExtrasId(), @prodId, "en", @model.getAlternativeStream())
      extras.initialize ->
        log "extras success"


    returnToLoginScreen: (type) ->
      loginScreenDetails = {}
      loginScreenDetails.productId = @prodId
      loginScreenDetails.returnValue = true
      loginScreenDetails.purchaseType = type
      loginScreenDetails.productType  = "product"

    upgradeProduct: (e) ->
      @purchaseProduct(e, true)

    purchaseProduct: (e, upgrade = false, purchaseCallback=null, purchaseFailCallback = null, pricingPlan = null) =>
      if Authentication.isLoggedIn()
        $(TWC.MVC.View.eventbus).trigger("loading", [true])

        selectedPricingPlan = if pricingPlan? then pricingPlan else @buyUHDPricingPlan
        if selectedPricingPlan.isRental()
          @ot.trackOutboundClickToBuy("ultraapp.com/confirmpurchase", "vod_#{@formattedTitle}_button")
        else
          @ot.trackOutboundClickToBuy("ultraapp.com/confirmpurchase", "4kultra_#{@formattedTitle}_button")

        purchaseProduct = ()=>
          currency = RegionSettings.default_payment_instruments_currency
          CDPaymentManagement.retrieveWallet currency, (error, data)=>
            CDPaymentManagement.updatePaypalData data.PaymentInstruments, (paymentInstruments)=>
              data.PaymentInstruments = paymentInstruments
              if not error
                paymentInstruments = _.filter(data.PaymentInstruments, (paymentInstrument) -> paymentInstrument.Type in Config.supportedPaymentOptions )
                if paymentInstruments.length is 0
                  TWC.MVC.Router.load "pin-view",
                    data: [
                      "", "", "paymentError", "", ()=>
                        TWC.MVC.History.reset(2)
                        TWC.MVC.History.popState()
                        #activate purchase flow again after subscriber has added payment option
                        setTimeout =>
                          @purchaseProduct(e, upgrade)
                        , 1000

                    ]
                else
                  TWC.MVC.Router.load "paymentconfirmation-view",
                    data: [@model, selectedPricingPlan, paymentInstruments, "", upgrade, null, purchaseCallback, purchaseFailCallback]


        if Config.enablePaymentPin
          CDSubscriberManagement.retrieveDevicePinStatus (error, hasPin) =>
            if hasPin
              TWC.MVC.Router.load "pin-view",
                data: [
                  @model.get("Name"), "", "purchasePin", purchaseProduct, purchaseFailCallback
                ]
            else
              purchaseProduct()
        else
          purchaseProduct()

      else
        historySize = TWC.MVC.History.size()
        loginConfirmation = new LoginConfirmation()
        loginConfirmation.load
          data: [false
            , ()=>
              TWC.MVC.Router.load "login-view",
                callback: ()=>
                  @ot.trackPageView('purchaselogin.html')
                data: [
                  ()=>
                    @model.refresh =>
                      entitledPricingPlan = @model.get("PricingPlans").findWhere({ Id : @model.get("EntitledPricingPlanId")})
                      if entitledPricingPlan?
                        messageAlert = new Alert();
                        messageAlert.load
                          data: ["", Manifest.payment_already_purchased, Manifest.ok, ()=>
                            return purchaseCallback() if purchaseCallback?
                            TWC.MVC.History.reset(historySize)
                            TWC.MVC.History.popState()
                          ]
                      else
                        @purchaseProduct(e, upgrade, purchaseCallback, purchaseFailCallback, pricingPlan)
                  , ()=>
                    purchaseFailCallback?()
                ]
            , false
            , ()=>
                purchaseFailCallback?()
          ]

    rentProduct: (e) ->
      @purchaseProduct(e, false, null, null, @rentUHDPricingPlan)

    moveToMenu: (focusOnLast=false) ->
      if focusOnLast
        TWC.MVC.Router.setFocus $("#subMenuItems .menuButton:last")
      else
        if $("#submenu_0").length
          if $(".submenu.select").length
            TWC.MVC.Router.setFocus "##{$(".submenu.select").attr("id")}"
          else
            TWC.MVC.Router.setFocus "#submenu_0"
        else
          @moveToMainMenu()

    moveToMainMenu: ->
      TWC.MVC.Router.setActive "menu-zone"
      TWC.MVC.Router.setFocus "#browse"

    moveToContent: ->
      log "moveToContent"
      if $(".buttonContainer .button:not(.disable):first").length
        TWC.MVC.Router.setFocus $(".buttonContainer .button:not(.disable):first")
      else if $("#favoritesButton:not(.disable)") and $("#noTrailerAvailable").hasClass("disable")
        TWC.MVC.Router.setFocus $("#favoritesButton")
      else if $("#pointerNaviUp").is(":visible")
        TWC.MVC.Router.setFocus("#pointerNaviUp")
      else if $("#pointerNaviDown").is(":visible")
        TWC.MVC.Router.setFocus("#pointerNaviDown")
      else if @recommendedProductsAvailable
        TWC.MVC.Router.setFocus("recommendedProduct_0")

    refresh: ->
      authenticated = Authentication.isLoggedIn()
      @model = new CDProductModel({Id:@prodId, manifest: Manifest, SubMenuTitle: SubMenuHelper.getSubMenuTitle(), SubMenuOptionalVisible: SubMenuHelper.getSubMenuOptionalVisible(), SubMenuItems: SubMenuHelper.getContextMenu(), authenticated: authenticated})
      super

    scrollUp: ->
      $(".productWindow").animate({scrollTop : $(".productWindow").scrollTop() - 500})

    scrollDown: ->
      $(".productWindow").animate({scrollTop: $(".productWindow").scrollTop() + 500})
      


  productDetailView.get()
