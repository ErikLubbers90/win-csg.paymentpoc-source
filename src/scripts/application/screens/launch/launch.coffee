define [
  "framework",
  "config",
  "jquery",
  "backbone",
  "moment",
  "../../models/baseModel",
  "i18n!../../../lang/nls/manifest",
  "i18n!../../../lang/nls/regionsettings",
  "application/api/authentication",
  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDSubscriberManagement",
  "../../popups/generic/alert",
  "../../models/analytics/omnituretracker"
], (TWC, Config, $, Backbone, Moment, BaseModel, Manifest, RegionSettings, Authentication, CDBaseRequest, CDSubscriberManagement, Alert, OmnitureTracker) ->


  launchModel = BaseModel.extend
    defaults:
      "manifest": Manifest,

  class launchView extends TWC.MVC.View
    uid: 'launch-view'

    el: "#screen"

    type: "page"

    model: new launchModel()

    external:
      html:
        url:"skin/templates/screens/launch/launch.tpl"

    events:
      "enter #loginOption":   "showLoginWithoutPromo"
      "enter #promoOption":   "showLoginWithPromo"
      "enter #browseOption":  "showStorefront"


    defaultFocus: "#loginOption"


    onload: ->


    onRenderComplete: ->


    onZoneRenderComplete: ->


    showLoginWithoutPromo: ->
      TWC.MVC.Router.load "register-view",
        data: [null, null, null, null, false]


    showLoginWithPromo: ->
      TWC.MVC.Router.load "register-view",
        data: [null, null, null, null, true]

    showStorefront: ->
      TWC.MVC.Router.load "home-view",
        data: [null, null, null, null]


  launchView.get()
