define [
  "framework",
  "config",
  "jquery",
  "backbone",
  "moment",
  "../../models/baseModel",
  "i18n!../../../lang/nls/manifest",
  "i18n!../../../lang/nls/regionsettings",
  "application/api/cd/CDPaymentManagement",
  "application/api/authentication",
  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDSubscriberManagement",
  "../../popups/generic/alert",
  "../../popups/generic/alertWithOptions",
  "../../models/analytics/omnituretracker"
], (TWC, Config, $, Backbone, Moment, BaseModel, Manifest, RegionSettings, CDPaymentManagement, Authentication, CDBaseRequest, CDSubscriberManagement, Alert, AlertWithOptions, OmnitureTracker) ->


  redemptionModel = BaseModel.extend
    defaults:
      "manifest": Manifest,      
    
  class redemptionView extends TWC.MVC.View
    uid: 'redemption-view'

    el: "#screen"

    type: "page"

    model: new redemptionModel()

    external:
      html:
        url:"skin/templates/screens/launch/redemption.tpl"

    events:
      "enter #submitPromoButton":        "validatePromoCode"
      "enter #submitRedemptionButton":   "confirmSelection"
      "enter #browseMoviesButton":       "showStorefront"
      "enter #browseLibraryButton" :     "showLibrary"
      "enter .product":                  "selectPromoProduct"
      "swipeLeft .playlistContent":      "itemFocusLeft"
      "swipeRight .playlistContent":     "itemFocusRight"
      


    defaultFocus: "#inputField"

    selectedProducts: []


    onload: ->


    onRenderComplete: ->
      

    onZoneRenderComplete: ->
      $("#promotionContent").hide()
      @adjustTitle()

    goBack: ->
      if Authentication.isLoggedIn()
        TWC.MVC.Router.load "storefrontpage-view",
          data: []     
      else
        TWC.MVC.Router.load "register-view",
          data: []   


    showStorefront: ->
      TWC.MVC.Router.load "storefrontpage-view",
        data: []

    showLibrary: ->
      TWC.MVC.Router.load "locker-view",
        data: []                 

    triggerAlert: (type = "global") ->        
      #custom alert messages       
      if type is "confirm"
        description = "Proceed with selected titles?"
        leftButton = "Yes"
        rightButton = "Continue browsing"

      messageAlert = new AlertWithOptions()
      messageAlert.load
        data: [description, rightButton, leftButton]
      $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])       


    confirmSelection: ()->
      return if $("#submitRedemptionButton").hasClass("disabled")
      confirmSelectionAlert = new AlertWithOptions()
      confirmSelectionAlert.rightButtonAction = ()=>
        confirmSelectionAlert.close()
        @submitPromoCode()
        
      confirmSelectionAlert.leftButtonAction = () =>
        confirmSelectionAlert.close()

      confirmSelectionAlert.load
        data: ["", Manifest.redemption_proceed, Manifest.popup_confirm, Manifest.redemption_continue_browsing]         

          
    renderSuccess: ->
      $("#redemptionContent").hide()
      $("#promotionContent").hide()
      $("#successContent").show()
      TWC.MVC.Router.setFocus "#browseMoviesButton"

    submitPromoCode: ->
      #check if user has selected the given amount of products  
      CDPaymentManagement.submitOrder([], @pricingPlanId, @selectedProducts, [@redemptionCode], (redemptionError, redemptionResponse) =>
        if redemptionError
          $(TWC.MVC.View.eventbus).trigger("loading", false)
          @selectedProducts = []
        else
          @renderSuccess()
        )


    adjustTitle: ->
      if @productAmount? and @productAmount < 11
        if @productAmount is 1 then amount = "one"
        if @productAmount is 2 then amount = "two"
        if @productAmount is 3 then amount = "three"
        if @productAmount is 4 then amount = "four"
        if @productAmount is 5 then amount = "fove"
        if @productAmount is 6 then amount = "six"
        if @productAmount is 7 then amount = "seven"
        if @productAmount is 8 then amount = "eight"
        if @productAmount is 9 then amount = "nine"
        if @productAmount is 10 then amount = "ten"
      else
        amount = "" 
      $("#promotionContent .redemptionTitle").html("<span class='blue'>Select</span> #{amount} free UHD movies")      


    adjustSubtitle: (amount, selectedAmount) ->  
      selectedAmount = @selectedProducts.length
      amount = @productAmount
      
      if selectedAmount > 0
        message = "#{selectedAmount} of #{amount}" + Manifest.redemption_titles_selected
      else
        message = ""  

      $("#promotionContent .redemptionSubtitle").html(message)



    validatePromoCode: ->
      redemptionCode = $("#inputField").val().substr(0, @redemptionCodeLength)

      @redemptionCode = redemptionCode
      # Check if a redemptioncode has been filled in

      if redemptionCode? and redemptionCode.length <= 4

        messageAlert = new Alert()
        messageAlert.load
          data: ["", Manifest.redemption_invalid_code, Manifest.ok]
        $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])
 
        return

      $(TWC.MVC.View.eventbus).trigger("loading", true)

      # Search for product using the redemptioncode
      CDPaymentManagement.searchProductsByCoupon(redemptionCode, (couponError, couponResponse) =>
        if not couponError
          if couponResponse?.Products?.length and couponResponse.Applications?.length
          
            productIds = []
            bundleRedemption = false
            applicationsProduct = couponResponse.Applications[0] #get first product information
            @pricingPlanId = applicationsProduct.PricingPlanIds[0]
            productInformation = couponResponse.Products[0]
            products = couponResponse.Products
            productTitle = productInformation.Name  #get first products name for the success popup
            productThumbnail = if productInformation.ThumbnailUrl? then productInformation.ThumbnailUrl else ""

            #if promotioncode has multiple products we need the product id's for the submitorder later on
            if couponResponse.Applications.length > 1
                @singleBundleItem = false
                bundleRedemption = true
                for prodId in couponResponse.Applications
                  productIds.push(prodId.ProductId)
            #only one product or bundled product
            else
              if couponResponse.Products[0].StructureType in [2,4]
                #single bundled product - check used for success screen to disable some options
                @singleBundleItem = true
              productIds.push(applicationsProduct.ProductId)

            @productAmount = products.length  
            #display promotion content
            @showContent(productTitle, productThumbnail, bundleRedemption, products)    

              
          else
            $("#inputField").val("")
            $(".redemption-char .text").text("")
            # No products found
            $(".redemption .actionResult").html(Manifest.code_redemption_code_error)
            $(TWC.MVC.View.eventbus).trigger("loading", false)

        else
          $("#inputField").val("")
          $(".redemption-char .text").text("")
          $(".redemption .actionResult").html(CDPaymentManagement.orderErrors(couponError))
          $(TWC.MVC.View.eventbus).trigger("loading", false)
      )    



    showContent: (productTitle, productThumbnail, bundleRedemption, products)->
      #show the selectable titles
      TWC.MVC.Router.find("keyboard-zone").setRedemptionSuccestoTrue?()

      $("#redemptionContent").hide()
      $("#promotionContent").show()
      $(".redemption").addClass("noBackground")

      @adjustTitle()

      if bundleRedemption
        @bundleRedemption = true
      $(".redemption-success h1").html(Manifest.settings_added_to_library.replace("%TITLE%", productTitle))
      if products
        i = 0
        for product in products
          $(".redemptionPlaylistContent").append("""<div id="playlist_product-#{i}" data-title="#{product.Name}" class="product content-sound" data-left="itemFocusLeft" data-right="itemFocusRight" data-down="itemFocusDown" data-id="#{product.Id}" data-type="#{product.StructureType}">
          <div class="productBorder"></div>
          <img src="#{product.ThumbnailUrl}" alt="#{product.Name}" /></div>""")
          i++

      else
        if productThumbnail.length then $(".content .top").prepend("<img src=\"#{productThumbnail}\"/>")

      
      TWC.MVC.Router.setFocus "#playlist_product-0"
      $(TWC.MVC.View.eventbus).trigger("loading", [false])



    selectPromoProduct: ->
      $el = $(TWC.MVC.Router.getFocus())

      #add/remove product from selectedProducts array      
      productId  = $el.data("id")
      if $el.hasClass("selected")
        $el.removeClass("selected")
        itemIndex = @selectedProducts.indexOf(productId)
        @selectedProducts.splice(itemIndex, 1) if itemIndex > -1
      else  
        $el.addClass("selected")
        @selectedProducts.push(productId)

      @adjustSubtitle()  
      @adjustSubmitButton()



    adjustSubmitButton: ->
      if @selectedProducts.length >= @productAmount
        $("#submitRedemptionButton").removeClass("disabled")  
      else
        $("#submitRedemptionButton").addClass("disabled")  

      

    focusIsOnProduct: ($focusedEl) ->
      $focusedEl.hasClass "product"


    itemFocusLeft: ->
      $el = $(TWC.MVC.Router.getFocus())
      return if !$el

      # Current focus on product
      if @focusIsOnProduct($el)
        focusedProductIndex = parseInt($el.attr("id").split("-")[1])

        # Focus on next playlist row if current rowindex less than 4
        if focusedProductIndex > 0
          # Focus on index of next playlist row if exists
          rowCnt = 1

          if $(".redemptionPlaylistContent .product:not('.disable'):eq(#{focusedProductIndex-rowCnt})").length
            $nextFocus = $(".redemptionPlaylistContent .product:not('.disable'):eq(#{focusedProductIndex-rowCnt})")

            #width of the item
            scrollWidth = $nextFocus.width() + parseInt($nextFocus.css("margin-right"))

            newFocussedIndex = focusedProductIndex - 1
            #if next item is not fully visible
            if newFocussedIndex > 4
              newPos = (scrollWidth * (newFocussedIndex - 6)) * -1
              $(".redemptionPlaylistContent").css({
                "-o-transform": "translate(" + newPos + "px, 0)"
                "-webkit-transform": "translate3d(" + newPos + "px, 0, 0)",
              })

            TWC.MVC.Router.setFocus($nextFocus)



    itemFocusRight: ->
      $el = $(TWC.MVC.Router.getFocus())
      return if !$el

      # Current focus on product
      if @focusIsOnProduct($el)
        focusedProductIndex = parseInt($el.attr("id").split("-")[1])

        #calculate the next focusable element, if on featured block, then skip 2, otherwise 3
        rowCnt = 1

        if $(".redemptionPlaylistContent .product:not('.disable'):eq(#{focusedProductIndex+rowCnt})").length
          $nextFocus = $(".redemptionPlaylistContent .product:not('.disable'):eq(#{focusedProductIndex+rowCnt})")

          #width of the item
          scrollWidth = $nextFocus.width() + parseInt($nextFocus.css("margin-right"))

          newFocussedIndex = focusedProductIndex + 1
          #if next item is not fully visible
          if newFocussedIndex > 5
            newPos = (scrollWidth * (newFocussedIndex - 6)) * -1
            $(".redemptionPlaylistContent").css({
              "-o-transform": "translate(" + newPos + "px, 0)",
              "-webkit-transform": "translate3d(" + newPos + "px, 0, 0)"
            })

          TWC.MVC.Router.setFocus($nextFocus)


    itemFocusUp: ->
      return if @singleBundleItem
      if @redemptionSuccessFocus?
        focus = @redemptionSuccessFocus
        @redemptionSuccessFocus = null
      else
        focus = "#playlist_product-0"

      TWC.MVC.Router.setFocus(focus)

    itemFocusDown: ->
      @redemptionSuccessFocus = TWC.MVC.Router.getFocus()
      TWC.MVC.Router.setFocus("#redemptionBackButton")      
    

  redemptionView.get()
