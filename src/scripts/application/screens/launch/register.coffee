define [
  "framework",
  "settingPages",
  "config",
  "jquery",
  "../../models/baseModel",
  "application/api/cd/models/CDStoreFrontPage",
  "application/api/cd/CDBaseRequest",
  "application/api/cd/CDSubscriberManagement",
  "application/api/cd/CDPaymentManagement",
  "application/api/authentication",
  "i18n!../../../lang/nls/manifest",
  "i18n!../../../lang/nls/regionsettings",
  "../../popups/generic/alert",
  "../../popups/generic/alertWithOptions",
  "../../models/analytics/omnituretracker"
], (TWC, ConfigSettings, Config, $, BaseModel, CDStoreFrontPageModel, CDBaseRequest, CDSubscriberManagement, CDPaymentManagement, Authentication, Manifest, RegionSettings, Alert, AlertWithOptions, OmnitureTracker) ->

  registerModel = BaseModel.extend
    defaults:
      "manifest": Manifest,

  class registerView extends TWC.MVC.View
    uid: 'register-view'

    el: "#screen"

    type: 'page'

    model: new registerModel()

    #page:
    #1 - email validation
    #2 - password enter
    #3 - rendezvous code enter
    #4 - password reminder
    #5 - password confirmation
    page: 1

    userEmailValid: false

    external:
      html:
        url:"skin/templates/screens/launch/register.tpl"

    events:
#      "enter    .playlistContent .product" : "showProductDetail"
#      "enter    #subMenuItems .menuButton"                  : "showSubMenuContent"
      "enter #nextButton": "next"
      "enter #loginButton": "login"
      "enter #cancelButton": "cancel"
      "enter #setEndpoint": "setEndpoint"
      "enter #setSystemID": "setSystemID"
      "enter #loginBackButton": "cancel"
      "enter #screenCloseButton": "cancel"
      "return"              : "goBack"
      "enter #forgetPasswordButton": "forgetPassword"
      "enter #reminderLoginButton": "startLogin"
      "enter #helpButton": "goToHelp"
      "enter #createAccount" : "createAccount"
      "enter #redeemPromotionCode" : "goToRedemption"

    zones: [
      {uid:"menu-zone", data:[]}
    ]

    onload: (@loginCallback, @returnCallback = null, @pageToRender = 1, @useremail = null, @promo = false) ->
      @model.set("fullscreen", false)

    onunload: ->
      if @checkInterval?
        clearInterval(@checkInterval)
        @checkInterval = null

    onRenderComplete: ->
      @currentEndpoint = CDBaseRequest.overrideURL
      if !@currentEndpoint?
        @currentEndpoint = "qa4.services." + Config.csg[Config.environment].CDDomain

      @currentSystemID = CDBaseRequest.overrideSystemID
      if !@currentSystemID?
        @currentSystemID = Config.csg[Config.environment].SystemId

      $("#currentEndpoint").text(@currentEndpoint)
      $("#currentSystemID").text(@currentSystemID)

    onZoneRenderComplete: ->
      TWC.MVC.Router.setFocus "#inputFieldEmail"
      $(".redeemPromoCode").show() if @promo

      $("#inputFieldEmail").on "blur", (e)=>
        @verifyUserEmail($("#inputFieldEmail").val())


      $("#inputFieldPassword").on "blur", (e)=>
        password = $("#inputFieldPassword").val()
        if password.length > 0 and @userEmailValid
          $("#loginButton").removeClass("disabled")


    updateButtons: (visibleButtons)->
      $(".buttons .button").addClass("disable")
      for e in visibleButtons
        $("##{e}").removeClass("disable")


    renderPage: ->
      $(".header").removeClass("long-email")
      $(".loginContainer").addClass("step-1")

    setEndpoint: ->
      endpoint = $("#inputFieldEndpoint").val()
      if endpoint? and endpoint!=""
        @currentEndpoint = endpoint
        CDBaseRequest.overrideURL = endpoint
        $("#currentEndpoint").text(endpoint)
        $("#inputFieldEndpoint").val("")

    setSystemID: ->
      systemID = $("#inputFieldSystemID").val()
      if systemID? and systemID!=""
        @currentSystemID = systemID
        CDBaseRequest.overrideSystemID = systemID
        $("#currentSystemID").text(systemID)
        $("#inputFieldSystemID").val("")

    createAccount: ->
      TWC.MVC.Router.load "account-view",
        data: [@promo]

    goToStoreFront: ->
        TWC.MVC.Router.load "home-view",
          data: []

    goToRedemption: ->
      if Authentication.isLoggedIn()
        TWC.MVC.Router.load "redemption-view",
          data: []
      else
        @triggerAlert("noAuthentication")


    validateEmail: (email)->
      re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      return re.test(email);

    next: ()->
      if @page is 1
        @checkUserEmail()
      else if @page is 5
        @login()


    checkUserEmail: ()->
      TWC.SDK.Input.get().setBlockAll(true)

      if not @validateEmail(@useremail)
        @triggerAlert("emailError")
        TWC.SDK.Input.get().setBlockAll(false)
        return false

      # Authentication.isValidUser @useremail
      #   #use device PIN callback
      #   ,(resp) =>
      #     if resp.Code is 113
      #       #username already used, proceed to password page
      #       @triggerAlert("emailError")
      #       TWC.SDK.Input.get().setBlockAll(false)
      #       return false
      #     else
      #       return true


    verifyUserEmail: (email) ->
      @userEmailValid = false
      if email
        @useremail = email
      TWC.SDK.Input.get().setBlockAll(true)
      Authentication.isValidUser @useremail
        #use device PIN callback
        ,(resp) =>
          if resp.Code is 113
            #username already used, proceed to password page
            $("#forgetPasswordButton").removeClass("disabled")
            TWC.SDK.Input.get().setBlockAll(false)
            @userEmailValid = true
            password = $("#inputFieldPassword").val()
            if password.length > 0 and @userEmailValid
              $("#loginButton").removeClass("disabled")
            return false
          else
            return true
            @userEmailValid = false
            TWC.SDK.Input.get().setBlockAll(false)




    triggerAlert: (type = "global") ->
      #custom alert messages
      button = Manifest.ok
      if type is "global"
        title = ""
        message = Manifest.error_occurred
      if type is "noAuthentication"
        title = Manifest.not_logged_in
        message = Manifest.register_please_login
      if type is "emailError"
        title = Manifest.error_occurred
        message = Manifest.error_message_enter_valid_email
      if type is "accountError"
        title = Manifest.regsiter_account_not_found
        message = Manifest.register_please_enter

      messageAlert = new Alert()
      messageAlert.load
        data: [title, message, button]
      $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])



    login: ()->
      return if $("#loginButton").hasClass("disabled")
      @useremail = $("#inputFieldEmail").val()

      @password = $("#inputFieldPassword").val()
      if @useremail.length is 0
        @triggerAlert("emailError")
      if @password.length is 0
        @triggerAlert("accountError")
        # $("#loginResult").html(Manifest.invalid_password_entered).show()
        return

      #      @password = "Test1234"
      $(TWC.MVC.View.eventbus).trigger("loading", [true, "keep"])
      #use authentication api and determine result
      Authentication.manualLogin @useremail, @password
        #callback
      ,(error) =>
        if error
          @password = ""
          $("#inputFieldPassword").val("")
          $("#inputFieldEmail").val("") if !@userEmailValid
          @triggerAlert("accountError")
        else
          $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])
          if @promo
            @goToRedemption()
          else
            @goToStoreFront()




    getSubCodeError: (subcodeError) ->
      switch subcodeError
      #specify subcodes related to login errors
        when 11701 then message = Manifest.error_message_invalid_login #AccessDeniedLoginInvalid
        when 11702 then message = Manifest.login_revoked_popup #AccessDeniedLoginRevoked
        when 11703 then message = Manifest.invalid_password_entered #AccessDeniedInvalidPassword
        when 11704 then message = Manifest.login_session_expired #AccessDeniedInactive
        when 11705 then message = Manifest.error_household_access_denied #AccessDeniedInsufficientAccess
        when 11706 then message = Manifest.change_password_invalid_current_password #AccessDeniedInvalidPasswordChallengeResponse
        when 11707 then message = Manifest.player_content_access_denied_message #AccessDeniedBusinessUnitNotFound
        when 11708 then message = Manifest.player_content_access_denied_message #AccessDeniedDayAndTimeDenied
        when 11709 then message = Manifest.player_content_access_denied_message #AccessDeniedBusinessUnitDisabled
        when 11710 then message = Manifest.error_subscriber_not_active #AccessDeniedInsufficientSubscriberAccess
        when 11711 then message = Manifest.error_household_access_denied #AccessDeniedSubscriberInputDisabled
        when 11712 then message = Manifest.error_household_access_denied #AccessDeniedInsufficientBusinessUnitAccess
        when 11713 then message = Manifest.error_household_access_denied #AccessDeniedTokenInvalid
        when 11714 then message = Manifest.player_content_access_denied_message #AccessDeniedAnonymousProxyServerDetected
        when 11715 then message = Manifest.error_household_access_denied #AccessDeniedInvalidLocation
        when 11716 then message = Manifest.error_message_general_error #Unspecified
        when 11717 then message = Manifest.error_login_suspended_account_message #AccessDeniedTemporaryPasswordExpired
        when 11718 then message = Manifest.error_message_invalid_login #The device session promotion pin was invalid
        when 11719 then message = Manifest.error_message_invalid_login #Invalid oAuth header
        when 11720 then message = Manifest.status_pending #AccessDeniedPendingApproval
      message

    getError: (errorCode) ->
      switch errorCode
      #specify common login errors and codes related to device registration
        when 117 then message = Manifest.error_message_invalid_login #AccessDeniedLoginInvalid
        when 118 then message = Manifest.error_message_email_address_invalid #InvalidProperty
        when 804 then message = Manifest.already_logged_in #DeviceAlreadyAssociated
        when 805 then message = Manifest.error_message_device_not_available #DeviceNotAvailable
        when 806 then message = Manifest.device_not_found #DeviceNotFound
        when 810 then message = Manifest.error_message_device_not_available #InvalidPhysicalDeviceType
        when 811 then message = Manifest.max_devices_exceeded #AssociationLimitViolation
        when 812 then message = Manifest.duplicate_device_error #DuplicateDeviceNickname
        when 813 then message = Manifest.error_registering_tv #ExternalDeviceRegistrationFailure
      #global error message, error code is not defined
        else message = Manifest.error_occurred
      message

    handleLoginError: (error)->
      $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])

      #if there is a subcode defined in the error result we can determine a more accurate description
      if error.Code is 117 and error.Subcode
        message = @getSubCodeError(error.Subcode)

        $("#loginResult").html(message).show()
        $("#loginResult a").click (e) =>
            url = $("#loginResult a").attr('href')
            @openURI(url)
            #window.open(url, '_blank');

          # Clear storage to prevent user logged in
          Authentication.logout()

      else if error.Code
        message = @getError(error.Code)

        $("#loginResult").html(message).show()
        $(TWC.MVC.View.eventbus).trigger("loading", [false, "reset"])

        if @page isnt 5
          # Clear storage to prevent user logged in
          Authentication.logout()


    cancel: ()->
      TWC.MVC.History.popState()

    goBack: ()->
      @returnCallback = null
#      TWC.MVC.History.popState()


    forgetPassword: ()->
      return if $("#forgetPasswordButton").hasClass("disabled")
      confirmationAlert = new AlertWithOptions()
      confirmationAlert.rightButtonAction = ()->
        @close()

      confirmationAlert.leftButtonAction = ()->
        @close ()->
          TWC.MVC.Router.find("login-view").forgetPasswordAction()

      confirmationAlert.load
        data: ["", Manifest.popup_forgot_password_description, Manifest.popup_no, Manifest.popup_confirm]

    forgetPasswordAction: ()->
      CDSubscriberManagement.generatePassword @useremail, (error, status, response)=>
        if error
          #TODO handle error
          log("error", error)
          return


    goToHelp: ()->
      TWC.MVC.History.reset()
      userEmail = null
      if @useremail
        userEmail = @useremail
      TWC.MVC.Router.load "settings-view",
        data: [0, userEmail, @loginCallback, @returnCallback]

    #Focus events

    focusUp: ->
      if TWC.MVC.Router.getFocus() is "#helpButton"
        TWC.MVC.Router.setFocus $(".buttons .button:visible:last")

    focusDown: ->
      if $(".helpContainer:not(.disabled) #helpButton").length > 0
        TWC.MVC.Router.setFocus "#helpButton"

    focusRight: ->
      if TWC.MVC.Router.getFocus() is "#cancelButton"
        if @page is 1
          TWC.MVC.Router.setFocus "#nextButton"
        else if @page is 4
          TWC.MVC.Router.setFocus "#reminderLoginButton"

    focusDownFromKeyboard: ->
      TWC.MVC.Router.setActive "login-view"
      if @page in [1, 5]
        TWC.MVC.Router.setFocus "#nextButton"
      if @page is 2
        TWC.MVC.Router.setFocus "#loginButton"

    moveToContent: ->
      TWC.MVC.Router.setFocus "#inputField"

    moveToMenu: ->
      log "menu removed"


  registerView.get()
