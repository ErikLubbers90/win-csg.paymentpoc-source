define [
  "framework",
  "underscore",
  "config",
], (TWC, _, Config) ->

  iframe = document.getElementById("sandbox")

  # This module allows to use Akamai QOS
  #
  class AkamaiQOS extends TWC.SDK.Base

    akaPlugin: null

    init: (@options = {}) ->
      if not Config.akamai_enabled
        return

      console.debug('[AKAMAI] new Akamai', @options)

      if not @akaPlugin?
        try
          iframeWindow = iframe.contentWindow
          iframeWindow.AKAMAI_MEDIA_ANALYTICS_CONFIG_FILE_PATH = Config.akamai_xml_path
          Akamai = iframeWindow.AkaHTML5MediaAnalytics
          console.log("akamai=", Akamai)

          @akaPlugin = new Akamai(@options.akaPluginCallback)
          @akaPlugin.setData("Title", @options.data.title)
          @akaPlugin.setData("eventName", @options.data.eventName)
          @akaPlugin.setData("Category", @options.data.category)
          @akaPlugin.setData("Show", @options.data.show) if @options.data.show?
          @akaPlugin.setData("contentType", @options.data.contentType)
          @akaPlugin.setData("Device", @options.data.device)
          @akaPlugin.setData("playerId", "TVPlayer")
          @akaPlugin.setData("viewerId", @options.data.viewerId)

        catch e
          console.log("Akamai - error: " + e)


    #Implemented calls
    handleSessionInit: ->
      return if not Config.akamai_enabled
      console.debug("[AKAMAI] handleSessionInit")
      @akaPlugin.handleSessionInit()

    handlePlaying: ->
      return if not Config.akamai_enabled
      console.debug("[AKAMAI] handlePlaying")
      @akaPlugin.handlePlaying()

    handlePause: ->
      return if not Config.akamai_enabled
      console.debug("[AKAMAI] handlePause")
      @akaPlugin.handlePause()

    handleResume: ->
      return if not Config.akamai_enabled
      console.debug("[AKAMAI] handleResume")
      @akaPlugin.handleResume()

    #not used
    handleBitRateSwitch: (newBitRate)->
      return if not Config.akamai_enabled
      console.debug("[AKAMAI] handleBitRateSwitch", newBitRate)
      @akaPlugin.handleBitRateSwitch(newBitRate)

    #not used
    handleBufferStart: ->
      return if not Config.akamai_enabled
      console.debug("[AKAMAI] handleBufferStart")
      @akaPlugin.handleBufferStart()

    #not used
    handleBufferEnd: ->
      return if not Config.akamai_enabled
      console.debug("[AKAMAI] handleBufferEnd")
      @akaPlugin.handleBufferEnd()

    handleApplicationExit: ->
      return if not Config.akamai_enabled
      console.debug("[AKAMAI] handleApplicationExit")
      @akaPlugin.handleApplicationExit()

    handleError: (errorCode) ->
      return if not Config.akamai_enabled
      console.debug("[AKAMAI] handleError")
      @akaPlugin.handleError(errorCode)

    handlePlayEnd: (endReasonCode) ->
      return if not Config.akamai_enabled
      console.debug("[AKAMAI] handlePlayEnd")
      log 'Akamai - handlePlayEnd'
      @akaPlugin.handlePlayEnd(endReasonCode)
