define [
  "jquery",
  "framework",
  "config"
], ($, TWC, Config)->

  iframe = document.getElementById("sandbox")

  class OmnitureTracker extends TWC.SDK.Base

    init: ()->
      if not Config.omniture_tracking
        return

    trackPageView: (pageName)->
      return if not Config.omniture_tracking
      console.debug("[OMNITURE] trackPageView", pageName)
      try
        iframe.contentWindow.sCode.trackPageView(pageName)
      catch e
        console.log "[OMNITURE] trackPageView failed"

    trackOutboundClick: (targetUrl, adId)->
      return if not Config.omniture_tracking
      console.debug("[OMNITURE] trackOutboundClick", targetUrl, adId)
      try
        iframe.contentWindow.sCode.trackOutboundClick(targetUrl, adId)
      catch e
        log "[OMNITURE] trackOutboundClick failed"

    trackOutboundClickToBuy: (targetUrl, adId)->
      return if not Config.omniture_tracking
      console.debug("[OMNITURE] trackOutboundClickToBuy", targetUrl, adId)
      try
        iframe.contentWindow.sCode.trackOutboundClickToBuy(targetUrl, adId)
      catch e
        log "[OMNITURE] trackOutboundClickToBuy failed"

    trackVideo: (name, event)->
      return if not Config.omniture_tracking
      console.debug("[OMNITURE] trackVideo", name, event)
      try
        iframe.contentWindow.sCode.trackVideo(name, event)
      catch e
        log "[OMNITURE] trackVideo failed"
