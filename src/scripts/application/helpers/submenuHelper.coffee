define [
  "jquery"
  "framework"
  "underscore"
  "i18n!../../lang/nls/manifest"

], ($, TWC, _, Manifest)->

  # Generic submenu class for juke app
  class SubMenu

    @CONTEXT_STOREFRONT: "storefront"
    @CONTEXT_LOCKER: "locker"
    @CONTEXT_SEARCH: "search"
    @CONTEXT_FAVORITES: "favorites"

    @subMenuStoreFrontPages: []
    @selectedSubMenuStoreFrontPage: null
    @subMenuCategoriesFilter: []
    @selectedSubMenuCategoriesFilter: null

    @totalProductsInLockerCollection: 0

    @selectedMenuItem: null

    # Indicates the current submenucontext: storefront/locker/search
    @subMenuContext: @CONTEXT_STOREFRONT


    @setTotalProductsInLockerCollection: (total) ->
      @totalProductsInLockerCollection = @checkTotalProductsInLockerCollection(total)

    @getTotalProductsInLockerCollection: ->
      return @totalProductsInLockerCollection

    @checkTotalProductsInLockerCollection: (total) ->
      if total > @totalProductsInLockerCollection
        return total
      else
        return @totalProductsInLockerCollection



    # Main menu
    @setSelectedMenuItem: (@selectedMenuItem) ->

    @getSelectedMenuItem: ->
      if @selectedMenuItem?
        @selectedMenuItem
      else
        @selectedMenuItem = "browse"

    # Video submenu #
    @setStoreFrontPages: (@subMenuStoreFrontPages) ->

    @getStoreFrontPages: (defaultName)->
      if _.isEmpty(@subMenuStoreFrontPages) and defaultName then [{Name:defaultName, Id:null}] else @subMenuStoreFrontPages

    @setSelectedStorefrontPage: (@selectedSubMenuStoreFrontPage) ->

    @getSelectedStorefrontPage: ->
      if @selectedSubMenuStoreFrontPage?
        @selectedSubMenuStoreFrontPage
      else
        @selectedSubMenuStoreFrontPage = @getFirstCategoryId(@subMenuStoreFrontPages)

    # Video filter #
    @setCategoriesFilter: (@subMenuCategoriesFilter) ->

    @getCategoriesFilter: (defaultName)->
      if _.isEmpty(@subMenuCategoriesFilter) and defaultName then [{Name:defaultName, Id:null}] else @subMenuCategoriesFilter


    @setSelectedCategoryFilter: (@selectedSubMenuCategoriesFilter) ->

    @getSelectedCategoryFilter: ->
      if @selectedSubMenuCategoriesFilter?
        @selectedSubMenuCategoriesFilter
      else
        @selectedSubMenuCategoriesFilter = @getFirstCategoryId(@subMenuCategoriesFilter)

    # Locker
    @setLockerPages: (@lockerPages)->

    @getLockerPages: ->
      @lockerPages

    @setSelectedLockerPage: (@selectedLockerPage) ->

    @getSelectedLockerPage: ->
      if @selectedLockerPage?
        @selectedLockerPage
      else
        @selectedLockerPage = @getFirstCategoryId(@lockerPages)

    @setSubMenuOptionalVisible: (@subMenuOptionalVisible)->

    @getSubMenuOptionalVisible: ()->
      if @subMenuContext is @CONTEXT_LOCKER
        @subMenuOptionalVisible
      else
        false

    # Favorites
    @setFavoritesPages: (@favoritesPages)->

    @getFavoritesPages: ->
      @favoritesPages

    @setSelectedFavoritesPage: (@selectedFavoritesPage) ->

    @getSelectedFavoritesPage: ->
      if @selectedFavoritesPage?
        @selectedFavoritesPage
      else
        @selectedFavoritesPage = @getFirstCategoryId(@favoritesPages)

    # Retrieve id of first item in array
    @getFirstCategoryId: (categories) ->
      firstCategory = _.first(categories)
      firstCategoryId = if categories?.length and firstCategory? then firstCategory.Id else ""

    # Context dependent #
    @setSubMenuContext: (@subMenuContext) ->

    @getSubMenuContext: ->
      @subMenuContext

    # Returns submenu based on selected context
    @getSubMenuTitle: ->
      if @subMenuContext is @CONTEXT_STOREFRONT
        Manifest.browse
      else if @subMenuContext is @CONTEXT_LOCKER
        Manifest.library
      else if @subMenuContext is @CONTEXT_FAVORITES
        Manifest.favorites
      else if @subMenuContext is @CONTEXT_SEARCH
        Manifest.search
      else
        Mainfest.browse


    @getContextMenu: ->
      if @subMenuContext is @CONTEXT_STOREFRONT
        @getStoreFrontPages()
      else if @subMenuContext is @CONTEXT_LOCKER
        @getLockerPages()
      else if @subMenuContext is @CONTEXT_FAVORITES
        @getFavoritesPages()
      else if @subMenuContext is @CONTEXT_SEARCH
        []
      else
        @getCategoriesFilter()

    @getContextMenuSelected: ->
      if @subMenuContext is @CONTEXT_STOREFRONT
        @getSelectedStorefrontPage()
      else if @subMenuContext is @CONTEXT_LOCKER
        @getSelectedLockerPage()
      else if @subMenuContext is @CONTEXT_FAVORITES
        @getSelectedFavoritesPage()
      else if @subMenuContext is @CONTEXT_SEARCH
        null
      else
        @getSelectedCategoryFilter()