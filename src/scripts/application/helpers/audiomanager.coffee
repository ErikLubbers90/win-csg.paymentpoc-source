define [
  "framework",
  "jquery",
  "config"
], (TWC, $, Config)->

  class AudioManager

    @basePath: "skin/audio"

    @context:null
    @sounds: {}
    @initialize: ()->

      if Config.platform isnt "sony"
        log("[AudioManager] initialize")

        try
          # Fix up for prefixing
          window.AudioContext = window.AudioContext||window.webkitAudioContext;
          @context = new AudioContext()
        catch e
          log('Web Audio API is not supported in this browser')

        @loadSounds(
          ["#{@basePath}/focus_button.mp3",
           "#{@basePath}/back_button.mp3",
           "#{@basePath}/confirmation_button.mp3",
           "#{@basePath}/error_action.mp3",
           "#{@basePath}/focus_button.mp3",
           "#{@basePath}/menu_button.mp3"]
          ["focus",
           "back",
           "confirmation",
           "error"
           "keyboard"
           "menu"]
        )

        @addEventListeners()

    @destroy: ()->
      @context?.close?()
      @context = null
      $("body").off(".audio")

    @loadSound: (url, key) ->
      request = new XMLHttpRequest()
      request.open('GET', url, true)
      request.responseType = 'arraybuffer'

      # Decode asynchronously
      request.onload = =>
        @context.decodeAudioData(
          request.response,
          (buffer) =>
            @sounds[key] = buffer
          , ->
            log "error loading sound: "+url
        )
      request.send()

    @loadSounds: (urls, keys)->

      bufferLoader = new window.BufferLoader(
        @context,
        urls,
        (bufferList)=>
          for buffer, index in bufferList
            @sounds[keys[index]] = buffer
      )
      bufferLoader.load()

    @playSound: (key, volume = 1.2) ->
      return log("sound #{key} does not exist") if not "key" in @sounds
      if @context?
        source = @context.createBufferSource()     # creates a sound source
        source.buffer = @sounds[key]               # tell the source which sound to play

        # Create a gain node to control volume
        gainNode = @context.createGain()
        # set new volume
        gainNode.gain.value = volume;
        # Connect the source to the gain node.
        source.connect(gainNode);
        # Connect the gain node to the destination.
        gainNode.connect(@context.destination)

        #source.connect(@context.destination)       # connect the source to the context's destination (the speakers)
        source.start(0);                           # play the source now
        #note: on older systems, may have to use deprecated noteOn(time);

    @addEventListeners: ()->
      $('body').on "left.audio right.audio up.audio down.audio", (e)=>
        $el = $(e.target)
        return if not $el.attr("data-#{e.type}")?

        if $el.hasClass("keyboard-sound")
          @playKeyboardAudio()
        else if $el.hasClass("menu-sound")
          @playMenuAudio()
        else if $el.hasClass("content-sound")
          @playFocusAudio()

      $('body').on "return.audio", ()=>
        @playBackAudio()

      $('body').on "enter.audio", (e)=>
        $el = $(e.target)
        return if $el.hasClass("no-sound")
        @playConfirmationAudio()

    @playerLimit:3
    @playerActive:0

    @playFocusAudio: ()->
      @playSound("focus", 0.3)

    @playBackAudio: ()->
      @playSound("back")

    @playConfirmationAudio: ()->
      @playSound("confirmation")

    @playErrorAudio: ()->
      @playSound("error")

    @playKeyboardAudio: ()->
      @playSound("keyboard", 0.3)

    @playMenuAudio: ()->
      @playSound("menu")


## Include bufferLoader helper class
BufferLoader = (context, urlList, callback) ->
  @context = context
  @urlList = urlList
  @onload = callback
  @bufferList = new Array
  @loadCount = 0
  return

BufferLoader::loadBuffer = (url, index) ->
# Load buffer asynchronously
  request = new XMLHttpRequest
  request.open 'GET', url, true
  request.responseType = 'arraybuffer'
  loader = this

  request.onload = ->
# Asynchronously decode the audio file data in request.response
    loader.context.decodeAudioData request.response, ((buffer) ->
      if !buffer
        alert 'error decoding file data: ' + url
        return
      loader.bufferList[index] = buffer
      if ++loader.loadCount == loader.urlList.length
        loader.onload loader.bufferList
      return
    ), (error) ->
      console.error 'decodeAudioData error', error
      return
    return

  request.onerror = ->
    alert 'BufferLoader: XHR error'
    return

  request.send()
  return

BufferLoader::load = ->
  i = 0
  while i < @urlList.length
    @loadBuffer @urlList[i], i
    ++i
  return

window.BufferLoader = BufferLoader
