define [
  "jquery"
], ($)->

  class Background
    @_delayedExecution = null

    @setBackgroundImage: (regularUrl, blurUrl, blur=true, timeout = 1500, force = false) ->
      #only execute background request after delay of 2s, to prevent overload of image request when quickly changes focus
      clearTimeout(@_delayedExecution)

      #do not execute if new url is equal to current background image
      return if (if blur then blurUrl else regularUrl) is @imageUrl and not force

      @_delayedExecution = setTimeout =>
        imageUrl = if blur then blurUrl else regularUrl
        #first load the image in memory
        $img = $("<img>").attr('data-src', regularUrl).attr('data-blur-src', blurUrl)
        $img.one "load", ->
          # inject the image in page
          $backgroundContainer = $("#backgroundContainer")
          #if blur then $img.addClass("blur") else $img.removeClass("blur")
          $backgroundContainer.prepend($img)

          # remove older background(s)
          $backgroundContainerImages = $backgroundContainer.children("img:not(.disable)")
          if ($backgroundContainerImages.length >  1)
            $backgroundContainerImages
              .slice(-($backgroundContainerImages.length-1))
              .addClass("disable")
              .one("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd", ->
                $(this).remove()
              )
        $img.attr('src', imageUrl)
        @imageUrl = imageUrl

      , timeout

    @enableBlur: ->
      $backgroundContainer = $("#backgroundContainer")
      $backgroundContainerImages = $backgroundContainer.children("img:not(.disable)")
      if $backgroundContainerImages.length > 0
        $image = $($backgroundContainerImages[0])
        @setBackgroundImage($image.attr('data-src'), $image.attr('data-blur-src'), true, 0)

    @disableBlur: ->
      $backgroundContainer = $("#backgroundContainer")
      $backgroundContainerImages = $backgroundContainer.children("img:not(.disable)")
      if $backgroundContainerImages.length > 0
        $image = $($backgroundContainerImages[0])
        @setBackgroundImage($image.attr('data-src'), $image.attr('data-blur-src'), false, 0)

    @enableBackground: ->
      $("#backgroundContainer").show()

    @disableBackground: ->
      $("#backgroundContainer").hide()

