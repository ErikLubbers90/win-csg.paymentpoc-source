var language = locale.language;

require.config({
  shim: {
      "underscore": {
          exports: '_'
      }
  },
  paths: {
    "twc-universal-sdk": "framework/twc-universal-sdk-windows10",
    "framework": "framework/twc-universal-mvc",
    "jquery": "vendor/jquery/jquery",
    "underscore": "vendor/underscore/underscore",
    "moment": 'vendor/moment/moment',
    "moment-timezone": 'vendor/moment/moment-timezone',
    "numeral": 'vendor/numeral/numeral',
    "i18n": 'vendor/i18n/i18n',
    "backbone" : "vendor/backbone/backbone"
  },
  waitSeconds: 10
});


require([
    "framework",
    "application/init",
    "config",
    "jquery",
    "backbone",
    "moment",
    "numeral",
    "i18n!lang/nls/manifest",
    "i18n!lang/nls/regionsettings"
], function(TWC, App, Config, $, Backbone, Moment, Numeral, Manifest, RegionSettings){
  TWC.SDK.init(Config);
  TWC.MVC.Router.init();
  $(document).ready(function(){
    App.launch();
  });
});

requirejs.onError = function (err) {
    console.log("REQUIRE-JS: [ERROR] " , err);
    throw err;
};