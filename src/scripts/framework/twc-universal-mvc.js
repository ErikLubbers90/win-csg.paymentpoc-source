/**
* TWC MVC 1.0.1
*
* TWC Apps Development
* http://www.twcapps.com
* Copyright (c) 2009-2015 All Rights Reserved.
*
* TWC Commercial Software License
* info@twcapps.com
*/
(function (root, factory) {
  if (typeof define === 'function') {
    define(["jquery", "underscore", "twc-universal-sdk"], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(require("jquery"), require("underscore"), require("twc-universal-sdk"));
  } else {
    root.TWC = factory((root.jQuery || root.Zepto || root.ender), root._, root.TWC);
  }
}(this, function($, _, TWC) {
/**
 * @license almond 0.3.3 Copyright jQuery Foundation and other contributors.
 * Released under MIT license, http://github.com/requirejs/almond/LICENSE
 */
//Going sloppy to avoid 'use strict' string cost, but strict practices should
//be followed.
/*global setTimeout: false */

var requirejs, require, define;
(function (undef) {
    var main, req, makeMap, handlers,
        defined = {},
        waiting = {},
        config = {},
        defining = {},
        hasOwn = Object.prototype.hasOwnProperty,
        aps = [].slice,
        jsSuffixRegExp = /\.js$/;

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    /**
     * Given a relative module name, like ./something, normalize it to
     * a real name that can be mapped to a path.
     * @param {String} name the relative name
     * @param {String} baseName a real name that the name arg is relative
     * to.
     * @returns {String} normalized name
     */
    function normalize(name, baseName) {
        var nameParts, nameSegment, mapValue, foundMap, lastIndex,
            foundI, foundStarMap, starI, i, j, part, normalizedBaseParts,
            baseParts = baseName && baseName.split("/"),
            map = config.map,
            starMap = (map && map['*']) || {};

        //Adjust any relative paths.
        if (name) {
            name = name.split('/');
            lastIndex = name.length - 1;

            // If wanting node ID compatibility, strip .js from end
            // of IDs. Have to do this here, and not in nameToUrl
            // because node allows either .js or non .js to map
            // to same file.
            if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, '');
            }

            // Starts with a '.' so need the baseName
            if (name[0].charAt(0) === '.' && baseParts) {
                //Convert baseName to array, and lop off the last part,
                //so that . matches that 'directory' and not name of the baseName's
                //module. For instance, baseName of 'one/two/three', maps to
                //'one/two/three.js', but we want the directory, 'one/two' for
                //this normalization.
                normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
                name = normalizedBaseParts.concat(name);
            }

            //start trimDots
            for (i = 0; i < name.length; i++) {
                part = name[i];
                if (part === '.') {
                    name.splice(i, 1);
                    i -= 1;
                } else if (part === '..') {
                    // If at the start, or previous value is still ..,
                    // keep them so that when converted to a path it may
                    // still work when converted to a path, even though
                    // as an ID it is less than ideal. In larger point
                    // releases, may be better to just kick out an error.
                    if (i === 0 || (i === 1 && name[2] === '..') || name[i - 1] === '..') {
                        continue;
                    } else if (i > 0) {
                        name.splice(i - 1, 2);
                        i -= 2;
                    }
                }
            }
            //end trimDots

            name = name.join('/');
        }

        //Apply map config if available.
        if ((baseParts || starMap) && map) {
            nameParts = name.split('/');

            for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");

                if (baseParts) {
                    //Find the longest baseName segment match in the config.
                    //So, do joins on the biggest to smallest lengths of baseParts.
                    for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = map[baseParts.slice(0, j).join('/')];

                        //baseName segment has  config, find if it has one for
                        //this name.
                        if (mapValue) {
                            mapValue = mapValue[nameSegment];
                            if (mapValue) {
                                //Match, update name to the new value.
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                }

                if (foundMap) {
                    break;
                }

                //Check for a star map match, but just hold on to it,
                //if there is a shorter segment match later in a matching
                //config, then favor over this star map.
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                    foundStarMap = starMap[nameSegment];
                    starI = i;
                }
            }

            if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
            }

            if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join('/');
            }
        }

        return name;
    }

    function makeRequire(relName, forceSync) {
        return function () {
            //A version of a require function that passes a moduleName
            //value for items that may need to
            //look up paths relative to the moduleName
            var args = aps.call(arguments, 0);

            //If first arg is not require('string'), and there is only
            //one arg, it is the array form without a callback. Insert
            //a null so that the following concat is correct.
            if (typeof args[0] !== 'string' && args.length === 1) {
                args.push(null);
            }
            return req.apply(undef, args.concat([relName, forceSync]));
        };
    }

    function makeNormalize(relName) {
        return function (name) {
            return normalize(name, relName);
        };
    }

    function makeLoad(depName) {
        return function (value) {
            defined[depName] = value;
        };
    }

    function callDep(name) {
        if (hasProp(waiting, name)) {
            var args = waiting[name];
            delete waiting[name];
            defining[name] = true;
            main.apply(undef, args);
        }

        if (!hasProp(defined, name) && !hasProp(defining, name)) {
            throw new Error('No ' + name);
        }
        return defined[name];
    }

    //Turns a plugin!resource to [plugin, resource]
    //with the plugin being undefined if the name
    //did not have a plugin prefix.
    function splitPrefix(name) {
        var prefix,
            index = name ? name.indexOf('!') : -1;
        if (index > -1) {
            prefix = name.substring(0, index);
            name = name.substring(index + 1, name.length);
        }
        return [prefix, name];
    }

    //Creates a parts array for a relName where first part is plugin ID,
    //second part is resource ID. Assumes relName has already been normalized.
    function makeRelParts(relName) {
        return relName ? splitPrefix(relName) : [];
    }

    /**
     * Makes a name map, normalizing the name, and using a plugin
     * for normalization if necessary. Grabs a ref to plugin
     * too, as an optimization.
     */
    makeMap = function (name, relParts) {
        var plugin,
            parts = splitPrefix(name),
            prefix = parts[0],
            relResourceName = relParts[1];

        name = parts[1];

        if (prefix) {
            prefix = normalize(prefix, relResourceName);
            plugin = callDep(prefix);
        }

        //Normalize according
        if (prefix) {
            if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relResourceName));
            } else {
                name = normalize(name, relResourceName);
            }
        } else {
            name = normalize(name, relResourceName);
            parts = splitPrefix(name);
            prefix = parts[0];
            name = parts[1];
            if (prefix) {
                plugin = callDep(prefix);
            }
        }

        //Using ridiculous property names for space reasons
        return {
            f: prefix ? prefix + '!' + name : name, //fullName
            n: name,
            pr: prefix,
            p: plugin
        };
    };

    function makeConfig(name) {
        return function () {
            return (config && config.config && config.config[name]) || {};
        };
    }

    handlers = {
        require: function (name) {
            return makeRequire(name);
        },
        exports: function (name) {
            var e = defined[name];
            if (typeof e !== 'undefined') {
                return e;
            } else {
                return (defined[name] = {});
            }
        },
        module: function (name) {
            return {
                id: name,
                uri: '',
                exports: defined[name],
                config: makeConfig(name)
            };
        }
    };

    main = function (name, deps, callback, relName) {
        var cjsModule, depName, ret, map, i, relParts,
            args = [],
            callbackType = typeof callback,
            usingExports;

        //Use name if no relName
        relName = relName || name;
        relParts = makeRelParts(relName);

        //Call the callback to define the module, if necessary.
        if (callbackType === 'undefined' || callbackType === 'function') {
            //Pull out the defined dependencies and pass the ordered
            //values to the callback.
            //Default to [require, exports, module] if no deps
            deps = !deps.length && callback.length ? ['require', 'exports', 'module'] : deps;
            for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relParts);
                depName = map.f;

                //Fast path CommonJS standard dependencies.
                if (depName === "require") {
                    args[i] = handlers.require(name);
                } else if (depName === "exports") {
                    //CommonJS module spec 1.1
                    args[i] = handlers.exports(name);
                    usingExports = true;
                } else if (depName === "module") {
                    //CommonJS module spec 1.1
                    cjsModule = args[i] = handlers.module(name);
                } else if (hasProp(defined, depName) ||
                           hasProp(waiting, depName) ||
                           hasProp(defining, depName)) {
                    args[i] = callDep(depName);
                } else if (map.p) {
                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                    args[i] = defined[depName];
                } else {
                    throw new Error(name + ' missing ' + depName);
                }
            }

            ret = callback ? callback.apply(defined[name], args) : undefined;

            if (name) {
                //If setting exports via "module" is in play,
                //favor that over return value and exports. After that,
                //favor a non-undefined return value over exports use.
                if (cjsModule && cjsModule.exports !== undef &&
                        cjsModule.exports !== defined[name]) {
                    defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                    //Use the return value from the function.
                    defined[name] = ret;
                }
            }
        } else if (name) {
            //May just be an object definition for the module. Only
            //worry about defining if have a module name.
            defined[name] = callback;
        }
    };

    requirejs = require = req = function (deps, callback, relName, forceSync, alt) {
        if (typeof deps === "string") {
            if (handlers[deps]) {
                //callback in this case is really relName
                return handlers[deps](callback);
            }
            //Just return the module wanted. In this scenario, the
            //deps arg is the module name, and second arg (if passed)
            //is just the relName.
            //Normalize module name, if it contains . or ..
            return callDep(makeMap(deps, makeRelParts(callback)).f);
        } else if (!deps.splice) {
            //deps is a config object, not an array.
            config = deps;
            if (config.deps) {
                req(config.deps, config.callback);
            }
            if (!callback) {
                return;
            }

            if (callback.splice) {
                //callback is an array, which means it is a dependency list.
                //Adjust args if there are dependencies
                deps = callback;
                callback = relName;
                relName = null;
            } else {
                deps = undef;
            }
        }

        //Support require(['a'])
        callback = callback || function () {};

        //If relName is a function, it is an errback handler,
        //so remove it.
        if (typeof relName === 'function') {
            relName = forceSync;
            forceSync = alt;
        }

        //Simulate async callback;
        if (forceSync) {
            main(undef, deps, callback, relName);
        } else {
            //Using a non-zero value because of concern for what old browsers
            //do, and latest browsers "upgrade" to 4 if lower value is used:
            //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:
            //If want a value immediately, use require('id') instead -- something
            //that works in almond on the global level, but not guaranteed and
            //unlikely to work in other AMD implementations.
            setTimeout(function () {
                main(undef, deps, callback, relName);
            }, 4);
        }

        return req;
    };

    /**
     * Just drops the config on the floor, but returns req in case
     * the config return value is used.
     */
    req.config = function (cfg) {
        return req(cfg);
    };

    /**
     * Expose module registry for debugging and tooling
     */
    requirejs._defined = defined;

    define = function (name, deps, callback) {
        if (typeof name !== 'string') {
            throw new Error('See almond README: incorrect module build, no module name');
        }

        //This module may not have dependencies
        if (!deps.splice) {
            //deps is not an array, so probably means
            //an object literal or factory function for
            //the value. Adjust args.
            callback = deps;
            deps = [];
        }

        if (!hasProp(defined, name) && !hasProp(waiting, name)) {
            waiting[name] = [name, deps, callback];
        }
    };

    define.amd = {
        jQuery: true
    };
}());

define("wrap/almond", function(){});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

define('mvc/ajax',['require','exports','module','twc-universal-sdk','underscore','jquery'],function(require, exports, module) {
  var $, Ajax, TWC, _, ref;
  TWC = require("twc-universal-sdk");
  _ = require("underscore");
  $ = require("jquery");
  if ((ref = $.support) != null) {
    ref.cors = true;
  }
  Ajax = (function(superClass) {
    extend(Ajax, superClass);

    function Ajax() {
      this.finish = bind(this.finish, this);
      this.success = bind(this.success, this);
      this.retrieve = bind(this.retrieve, this);
      this.urls = {};
      this.data = {};
      this.onComplete = null;
    }

    Ajax.prototype.retrieve = function(urls, onComplete1) {
      var params, ref1, results, type;
      this.urls = urls;
      this.onComplete = onComplete1;
      if (_.keys(this.urls).length < 1) {
        return typeof onComplete === "function" ? onComplete() : void 0;
      }
      ref1 = this.urls;
      results = [];
      for (type in ref1) {
        params = ref1[type];
        results.push((function(_this) {
          return function(type, params) {
            var config, parameters;
            parameters = _.clone(params);
            config = TWC.SDK.Config[TWC.SDK.Config.platform];
            if (((config != null ? config.http_prefix : void 0) != null) && parameters.url.startsWith("http") && parameters.type !== "jsonp") {
              parameters.url = config.http_prefix + parameters.url;
            }
            if (((config != null ? config.http_postfix : void 0) != null) && parameters.url.startsWith("http") && parameters.type !== "jsonp") {
              parameters.url += config.http_postfix;
            }
            parameters.url = parameters.url.replace("skin://", "skin/" + TWC.SDK.Config.resolution[1] + "/");
            if (!parameters.error) {
              parameters.error = function(err) {
                if (parameters.always_success) {
                  return _this.success(type, {
                    "error": true
                  });
                } else {
                  $(TWC.SDK.Base.eventbus).trigger("error", [
                    {
                      "msg": err.statusText,
                      "type": "http",
                      "status": err.status,
                      "url": parameters.url
                    }
                  ]);
                  return false;
                }
              };
            }
            parameters.success = function(result) {
              return _this.success(type, result);
            };
            return _this.ajax(parameters);
          };
        })(this)(type, params));
      }
      return results;
    };

    Ajax.prototype.success = function(type, result) {
      this.data[type] = result;
      return this.finish();
    };

    Ajax.prototype.finish = function() {
      if (_.keys(this.urls).length === _.keys(this.data).length) {
        if (typeof this.onComplete === "function") {
          return this.onComplete(this.data);
        }
      }
    };

    Ajax.prototype.ajax = function(param) {
      var err, error;
      log("HTTP: " + param.url);
      try {
        return $.ajax(param);
      } catch (error) {
        err = error;
        log(err);
        $(TWC.SDK.Base.eventbus).trigger("error", err);
        return false;
      }
    };

    return Ajax;

  })(TWC.SDK.Base);
  return module.exports = Ajax;
});

define('mvc/router',['require','exports','module','twc-universal-sdk','underscore','jquery'],function(require, exports, module) {
  var $, Router, TWC, _;
  TWC = require("twc-universal-sdk");
  _ = require("underscore");
  $ = require("jquery");
  Router = (function() {
    function Router() {}

    Router.views = {};

    Router.active = void 0;

    Router.activePage = void 0;

    Router.popup = void 0;

    Router.blockMouseEvents = false;

    Router.init = function() {
      this.keyboardEvents();
      this.mouseEvents();
      return this.touchEvents();
    };

    Router.keyboardEvents = function() {
      var keyboardkeys;
      keyboardkeys = _.uniq(_.values(TWC.SDK.Input.get().getKeys()));
      return $(TWC.SDK.Base.eventbus).on(keyboardkeys.join(" "), (function(_this) {
        return function(event, src) {
          var active, key;
          key = event.type;
          active = _this.getFocus();
          if (!active) {
            return false;
          }
          if (key === "left" || key === "right" || key === "up" || key === "down") {
            _this.navigate(key);
          }
          if (key === "enter") {
            _this.setSelect(Router.getFocus());
          }
          return $(active).trigger(key);
        };
      })(this));
    };

    Router.mouseEvents = function() {
      return $(TWC.SDK.Base.eventbus).on("mouseover mouseout click", function(event, src) {
        var active, nodeid, parentid, type;
        if (Router.blockMouseEvents) {
          return;
        }
        type = event.type;
        nodeid = Router.findFocusable(src.target);
        if (!nodeid) {
          return;
        }
        switch (type) {
          case "mouseover":
            parentid = Router.findParent(nodeid);
            if (!parentid) {
              return;
            }
            Router.setActive(parentid);
            return Router.setFocus(nodeid);
          case "mouseout":
            return Router.unFocus(nodeid);
          case "click":
            active = Router.getFocus();
            if (nodeid !== active) {
              return;
            }
            Router.setSelect(nodeid);
            log("INPUT: received (enter)");
            return $(active).trigger('enter');
        }
      });
    };

    Router.touchEvents = function() {
      $(TWC.SDK.Base.eventbus).on("touchstart", function(event, target) {
        var focusel, id, parentid;
        Router.blockMouseEvents = false;
        Router.tempBlock = setTimeout(function() {
          Router.blockMouseEvents = true;
          return Router.blockMouseEventsTimeout = setTimeout(function() {
            Router.blockMouseEvents = false;
            return clearTimeout(Router.blockMouseEventsTimeout);
          }, 1500);
        }, 150);
        focusel = (target != null ? target[0] : void 0) != null ? target[0] : target;
        id = Router.findFocusable(focusel);
        if (!id) {
          return;
        }
        log("INPUT: received (touchstart) - " + id);
        parentid = Router.findParent(id);
        if (!parentid) {
          return;
        }
        Router.setActive(parentid);
        return Router.setFocus(id);
      });
      $(TWC.SDK.Base.eventbus).on("blur", function(event, target) {
        clearTimeout(this.blockMouseEventsTimeout);
        return Router.unFocus();
      });
      $(TWC.SDK.Base.eventbus).on("tap", function(event, target) {
        var active, focusel, id, parentid;
        Router.blockMouseEvents = false;
        clearTimeout(Router.blockMouseEventsTimeout);
        focusel = (target != null ? target[0] : void 0) != null ? target[0] : target;
        id = Router.findFocusable(focusel);
        active = Router.getActive();
        if (!id) {
          return;
        }
        parentid = Router.findParent(id);
        if (!parentid) {
          return;
        }
        $(focusel).trigger("enter");
        return setTimeout(function() {
          if (active) {
            return active.unfocus();
          }
        }, 500);
      });
      return $(TWC.SDK.Base.eventbus).on("swipe swipeRight swipeLeft swipeUp swipeDown", function(event, target) {
        var type;
        Router.blockMouseEvents = false;
        clearTimeout(Router.blockMouseEventsTimeout);
        type = event.type;
        return $(target).trigger(type);
      });
    };

    Router.find = function(uid) {
      var v;
      v = this.views[uid];
      if (!v) {
        return false;
      }
      return v;
    };

    Router.remove = function(uid) {
      var view;
      view = this.views[uid];
      if (view) {
        view.unload();
        return delete this.views[uid];
      }
    };

    Router.add = function(view) {
      return this.views[view.uid] = view;
    };

    Router.load = function(uid, parameters) {
      var newView, oldView;
      newView = this.find(uid);
      if (!newView) {
        return false;
      }
      oldView = this.getActivePage();
      if (newView.type === "page" && oldView && (oldView.uid != null)) {
        log("ROUTER: [" + oldView.type + "] unload '" + oldView.uid + "'");
        return this.unload(oldView.uid, function() {
          log("ROUTER: [" + newView.type + "] load '" + newView.uid + "'");
          return newView.load(parameters);
        });
      } else {
        log("ROUTER: load [" + newView.type + "] '" + newView.uid + "'");
        return newView.load(parameters);
      }
    };

    Router.unload = function(uid, callback) {
      var view;
      view = this.find(uid);
      if (!view) {
        return typeof callback === "function" ? callback() : void 0;
      }
      return view.unload(callback);
    };

    Router.getFocus = function() {
      var v;
      v = this.getActive();
      if (v) {
        if (v.activeFocus) {
          return v.activeFocus;
        } else if (v.lastFocused) {
          return v.lastFocused;
        } else {
          return v.defaultFocus;
        }
      }
    };

    Router.getSelect = function() {
      var v;
      v = this.getActive();
      if (v && v.activeSelect) {
        return v.activeSelect;
      }
    };

    Router.setFocus = function(id) {
      var v;
      v = this.getActive();
      if (v && id && this.canFocus(id)) {
        v.focus(id);
        return true;
      } else {
        return false;
      }
    };

    Router.setSelect = function(id) {
      var v;
      if (id == null) {
        id = this.getFocus();
      }
      if (_.isElement(id[0] || id)) {
        id = "#" + id[0].id;
      }
      v = this.getActive();
      if (v && id && this.canFocus(id)) {
        v.select(id);
        return true;
      } else {
        return false;
      }
    };

    Router.unSelect = function() {
      var v;
      v = this.getActive();
      if (v) {
        return v.unselect();
      }
    };

    Router.unFocus = function() {
      var v;
      v = this.getActive();
      if (v) {
        return v.unfocus();
      }
    };

    Router.canFocus = function(id) {
      var down, el, focusable, left, right, up;
      el = $(id);
      if (el.length === 0) {
        return false;
      }
      up = el.attr("data-up");
      down = el.attr("data-down");
      left = el.attr("data-left");
      right = el.attr("data-right");
      focusable = el.attr("data-focusable");
      el = null;
      if (up || down || left || right || focusable) {
        return true;
      } else {
        return false;
      }
    };

    Router.setActive = function(uid) {
      var ref, ref1;
      if (this.active !== uid && !this.popup) {
        if ((ref = this.getActive()) != null) {
          if (typeof ref.onDeactivate === "function") {
            ref.onDeactivate();
          }
        }
        this.unFocus();
        if (uid) {
          this.active = uid;
        }
        if (this.views[uid].type === "page") {
          this.activePage = uid;
        }
        if ((ref1 = this.getActive()) != null) {
          if (typeof ref1.onActivate === "function") {
            ref1.onActivate();
          }
        }
        return true;
      } else {
        return false;
      }
    };

    Router.getActive = function() {
      var active;
      active = this.popup ? this.popup : this.active;
      return this.views[active];
    };

    Router.getActivePage = function() {
      return this.views[this.activePage];
    };

    Router.navigate = function(key) {
      var el, err, id, method, view;
      id = this.getFocus();
      if (!id) {
        this.setFocus();
        log("can not fetch '" + id + "' fallback to default");
        return;
      }
      el = $(id);
      method = el.attr("data-" + key);
      if (method != null) {
        if (method.indexOf("#") !== -1) {
          this.setFocus(method);
        } else if (method === "defaultFocus") {
          this.defaultMove(key);
        } else if (method === "defaultSelect") {
          this.defaultMove(key, "select");
        } else {
          try {
            view = this.getActive();
            view[method]();
          } catch (error) {
            err = error;
            log("Error when trying to execute function '" + method + "' in view '" + view.uid + "'", err);
          }
        }
      }
      el = null;
      return true;
    };

    Router.defaultMove = function(key, type) {
      var el, id;
      if (type == null) {
        type = "focus";
      }
      id = Router.getFocus();
      el = $(id);
      while (true) {
        switch (key) {
          case "up":
            el = el.prev();
            break;
          case "right":
            el = el.next();
            break;
          case "down":
            el = el.next();
            break;
          case "left":
            el = el.prev();
        }
        if (!el[0] || (!(el[0].offsetWidth === 0 && el[0].offsetHeight === 0) && this.canFocus(el[0]))) {
          break;
        }
      }
      if (el[0]) {
        if (type === "select") {
          this.setSelect("#" + el[0].id);
        } else {
          this.setFocus("#" + el[0].id);
        }
      }
      el = null;
      return true;
    };

    Router.findFocusable = function(tg) {
      while (tg) {
        if (this.canFocus("#" + tg.id)) {
          return "#" + tg.id;
        }
        tg = tg.parentNode;
      }
    };

    Router.findParent = function(id) {
      var err, i, k, len, list, node, ref, ref1, v, views;
      views = {};
      ref = this.views;
      for (k in ref) {
        v = ref[k];
        if (v.isActive) {
          views[v.el] = v.uid;
        }
      }
      try {
        list = _.keys(views);
        ref1 = $(id).parents("*");
        for (i = 0, len = ref1.length; i < len; i++) {
          node = ref1[i];
          if (node.id === "screen") {
            return this.getActivePage().uid;
          } else if (views["#" + node.id]) {
            return views["#" + node.id];
          }
        }
      } catch (error) {
        err = error;
        log(err);
        return false;
      }
    };

    return Router;

  })();
  return module.exports = Router;
});

define('mvc/history',['require','exports','module','./router','twc-universal-sdk','underscore','jquery'],function(require, exports, module) {
  var $, History, Router, TWC, _;
  Router = require("./router");
  TWC = require("twc-universal-sdk");
  _ = require("underscore");
  $ = require("jquery");
  History = (function() {
    function History() {}

    History.history = [];

    History.pushState = function(uid, suid) {
      if (!uid) {
        return;
      }
      log("HISTORY: [" + uid + "] push view");
      $(TWC.SDK.Base.eventbus).trigger("history_push", [uid]);
      return this.history.push({
        'uid': uid,
        'suid': suid
      });
    };

    History.popState = function(callback) {
      var base, e, error, nw, old, ref, type;
      if (callback == null) {
        callback = null;
      }
      if (this.size() <= 1) {
        type = (ref = TWC.SDK.Config.platform) === "lg" || ref === "webos" || ref === "playstation" || ref === "tizen" || ref === "humax" ? "hard" : "soft";
        $('body').trigger("exit", [type]);
        return typeof callback === "function" ? callback() : void 0;
      }
      old = this.history.pop();
      nw = this.history[this.size() - 1];
      try {
        Router.unload(old.uid);
      } catch (error) {
        e = error;
        log(e);
      }
      if (typeof (base = Router.find(old.uid)).delHistoryData === "function") {
        base.delHistoryData(old.suid);
      }
      Router.setActive(nw.uid);
      log("HISTORY: [" + nw.uid + "] pop to view");
      $(TWC.SDK.Base.eventbus).trigger("history_pop", [nw.uid]);
      return Router.load(nw.uid, {
        session: nw.suid,
        pop: true,
        callback: callback
      });
    };

    History.reloadLast = function(callback) {
      var nw;
      if (callback == null) {
        callback = null;
      }
      nw = this.last();
      if (!nw) {
        return false;
      }
      Router.setActive(nw.uid);
      log("HISTORY: [" + nw.uid + "] reload view");
      $(TWC.SDK.Base.eventbus).trigger("history_reload", [nw.uid]);
      Router.load(nw.uid, {
        session: nw.suid,
        pop: true,
        callback: callback
      });
      return true;
    };

    History.popToState = function(removeFromStack) {
      if (removeFromStack == null) {
        removeFromStack = 0;
      }
      this.reset(this.history.length - removeFromStack);
      return this.popState();
    };

    History.reset = function(remainInStack) {
      if (remainInStack == null) {
        remainInStack = 0;
      }
      this.history = ((function(_this) {
        return function() {
          var base, current, history, i, len, ref, state;
          current = _this.history.pop();
          history = _this.history.slice(0, remainInStack);
          ref = _this.history.slice(remainInStack, +_this.history.length + 1 || 9e9);
          for (i = 0, len = ref.length; i < len; i++) {
            state = ref[i];
            if (typeof (base = Router.find(state.uid)).delHistoryData === "function") {
              base.delHistoryData(state.suid);
            }
          }
          history.push(current);
          return history;
        };
      })(this))();
      return $(TWC.SDK.Base.eventbus).trigger("history_reset");
    };

    History.size = function() {
      return this.history.length;
    };

    History.last = function() {
      return this.history[this.history.length - 1] || null;
    };

    History.addListner = function() {
      return $(document.body).on("return", (function(_this) {
        return function(e) {
          return _this.popState();
        };
      })(this));
    };

    History.removeListner = function() {
      return $(document.body).off("return");
    };

    return History;

  })();
  return module.exports = History;
});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty,
  slice = [].slice;

define('mvc/model',['require','exports','module','twc-universal-sdk','./ajax','underscore','jquery'],function(require, exports, module) {
  var $, Http, Model, TWC, _;
  TWC = require("twc-universal-sdk");
  Http = require("./ajax");
  _ = require("underscore");
  $ = require("jquery");
  Model = (function(superClass) {
    extend(Model, superClass);

    Model.prototype.uid = void 0;

    Model.prototype.state = 0;

    Model.prototype.external = void 0;

    Model.prototype.persistant = false;

    Model.prototype.data = {};

    Model.prototype.defaults = {};

    Model.prototype.timer = void 0;

    function Model(classProperties) {
      this.set = bind(this.set, this);
      var key, method;
      for (key in classProperties) {
        method = classProperties[key];
        this[key] = method;
      }
      if (this.uid == null) {
        this.uid = _.uniqueId('model-');
      }
      log("MODEL: [" + this.uid + "] init");
      if (typeof this.init === "function") {
        this.init();
      }
    }

    Model.prototype.retrieve = function(callback) {
      var http;
      log("MODEL: [" + this.uid + "] retrieve - " + (this.external ? 'external' : 'skip'));
      if (this.external) {
        this.empty();
        http = Http.init();
        return http.retrieve(this.external, (function(_this) {
          return function(resp) {
            var data;
            data = _this.parse(resp);
            _this.set(data);
            if (_this.persistant) {
              _this.setPersistant();
            }
            return typeof callback === "function" ? callback() : void 0;
          };
        })(this));
      } else {
        return typeof callback === "function" ? callback() : void 0;
      }
    };

    Model.prototype.parse = function(data) {
      return data;
    };

    Model.prototype.depend = function(callback) {
      log("MODEL: [" + this.uid + "] depends");
      if (_.isEmpty(this.data)) {
        return this.retrieve(function() {
          return typeof callback === "function" ? callback() : void 0;
        });
      } else {
        return typeof callback === "function" ? callback() : void 0;
      }
    };

    Model.prototype.fetch = Model.prototype.depend;

    Model.prototype.publish = function() {
      log("MODEL: [" + this.uid + "] publish");
      this.state++;
      return $(TWC.SDK.Base.eventbus).trigger(this.uid);
    };

    Model.prototype.get = function() {
      var args, i, len, param, ref, resultset;
      args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
      log("MODEL: [" + this.uid + "] get - " + args);
      if (args != null) {
        resultset = this.data;
        for (i = 0, len = args.length; i < len; i++) {
          param = args[i];
          resultset = (ref = resultset[param]) != null ? ref : {};
        }
        if (_.isEmpty(resultset) && !_.isNumber(resultset) && !_.isBoolean(resultset)) {
          return null;
        } else {
          return this.clone(resultset);
        }
      } else {
        return this.clone(this.data);
      }
    };

    Model.prototype.set = function(data) {
      log("MODEL: [" + this.uid + "] set");
      if (!_.isEmpty(this.defaults)) {
        data = _.defaults(data, this.defaults);
      }
      this.data = data;
      return this.publish();
    };

    Model.prototype.size = function() {
      var args, ref, ref1;
      args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
      if (args) {
        return ((ref = this.get.apply(this, args)) != null ? ref.length : void 0) || 0;
      } else {
        return (ref1 = this.data) != null ? ref1.length : void 0;
      }
    };

    Model.prototype.append = function(data) {
      this.data = _.extend(this.data, data);
      return this.publish();
    };

    Model.prototype.toJSON = function() {
      return _.clone(this.data);
    };

    Model.prototype.clone = function(data) {
      return JSON.parse(JSON.stringify(data));
    };

    Model.prototype.empty = function() {
      this.data = null;
      return this.data = {};
    };

    Model.prototype.destroy = function() {
      log("MODEL: [" + this.uid + "] destroy");
      this.empty();
      if (this.timer) {
        clearInterval(this.timer);
        return this.timer = void 0;
      }
    };

    Model.prototype.refresh = function(callback) {
      if (callback == null) {
        callback = null;
      }
      log("MODEL: [" + this.uid + "] refresh");
      return this.retrieve(callback);
    };

    Model.prototype.sync = function(bool, msec) {
      if (msec == null) {
        msec = 1000;
      }
      log(("MODEL: [" + this.uid + "] sync (") + bool + ")");
      if (bool) {
        return this.timer = setInterval(((function(_this) {
          return function() {
            return _this.refresh();
          };
        })(this)), msec);
      } else {
        if (this.timer) {
          clearInterval(this.timer);
          return this.timer = null;
        }
      }
    };

    return Model;

  })(TWC.SDK.Base);
  return module.exports = Model;
});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty,
  slice = [].slice;

define('mvc/view',['require','exports','module','twc-universal-sdk','./router','./history','./ajax','underscore','jquery'],function(require, exports, module) {
  var $, History, Http, Router, TWC, View, _;
  TWC = require("twc-universal-sdk");
  Router = require("./router");
  History = require("./history");
  Http = require("./ajax");
  _ = require("underscore");
  $ = require("jquery");
  View = (function(superClass) {
    extend(View, superClass);

    View.prototype.uid = void 0;

    View.prototype.suid = void 0;

    View.prototype.sdata = {};

    View.prototype.activeFocus = null;

    View.prototype.lastFocused = null;

    View.prototype.lastSelected = null;

    View.prototype.eventSplitter = /^(\S+)\s*(.*)$/;

    View.prototype.type = "page";

    View.prototype.html = void 0;

    View.prototype.css = void 0;

    View.prototype.zones = [];

    View.prototype.external = null;

    View.prototype.model = {};

    View.prototype.event = {};

    View.prototype.history = true;

    View.prototype.el = "";

    View.prototype.animation = {};

    View.prototype.defaultFocus = "";

    View.prototype.loading = true;

    View.prototype.isHidden = false;

    View.prototype.isActive = false;

    View.prototype.unloadDelay = 0;

    View.prototype.children = [];

    View.prototype.parent = null;

    View.prototype.manifest = {};

    function View(classProperties) {
      this.disableEvents = bind(this.disableEvents, this);
      this.refresh = bind(this.refresh, this);
      var key, method;
      for (key in classProperties) {
        method = classProperties[key];
        this[key] = method;
      }
      if (this.uid == null) {
        this.uid = _.uniqueId('view-');
      }
      log("VIEW: [" + this.uid + "] init");
      Router.add(this);
      if (typeof this.init === "function") {
        this.init();
      }
    }

    View.prototype.load = function(parameters) {
      if (parameters == null) {
        parameters = {};
      }
      log("VIEW: [" + this.uid + "] load");
      parameters = _.defaults(parameters, {
        session: null,
        pop: false,
        data: null,
        callback: null
      });
      this.suid = !parameters.session ? _.uniqueId('session-') : parameters.session;
      if (!this.sdata[this.suid]) {
        this.sdata[this.suid] = [];
      }
      if (parameters.data) {
        this.sdata[this.suid] = parameters.data;
      }
      if (this.type === "popup") {
        $(TWC.SDK.Base.eventbus).trigger("popup", true);
      }
      this.isActive = true;
      if (this.type === "page" && this.history && !parameters.pop) {
        History.pushState(this.uid, this.suid);
      }
      if (typeof this.onload === "function") {
        this.onload.apply(this, this.sdata[this.suid]);
      }
      return this.retrieve((function(_this) {
        return function() {
          return _this.refresh(function() {
            return typeof parameters.callback === "function" ? parameters.callback() : void 0;
          });
        };
      })(this));
    };

    View.prototype.refresh = function(callback) {
      log("VIEW: [" + this.uid + "] refresh");
      this.loading && this.type !== "popup" && $(TWC.SDK.Base.eventbus).trigger('loading', [true, this.type]);
      this.activeFocus = null;
      this.lastFocused = null;
      this.lastSelected = null;
      return this.model.refresh((function(_this) {
        return function() {
          return _this.render(function() {
            if (typeof callback === "function") {
              callback();
            }
            return _this.loading && _this.type !== "popup" && $(TWC.SDK.Base.eventbus).trigger('loading', [false, _this.type]);
          });
        };
      })(this));
    };

    View.prototype.refreshZones = function(callback) {
      if (callback == null) {
        callback = null;
      }
      return this.unloadZones((function(_this) {
        return function() {
          return _this.loadZones(function() {
            if (typeof _this.onZoneRenderComplete === "function") {
              _this.onZoneRenderComplete();
            }
            return typeof callback === "function" ? callback() : void 0;
          });
        };
      })(this));
    };

    View.prototype.refreshZone = function(index, callback) {
      if (callback == null) {
        callback = null;
      }
      return this.unloadZone(index, (function(_this) {
        return function() {
          return _this.loadZone(index, function() {
            return typeof callback === "function" ? callback() : void 0;
          });
        };
      })(this));
    };

    View.prototype.loadZones = function(callback) {
      var checkComplete, counter, size;
      if (callback == null) {
        callback = null;
      }
      if (this.zones.length < 1) {
        return typeof callback === "function" ? callback() : void 0;
      }
      log("VIEW: [" + this.uid + "] loadZones");
      size = this.zones.length;
      counter = 0;
      checkComplete = (function(_this) {
        return function() {
          log("VIEW: [" + _this.uid + ("] loadZones " + counter + ":" + size));
          if (counter === size) {
            return $(document).ready(function() {
              return typeof callback === "function" ? callback() : void 0;
            });
          }
        };
      })(this);
      this.children = [];
      return _.each(this.zones, (function(_this) {
        return function(zone, index) {
          return _this.loadZone(index, function() {
            counter++;
            return checkComplete();
          });
        };
      })(this));
    };

    View.prototype.unloadZones = function(callback) {
      var checkComplete, counter, size;
      if (callback == null) {
        callback = null;
      }
      log("VIEW: [" + this.uid + "] unloadZones");
      size = this.zones.length;
      if (size === 0) {
        return typeof callback === "function" ? callback() : void 0;
      }
      counter = 0;
      checkComplete = (function(_this) {
        return function() {
          log("VIEW: [" + _this.uid + ("] unloadZones " + counter + ":" + size));
          if (counter === size) {
            _this.zones.reverse();
            return typeof callback === "function" ? callback() : void 0;
          }
        };
      })(this);
      this.zones.reverse();
      _.each(this.zones, (function(_this) {
        return function(zone, index) {
          return _this.unloadZone(index, function() {
            counter++;
            return checkComplete();
          });
        };
      })(this));
      return this.children = [];
    };

    View.prototype.loadZone = function(i, callback) {
      var z;
      if (callback == null) {
        callback = null;
      }
      z = Router.find(this.zones[i].uid);
      if (!z) {
        if (typeof callback === "function") {
          callback();
        }
      }
      z.parent = this;
      return z.load({
        data: this.zones[i].data || [],
        callback: (function(_this) {
          return function() {
            _this.children[i] = z;
            return typeof callback === "function" ? callback() : void 0;
          };
        })(this)
      });
    };

    View.prototype.unloadZone = function(i, callback) {
      var z;
      if (callback == null) {
        callback = null;
      }
      z = Router.find(this.zones[i].uid);
      if (z && z.isActive) {
        z.parent = null;
        return z.unload((function(_this) {
          return function() {
            _this.children[i] = null;
            return typeof callback === "function" ? callback() : void 0;
          };
        })(this));
      } else {
        return typeof callback === "function" ? callback() : void 0;
      }
    };

    View.prototype.hide = function() {
      this.isHidden = true;
      this.unfocus();
      this.unselect();
      return $(this.el).hide();
    };

    View.prototype.show = function() {
      this.isHidden = false;
      return $(this.el).show();
    };

    View.prototype.unload = function(callback, clearHTML) {
      var err, error;
      if (callback == null) {
        callback = null;
      }
      if (clearHTML == null) {
        clearHTML = false;
      }
      log("VIEW: [" + this.uid + "] unload");
      this.isActive = false;
      this.disableEvents();
      try {
        if (typeof this.onunload === "function") {
          this.onunload();
        }
      } catch (error) {
        err = error;
        log("VIEW: [" + this.uid + "] Error onunload ", err);
      }
      if (this.type === "popup") {
        Router.popup = null;
      }
      return this.unloadZones((function(_this) {
        return function() {
          var error1;
          if (clearHTML) {
            $(_this.el).empty();
          }
          try {
            if (typeof _this.onunloadComplete === "function") {
              _this.onunloadComplete();
            }
          } catch (error1) {
            err = error1;
            log("VIEW: [" + _this.uid + "] Error onunloaded ", err);
          }
          if (_this.type === "popup") {
            if ((_this.unloadDelay != null) && _this.unloadDelay > 0) {
              setTimeout(function() {
                return $("#popup").hide();
              }, _this.unloadDelay);
            } else {
              $("#popup").hide();
            }
            $(TWC.SDK.Base.eventbus).trigger("popup", false);
          }
          if ((_this.unloadDelay != null) && _this.unloadDelay > 0) {
            if (callback != null) {
              return setTimeout(callback, _this.unloadDelay);
            }
          } else {
            return typeof callback === "function" ? callback() : void 0;
          }
        };
      })(this));
    };

    View.prototype.focus = function(id) {
      if (id == null) {
        id = this.defaultFocus;
      }
      this.unfocus();
      $(id).addClass("focus");
      return this.activeFocus = id;
    };

    View.prototype.unfocus = function() {
      if (!this.activeFocus) {
        return false;
      }
      $(this.activeFocus).removeClass("focus");
      this.lastFocused = _.clone(this.activeFocus);
      return this.activeFocus = null;
    };

    View.prototype.select = function(id) {
      var el;
      if (id == null) {
        id = this.defaultFocus;
      }
      el = $(id);
      if (el.hasClass("select")) {
        if (!el.hasClass("focus")) {
          return this.focus(id);
        }
      } else {
        this.unselect();
        el.addClass("select");
        this.lastSelected = id;
        this.focus(id);
        log("VIEW: trigger (selected)");
        return $(this.activeFocus).trigger("selected");
      }
    };

    View.prototype.unselect = function() {
      if (!this.lastSelected) {
        return false;
      }
      $(this.lastSelected).removeClass("select");
      this.lastSelected = null;
      return this.unfocus();
    };

    View.prototype.clone = function() {
      return _.clone(this);
    };

    View.prototype.retrieve = function(callback) {
      var http;
      log("VIEW: [" + this.uid + "] retrieve");
      if (this.external) {
        http = Http.init();
        return http.retrieve(this.external, (function(_this) {
          return function(resp) {
            var key, method;
            for (key in resp) {
              method = resp[key];
              _this[key] = method;
            }
            return typeof callback === "function" ? callback() : void 0;
          };
        })(this));
      } else {
        return typeof callback === "function" ? callback() : void 0;
      }
    };

    View.prototype.enableEvents = function() {
      var el, eventName, key, match, method, ref, selector;
      el = $(this.el);
      ref = this.events;
      for (key in ref) {
        method = ref[key];
        if (typeof method !== 'function') {
          method = this.proxy(this[method]);
        }
        match = key.match(this.eventSplitter);
        eventName = match[1];
        selector = match[2];
        if (selector === "") {
          el.on(eventName, method);
        } else {
          el.delegate(selector, eventName, method);
        }
      }
      return el = null;
    };

    View.prototype.disableEvents = function() {
      return $(this.el).off();
    };

    View.prototype.onRenderStart = function() {};

    View.prototype.onRenderComplete = function() {};

    View.prototype.template = function(html, modelData) {
      var data;
      modelData = _.isArray(modelData) ? {
        data: modelData
      } : modelData;
      data = _.extend({}, {
        manifest: this.manifest
      }, modelData);
      return _.template(html)(data);
    };

    View.prototype.render = function(callback) {
      log("VIEW: [" + this.uid + "] render (start)");
      return this.unloadZones((function(_this) {
        return function() {
          var base, content, data;
          data = typeof (base = _this.model).toJSON === "function" ? base.toJSON() : void 0;
          if (!_this.el) {
            throw new Error("VIEW: [" + _this.uid + "] empty @el property");
          }
          if (!_this.html) {
            throw new Error("VIEW: [" + _this.uid + "] empty @html property");
          }
          content = "";
          if (_this.css) {
            content += "<style type='text/css'>" + _this.css + "</style>";
          }
          content += _this.template(_this.html, data);
          if (TWC.SDK.Config.platform === "philips" || TWC.SDK.Config.platform === "hbbtv") {
            content = content.replace(/&nbsp;/g, "&#160;");
          }
          _this.disableEvents();
          _this.onRenderStart();
          $(_this.el).html(content);
          return $(document).ready(function() {
            var ref;
            _this.enableEvents();
            if (_this.type === "popup") {
              Router.popup = _this.uid;
              $("#popup").show();
            }
            if (((ref = _this.type) === "page" || ref === "popup") || (_this.defaultFocus && _this.type === "zone")) {
              Router.setActive(_this.uid);
              if (_this.defaultFocus) {
                Router.setSelect(Router.getFocus(), true);
              }
            }
            _this.onRenderComplete();
            return _this.loadZones(function() {
              if (typeof _this.onZoneRenderComplete === "function") {
                _this.onZoneRenderComplete();
              }
              if (typeof callback === "function") {
                callback();
              }
              return log("VIEW: [" + _this.uid + "] render (done)");
            });
          });
        };
      })(this));
    };

    View.prototype.deleteHistory = function() {
      return delete this.sdata[this.suid];
    };

    View.prototype.updateHistory = function() {
      var args, i, j, len, value;
      args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
      for (i = j = 0, len = args.length; j < len; i = ++j) {
        value = args[i];
        if (value != null) {
          this.sdata[this.suid][i] = value;
        }
      }
      return true;
    };

    return View;

  })(TWC.SDK.Base);
  return module.exports = View;
});

define('mvc/index',['require','exports','module','./ajax','./router','./history','./model','./view'],function(require, exports, module) {
  var MVC;
  MVC = {};
  MVC.Ajax = require("./ajax");
  MVC.Router = require("./router");
  MVC.History = require("./history");
  MVC.Model = require("./model");
  MVC.View = require("./view");
  return module.exports = MVC;
});

define('mvc',['require','exports','module','twc-universal-sdk','./mvc/index'],function(require, exports, module){
	TWC = require("twc-universal-sdk");
	// append MVC to TWC universal framework
	TWC.MVC = require("./mvc/index");
	module.exports = TWC;
});

  define('jquery', function(){ return $; });
  define('underscore', function(){ return _; });
  define('twc-universal-sdk', function(){ return TWC; });
  return require('mvc');
}));
