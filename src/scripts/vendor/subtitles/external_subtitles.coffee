define (require, exports, module) ->
  TWC = require("framework")
  _ = require("underscore")
  $ = require("jquery")


  module.exports = {
    # sets the external captions
    #
    # @property [Array] list of subtitle objects using the format {language:"", url:""}
    _textTracks: []

    # sets the external captions
    #
    # @property [Array] list of active subtitle text data using the format {text:"", id:"", start:"", end:"", target:""}
    _textData: []

    # @property [Bool] boolean that determines if text tracks are active
    _textActive: false

    # sets the external captions
    #
    # @param [Array] _textTracks list of subtitle objects using the format {language:"", url:""}
    setExternalSubtitles: (@_textTracks) ->
      for subtitle, index in @_textTracks
        if not subtitle.index? then subtitle.index = index
      return

    # load the subtitle data an d start broadcasting subtitle events
    #
    initSubtitleEvents: ->
      #temporary store visible subtitles
      visibleSubtitles = []
      @stopSubtitleEvents()

      $(TWC.SDK.Base.eventbus).on "player.onProgress._player._subtitle", (e, seconds) =>
        return if !@_textActive
        #clean old visible subtitles
        for visibleSubtitle, index in visibleSubtitles
          if visibleSubtitle and parseFloat(seconds) >= visibleSubtitle.end
            $(TWC.SDK.Base.eventbus).trigger("player.subtitleOff", [visibleSubtitle])
            visibleSubtitles.shift()

        #check for new visible subtitle
        new_subtitle = _.find @_textData, (subtitle) ->
          return (subtitle.start <= seconds <= subtitle.end) and (subtitle not in visibleSubtitles)

        if new_subtitle
          visibleSubtitles.push new_subtitle
          $(TWC.SDK.Base.eventbus).trigger "player.subtitleOn", [new_subtitle]

    # stop broadcasting subtitle events
    #
    stopSubtitleEvents: ->
      $(TWC.SDK.Base.eventbus).off("._subtitle")
      $(TWC.SDK.Base.eventbus).trigger("player.subtitleOff")

    # disable/enable subtitle
    #
    # @param [Bool] val enable or disable the subtitles
    subtitleActive: (active) ->
      storage = TWC.SDK.Storage.get()
      #when subtitle should be activated
      if active? and active is true
        #check if there are subtitles available
        subtitleTracks = @allSubtitleTracks()
        return false if not subtitleTracks and subtitleTracks.length is 0

        #get default subtitle language
        lastTrackLanguage = storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_LANGUAGE)

        track = subtitleTracks[0]
        for subtitleTrack in subtitleTracks
          if lastTrackLanguage is subtitleTrack.language
            track = subtitleTrack
            break

        #set subtitle active
        storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_ACTIVE, JSON.stringify(true))
        @subtitleTrack(track)
        @_textActive = true

        #when subtitle should be deactivated
      else if active? and active is false
        @_textActive = false
        @stopSubtitleEvents()
        storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_ACTIVE, JSON.stringify(false))

      state = JSON.parse(storage.getItem(TWC.SDK.Player.STORAGE_SUBTITLE_ACTIVE)) or false

      #return current state
      return state

    # set/get subtitle track
    #
    # @param [Object<{language:String, index:Integer}>] subtitle track
    # @return [Object<{language:String, index:Integer}>] current subtitle track
    subtitleTrack: (track, forced = false) ->
      return if not @_textTracks? or not (@_textTracks and @_textTracks.length > 0)
      if !(typeof track == 'undefined') and (track != null)
        log "EXTERNAL PLAYER: subtitleTrack:'#{track}'"
        #save last used subtitle language as default, do not store if its a forced text track
        storage = TWC.SDK.Storage.get()
        storage.setItem(TWC.SDK.Player.STORAGE_SUBTITLE_LANGUAGE, track.language) if not forced

        #first stop current subtitles if applicable
        @_textData = []
        @stopSubtitleEvents()

        for oldTextTrack in @_textTracks
          if oldTextTrack.enabled is true
            oldTextTrack.enabled = false


        if not forced
          #try to load new subtitle
          textTrack = _.findWhere(@_textTracks, {index: track.index})
          # else try to load new subtitle based on language
          textTrack = if not textTrack then _.findWhere(@_textTracks, {language: track.language})
          textTrack = if not textTrack then track
        else
          #if its forced the language url and tag should be already present
          textTrack = track
          @_textActive = true


        if textTrack and textTrack.url
          $.ajax
            type: "GET",
            url: textTrack.url,
          #dataType:"xml", //enable if CDN does not provide the correct content-type header
            success: (data)=>
              @_textData = @parseSubtitle(data)
              @initSubtitleEvents()
              log "EXTERNAL PLAYER: external subtitleTrack loaded: ", textTrack.url
              textTrack.enabled = true

      else
        for textTrack in @_textTracks when textTrack.enabled
          return textTrack

    # get all subtitle tracks
    #
    # @return [Array] array of all available subtitle tracks
    allSubtitleTracks: ->
      result = []
      return result if not @_textTracks? or not (@_textTracks and @_textTracks.length > 0)
      return @_textTracks

    # global method to parse timestring from ISO 8601 format (hh:mm:ss[.mmm]) to seconds
    #
    # @param [String] timeStr ISO 8601 format (hh:mm:ss[.mmm])
    # @param [Integer] framerate
    # @return [Integer] seconds
    toSeconds: (timeStr, framerate) ->

      # Hours and minutes are optional
      # Seconds must be specified
      # Seconds can be followed by milliseconds OR by the frame information
      validTimeFormat = /^([0-9]+:){0,2}[0-9]+([.;:][0-9]+)?$/
      errorMessage = 'Invalid time format'
      digitPairs = undefined
      lastIndex = undefined
      lastPair = undefined
      firstPair = undefined
      frameInfo = undefined
      frameTime = undefined
      if typeof timeStr == 'number'
        return timeStr
      if typeof timeStr == 'string' and !validTimeFormat.test(timeStr)
        throw "invalid time format"
      digitPairs = timeStr.split(':')

      # Fix last element ;
      lastIndex = digitPairs.length - 1
      lastPair = digitPairs[lastIndex]
      if lastPair.indexOf(';') > -1
        frameInfo = lastPair.split(';')
        frameTime = 0
        if framerate and typeof framerate == 'number'
          frameTime = parseFloat(frameInfo[1], 10) / framerate
        digitPairs[lastIndex] = parseInt(frameInfo[0], 10) + frameTime

      # Fix last element :
      if lastIndex is 3
        numeral = (parseInt(digitPairs[3]) / 60).toFixed(2) #convert to milli seconds
        digitPairs[2] = parseFloat(digitPairs[2]) + parseFloat(numeral)
        delete digitPairs[3]
        digitPairs.pop()

      firstPair = digitPairs[0]
      {
      1: parseFloat(firstPair, 10)
      2: parseInt(firstPair, 10) * 60 + parseFloat(digitPairs[1], 10)
      3: parseInt(firstPair, 10) * 3600 + parseInt(digitPairs[1], 10) * 60 + parseFloat(digitPairs[2], 10)
      }[digitPairs.length or 1]
  }
