define [
  "framework",
  "underscore",
  "backbone"
], (TWC, _, Backbone)->

  # Wrapper for the Backbone ajax call
  #
  # @param [Object] param
  Backbone.ajax = (parameters) ->
    log "HTTP: #{parameters.url}"
    Config = TWC.SDK.Config

    # Add prefix
    parameters.url = Config[Config.platform].http_prefix+parameters.url if Config[Config.platform]?.http_prefix? and parameters.url.startsWith("http") and parameters.type isnt "jsonp"
    # Add postfix
    parameters.url += Config[Config.platform].http_postfix if Config[Config.platform]?.http_postfix? and parameters.url.startsWith("http") and parameters.type isnt "jsonp"
    # replace skin protocol with correct skin url
    parameters.url = parameters.url.replace("skin://", "skin/#{Config.resolution[1]}/")

    #set default timeout
    parameters.timeout = 10000 if not parameters.timeout

    #add error wrapper to trigger framework error
    if not parameters.not_trigger_error
      original_error = _.clone(parameters.error)
      parameters.error = (err) =>
        $(TWC.SDK.Base.eventbus).trigger( "error", [{"msg":err.statusText, "type":"http", "status":err.status, "url":parameters.url}] )
        original_error?(arguments)

    return Backbone.$.ajax.apply(Backbone.$, [parameters]);


  # Add refresh call to Backbone model
  Backbone.Model.prototype.refresh = (callback) ->
    model = @
    model.fetch({
      reset:true,
      success: ->
        callback?()
    })

  # Add refresh call to Backbone collection
  Backbone.Collection.prototype.refresh = (callback) ->
    collection = @
    collection.fetch({
      reset:true,
      success: ->
        callback?()
    })

  Backbone