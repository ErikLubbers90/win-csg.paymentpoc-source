define ({
    audio_language:"en",
    subtitle_active:false,
    subtitle_language:"en",
    languageCodeTable:{
        "de"  : "German",
        "deu" : "German",
        "ger" : "German",
        "en"  : "English",
        "eng" : "English",
        "fr"  : "French",
        "fra" : "French",
        "fre" : "French",
        "es"  : "Spanish",
        "spa" : "Spanish",
        "esp" : "Spanish"
    },
    ime_default_layout: "qwerty",
    default_payment_instruments_currency: "USD"
});