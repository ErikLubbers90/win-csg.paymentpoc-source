define({

    /*
     Main application configuration variables
     */
    "title": "Payment proof of concept",
    "version": "1.0.8",

    "build": "4",
    "platform": "windows10",
    "resolution": ["1920", "1080"],

    "debug": true,
    "on_screen_debug": false,
    "splash": {
        "minTime": 0,
        "maxTime": 500
    },

    // @property [String] options: development, staging, production
    // notice: during a build this value is overruled by the 'environment' value of the build configuration (package.json)
    "environment": "development",

    // @property [Object<EnvironmentObject>]  CD Environment configuration
    // EnvironmentObject {
    //  CDDomain: String                                        CD Domain
    //  SystemId: String                                        CD SystemId
    //  DistributionChannelId: String                           CD DistributionChannelId
    //  DistributionChannelId: Object(platform:String)          CD DistributionChannelId
    // }
    "csg": {
      "development": {
//        "CDDomain": "sbx1.cdops.net",
//        "SystemId": "af4f8d71-76ca-41e0-8278-cba844bffb72",
//        "DistributionChannelId": "8797f982-d549-4767-9e4f-e8306fb45993"

        // "CDDomain": "sbx1.cdops.net",
        // "SystemId": "0e2561ef-015e-4d09-90e2-6b6332492083",
        // "DistributionChannelId": "99b0d805-5e3d-4b64-b84a-b8e421404f61",

        "CDDomain": "ascendon.tv",
        "SystemId": "64c0bb7a-53ec-4130-a2ec-8dc8a024d98f",
        "DistributionChannelId": "C82B67A1-0597-45FC-BFE1-65F041590085",
        "RentalDeliveryCapabilityId": "30332",
        "NotOrderable": 877
////        "ExtrasEnvironment": "http://cdchicago.com/LAB/SonyPictures/sbx/v5.6.1/est/Extras2/scripts/package"
//        "ExtrasEnvironment": "http://sony.demo.cdops.net/sbx/est/Extras2/scripts/package"
      },
      "staging": {
        "CDDomain": "stg3.cdops.net",
        "SystemId": "",
        "DistributionChannelId": "",
        "RentalDeliveryCapabilityId": "30332"
      },
      "production": {
        "CDDomain": "prd1.contentdirect.tv",
        "SystemId": "10f01d27-92a4-4fca-8a4e-8f79d3c71ee0",
        "DistributionChannelId":  "c91f20e4-d62b-4560-933f-8597b4375f71",
        "RentalDeliveryCapabilityId": "30324",
        "NotOrderable": 437
      }
    },

    // @property [Array<String>] List of ISO 639-1 language codes support by the active CD Environment
    availableLanguages: ["en"],

    // @property [String] ISO 639-1 language code, the default fallback language if the customer-device uses a language that is not in the availableLanguages property
    defaultLanguage: "en",

    // @property [Array<String>] List of ISO 3166-1 country codes support by the active CD Environment
    availableCountries: ["US"],

    // @property [String] ISO 3166-1 country code, the default fallback country if the customer-device uses a country that is not in the availableCountries property
    defaultCountry:"US",

    // @property [CaptionsObject] Closed caption mapping on the CD Media response object (only for closed caption, not for subtitles)
    //
    // CaptionsObject {
    //   Container: String                         object container in media request
    //   ExternalTypeCode: Integer                 external type code to map to
    //   ExternalTypeFieldCode: Integer            external type field code to map to
    // }
    closedCaptionsMapping: {
        Container: "Markers",
        ExternalTypeCode: 4,
        ExternalTypeFieldCode: 21
    },

    // @property [Boolean] Enable/disable showing prices when unauthenticated
    enablePricesWhenUnauthenticated: false,

    // @property [Boolean] Enable/disable purchases option for products
    enablePurchase: false,

    // @property [Boolean] Enable/disable rental option for products
    enableRental: false,

    // @property [Boolean] Enable/disable upgrade option for products
    enableUpgrade: false,

    // @property [Boolean] Enable/disable purchases options for (episodic) bundles
    enableBundlePurchase: false,

    // @property [Boolean] Enable/disable payment options for the user
    enablePaymentOptions: false,

    // @property [Boolean] Enable/disable rental options for (episodic) bundles
    enableBundleRental: true,

    // @property [Boolean] Enable/disable Device PIN feature, a user is asked if he would like to use the payment PIN
    enablePaymentPin: false,

    // @property [Boolean] Enable/disable Device PIN enforcement, when enabled this will enforce the user to set a payment PIN
    enablePaymentPinEnforcement: false,

    // @property [Boolean] Enable/disable related content in product information page
    enableRelatedProductContent: true,

    // @property [Boolean] Enable/disable featured items (hero banners) on storefront pages
    enableFeaturedItems: true,

    // @property [Boolean] Enable/disable dynamic background image on login page (linked to main storefront backgroundImage)
    enableDynamicLoginBackground: true,

    // @property [Boolean] Enable/disable loyalty points feature
    enableLoyaltyPoints: true,

    // @property [Boolean] Enable/disable featured items (hero banners) on storefront pages
    useFavorites: false,

    // @property [Boolean] Enable/disable the use of the default payment instrument, when enabled a user is not presented with a option to choose the payment option instead the default instrument is used.
    useDefaultPaymentOption: false,


    // @property [Boolean] If narrative subtitles are available they will be activated if subtitles are disabled by the user
    enableForcedSubtitle: true,

    // @property [Array<Integer>] Enabled payment instruments in the payment flow
    //    0	Credit Card
    //    1	E-Check
    //    2	Stored Value Account
    //    3	Gift Card
    //    4	External Bill
    //    5	PayPal Account
    //    6	iTunes Account
    //    8	Direct Debit
    //    9	Xbox Account
    //    10 External Gift Card
    //    11 Roku Account
    supportedPaymentOptions: [0,3,5],

    // @property [Boolean] Enable promotion code in payment overview
    supportPromotionCode: true,

    // @property [Object] map CD currency code to currency symbol
    currencyTable: {
        "EUR":"€",
        "USD":"$",
        "CAD":"$"
    },

    // @property [Boolean] Enable Omniture tracker
    omniture_tracking: false,

    // @property [Boolean] Enable Akamai tracker
    akamai_enabled: false,

    // @property [String] Path of the Akamai Config xml
    akamai_xml_path: "http://ma1230-r.analytics.edgesuite.net/config/beacon-16155.xml?enableGenericAPI=1",
    // akamai_xml_path: "http://79423.analytics.edgesuite.net/analyticsplugin/configuration/SampleBeacon.xml?enableGenericAPI=1", //test beacon


    // @property [String] Path of account iframe
    iframe_account_url: "https://sony.demo.cdops.net/sbx/intel/",



    // @property [PlatformObject] TV Platform specific configuration
    //
    // PlatformObject {
    //   input: Array<String>                           app user input types
    //   deviceType: Integer                            CD Device type (registration)
    //   physicalDeviceCode: Integer                    CD Device code (registration)
    //   deliveryCapabilityCodes: Object(SD:String, HD:String) CD Device Action Code (playback)
    // }
    "core" : {
        "input": ["keyboard", "mouse", "touch"],
        "deviceType": 35,
        "physicalDeviceCode": 7,
        "streamActionCode":"20",
        "deliveryCapabilityCodes": {
            "HDR": "706",
            "UHD": "705",
            "HD": "700",
            "SD": "702"
        }
    },
    "windows10" : {
        "input": ["keyboard", "mouse", "touch"],
        "deviceType": 20054,
        "physicalDeviceCode": 10049,
        "streamActionCode":"20",
        "deliveryCapabilityCodes": {
            "HDR": "748",
            "UHD": "744",
            "HD": "30338",
            "SD": "30340"
        }
    }

});
