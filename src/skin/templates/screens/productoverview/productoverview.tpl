<div id="container" class="locker">
    <div id="backgroundOverlay"></div>
    <!--#header-->
    <div id="menu">
        <div id="subBar">

            <div id="subMenuItems">
                <div id="subMenuHeader">
                    <%- SubMenuTitle %>
                </div>
                <div class="divider"></div>
                <% for(var i=0;i<SubMenuItems.length;i++){ %>
                    <% var subMenuItem = SubMenuItems[i] %>
                    <% if(!SubMenuOptionalVisible && subMenuItem.Optional){ %>
                    <% }else{ %>
                        <div id="submenu_<%= i %>" class="menuButton submenu menu-sound"
                             data-left="moveToMainMenu"
                             data-right="moveToContent"
                            <% if(i > 0){ %>
                                data-up="defaultFocus"
                            <% } %>

                            <% if(i < SubMenuItems.length-1){ %>
                                data-down="defaultFocus"
                            <% } %>
                             data-id="<%= subMenuItem.Id %>"><span class="icon noAnimation"></span><%= subMenuItem.Name %></div>
                    <% } %>
                <% } %>
            </div>
        </div>
    </div>
    <!--#navigation arrows-->
    <div class="navigationContainer">
        <div id="navigationLeft" class="navigationButton menu-sound" data-right="defaultFocus">
            <div class="icon" />
        </div>
        <div id="navigationRight" class="navigationButton menu-sound" data-left="defaultFocus">
            <div class="icon" />
        </div>
    </div>
    <!--#content-->
    <div id="viewPort">
        <div id="content">
            <% var featuredPlaylistActive = false; %>
            <% var playlistStartIndex = (featuredPlaylistActive ? 1 : 0); %>
            <div class="playlistContent">
                <% productIdx = data.CurrentPage * data.totalProductsPerPage - 1 %>
                <% currentPage = data.CurrentPage - 1 %>
                <% if( data.Products.length > 0) { %>
                    <% var product = "" %>
                    <% if(currentPage > -1){ %>
                        <% for(var i=0;i<=currentPage;i++){ %>
                           <div id="playlist_page-<%-i%>" class="playlist-page"></div>
                        <% } %>
                    <% } %>
                    <% for(var j=0;j<Math.min(data.Products.length, data.totalProductsPerPage*2);j++) { %>
                        <% product = data.Products[j] %>
                        <% productIdx++ %>
                        <% if(j % data.totalProductsPerPage == 0){ %>
                            <% currentPage++ %>
                            <div id="playlist_page-<%- currentPage %>" class="playlist-page">
                        <% } %>
                        <div id="playlist_product-<%= productIdx %>" class="product content-sound" data-left="focusLeft" data-right="focusRight"
                                <% if(j < data.Products.length-1 ){ %>
                                    data-right="focusRight"
                                <% } %>

                             <% if(j % data.totalProductsPerPage >= 6){ %>
                                data-up="focusUp"
                             <% } %>

                             <% if(j % data.totalProductsPerPage < 12){ %>
                                data-down="focusDown"
                             <% } %>
                             data-id="<%= product.Id %>" data-type="<%= product.StructureType %>">
                            <div class="productTitle"><% if(product.Name.length > 20){ print("<marquee scrolldelay=125>"+product.Name+"</marquee>")} else { print(product.Name) } %></div>
	                          <p class="remainingWatchTimeLeft"><%- product.LicenseExpirationTimeString %></p>
                            <div class="productBorder"></div>
                            <img src="<%= product.ThumbnailUrl %>" alt="<%= product.Name %>" />
                        </div>
                        <% if(j % data.totalProductsPerPage == data.totalProductsPerPage-1){ %>
                            </div>
                        <% } %>
                    <% } %>
                    <% if(j % data.totalProductsPerPage != data.totalProductsPerPage-1){ %>
                          </div>
                    <% } %>
                <% }else{ %>
                    <div class="noResultsContent">
                        <div class="description">There are no products available for display</div>
                    </div>
                <% } %>
            </div>
        </div>
    </div>
</div>
