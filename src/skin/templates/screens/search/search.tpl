<div id="container" class="search">
    <div id="backgroundOverlay"></div>
    <!--#header-->
    <div id="menu">
        <div id="subBar">

            <div id="subMenuItems">
                <div id="subMenuHeader">
                    <%- SubMenuTitle %>
                </div>
                <div class="divider"></div>
            </div>
        </div>
    </div>
    <!--#navigation arrows-->
    <div class="navigationContainer">
        <div id="navigationLeft" class="navigationButton menu-sound" data-right="defaultFocus">
            <div class="icon" />
        </div>
        <div id="navigationRight" class="navigationButton menu-sound" data-left="defaultFocus">
            <div class="icon" />
        </div>
    </div>
    <!--#content-->
    <div id="viewPort">
        <div id="content">
            <input id="searchInput" disable/>
            <div id="searchResults"></div>
            <div id="ime"></div>
        </div>
    </div>
</div>
</div>
