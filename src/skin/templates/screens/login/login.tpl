<div class="content login">
    <div id="loginContent" class="fullscreen">
        <div id="logo"></div>
        <div id="screenCloseButton" class="menu-sound closeButton" data-down="defaultFocus"><span class='buttonText'><%- manifest.close %></span></div>
        <div class="loginContainer">
            <div class="header">
                <div class="title"></div>
                <div class="sps-logo"></div>
            </div>
            <div class="divider"></div>

            <div class="subtitle"></div>
            <div id="loginResult"></div>
            <div class="code"></div>
            <input id="inputField" data-down="focusDownFromKeyboard"/>

            <div class="buttons">
                <div id="cancelButton" data-up="focusUp" data-down="focusDown" data-right="focusRight" class="button"><div class="glow"></div><%- manifest.login_cancel %></div>
                <div id="nextButton" data-up="focusUp" data-down="focusDown" data-left="defaultFocus" data-right="defaultFocus" class="button"><div class="glow"></div><%- manifest.login_next %></div>
                <div id="forgetPasswordButton" data-up="focusUp" data-down="focusDown" data-left="defaultFocus" data-right="defaultFocus" class="button"><div class="glow"></div><%- manifest.login_forgot_password %></div>
                <div id="loginButton" data-up="focusUp" data-down="focusDown" data-left="defaultFocus" class="button"><div class="glow"></div><%- manifest.login_login %></div>
                <div id="reminderLoginButton" data-up="focusUp" data-down="focusDown" data-left="#cancelButton" class="button"><div class="glow"></div><%- manifest.login_login %></div>
            </div>
        </div>
    </div>
    <div class="helpContainer disabled">
        <div class="divider"></div>
        <div class="message"><%- manifest.help_message %></div>
        <div id="helpButton" class="button" data-up="focusUp"><%- manifest.help %></div>
    </div>
</div>

