<div class="content pin">
    <div id="loginContent" class="fullscreen">

        <div id="logo"></div>
        <div id="screenCloseButton" class="menu-sound closeButton" data-down="defaultFocus"><span class='buttonText'><%- manifest.close %></span></div>
        <div class="pinContainer">
            <div class="header">
                <div class="title"></div>
                <div class="sps-logo"></div>
            </div>
            <div class="divider"></div>

            <div class="subtitle"></div>
            <div id="loginResult"></div>
            <input id="inputField"/>
            <div class="buttons">
                <div id="confirmButton" data-down="focusDown" data-right="defaultFocus" class="button"><%- manifest.pin_confirm %></div>
                <div id="changePinButton" data-down="focusDown" data-left="defaultFocus" class="button"><%- manifest.pin_change_pin %></div>
                <div id="setPinButton" data-down="focusKeyboard" data-left="focusKeyboard" data-right="defaultFocus" class="button inactive"><%- manifest.pin_set_pin %></div>
                <div id="skipButton" data-down="focusDown" data-left="focusLeft" class="button"><%- manifest.pin_skip %></div>
                <div id="closeButton" data-down="focusDown" class="button"><%- manifest.close %></div>

                <div id="passwordCancelButton" data-up="focusKeyboard" data-right="defaultFocus" class="button"><%- manifest.cancel %></div>
                <div id="passwordChangePinButton" data-up="focusKeyboard" data-left="defaultFocus" class="button"><%- manifest.pin_change_pin %></div>

                <div id="nextButton" data-left="focusKeyboard" data-right="focusRight" class="button"><%- manifest.next %></div>
                <div id="forgotButton" data-left="focusLeft" data-right="defaultFocus" class="button"><%- manifest.pin_forgot_pin %></div>
                <div id="backButton" data-left="focusLeft" class="button"><%- manifest.back %></div>

                <div id="passwordReminder" data-up="focusKeyboard" data-right="defaultFocus" class="button"><%- manifest.login_forgot_password %></div>
                <div id="resetPin" data-left="defaultFocus" data-up="focusKeyboard" class="button"><%- manifest.pin_disable_pin %></div>

            </div>

            <div id="ime"></div>
            <div id="pin">
                <div id="one" class="pinfield"><span></span></div>
                <div id="two" class="pinfield"><span></span></div>
                <div id="three" class="pinfield"><span></span></div>
                <div id="four" class="pinfield"><span></span></div>
                <input type="text" id="pinNumber" maxlength="4" />
            </div>

        </div>
    </div>
    <div class="helpContainer">
        <div class="divider"></div>
        <div class="message"><%- manifest.help_message %></div>
        <div id="helpButton" class="button" data-up="focusHelpUp"><%- manifest.help %></div>
    </div>
</div>

