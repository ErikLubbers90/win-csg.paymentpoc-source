<div class="content login">
    <div id="launchContent" class="fullscreen">
        <iframe id="accountFrame" src="" style="width:100%;height:100%;border:0;"></iframe>
    </div>
    <div id="iframeOverlay">
        <div id="returnPage" data-left="defaultFocus" data-right="defaultFocus" class="returnButton"></div>
    </div>


    <div id="successContent" class="fullscreen redemption-success settingsContainer">
        <div class="redemptionTitle"><%- manifest.account_success %></div>
        <div class="divider"></div>
        <div class="subtitle"><%= manifest.account_your_account_has_been_created %></div>                   
        <div class="buttons">        
            <div id="browseMoviesButton" class="button"><%- manifest.account_button_browse_movies %></div>
            <div id="browseLibraryButton" class="button"><%- manifest.account_button_go_to_library %></div>
        </div>
    </div>       
</div>

