<div id="debugText"></div>
<div id="vpPlayer"></div>
<div id="triviaContainer">
    <div class="content" data-left="focusDown" data-down="focusDown">
        <div class="thumbnail">
            <div class="icon"></div>
        </div>
        <p class="message">Message</p>
    </div>
</div>
<div id="purchaseSuccesfull">
    <div class="thumb"></div>
    <div class="content">
        <p></p>
        <div class="buttons">
            <div id="previewContinueButton" class="no-sound" data-right="#previewBackButton"><%- manifest.ten_minute_preview_continue_watching %></div>
            <div id="previewBackButton" class="no-sound" data-left="#previewContinueButton"><%- manifest.ten_minute_preview_back %></div>
        </div>
    </div>

</div>
<div id="overlay">
    <div id="header">

	    <div id="playerBackButton" class="menu-sound backButton" data-down="focusDown"><span class='buttonText'><%- manifest.back %></span></div>
        <!--
        <div id="subs" class="playerButton headerButton disable no-sound" data-down="focusDown" data-right="focusRight"></div>
        <div id="audio" class="playerButton headerButton disable no-sound" data-down="focusDown" data-right="focusRight" data-left="focusLeft"></div>
        <div id="extras" class="playerButton headerButton disable no-sound" data-down="focusDown" data-left="focusLeft"></div>
        -->
        <div class="purchaseOptions">
            <div id="rightPreviewButton" class="headerButton disable no-sound" data-down="focusDown" data-left="#leftPreviewButton"></div>
            <div id="leftPreviewButton" class="headerButton disable no-sound" data-down="focusDown" data-right="#rightPreviewButton"></div>
            <div id="purchasePreviewProduct"></div>
        </div>
    </div>
    <div class="vpContainer">
        <div class="background"></div>
        <div class="videoInfo">
            <div class="title"></div>
            <div class="year"></div>
            <div class="quality"></div>
        </div>
        <div id="progress">
            <div id="chapter-thumb-container" data-down="#play">
                <img id="chapter-thumb"/>
                <div class="chapter-info"></div>
                <div id="chapter-arrow-down"></div>
            </div>
            <div class="progress">
                <div id="progress-dot" draggable></div>
                <div id="progressBar">
                    <div id="video-progress"></div>
                </div>
                <div id="chapters">
                </div>
            </div>
        </div>
        <div id="controls">
	        <div class="buttonControls">
                <div id="rewindChapter" class="playerButton disable no-sound" data-up="focusUp" data-down="focusDown" data-right="defaultFocus"></div>
                <div id="rewind" class="playerButton no-sound" data-up="focusUp" data-down="focusDown" data-right="defaultFocus" data-left="focusLeft">
                    <div class="speed">1x</div>
                </div>
                <div id="play" class="playerButton pause no-sound" data-up="focusUp" data-down="focusDown" data-left="defaultFocus" data-right="defaultFocus"></div>
    <!--            <div id="stop" class="playerButton" data-left="defaultFocus" data-right="defaultFocus"></div>-->
                <div id="forward" class="playerButton no-sound" data-up="focusUp" data-down="focusDown" data-right="focusRight" data-left="defaultFocus">
                    <div class="speed">1x</div>
                </div>
                <div id="forwardChapter" class="playerButton disable no-sound" data-up="focusUp" data-down="focusDown" data-left="defaultFocus" data-right="focusRight"></div>

                <div id="extras" class="playerButton disable no-sound" data-up="focusUp" data-down="focusDown"  data-left="focusLeft" data-right="focusRight"></div>
                <div id="subs" class="playerButton disable audio-sub-button no-sound" data-up="focusUp" data-left="focusLeft" data-right="focusRight"><span class="icon"></span><%- manifest.subtitles %></div>
                <div id="audio" class="playerButton disable audio-sub-button no-sound" data-up="focusUp" data-left="focusLeft"><span class="icon"></span><%- manifest.languages %></div>

            </div>
            <div class="playerTimes">
                <span id="curTime">00:00</span><span class="time-divider"> - </span><span id="totalTime"></span>
            </div>
            <div class="previewPlayerTimes">
                <span class="remaining"><div class="progressTimeIcon"></div><%- manifest.ten_minute_preview_time_remaining %></span><span id="remainingTime">00:00</span>
            </div>
        </div>
    </div>
</div>
<div id="subtitleContainer">
    <div class="background"><div class="subtitleContent"></div></div>
</div>


