<div id="container" class="background">

    <!--#header-->
    <div id="menu">
        <div id="subBar">

            <div id="subMenuItems">
                <div id="subMenuHeader">
                    Browse
                </div>
                <div class="divider"></div>
                <% for(var i=0;i<SubMenuItems.length;i++){ %>
                <% var subMenuItem = SubMenuItems[i] %>
                <div id="submenu_<%= i %>" class="menuButton submenu menu-sound"
                     data-left="moveToMainMenu"
                     data-right="moveToContent"
                    <% if(i > 0){ %>
                        data-up="defaultFocus"
                    <% } %>

                    <% if(i < SubMenuItems.length-1){ %>
                        data-down="defaultFocus"
                    <% } %>
                     data-id="<%= subMenuItem.Id %>"><span class="icon noAnimation"></span><%= subMenuItem.Name %></div>
                <% } %>
            </div>
        </div>
    </div>
		<!--#navigation arrows-->
		<div class="navigationContainer">
			<div id="navigationLeft" class="navigationButton menu-sound" data-right="defaultFocus">
				<div class="icon" />
			</div>
			<div id="navigationRight" class="navigationButton menu-sound" data-left="defaultFocus">
				<div class="icon" />
			</div>
		</div>
    <!--#content-->
    <div id="viewPort">
        <div id="content">
            <div class="playlistContent">
                <% productIdx = -1 %>
                <% currentCol = -1 %>
                <% currentPage = -1 %>
                <% if(FeaturedPlaylist.length > 0) { %>
                    <% currentPage++ %>
                    <div id="playlist_page-0" class="playlist-page first-page">
                    <% var product = "" %>
                    <% for(var k=0;k<Math.min(FeaturedPlaylist.length, FeaturedPlaylistLimit);k++) { %>
                        <% productIdx++ %>
                        <% product = FeaturedPlaylist[k] %>
                        <% if(k % 2 == 0){ %>
                            <% currentCol++ %>
                            <div id="playlist_col-<%- currentCol %>" class="playlist-col featured">
                        <% } %>

                        <div id="playlist_product-<%= productIdx %>" class="product featured featured-<%-productIdx %> content-sound" data-left="focusLeft" data-right="focusRight"
                             <% if( k % 2 != 0){ %>
                                data-up="focusUp"
                             <% } %>
                             <% if( k % 2 != 1){ %>
                                data-down="focusDown"
                             <% } %>
                             data-id="<%= product.Id %>" data-type="<%= product.StructureType %>">

                            <div class="productTitle"><%= product.Name %></div>
                            <div class="productBorder"></div>
                            <img src="<%= product.FeaturedSmallBackground %>" alt="" class="background-asset"/>
                            <img src="<%= product.FeaturedSmallForeground %>" alt="" class="foreground-asset" />
                        </div>
                        <% if(k % 2 == 1){ %>
                            </div>
                        <% } %>
                    <% } %>
                    </div>
                <% } %>

            </div>
        </div><!-- END #content -->

    </div> <!-- END #viewPort -->
</div><!-- END .container -->
