<div class="content login promocode">
    <div id="loginContent" class="fullscreen">
        <div id="logo"></div>
        <div id="screenCloseButton" class="menu-sound closeButton" data-down="defaultFocus"><span class='buttonText'><%- manifest.close %></span></div>
        <div class="loginContainer">
            <div class="header">
                <div class="title"><%- manifest.payment_promo_code %></div>
                <div class="sps-logo"></div>
            </div>
            <div class="divider"></div>
            <div class="subtitle"><%- manifest.payment_promo_subtitle %></div>
            <div id="loginResult"></div>
            <input id="inputField" placeholder="Enter Promo Code"/>

            <div id="ime"></div>
            <div class="buttons">
                <div id="cancelButton" data-up="focusUp" data-right="defaultFocus" data-down="focusDown" class="button"><%- manifest.back %></div>
                <div id="submitButton" data-up="focusUp" data-left="defaultFocus" data-down="focusDown" class="button"><%- manifest.payment_submit %></div>
            </div>
        </div>
    </div>
    <div class="helpContainer disable">
        <div class="divider"></div>
        <div class="message"><%- manifest.help_message_promo %></div>
        <div id="helpButton" class="button" data-up="#submitButton"><%- manifest.help %></div>
    </div>

</div>

