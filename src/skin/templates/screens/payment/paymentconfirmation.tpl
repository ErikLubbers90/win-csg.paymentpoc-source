<div class="content payment paymentConfirmation">
    <div id="paymentContent">
        <div id="logo"></div>
        <div id="screenCloseButton" class="menu-sound closeButton" data-down="defaultFocus"><span class='buttonText'><%- manifest.close %></span></div>
	      <div id="thumbContainer">
          <img class="thumb" src="<%- ProductThumbnail %>"/>
		      <p class="rentalDescription disable"><%- manifest.payment_rental_description %></p>
	      </div>

        <div class="paymentContainer">
            <div class="header">
                <div class="title"><%- manifest.payment_confirm_purchase %></div>
                <div class="sps-logo"></div>
            </div>
            <div class="divider"></div>
            <div class="item-info">
                <div class="row product-title">
                    <div class="left"><%- ProductTitle %></div>
                    <div class="right"><%- data.SubTotalAmount %></div>
                </div>
                <% if(data.DiscountAmount > 0){ %>
                    <div class="row product-title">
                        <div class="left"><%- manifest.payment_discount %></div>
                        <div class="right">-<%- data.DiscountAmountAsPrice %></div>
                    </div>
                <% } %>
                <div class="row product-tax">
                    <div class="left"><%= manifest.payment_sales_tax %></div>
                    <div class="right"><%= data.TaxAmount %></div>
                </div>
                <div class="row order-total">
                    <div class="left"><%- manifest.payment_order_total %></div>
                    <div class="right"><%= data.TotalAmountAsPrice %></div>
                </div>
            </div>
            <div class="divider"></div>
            <div class="payment-info">
                <div class="payment-title"><%- manifest.payment_method %></div>
                <div class="payment-container">
                    <% for(var i=0;i<PaymentInstruments.length;i++){ %>
                        <% if(PaymentInstruments[i].TypeName == "Credit Card"){ %>
                            <div class="payment-item <%= (PaymentInstruments[i].Id == SelectedPaymentInstrument.Id ? '':'disable') %>" data-paymentid="<%- PaymentInstruments[i].Id %>"><%-PaymentInstruments[i].CardType%> ****-****-****-<%- PaymentInstruments[i].LastPartOfID %></div>
                        <% }else if(PaymentInstruments[i].TypeName == "PayPal Account"){ %>
                            <div class="payment-item <%= (PaymentInstruments[i].Id == SelectedPaymentInstrument.Id ? '':'disable') %>" data-paymentid="<%- PaymentInstruments[i].Id %>"><span class="paypal-icon"></span>
                            <% if(typeof PaymentInstruments[i].PayPalAccount !== "undefined" && typeof PaymentInstruments[i].PayPalAccount.UserName !== "undefined"){ %>
                                <%- PaymentInstruments[i].PayPalAccount.UserName %>
                            <% }else{ %>
                                <%- PaymentInstruments[i].TypeName %>
                            <% } %>
                            </div>
                        <% }else{ %>
                            <div class="payment-item <%= (PaymentInstruments[i].Id == SelectedPaymentInstrument.Id ? '':'disable') %>" data-paymentid="<%- PaymentInstruments[i].Id %>"><%- PaymentInstruments[i].Name %></div>
                        <% } %>
                    <% } %>
                    <% if(PaymentInstruments.length > 1){ %>
                        <div id="changePayment" class="button" data-down="#purchaseButton"><%- manifest.payment_change_payment %></div>
                    <% } %>
                </div>
            </div>
            <div class="divider"></div>

            <div class="buttons">
                <div id="purchaseButton" data-up="focusUp" data-right="defaultFocus" class="button"><%- manifest.payment_purchase %></div>
                <div id="promoButton" data-up="focusUp" data-left="defaultFocus" data-right="focusRight" class="button"><%- manifest.payment_promo_code %></div>
                <div id="removePromoButton" data-up="focusUp" data-left="defaultFocus" data-right="defaultFocus" class="button disable"><%- manifest.payment_remove_code %></div>
                <div id="cancelButton" data-up="focusUp" data-left="focusLeft" class="button"><%- manifest.cancel %></div>
            </div>
        </div>
    </div>
</div>

