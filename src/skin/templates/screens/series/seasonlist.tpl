<div id="container" class="series-list">
    <div id="menu">
        <div id="mainMenu"></div>
        <div id="subBar">

            <div id="subMenuItems">
                <div id="subMenuHeader">
                    <%- SubMenuTitle %>
                </div>
                <div class="divider"></div>
                <% for(var i=0;i<SubMenuItems.length;i++){ %>
                <% var subMenuItem = SubMenuItems[i] %>
                <% if(!SubMenuOptionalVisible && subMenuItem.Optional){ %>
                <% }else{ %>
                <div id="submenu_<%= i %>" class="menuButton submenu menu-sound"
                     data-left="moveToMainMenu"
                     data-right="moveToContent"
                    <% if(i > 0){ %>
                        data-up="defaultFocus"
                    <% } %>

                    <% if(i == 0){ %>
                        data-up="focusBack"
                    <% } %>

                    <% if(i < SubMenuItems.length-1){ %>
                        data-down="defaultFocus"
                    <% } %>
                     data-id="<%= subMenuItem.Id %>"><span class="icon"></span><%= subMenuItem.Name %></div>
                <% } %>
                <% } %>
            </div>
        </div>
    </div>
    <div class="series-name">
        <%- Name %>
    </div>
    <div class="season-container">
        <div class="scroll-container">
            <% for(var i=0;i<BundleChildren.length;i++){ %>
                <div id="season-<%- i %>" class="season-thumbnail" data-left="focusLeft" data-right="focusRight">
                    <div class="season-border"></div>
                    <img src="<%- BundleChildren[i].ThumbnailUrl %>"/>
                    <div class="season-title">
                        Season <%- BundleChildren[i].SeasonNumber %>
                    </div>
                </div>
            <% } %>
        </div>
    </div>

</div>
