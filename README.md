
# CSG VOD Core

## TV app (white label)
This application is a white label product, one can make change to the core product or start a customised distribution. 

The application source is universal for all TV platform, the build phase adds the necessary differentiations for the supported TV platforms.

To start a new distribution please clone this project in GIT and push it to a new repository, due to the shared git history one is able to pull in upstream changes from the core white label product, while keeping the distribution specific changes separated.

---

## Prerequisites
1. Install **[node](https://nodejs.org/)**     
2. Install **grunt**    
   - _npm install -g grunt-cli_
3. Install **project dependencies**     
   - _npm install_ (from the project directory)

---

## Project directories
**./build**     
This directory contains output files of the build process

**./src**    
This directory contains the application source files
 

**./task**    
This directory contains platform specific build configurations automatically performed after the main build process. It contains the platform build scripts and platform resource files (e.g. icons)

---

## Development, Configuration and Testing


#### configuration
The application has a configuration file, it contains the base configuration of the app. 

It allows one to enable/disable features or change the CD configuration.

Find more details here:    
**./src/scripts/config.js**

#### theming
Theming is based on images and css styles. 

We are making use of [less](http://lesscss.org/), which transpiles back to css. This allows us to make use of variables to easily update color scheme's across all UI elements

One can use an editor that supports less (e.g. Intellij) or use the below cli command in the root project directory to auto transpile less files after a change directly to css.

```
    npm install -g autoless
    autoless ./src
```

Base theme configuration file:   
**./src/skin/less/globals.less**
 
Advanced theme configuration file:   
**./src/skin/less/theme.less**

Images are stored in 2 resolution optimised for 720p and 1080p app resolutions. (Images in the 720 folder are a factor 1.5 smaller in pixel size.) So one is always required to update both resolutions!

Theme image folder:    
**./src/skin/720/media/theme**    
**./src/skin/1080/media/theme**


#### coffee-script
The core application source files are written in coffee-script, coffee-script improves readability of  (complex) code and adds syntactic sugar

We advice to read the excellent documentation on ([coffee-script](http://coffeescript.org)). All coffee-script files should be compiled directly during editing. 

One can use an editor that supports coffee-script (e.g. Intellij) or use the below cli command in the root project directory to auto transpile coffee-script files after a change directly to JavaScript.

```
    npm install -g coffee-script
    coffee --compile --watch --bare ./src
```

#### run and test the app from source
The application can be run directly from the source files in a google chrome webbrowser by pointing a webserver to the ./src folder. 

To start a local webserver at localhost:8000 one could use the below ws service:

```
    npm install -g local-web-server
    ws -d ./src
```

---

## Build process
The build process converts the universal source files to an application package optimised for the designated TV platform.

#### build configuration
The **./package.json** file is the node project file and it contains the build configuration parameters for the grunt build script. 

The values in this file are used during the grunt build process and sometime over-rule (source) configuration and/or provide additional metadata information for the TV platforms.

Please update the below properties for each new distribution!

Properties:   
 
- *name*: project name 
    - lowercase without spaces
- *title*: application title
- *email*: support email
- *link*: support website
- *category*: samsung category
- *version*: application version number
- *build*: build number (requires manual increment)
    - increment this for core white label app version 
- *distribution*: distribution number (requires manual increment) 
    - increment this for theme distribution version
- *debug*: enable/disable application logs on compiled builds
- *environment*: CD environment on compiled build (development, staging, production)
- *platforms*: supported TV platforms
- *resolutions*: build resolutions for supported tv platforms


#### build commands (cli)
_optimize:_    
build and optimise the app for the destinated TV platform

```
    grunt optimize
```

_compress:_    
build and optimise the app for the destinated TV platform and compress all the JS+CSS files

```
    grunt compress
```

_deploy:_    
build and optimise the app for the destinated TV platform, compress all the JS+CSS files and zip the package

```
    grunt deploy
```
	
**Optional arguments:**

_platform:_    
By default a project is build for all supported platforms configured in the build configuration (package.json), if you intent to build only for 1 platform add this platform as an argument

```
    --platform=<platform>
```

#### Android builds
Make sure you have the cordova CLI tools installed

```
    npm install -g cordova_
```
  
When a new build from source is generated the Android project requires an update, one needs to call the below cordova command to update the Android Project

```
    cd cordova
    cordova build android
```

An apk file can be generated from Android Studio, import the Android gradle project found on the below project path:

```
    cd cordova/platforms/android
```

## Installation
#### Core
1. unzip the Core application package and upload it to a public web-server
2. point a google chrome browser the the service url

#### Samsung
##### Orsay
1. unzip the samsung application package to a usb flash-drive
2. press the "SmartHub" key on the remote to start the SmartHub
3. insert the usb flash-drive into the TV

##### Tizen
1. sign the wgt package file
	1. import the wgt package file into the Samsung (Partner) IDE
	2. right-click on the project and select "build package" to generate a signed package file
2. copy the signed package to a folder *./userwidget* on a usb flash-drive
3. insert the usb flash-drive  into the TV
4. press the "SmartHub" key on the remote to start the SmartHub
5. Selected "Featured" -> Apps -> My apps

#### LG
##### NetCast
1. unzip the LG application package and upload it to a public web-server
2. create a LG netCast package at the LG developer [portal](http://developer.lge.com/apptest/retrieveApptestNCList.dev) by specifying the web-server address
3. download the signed package file and un-zip it to a folder *./lgapps/installed/* on a usb flash-drive
4. insert the usb flash-drive into the TV

##### WebOS
1. unzip the LG application package to a public web-server
2. create a WebOs hosted app package, see [documentation](http://developer.lge.com/webOSTV/develop/web-app/app-developer-guide/creating-and-deploying-hosted-web-application/)
3. create a LG WebOs package at the LG developer [portal](http://developer.lge.com/apptest/retrieveApptestNCList.dev) 
5. download the signed package and un-zip it to a folder *./developer/usr/palm/applications/* on a usb flash-drive
6. insert the usb flash-drive into the TV

#### Android
1. Allow the device to install from unkown sources: Settings >> Applications >> Unknown sources.
2. Install the apk using usb